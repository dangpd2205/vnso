<?php

namespace ADMIN;

class ArtistController extends \BaseController {
	private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý nghệ sĩ';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }
    public function postResearchCategory() {
        if (\Request::ajax()) {
            foreach ($this->listLang as $item) {
                $name = \Input::get('product_add_cat_name_' . $item->code);
                $parent = \Input::get('product_add_cat_parent_' . $item->code);
                $des = \Input::get('product_add_cat_description_' . $item->code);
                $tblNewsCategoryModel = new \tblArtistCategoryModel();
                $tblNewsCategoryModel->name = $name;
                $tblNewsCategoryModel->parent = $parent;
                $tblNewsCategoryModel->lang_id = $item->id;
                $tblNewsCategoryModel->description = $des;
                $tblNewsCategoryModel->slug = \Tree::gen_slug($name);
                $tblNewsCategoryModel->save();
            }
            $out_put = [];
            foreach ($this->listLang as $item1) {
                $array = \tblArtistCategoryModel::where('lang_id', $item1->id)->orderBy('tbl_artist_category.name')->get();
                $out_put[] = array(
                    'check' => $item1->code,
                    'content' => \Tree::treereturn($array, [], $item1->id)
                );
            }
            echo json_encode($out_put);
        }
    }
    /* danh muc tin tuc */
    public function getCategoryView() {
		$all_category = \tblArtistCategoryModel::orderBy('position','asc')->get();
        $this->titlepage = 'Quản lý nhóm nghệ sĩ';
        $tblArtistCategoryModel = \tblArtistCategoryModel::orderBy('position','asc')->get();
        return \View::make('admin.artist.category.add_category')->with('data', $tblArtistCategoryModel)->with('all_category', $all_category);
    }
	
	public function getAddCate(){
		$all_category = \tblArtistCategoryModel::orderBy('name')->get();
        return \View::make('admin.artist.category.add_cate')->with('all_category', $all_category);
	}
	
	public function postAddCate(){
		$all_input = \Input::all();
		foreach ($this->listLang as $item) {
			if(\Input::get('cate_name_' . $item->code)!=''){
				
                $tbl_seo = new \tblSeoModel();
				$tbl_seo->title = isset($all_input['seo_title_' . $item->code]) ? $all_input['seo_title_' . $item->code] : '';
				$tbl_seo->description = isset($all_input['seo_description_' . $item->code]) ? $all_input['seo_description_' . $item->code] : '';
				$tbl_seo->keyword = isset($all_input['seo_keyword_' . $item->code]) ? $all_input['seo_keyword_' . $item->code] : '';
				$tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
				$tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
				$tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'None';
				$tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item->code]) ? $all_input['seo_f_title_' . $item->code] : '';
				$tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item->code]) ? $all_input['seo_f_description_' . $item->code] : '';
				$tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item->code]) ? $all_input['seo_f_image_' . $item->code] : '';
				$tbl_seo->g_title = isset($all_input['seo_g_title_' . $item->code]) ? $all_input['seo_g_title_' . $item->code] : '';
				$tbl_seo->g_description = isset($all_input['seo_g_description_' . $item->code]) ? $all_input['seo_g_description_' . $item->code] : '';
				$tbl_seo->g_image = isset($all_input['seo_g_image_' . $item->code]) ? $all_input['seo_g_image_' . $item->code] : '';
				$tbl_seo->save();
				$name = strip_tags(\Input::get('cate_name_' . $item->code));
				$parent = \Input::get('news_add_cat_parent_' . $item->code);
				$des = strip_tags(\Input::get('news_add_cat_description_' . $item->code));
				$tblCateNewsModel = new \tblArtistCategoryModel();
				$tblCateNewsModel->name = $name;
				$tblCateNewsModel->description = $des;
				$tblCateNewsModel->parent = $parent;
				$tblCateNewsModel->lang_id = $item->id;
				$tblCateNewsModel->seo_id = $tbl_seo->id;				
				$check = \tblArtistCategoryModel::where('slug',\Tree::gen_slug($name))->first();
				if(count($check)>0){
					$maxid = \tblArtistCategoryModel::count();
					$tblCateNewsModel->slug = \Tree::gen_slug($name).($maxid+1);
				}else{
					$tblCateNewsModel->slug = \Tree::gen_slug($name);
				}			
				$tblCateNewsModel->save();
			}
		}
		\Session::flash('alert_success', \Lang::get('admin/message.msg_add.success'));
		return \Redirect::action('\ADMIN\ArtistController@getAddCate');
	}
	
	public function getEditCate($id){
		$dataedit = \tblArtistCategoryModel::find($id);
		$seo = \tblSeoModel::find($dataedit->seo_id);
		$all_category = \tblArtistCategoryModel::orderBy('name')->get();
		return \View::make('admin.artist.category.edit_cate')->with('dataedit', $dataedit)->with('seo', $seo)->with('all_category', $all_category);
	}
	
	public function postEditCate(){
		$all_input = \Input::all();
		if(\Input::get('cate_name')!=''){			
			$maxid = \tblArtistCategoryModel::select(\DB::raw("max(id) as maxid"))->first();
			$check = \tblArtistCategoryModel::where('slug',\Tree::gen_slug(\Input::get('cate_name')))->where('id','!=',\Input::get('id_cate'))->first();
			$tblCateNewsModel = \tblArtistCategoryModel::find(\Input::get('id_cate'));
			$tblCateNewsModel->name = \Input::get('cate_name');
			$tblCateNewsModel->description = \Input::get('cate_des');
			if(count($check)>0){
				$tblCateNewsModel->slug = \Tree::gen_slug(\Input::get('cate_name')).$maxid->maxid;
			}else{
				$tblCateNewsModel->slug = \Tree::gen_slug(\Input::get('cate_name'));
			}
			$tblCateNewsModel->save();
			$tblSeoModel = \tblSeoModel::find(\Input::get('seo_id'));
			if(count($tblSeoModel)>0){
				$tblSeoModel->title = isset($all_input['seo_title']) ? $all_input['seo_title'] : '';
				$tblSeoModel->description = isset($all_input['seo_description']) ? $all_input['seo_description'] : '';
				$tblSeoModel->keyword = isset($all_input['seo_keyword']) ? $all_input['seo_keyword'] : '';
				$tblSeoModel->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
				$tblSeoModel->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
				$tblSeoModel->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'None';
				$tblSeoModel->fb_title=\Input::get('seo_f_title');
				$tblSeoModel->fb_description=\Input::get('seo_f_description');
				$tblSeoModel->fb_image=\Input::get('seo_f_image');
				$tblSeoModel->g_title=\Input::get('seo_g_title');
				$tblSeoModel->g_description=\Input::get('seo_g_description');
				$tblSeoModel->g_image=\Input::get('seo_g_image');
				$tblSeoModel->save();
			}else{
				$tbl_seo = new \tblSeoModel();
				$tbl_seo->title = isset($all_input['seo_title']) ? $all_input['seo_title'] : '';
				$tbl_seo->description = isset($all_input['seo_description_']) ? $all_input['seo_description'] : '';
				$tbl_seo->keyword = isset($all_input['seo_keyword_']) ? $all_input['seo_keyword'] : '';
				$tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
				$tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
				$tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'None';
				$tbl_seo->fb_title = isset($all_input['seo_f_title']) ? $all_input['seo_f_title'] : '';
				$tbl_seo->fb_description = isset($all_input['seo_f_description']) ? $all_input['seo_f_description'] : '';
				$tbl_seo->fb_image = isset($all_input['seo_f_image']) ? $all_input['seo_f_image'] : '';
				$tbl_seo->g_title = isset($all_input['seo_g_title']) ? $all_input['seo_g_title'] : '';
				$tbl_seo->g_description = isset($all_input['seo_g_description']) ? $all_input['seo_g_description'] : '';
				$tbl_seo->g_image = isset($all_input['seo_g_image']) ? $all_input['seo_g_image'] : '';
				$tbl_seo->save();
				$tblCateNewsModel = \tblArtistCategoryModel::find(\Input::get('id_cate'));
				$tblCateNewsModel->seo_id = $tbl_seo->id;
				$tblCateNewsModel->save();
			}
		}
		\Session::flash('alert_success', \Lang::get('admin/message.msg_update.success'));
		return \Redirect::action('\ADMIN\ArtistController@getCategoryView');
	}

    public function postCategoryAdd() {
        if (\Request::ajax()) {
            $product_cate_name = "";

            foreach ($this->listLang as $item) {
                $product_cate_name = \Input::get('product_add_cat_name_' . $item->code);
                $count = \tblArtistCategoryModel::where('name', 'LIKE', $product_cate_name . '%')->count();
                if ($count > 0) {
                    $product_cate_name = trim($product_cate_name . ' ' . $count);
                }
                $tblArtistCategoryModel = new \tblArtistCategoryModel();
                $tblArtistCategoryModel->name = \Input::get('product_add_cat_name_' . $item->code);
                $tblArtistCategoryModel->slug = \Tree::gen_slug($product_cate_name);
                $tblArtistCategoryModel->parent = \Input::get('product_add_cat_parent_' . $item->code);
                $tblArtistCategoryModel->description = \Input::get('product_add_cat_description_' . $item->code);
                $tblArtistCategoryModel->lang_id = $item->id;
                if ($tblArtistCategoryModel->name != "") {
                    $tblArtistCategoryModel->save();
                }
            }

            if ($product_cate_name != "") {
                $out_put = array(
                    'check' => 'reload',
                    'content' => ''
                );
            } else {
                $out_put = array(
                    'check' => '0',
                    'content' => '<li>Trường tên nhóm không được để trống!</li>'
                );
            }
            echo json_encode($out_put);
        }
    }

    public function postCategoryEdit() {
        if (\Request::ajax()) {
            $rules = array(
                "product_add_cat_name" => "required"
            );
            $validate = \Validator::make(\Input::all(), $rules, array(), \Lang::get('admin/attributes.tbl_category'));
            if (!$validate->fails()) {
                $name = \Input::get('product_add_cat_name');
                $tblArtistCategoryModel = \tblArtistCategoryModel::where('id', '=', \Input::get('id'))->first();
                $count = \tblArtistCategoryModel::where('name', 'LIKE', $name . '%')->count();
                if ($count > 0) {
                    $name = trim($name. ' ' . $count);
                }
                $tblArtistCategoryModel->name = \Input::get('product_add_cat_name');
                if($tblArtistCategoryModel->name != \Input::get('product_add_cat_name')){
                        $tblArtistCategoryModel->slug = \Tree::gen_slug($name);
                }
                $tblArtistCategoryModel->description = \Input::get('product_add_cat_description');
                $tblArtistCategoryModel->save();

                $out_put = array(
                    'check' => 'reload',
                    'content' => ''
                );
            } else {
                $out_put = array(
                    'check' => '0',
                    'content' => $validate->messages()->all('<li>:message</li>')
                );
            }
            echo json_encode($out_put);
        }
    }

    public function postDeleteCategory() {
        if (\Request::ajax()) {
            $product_cate_con = \tblArtistCategoryModel::where('parent', '=', \Input::get('id'))->get();
            foreach ($product_cate_con as $item_delete) {
                $product_cate_con_con = \tblArtistCategoryModel::where('parent', '=', $item_delete->id)->get();
                foreach ($product_cate_con_con as $item_delete_con) {
                    \tblArtistCategoryModel::where('id', '=', $item_delete_con->id)->delete();
                }
                \tblArtistCategoryModel::where('id', '=', $item_delete->id)->delete();
            }
            \tblArtistCategoryModel::where('id', '=', \Input::get('id'))->delete();
        }
    }

    public function postUpdatePosCategory() {
        $data = json_decode(\Input::get('data'));
        $count = 1;
        foreach ($data as $item) {
            $cate = \tblArtistCategoryModel::find($item->id);
            $cate->parent=0;
            $cate->position=$count;
            $cate->save();
            
            if (isset($item->children)) {
                $data1 = $item->children;
                $count1 = 1;
                foreach ($data1 as $item1) {
                    $cate = \tblArtistCategoryModel::find($item1->id);
                    $cate->parent=$item->id;
                    $cate->position=$count1;
                    $cate->save();
                    if (isset($item1->children)) {
                        $data2 = $item1->children;
                        $count2 = 1;
                        foreach ($data2 as $item2) {
                            $cate = \tblArtistCategoryModel::find($item2->id);
                            $cate->parent=$item1->id;
                            $cate->position=$count2;
                            $cate->save();
                            $count2++;
                        }
                    }
                    $count1++;
                }
            }
            $count++;
        }
    }

    /* tin tuc */
    public function getView() {
        \Session::forget('artist_cate_key');
        \Session::forget('artist_status_key');
        \Session::forget('artist_search_key');
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $obj_news = new \tblArtistModel();
            $obj_news->resolveConnection()->getPaginator()->setBaseUrl(\URL::action('\ADMIN\ArtistController@getView'));
            $data_lang = $obj_news->join('tbl_artist_lang', 'tbl_artist.id', '=', 'tbl_artist_lang.artist_id')->where('tbl_artist_lang.lang_id', $this->d_lang)->orderBy('tbl_artist.id', 'desc')->where('tbl_artist_lang.status', '!=', 2)->where('tbl_artist.type', 0)->paginate(10);
            $list_category = \tblArtistCategoryModel::where('lang_id',$this->d_lang)->orderBy('name')->get();
            return \View::make('admin.artist.ajax')->with('data', $data_lang)->with('list_category', $list_category);
        } else {
            $list_category = \tblArtistCategoryModel::where('lang_id',$this->d_lang)->orderBy('name')->get();
            $data  = \tblArtistModel::join('tbl_artist_lang', 'tbl_artist.id', '=', 'tbl_artist_lang.artist_id')->where('tbl_artist_lang.lang_id',$this->d_lang)->where('tbl_artist_lang.status', '!=', 2)->where('tbl_artist.type', 0)->orderBy('tbl_artist.id', 'desc')->paginate(10);
                
            return \View::make('admin.artist.view')->with('data', $data)->with('list_category', $list_category);
        }

    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('artist_cate_key');
        \Session::forget('artist_status_key');
        \Session::forget('artist_search_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword='null';
        }
        \Session::set('artist_search_key', $keyword);
        return \Redirect::action('\ADMIN\ArtistController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblArtistModel::join('tbl_artist_lang', 'tbl_artist.id', '=', 'tbl_artist_lang.artist_id')->where('tbl_artist_lang.lang_id',$this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_artist_lang.a_title', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('tbl_artist_lang.a_description', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('tbl_artist_lang.a_content', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_artist.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        $list_category = \tblArtistCategoryModel::where('lang_id',$this->d_lang)->orderBy('name')->get();
        return \View::make('admin.artist.ajax')->with('data', $data_lang)->with('list_category', $list_category);
    }

    public function postFilter(){
        $cate = '';
        $status = '';
        \Session::forget('artist_cate_key');
        \Session::forget('artist_status_key');
        \Session::forget('artist_search_key');
        if (\Input::has('news_cat_filter') || \Input::get('news_cat_filter') != '') {
            $cate=\Input::get('news_cat_filter');
        }else{
            $cate='null';
        }
        if (\Input::has('news_status_filter') || \Input::get('news_status_filter') != '') {
            $status=\Input::get('news_status_filter');
        }else{
            $status='null';
        }
        \Session::set('artist_cate_key', $cate);
        \Session::set('artist_status_key', $status);
        return \Redirect::action('\ADMIN\ArtistController@getFilter', array($cate,$status));
    }

    public function getFilter($cate='',$status=''){
        $sql_data = \tblArtistModel::join('tbl_artist_lang', 'tbl_artist.id', '=', 'tbl_artist_lang.artist_id')->where('tbl_artist_lang.lang_id', $this->d_lang);
        if($cate!='null'){
            $cat_r = \tblArtistCategoryModel::find($cate);
            $list_cate = [];
            if ($cat_r->parent == 0) {
                $list_cate[] = $cat_r->id;
                $cat_r_c = \tblArtistCategoryModel::where('parent', $cat_r->id)->get();
                foreach ($cat_r_c as $item) {
                    $list_cate[] = $item->id;
                }
            } else {
                $list_cate[] = $cat_r->id;
            }
            $sql_data->whereIn('tbl_artist.id', \tblArtistViewsModel::select('artist_id')->whereIn('cat_id', $list_cate)->get()->toArray());
        }
        if($status!='null'){
            $sql_data->where('tbl_artist_lang.status', $status);
        }
        $sql_data->orderBy('tbl_artist.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        $list_category = \tblArtistCategoryModel::where('lang_id',$this->d_lang)->orderBy('name')->get();
        return \View::make('admin.artist.ajax')->with('data', $data_lang)->with('list_category', $list_category);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\ArtistController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = \tblArtistModel::join('tbl_artist_lang', 'tbl_artist.id', '=', 'tbl_artist_lang.artist_id')->where('tbl_artist_lang.lang_id', $this->d_lang);
        if(\Session::has('artist_cate_key') && \Session::get('artist_cate_key')!='null'){
            $cat_r = \tblArtistCategoryModel::find(\Session::get('artist_cate_key'));
            $list_cate = [];
            if ($cat_r->parent == 0) {
                $list_cate[] = $cat_r->id;
                $cat_r_c = \tblArtistCategoryModel::where('parent', $cat_r->id)->get();
                foreach ($cat_r_c as $item) {
                    $list_cate[] = $item->id;
                }
            } else {
                $list_cate[] = $cat_r->id;
            }
            $sql_data->whereIn('tbl_artist.id', \tblArtistViewsModel::select('artist_id')->whereIn('cat_id', $list_cate)->get()->toArray());
        }
        if(\Session::has('artist_status_key') && \Session::get('artist_status_key')!='null'){
            $sql_data->where('tbl_artist_lang.status', \Session::get('artist_status_key'));
        }else{
            $sql_data->where('tbl_artist_lang.status', '!=',2);
        }
        if(\Session::has('artist_search_key') && \Session::get('artist_search_key')!='null'){
            $keyword=\Session::get('artist_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('tbl_artist_lang.a_title', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('tbl_artist_lang.a_description', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('tbl_artist_lang.a_content', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->orderBy('tbl_artist.id', 'desc')->paginate($page);
        $list_category = \tblArtistCategoryModel::orderBy('name')->get();
        return \View::make('admin.artist.ajax')->with('data', $data_lang)->with('list_category', $list_category);
    }

    public function getAdd() {
        $all_category = \tblArtistCategoryModel::orderBy('name')->get();
        return \View::make('admin.artist.add')->with('all_category', $all_category);
    }

    public function postAdd() {
        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();

        $log = [];
        $all_input = \Input::all();
		
        if(\Input::get('a_title_'.$tblLangModel->code)){

            $tbl_artist = new \tblArtistModel();
            $tbl_artist->avatar = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            $tbl_artist->images = ($all_input['image_hidden1'] != '') ? $all_input['image_hidden1'] : '';
			$tbl_artist->type= isset($all_input['type']) ? $all_input['type'] : 0;			$tbl_artist->position= isset($all_input['position']) ? $all_input['position'] : 5;
            $tbl_artist->user_id = \Auth::user()->id;
            
           
            $tbl_artist->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['a_title_' . $item_lang->code]) {
                    $tbl_seo = new \tblSeoModel();
                    $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                    $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                    $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                    $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                    $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                    $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                    $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                    $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                    $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                    $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                    $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                    $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                    $tbl_seo->save();
                    $tbl_detail_news = new \tblArtistLangModel();
                    $tbl_detail_news->artist_id = $tbl_artist->id;
                    $tbl_detail_news->lang_id = $item_lang->id;
                    $tbl_detail_news->seo_id = $tbl_seo->id;
                    $tbl_detail_news->a_title = ($all_input['a_title_' . $item_lang->code] != '') ? strip_tags($all_input['a_title_' . $item_lang->code]) : '';
                    $tbl_detail_news->a_description = ($all_input['a_description_' . $item_lang->code] != '') ? strip_tags($all_input['a_description_' . $item_lang->code]) : '';
                    $tbl_detail_news->a_content = ($all_input['a_content_' . $item_lang->code] != '') ? $all_input['a_content_' . $item_lang->code] : '';
                    $tbl_detail_news->a_tags = ($all_input['news_tag_' . $item_lang->code] != '') ? $all_input['news_tag_' . $item_lang->code] : '';
                    $tbl_detail_news->a_instrument = ($all_input['a_instrument_' . $item_lang->code] != '') ? $all_input['a_instrument_' . $item_lang->code] : '';
                    $tbl_detail_news->a_slug = \Tree::gen_slug($all_input['a_title_' . $item_lang->code]) . '-' . $tbl_artist->id;
                    $tbl_detail_news->a_phone = ($all_input['a_phone_' . $item_lang->code] != '') ? $all_input['a_phone_' . $item_lang->code] : '';
                            $tbl_detail_news->a_email = ($all_input['a_email_' . $item_lang->code] != '') ? $all_input['a_email_' . $item_lang->code] : '';
                    $tbl_detail_news->status = 1;
                    $tbl_detail_news->save();
					
					/* bang tag */
					if(\Input::get('news_tag_' . $item_lang->code)!=''){
						foreach(explode(',',\Input::get('news_tag_' . $item_lang->code)) as $i_tag){
							$check = \tblTagsModel::where('slug', \Tree::gen_slug($i_tag))->first();
							if(count($check)<=0){
								$tblTagModel = new \tblTagsModel();
								$tblTagModel->name = $i_tag;
								$tblTagModel->slug = \Tree::gen_slug($i_tag);
								$tblTagModel->save();
								$tblTagViewModel = new \tblTagsRelationshipModel();
								$tblTagViewModel->tags_id = $tblTagModel->id;
								$tblTagViewModel->post_id = $tbl_detail_news->id;
								$tblTagViewModel->type = 6;
								$tblTagViewModel->save();
							}else{
								$tblTagViewModel = new \tblTagsRelationshipModel();
								$tblTagViewModel->tags_id = $check->id;
								$tblTagViewModel->post_id = $tbl_detail_news->id;
								$tblTagViewModel->type = 6;
								$tblTagViewModel->save();
							}
						}
					}
					
					$cat_list = isset($all_input['cat_id_' . $item_lang->id]) ? $all_input['cat_id_' . $item_lang->id] : '';
                    if (count($cat_list) > 0 && $cat_list != '') {
                        foreach ($cat_list as $item) {
                            $tbl_artist_view = new \tblArtistViewsModel();
                            $tbl_artist_view->artist_id = $tbl_artist->id;
                            $tbl_artist_view->cat_id = $item;
                            $tbl_artist_view->lang_id = $item_lang->id;
                            $tbl_artist_view->save();
                        }
                    }
					

               

                }
            }
            return \Redirect::action('\ADMIN\ArtistController@getAdd');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }
    
    public function postQuickAddCategory() {
        
    }
    public function getEdit($lang, $id) {
        $data_news = \tblArtistModel::join('tbl_artist_lang', 'tbl_artist.id', '=', 'tbl_artist_lang.artist_id')
                    ->leftJoin('tbl_lang', 'tbl_artist_lang.lang_id', '=', 'tbl_lang.id')
                    ->leftJoin('tbl_seo', 'tbl_artist_lang.seo_id', '=', 'tbl_seo.id')
                    ->where('tbl_artist.id', $id)
                    ->select('tbl_lang.id as langid','tbl_artist.position','tbl_artist.type','tbl_artist.images','tbl_artist.avatar','tbl_artist.user_id','tbl_artist_lang.*','tbl_seo.id as seoid','tbl_seo.title','tbl_seo.description as seodesc','tbl_seo.keyword','tbl_seo.robots_index','tbl_seo.robots_follow','tbl_seo.robots_advanced','tbl_seo.fb_title','tbl_seo.fb_description','tbl_seo.fb_image','tbl_seo.g_title','tbl_seo.g_description','tbl_seo.g_image')
                    ->get();
        $lang_code = \tblLangModel::where('id',$lang)->first();

        if (count($data_news) > 0) {
            $cat_select = \tblArtistViewsModel::select('cat_id')->where('artist_id', $id)->get();
            $list_selected = [];
            foreach ($cat_select as $list_catvalue) {
                $list_selected[] = $list_catvalue->cat_id;
            }
            $all_category = \tblArtistCategoryModel::orderBy('name')->get();
		
            return \View::make('admin.artist.edit')->with('lang_code', $lang_code)->with('list_selected', $list_selected)->with('data_news', $data_news)->with('all_category', $all_category);
            
        } else {
            return \Redirect::action('\ADMIN\ArtistController@getView');
        }
    }

    public function postEdit() {

        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();
        $all_input = \Input::all();
		
        if(\Input::get('a_title_'.$tblLangModel->code)){
            $row_news = \tblArtistModel::find($all_input['id_artist']);
             $row_news->avatar = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            $row_news->images = ($all_input['image_hidden1'] != '') ? $all_input['image_hidden1'] : '';
			$row_news->type= isset($all_input['type']) ? $all_input['type'] : 0;			$row_news->position= isset($all_input['position']) ? $all_input['position'] : 5;
            $row_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['a_title_' . $item_lang->code]) {
                    $check = \tblArtistLangModel::where('artist_id', $all_input['id_artist'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                    // neu co roi thi update
                    if(count($check)>0){
                        $tbl_seo = \tblSeoModel::find($check->seo_id);
                        if(count($tbl_seo)>0){
                            $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                            $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                            $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                            $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                            $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                            $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                            $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                            $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                            $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                            $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                            $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                            $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                            $tbl_seo->save();
                            $tbl_detail_news = \tblArtistLangModel::where('artist_id', $all_input['id_artist'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->where('seo_id', $all_input['seo_id_'. $item_lang->code])->first();
                            if($tbl_detail_news->a_title != $all_input['a_title_' . $item_lang->code]){
                                $tbl_detail_news->a_slug = \Tree::gen_slug($all_input['a_title_' . $item_lang->code]) . '-' . $all_input['id_artist'];                        
                            }
                            $tbl_detail_news->a_title = ($all_input['a_title_' . $item_lang->code] != '') ? strip_tags($all_input['a_title_' . $item_lang->code]) : '';
                            
                            $tbl_detail_news->a_description = ($all_input['a_description_' . $item_lang->code] != '') ? strip_tags($all_input['a_description_' . $item_lang->code]) : '';
                            $tbl_detail_news->a_content = ($all_input['a_content_' . $item_lang->code] != '') ? $all_input['a_content_' . $item_lang->code] : '';
                            $tbl_detail_news->a_tags = ($all_input['news_tag_' . $item_lang->code] != '') ? $all_input['news_tag_' . $item_lang->code] : '';
                            $tbl_detail_news->a_instrument = ($all_input['a_instrument_' . $item_lang->code] != '') ? $all_input['a_instrument_' . $item_lang->code] : '';
                            $tbl_detail_news->a_phone = ($all_input['a_phone_' . $item_lang->code] != '') ? $all_input['a_phone_' . $item_lang->code] : '';
                            $tbl_detail_news->a_email = ($all_input['a_email_' . $item_lang->code] != '') ? $all_input['a_email_' . $item_lang->code] : '';
                            $tbl_detail_news->save();
                        }else{
                            $tblSeoModel = new \tblSeoModel();
                            $tblSeoModel->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                            $tblSeoModel->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                            $tblSeoModel->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                            $tblSeoModel->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                            $tblSeoModel->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                            $tblSeoModel->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                            $tblSeoModel->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                            $tblSeoModel->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                            $tblSeoModel->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                            $tblSeoModel->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                            $tblSeoModel->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                            $tblSeoModel->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                            $tblSeoModel->save();
                            $tbl_detail_news = \tblArtistLangModel::where('artist_id', $all_input['id_artist'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->first();
                            if($tbl_detail_news->a_title != $all_input['a_title_' . $item_lang->code]){
                                $tbl_detail_news->a_slug = \Tree::gen_slug($all_input['a_title_' . $item_lang->code]) . '-' . $all_input['id_artist'];                        
                            }
                            $tbl_detail_news->seo_id=$tblSeoModel->id;
                            $tbl_detail_news->a_title = ($all_input['a_title_' . $item_lang->code] != '') ? strip_tags($all_input['a_title_' . $item_lang->code]) : '';
                            
                            $tbl_detail_news->a_description = ($all_input['a_description_' . $item_lang->code] != '') ? strip_tags($all_input['a_description_' . $item_lang->code]) : '';
                            $tbl_detail_news->a_content = ($all_input['a_content_' . $item_lang->code] != '') ? $all_input['a_content_' . $item_lang->code] : '';
                            $tbl_detail_news->a_tags = ($all_input['news_tag_' . $item_lang->code] != '') ? $all_input['news_tag_' . $item_lang->code] : '';
                            $tbl_detail_news->a_instrument = ($all_input['a_instrument_' . $item_lang->code] != '') ? $all_input['a_instrument_' . $item_lang->code] : '';
                            $tbl_detail_news->a_phone = ($all_input['a_phone_' . $item_lang->code] != '') ? $all_input['a_phone_' . $item_lang->code] : '';
                            $tbl_detail_news->a_email = ($all_input['a_email_' . $item_lang->code] != '') ? $all_input['a_email_' . $item_lang->code] : '';
                            $tbl_detail_news->save();
                        }                        
                        
                    }else{
                        $tbl_seo = new \tblSeoModel();
                        $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                        $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                        $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                        $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                        $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                        $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                        $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                        $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                        $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                        $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                        $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                        $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                        $tbl_seo->save();
                        $tbl_detail_news = new \tblArtistLangModel();
                        $tbl_detail_news->artist_id = $all_input['id_artist'];
                        $tbl_detail_news->lang_id = $item_lang->id;
                        $tbl_detail_news->seo_id = $tbl_seo->id;
                        $tbl_detail_news->a_title = ($all_input['a_title_' . $item_lang->code] != '') ? strip_tags($all_input['a_title_' . $item_lang->code]) : '';
                        $tbl_detail_news->a_description = ($all_input['a_description_' . $item_lang->code] != '') ? strip_tags($all_input['a_description_' . $item_lang->code]) : '';
                        $tbl_detail_news->a_content = ($all_input['a_content_' . $item_lang->code] != '') ? $all_input['a_content_' . $item_lang->code] : '';
                        $tbl_detail_news->a_tags = ($all_input['news_tag_' . $item_lang->code] != '') ? $all_input['news_tag_' . $item_lang->code] : '';
                        $tbl_detail_news->a_instrument = ($all_input['a_instrument_' . $item_lang->code] != '') ? $all_input['a_instrument_' . $item_lang->code] : '';
                        $tbl_detail_news->a_slug = \Tree::gen_slug($all_input['a_title_' . $item_lang->code]) . '-' . $all_input['id_artist'];
                        $tbl_detail_news->a_phone = ($all_input['a_phone_' . $item_lang->code] != '') ? $all_input['a_phone_' . $item_lang->code] : '';
                            $tbl_detail_news->a_email = ($all_input['a_email_' . $item_lang->code] != '') ? $all_input['a_email_' . $item_lang->code] : '';
                        $tbl_detail_news->status = 1;
                        $tbl_detail_news->save();
                    }

                    /* bang tag */
                    \tblTagsRelationshipModel::where('post_id',$tbl_detail_news->id)->where('type',6)->delete();
                    if(\Input::get('news_tag_' . $item_lang->code)!=''){
                        foreach(explode(',',\Input::get('news_tag_' . $item_lang->code)) as $i_tag){
                            $check = \tblTagsModel::where('slug', \Tree::gen_slug($i_tag))->first();
                            if(count($check)<=0){
                                $tblTagModel = new \tblTagsModel();
                                $tblTagModel->name = $i_tag;
                                $tblTagModel->slug = \Tree::gen_slug($i_tag);
                                $tblTagModel->save();
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $tblTagModel->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 6;
                                $tblTagViewModel->save();
                            }else{
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $check->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 6;
                                $tblTagViewModel->save();
                            }
                        }
                    }

                    $cat_list = isset($all_input['cat_id_' . $item_lang->id]) ? $all_input['cat_id_' . $item_lang->id] : '';
                    if (count($cat_list) > 0 && $cat_list != '') {
                        \tblArtistViewsModel::where('artist_id',$all_input['id_artist'])->where('lang_id',$item_lang->id)->delete();
                        foreach ($cat_list as $item) {
                            $tbl_artist_view = new \tblArtistViewsModel();
                            $tbl_artist_view->artist_id = $all_input['id_artist'];
                            $tbl_artist_view->cat_id = $item;
                            $tbl_artist_view->lang_id = $item_lang->id;
                            $tbl_artist_view->save();
                        }
                    }

                }   
            }
    	
            return \Redirect::action('\ADMIN\ArtistController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }

    public function postDelete() {

        if (\Request::ajax()) {
            $all_input = \Input::all();
            $tblArtistLangModel = \tblArtistLangModel::where('artist_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 2));
            return $this->getView()->render();
        }
    }

    public function postPostConfirm() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblArtistLangModel::where('artist_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 1));
        }
    }

   

}
