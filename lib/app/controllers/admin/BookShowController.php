<?php

namespace ADMIN;

class BookShowController extends \BaseController {
    private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý bán vé';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }

    public function getView() {
        \Session::forget('show_cate_key1');
        \Session::forget('show_status_key1');
        \Session::forget('show_search_key1');
        \Session::forget('show_start_key1');
        \Session::forget('show_end_key1');
        \Session::forget('show_place_key1');
        $place  = \tblPlaceModel::join('tbl_place_lang', 'tbl_place.id', '=', 'tbl_place_lang.place_id')->where('tbl_place_lang.lang_id', $this->d_lang)->orderBy('tbl_place.id', 'desc')->where('tbl_place_lang.status',1)->get();
        $list_category = \tblShowCategoryModel::where('lang_id',$this->d_lang)->orderBy('name')->get();
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $obj_news = new \tblShowModel();
            $obj_news->resolveConnection()->getPaginator()->setBaseUrl(\URL::action('\ADMIN\BookShowController@getView'));
            $data_lang = $obj_news->join('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')->where('tbl_show_lang.lang_id', $this->d_lang)->orderBy('tbl_show.time_show', 'asc')->where('tbl_show_lang.status',1)->paginate(10);
            if(count($data_lang)>0){
                foreach($data_lang as $item){
                    $list_seat[] = \tblTicketShowModel::where('show_id',$item->show_id)->count();
                    $list_seat_sold[] = \tblTicketShowModel::where('show_id',$item->show_id)->where('status',3)->count();
                    if($item->artist_id!=''){
                        $list_art[] = \tblArtistLangModel::whereIn('artist_id',explode(',',$item->artist_id))->where('lang_id',$this->d_lang)->where('status',1)->get();

                    }else{
                        $list_art[]=[];
                    }
                }
            }else{
                $list_seat_sold=[];
                $list_seat=[];
                $list_art=[];
            }
            return \View::make('admin.book_show.ajax')->with('list_category', $list_category)->with('data', $data_lang)->with('list_art', $list_art)->with('list_seat', $list_seat)->with('list_seat_sold', $list_seat_sold);
        } else {
            $data  = \tblShowModel::join('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')->where('tbl_show_lang.lang_id', $this->d_lang)->orderBy('tbl_show.time_show', 'asc')->where('tbl_show_lang.status',1)->paginate(10);
            if(count($data)>0){
                foreach($data as $item){
                    $list_seat[] = \tblTicketShowModel::where('show_id',$item->show_id)->count();
                    $list_seat_sold[] = \tblTicketShowModel::where('show_id',$item->show_id)->where('status',3)->count();
                    if($item->artist_id!=''){
                        $list_art[] = \tblArtistLangModel::whereIn('artist_id',explode(',',$item->artist_id))->where('lang_id',$this->d_lang)->where('status',1)->get();
                    }else{
                        $list_art[]=[];
                    }
                }
            }else{
                $list_seat_sold=[];
                $list_seat=[];
                $list_art=[];
            }
            return \View::make('admin.book_show.view')->with('place', $place)->with('list_category', $list_category)->with('data', $data)->with('list_art', $list_art)->with('list_seat', $list_seat)->with('list_seat_sold', $list_seat_sold);
        }

    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('show_cate_key1');
        \Session::forget('show_status_key1');
        \Session::forget('show_search_key1');
        \Session::forget('show_start_key1');
        \Session::forget('show_end_key1');
        \Session::forget('show_place_key1');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword='null';
        }
        \Session::set('show_search_key1', $keyword);
        return \Redirect::action('\ADMIN\BookShowController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblShowModel::join('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')->where('tbl_show_lang.lang_id', $this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_show_lang.name', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_show.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        if(count($data_lang)>0){
            foreach($data_lang as $item){
                $list_seat[] = \tblTicketShowModel::where('show_id',$item->show_id)->count();
                $list_seat_sold[] = \tblTicketShowModel::where('show_id',$item->show_id)->where('status',3)->count();
                if($item->artist_id!=''){
                    $list_art[] = \tblArtistLangModel::whereIn('artist_id',explode(',',$item->artist_id))->where('lang_id',$this->d_lang)->where('status',1)->get();
                }else{
                    $list_art[]=[];
                }
            }
        }else{
            $list_seat_sold=[];
            $list_seat=[];
            $list_art=[];
        }
        return \View::make('admin.book_show.ajax')->with('data', $data_lang)->with('list_art', $list_art)->with('list_seat', $list_seat)->with('list_seat_sold', $list_seat_sold);
    }

    public function postFilter(){
        
        $status = '';
        \Session::forget('show_cate_key1');
        \Session::forget('show_status_key1');
        \Session::forget('show_search_key1');
        \Session::forget('show_start_key1');
        \Session::forget('show_end_key1');
        \Session::forget('show_place_key1');
        if (\Input::has('start_date_filter') || \Input::get('start_date_filter') != '') {
            $start=\Input::get('start_date_filter');
        }else{
            $start='null';
        }
        if (\Input::has('end_date_filter') || \Input::get('end_date_filter') != '') {
            $end=\Input::get('end_date_filter');
        }else{
            $end='null';
        }
        if (\Input::has('news_cat_filter') || \Input::get('news_cat_filter') != '') {
            $cate=\Input::get('news_cat_filter');
        }else{
            $cate='null';
        }
        if (\Input::has('news_status_filter') || \Input::get('news_status_filter') != '') {
            $status=\Input::get('news_status_filter');
        }else{
            $status='null';
        }
         if (\Input::has('news_place_filter') || \Input::get('news_place_filter') != '') {
            $place=\Input::get('news_place_filter');
        }else{
            $place='null';
        }
        \Session::set('show_status_key1', $status);
        \Session::set('show_cate_key1', $cate);
        \Session::set('show_start_key1',$start);
        \Session::set('show_end_key1',$end);
        \Session::set('show_place_key1',$place);
        return \Redirect::action('\ADMIN\BookShowController@getFilter', array($cate,$status,$start,$end,$place));
    }

    public function getFilter($cate='',$status='',$start='',$end='',$place=''){
        $sql_data = \tblShowModel::join('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')->where('tbl_show_lang.lang_id', $this->d_lang);
        if($cate!='null'){
            $cat_r = \tblShowCategoryModel::find($cate);
            $list_cate = [];
            if ($cat_r->parent == 0) {
                $list_cate[] = $cat_r->id;
                $cat_r_c = \tblShowCategoryModel::where('parent', $cat_r->id)->get();
                foreach ($cat_r_c as $item) {
                    $list_cate[] = $item->id;
                }
            } else {
                $list_cate[] = $cat_r->id;
            }
            $sql_data->whereIn('tbl_show.id', \tblShowViewsModel::select('show_id')->whereIn('cat_id', $list_cate)->get()->toArray());
        }
        if($start!='null'){
            $sql_data->where('tbl_show.time_show', '>=',strtotime($start));
        }
        if($end!='null'){
            $sql_data->where('tbl_show.time_show', '<=',strtotime($end));
        }
        if($status!='null'){
            $sql_data->where('tbl_show_lang.status', $status);
        }
        if($place!='null'){
            $sql_data->where('tbl_show.place_id', $place);
        }
        $sql_data->orderBy('tbl_show.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        if(count($data_lang)>0){
            foreach($data_lang as $item){
                $list_seat[] = \tblTicketShowModel::where('show_id',$item->show_id)->count();
                $list_seat_sold[] = \tblTicketShowModel::where('show_id',$item->show_id)->where('status',3)->count();
                if($item->artist_id!=''){
                    $list_art[] = \tblArtistLangModel::whereIn('artist_id',explode(',',$item->artist_id))->where('lang_id',$this->d_lang)->where('status',1)->get();
                }else{
                    $list_art[]=[];
                }
            }
        }else{
            $list_seat_sold=[];
            $list_seat=[];
            $list_art=[];
        }
        return \View::make('admin.book_show.ajax')->with('data', $data_lang)->with('list_art', $list_art)->with('list_seat', $list_seat)->with('list_seat_sold', $list_seat_sold);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\BookShowController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = \tblShowModel::join('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')->where('tbl_show_lang.lang_id', $this->d_lang);
        if(\Session::has('show_cate_key1') && \Session::get('show_cate_key1')!='null'){
            $cat_r = \tblShowCategoryModel::find(\Session::get('show_cate_key1'));
            $list_cate = [];
            if ($cat_r->parent == 0) {
                $list_cate[] = $cat_r->id;
                $cat_r_c = \tblShowCategoryModel::where('parent', $cat_r->id)->get();
                foreach ($cat_r_c as $item) {
                    $list_cate[] = $item->id;
                }
            } else {
                $list_cate[] = $cat_r->id;
            }
            $sql_data->whereIn('tbl_show.id', \tblShowViewsModel::select('show_id')->whereIn('cat_id', $list_cate)->get()->toArray());
        }
        if(\Session::has('show_start_key1') && \Session::get('show_start_key1')!='null'){
            $sql_data->where('tbl_show.time_show', '>=',strtotime(\Session::get('show_start_key1')));
        }
        if(\Session::has('show_end_key1') && \Session::get('show_end_key1')!='null'){
            $sql_data->where('tbl_show.time_show', '<=',strtotime(\Session::get('show_end_key1')));
        }
        if(\Session::has('show_status_key1') && \Session::get('show_status_key1')!='null'){
            $sql_data->where('tbl_show_lang.status', \Session::get('show_status_key1'));
        }else{
            $sql_data->where('tbl_show_lang.status',1);
        }
         if(\Session::has('show_place_key1') && \Session::get('show_place_key1')!='null'){
            $sql_data->where('tbl_show.place_id', \Session::get('show_place_key1'));
        }
        if(\Session::has('show_search_key1') && \Session::get('show_search_key1')!='null'){
            $keyword=\Session::get('show_search_key1');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('tbl_show_lang.name', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->orderBy('tbl_show.id', 'desc')->paginate($page);
        if(count($data_lang)>0){
            foreach($data_lang as $item){
                $list_seat[] = \tblTicketShowModel::where('show_id',$item->show_id)->count();
                $list_seat_sold[] = \tblTicketShowModel::where('show_id',$item->show_id)->where('status',3)->count();
                if($item->artist_id!=''){
                    $list_art[] = \tblArtistLangModel::whereIn('artist_id',explode(',',$item->artist_id))->where('lang_id',$this->d_lang)->where('status',1)->get();
                }else{
                    $list_art[]=[];
                }
            }
        }else{
            $list_seat_sold=[];
            $list_seat=[];
            $list_art=[];
        }
        return \View::make('admin.book_show.ajax')->with('data', $data_lang)->with('list_art', $list_art)->with('list_seat', $list_seat)->with('list_seat_sold', $list_seat_sold);
    }
    public function postModal(){
        \tblTicketShowModel::where('time_reset','<=',time())->where('time_reset','!=','')->where('status',2)->where('show_id',\Input::get('id'))->update(['time_reset'=>'','status'=>1]);
        $data = \tblTicketShowModel::where('show_id',\Input::get('id'))->get();
        $show = \tblShowModel::find(\Input::get('id'));
        $place = \tblPlaceLangModel::leftJoin('tbl_place_map','tbl_place_lang.place_id','=','tbl_place_map.place_id')->where('tbl_place_lang.lang_id',$this->d_lang)->where('tbl_place_lang.place_id',$show->place_id)->get();

        $ticket = \tblTicketShowModel::leftJoin('tbl_ticket_lang','tbl_ticket_show.ticket_id','=','tbl_ticket_lang.ticket_id')->where('tbl_ticket_show.show_id',\Input::get('id'))->groupBy('tbl_ticket_show.ticket_id')->get();

        if($show->place_id==1){
            return \View::make('admin.book_show.nhahat')->with('show', $show)->with('data', $data)->with('ticket', $ticket)->with('place', $place);
        }else if($show->place_id==2){
            return \View::make('admin.book_show.hoanhac')->with('show', $show)->with('data', $data)->with('ticket', $ticket)->with('place', $place);
        }        
    }
    public function getAdd($id) {
        $show = \tblShowLangModel::where('status',1)->where('show_id',$id)->where('lang_id',$this->d_lang)->first();
        if(count($show)>0){
			if($show->is_sell==0){
				$place = \tblShowModel::leftJoin('tbl_place_lang','tbl_show.place_id','=','tbl_place_lang.place_id')
							->where('tbl_show.id',$id)->where('tbl_place_lang.lang_id',$this->d_lang)->first();
				/* update lai time reset */

				return \View::make('admin.book_show.add')->with('place', $place)->with('show', $show);
			}else{
				return \Redirect::action('\ADMIN\BookShowController@getView');
			}
        }else{
            return \Redirect::action('\ADMIN\BookShowController@getView');
        }
    }
    function generateRandomString($length = 10) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;

    }
    public function postAdd() {       
        $all_input = \Input::all();
        $checkempty='';
        if(\Input::get('email')==''){
            $checkempty.='0';
        }else{
            $checkempty.='1';
        }
        if(\Input::get('phone')==''){
            $checkempty.='0';
        }else{
            $checkempty.='1';
        }
        if (strpos($checkempty, '0') !== false) {
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập thông tin đặt vé</li>');
        }else{
            if(\Input::has('name_seat') && \Input::get('name_seat')!=''){
                $show = \tblShowLangModel::where('status',1)->where('show_id',\Input::get('show_id'))->where('lang_id',$this->d_lang)->first();            
                if(count($show)>0){
                    /* check xem ghe co ton tai ko */
                    $check_seat_name='';
                    $explode_name = explode(',',$all_input['name_seat']);
                    $explode_type = explode(',',$all_input['type_seat']);
                    $i=0;
                    foreach($explode_name as $item){
                        if(isset($explode_type[$i])){
                            if($explode_type[$i]=='seat-s'){
                                $ticket_id = 3;
                            }else if($explode_type[$i]=='seat-b'){
                                $ticket_id = 1;
                            }else if($explode_type[$i]=='seat-a'){
                                $ticket_id = 2;
                            }else{
                                $ticket_id=0;
                            }
                        }else{
                            $ticket_id=0;
                        }
                        $check = \tblTicketShowModel::where('show_id',\Input::get('show_id'))->where('ticket_id',$ticket_id)->where('seat_name_orin',$item)->where('status',1)->first();						
                        if(count($check)>0){
							$check_seat_name.='1';							
                        }else{							$check1 = \tblTicketShowModel::where('show_id',\Input::get('show_id'))->where('ticket_id',$ticket_id)->where('seat_name_orin',$item)->where('status',4)->first();
                            if(count($check1)>0){								$check_seat_name.='1';							}else{								$check_seat_name.='0';							}
                        }
                        $i++;
                    }
                    /* ghe chon ko ton tai */
                    if (strpos($check_seat_name, '0') !== false) {
                        return \Redirect::back()->withInput()->withErrors('<li>Ghế chọn không hợp lệ</li>');
                    }else{						
                        $order = new \tblOrderModel();
                        $code = $this->generateRandomString().rand(10000000, 100000000);
                        $check = \tblOrderModel::where('code',$code)->first();
                        $count = \tblOrderModel::count();
                        if(count($check)>0){
                            $code .= $code.$count;
                        }
                        $order->code=$code;
                        $order->show_id=\Input::get('show_id');
                        $order->cus_name=\Input::get('name');
                        $order->cus_phone=\Input::get('phone');
                        $order->cus_email=\Input::get('email');
                        $order->cus_address=\Input::get('address');
                        $order->order_type=0;
                        $order->status=1;
                        $order->save();
                        $data = [];
                        $j=0;
                        foreach(explode(',',\Input::get('name_seat')) as $item){
                            if(isset($explode_type[$j])){
                                if($explode_type[$j]=='seat-s'){
                                    $ticket_id = 3;
                                }else if($explode_type[$j]=='seat-b'){
                                    $ticket_id = 1;
                                }else if($explode_type[$j]=='seat-a'){
                                    $ticket_id = 2;
                                }else{
                                    $ticket_id=0;
                                }
                            }else{
                                $ticket_id=0;
                            }
                            $ticket_show = \tblTicketShowModel::where('show_id',\Input::get('show_id'))->where('ticket_id',$ticket_id)->where('seat_name_orin',$item)->first();
                            $data[] = ['order_id'=>$order->id,'seat_name_orin'=>$item,'seat_name'=>$ticket_show->seat_name,'seat_price'=>$ticket_show->price,'seat_type'=>$ticket_show->ticket_id];
                            /* update lai status ve */
                            \tblTicketShowModel::where('show_id',\Input::get('show_id'))->where('seat_name_orin',$item)->where('ticket_id',$ticket_id)->update(array('status' => 3));
                            $j++;     
                        }
                        \DB::table('tbl_order_detail')->insert($data);
                        \Session::flash('noti_susscess', 'Mua vé thành công');
                        return \Redirect::action('\ADMIN\BookShowController@getAdd',\Input::get('show_id'));
                    }
                }else{
                    return \Redirect::back()->withInput()->withErrors('<li>Chương trình không hợp lệ</li>');
                }
            }else{
                return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa chọn ghế</li>');
            }
        }
    }
    public function getEdit($id){
       
    }
    public function postEdit(){

    }

}
