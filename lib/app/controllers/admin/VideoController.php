<?php

namespace ADMIN;

class VideoController extends \BaseController {
	private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý video';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }


    public function getView() {
		
        \Session::forget('video_status_key');
        \Session::forget('video_search_key');
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $obj_news = new \tblVideoModel();
            $obj_news->resolveConnection()->getPaginator()->setBaseUrl(\URL::action('\ADMIN\VideoController@getView'));
            $data_lang = $obj_news->join('tbl_video_lang', 'tbl_video.id', '=', 'tbl_video_lang.video_id')->where('tbl_video_lang.lang_id', $this->d_lang)->orderBy('tbl_video.id', 'desc')->where('tbl_video_lang.status',1)->paginate(10);
            
            return \View::make('admin.video.ajax')->with('data', $data_lang);
        } else {
            $data  = \tblVideoModel::join('tbl_video_lang', 'tbl_video.id', '=', 'tbl_video_lang.video_id')->where('tbl_video_lang.lang_id', $this->d_lang)->orderBy('tbl_video.id', 'desc')->where('tbl_video_lang.status',1)->paginate(10);
                
            return \View::make('admin.video.view')->with('data', $data);
        }

    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('video_status_key');
        \Session::forget('video_search_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword='null';
        }
        \Session::set('video_search_key', $keyword);
        return \Redirect::action('\ADMIN\VideoController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblVideoModel::join('tbl_video_lang', 'tbl_video.id', '=', 'tbl_video_lang.video_id')->where('tbl_video_lang.lang_id', $this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_video_lang.name', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_video.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.video.ajax')->with('data', $data_lang);
    }

    public function postFilter(){
        
        $status = '';
        \Session::forget('video_status_key');
        \Session::forget('video_search_key');
        
        if (\Input::has('news_status_filter') || \Input::get('news_status_filter') != '') {
            $status=\Input::get('news_status_filter');
        }else{
            $status='null';
        }
        \Session::set('video_status_key', $status);
        return \Redirect::action('\ADMIN\VideoController@getFilter', array($status));
    }

    public function getFilter($status=''){
        $sql_data = \tblVideoModel::join('tbl_video_lang', 'tbl_video.id', '=', 'tbl_video_lang.video_id')->where('tbl_video_lang.lang_id', $this->d_lang);
       
        if($status!='null'){
            $sql_data->where('tbl_video_lang.status', $status);
        }
        $sql_data->orderBy('tbl_video.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.video.ajax')->with('data', $data_lang);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\VideoController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = \tblVideoModel::join('tbl_video_lang', 'tbl_video.id', '=', 'tbl_video_lang.video_id')->where('tbl_video_lang.lang_id', $this->d_lang);
        
        if(\Session::has('video_status_key') && \Session::get('video_status_key')!='null'){
            $sql_data->where('tbl_video_lang.status', \Session::get('video_status_key'));
        }else{
            $sql_data->where('tbl_video_lang.status',1);
        }
        if(\Session::has('video_search_key') && \Session::get('video_search_key')!='null'){
            $keyword=\Session::get('video_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('tbl_video_lang.name', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->orderBy('tbl_video.id', 'desc')->paginate($page);
        return \View::make('admin.video.ajax')->with('data', $data_lang);
    }

    public function getAdd() {
        return \View::make('admin.video.add');
    }

    public function postAdd() {
        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();

        $log = [];
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){

            $tbl_news = new \tblVideoModel();
            $tbl_news->avatar = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';            
            $tbl_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    
                    $tbl_detail_news = new \tblVideoLangModel();
                    $tbl_detail_news->video_id = $tbl_news->id;
                    $tbl_detail_news->lang_id = $item_lang->id;
                    $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';
                    $tbl_detail_news->url = ($all_input['url_' . $item_lang->code] != '') ? strip_tags($all_input['url_' . $item_lang->code]) : '';       
                    $tbl_detail_news->v_tags = ($all_input['video_tag_' . $item_lang->code] != '') ? $all_input['video_tag_' . $item_lang->code] : '';                
                    $tbl_detail_news->status = 1;
                    $tbl_detail_news->save();

                    /* bang tag */
                    if(\Input::get('video_tag_' . $item_lang->code)!=''){
                        foreach(explode(',',\Input::get('video_tag_' . $item_lang->code)) as $i_tag){
                            $check = \tblTagsModel::where('slug', \Tree::gen_slug($i_tag))->first();
                            if(count($check)<=0){
                                $tblTagModel = new \tblTagsModel();
                                $tblTagModel->name = $i_tag;
                                $tblTagModel->slug = \Tree::gen_slug($i_tag);
                                $tblTagModel->save();
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $tblTagModel->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 1;
                                $tblTagViewModel->save();
                            }else{
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $check->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 1;
                                $tblTagViewModel->save();
                            }
                        }
                    }

                }
            }
            return \Redirect::action('\ADMIN\VideoController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }
    
    public function getEdit($lang, $id) {
        $data_news = \tblVideoModel::join('tbl_video_lang', 'tbl_video.id', '=', 'tbl_video_lang.video_id')
                    ->leftJoin('tbl_lang', 'tbl_video_lang.lang_id', '=', 'tbl_lang.id')
                    ->where('tbl_video.id', $id)
                    ->select('tbl_lang.id as langid','tbl_video.avatar','tbl_video_lang.*')
                    ->get();
        $lang_code = \tblLangModel::where('id',$lang)->first();
        if (count($data_news) > 0) {           
            return \View::make('admin.video.edit')->with('lang_code', $lang_code)->with('data_news', $data_news);            
        } else {
            return \Redirect::action('\ADMIN\VideoController@getView');
        }
    }

    public function postEdit() {

        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){
            $row_news = \tblVideoModel::find($all_input['id_video']);
            if ($all_input['image_hidden'] != $row_news->avatar) {
                $row_news->avatar = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            }
          
            $row_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    $check = \tblVideoLangModel::where('video_id', $all_input['id_video'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                    // neu co roi thi update
                    if(count($check)>0){                        
                        $tbl_detail_news = \tblVideoLangModel::where('video_id', $all_input['id_video'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->first();
						
                        $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';						
                        $tbl_detail_news->url = ($all_input['url_' . $item_lang->code] != '') ? strip_tags($all_input['url_' . $item_lang->code]) : '';    
                        $tbl_detail_news->v_tags = ($all_input['video_tag_' . $item_lang->code] != '') ? $all_input['video_tag_' . $item_lang->code] : '';                       
                        $tbl_detail_news->save();
                    }else{
                       
                        $tbl_detail_news = new \tblVideoLangModel();
                        $tbl_detail_news->video_id = $all_input['id_video'];
						$tbl_detail_news->lang_id = $item_lang->id;
						$tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';
						$tbl_detail_news->url = ($all_input['url_' . $item_lang->code] != '') ? strip_tags($all_input['url_' . $item_lang->code]) : '';   
                        $tbl_detail_news->v_tags = ($all_input['video_tag_' . $item_lang->code] != '') ? $all_input['video_tag_' . $item_lang->code] : '';                   
						$tbl_detail_news->status = 1;
                        $tbl_detail_news->save();
                    }
                    /* bang tag */
                    \tblTagsRelationshipModel::where('post_id',$tbl_detail_news->id)->where('type',1)->delete();
                    if(\Input::get('video_tag_' . $item_lang->code)!=''){
                        foreach(explode(',',\Input::get('video_tag_' . $item_lang->code)) as $i_tag){
                            $check = \tblTagsModel::where('slug', \Tree::gen_slug($i_tag))->first();
                            if(count($check)<=0){
                                $tblTagModel = new \tblTagsModel();
                                $tblTagModel->name = $i_tag;
                                $tblTagModel->slug = \Tree::gen_slug($i_tag);
                                $tblTagModel->save();
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $tblTagModel->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 1;
                                $tblTagViewModel->save();
                            }else{
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $check->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 1;
                                $tblTagViewModel->save();
                            }
                        }
                    }

                }   
            }
    	
            return \Redirect::action('\ADMIN\VideoController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }

    public function postDelete() {

        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblVideoLangModel::where('video_id', $all_input['id'])->update(array('status' => 2));     
             
            return $this->getView()->render();
        }
    }

    public function postPostConfirm() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
           \tblVideoLangModel::where('video_id', $all_input['id'])->update(array('status' => 1));
           
            return $this->getView()->render();
        }
    }


}
