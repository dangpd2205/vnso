<?php

namespace ADMIN;

class UserController extends \BaseController {

    private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý tài khoản';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }

    

    /* xem nhan vien */
    public function getView() {
        \Session::forget('user_search_key');
        
        \Session::forget('user_status_key');
        \Session::forget('user_start_key');
        \Session::forget('user_end_key');
        $list_group = \tblGroupAdminModel::get();
        if (\Request::ajax()) {     
            $data = \tblUsersModel::where('admin','1')->where('id','!=',\Auth::user()->id)->where('status',1)->paginate(10);
            return \View::make('admin.users.users.ajax')->with('list_group', $list_group)->where('status',1)->with('data', $data);
        } else {
            $data = \tblUsersModel::where('admin','1')->where('id','!=',\Auth::user()->id)->paginate(10);
            return \View::make('admin.users.users.view')->with('list_group', $list_group)->with('data', $data);
        }
    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('user_search_key');
       
        \Session::forget('user_status_key');
        \Session::forget('user_start_key');
        \Session::forget('user_end_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword = 'null';
            
        }
        \Session::set('user_search_key', $keyword);
        return \Redirect::action('\ADMIN\UserController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblUsersModel::orderBy('id', 'desc');
        $sql_data->where(function($query) use ($keyword) {
            $query->where('email', 'LIKE', '%' . $keyword . '%')
                                ->orWhere('address', 'LIKE', '%' . $keyword . '%')
                                ->orWhere('full_name', 'LIKE', '%' . $keyword . '%')
                                ->orWhere('phone', 'LIKE', '%' . $keyword . '%');
        });
        $data_lang = $sql_data->where('admin','1')->where('id','!=',\Auth::user()->id)->paginate(10);
        $list_group = \tblGroupAdminModel::get();
        return \View::make('admin.users.users.ajax')->with('data', $data_lang)->with('list_group', $list_group);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\UserController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = $sql_data = \tblUsersModel::orderBy('id', 'desc')->where('admin','1')->where('id','!=',\Auth::user()->id);
      
        if(\Session::has('user_status_key') && \Session::get('user_status_key')!='null'){
            $sql_data->where('status', \Session::get('user_status_key'));
        }
        if(\Session::has('user_start_key') && \Session::get('user_start_key')!='null'){
            $sql_data->where('created_at', '>=',\Session::get('user_start_key'));
        }
        if(\Session::has('user_end_key') && \Session::get('user_end_key')!='null'){
            $sql_data->where('created_at', '<=', \Session::get('user_end_key'));
        }
        if(\Session::has('user_search_key') && \Session::get('user_search_key')!='null'){
            $keyword = \Session::get('user_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('email', 'LIKE', '%' . $keyword . '%')
                                    ->orWhere('address', 'LIKE', '%' . $keyword . '%')
                                    ->orWhere('full_name', 'LIKE', '%' . $keyword . '%')
                                    ->orWhere('phone', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->paginate(10);
        $list_group = \tblGroupAdminModel::get();
        return \View::make('admin.users.users.ajax')->with('data', $data_lang)->with('list_group', $list_group);
    }

    public function postFilter(){
        $status = '';
        $start='';
        $end='';
        $cate='';
        \Session::forget('user_search_key');
       
        \Session::forget('user_status_key');
        \Session::forget('user_start_key');
        \Session::forget('user_end_key');
        
        if (\Input::has('user_start_date_filter') || \Input::get('user_start_date_filter') != '') {
            $start=\Input::get('user_start_date_filter');
        }else{
            $start='null';
        }
        if (\Input::has('user_end_date_filter') || \Input::get('user_end_date_filter') != '') {
            $end=\Input::get('user_end_date_filter');
        }else{
            $end='null';
        }
        if (\Input::has('user_filter_status') || \Input::get('user_filter_status') != '') {
            $status=\Input::get('user_filter_status');
        }else{
            $status='null';
        }
    
        \Session::set('user_status_key', $status);
        \Session::set('user_start_key', $start);
        \Session::set('user_end_key', $end);
        return \Redirect::action('\ADMIN\UserController@getFilter', array($status,$start,$end));
    }

    public function getFilter($status='',$start='',$end=''){
         $sql_data = \tblUsersModel::orderBy('id', 'desc');
     
        if($status!='null'){
            $sql_data->where('status', $status);
        }
        if($start!='null'){
            $sql_data->where('created_at', '>=', $start);
        }
        if($end!='null'){
            $sql_data->where('created_at', '<=', $end);
        }
        $data_lang = $sql_data->where('admin','1')->where('id','!=',\Auth::user()->id)->paginate(10);
        $list_group = \tblGroupAdminModel::get();
        return \View::make('admin.users.users.ajax')->with('data', $data_lang)->with('list_group', $list_group);
    }

    /* xem khach hang*/
    /* xem nhan vien */
    public function getAddCustomer() {
        
    }

    public function postAddCustomer() {
        
    }

    public function getEditCustomer($id) {
        
    }

    public function postEditCustomer(){
        

    }   
    public function getCustomer() {
      
    }

    public function postSearch1(){
       
    }

    public function getSearch1($keyword=''){
      
    }

    public function postShow1(){
       
    }

    public function getShow1($page=''){
       
    }

    public function postFilter1(){
        
    }

    public function getFilter1($status='',$start='',$end=''){
        
    }

    public function getGroup() {
        $list_role = \tblRolesModel::get();
        if (\Request::ajax()) {
            $data = \tblGroupAdminModel::paginate(10);
            return \View::make('admin.users.users.ajax_group')->with('list_role', $list_role)->with('data', $data);
        } else {
            $data = \tblGroupAdminModel::paginate(10);
            return \View::make('admin.users.users.view_group')->with('list_role', $list_role)->with('data', $data);
        }
    }

    public function getEditGroup($id) {
        $data = \tblGroupAdminModel::find($id);
        $all_role = \tblRolesModel::all();
        $selected=array();
        if(count($data)>0){
            if($data->roles_code!=''){
                $selected = explode(',',$data->roles_code);
            }
            
        }
        return \View::make('admin.users.users.edit_group')->with('all_role', $all_role)->with('data', $data)->with('selected', $selected);
    }

    public function postEditGroup() {
        $rules = array(
            "name" => "required",
        );
        $input_all = \Input::all();
        $validate = \Validator::make(\Input::all(), $rules, array(), \Lang::get('admin/attributes.tbl_group'));
        if (!$validate->fails()) {
            $tbl_user = \tblGroupAdminModel::find($input_all['id']);
            $tbl_user->name = $input_all['name'];
            $tbl_user->roles_code = implode(',', $input_all['role_admin']);
            $tbl_user->save();
            return \Redirect::action('\ADMIN\UserController@getGroup');
        } else {
            return \Redirect::back()->withErrors($validate->messages()->all('<li>:message</li>'));
        }
    }

    public function getAddGroup() {
        $all_role = \tblRolesModel::all();
        return \View::make('admin.users.users.add_group')->with('all_role', $all_role);
        
    }

    public function postAddGroup() {
        $rules = array(
            "name" => "required",
        );
        $input_all = \Input::all();
        $validate = \Validator::make(\Input::all(), $rules, array(), \Lang::get('admin/attributes.tbl_group'));
        if (!$validate->fails()) {
            $tbl_user = new \tblGroupAdminModel();
            $tbl_user->name = $input_all['name'];
            $tbl_user->roles_code = implode(',', $input_all['role_admin']);
            $tbl_user->save();
            return \Redirect::action('\ADMIN\UserController@getGroup');
        } else {
            return \Redirect::back()->withErrors($validate->messages()->all('<li>:message</li>'));
        }
    }

    public function postQuickOrderDetail() {
     
    }

    public function postQuickView() {
        $data = \tblOrderModel::where('user_id',\Input::get('id'))->get();
        return \View::make('admin.users.customer.quick_detail')->with('data', $data)->with('tabid', \Input::get('id'));
    }

    public function getAdd() {
        $all_group = \tblGroupAdminModel::all();
       
        
        
        if(\Auth::user()->root=='1'){
            $list_role=$all_group;            
        }else{
            $user = \tblUsersModel::find(\Auth::user()->id);
            /* lay list role */
            $role=array();
            if($user->group_admin_id!=''){
                foreach(explode(',',$user->group_admin_id) as $item){                    
                    $role[] = \tblGroupAdminModel::find($item);
                }
            }else{
                $role=array();
            }            
            $list_role = $role;
        }
        return \View::make('admin.users.users.add')->with('all_group', $list_role);
    }

    public function postAdd() {
        $rules = array(
            "email" => "required|email|unique:tbl_users",
            "full_name" => "required",
            "password" => "required",
            "phone" => "numeric",
        );
        $all_input = \Input::all();
        $validate = \Validator::make(\Input::all(), $rules, array(), \Lang::get('admin/attributes.tbl_users'));
        if (!$validate->fails()) {
            $tblUsersModel = new \tblUsersModel();
            $tblUsersModel->email = $all_input['email'];
            $tblUsersModel->full_name = isset($all_input['full_name']) ? $all_input['full_name'] : '';
            $tblUsersModel->password = isset($all_input['password']) ? \Hash::make($all_input['password']) : '';
            $tblUsersModel->phone = isset($all_input['phone']) ? $all_input['phone'] : '';
            $tblUsersModel->avatar = isset($all_input['imageselect']) ? $all_input['imageselect'] : '';
            $tblUsersModel->address = isset($all_input['address']) ? $all_input['address'] : '';
            if(\Input::has('role_admin')){
                $tblUsersModel->group_admin_id = implode(',',\Input::get('role_admin'));
            }
            $tblUsersModel->admin = 1;
            $tblUsersModel->root = 0;
            $tblUsersModel->status=1;
            $tblUsersModel->gender = isset($all_input['gender']) ? $all_input['gender'] : '';
            $tblUsersModel->save();
            return \Redirect::action('\ADMIN\UserController@getAdd');
        }else{
            return \Redirect::back()->withInput()->withErrors($validate->messages()->all('<li>:message</li>'));
        }

    }

    public function getEdit($id) {
        $tblUsersModel = \tblUsersModel::find($id);
        $all_group = \tblGroupAdminModel::all();
       
        if(\Auth::user()->root==1){
            $list_role=$all_group;
           
        }else{
            $user = \tblUsersModel::find(\Auth::user()->id);
            /* lay list role */
            $role=array();
            if($user->group_admin_id!=''){
                foreach(explode(',',$user->group_admin_id) as $item){                    
                    $role[] = \tblGroupAdminModel::find($item);
                }
            }else{
                $role=array();
            }            
            $list_role = $role;
            
        }
        return \View::make('admin.users.users.edit')->with('data', $tblUsersModel)->with('all_group', $list_role);
    }

    public function postEdit(){
        $all_input = \Input::all();
        $rules = array(
            "email" => "required|email|unique:tbl_users,email,".$all_input['id'],
            "full_name" => "required",
            "phone" => "numeric",
        );
        
        $validate = \Validator::make(\Input::all(), $rules, array(), \Lang::get('admin/attributes.tbl_users'));
        if (!$validate->fails()) {
            $email = $all_input['email'];
            $full_name = isset($all_input['full_name']) ? $all_input['full_name'] : '';
         
            $password = isset($all_input['password']) ? \Hash::make($all_input['password']) : '';
            $phone = isset($all_input['phone']) ? $all_input['phone'] : '';
            $avatar = isset($all_input['imageselect']) ? $all_input['imageselect'] : '';
            $address = isset($all_input['address']) ? $all_input['address'] : '';
            $gender = isset($all_input['gender']) ? $all_input['gender'] : '';
            $tblUsersModel = \tblUsersModel::find($all_input['id']);
            $tblUsersModel->email = $email;
            $tblUsersModel->full_name = $full_name;
            if($password!=''){
                $tblUsersModel->password = $password;
            }
            if(\Input::has('role_admin')){
                $tblUsersModel->group_admin_id = implode(',',\Input::get('role_admin'));
            }
            $tblUsersModel->phone = $phone;
            $tblUsersModel->avatar = $avatar;
            $tblUsersModel->address = $address;
            $tblUsersModel->gender = $gender;
            $tblUsersModel->save();
            return \Redirect::action('\ADMIN\UserController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors($validate->messages()->all('<li>:message</li>'));
        }

    }   

    public function postDeleteMulti() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $comment = \tblGroupAdminModel::whereIn('id', $all_input['list_id'])->delete();
            return $this->getView()->render();
        }
    }
    public function postDelete1() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblUsersModel::where('id', $all_input['id'])->update(array('status' => 2));
            return $this->getCustomer()->render();
        } else {
            return \App::abort(404);
        }
    }
    public function postDelete() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblUsersModel::where('id', $all_input['id'])->update(array('status' => 2));
            return $this->getView()->render();
        } else {
            return \App::abort(404);
        }
    }
  public function postDeleteGroup() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblGroupAdminModel::where('id',$all_input['id'])->delete();
            return $this->getView()->render();
        } else {
            return \App::abort(404);
        }
    }
	
	
	
	public function postQuickCustomer(){
		$all_input = \Input::all();
		$out_put = '';
		$html='';
		$rules = array(
            "email" => "required|email|unique:tbl_users",
			"password" => "required",
			"full_name" => "required",
        );
		$validate = \Validator::make(\Input::all(), $rules, array(), \Lang::get('admin/attributes.tbl_users'));
        if (!$validate->fails()) {
			$tblUsersModel = new \tblUsersModel();
            $tblUsersModel->email = $all_input['email'];
            $tblUsersModel->code = isset($all_input['code']) ? $all_input['code'] : '';
            $tblUsersModel->full_name = isset($all_input['full_name']) ? $all_input['full_name'] : '';
            $tblUsersModel->tax_number = isset($all_input['tax_number']) ? $all_input['tax_number'] : '';
            $tblUsersModel->password = isset($all_input['password']) ? \Hash::make($all_input['password']) : '';
            $tblUsersModel->phone = isset($all_input['phone']) ? $all_input['phone'] : '';
            $tblUsersModel->address = isset($all_input['address']) ? $all_input['address'] : '';
            $tblUsersModel->admin = 0;            
            $tblUsersModel->gender = isset($all_input['gender']) ? $all_input['gender'] : '';
            $tblUsersModel->status = 1;
            $tblUsersModel->save();
			$customer = \tblUsersModel::where('admin','0')->where('status',1)->orderBy('id', 'desc')->get();
			if(count($customer)>0){
				foreach($customer as $item){
					$html.="<option value='".$item->id."'>".$item->full_name."</option>";
				}
			}
			$out_put = array(
				'check' => 1,
				'value'=>$html
			);
			$return_output = json_encode($out_put);
		}else{
			$out_put = array(
				'check' => 0,
				'content' => $validate->messages()->all('<li>:message</li>')
			);
			$return_output = json_encode($out_put);
		}
		return $return_output;
	}
	
	
}
