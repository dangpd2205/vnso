<?php

namespace ADMIN;

class AuthorController extends \BaseController {
	private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý tác giả';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }


    public function getView() {
		
        \Session::forget('author_status_key');
        \Session::forget('author_search_key');
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $obj_news = new \tblAuthorModel();
            $obj_news->resolveConnection()->getPaginator()->setBaseUrl(\URL::action('\ADMIN\AuthorController@getView'));
            $data_lang = $obj_news->join('tbl_author_lang', 'tbl_author.id', '=', 'tbl_author_lang.author_id')->where('tbl_author_lang.lang_id', $this->d_lang)->orderBy('tbl_author.id', 'desc')->where('tbl_author_lang.status',1)->paginate(10);
            
            return \View::make('admin.author.ajax')->with('data', $data_lang);
        } else {
            $data  = \tblAuthorModel::join('tbl_author_lang', 'tbl_author.id', '=', 'tbl_author_lang.author_id')->where('tbl_author_lang.lang_id', $this->d_lang)->orderBy('tbl_author.id', 'desc')->where('tbl_author_lang.status',1)->paginate(10);
                
            return \View::make('admin.author.view')->with('data', $data);
        }

    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('author_status_key');
        \Session::forget('author_search_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword='null';
        }
        \Session::set('author_search_key', $keyword);
        return \Redirect::action('\ADMIN\AuthorController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblAuthorModel::join('tbl_author_lang', 'tbl_author.id', '=', 'tbl_author_lang.author_id')->where('tbl_author_lang.lang_id', $this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_author_lang.content', 'LIKE', '%' . $keyword . '%')->orWhere('tbl_author_lang.name', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_author.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.author.ajax')->with('data', $data_lang);
    }

    public function postFilter(){
        
        $status = '';
        \Session::forget('author_status_key');
        \Session::forget('author_search_key');
        
        if (\Input::has('news_status_filter') || \Input::get('news_status_filter') != '') {
            $status=\Input::get('news_status_filter');
        }else{
            $status='null';
        }
        \Session::set('author_status_key', $status);
        return \Redirect::action('\ADMIN\AuthorController@getFilter', array($status));
    }

    public function getFilter($status=''){
        $sql_data = \tblAuthorModel::join('tbl_author_lang', 'tbl_author.id', '=', 'tbl_author_lang.author_id')->where('tbl_author_lang.lang_id', $this->d_lang);
       
        if($status!='null'){
            $sql_data->where('tbl_author_lang.status', $status);
        }
        $sql_data->orderBy('tbl_author.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.author.ajax')->with('data', $data_lang);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\AuthorController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = \tblAuthorModel::join('tbl_author_lang', 'tbl_author.id', '=', 'tbl_author_lang.author_id')->where('tbl_author_lang.lang_id', $this->d_lang);
        
        if(\Session::has('author_status_key') && \Session::get('author_status_key')!='null'){
            $sql_data->where('tbl_author_lang.status', \Session::get('author_status_key'));
        }else{
            $sql_data->where('tbl_author_lang.status',1);
        }
        if(\Session::has('author_search_key') && \Session::get('author_search_key')!='null'){
            $keyword=\Session::get('author_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('tbl_author_lang.content', 'LIKE', '%' . $keyword . '%')->orWhere('tbl_author_lang.name', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->orderBy('tbl_author.id', 'desc')->paginate($page);
        return \View::make('admin.author.ajax')->with('data', $data_lang);
    }

    public function getAdd() {
        $question = \tblAuthorModel::leftJoin('tbl_author_lang','tbl_author.id','=','tbl_author_lang.author_id')->where('tbl_author_lang.lang_id',$this->d_lang)
            ->where('tbl_author_lang.status',1)->get();
        return \View::make('admin.author.add')->with('question',$question);
    }

    public function postAdd() {
        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();

        $log = [];
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){

            $tbl_news = new \tblAuthorModel();       
            $tbl_news->avatar = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            $tbl_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    
                    $tbl_detail_news = new \tblAuthorLangModel();
                    $tbl_detail_news->author_id = $tbl_news->id;
                    $tbl_detail_news->lang_id = $item_lang->id;
                     $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';
                    $tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? strip_tags($all_input['content_' . $item_lang->code]) : '';                    
                    $tbl_detail_news->status = 1;
                    $tbl_detail_news->save();

                }
            }
            return \Redirect::action('\ADMIN\AuthorController@getAdd');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }
    
    public function getEdit($lang, $id) {
        $data_news = \tblAuthorModel::join('tbl_author_lang', 'tbl_author.id', '=', 'tbl_author_lang.author_id')
                    ->leftJoin('tbl_lang', 'tbl_author_lang.lang_id', '=', 'tbl_lang.id')
                    ->where('tbl_author.id', $id)
                    ->select('tbl_lang.id as langid','tbl_author_lang.*','tbl_author.avatar')
                    ->get();
        $lang_code = \tblLangModel::where('id',$lang)->first();
        if (count($data_news) > 0) {           
            $question = \tblAuthorModel::leftJoin('tbl_author_lang','tbl_author.id','=','tbl_author_lang.author_id')->where('tbl_author_lang.lang_id',$this->d_lang)
            ->where('tbl_author_lang.status',1)->get();
            return \View::make('admin.author.edit')->with('question',$question)->with('lang_code', $lang_code)->with('data_news', $data_news);            
        } else {
            return \Redirect::action('\ADMIN\AuthorController@getView');
        }
    }

    public function postEdit() {

        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){
            $row_news = \tblAuthorModel::find($all_input['id_author']);
            
            $tbl_news->avatar = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            $row_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    $check = \tblAuthorLangModel::where('author_id', $all_input['id_author'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                    // neu co roi thi update
                    if(count($check)>0){                        
                        $tbl_detail_news = \tblAuthorLangModel::where('author_id', $all_input['id_author'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->first();		
                        $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';   			
                        $tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? strip_tags($all_input['content_' . $item_lang->code]) : '';	                      
                        $tbl_detail_news->save();
                    }else{
                       
                        $tbl_detail_news = new \tblAuthorLangModel();
                        $tbl_detail_news->author_id = $all_input['id_author'];
						$tbl_detail_news->lang_id = $item_lang->id; 
                        $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';
						$tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? strip_tags($all_input['content_' . $item_lang->code]) : '';                    
						$tbl_detail_news->status = 1;
                        $tbl_detail_news->save();
                    }
                }   
            }
    	
            return \Redirect::action('\ADMIN\AuthorController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }

    public function postDelete() {

        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblAuthorLangModel::where('author_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 2));           
            return $this->getView()->render();
        }
    }

    public function postPostConfirm() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
           \tblAuthorLangModel::where('author_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 1));
            return $this->getView()->render();
        }
    }


}
