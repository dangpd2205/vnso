<?php

namespace ADMIN;

use View;

class CommentController extends \BaseController {

    private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý comment';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
    }

    public function getView() {
        $data = \tblCommentModel::orderBy('created_at','desc')->where('status',0)->paginate(10);
        $arr_post=array();
        if(count($data)>0){
            foreach($data as $item){
                if($item->post_type=='4'){
                    $arr_post[] = \tblCommentModel::leftJoin('tbl_news', 'tbl_comment.post_id_lang', '=', 'tbl_news.id')->leftJoin('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')->leftJoin('tbl_lang', 'tbl_news_detail_lang.lang_id', '=', 'tbl_lang.id')->where('tbl_news_detail_lang.id',$item->post_id_lang)->select('tbl_news_detail_lang.*')->where('tbl_news_detail_lang.lang_id',$this->d_lang)->first();
                }else if($item->post_type=='5'){
                    $arr_post[] = \tblCommentModel::leftJoin('tbl_show', 'tbl_comment.post_id_lang', '=', 'tbl_show.id')->leftJoin('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')->leftJoin('tbl_lang', 'tbl_show_lang.lang_id', '=', 'tbl_lang.id')->where('tbl_show_lang.id',$item->post_id_lang)->select('tbl_show_lang.*')->where('tbl_show_lang.lang_id',$this->d_lang)->first();
                }
            }
        }
        if (\Request::ajax()) {
            return \View::make('admin.comment.ajax')->with('data', $data)->with('arr_post', $arr_post);
        } else {
            return \View::make('admin.comment.view')->with('data', $data)->with('arr_post', $arr_post);
        }
    }

    public function postPostConfirm() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblCommentModel::where('id', $all_input['id'])->update(array('status' => 1));
            return $this->getView()->render();
        }
    }


    public function postDelete() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblCommentModel::where('id', $all_input['id'])->delete();
            return $this->getView()->render();
        } else {
            return \App::abort(404);
        }
    }

    public function postDeleteMulti() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $comment = \tblCommentModel::whereIn('id', $all_input['list_id'])->delete();
            return $this->getView()->render();
        }
    }

   

}
