<?php

namespace ADMIN;

use View,
    Input,
    Redirect,
    Session,
    Auth,
    Password,
    Lang,
    Hash;

class HomeController extends \BaseController {

    private $titlepage = 'Pubweb.vn';

    public function __construct() {
        $this->titlepage = 'Đăng nhập';
        \View::share('titlepage', $this->titlepage);
    }
	
	public function getProfile(){
		if(Auth::check()){
			$detail = \tblUsersModel::find(Auth::user()->id);
			return View::make('admin.index.profile')->with('detail', $detail);
		}
	}
	
	public function postProfile(){
		$tblUsersModel = \tblUsersModel::find(\Input::get('id'));
		$tblUsersModel->full_name=\Input::get('full_name');
		if(\Input::get('password')!=''){
			$tblUsersModel->password=\Hash::make(\Input::get('password'));
		}
		$tblUsersModel->phone=\Input::get('phone');
		$tblUsersModel->address=\Input::get('address');
		$tblUsersModel->full_name=\Input::get('full_name');
		$tblUsersModel->save();
		if(\Input::get('password')!=''){
			return Redirect::action('\ADMIN\HomeController@getLogin');
		}else{
			return Redirect::action('\ADMIN\HomeController@getHomeAdmin');
		}
	}
	

    public function getIndex() {
        
    }

    public function getLock() {
        $data = [];
        if (Auth::check()) {
            \Session::set('user_lock', \Auth::user());
            $data = \Auth::user();
            \Auth::logout();
        } else {
            if (\Session::has('user_lock')) {
                $data = \Session::get('user_lock');
            }
        }
        if (\Session::has('user_lock')) {
            \Session::forget('url_back');
            \Session::push('url_back', \URL::previous());
            return View::make('admin.home.lock')->with('curent_user_lock', $data);
        } else {
            return Redirect::action('\ADMIN\HomeController@getLogin');
        }
    }

    public function postLock() {
        if (\Session::has('user_lock')) {
            $data_user = \Session::get('user_lock');

            if (\Auth::attempt(array('email' => $data_user->email, 'password' => Input::get('password'), 'status' => 1, 'admin' => 1), false)) {
                if (Session::has('url_back')) {
                    $url_back = Session::get('url_back');
                    return Redirect::to($url_back[0]);
                    Session::forget('url_back');
                } else {
                    return Redirect::action('\ADMIN\HomeController@getHomeAdmin');
                }
            } else {
                return Redirect::back()->withInput()->withErrors(Lang::get('admin/message.msg_login.error'));
            }
        } else {
            return Redirect::action('\ADMIN\HomeController@getLogin');
        }
    }

    public function getLogin() {
        if (Auth::check()) {
            return Redirect::action('\ADMIN\HomeController@getIndex');
        } else {
            return View::make('admin.home.login');
        }
    }

    public function getLogout() {
        Auth::logout();
        return Redirect::action('\ADMIN\HomeController@getLogin');
    }

    public function postLogin() {
        if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'), 'status' => 1, 'admin' => 1), false)) {
            if (Session::has('url_back')) {
                $url_back = Session::get('url_back');
                return Redirect::to($url_back[0]);
                Session::forget('url_back');
            } else {
                return Redirect::action('\ADMIN\ProductController@getView');
            }
        } else {
            return Redirect::back()->withInput()->withErrors(Lang::get('admin/message.msg_login.error'));
            ;
        }
    }

    public function getForGotPassword() {
        return View::make('admin.index.forgot_password');
    }

    public function postForGotPassword() {

        $response = @Password::remind(Input::only('email'), function($message) {
                    $message->subject('Thay đổi mật khẩu');
//                    $objAdmin = \Auth::user();
//                    $historyContent = Lang::get('backend/history.login.forgot') . ' ' . Input::only('email');
//                    $tblHistoryAdminModel = new \BackEnd\tblHistoryUserModel();
//                    $tblHistoryAdminModel->addHistory($objAdmin->id, $historyContent, '0');
                });
        switch ($response) {
            case Password::INVALID_USER:
                return Redirect::back()->withErrors(Lang::get('admin/message.msg_forgot_password.email_exist'));

            case Password::REMINDER_SENT:
                return Redirect::back()->withErrors(Lang::get('admin/message.msg_forgot_password.forgot_success'));
        }
    }

    public function getChangePassword($token = '') {
        return View::make('admin.index.change_password')->with('token', $token);
    }

    public function postChangePassword() {
        $credentials = Input::only(
                        'email', 'password', 'password_confirmation', 'token'
        );
        $response = Password::reset($credentials, function($user, $password) {
                    $user->password = Hash::make($password);
                    $user->save();
                });
        switch ($response) {
            case Password::INVALID_PASSWORD:
                return Redirect::back()->withErrors(Lang::get($response));
            case Password::INVALID_TOKEN:
                return Redirect::back()->withErrors(Lang::get($response));
            case Password::INVALID_USER:
                return Redirect::back()->withErrors(Lang::get($response));

            case Password::PASSWORD_RESET:
                return Redirect::action('\ADMIN\HomeController@getLogin')->withErrors(Lang::get($response));
        }
    }

    public function getHomeAdmin() {		
		$one=date('Y-m-d 00:00:00',time());
        $two=date('Y-m-d 59:59:59',time());
        $order = \tblOrderModel::select(\DB::raw("COUNT(id) as total"),\DB::raw("SUM(total_amount) as price"))->whereBetween('created_at',array($one,$two))->first();
		$list_date=[];
		$sum_doanhthu=[];
		$sum_user=[];
		for($i=6;$i>=0;$i--){
            $list_date[] = date('Y-m-d',strtotime('-'.$i.' days'));  
        }
		if(count($list_date)>0){
			foreach($list_date as $i_list_date){
				$doanhthu=0;
				$user=0;
				$tblOrderModel = \tblOrderModel::select(\DB::raw("SUM(total_amount) as price"))->whereBetween('created_at',array(date('Y-m-d 00:00:00', strtotime($i_list_date)),date('Y-m-d 23:59:59', strtotime($i_list_date))))->first();
				$tblUsersModel = \tblUsersModel::select(\DB::raw("COUNT(id) as total"))->whereBetween('created_at',array(date('Y-m-d 00:00:00', strtotime($i_list_date)),date('Y-m-d 23:59:59', strtotime($i_list_date))))->where('admin',0)->first();
				if(count($tblOrderModel)>0){
					$doanhthu+=$tblOrderModel->price;
				}
				if(count($tblUsersModel)>0){
					$user+=$tblUsersModel->total;
				}
				$sum_doanhthu[] = $doanhthu;
				$sum_user[] = $user;
			}
		}
        return View::make('admin.index.home')->with('order',$order)->with('sum_user',$sum_user)->with('sum_doanhthu',$sum_doanhthu)->with('list_date',$list_date);
    }

    public function getErrorAdmin() {
        return View::make('admin.index.404');
    }

	public function postVTC(){
		$destinationUrl="http://sandbox1.vtcebank.vn/pay.vtc.vn/cong-thanh-toan/checkout.html";
		//new version
		$plaintext = $_POST["txtWebsiteID"] . "-" . $_POST["txtCurency"] . "-" . $_POST["txtOrderID"] . "-" . $_POST["txtTotalAmount"] . "-" . $_POST["txtReceiveAccount"] . "-" . $_POST["txtParamExt"] . "-" . $_POST["txtSecret"]. "-" . $_POST["txtUrlReturn"];
		$sign = strtoupper(hash('sha256', $plaintext));
		$data = "?website_id=" . $_POST["txtWebsiteID"] . "&payment_method=" . $_POST["txtCurency"] . "&order_code=" . $_POST["txtOrderID"] . "&amount=" . $_POST["txtTotalAmount"] . "&receiver_acc=" .  $_POST["txtReceiveAccount"]. "&urlreturn=" .  urlencode($_POST["txtUrlReturn"]);
		$customer_first_name = htmlentities($_POST["txtCustomerFirstName"]);
		$customer_last_name = htmlentities($_POST["txtCustomerLastName"]);
		$bill_to_address_line1 = htmlentities($_POST["txtBillAddress1"]);
		$bill_to_address_line2 = htmlentities($_POST["txtBillAddress2"]);
		$city_name = htmlentities($_POST["txtCity"]);
		$address_country = htmlentities($_POST["txtCountry"]);
		$customer_email = htmlentities($_POST["txtCustomerEmail"]);
		$order_des = htmlentities($_POST["txtDescription"]);
		$data = $data . "&customer_first_name=" . $customer_first_name. "&customer_last_name=" . $customer_last_name. "&customer_mobile=" . $_POST["txtCustomerMobile"]. "&bill_to_address_line1=" . $bill_to_address_line1. "&bill_to_address_line2=" . $bill_to_address_line2. "&city_name=" . $city_name. "&address_country=" . $address_country. "&customer_email=" . $customer_email . "&order_des=" . $order_des . "&param_extend=" . $_POST["txtParamExt"] . "&sign=" . $sign;

		$destinationUrl = $destinationUrl . $data;
	}
	
}
