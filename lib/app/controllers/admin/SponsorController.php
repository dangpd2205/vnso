<?php

namespace ADMIN;

use View;

class SponsorController extends \BaseController {

    private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý sponsor';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
    }

    public function getView() {
        $data = \tblSponsorModel::orderBy('created_at','desc')->paginate(10);       
        if (\Request::ajax()) {
            return \View::make('admin.sponsor.ajax')->with('data', $data);
        } else {
            return \View::make('admin.sponsor.view')->with('data', $data);
        }
    }


    public function postDelete() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblSponsorModel::where('id', $all_input['id'])->delete();
            return $this->getView()->render();
        } else {
            return \App::abort(404);
        }
    }

    public function postDeleteMulti() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $comment = \tblSponsorModel::whereIn('id', $all_input['list_id'])->delete();
            return $this->getView()->render();
        }
    }

   

}
