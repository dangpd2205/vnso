<?php

namespace ADMIN;

class ShowController extends \BaseController {
    private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý chương trình';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }
	public function postQuickArtist() {
		$tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();
		$all_input = \Input::all();
        if (\Request::ajax()) {
			if(\Input::get('name_'.$tblLangModel->code) !=''){
				$tbl_artist = new \tblArtistModel();
				$tbl_artist->avatar = ($all_input['avatar'] != '') ? $all_input['avatar'] : '';
				$tbl_artist->type= isset($all_input['type']) ? $all_input['type'] : 0;
				$tbl_artist->save();
				foreach ($this->listLang as $item_lang) {   
					if ($all_input['name_' . $item_lang->code]) {
						$tbl_seo = new \tblSeoModel();
						$tbl_seo->title = '';
						$tbl_seo->description = '';
						$tbl_seo->keyword =  '';
						$tbl_seo->robots_index = 'index';
						$tbl_seo->robots_follow = 'Follow';
						$tbl_seo->robots_advanced = 'NONE';
						$tbl_seo->fb_title = '';
						$tbl_seo->fb_description =  '';
						$tbl_seo->fb_image = '';
						$tbl_seo->g_title = '';
						$tbl_seo->g_description =  '';
						$tbl_seo->g_image = '';
						$tbl_seo->save();
						$tbl_detail_news = new \tblArtistLangModel();
						$tbl_detail_news->artist_id = $tbl_artist->id;
						$tbl_detail_news->lang_id = $item_lang->id;
						$tbl_detail_news->seo_id = $tbl_seo->id;
						$tbl_detail_news->a_title = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';
						$tbl_detail_news->a_description = ($all_input['description_' . $item_lang->code] != '') ? strip_tags($all_input['description_' . $item_lang->code]) : '';
						$tbl_detail_news->a_content = ($all_input['content_' . $item_lang->code] != '') ? $all_input['content_' . $item_lang->code] : '';
						$tbl_detail_news->a_tags = '';
						$tbl_detail_news->a_instrument = ($all_input['instrument_' . $item_lang->code] != '') ? $all_input['instrument_' . $item_lang->code] : '';
						$tbl_detail_news->a_slug = \Tree::gen_slug($all_input['name_' . $item_lang->code]) . '-' . $tbl_artist->id;
						$tbl_detail_news->a_phone = '';
						$tbl_detail_news->a_email = '';
						$tbl_detail_news->status = 1;
						$tbl_detail_news->save();
					}
				}
				$out_put = array(
					'check' => 1,
					'content' => 'Thêm thành công'
				);
			}else{
				$out_put = array(
					'check' => 0,
					'content' => 'Bạn chưa nhập tiêu đề '.$tblLangModel->name
				);
			}
            echo json_encode($out_put);
        }
    }
    public function postQuickCategory() {
        if (\Request::ajax()) {
            foreach ($this->listLang as $item) {
                $name = \Input::get('product_add_cat_name_' . $item->code);
                $parent = \Input::get('product_add_cat_parent_' . $item->code);
                $des = \Input::get('product_add_cat_description_' . $item->code);
                $tblShowCategoryModel = new \tblShowCategoryModel();
                $tblShowCategoryModel->name = $name;
                $tblShowCategoryModel->parent = $parent;
                $tblShowCategoryModel->lang_id = $item->id;
                $tblShowCategoryModel->description = $des;
                $tblShowCategoryModel->slug = \Tree::gen_slug($name);
                $tblShowCategoryModel->save();
            }
            $out_put = [];
            foreach ($this->listLang as $item1) {
                $array = \tblShowCategoryModel::where('lang_id', $item1->id)->orderBy('tbl_show_category.name')->get();
                $out_put[] = array(
                    'check' => $item1->code,
                    'content' => \Tree::treereturn($array, [], $item1->id)
                );
            }
            echo json_encode($out_put);
        }
    }
    public function getCategoryView() {
        $all_category = \tblShowCategoryModel::orderBy('position','asc')->get();
        $this->titlepage = 'Quản lý nhóm tin tức';
        $tblShowCategoryModel = \tblShowCategoryModel::orderBy('position','asc')->get();
        return \View::make('admin.show.category.add_category')->with('data', $tblShowCategoryModel)->with('all_category', $all_category);
    }
    
    public function getAddCate(){
        $all_category = \tblShowCategoryModel::orderBy('name')->get();
        return \View::make('admin.show.category.add_cate')->with('all_category', $all_category);
    }
    
    public function postAddCate(){
        $all_input = \Input::all();
        foreach ($this->listLang as $item) {
            if(\Input::get('cate_name_' . $item->code)!=''){
                
                $tbl_seo = new \tblSeoModel();
                $tbl_seo->title = isset($all_input['seo_title_' . $item->code]) ? $all_input['seo_title_' . $item->code] : '';
                $tbl_seo->description = isset($all_input['seo_description_' . $item->code]) ? $all_input['seo_description_' . $item->code] : '';
                $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item->code]) ? $all_input['seo_keyword_' . $item->code] : '';
                $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'None';
                $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item->code]) ? $all_input['seo_f_title_' . $item->code] : '';
                $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item->code]) ? $all_input['seo_f_description_' . $item->code] : '';
                $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item->code]) ? $all_input['seo_f_image_' . $item->code] : '';
                $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item->code]) ? $all_input['seo_g_title_' . $item->code] : '';
                $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item->code]) ? $all_input['seo_g_description_' . $item->code] : '';
                $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item->code]) ? $all_input['seo_g_image_' . $item->code] : '';
                $tbl_seo->save();
                $name = strip_tags(\Input::get('cate_name_' . $item->code));
                $parent = \Input::get('news_add_cat_parent_' . $item->code);
                $des = strip_tags(\Input::get('news_add_cat_description_' . $item->code));
                $tblCateNewsModel = new \tblShowCategoryModel();
                $tblCateNewsModel->name = $name;
                $tblCateNewsModel->description = $des;
                $tblCateNewsModel->parent = $parent;
                $tblCateNewsModel->lang_id = $item->id;
                $tblCateNewsModel->seo_id = $tbl_seo->id;   
                $tblCateNewsModel->avatar = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';                       $tblCateNewsModel->background = ($all_input['image_hidden1'] != '') ? $all_input['image_hidden1'] : '';
                $check = \tblShowCategoryModel::where('slug',\Tree::gen_slug($name))->first();
				if(count($check)>0){
					$maxid = \tblShowCategoryModel::count();
					$tblCateNewsModel->slug = \Tree::gen_slug($name).($maxid+1);
				}else{
					$tblCateNewsModel->slug = \Tree::gen_slug($name);
				}	           
                $tblCateNewsModel->save();
            }
        }
        \Session::flash('alert_success', \Lang::get('admin/message.msg_add.success'));
        return \Redirect::action('\ADMIN\ShowController@getAddCate');
    }
    
    public function getEditCate($id){
        $dataedit = \tblShowCategoryModel::find($id);
        $seo = \tblSeoModel::find($dataedit->seo_id);
        $all_category = \tblShowCategoryModel::orderBy('name')->get();
        return \View::make('admin.show.category.edit_cate')->with('dataedit', $dataedit)->with('seo', $seo)->with('all_category', $all_category);
    }
    
    public function postEditCate(){
        $all_input = \Input::all();
        if(\Input::get('cate_name')!=''){           
            $maxid = \tblShowCategoryModel::select(\DB::raw("max(id) as maxid"))->first();
            $check = \tblShowCategoryModel::where('slug',\Tree::gen_slug(\Input::get('cate_name')))->where('id','!=',\Input::get('id_cate'))->first();
            $tblCateNewsModel = \tblShowCategoryModel::find(\Input::get('id_cate'));
            $tblCateNewsModel->name = \Input::get('cate_name');
            $tblCateNewsModel->description = \Input::get('cate_des');
            if(count($check)>0){
                $tblCateNewsModel->slug = \Tree::gen_slug(\Input::get('cate_name')).$maxid->maxid;
            }else{
                $tblCateNewsModel->slug = \Tree::gen_slug(\Input::get('cate_name'));
            }
            $tblCateNewsModel->avatar = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            $tblCateNewsModel->background = ($all_input['image_hidden1'] != '') ? $all_input['image_hidden1'] : '';
            $tblCateNewsModel->save();
            $tblSeoModel = \tblSeoModel::find(\Input::get('seo_id'));
            if(count($tblSeoModel)>0){
                $tblSeoModel->title = isset($all_input['seo_title']) ? $all_input['seo_title'] : '';
                $tblSeoModel->description = isset($all_input['seo_description']) ? $all_input['seo_description'] : '';
                $tblSeoModel->keyword = isset($all_input['seo_keyword']) ? $all_input['seo_keyword'] : '';
                $tblSeoModel->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                $tblSeoModel->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                $tblSeoModel->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'None';
                $tblSeoModel->fb_title=\Input::get('seo_f_title');
                $tblSeoModel->fb_description=\Input::get('seo_f_description');
                $tblSeoModel->fb_image=\Input::get('seo_f_image');
                $tblSeoModel->g_title=\Input::get('seo_g_title');
                $tblSeoModel->g_description=\Input::get('seo_g_description');
                $tblSeoModel->g_image=\Input::get('seo_g_image');
                $tblSeoModel->save();
            }else{
                $tbl_seo = new \tblSeoModel();
                $tbl_seo->title = isset($all_input['seo_title']) ? $all_input['seo_title'] : '';
                $tbl_seo->description = isset($all_input['seo_description_']) ? $all_input['seo_description'] : '';
                $tbl_seo->keyword = isset($all_input['seo_keyword_']) ? $all_input['seo_keyword'] : '';
                $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'None';
                $tbl_seo->fb_title = isset($all_input['seo_f_title']) ? $all_input['seo_f_title'] : '';
                $tbl_seo->fb_description = isset($all_input['seo_f_description']) ? $all_input['seo_f_description'] : '';
                $tbl_seo->fb_image = isset($all_input['seo_f_image']) ? $all_input['seo_f_image'] : '';
                $tbl_seo->g_title = isset($all_input['seo_g_title']) ? $all_input['seo_g_title'] : '';
                $tbl_seo->g_description = isset($all_input['seo_g_description']) ? $all_input['seo_g_description'] : '';
                $tbl_seo->g_image = isset($all_input['seo_g_image']) ? $all_input['seo_g_image'] : '';
                $tbl_seo->save();
                $tblCateNewsModel = \tblShowCategoryModel::find(\Input::get('id_cate'));
                $tblCateNewsModel->seo_id = $tbl_seo->id;
                $tblCateNewsModel->save();
            }
        }
        \Session::flash('alert_success', \Lang::get('admin/message.msg_update.success'));
        return \Redirect::action('\ADMIN\ShowController@getCategoryView');
    }

    public function postCategoryAdd() {
        if (\Request::ajax()) {
            $product_cate_name = "";

            foreach ($this->listLang as $item) {
                $product_cate_name = \Input::get('product_add_cat_name_' . $item->code);
                $count = \tblShowCategoryModel::where('name', 'LIKE', $product_cate_name . '%')->count();
                if ($count > 0) {
                    $product_cate_name = trim($product_cate_name . ' ' . $count);
                }
                $tblShowCategoryModel = new \tblShowCategoryModel();
                $tblShowCategoryModel->name = \Input::get('product_add_cat_name_' . $item->code);
                $tblShowCategoryModel->slug = \Tree::gen_slug($product_cate_name);
                $tblShowCategoryModel->parent = \Input::get('product_add_cat_parent_' . $item->code);
                $tblShowCategoryModel->description = \Input::get('product_add_cat_description_' . $item->code);
                $tblShowCategoryModel->lang_id = $item->id;
                if ($tblShowCategoryModel->name != "") {
                    $tblShowCategoryModel->save();
                }
            }

            if ($product_cate_name != "") {
                $out_put = array(
                    'check' => 'reload',
                    'content' => ''
                );
            } else {
                $out_put = array(
                    'check' => '0',
                    'content' => '<li>Trường tên nhóm  không được để trống!</li>'
                );
            }
            echo json_encode($out_put);
        }
    }

    public function postCategoryEdit() {
        if (\Request::ajax()) {
            $rules = array(
                "product_add_cat_name" => "required"
            );
            $validate = \Validator::make(\Input::all(), $rules, array(), \Lang::get('admin/attributes.tbl_category'));
            if (!$validate->fails()) {
                $name = \Input::get('product_add_cat_name');
                $tblShowCategoryModel = \tblShowCategoryModel::where('id', '=', \Input::get('id'))->first();
                $count = \tblShowCategoryModel::where('name', 'LIKE', $name . '%')->count();
                if ($count > 0) {
                    $name = trim($name. ' ' . $count);
                }
                $tblShowCategoryModel->name = \Input::get('product_add_cat_name');
                if($tblShowCategoryModel->name != \Input::get('product_add_cat_name')){
                        $tblShowCategoryModel->slug = \Tree::gen_slug($name);
                }
                $tblShowCategoryModel->description = \Input::get('product_add_cat_description');
                $tblShowCategoryModel->save();

                $out_put = array(
                    'check' => 'reload',
                    'content' => ''
                );
            } else {
                $out_put = array(
                    'check' => '0',
                    'content' => $validate->messages()->all('<li>:message</li>')
                );
            }
            echo json_encode($out_put);
        }
    }

    public function postDeleteCategory() {
        if (\Request::ajax()) {
            $product_cate_con = \tblShowCategoryModel::where('parent', '=', \Input::get('id'))->get();
            foreach ($product_cate_con as $item_delete) {
                $product_cate_con_con = \tblShowCategoryModel::where('parent', '=', $item_delete->id)->get();
                foreach ($product_cate_con_con as $item_delete_con) {
                    \tblShowCategoryModel::where('id', '=', $item_delete_con->id)->delete();
                }
                \tblShowCategoryModel::where('id', '=', $item_delete->id)->delete();
            }
            \tblShowCategoryModel::where('id', '=', \Input::get('id'))->delete();
        }
    }

    public function postUpdatePosCategory() {
        $data = json_decode(\Input::get('data'));
        $count = 1;
        foreach ($data as $item) {
            $cate = \tblShowCategoryModel::find($item->id);
            $cate->parent=0;
            $cate->position=$count;
            $cate->save();
            
            if (isset($item->children)) {
                $data1 = $item->children;
                $count1 = 1;
                foreach ($data1 as $item1) {
                    $cate = \tblShowCategoryModel::find($item1->id);
                    $cate->parent=$item->id;
                    $cate->position=$count1;
                    $cate->save();
                    if (isset($item1->children)) {
                        $data2 = $item1->children;
                        $count2 = 1;
                        foreach ($data2 as $item2) {
                            $cate = \tblShowCategoryModel::find($item2->id);
                            $cate->parent=$item1->id;
                            $cate->position=$count2;
                            $cate->save();
                            $count2++;
                        }
                    }
                    $count1++;
                }
            }
            $count++;
        }
    }
    public function getView() {

        \Session::forget('show_cate_key');
        \Session::forget('show_status_key');
        \Session::forget('show_search_key');
        \Session::forget('show_start_key');
        \Session::forget('show_end_key');
        \Session::forget('show_place_key');
        $list_category = \tblShowCategoryModel::where('lang_id',$this->d_lang)->orderBy('name')->get();
        $place  = \tblPlaceModel::join('tbl_place_lang', 'tbl_place.id', '=', 'tbl_place_lang.place_id')->where('tbl_place_lang.lang_id', $this->d_lang)->orderBy('tbl_place.id', 'desc')->where('tbl_place_lang.status',1)->get();
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $obj_news = new \tblShowModel();
            $obj_news->resolveConnection()->getPaginator()->setBaseUrl(\URL::action('\ADMIN\ShowController@getView'));
            $data_lang = $obj_news->join('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')->where('tbl_show_lang.lang_id', $this->d_lang)->orderBy('tbl_show.time_show', 'asc')->where('tbl_show_lang.status',1)->paginate(10);
            if(count($data_lang)>0){
                foreach($data_lang as $item){
                    if($item->artist_id!=''){
                        $list_art[] = \tblArtistLangModel::whereIn('artist_id',explode(',',$item->artist_id))->where('lang_id',$this->d_lang)->where('status',1)->get();
                    }else{
                        $list_art[]=[];
                    }
                }
            }else{
                $list_art=[];
            }

            return \View::make('admin.show.ajax')->with('place', $place)->with('list_category', $list_category)->with('data', $data_lang)->with('list_art', $list_art);
        } else {
            $data  = \tblShowModel::join('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')->where('tbl_show_lang.lang_id', $this->d_lang)->orderBy('tbl_show.time_show', 'asc')->where('tbl_show_lang.status',1)->paginate(10);
            if(count($data)>0){
                foreach($data as $item){
                    if($item->artist_id!=''){
                        $list_art[] = \tblArtistLangModel::whereIn('artist_id',explode(',',$item->artist_id))->where('lang_id',$this->d_lang)->where('status',1)->get();
                    }else{
                        $list_art[]=[];
                    }
                }
            }else{
                $list_art=[];
            }
            return \View::make('admin.show.view')->with('place', $place)->with('list_category', $list_category)->with('data', $data)->with('list_art', $list_art);
        }

    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('show_cate_key');
        \Session::forget('show_status_key');
        \Session::forget('show_search_key');
        \Session::forget('show_start_key');
        \Session::forget('show_end_key');
        \Session::forget('show_place_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword='null';
        }
        \Session::set('show_search_key', $keyword);
        return \Redirect::action('\ADMIN\ShowController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblShowModel::join('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')->where('tbl_show_lang.lang_id', $this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_show_lang.name', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_show.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        if(count($data_lang)>0){
            foreach($data_lang as $item){
                if($item->artist_id!=''){
                    $list_art[] = \tblArtistLangModel::whereIn('artist_id',explode(',',$item->artist_id))->where('lang_id',$this->d_lang)->where('status',1)->get();
                }else{
                    $list_art[]=[];
                }
            }
        }else{
            $list_art=[];
        }

        return \View::make('admin.show.ajax')->with('data', $data_lang)->with('list_art', $list_art);
    }

    public function postFilter(){
        
        $status = '';
        \Session::forget('show_cate_key');
        \Session::forget('show_status_key');
        \Session::forget('show_search_key');
        \Session::forget('show_start_key');
        \Session::forget('show_end_key');
        \Session::forget('show_place_key');
        if (\Input::has('start_date_filter') || \Input::get('start_date_filter') != '') {
            $start=\Input::get('start_date_filter');
        }else{
            $start='null';
        }
        if (\Input::has('end_date_filter') || \Input::get('end_date_filter') != '') {
            $end=\Input::get('end_date_filter');
        }else{
            $end='null';
        }
        if (\Input::has('news_cat_filter') || \Input::get('news_cat_filter') != '') {
            $cate=\Input::get('news_cat_filter');
        }else{
            $cate='null';
        }
        if (\Input::has('news_status_filter') || \Input::get('news_status_filter') != '') {
            $status=\Input::get('news_status_filter');
        }else{
            $status='null';
        }
        if (\Input::has('news_place_filter') || \Input::get('news_place_filter') != '') {
            $place=\Input::get('news_place_filter');
        }else{
            $place='null';
        }
        \Session::set('show_status_key', $status);
        \Session::set('show_cate_key', $cate);
        \Session::set('show_start_key',$start);
        \Session::set('show_end_key',$end);
        \Session::set('show_place_key',$place);
        return \Redirect::action('\ADMIN\ShowController@getFilter', array($cate,$status,$start,$end,$place));
    }

    public function getFilter($cate='',$status='',$start='',$end='',$place=''){
        $sql_data = \tblShowModel::join('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')->where('tbl_show_lang.lang_id', $this->d_lang);
        if($cate!='null'){
            $cat_r = \tblShowCategoryModel::find($cate);
            $list_cate = [];
            if ($cat_r->parent == 0) {
                $list_cate[] = $cat_r->id;
                $cat_r_c = \tblShowCategoryModel::where('parent', $cat_r->id)->get();
                foreach ($cat_r_c as $item) {
                    $list_cate[] = $item->id;
                }
            } else {
                $list_cate[] = $cat_r->id;
            }
            $sql_data->whereIn('tbl_show.id', \tblShowViewsModel::select('show_id')->whereIn('cat_id', $list_cate)->get()->toArray());
        }
        if($start!='null'){
            $sql_data->where('tbl_show.time_show', '>=',strtotime($start));
        }
        if($end!='null'){
            $sql_data->where('tbl_show.time_show', '<=',strtotime($end));
        }
        if($status!='null'){
            $sql_data->where('tbl_show_lang.status', $status);
        }
        if($place!='null'){
            $sql_data->where('tbl_show.place_id', $place);
        }
        $sql_data->orderBy('tbl_show.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        if(count($data_lang)>0){
            foreach($data_lang as $item){
                if($item->artist_id!=''){
                    $list_art[] = \tblArtistLangModel::whereIn('artist_id',explode(',',$item->artist_id))->where('lang_id',$this->d_lang)->where('status',1)->get();
                }else{
                    $list_art[]=[];
                }
            }
        }else{
            $list_art=[];
        }
        return \View::make('admin.show.ajax')->with('data', $data_lang)->with('list_art', $list_art);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\ShowController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = \tblShowModel::join('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')->where('tbl_show_lang.lang_id', $this->d_lang);
        if(\Session::has('show_cate_key') && \Session::get('show_cate_key')!='null'){
            $cat_r = \tblShowCategoryModel::find(\Session::get('show_cate_key'));
            $list_cate = [];
            if ($cat_r->parent == 0) {
                $list_cate[] = $cat_r->id;
                $cat_r_c = \tblShowCategoryModel::where('parent', $cat_r->id)->get();
                foreach ($cat_r_c as $item) {
                    $list_cate[] = $item->id;
                }
            } else {
                $list_cate[] = $cat_r->id;
            }
            $sql_data->whereIn('tbl_show.id', \tblShowViewsModel::select('show_id')->whereIn('cat_id', $list_cate)->get()->toArray());
        }
        if(\Session::has('show_start_key') && \Session::get('show_start_key')!='null'){
            $sql_data->where('tbl_show.time_show', '>=',strtotime(\Session::get('show_start_key')));
        }
        if(\Session::has('show_end_key') && \Session::get('show_end_key')!='null'){
            $sql_data->where('tbl_show.time_show', '<=',strtotime(\Session::get('show_end_key')));
        }
        if(\Session::has('show_status_key') && \Session::get('show_status_key')!='null'){
            $sql_data->where('tbl_show_lang.status', \Session::get('show_status_key'));
        }else{
            $sql_data->where('tbl_show_lang.status',1);
        }
         if(\Session::has('show_place_key') && \Session::get('show_place_key')!='null'){
            $sql_data->where('tbl_show.place_id', \Session::get('show_place_key'));
        }
        if(\Session::has('show_search_key') && \Session::get('show_search_key')!='null'){
            $keyword=\Session::get('show_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('tbl_show_lang.name', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->orderBy('tbl_show.id', 'desc')->paginate($page);
        if(count($data_lang)>0){
            foreach($data_lang as $item){
                if($item->artist_id!=''){
                    $list_art[] = \tblArtistLangModel::whereIn('artist_id',explode(',',$item->artist_id))->where('lang_id',$this->d_lang)->where('status',1)->get();
                }else{
                    $list_art[]=[];
                }
            }
        }else{
            $list_art=[];
        }
        return \View::make('admin.show.ajax')->with('data', $data_lang)->with('list_art', $list_art);
    }

    public function getAdd() {
        $place = \tblPlaceModel::leftJoin('tbl_place_lang', 'tbl_place.id', '=', 'tbl_place_lang.place_id')->where('tbl_place_lang.lang_id',$this->d_lang)
            ->where('tbl_place_lang.status',1)->get();
        $ticket = \tblTicketModel::leftJoin('tbl_ticket_lang', 'tbl_ticket.id', '=', 'tbl_ticket_lang.ticket_id')->where('tbl_ticket_lang.lang_id',$this->d_lang)
            ->where('tbl_ticket_lang.status',1)->orderBy('tbl_ticket_lang.created_at','desc')->get();
        $all_category = \tblShowCategoryModel::orderBy('position','asc')->get();
        return \View::make('admin.show.add')->with('place',$place)->with('ticket',$ticket)->with('all_category', $all_category);
    }
    
    public function postAdd() {       
   
        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();

        $log = [];
        $all_input = \Input::all();
        
        if(\Input::get('name_'.$tblLangModel->code)){

            $tbl_news = new \tblShowModel();       
            $tbl_news->avatar = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            $tbl_news->conductor_id = ($all_input['list_id_autokeyword3'] != '') ? $all_input['list_id_autokeyword3'] : '';
            $tbl_news->artist_id = ($all_input['list_id_autokeyword'] != '') ? $all_input['list_id_autokeyword'] : '';
            $tbl_news->sheet_id = ($all_input['list_id_autokeyword1'] != '') ? $all_input['list_id_autokeyword1'] : '';
            $tbl_news->sponsor_id = ($all_input['list_id_autokeyword2'] != '') ? $all_input['list_id_autokeyword2'] : '';
            $tbl_news->period = ($all_input['period'] != '') ? $all_input['period'] : '';
            $tbl_news->place_id = ($all_input['place_id'] != '') ? $all_input['place_id'] : '';
			if(\Input::has('is_sell')){
				$tbl_news->is_sell=1;
			}else{
				$tbl_news->is_sell=0;
			}
			if(\Input::has('sell_status')){
				$tbl_news->sell_status=1;
			}else{
				$tbl_news->sell_status=0;
			}
            $tbl_news->time_show = ($all_input['news_time'] != '') ?  strtotime(\Datetime::createFromFormat('d M Y - H:i', $all_input['news_time'])->format('Y-m-d H:i')) : time();
            $tbl_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    $tbl_seo = new \tblSeoModel();
                    $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                    $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                    $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                    $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                    $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                    $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                    $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                    $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                    $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                    $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                    $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                    $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                    $tbl_seo->save();
                    $tbl_detail_news = new \tblShowLangModel();
                    $tbl_detail_news->show_id = $tbl_news->id;
                    $tbl_detail_news->lang_id = $item_lang->id;
                    $tbl_detail_news->seo_id = $tbl_seo->id;
                     $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';
                     $tbl_detail_news->description = ($all_input['description_' . $item_lang->code] != '') ? strip_tags($all_input['description_' . $item_lang->code]) : ''; 
					 $tbl_detail_news->artist_content = ($all_input['artist_content_' . $item_lang->code] != '') ? ($all_input['artist_content_' . $item_lang->code]) : ''; 
                    $tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? $all_input['content_' . $item_lang->code] : '';     
					$tbl_detail_news->place_text = ($all_input['place_text_' . $item_lang->code] != '') ? ($all_input['place_text_' . $item_lang->code]) : ''; 
                    $tbl_detail_news->slug = \Tree::gen_slug($all_input['name_' . $item_lang->code]) . '-' . $tbl_news->id;              
                    $tbl_detail_news->tags = ($all_input['tags_' . $item_lang->code] != '') ? $all_input['tags_' . $item_lang->code] : ''; 
                    $tbl_detail_news->status = 1;
                    $tbl_detail_news->save();
                    /* bang tag */
                    if(\Input::get('tags_' . $item_lang->code)!=''){
                        foreach(explode(',',\Input::get('tags_' . $item_lang->code)) as $i_tag){
                            $check = \tblTagsModel::where('slug', \Tree::gen_slug($i_tag))->first();
                            if(count($check)<=0){
                                $tblTagModel = new \tblTagsModel();
                                $tblTagModel->name = $i_tag;
                                $tblTagModel->slug = \Tree::gen_slug($i_tag);
                                $tblTagModel->save();
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $tblTagModel->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 4;
                                $tblTagViewModel->save();
                            }else{
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $check->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 4;
                                $tblTagViewModel->save();
                            }
                        }
                    }
                    $cat_list = isset($all_input['cat_id_' . $item_lang->id]) ? $all_input['cat_id_' . $item_lang->id] : '';
                    if (count($cat_list) > 0 && $cat_list != '') {
                        foreach ($cat_list as $item) {
                            $tbl_news_view = new \tblShowViewsModel();
                            $tbl_news_view->show_id = $tbl_news->id;
                            $tbl_news_view->cat_id = $item;
                            $tbl_news_view->lang_id = $item_lang->id;
                            $tbl_news_view->save();
                        }
                    }
                }
            }

            /* nhap show view */
            if($all_input['place_id']==1){
                $place_mapb = \tblPlaceMapModel::where('place_id',1)->where('ticket_id',1)->get(); 
                $this->insertTicketShow($place_mapb,1,str_replace(',', '',$all_input['ticket_1']),$tbl_news->id,'seat-b');            
                $place_mapa = \tblPlaceMapModel::where('place_id',1)->where('ticket_id',2)->get();             
                $this->insertTicketShow($place_mapa,2,str_replace(',', '',$all_input['ticket_2']),$tbl_news->id,'seat-a');
                $place_maps = \tblPlaceMapModel::where('place_id',1)->where('ticket_id',3)->get();             
                $this->insertTicketShow($place_maps,3,str_replace(',', '',$all_input['ticket_3']),$tbl_news->id,'seat-s');
				/* khoa ghế mặc định */
				$default_lock = ['T2B18','E01','E02','E03','E04','E05','E06','E07','E08','E09','E10','E11','E12','E13','E14','E15','E16','E17','E18','E19','E20','E21','E22','L2LA01','L2LA02','L2LA03','L2LA04','L2LA05','L2LA06','L2L01','L2L02','L2L03','L2L04','L2L05','L2L06','L2L07','L2L08','L2LB01','L2LB02','L2LB03','L2LB04','L2LB05','L2LB06','L2M01','L2M02','L2M03','L2M04','L2N01','L2N02','L2N03','L2N04','L2J01','L2J02','L2J03','L2J04','L2I01','L2I02','L2O01','L2O02','T2B13','T2B15','T2B17','T2B16','T2B14','T2B08','T2B02','T2B04','T2B06','A31','A33','A35','A37','A39','A41','A43','A45','A47','A49','A32','A34','A36','A38','A40','A42','A44','A46','A48','A01','A02','A03','A04','A05','A06','A07','A08','A09','A10','A11','A12','A13','A14','A15','A16','A17','A18','A19','A20','B02','B04','B06','B08','B10','B12','B14','B16','B18','B20','C02','C04','C06','C08','C10','C12','C14','C16','C18','C20','D02','D04','D06','D08','D10','D12','D14','D16','D18','D20','D22','D13','D15','D17','D19','D21','D23','F09','F11','F13','F15','F17','F19','F21','F23','F01','F03','F05','F07','F20','F22','H09','H11','H13','H15','H17','H19','H21','H23','K01','K02','K03','K04','K05','K06','K07','K08','K09','K10','K11','K12','K13','K14','K15','K16','K17','K18','L01','L02','L03','L04','L05','L06','L07','L08','L09','L10','L11','L12','L13','L14','L15','L16','L17','G09','G11','G13','G15','G17','G19','G21','G22','G01','G03','G05','G07','L2K01','L2K02','L2K03','L2K04'];
				$lock_2=['L2B01','L2B02','L2C01','L2C02','L2D01','L2D02','L2E01','L2E02','L2F01','L2F02','L1I01','L1I02','L1I03','L1I04','L1I05','L1I06','L1K01','L1K02','L1K03','L1K04','L1K05','L1K06','L1D01','L1D02','L1D03','L1D04','L1D05','L1D06','L1D07','L1D08','L1E01','L1E03','L1E05','L1E07','L1E09','L1E11','L1E13','L1E15','L1E17','L1E19','L1E21','L1E23','L1E25','L1E27','C26','C28','C30','C32','C34','C36','C17','C19','C21','C23','C25','C27','C29','C31','C33','C35','L2H02','L2H01','L2G02','L2G01','L2P02','L2P01','L2Q02','L2Q01','L1G01','L1G02','L1G03','L1G04','L1G05','L1G06','L1G07','L1G08','L1H01','L1H02','L1H03','L1H04','L1H05','L1H06','L1I01','L1I02','L1I03','L1I04','L1I05','L1I06','L1K01','L1K02','L1K03','L1K04','L1K05','L1K06'];
				$lock_1=['A01','A03','A05','A07','A09','A11','A13','A15','A14','A12','A10','A08','A06','A04','A02','E01','E02','E03','E04','E05','E06','E07','E08','E09','E10','E11','E12','E13','E14','E15','E16','E17','E18','E19','E20','E21','E22','E23','E24','D01','D02','D03','D04','D05','D06','D07','D08','D09','D10','D11','D12','D13','D14','D15','D16','D17','D18','D19','D20','D21','D22','D23','D24','D25','D26'];
					\DB::table('tbl_ticket_show')->where('ticket_id',3)->whereIn('seat_name_orin',$default_lock)->where('show_id',$tbl_news->id)->update(['status'=>4]); /* s */ 
				\DB::table('tbl_ticket_show')->where('ticket_id',2)->whereIn('seat_name_orin',$lock_2)->where('show_id',$tbl_news->id)->update(['status'=>4]); /* a */
				\DB::table('tbl_ticket_show')->where('ticket_id',1)->whereIn('seat_name_orin',$lock_1)->where('show_id',$tbl_news->id)->update(['status'=>4]);
            }else if($all_input['place_id']==2){
                $place_mapb = \tblPlaceMapModel::where('place_id',2)->where('ticket_id',1)->get();
                $this->insertTicketShow($place_mapb,1,str_replace(',', '',$all_input['ticket_1']),$tbl_news->id,'seat-b');             
                $place_mapa = \tblPlaceMapModel::where('place_id',2)->where('ticket_id',2)->get();             
                $this->insertTicketShow($place_mapa,2,str_replace(',', '',$all_input['ticket_2']),$tbl_news->id,'seat-a');
                $place_maps = \tblPlaceMapModel::where('place_id',2)->where('ticket_id',3)->get(); 
                $this->insertTicketShow($place_maps,3,str_replace(',', '',$all_input['ticket_3']),$tbl_news->id,'seat-s');
				/* khoa ghế mặc định */
				$default_lock = ['AA01','AA02','AA03','AA04','AA05','AA06','AA07','AA08','AA09','AA10','AA11','AA12','AA13','AA14','AA15','AA16','AA17','AA18','AA19','AA20','AA21','AA22','AA23','AA24','AA25','AA26','AA27','AA28','AA30','AA32','BB01','BB02','BB03','BB04','BB05','BB06','BB07','BB08','BB09','BB10','BB11','BB12','BB13','BB14','BB15','BB16','BB17','BB18','BB19','BB20','BB21','BB22','BB23','BB24','BB25','BB26','BB28','BB30','BB32','CC01','CC02','CC03','CC04','CC05','CC06','CC07','CC08','CC09','CC10','CC11','CC12','CC13','CC14','CC15','CC16','CC17','CC18','CC19','CC20','CC21','CC22','CC23','CC24','CC25','CC26','CC27','CC28','CC30','CC32','DD01','DD02','DD03','DD04','DD05','DD06','DD07','DD08','DD09','DD10','DD11','DD12','DD13','DD14','DD15','DD16','DD17','DD18','DD19','DD20','DD21','DD22','DD23','DD24','DD25','DD26','DD27','DD28','DD29','DD30','DD32','DD34'];				
				\DB::table('tbl_ticket_show')->where('ticket_id',2)->whereIn('seat_name_orin',$default_lock)->where('show_id',$tbl_news->id)->update(['status'=>4]);
            }
            /* khoa ghe */
            if(isset($all_input['name_seat_lock']) && $all_input['name_seat_lock']!=''){
                $explode_name = explode(',',$all_input['name_seat_lock']);
                $explode_type = explode(',',$all_input['type_seat_lock']);
                for($i=0;$i<count($explode_name);$i++){
                    if(isset($explode_type[$i])){
                        if($explode_type[$i]=='seat-s'){
                            \DB::table('tbl_ticket_show')->where('ticket_id',3)->where('seat_name_orin',$explode_name[$i])->where('show_id',$tbl_news->id)->update(['status'=>4]);
                        }else if($explode_type[$i]=='seat-b'){
                            \DB::table('tbl_ticket_show')->where('ticket_id',1)->where('seat_name_orin',$explode_name[$i])->where('show_id',$tbl_news->id)->update(['status'=>4]);
                        }else if($explode_type[$i]=='seat-a'){
                            \DB::table('tbl_ticket_show')->where('ticket_id',2)->where('seat_name_orin',$explode_name[$i])->where('show_id',$tbl_news->id)->update(['status'=>4]);
                        }
                    }
                }
            }

            return \Redirect::action('\ADMIN\ShowController@getAdd');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }
    public function insertTicketShow($arr=[],$ticket_id,$price,$id_show,$seat_position_class){
        if(count($arr)>0){
            $data = [];
            foreach($arr as $item){
                $data[] = ['show_id'=>$id_show,'ticket_id'=>$ticket_id,'seat_name'=>$item->seat_name,'seat_name_orin'=>$item->seat_name_orin,'price'=>$price,'status'=>1,'seat_type_class'=>$item->class_name,'seat_position_class'=>$seat_position_class];                
            }
            \DB::table('tbl_ticket_show')->insert($data);
        }
    }
    private function updatePriceShow($ticket_id,$price,$id_show){        
        $ticket_show = \tblTicketShowModel::where('show_id',$id_show)->where('ticket_id',$ticket_id)->update(array('price' => $price));        
    }
    public function getEdit($lang, $id) {
        $data_news = \tblShowModel::join('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')
                    ->leftJoin('tbl_lang', 'tbl_show_lang.lang_id', '=', 'tbl_lang.id')
                    ->leftJoin('tbl_seo', 'tbl_show_lang.seo_id', '=', 'tbl_seo.id')
                    ->where('tbl_show.id', $id)
                    ->select('tbl_lang.id as langid','tbl_show_lang.*','tbl_show.*','tbl_seo.id as seoid','tbl_seo.title','tbl_seo.description as seodesc','tbl_seo.keyword','tbl_seo.robots_index','tbl_seo.robots_follow','tbl_seo.robots_advanced','tbl_seo.fb_title','tbl_seo.fb_description','tbl_seo.fb_image','tbl_seo.g_title','tbl_seo.g_description','tbl_seo.g_image')
                    ->get();
        $lang_code = \tblLangModel::where('id',$lang)->first();
        if (count($data_news) > 0) {          
            $allplace =  \tblPlaceModel::leftJoin('tbl_place_lang', 'tbl_place.id', '=', 'tbl_place_lang.place_id')->where('tbl_place_lang.lang_id',$this->d_lang)
            ->where('tbl_place_lang.status',1)->get();
            $place = \tblPlaceModel::leftJoin('tbl_place_lang', 'tbl_place.id', '=', 'tbl_place_lang.place_id')->where('tbl_place_lang.lang_id',$this->d_lang)
            ->where('tbl_place_lang.place_id',$data_news[0]->place_id)->get();
            $ticket = \tblTicketModel::leftJoin('tbl_ticket_lang', 'tbl_ticket.id', '=', 'tbl_ticket_lang.ticket_id')->where('tbl_ticket_lang.lang_id',$this->d_lang)
            ->where('tbl_ticket_lang.status',1)->orderBy('tbl_ticket_lang.created_at','desc')->get();
            if($data_news[0]->artist_id!=''){
                $list_artist = \tblArtistModel::join('tbl_artist_lang', 'tbl_artist.id', '=', 'tbl_artist_lang.artist_id')->where('tbl_artist_lang.lang_id',$this->d_lang)->whereIn('tbl_artist_lang.artist_id',explode(',',$data_news[0]->artist_id))->where('tbl_artist_lang.status',1)->get();
            }else{
                $list_artist=[];
            }
            if($data_news[0]->sheet_id!=''){
                $list_sheet = \tblSheetModel::join('tbl_sheet_lang', 'tbl_sheet.id', '=', 'tbl_sheet_lang.sheet_id')->where('tbl_sheet_lang.lang_id',$this->d_lang)->whereIn('tbl_sheet_lang.sheet_id',explode(',',$data_news[0]->sheet_id))->where('tbl_sheet_lang.status',1)->get();
            }else{
                $list_sheet=[];
            }
            if($data_news[0]->sponsor_id!=''){
                $list_sponsor = \tblManufModel::whereIn('tbl_manuf.id',explode(',',$data_news[0]->sponsor_id))->get();
            }else{
                $list_sponsor=[];
            }
            $cat_select = \tblShowViewsModel::select('cat_id')->where('show_id', $id)->get();
            $list_selected = [];
            foreach ($cat_select as $list_catvalue) {
                $list_selected[] = $list_catvalue->cat_id;
            }
            $all_category = \tblShowCategoryModel::orderBy('position','asc')->get();
            if($data_news[0]->conductor_id!=''){
                $conductor = \tblArtistModel::join('tbl_artist_lang', 'tbl_artist.id', '=', 'tbl_artist_lang.artist_id')->where('tbl_artist_lang.lang_id',$this->d_lang)->where('tbl_artist_lang.artist_id',$data_news[0]->conductor_id)->first();
            }else{
                $conductor=[];
            }
            $list_ticket = \tblTicketShowModel::where('show_id',$id)->groupBy('ticket_id')->get();
			
            /* lay ghe khoa */
            $list_lock = \tblTicketShowModel::where('show_id',$id)->where('status',4)->get();

            /* show chua ban ghe */
            $check_order = \tblOrderModel::where('show_id',$id)->count();
            if($check_order>0){
                return \View::make('admin.show.edit_info')->with('list_lock',$list_lock)->with('list_artist',$list_artist)->with('list_sheet',$list_sheet)->with('ticket',$ticket)->with('place',$place)->with('lang_code', $lang_code)->with('data_news', $data_news)->with('list_selected', $list_selected)->with('all_category', $all_category)->with('list_sponsor', $list_sponsor)->with('list_ticket', $list_ticket)->with('conductor', $conductor); 
            }else{
                return \View::make('admin.show.edit')->with('allplace',$allplace)->with('list_lock',$list_lock)->with('list_artist',$list_artist)->with('list_sheet',$list_sheet)->with('ticket',$ticket)->with('place',$place)->with('lang_code', $lang_code)->with('data_news', $data_news)->with('list_selected', $list_selected)->with('all_category', $all_category)->with('list_sponsor', $list_sponsor)->with('list_ticket', $list_ticket)->with('conductor', $conductor);            
            }
        } else {
            return \Redirect::action('\ADMIN\ShowController@getView');
        }
    }

    public function postEdit() {

        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){
            $row_news = \tblShowModel::find($all_input['id_show']);
			
            if($all_input['place_id']!=$row_news->place_id){
                \tblTicketShowModel::where('show_id',$all_input['id_show'])->delete();
                if($all_input['place_id']==1){
                    $place_mapb = \tblPlaceMapModel::where('place_id',1)->where('ticket_id',1)->get(); 
                    $this->insertTicketShow($place_mapb,1,str_replace(',', '',$all_input['ticket_1']),$all_input['id_show'],'seat-b');            
                    $place_mapa = \tblPlaceMapModel::where('place_id',1)->where('ticket_id',2)->get();             
                    $this->insertTicketShow($place_mapa,2,str_replace(',', '',$all_input['ticket_2']),$all_input['id_show'],'seat-a');
                    $place_maps = \tblPlaceMapModel::where('place_id',1)->where('ticket_id',3)->get();             
                    $this->insertTicketShow($place_maps,3,str_replace(',', '',$all_input['ticket_3']),$all_input['id_show'],'seat-s');
					
                }else if($all_input['place_id']==2){
                    $place_mapb = \tblPlaceMapModel::where('place_id',2)->where('ticket_id',1)->get();
                    $this->insertTicketShow($place_mapb,1,str_replace(',', '',$all_input['ticket_1']),$all_input['id_show'],'seat-b');             
                    $place_mapa = \tblPlaceMapModel::where('place_id',2)->where('ticket_id',2)->get();             
                    $this->insertTicketShow($place_mapa,2,str_replace(',', '',$all_input['ticket_2']),$all_input['id_show'],'seat-a');
                    $place_maps = \tblPlaceMapModel::where('place_id',2)->where('ticket_id',3)->get(); 
                    $this->insertTicketShow($place_maps,3,str_replace(',', '',$all_input['ticket_3']),$all_input['id_show'],'seat-s');
                }
            }else{
                $this->updatePriceShow(1,str_replace(',', '',$all_input['ticket_1']),$all_input['id_show']);
                $this->updatePriceShow(2,str_replace(',', '',$all_input['ticket_2']),$all_input['id_show']);
                $this->updatePriceShow(3,str_replace(',', '',$all_input['ticket_3']),$all_input['id_show']);
            }
            $row_news->conductor_id = ($all_input['list_id_autokeyword3'] != '') ? $all_input['list_id_autokeyword3'] : '';
            $row_news->artist_id = ($all_input['list_id_autokeyword'] != '') ? $all_input['list_id_autokeyword'] : '';
            $row_news->sheet_id = ($all_input['list_id_autokeyword1'] != '') ? $all_input['list_id_autokeyword1'] : '';
            $row_news->sponsor_id = ($all_input['list_id_autokeyword2'] != '') ? $all_input['list_id_autokeyword2'] : '';
            $row_news->place_id = ($all_input['place_id'] != '') ? $all_input['place_id'] : '';
            $row_news->avatar = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            $row_news->period = ($all_input['period'] != '') ? $all_input['period'] : '';
			if(\Input::has('is_sell')){
				$row_news->is_sell=1;
			}else{
				$row_news->is_sell=0;
			}
			if(\Input::has('sell_status')){
				$row_news->sell_status=1;
			}else{
				$row_news->sell_status=0;
			}
            $row_news->time_show = ($all_input['news_time'] != '') ? strtotime(\Datetime::createFromFormat('d M Y - H:i', $all_input['news_time'])->format('Y-m-d H:i')) : time();
            $row_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    $check = \tblShowLangModel::where('show_id', $all_input['id_show'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                    // neu co roi thi update
                    if(count($check)>0){      
                        $tbl_seo = \tblSeoModel::find($check->seo_id);
                        if(count($tbl_seo)>0){
                            $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                            $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                            $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                            $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                            $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                            $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                            $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                            $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                            $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                            $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                            $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                            $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                            $tbl_seo->save();
                            $tbl_detail_news = \tblShowLangModel::where('show_id', $all_input['id_show'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->where('seo_id', $all_input['seo_id_'. $item_lang->code])->first();  
                            if($tbl_detail_news->name != $all_input['name_' . $item_lang->code]){
                                $tbl_detail_news->slug = \Tree::gen_slug($all_input['name_' . $item_lang->code]) . '-' . $all_input['id_show'];                        
                            }     
                            $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';  
                            $tbl_detail_news->description = ($all_input['description_' . $item_lang->code] != '') ? strip_tags($all_input['description_' . $item_lang->code]) : ''; 
							$tbl_detail_news->artist_content = ($all_input['artist_content_' . $item_lang->code] != '') ? ($all_input['artist_content_' . $item_lang->code]) : ''; 
                            $tbl_detail_news->tags = ($all_input['tags_' . $item_lang->code] != '') ? $all_input['tags_' . $item_lang->code] : ''; 
                            $tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? $all_input['content_' . $item_lang->code] : '';                       
							$tbl_detail_news->place_text = ($all_input['place_text_' . $item_lang->code] != '') ? ($all_input['place_text_' . $item_lang->code]) : ''; 
                            $tbl_detail_news->save();
                        }else{
                            $tblSeoModel = new \tblSeoModel();
                            $tblSeoModel->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                            $tblSeoModel->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                            $tblSeoModel->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                            $tblSeoModel->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                            $tblSeoModel->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                            $tblSeoModel->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                            $tblSeoModel->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                            $tblSeoModel->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                            $tblSeoModel->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                            $tblSeoModel->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                            $tblSeoModel->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                            $tblSeoModel->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                            $tblSeoModel->save();
                            $tbl_detail_news = \tblShowLangModel::where('show_id', $all_input['id_show'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->first();  
                            $tbl_detail_news->seo_id = $tblSeoModel->id;
                            if($tbl_detail_news->name != $all_input['name_' . $item_lang->code]){
                                $tbl_detail_news->slug = \Tree::gen_slug($all_input['name_' . $item_lang->code]) . '-' . $all_input['id_show'];                        
                            }     
                            $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';  
                            $tbl_detail_news->description = ($all_input['description_' . $item_lang->code] != '') ? strip_tags($all_input['description_' . $item_lang->code]) : ''; 
							$tbl_detail_news->artist_content = ($all_input['artist_content_' . $item_lang->code] != '') ? ($all_input['artist_content_' . $item_lang->code]) : ''; 
							$tbl_detail_news->place_text = ($all_input['place_text_' . $item_lang->code] != '') ? ($all_input['place_text_' . $item_lang->code]) : ''; 
                            $tbl_detail_news->tags = ($all_input['tags_' . $item_lang->code] != '') ? $all_input['tags_' . $item_lang->code] : ''; 
                            $tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? $all_input['content_' . $item_lang->code] : '';                       
                            $tbl_detail_news->save();
                        }                  
                        
                    }else{
                        $tbl_seo = new \tblSeoModel();
                        $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                        $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                        $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                        $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                        $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                        $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                        $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                        $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                        $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                        $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                        $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                        $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                        $tbl_seo->save();
                        $tbl_detail_news = new \tblShowLangModel();
                        $tbl_detail_news->show_id = $all_input['id_show'];
                        $tbl_detail_news->lang_id = $item_lang->id;
                        $tbl_detail_news->seo_id = $tbl_seo->id;
                        $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';
                        $tbl_detail_news->description = ($all_input['description_' . $item_lang->code] != '') ? strip_tags($all_input['description_' . $item_lang->code]) : ''; 
						$tbl_detail_news->artist_content = ($all_input['artist_content_' . $item_lang->code] != '') ? ($all_input['artist_content_' . $item_lang->code]) : ''; 
                        $tbl_detail_news->tags = ($all_input['tags_' . $item_lang->code] != '') ? $all_input['tags_' . $item_lang->code] : ''; 
                        $tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? $all_input['content_' . $item_lang->code] : '';   
						$tbl_detail_news->place_text = ($all_input['place_text_' . $item_lang->code] != '') ? ($all_input['place_text_' . $item_lang->code]) : ''; 
                        $tbl_detail_news->slug = \Tree::gen_slug($all_input['name_' . $item_lang->code]) . '-' . $all_input['id_show'];                   
                        $tbl_detail_news->status = 1;
                        $tbl_detail_news->save();
                    }
                    /* bang tag */
                    \tblTagsRelationshipModel::where('post_id',$tbl_detail_news->id)->where('type',4)->delete();
                    if(\Input::get('tags_' . $item_lang->code)!=''){
                        foreach(explode(',',\Input::get('tags_' . $item_lang->code)) as $i_tag){
                            $check = \tblTagsModel::where('slug', \Tree::gen_slug($i_tag))->first();
                            if(count($check)<=0){
                                $tblTagModel = new \tblTagsModel();
                                $tblTagModel->name = $i_tag;
                                $tblTagModel->slug = \Tree::gen_slug($i_tag);
                                $tblTagModel->save();
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $tblTagModel->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 4;
                                $tblTagViewModel->save();
                            }else{
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $check->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 4;
                                $tblTagViewModel->save();
                            }
                        }
                    }
                }   

                

                $cat_list = isset($all_input['cat_id_' . $item_lang->id]) ? $all_input['cat_id_' . $item_lang->id] : '';
                if (count($cat_list) > 0 && $cat_list != '') {
                    \tblShowViewsModel::where('show_id',$all_input['id_show'])->where('lang_id',$item_lang->id)->delete();
                    foreach ($cat_list as $item) {
                        $tbl_news_view = new \tblShowViewsModel();
                        $tbl_news_view->show_id = $all_input['id_show'];
                        $tbl_news_view->cat_id = $item;
                        $tbl_news_view->lang_id = $item_lang->id;
                        $tbl_news_view->save();
                    }
                }else{
					\tblShowViewsModel::where('show_id',$all_input['id_show'])->where('lang_id',$item_lang->id)->delete();
				}
            }

            /* khoa ghe */
            \DB::table('tbl_ticket_show')->where('status',4)->where('show_id',$all_input['id_show'])->update(['status'=>1]);
            if(isset($all_input['name_seat_lock']) && $all_input['name_seat_lock']!=''){
                $explode_name = explode(',',$all_input['name_seat_lock']);
                $explode_type = explode(',',$all_input['type_seat_lock']);
                for($i=0;$i<count($explode_name);$i++){
                    if(isset($explode_type[$i])){
                        if($explode_type[$i]=='seat-s'){
                            \DB::table('tbl_ticket_show')->where('ticket_id',3)->where('seat_name_orin',$explode_name[$i])->where('show_id',$all_input['id_show'])->update(['status'=>4]);
                        }else if($explode_type[$i]=='seat-b'){
                            \DB::table('tbl_ticket_show')->where('ticket_id',1)->where('seat_name_orin',$explode_name[$i])->where('show_id',$all_input['id_show'])->update(['status'=>4]);
                        }else if($explode_type[$i]=='seat-a'){
                            \DB::table('tbl_ticket_show')->where('ticket_id',2)->where('seat_name_orin',$explode_name[$i])->where('show_id',$all_input['id_show'])->update(['status'=>4]);
                        }
                    }
                }
            }
            return \Redirect::action('\ADMIN\ShowController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }    
    public function postEditInfo() {

        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();
        $all_input = \Input::all();
	
        if(\Input::get('name_'.$tblLangModel->code)){
            $row_news = \tblShowModel::find($all_input['id_show']);
            
            $row_news->conductor_id = ($all_input['list_id_autokeyword3'] != '') ? $all_input['list_id_autokeyword3'] : '';
            $row_news->artist_id = ($all_input['list_id_autokeyword'] != '') ? $all_input['list_id_autokeyword'] : '';
            $row_news->sheet_id = ($all_input['list_id_autokeyword1'] != '') ? $all_input['list_id_autokeyword1'] : '';
            $row_news->sponsor_id = ($all_input['list_id_autokeyword2'] != '') ? $all_input['list_id_autokeyword2'] : '';
            $row_news->avatar = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            $row_news->period = ($all_input['period'] != '') ? $all_input['period'] : '';
			if(\Input::has('is_sell')){
				$row_news->is_sell=1;
			}else{
				$row_news->is_sell=0;
			}
			if(\Input::has('sell_status')){
				$row_news->sell_status=1;
			}else{
				$row_news->sell_status=0;
			}
            $row_news->time_show = ($all_input['news_time'] != '') ? strtotime(\Datetime::createFromFormat('d M Y - H:i', $all_input['news_time'])->format('Y-m-d H:i')) : time();
            $row_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    $check = \tblShowLangModel::where('show_id', $all_input['id_show'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                    // neu co roi thi update
                    if(count($check)>0){      
                        $tbl_seo = \tblSeoModel::find($check->seo_id);
                        if(count($tbl_seo)>0){
                            $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                            $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                            $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                            $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                            $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                            $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                            $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                            $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                            $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                            $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                            $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                            $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                            $tbl_seo->save();
                            $tbl_detail_news = \tblShowLangModel::where('show_id', $all_input['id_show'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->where('seo_id', $all_input['seo_id_'. $item_lang->code])->first();  
                            if($tbl_detail_news->name != $all_input['name_' . $item_lang->code]){
                                $tbl_detail_news->slug = \Tree::gen_slug($all_input['name_' . $item_lang->code]) . '-' . $all_input['id_show'];                        
                            }     
                            $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';  
                            $tbl_detail_news->description = ($all_input['description_' . $item_lang->code] != '') ? strip_tags($all_input['description_' . $item_lang->code]) : ''; 
                            $tbl_detail_news->tags = ($all_input['tags_' . $item_lang->code] != '') ? $all_input['tags_' . $item_lang->code] : ''; 
                            $tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? $all_input['content_' . $item_lang->code] : '';                       
							$tbl_detail_news->place_text = ($all_input['place_text_' . $item_lang->code] != '') ? ($all_input['place_text_' . $item_lang->code]) : ''; 
                            $tbl_detail_news->save();
                        }else{
                            $tblSeoModel = new \tblSeoModel();
                            $tblSeoModel->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                            $tblSeoModel->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                            $tblSeoModel->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                            $tblSeoModel->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                            $tblSeoModel->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                            $tblSeoModel->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                            $tblSeoModel->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                            $tblSeoModel->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                            $tblSeoModel->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                            $tblSeoModel->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                            $tblSeoModel->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                            $tblSeoModel->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                            $tblSeoModel->save();
                            $tbl_detail_news = \tblShowLangModel::where('show_id', $all_input['id_show'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->first();  
                            $tbl_detail_news->seo_id = $tblSeoModel->id;
                            if($tbl_detail_news->name != $all_input['name_' . $item_lang->code]){
                                $tbl_detail_news->slug = \Tree::gen_slug($all_input['name_' . $item_lang->code]) . '-' . $all_input['id_show'];                        
                            }     
                            $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';  
                            $tbl_detail_news->description = ($all_input['description_' . $item_lang->code] != '') ? strip_tags($all_input['description_' . $item_lang->code]) : ''; 
                            $tbl_detail_news->tags = ($all_input['tags_' . $item_lang->code] != '') ? $all_input['tags_' . $item_lang->code] : ''; 
                            $tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? $all_input['content_' . $item_lang->code] : '';                       
							$tbl_detail_news->place_text = ($all_input['place_text_' . $item_lang->code] != '') ? ($all_input['place_text_' . $item_lang->code]) : ''; 
                            $tbl_detail_news->save();
                        }                  
                        
                    }else{
                        $tbl_seo = new \tblSeoModel();
                        $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                        $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                        $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                        $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                        $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                        $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                        $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                        $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                        $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                        $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                        $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                        $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                        $tbl_seo->save();
                        $tbl_detail_news = new \tblShowLangModel();
                        $tbl_detail_news->show_id = $all_input['id_show'];
                        $tbl_detail_news->lang_id = $item_lang->id;
                        $tbl_detail_news->seo_id = $tbl_seo->id;
                        $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';
                        $tbl_detail_news->description = ($all_input['description_' . $item_lang->code] != '') ? strip_tags($all_input['description_' . $item_lang->code]) : ''; 
                        $tbl_detail_news->tags = ($all_input['tags_' . $item_lang->code] != '') ? $all_input['tags_' . $item_lang->code] : ''; 
                        $tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? $all_input['content_' . $item_lang->code] : ''; 
$tbl_detail_news->place_text = ($all_input['place_text_' . $item_lang->code] != '') ? ($all_input['place_text_' . $item_lang->code]) : ''; 						
                        $tbl_detail_news->slug = \Tree::gen_slug($all_input['name_' . $item_lang->code]) . '-' . $all_input['id_show'];                   
                        $tbl_detail_news->status = 1;
                        $tbl_detail_news->save();
                    }
                    /* bang tag */
                    \tblTagsRelationshipModel::where('post_id',$tbl_detail_news->id)->where('type',4)->delete();
                    if(\Input::get('tags_' . $item_lang->code)!=''){
                        foreach(explode(',',\Input::get('tags_' . $item_lang->code)) as $i_tag){
                            $check = \tblTagsModel::where('slug', \Tree::gen_slug($i_tag))->first();
                            if(count($check)<=0){
                                $tblTagModel = new \tblTagsModel();
                                $tblTagModel->name = $i_tag;
                                $tblTagModel->slug = \Tree::gen_slug($i_tag);
                                $tblTagModel->save();
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $tblTagModel->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 4;
                                $tblTagViewModel->save();
                            }else{
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $check->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 4;
                                $tblTagViewModel->save();
                            }
                        }
                    }
                }   

                

                $cat_list = isset($all_input['cat_id_' . $item_lang->id]) ? $all_input['cat_id_' . $item_lang->id] : '';
                if (count($cat_list) > 0 && $cat_list != '') {
                    \tblShowViewsModel::where('show_id',$all_input['id_show'])->where('lang_id',$item_lang->id)->delete();
                    foreach ($cat_list as $item) {
                        $tbl_news_view = new \tblShowViewsModel();
                        $tbl_news_view->show_id = $all_input['id_show'];
                        $tbl_news_view->cat_id = $item;
                        $tbl_news_view->lang_id = $item_lang->id;
                        $tbl_news_view->save();
                    }
                }else{
					\tblShowViewsModel::where('show_id',$all_input['id_show'])->where('lang_id',$item_lang->id)->delete();
				}
            }

            
            return \Redirect::action('\ADMIN\ShowController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    } 
    public function postDelete() {

        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblShowLangModel::where('show_id', $all_input['id'])->update(array('status' => 2));           
            return $this->getView()->render();
        }
    }

    public function postPostConfirm() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
           \tblShowLangModel::where('show_id', $all_input['id'])->update(array('status' => 1));
            return $this->getView()->render();
        }
    }
    public function postPlace(){
        $data = \tblPlaceModel::find(\Input::get('id'));
        return $data->avatar;
    }
    public function postAutocomplete(){
        $keyword = \Input::get('keyword');
        $sql_data  = \tblArtistModel::join('tbl_artist_lang', 'tbl_artist.id', '=', 'tbl_artist_lang.artist_id')->where('tbl_artist_lang.lang_id',$this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_artist_lang.a_title', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_artist.id', 'desc');
        $data = $sql_data->limit(5)->get();
        return \View::make('admin.show.autocomplete')->with('data', $data);
    }
    public function postAutocomplete1(){
        $keyword = \Input::get('keyword');
        $sql_data  = \tblSheetModel::join('tbl_sheet_lang', 'tbl_sheet.id', '=', 'tbl_sheet_lang.sheet_id')->where('tbl_sheet_lang.lang_id',$this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_sheet_lang.title', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_sheet.id', 'desc');
        $data = $sql_data->limit(5)->get();
        return \View::make('admin.show.autocomplete1')->with('data', $data);
    }
    public function postAutocomplete2(){
        $keyword = \Input::get('keyword');
        $sql_data  = \tblManufModel::select('tbl_manuf.*');
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_manuf.name', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_manuf.id', 'desc');
        $data = $sql_data->limit(5)->get();
        return \View::make('admin.show.autocomplete2')->with('data', $data);
    }
    public function postAutocomplete3(){
        $keyword = \Input::get('keyword');
        $sql_data  = \tblArtistModel::join('tbl_artist_lang', 'tbl_artist.id', '=', 'tbl_artist_lang.artist_id')->where('tbl_artist_lang.lang_id',$this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_artist_lang.a_title', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_artist.id', 'desc');
        $data = $sql_data->limit(5)->get();
        return \View::make('admin.show.autocomplete3')->with('data', $data);
    }
    public function postModal(){
        
        $data = \tblPlaceMapModel::where('place_id',\Input::get('id'))->get();
        if(\Input::get('id')==1){
            return \View::make('admin.show.nhahat')->with('data', $data);
        }else if(\Input::get('id')==2){
            return \View::make('admin.show.hoanhac')->with('data', $data);
        }        
    }
    /* load map edit */
    public function postMap(){
        $data = \tblTicketShowModel::where('show_id',\Input::get('id'))->get();
        $show = \tblShowModel::find(\Input::get('id'));
        if($show->place_id==1){
            return \View::make('admin.show.nhahat_edit')->with('data', $data);
        }else if($show->place_id==2){
            return \View::make('admin.show.hoanhac_edit')->with('data', $data);
        }        
    }
    public function postCheckLock(){
        var_dump(\Input::get('seat'));

    }

}
