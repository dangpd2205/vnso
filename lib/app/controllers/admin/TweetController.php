<?php

namespace ADMIN;

use View;

class TweetController extends \BaseController {
    private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // H?m ch?y khi g?i class
    public function __construct() {
        $this->titlepage = 'Quản lý phản hồi trang chủ';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
    }
	
	public function getAdd(){
		return \View::make('admin.tweet.add');
	}
	public function postAdd(){
		$rules = array(
            "name" => "required",
            "content" => "required",
        );
        $all_input = \Input::all();
        $validate = \Validator::make(\Input::all(), $rules, array(), \Lang::get('admin/attributes.tbl_tweet'));
		if (!$validate->fails()) {
			$tweet = new \tblTweetModel();
			$tweet->name = $all_input['name'];
			$tweet->content = $all_input['content'];
			$tweet->image = $all_input['image_hidden'];
			$tweet->save();
			return \Redirect::action('\ADMIN\TweetController@getAdd');
		}else{
			return \Redirect::back()->withInput()->withErrors($validate->messages()->all('<li>:message</li>'));
		}
		
	}
	public function getEdit($id){
		$data = \tblTweetModel::find($id);
		return \View::make('admin.tweet.edit')->with('data',$data);
	}
	public function postEdit(){
		$rules = array(
            "name" => "required",
            "content" => "required",
        );
        $all_input = \Input::all();
        $validate = \Validator::make(\Input::all(), $rules, array(), \Lang::get('admin/attributes.tbl_tweet'));
		if (!$validate->fails()) {
			$tweet = \tblTweetModel::find($all_input['id']);
			$tweet->name = $all_input['name'];
			$tweet->content = $all_input['content'];
			$tweet->image = $all_input['image_hidden'];
			$tweet->save();
			return \Redirect::action('\ADMIN\TweetController@getView');
		}else{
			return \Redirect::back()->withInput()->withErrors($validate->messages()->all('<li>:message</li>'));
		}
		
	}
	
	public function postDeleteMulti() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $comment = \tblTweetModel::whereIn('id', $all_input['list_id'])->delete();
            return $this->getView()->render();
        }
    }
	
	public function getView(){
		\Session::forget('tweet_search_key');
        \Session::forget('tweet_status_key');
		$data = \tblTweetModel::orderBy('id','desc')->paginate(10);        
        if (\Request::ajax()) {
            return \View::make('admin.tweet.ajax')->with('data', $data);
        } else {
            return \View::make('admin.tweet.view')->with('data', $data);
        }
	}	
	public function postSearch(){
        $keyword = '';
        \Session::forget('tweet_search_key');
        \Session::forget('tweet_status_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword = 'null';
            
        }
        \Session::set('fb_search_key', $keyword);
        return \Redirect::action('\ADMIN\TweetController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblTweetModel::orderBy('id', 'desc');
        $sql_data->where(function($query) use ($keyword) {
            $query->where('name', 'LIKE', '%' . $keyword . '%');
        });
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.tweet.ajax')->with('data', $data_lang);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\TweetController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = $sql_data = \tblTweetModel::orderBy('id', 'desc');
        
        if(\Session::has('tweet_status_key') && \Session::get('tweet_status_key')!='null'){
            $sql_data->where('status', \Session::get('tweet_status_key'));
        }
        
        if(\Session::has('tweet_search_key') && \Session::get('tweet_search_key')!='null'){
            $keyword = \Session::get('tweet_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('name', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.tweet.ajax')->with('data', $data_lang);
    }

   
}
