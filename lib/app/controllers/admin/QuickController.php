<?php

namespace ADMIN;

use View;

class QuickController extends \BaseController {

    private $listLang;
    private $d_lang;

// Hạm chạy khi gọi class
    public function __construct() {
        $this->listLang = \tblLangModel::orderBy('name')->get();
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
    }



    

    

    

    public function postQuickAdd() {
        if (\Request::ajax()) {
            foreach ($this->listLang as $item) {
                $name = \Input::get('product_add_cat_name_' . $item->code);
                $parent = \Input::get('product_add_cat_parent_' . $item->code);
                $des = \Input::get('product_add_cat_description_' . $item->code);
                $tblNewsCategoryModel = new \tblProjectCategoryModel();
                $tblNewsCategoryModel->name = $name;
                $tblNewsCategoryModel->parent = $parent;
                $tblNewsCategoryModel->lang_id = $item->id;
                $tblNewsCategoryModel->description = $des;
                $tblNewsCategoryModel->slug = \Str::slug($name);
                $tblNewsCategoryModel->save();
            }
            $out_put = [];
            foreach ($this->listLang as $item1) {
                $array = \tblProjectCategoryModel::where('lang_id', $item1->id)->orderBy('name')->get();
                $out_put[] = array(
                    'check' => $item1->code,
                    'content' => \Tree::treereturn($array, [], $item1->id)
                );
            }
            echo json_encode($out_put);
        }
    }

}
