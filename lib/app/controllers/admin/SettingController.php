<?php

namespace ADMIN;

class SettingController extends \BaseController {

    private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Cấu hình website';
        \View::share('titlepage', $this->titlepage);
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
    }

    public function getView() {
        $data = \tblSettingModel::find(1);
        $menu_list = \tblMenuGroupModel::all();
        $all_category = \tblProductCategoryModel::orderBy('name')->where('lang_id', $this->d_lang)->get();
        $currency = \tblCurrencyModel::orderBy('name')->get();
        $currencyarray = [];
        foreach ($currency as $item) {
            $currencyarray += array($item->id => $item->name);
        }
        $income = \tblIncomeGroupModel::orderBy('name')->where('status', 1)->get();
        $incomearray = [];
        foreach ($income as $item) {
            $incomearray += array($item->id => $item->name);
        }
        $slider_list = \tblLayersliderModel::all();
        return \View::make('admin.setting.view')->with('slider_list', $slider_list)->with('incomearray', $incomearray)->with('currencyarray', $currencyarray)->with('data', $data)->with('menu_list', $menu_list)->with('all_category', $all_category);
    }

    public function postView() {
        
    }

    public function postSaveSetting() {
        \Cache::flush();
        $all_input = \Input::all();
        $id = $all_input['id'];
        unset($all_input['_token']);
        unset($all_input['id']);
        $tbl_setting = \tblSettingModel::find($id);
        $tbl_setting->data = json_encode($all_input);
        $tbl_setting->save();
        \Cache::forget('all_setting');
        return \Redirect::action('\ADMIN\SettingController@getView');
    }

    public function getDefault() {
        \Cache::flush();
        $data = \tblSettingModel::find(1);
        $data->data = $data->d_data;
        $data->save();
        return \Redirect::action('\ADMIN\SettingController@getView');
    }

    public function getInfo($page) {
        $menu_list = \tblMenuGroupModel::all();
        $all_category = \tblNewsCategoryModel::orderBy('position','asc')->get();
        $show_category = \tblShowCategoryModel::orderBy('position','asc')->get();
		$video = \tblVideoModel::leftJoin('tbl_video_lang','tbl_video.id','=','tbl_video_lang.video_id')
						->where('tbl_video_lang.status',1)
						->get();
     
        $data_s = new \stdClass();
        $allset = \tblSettingModel::all();
        foreach ($allset as $item) {
            if ($item->key && $item->value) {
                $data_s->{$item->key} = $item->value;
            }
        }
		
        $gallery = \tblGalleryModel::leftJoin('tbl_gallery_lang','tbl_gallery.id','=','tbl_gallery_lang.gallery_id')
                        ->orderBy('tbl_gallery_lang.created_at','desc')->where('tbl_gallery_lang.status', 1)->get();
		$slide = \tblLayersliderModel::all();
        return \View::make('admin.setting.' . $page)->with('show_category', $show_category)->with('gallery', $gallery)->with('data', $data_s)->with('slide', $slide)->with('video', $video)->with('all_category', $all_category)->with('menu_list', $menu_list);
    }

    public function postInfo() {
        \Cache::flush();
        $all_input = \Input::all();
        $id_insert = [];
        foreach ($all_input as $key => $value) {
            if ($key) {
                $check = \tblSettingModel::where('key', $key)->first();
                if ($check) {
                    $check->key = $key;
                    $check->value = $value;
                    $check->save();
                    $id_insert[] = $check->id;
                } else {
                    $add_new = new \tblSettingModel();
                    $add_new->key = $key;
                    $add_new->value = $value;
                    $add_new->save();
                    $id_insert[] = $add_new->id;
                }
            }
        }
        //  \tblSettingModel::whereNotIn('id', $id_insert)->delete();
        \Cache::forget('all_setting');
        return \Redirect::back();
    }
	
	public function postAutocomplete(){
		$keyword = \Input::get('keyword');
        $data = \tblNewsModel::leftJoin('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id') 
			->select('tbl_news_detail_lang.*')
			->where('tbl_news_detail_lang.status',1)
			->where('tbl_news_detail_lang.lang_id',\Input::get('lang'));
        if($keyword!='' || $keyword!=null || $keyword!='null'){  
            $data->where(function($query) use ($keyword){
                $query->where('tbl_news_detail_lang.news_title', 'LIKE', '%' . $keyword . '%');
            });                                           
        } 
        $return = $data->limit(5)->get();
        return \View::make('admin.setting.autocomplete')->with('data', $return);
    }
}
