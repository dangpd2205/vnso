<?php

namespace ADMIN;

class TicketController extends \BaseController {
	private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý loại vé';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }


    public function getView() {
		
        \Session::forget('ticket_status_key');
        \Session::forget('ticket_search_key');
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $obj_news = new \tblTicketModel();
            $obj_news->resolveConnection()->getPaginator()->setBaseUrl(\URL::action('\ADMIN\TicketController@getView'));
            $data_lang = $obj_news->join('tbl_ticket_lang', 'tbl_ticket.id', '=', 'tbl_ticket_lang.ticket_id')->where('tbl_ticket_lang.lang_id', $this->d_lang)->orderBy('tbl_ticket.id', 'desc')->where('tbl_ticket_lang.status',1)->paginate(10);
            
            return \View::make('admin.ticket.ajax')->with('data', $data_lang);
        } else {
            $data  = \tblTicketModel::join('tbl_ticket_lang', 'tbl_ticket.id', '=', 'tbl_ticket_lang.ticket_id')->where('tbl_ticket_lang.lang_id', $this->d_lang)->orderBy('tbl_ticket.id', 'desc')->where('tbl_ticket_lang.status',1)->paginate(10);
                
            return \View::make('admin.ticket.view')->with('data', $data);
        }

    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('ticket_status_key');
        \Session::forget('ticket_search_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword='null';
        }
        \Session::set('ticket_search_key', $keyword);
        return \Redirect::action('\ADMIN\TicketController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblTicketModel::join('tbl_ticket_lang', 'tbl_ticket.id', '=', 'tbl_ticket_lang.ticket_id')->where('tbl_ticket_lang.lang_id', $this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_ticket_lang.name', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_ticket.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.ticket.ajax')->with('data', $data_lang);
    }

    public function postFilter(){
        
        $status = '';
        \Session::forget('ticket_status_key');
        \Session::forget('ticket_search_key');
        
        if (\Input::has('news_status_filter') || \Input::get('news_status_filter') != '') {
            $status=\Input::get('news_status_filter');
        }else{
            $status='null';
        }
        \Session::set('ticket_status_key', $status);
        return \Redirect::action('\ADMIN\TicketController@getFilter', array($status));
    }

    public function getFilter($status=''){
        $sql_data = \tblTicketModel::join('tbl_ticket_lang', 'tbl_ticket.id', '=', 'tbl_ticket_lang.ticket_id')->where('tbl_ticket_lang.lang_id', $this->d_lang);
       
        if($status!='null'){
            $sql_data->where('tbl_ticket_lang.status', $status);
        }
        $sql_data->orderBy('tbl_ticket.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.ticket.ajax')->with('data', $data_lang);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\TicketController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = \tblTicketModel::join('tbl_ticket_lang', 'tbl_ticket.id', '=', 'tbl_ticket_lang.ticket_id')->where('tbl_ticket_lang.lang_id', $this->d_lang);
        
        if(\Session::has('ticket_status_key') && \Session::get('ticket_status_key')!='null'){
            $sql_data->where('tbl_ticket_lang.status', \Session::get('ticket_status_key'));
        }else{
            $sql_data->where('tbl_ticket_lang.status',1);
        }
        if(\Session::has('ticket_search_key') && \Session::get('ticket_search_key')!='null'){
            $keyword=\Session::get('ticket_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('tbl_ticket_lang.name', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->orderBy('tbl_ticket.id', 'desc')->paginate($page);
        return \View::make('admin.ticket.ajax')->with('data', $data_lang);
    }

    public function getAdd() {
        return \View::make('admin.ticket.add');
    }

    public function postAdd() {
        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();

        $log = [];
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){

            $tbl_news = new \tblTicketModel();      
            $tbl_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    
                    $tbl_detail_news = new \tblTicketLangModel();
                    $tbl_detail_news->ticket_id = $tbl_news->id;
                    $tbl_detail_news->lang_id = $item_lang->id;
                    $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';                    
                    $tbl_detail_news->status = 1;
                    $tbl_detail_news->save();

                }
            }
            return \Redirect::action('\ADMIN\TicketController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }
    
    public function getEdit($lang, $id) {
        $data_news = \tblTicketModel::join('tbl_ticket_lang', 'tbl_ticket.id', '=', 'tbl_ticket_lang.ticket_id')
                    ->leftJoin('tbl_lang', 'tbl_ticket_lang.lang_id', '=', 'tbl_lang.id')
                    ->where('tbl_ticket.id', $id)
                    ->select('tbl_lang.id as langid','tbl_ticket.price','tbl_ticket_lang.*')
                    ->get();
        $lang_code = \tblLangModel::where('id',$lang)->first();
        if (count($data_news) > 0) {           
            return \View::make('admin.ticket.edit')->with('lang_code', $lang_code)->with('data_news', $data_news);            
        } else {
            return \Redirect::action('\ADMIN\TicketController@getView');
        }
    }

    public function postEdit() {

        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){
            $row_news = \tblTicketModel::find($all_input['id_ticket']);
            $row_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    $check = \tblTicketLangModel::where('ticket_id', $all_input['id_ticket'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                    // neu co roi thi update
                    if(count($check)>0){                        
                        $tbl_detail_news = \tblTicketLangModel::where('ticket_id', $all_input['id_ticket'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->first();					
                        $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';	                      
                        $tbl_detail_news->save();
                    }else{
                       
                        $tbl_detail_news = new \tblTicketLangModel();
                        $tbl_detail_news->ticket_id = $all_input['id_ticket'];
						$tbl_detail_news->lang_id = $item_lang->id; 
						$tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';                    
						$tbl_detail_news->status = 1;
                        $tbl_detail_news->save();
                    }
                }   
            }
    	
            return \Redirect::action('\ADMIN\TicketController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }

    public function postDelete() {

        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblTicketLangModel::where('ticket_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 2));   
            
            return $this->getView()->render();
        }
    }

    public function postPostConfirm() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
           \tblTicketLangModel::where('ticket_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 1));
           
            return $this->getView()->render();
        }
    }

}
