<?php

namespace ADMIN;

use View;

class SubcribeController extends \BaseController {
    private $titlepage = 'webshop-x.com';
    private $listLang;
    private $d_lang;

  
    public function __construct() {
        $this->titlepage = 'Quản lý phản hồi';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
    }
	
	public function postDeleteMulti() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $comment = \tblSubcribeModel::whereIn('id', $all_input['list_id'])->delete();
            return $this->getView()->render();
        }
    }
	
	public function getView(){
		\Session::forget('sub_search_key');
		$data = \tblSubcribeModel::orderBy('id','desc')->paginate(10);        
        if (\Request::ajax()) {
            return \View::make('admin.subcribe.ajax')->with('data', $data);
        } else {
            return \View::make('admin.subcribe.view')->with('data', $data);
        }
	}	
	public function postSearch(){
        $keyword = '';
        \Session::forget('sub_search_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword = 'null';
            
        }
        \Session::set('sub_search_key', $keyword);
        return \Redirect::action('\ADMIN\SubcribeController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblSubcribeModel::orderBy('id', 'desc');
        $sql_data->where(function($query) use ($keyword) {
            $query->where('email', 'LIKE', '%' . $keyword . '%')
                                ->orWhere('name', 'LIKE', '%' . $keyword . '%');
        });
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.subcribe.ajax')->with('data', $data_lang);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\SubcribeController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = $sql_data = \tblSubcribeModel::orderBy('id', 'desc');
   
        
        if(\Session::has('sub_search_key') && \Session::get('sub_search_key')!='null'){
            $keyword = \Session::get('sub_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('email', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.subcribe.ajax')->with('data', $data_lang);
    }
	/* xuat excel */
	public function getExport(){
		$sql_data = $sql_data = \tblSubcribeModel::orderBy('id', 'desc');
		 
		if(\Session::has('sub_search_key') && \Session::get('sub_search_key')!='null'){
            $keyword = \Session::get('sub_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('email', 'LIKE', '%' . $keyword . '%');
            });
        }

		$return = $sql_data->get();
		\Excel::create('list_subcribe_' . date('d_m_Y_h_i_s', time()), function($excel) use($return){
			$excel->sheet('First sheet', function($sheet) use($return){
				$sheet->loadView('admin.subcribe.export', array('data' => $return));
			});
		})->download('xls');

	}
}
