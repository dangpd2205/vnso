<?php

namespace ADMIN;

use View;

class ManufController extends \BaseController {

    private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý tài trợ';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }

    public function getView() {
        $data = \tblManufModel::orderBy('created_at','desc')->paginate(10);
      
        if (\Request::ajax()) {
            return \View::make('admin.manuf.ajax')->with('data', $data);
        } else {
            return \View::make('admin.manuf.view')->with('data', $data);
        }
    }


    public function postDelete() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblManufModel::where('id', $all_input['id'])->delete();
            return $this->getView()->render();
        } else {
            return \App::abort(404);
        }
    }

    public function postDeleteMulti() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $comment = \tblManufModel::whereIn('id', $all_input['list_id'])->delete();
            return $this->getView()->render();
        }
    }
	
	public function getAdd(){
		return \View::make('admin.manuf.add');
	}
	public function postAdd(){
		if(\Input::get('name')!=''){
			$tblManufModel = new \tblManufModel();
			$tblManufModel->name = \Input::get('name');
            $tblManufModel->avatar = \Input::get('avatar');
			 $tblManufModel->content = \Input::get('content');
			$tblManufModel->url = \Input::get('url');						$tblManufModel->position = \Input::get('position');
			$tblManufModel->save();
			return \Redirect::action('\ADMIN\ManufController@getView');
		}else{
			return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập tên</li>');
		}
	}
	public function getEdit($id){
		$data = \tblManufModel::find($id);
		return \View::make('admin.manuf.edit')->with('data',$data);
	}
	public function postEdit(){
		if(\Input::get('name')!=''){
			$tblManufModel = \tblManufModel::find(\Input::get('id'));
			$tblManufModel->name = \Input::get('name');
            $tblManufModel->avatar = \Input::get('avatar');
			$tblManufModel->content = \Input::get('content');
			$tblManufModel->url = \Input::get('url');						$tblManufModel->position = \Input::get('position');
			$tblManufModel->save();
			return \Redirect::action('\ADMIN\ManufController@getView');
		}else{
			return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập tên</li>');
		}
	}

}
