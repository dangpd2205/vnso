<?php

namespace ADMIN;

use View;

class TranslateGoogleController extends \BaseController {

    private $titlepage = 'Pubweb.vn';

    public function postTranslateGoogle() {
        if (\Request::ajax()) {
            $tran = new \ADMIN\TranslateGoogle();
            $tran_string = html_entity_decode(\Input::get('string'));
            $tran_form = \Input::get('form');
            $tran_to = \Input::get('to');
            $str_return = $tran->staticTranslate($tran_string, $tran_form, $tran_to);
            return $str_return;
        }
    }

}
