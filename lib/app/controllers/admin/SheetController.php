<?php

namespace ADMIN;

class SheetController extends \BaseController {
	private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý bản nhạc';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }


    public function getView() {
		
        \Session::forget('sheet_status_key');
        \Session::forget('sheet_search_key');
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $obj_news = new \tblSheetModel();
            $obj_news->resolveConnection()->getPaginator()->setBaseUrl(\URL::action('\ADMIN\SheetController@getView'));
            $data_lang = $obj_news->join('tbl_sheet_lang', 'tbl_sheet.id', '=', 'tbl_sheet_lang.sheet_id')->where('tbl_sheet_lang.lang_id', $this->d_lang)->orderBy('tbl_sheet.id', 'desc')->where('tbl_sheet_lang.status',1)->paginate(10);
            
            return \View::make('admin.sheet.ajax')->with('data', $data_lang);
        } else {
            $data  = \tblSheetModel::join('tbl_sheet_lang', 'tbl_sheet.id', '=', 'tbl_sheet_lang.sheet_id')->where('tbl_sheet_lang.lang_id', $this->d_lang)->orderBy('tbl_sheet.id', 'desc')->where('tbl_sheet_lang.status',1)->paginate(10);
                
            return \View::make('admin.sheet.view')->with('data', $data);
        }

    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('sheet_status_key');
        \Session::forget('sheet_search_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword='null';
        }
        \Session::set('sheet_search_key', $keyword);
        return \Redirect::action('\ADMIN\SheetController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblSheetModel::join('tbl_sheet_lang', 'tbl_sheet.id', '=', 'tbl_sheet_lang.sheet_id')->where('tbl_sheet_lang.lang_id', $this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_sheet_lang.content', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_sheet.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.sheet.ajax')->with('data', $data_lang);
    }

    public function postFilter(){
        
        $status = '';
        \Session::forget('sheet_status_key');
        \Session::forget('sheet_search_key');
        
        if (\Input::has('news_status_filter') || \Input::get('news_status_filter') != '') {
            $status=\Input::get('news_status_filter');
        }else{
            $status='null';
        }
        \Session::set('sheet_status_key', $status);
        return \Redirect::action('\ADMIN\SheetController@getFilter', array($status));
    }

    public function getFilter($status=''){
        $sql_data = \tblSheetModel::join('tbl_sheet_lang', 'tbl_sheet.id', '=', 'tbl_sheet_lang.sheet_id')->where('tbl_sheet_lang.lang_id', $this->d_lang);
       
        if($status!='null'){
            $sql_data->where('tbl_sheet_lang.status', $status);
        }
        $sql_data->orderBy('tbl_sheet.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.sheet.ajax')->with('data', $data_lang);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\SheetController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = \tblSheetModel::join('tbl_sheet_lang', 'tbl_sheet.id', '=', 'tbl_sheet_lang.sheet_id')->where('tbl_sheet_lang.lang_id', $this->d_lang);
        
        if(\Session::has('sheet_status_key') && \Session::get('sheet_status_key')!='null'){
            $sql_data->where('tbl_sheet_lang.status', \Session::get('sheet_status_key'));
        }else{
            $sql_data->where('tbl_sheet_lang.status',1);
        }
        if(\Session::has('sheet_search_key') && \Session::get('sheet_search_key')!='null'){
            $keyword=\Session::get('sheet_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('tbl_sheet_lang.content', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->orderBy('tbl_sheet.id', 'desc')->paginate($page);
        return \View::make('admin.sheet.ajax')->with('data', $data_lang);
    }

    public function getAdd() {
        return \View::make('admin.sheet.add');
    }

    public function postAdd() {
        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();

        $log = [];
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){

            $tbl_news = new \tblSheetModel();       
            $tbl_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    
                    $tbl_detail_news = new \tblSheetLangModel();
                    $tbl_detail_news->sheet_id = $tbl_news->id;
                    $tbl_detail_news->lang_id = $item_lang->id;
                    $tbl_detail_news->title = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';  
                    $tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? strip_tags($all_input['content_' . $item_lang->code]) : '';                    
                    $tbl_detail_news->status = 1;
                    $tbl_detail_news->save();

                }
            }
            return \Redirect::action('\ADMIN\SheetController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }
    
    public function getEdit($lang, $id) {
        $data_news = \tblSheetModel::join('tbl_sheet_lang', 'tbl_sheet.id', '=', 'tbl_sheet_lang.sheet_id')
                    ->leftJoin('tbl_lang', 'tbl_sheet_lang.lang_id', '=', 'tbl_lang.id')
                    ->where('tbl_sheet.id', $id)
                    ->select('tbl_lang.id as langid','tbl_sheet_lang.*')
                    ->get();
        $lang_code = \tblLangModel::where('id',$lang)->first();
        if (count($data_news) > 0) {           
            return \View::make('admin.sheet.edit')->with('lang_code', $lang_code)->with('data_news', $data_news);            
        } else {
            return \Redirect::action('\ADMIN\SheetController@getView');
        }
    }

    public function postEdit() {

        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){
            $row_news = \tblSheetModel::find($all_input['id_sheet']);
          
            $row_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    $check = \tblSheetLangModel::where('sheet_id', $all_input['id_sheet'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                    // neu co roi thi update
                    if(count($check)>0){                        
                        $tbl_detail_news = \tblSheetLangModel::where('sheet_id', $all_input['id_sheet'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->first();					
                        $tbl_detail_news->title = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';	    
                         $tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? strip_tags($all_input['content_' . $item_lang->code]) : '';                    
                        $tbl_detail_news->save();
                    }else{
                       
                        $tbl_detail_news = new \tblSheetLangModel();
                        $tbl_detail_news->sheet_id = $all_input['id_sheet'];
						$tbl_detail_news->lang_id = $item_lang->id; 
						$tbl_detail_news->title = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';        
                        $tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? strip_tags($all_input['content_' . $item_lang->code]) : '';              
						$tbl_detail_news->status = 1;
                        $tbl_detail_news->save();
                    }
                }   
            }
    	
            return \Redirect::action('\ADMIN\SheetController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }

    public function postDelete() {

        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblSheetLangModel::where('sheet_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 2));           
            return $this->getView()->render();
        }
    }

    public function postPostConfirm() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
           \tblSheetLangModel::where('sheet_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 1));
            return $this->getView()->render();
        }
    }

}
