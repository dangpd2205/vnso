<?php

namespace ADMIN;

class SliderController extends \BaseController {

    private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý sản phẩm';
        \View::share('titlepage', $this->titlepage);
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }

    public static function getInputText($name, $value) {
        $val = '';
        if (isset($value[$name])) {
            $val = $value[$name];
        }
        echo \Form::text($name, $val);
    }

    public static function getInputCheckbox($name, $value) {
        $check = '';
        $val = '';
        if (isset($value[$name])) {
            if ($value[$name] == 'on') {
                $check = 'checked="checked';
                $val = 'on';
            }
        }
        echo \Form::checkbox($name, $val, $check);
    }

    public static function getInputNumber($name, $value) {
        $val = 0;
        if (isset($value[$name])) {
            $val = $value[$name];
        }
        echo \Form::input('number', $name, $val);
    }

    public static function lsGetInput($default, $current, $attrs = array()) {
        // Markup
        $el = \phpQuery::newDocumentHTML('<input>')->children();
        $type = is_string($default['value']) ? 'text' : 'number';
        $name = is_string($default['keys']) ? $default['keys'] : $default['keys'][0];
        $el->attr('type', $type);
        $el->attr('name', $name);
        $el->val($default['value']);

        // Attributes
        $attrs = isset($default['attrs']) ? array_merge($default['attrs'], $attrs) : $attrs;
        if (isset($attrs) && is_array($attrs)) {
            foreach ($attrs as $attr => $val) {
                $el->attr($attr, $val);
            }
        }

        // Tooltip
        if (isset($default['tooltip'])) {
            $el->attr('data-help', $default['tooltip']);
        }

        // Override the default
        if (isset($current[$name]) && $current[$name] !== '') {
            $el->val(stripslashes($current[$name]));
        }

        echo $el;
    }

    public static function lsGetCheckbox($default, $current, $attrs = array()) {

        // Markup
        $el = \phpQuery::newDocumentHTML('<input>')->children();
        $name = is_string($default['keys']) ? $default['keys'] : $default['keys'][0];
        $el->attr('type', 'checkbox');
        $el->attr('name', $name);

        // Attributes
        $attrs = isset($default['attrs']) ? array_merge($default['attrs'], $attrs) : $attrs;
        if (isset($attrs) && is_array($attrs)) {
            foreach ($attrs as $attr => $val) {
                $el->attr($attr, $val);
            }
        }

        // Checked?
        if ($default['value'] === true && count($current) < 3) {
            $el->attr('checked', 'checked');
        } elseif (isset($current[$name]) && $current[$name] != false && $current[$name] !== 'false') {
            $el->attr('checked', 'checked');
        }

        echo $el;
    }

    public static function lsGetSelect($default, $current, $attrs = array()) {

        // Var to hold data to print
        $el = \phpQuery::newDocumentHTML('<select>')->children();
        $name = is_string($default['keys']) ? $default['keys'] : $default['keys'][0];
        $el->attr('name', $name);
        $value = $default['value'];
        $options = array();

        // Attributes
        $attrs = isset($default['attrs']) ? array_merge($default['attrs'], $attrs) : $attrs;
        if (isset($attrs) && is_array($attrs)) {
            foreach ($attrs as $attr => $val) {
                if (!is_array($val)) {
                    $el->attr($attr, $val);
                }
            }
        }

        // Get options
        if (isset($default['options']) && is_array($default['options'])) {
            $options = $default['options'];
        } elseif (isset($attrs['options']) && is_array($attrs['options'])) {
            $options = $attrs['options'];
        }

        // Override the default
        if (isset($current[$name]) && $current[$name] !== '') {
            $value = $current[$name];
        }

        // Tooltip
        if (isset($default['tooltip'])) {
            $el->attr('data-help', $default['tooltip']);
        }

        // Add options
        foreach ($options as $name => $val) {
            $name = is_string($name) ? $name : $val;
            $checked = ($name == $value) ? ' selected="selected"' : '';
            $el->append("<option value=\"$name\" $checked>$val</option>");
        }

        echo $el;
    }

    public function getSliderView() {
        $slide = \tblLayersliderModel::orderBy('name')->get();
        if (\Request::ajax()) {
            return \View::make('admin.slider.ajax')->with('data', $slide);
        } else {
            return \View::make('admin.slider.view')->with('data', $slide);
        }
    }

    public function postAddSlider() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $tbl_slider = new \tblLayersliderModel();
            $check = $tbl_slider->add_slider_name($all_input['name']);
            if ($check > 1) {
                $out_put = array(
                    'check' => 1,
                    'content' => $this->getSliderView()->render()
                );
                echo json_encode($out_put);
            } else {
                $out_put = array(
                    'check' => 0,
                    'content' => 'Lỗi hệ thống'
                );
                echo json_encode($out_put);
            }
        } else {
            return \App::abort(404);
        }
    }

    public function postDeleteSlider() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $tbl_slider = new \tblLayersliderModel();
            $tbl_slider->slider_delete($all_input['id']);
            echo $this->getSliderView()->render();
        } else {
            return \App::abort(404);
        }
    }

    public function postDeleteMultiSlider() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $comment = \tblLayersliderModel::whereIn('id', $all_input['list_id'])->delete();
            return $this->getSliderView()->render();
        }
    }

    public function getSliderEdit($id) {
        $slide = \tblLayersliderModel::find($id);

        if (isset($slide)) {
            $slide->toArray();
            $slide['data'] = json_decode($slide['data'], true);
            return \View::make('admin.slider.add')->with('data', $slide['data'])->with('id_slider', $slide['id']);
        } else {
            $slide = [
                'properties' => [
                    'title' => ''
                ],
                'layers' => []
            ];

            return \View::make('admin.slider.add')->with('data', $slide);
        }
    }

    public function postSaveSlider() {
        $all_input = (\Input::all());
        $id = (int) $all_input['id'];
        $settings = $slides = $callbacks = $data = array();
        // Decode data
        parse_str($all_input['settings'], $settings);
        parse_str($all_input['callbacks'], $callbacks);
        if (!empty($all_input['slides']) && is_array($all_input['slides'])) {
            foreach ($all_input['slides'] as $key => $val) {
                $tmp = array();
                parse_str($val, $tmp);
                $slides['ls_data']['layers'][$key] = $tmp['ls_data']['layers'][$key];
            }
        }

        $data = array_merge_recursive($settings, $slides, $callbacks);
        $data = $data['ls_data'];
        $title = $data['properties']['title'];
        $slug = !empty($data['properties']['slug']) ? $data['properties']['slug'] : '';
        if (isset($data['properties']['relativeurls'])) {
            $data = $this->layerslider_convert_urls($data);
        }
// Update the slider
        if (empty($id)) {
            $obj = new \tblLayersliderModel();
            $id_r = $obj->slider_add($title, $data);
            echo json_encode(array('id_slider' => $id_r));
        } else {
            $obj = new \tblLayersliderModel();
            $id_r = $obj->slider_update($id, $title, $data);
            die(json_encode(array('status' => 'ok')));
        }
    }

    public static function layerslider_convert_urls($arr) {

        // Global BG
        if (!empty($arr['properties']['backgroundimage']) && strpos($arr['properties']['backgroundimage'], 'http://') !== false) {
            $arr['properties']['backgroundimage'] = parse_url($arr['properties']['backgroundimage'], PHP_URL_PATH);
        }

        // YourLogo img
        if (!empty($arr['properties']['yourlogo']) && strpos($arr['properties']['yourlogo'], 'http://') !== false) {
            $arr['properties']['yourlogo'] = parse_url($arr['properties']['yourlogo'], PHP_URL_PATH);
        }

        foreach ($arr['layers'] as $key => $slide) {

            // Layer BG
            if (strpos($slide['properties']['background'], 'http://') !== false) {
                $arr['layers'][$key]['properties']['background'] = parse_url($slide['properties']['background'], PHP_URL_PATH);
            }

            // Layer Thumb
            if (strpos($slide['properties']['thumbnail'], 'http://') !== false) {
                $arr['layers'][$key]['properties']['thumbnail'] = parse_url($slide['properties']['thumbnail'], PHP_URL_PATH);
            }

            // Image sublayers
            foreach ($slide['sublayers'] as $subkey => $layer) {
                if ($layer['media'] == 'img' && strpos($layer['image'], 'http://') !== false) {
                    $arr['layers'][$key]['sublayers'][$subkey]['image'] = parse_url($layer['image'], PHP_URL_PATH);
                }
            }
        }

        return $arr;
    }

}
