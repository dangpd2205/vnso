<?php

namespace ADMIN;

use View;

class MenuController extends \BaseController {

    private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

// Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý trình đơn';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
    }

    public function getView() {
        $data = \tblMenuGroupModel::all();
        if (\Request::ajax()) {
            return View::make('admin.menu.ajax')->with('data', $data);
        } else {
            return View::make('admin.menu.view')->with('data', $data);
        }
    }

    public function getEdit($id) {
        $data = \tblMenuGroupModel::find($id);
        $menu = \tblMenuModel::where('group_id', $id)->orderBy('position')->orderBy('position')->get();
        $all_show_cate = \tblShowCategoryModel::orderBy('position','asc')->get()->toArray();
        $all_news_cate = \tblNewsCategoryModel::orderBy('name')->get()->toArray();
        $all_page = \tblPageModel::leftJoin('tbl_page_lang', 'tbl_page.id', '=', 'tbl_page_lang.page_id')->where('tbl_page_lang.status', 1)->get();
		
        $video = \tblVideoModel::leftJoin('tbl_video_lang', 'tbl_video.id', '=', 'tbl_video_lang.video_id')->where('tbl_video_lang.status', 1)->get();
        if (\Request::ajax()) {
            return View::make('admin.menu.ajax_edit')->with('menu', $menu);
        } else {
            return View::make('admin.menu.edit')->with(['video' => $video,'data' => $data, 'all_page' => $all_page, 'menu' => $menu,'all_news_cate' => $all_news_cate,'all_show_cate' => $all_show_cate]);
        }
    }

    public function postAddMenuGroup() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $tbl_menu_group = new \tblMenuGroupModel();
            $tbl_menu_group->name = $all_input['name'];
            $check = $tbl_menu_group->save();
            if ($check) {
                $out_put = array(
                    'check' => 1,
                    'content' => $this->getView()->render()
                );
                echo json_encode($out_put);
            } else {
                $out_put = array(
                    'check' => 0,
                    'content' => 'Lỗi hệ thống'
                );
                echo json_encode($out_put);
            }
        } else {
            return \App::abort(404);
        }
    }

    public function postAddMenuChild() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            if ($all_input['menu_id_edit']) {
                $tbl_menu = \tblMenuModel::where('group_id', $all_input['group_id'])->where('id', $all_input['menu_id_edit'])->first();
                $tbl_menu->name = $all_input['name'];
                $tbl_menu->url = $all_input['url'];
                $tbl_menu->save();
                $out_put = array(
                    'check' => 1,
                    'content' => $this->getEdit($all_input['group_id'])->render()
                );
                echo json_encode($out_put);
            } else {
                $tbl_menu = new \tblMenuModel();
                $count = $tbl_menu->where('group_id', $all_input['group_id'])->where('parent_id', 0)->count();
                $tbl_menu->parent_id = 0;
                $tbl_menu->name = $all_input['name'];
                $tbl_menu->url = $all_input['url'];
                $tbl_menu->position = $count + 1;
                $tbl_menu->group_id = $all_input['group_id'];
                $tbl_menu->save();
                if ($tbl_menu->id > 0) {
                    $out_put = array(
                        'check' => 1,
                        'content' => $this->getEdit($all_input['group_id'])->render()
                    );
                    echo json_encode($out_put);
                }
            }
        }
    }

    public function postUpdateMenuChild() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $group_id = $all_input['group_id'];
            $data = json_decode($all_input['data']);
            $count = 1;
            foreach ($data as $item) {
                $tbl_menu = \tblMenuModel::find($item->id);
                $tbl_menu->parent_id = 0;
                $tbl_menu->position = $count;
                $tbl_menu->save();
                if (isset($item->children)) {
                    $data1 = $item->children;
                    $count1 = 1;
                    foreach ($data1 as $item1) {
                        $tbl_menu = \tblMenuModel::find($item1->id);
                        $tbl_menu->parent_id = $item->id;
                        $tbl_menu->position = $count1;
                        $tbl_menu->save();
                        if (isset($item1->children)) {
                            $data2 = $item1->children;
                            $count2 = 1;
                            foreach ($data2 as $item2) {
                                $tbl_menu = \tblMenuModel::find($item2->id);
                                $tbl_menu->parent_id = $item1->id;
                                $tbl_menu->position = $count2;
                                $tbl_menu->save();
                                $count2++;
                            }
                        }
                        $count1++;
                    }
                }
                $count++;
            }
        }
    }

    public function postDeleteMenuChild() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblMenuModel::find($all_input['id'])->delete();
            \tblMenuModel::where('parent_id', $all_input['id'])->delete();
            echo $this->getEdit($all_input['group_id'])->render();
        }
    }

    public function postEditMenuGroup() {
        if (\Request::ajax()) {
            
        } else {
            return \App::abort(404);
        }
    }

    public function postDeleteMenuGroup() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblMenuGroupModel::find($all_input['id'])->delete();
            echo $this->getView()->render();
        } else {
            return \App::abort(404);
        }
    }

    public function postRenameMenuGroup() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $tbl_menu_group = \tblMenuGroupModel::find($all_input['id']);
            $tbl_menu_group->name = $all_input['name'];
            $tbl_menu_group->save();
        } else {
            return \App::abort(404);
        }
    }

    public function postDeleteMultiMenuGroup() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $munu_g = \tblMenuGroupModel::whereIn('id', $all_input['list_id'])->delete();
            $menu = \tblMenuModel::whereIn('group_id', $all_input['list_id'])->delete();
            return $this->getView()->render();
        }
    }

}
