<?php

namespace ADMIN;

class GalleryController extends \BaseController {
	private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý video';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }


    public function getView() {
		
        \Session::forget('gallery_status_key');
        \Session::forget('gallery_search_key');
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $obj_news = new \tblGalleryModel();
            $obj_news->resolveConnection()->getPaginator()->setBaseUrl(\URL::action('\ADMIN\GalleryController@getView'));
            $data_lang = $obj_news->join('tbl_gallery_lang', 'tbl_gallery.id', '=', 'tbl_gallery_lang.gallery_id')->where('tbl_gallery_lang.lang_id', $this->d_lang)->orderBy('tbl_gallery.id', 'desc')->where('tbl_gallery_lang.status',1)->paginate(10);
            
            return \View::make('admin.gallery.ajax')->with('data', $data_lang);
        } else {
            $data  = \tblGalleryModel::join('tbl_gallery_lang', 'tbl_gallery.id', '=', 'tbl_gallery_lang.gallery_id')->where('tbl_gallery_lang.lang_id', $this->d_lang)->orderBy('tbl_gallery.id', 'desc')->where('tbl_gallery_lang.status',1)->paginate(10);
                
            return \View::make('admin.gallery.view')->with('data', $data);
        }

    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('gallery_status_key');
        \Session::forget('gallery_search_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword='null';
        }
        \Session::set('gallery_search_key', $keyword);
        return \Redirect::action('\ADMIN\GalleryController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblGalleryModel::join('tbl_gallery_lang', 'tbl_gallery.id', '=', 'tbl_gallery_lang.gallery_id')->where('tbl_gallery_lang.lang_id', $this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_gallery_lang.name', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_gallery.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.gallery.ajax')->with('data', $data_lang);
    }

    public function postFilter(){
        
        $status = '';
        \Session::forget('gallery_status_key');
        \Session::forget('gallery_search_key');
        
        if (\Input::has('news_status_filter') || \Input::get('news_status_filter') != '') {
            $status=\Input::get('news_status_filter');
        }else{
            $status='null';
        }
        \Session::set('gallery_status_key', $status);
        return \Redirect::action('\ADMIN\GalleryController@getFilter', array($status));
    }

    public function getFilter($status=''){
        $sql_data = \tblGalleryModel::join('tbl_gallery_lang', 'tbl_gallery.id', '=', 'tbl_gallery_lang.gallery_id')->where('tbl_gallery_lang.lang_id', $this->d_lang);
       
        if($status!='null'){
            $sql_data->where('tbl_gallery_lang.status', $status);
        }
        $sql_data->orderBy('tbl_gallery.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.gallery.ajax')->with('data', $data_lang);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\GalleryController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = \tblGalleryModel::join('tbl_gallery_lang', 'tbl_gallery.id', '=', 'tbl_gallery_lang.gallery_id')->where('tbl_gallery_lang.lang_id', $this->d_lang);
        
        if(\Session::has('gallery_status_key') && \Session::get('gallery_status_key')!='null'){
            $sql_data->where('tbl_gallery_lang.status', \Session::get('gallery_status_key'));
        }else{
            $sql_data->where('tbl_gallery_lang.status',1);
        }
        if(\Session::has('gallery_search_key') && \Session::get('gallery_search_key')!='null'){
            $keyword=\Session::get('gallery_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('tbl_gallery_lang.name', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->orderBy('tbl_gallery.id', 'desc')->paginate($page);
        return \View::make('admin.gallery.ajax')->with('data', $data_lang);
    }

    public function getAdd() {
        return \View::make('admin.gallery.add');
    }

    public function postAdd() {
        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();

        $log = [];
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){

            $tbl_news = new \tblGalleryModel();       
			$tbl_news->images = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            $tbl_news->avatar = ($all_input['avatar'] != '') ? $all_input['avatar'] : '';			if(\Input::has('pin')){				$tbl_news->pin=1;			}else{				$tbl_news->pin=0;			}
            $tbl_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    
                    $tbl_detail_news = new \tblGalleryLangModel();
                    $tbl_detail_news->gallery_id = $tbl_news->id;
                    $tbl_detail_news->lang_id = $item_lang->id;
                    $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';                    
                    $tbl_detail_news->status = 1;
                    $tbl_detail_news->save();

                }
            }
            return \Redirect::action('\ADMIN\GalleryController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }
    
    public function getEdit($lang, $id) {
        $data_news = \tblGalleryModel::join('tbl_gallery_lang', 'tbl_gallery.id', '=', 'tbl_gallery_lang.gallery_id')
                    ->leftJoin('tbl_lang', 'tbl_gallery_lang.lang_id', '=', 'tbl_lang.id')
                    ->where('tbl_gallery.id', $id)
                    ->select('tbl_lang.id as langid','tbl_gallery.images','tbl_gallery.avatar','tbl_gallery_lang.*','tbl_gallery.pin')
                    ->get();
        $lang_code = \tblLangModel::where('id',$lang)->first();
        if (count($data_news) > 0) {           
            return \View::make('admin.gallery.edit')->with('lang_code', $lang_code)->with('data_news', $data_news);            
        } else {
            return \Redirect::action('\ADMIN\GalleryController@getView');
        }
    }

    public function postEdit() {

        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){
            $row_news = \tblGalleryModel::find($all_input['id_gallery']);
            if ($all_input['image_hidden'] != $row_news->images) {
                $row_news->images = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            }
            $row_news->avatar = ($all_input['avatar'] != '') ? $all_input['avatar'] : '';			if(\Input::has('pin')){				$row_news->pin=1;			}else{				$row_news->pin=0;			}
            $row_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    $check = \tblGalleryLangModel::where('gallery_id', $all_input['id_gallery'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                    // neu co roi thi update
                    if(count($check)>0){                        
                        $tbl_detail_news = \tblGalleryLangModel::where('gallery_id', $all_input['id_gallery'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->first();					
                        $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';	                      
                        $tbl_detail_news->save();
                    }else{
                       
                        $tbl_detail_news = new \tblGalleryLangModel();
                        $tbl_detail_news->gallery_id = $all_input['id_gallery'];
						$tbl_detail_news->lang_id = $item_lang->id; 
						$tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';                    
						$tbl_detail_news->status = 1;
                        $tbl_detail_news->save();
                    }
                }   
            }
    	
            return \Redirect::action('\ADMIN\GalleryController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }

    public function postDelete() {

        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblGalleryLangModel::where('gallery_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 2));   
            
            return $this->getView()->render();
        }
    }

    public function postPostConfirm() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
           \tblGalleryLangModel::where('gallery_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 1));
           
            return $this->getView()->render();
        }
    }

}
