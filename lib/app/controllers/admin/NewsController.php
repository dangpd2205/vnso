<?php

namespace ADMIN;

class NewsController extends \BaseController {
	private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý tin tức';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }
    public function postQuickAddNewsCategory() {
        if (\Request::ajax()) {
            foreach ($this->listLang as $item) {
                $name = \Input::get('product_add_cat_name_' . $item->code);
                $parent = \Input::get('product_add_cat_parent_' . $item->code);
                $des = \Input::get('product_add_cat_description_' . $item->code);
                $tblNewsCategoryModel = new \tblNewsCategoryModel();
                $tblNewsCategoryModel->name = $name;
                $tblNewsCategoryModel->parent = $parent;
                $tblNewsCategoryModel->lang_id = $item->id;
                $tblNewsCategoryModel->description = $des;
                $tblNewsCategoryModel->slug = \Tree::gen_slug($name);
                $tblNewsCategoryModel->save();
            }
            $out_put = [];
            foreach ($this->listLang as $item1) {
                $array = \tblNewsCategoryModel::where('lang_id', $item1->id)->orderBy('tbl_news_category.name')->get();
                $out_put[] = array(
                    'check' => $item1->code,
                    'content' => \Tree::treereturn($array, [], $item1->id)
                );
            }
            echo json_encode($out_put);
        }
    }
    /* danh muc tin tuc */
    public function getCategoryView() {
		$all_category = \tblNewsCategoryModel::orderBy('position','asc')->get();
        $this->titlepage = 'Quản lý nhóm tin tức';
        $tblNewsCategoryModel = \tblNewsCategoryModel::orderBy('position','asc')->get();
        return \View::make('admin.news.category.add_category')->with('data', $tblNewsCategoryModel)->with('all_category', $all_category);
    }
	
	public function getAddCate(){
		$all_category = \tblNewsCategoryModel::orderBy('name')->get();
        return \View::make('admin.news.category.add_cate')->with('all_category', $all_category);
	}
	
	public function postAddCate(){
		$all_input = \Input::all();
		foreach ($this->listLang as $item) {
			if(\Input::get('cate_name_' . $item->code)!=''){
				
                $tbl_seo = new \tblSeoModel();
				$tbl_seo->title = isset($all_input['seo_title_' . $item->code]) ? $all_input['seo_title_' . $item->code] : '';
				$tbl_seo->description = isset($all_input['seo_description_' . $item->code]) ? $all_input['seo_description_' . $item->code] : '';
				$tbl_seo->keyword = isset($all_input['seo_keyword_' . $item->code]) ? $all_input['seo_keyword_' . $item->code] : '';
				$tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
				$tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
				$tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'None';
				$tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item->code]) ? $all_input['seo_f_title_' . $item->code] : '';
				$tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item->code]) ? $all_input['seo_f_description_' . $item->code] : '';
				$tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item->code]) ? $all_input['seo_f_image_' . $item->code] : '';
				$tbl_seo->g_title = isset($all_input['seo_g_title_' . $item->code]) ? $all_input['seo_g_title_' . $item->code] : '';
				$tbl_seo->g_description = isset($all_input['seo_g_description_' . $item->code]) ? $all_input['seo_g_description_' . $item->code] : '';
				$tbl_seo->g_image = isset($all_input['seo_g_image_' . $item->code]) ? $all_input['seo_g_image_' . $item->code] : '';
				$tbl_seo->save();
				$name = strip_tags(\Input::get('cate_name_' . $item->code));
				$parent = \Input::get('news_add_cat_parent_' . $item->code);
				$des = strip_tags(\Input::get('news_add_cat_description_' . $item->code));
				$tblCateNewsModel = new \tblNewsCategoryModel();
				$tblCateNewsModel->name = $name;
				$tblCateNewsModel->description = $des;
				$tblCateNewsModel->parent = $parent;
				$tblCateNewsModel->lang_id = $item->id;
				$tblCateNewsModel->seo_id = $tbl_seo->id;				
				$check = \tblNewsCategoryModel::where('slug',\Tree::gen_slug($name))->first();
				if(count($check)>0){
					$maxid = \tblNewsCategoryModel::count();
					$tblCateNewsModel->slug = \Tree::gen_slug($name).($maxid+1);
				}else{
					$tblCateNewsModel->slug = \Tree::gen_slug($name);
				}				
				$tblCateNewsModel->save();
			}
		}
		\Session::flash('alert_success', \Lang::get('admin/message.msg_add.success'));
		return \Redirect::action('\ADMIN\NewsController@getAddCate');
	}
	
	public function getEditCate($id){
		$dataedit = \tblNewsCategoryModel::find($id);
		$seo = \tblSeoModel::find($dataedit->seo_id);
		$all_category = \tblNewsCategoryModel::orderBy('name')->get();
		return \View::make('admin.news.category.edit_cate')->with('dataedit', $dataedit)->with('seo', $seo)->with('all_category', $all_category);
	}
	
	public function postEditCate(){
		$all_input = \Input::all();
		if(\Input::get('cate_name')!=''){			
			$maxid = \tblNewsCategoryModel::select(\DB::raw("max(id) as maxid"))->first();
			$check = \tblNewsCategoryModel::where('slug',\Tree::gen_slug(\Input::get('cate_name')))->where('id','!=',\Input::get('id_cate'))->first();
			$tblCateNewsModel = \tblNewsCategoryModel::find(\Input::get('id_cate'));
			$tblCateNewsModel->name = \Input::get('cate_name');
			$tblCateNewsModel->description = \Input::get('cate_des');
			if(count($check)>0){
				$tblCateNewsModel->slug = \Tree::gen_slug(\Input::get('cate_name')).$maxid->maxid;
			}else{
				$tblCateNewsModel->slug = \Tree::gen_slug(\Input::get('cate_name'));
			}
			$tblCateNewsModel->save();
			$tblSeoModel = \tblSeoModel::find(\Input::get('seo_id'));
			if(count($tblSeoModel)>0){
				$tblSeoModel->title = isset($all_input['seo_title']) ? $all_input['seo_title'] : '';
				$tblSeoModel->description = isset($all_input['seo_description']) ? $all_input['seo_description'] : '';
				$tblSeoModel->keyword = isset($all_input['seo_keyword']) ? $all_input['seo_keyword'] : '';
				$tblSeoModel->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
				$tblSeoModel->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
				$tblSeoModel->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'None';
				$tblSeoModel->fb_title=\Input::get('seo_f_title');
				$tblSeoModel->fb_description=\Input::get('seo_f_description');
				$tblSeoModel->fb_image=\Input::get('seo_f_image');
				$tblSeoModel->g_title=\Input::get('seo_g_title');
				$tblSeoModel->g_description=\Input::get('seo_g_description');
				$tblSeoModel->g_image=\Input::get('seo_g_image');
				$tblSeoModel->save();
			}else{
				$tbl_seo = new \tblSeoModel();
				$tbl_seo->title = isset($all_input['seo_title']) ? $all_input['seo_title'] : '';
				$tbl_seo->description = isset($all_input['seo_description_']) ? $all_input['seo_description'] : '';
				$tbl_seo->keyword = isset($all_input['seo_keyword_']) ? $all_input['seo_keyword'] : '';
				$tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
				$tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
				$tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'None';
				$tbl_seo->fb_title = isset($all_input['seo_f_title']) ? $all_input['seo_f_title'] : '';
				$tbl_seo->fb_description = isset($all_input['seo_f_description']) ? $all_input['seo_f_description'] : '';
				$tbl_seo->fb_image = isset($all_input['seo_f_image']) ? $all_input['seo_f_image'] : '';
				$tbl_seo->g_title = isset($all_input['seo_g_title']) ? $all_input['seo_g_title'] : '';
				$tbl_seo->g_description = isset($all_input['seo_g_description']) ? $all_input['seo_g_description'] : '';
				$tbl_seo->g_image = isset($all_input['seo_g_image']) ? $all_input['seo_g_image'] : '';
				$tbl_seo->save();
				$tblCateNewsModel = \tblNewsCategoryModel::find(\Input::get('id_cate'));
				$tblCateNewsModel->seo_id = $tbl_seo->id;
				$tblCateNewsModel->save();
			}
		}
		\Session::flash('alert_success', \Lang::get('admin/message.msg_update.success'));
		return \Redirect::action('\ADMIN\NewsController@getCategoryView');
	}

    public function postCategoryAdd() {
        if (\Request::ajax()) {
            $product_cate_name = "";

            foreach ($this->listLang as $item) {
                $product_cate_name = \Input::get('product_add_cat_name_' . $item->code);
                $count = \tblNewsCategoryModel::where('name', 'LIKE', $product_cate_name . '%')->count();
                if ($count > 0) {
                    $product_cate_name = trim($product_cate_name . ' ' . $count);
                }
                $tblNewsCategoryModel = new \tblNewsCategoryModel();
                $tblNewsCategoryModel->name = \Input::get('product_add_cat_name_' . $item->code);
                $tblNewsCategoryModel->slug = \Tree::gen_slug($product_cate_name);
                $tblNewsCategoryModel->parent = \Input::get('product_add_cat_parent_' . $item->code);
                $tblNewsCategoryModel->description = \Input::get('product_add_cat_description_' . $item->code);
                $tblNewsCategoryModel->lang_id = $item->id;
                if ($tblNewsCategoryModel->name != "") {
                    $tblNewsCategoryModel->save();
                }
            }

            if ($product_cate_name != "") {
                $out_put = array(
                    'check' => 'reload',
                    'content' => ''
                );
            } else {
                $out_put = array(
                    'check' => '0',
                    'content' => '<li>Trường tên nhóm tin tức không được để trống!</li>'
                );
            }
            echo json_encode($out_put);
        }
    }

    public function postCategoryEdit() {
        if (\Request::ajax()) {
            $rules = array(
                "product_add_cat_name" => "required"
            );
            $validate = \Validator::make(\Input::all(), $rules, array(), \Lang::get('admin/attributes.tbl_category'));
            if (!$validate->fails()) {
                $name = \Input::get('product_add_cat_name');
                $tblNewsCategoryModel = \tblNewsCategoryModel::where('id', '=', \Input::get('id'))->first();
                $count = \tblNewsCategoryModel::where('name', 'LIKE', $name . '%')->count();
                if ($count > 0) {
                    $name = trim($name. ' ' . $count);
                }
                $tblNewsCategoryModel->name = \Input::get('product_add_cat_name');
                if($tblNewsCategoryModel->name != \Input::get('product_add_cat_name')){
                        $tblNewsCategoryModel->slug = \Tree::gen_slug($name);
                }
                $tblNewsCategoryModel->description = \Input::get('product_add_cat_description');
                $tblNewsCategoryModel->save();

                $out_put = array(
                    'check' => 'reload',
                    'content' => ''
                );
            } else {
                $out_put = array(
                    'check' => '0',
                    'content' => $validate->messages()->all('<li>:message</li>')
                );
            }
            echo json_encode($out_put);
        }
    }

    public function postDeleteCategory() {
        if (\Request::ajax()) {
            $product_cate_con = \tblNewsCategoryModel::where('parent', '=', \Input::get('id'))->get();
            foreach ($product_cate_con as $item_delete) {
                $product_cate_con_con = \tblNewsCategoryModel::where('parent', '=', $item_delete->id)->get();
                foreach ($product_cate_con_con as $item_delete_con) {
                    \tblNewsCategoryModel::where('id', '=', $item_delete_con->id)->delete();
                }
                \tblNewsCategoryModel::where('id', '=', $item_delete->id)->delete();
            }
            \tblNewsCategoryModel::where('id', '=', \Input::get('id'))->delete();
        }
    }

    public function postUpdatePosCategory() {
        $data = json_decode(\Input::get('data'));
        $count = 1;
        foreach ($data as $item) {
            $cate = \tblNewsCategoryModel::find($item->id);
            $cate->parent=0;
            $cate->position=$count;
            $cate->save();
            
            if (isset($item->children)) {
                $data1 = $item->children;
                $count1 = 1;
                foreach ($data1 as $item1) {
                    $cate = \tblNewsCategoryModel::find($item1->id);
                    $cate->parent=$item->id;
                    $cate->position=$count1;
                    $cate->save();
                    if (isset($item1->children)) {
                        $data2 = $item1->children;
                        $count2 = 1;
                        foreach ($data2 as $item2) {
                            $cate = \tblNewsCategoryModel::find($item2->id);
                            $cate->parent=$item1->id;
                            $cate->position=$count2;
                            $cate->save();
                            $count2++;
                        }
                    }
                    $count1++;
                }
            }
            $count++;
        }
    }

    /* tin tuc */
    public function getView() {
        \Session::forget('news_cate_key');
        \Session::forget('news_status_key');
        \Session::forget('news_search_key');
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $obj_news = new \tblNewsModel();
            $obj_news->resolveConnection()->getPaginator()->setBaseUrl(\URL::action('\ADMIN\NewsController@getView'));
            $data_lang = $obj_news->join('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')->where('tbl_news_detail_lang.lang_id', $this->d_lang)->orderBy('tbl_news.id', 'desc')->where('tbl_news_detail_lang.status', '!=', 2)->paginate(10);
            $list_category = \tblNewsCategoryModel::where('lang_id',$this->d_lang)->orderBy('name')->get();
            return \View::make('admin.news.ajax')->with('data', $data_lang)->with('list_category', $list_category);
        } else {
            $list_category = \tblNewsCategoryModel::where('lang_id',$this->d_lang)->orderBy('name')->get();
            $data  = \tblNewsModel::join('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')->where('tbl_news_detail_lang.lang_id',$this->d_lang)->where('tbl_news_detail_lang.status', '!=', 2)->orderBy('tbl_news.id', 'desc')->paginate(10);
                
            return \View::make('admin.news.view')->with('data', $data)->with('list_category', $list_category);
        }

    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('news_cate_key');
        \Session::forget('news_status_key');
        \Session::forget('news_search_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword='null';
        }
        \Session::set('news_search_key', $keyword);
        return \Redirect::action('\ADMIN\NewsController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblNewsModel::join('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')->where('tbl_news_detail_lang.lang_id',$this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_news_detail_lang.news_title', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('tbl_news_detail_lang.news_excerpt', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('tbl_news_detail_lang.news_content', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_news.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        $list_category = \tblNewsCategoryModel::where('lang_id',$this->d_lang)->orderBy('name')->get();
        return \View::make('admin.news.ajax')->with('data', $data_lang)->with('list_category', $list_category);
    }

    public function postFilter(){
        $cate = '';
        $status = '';
        \Session::forget('news_cate_key');
        \Session::forget('news_status_key');
        \Session::forget('news_search_key');
        if (\Input::has('news_cat_filter') || \Input::get('news_cat_filter') != '') {
            $cate=\Input::get('news_cat_filter');
        }else{
            $cate='null';
        }
        if (\Input::has('news_status_filter') || \Input::get('news_status_filter') != '') {
            $status=\Input::get('news_status_filter');
        }else{
            $status='null';
        }
        \Session::set('news_cate_key', $cate);
        \Session::set('news_status_key', $status);
        return \Redirect::action('\ADMIN\NewsController@getFilter', array($cate,$status));
    }

    public function getFilter($cate='',$status=''){
        $sql_data = \tblNewsModel::join('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')->where('tbl_news_detail_lang.lang_id', $this->d_lang);
        if($cate!='null'){
            $cat_r = \tblNewsCategoryModel::find($cate);
            $list_cate = [];
            if ($cat_r->parent == 0) {
                $list_cate[] = $cat_r->id;
                $cat_r_c = \tblNewsCategoryModel::where('parent', $cat_r->id)->get();
                foreach ($cat_r_c as $item) {
                    $list_cate[] = $item->id;
                }
            } else {
                $list_cate[] = $cat_r->id;
            }
            $sql_data->whereIn('tbl_news.id', \tblNewsViewsModel::select('news_id')->whereIn('cat_id', $list_cate)->get()->toArray());
        }
        if($status!='null'){
            $sql_data->where('tbl_news_detail_lang.status', $status);
        }
        $sql_data->orderBy('tbl_news.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        $list_category = \tblNewsCategoryModel::where('lang_id',$this->d_lang)->orderBy('name')->get();
        return \View::make('admin.news.ajax')->with('data', $data_lang)->with('list_category', $list_category);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\NewsController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = \tblNewsModel::join('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')->where('tbl_news_detail_lang.lang_id', $this->d_lang);
        if(\Session::has('news_cate_key') && \Session::get('news_cate_key')!='null'){
            $cat_r = \tblNewsCategoryModel::find(\Session::get('news_cate_key'));
            $list_cate = [];
            if ($cat_r->parent == 0) {
                $list_cate[] = $cat_r->id;
                $cat_r_c = \tblNewsCategoryModel::where('parent', $cat_r->id)->get();
                foreach ($cat_r_c as $item) {
                    $list_cate[] = $item->id;
                }
            } else {
                $list_cate[] = $cat_r->id;
            }
            $sql_data->whereIn('tbl_news.id', \tblNewsViewsModel::select('news_id')->whereIn('cat_id', $list_cate)->get()->toArray());
        }
        if(\Session::has('news_status_key') && \Session::get('news_status_key')!='null'){
            $sql_data->where('tbl_news_detail_lang.status', \Session::get('news_status_key'));
        }else{
            $sql_data->where('tbl_news_detail_lang.status', '!=',2);
        }
        if(\Session::has('news_search_key') && \Session::get('news_search_key')!='null'){
            $keyword=\Session::get('news_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('tbl_news_detail_lang.news_title', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('tbl_news_detail_lang.news_excerpt', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('tbl_news_detail_lang.news_content', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->orderBy('tbl_news.id', 'desc')->paginate($page);
        $list_category = \tblNewsCategoryModel::orderBy('name')->get();
        return \View::make('admin.news.ajax')->with('data', $data_lang)->with('list_category', $list_category);
    }

    public function getAdd() {
        $all_category = \tblNewsCategoryModel::orderBy('name')->get();
        return \View::make('admin.news.add')->with('all_category', $all_category);
    }

    public function postAdd() {
        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();

        $log = [];
        $all_input = \Input::all();
		
        if(\Input::get('news_title_'.$tblLangModel->code)){

            $tbl_news = new \tblNewsModel();
            $tbl_news->images = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            $tbl_news->user_id = \Auth::user()->id;
            $tbl_news->time_post = ($all_input['news_time'] != '') ? strtotime($all_input['news_time']) : time();
           
            $tbl_news->status = 0;
            $tbl_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['news_title_' . $item_lang->code]) {
                    $tbl_seo = new \tblSeoModel();
                    $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                    $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                    $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                    $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                    $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                    $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                    $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                    $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                    $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                    $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                    $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                    $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                    $tbl_seo->save();
                    $tbl_detail_news = new \tblNewsDetailLangModel();
                    $tbl_detail_news->news_id = $tbl_news->id;
                    $tbl_detail_news->lang_id = $item_lang->id;
                    $tbl_detail_news->seo_id = $tbl_seo->id;
                    $tbl_detail_news->news_title = ($all_input['news_title_' . $item_lang->code] != '') ? strip_tags($all_input['news_title_' . $item_lang->code]) : '';
                    $tbl_detail_news->news_excerpt = ($all_input['news_excerpt_' . $item_lang->code] != '') ? strip_tags($all_input['news_excerpt_' . $item_lang->code]) : '';
                    $tbl_detail_news->news_content = ($all_input['news_content_' . $item_lang->code] != '') ? $all_input['news_content_' . $item_lang->code] : '';
                    $tbl_detail_news->news_tags = ($all_input['news_tag_' . $item_lang->code] != '') ? $all_input['news_tag_' . $item_lang->code] : '';
                    $tbl_detail_news->news_slug = \Tree::gen_slug($all_input['news_title_' . $item_lang->code]) . '-' . $tbl_news->id;
                    $tbl_detail_news->source = ($all_input['source_' . $item_lang->code] != '') ? $all_input['source_' . $item_lang->code] : '';
                    $tbl_detail_news->status = 1;
                    $tbl_detail_news->save();
					
					/* bang tag */
					if(\Input::get('news_tag_' . $item_lang->code)!=''){
						foreach(explode(',',\Input::get('news_tag_' . $item_lang->code)) as $i_tag){
							$check = \tblTagsModel::where('slug', \Tree::gen_slug($i_tag))->first();
							if(count($check)<=0){
								$tblTagModel = new \tblTagsModel();
								$tblTagModel->name = $i_tag;
								$tblTagModel->slug = \Tree::gen_slug($i_tag);
								$tblTagModel->save();
								$tblTagViewModel = new \tblTagsRelationshipModel();
								$tblTagViewModel->tags_id = $tblTagModel->id;
								$tblTagViewModel->post_id = $tbl_detail_news->id;
								$tblTagViewModel->type = 2;
								$tblTagViewModel->save();
							}else{
								$tblTagViewModel = new \tblTagsRelationshipModel();
								$tblTagViewModel->tags_id = $check->id;
								$tblTagViewModel->post_id = $tbl_detail_news->id;
								$tblTagViewModel->type = 2;
								$tblTagViewModel->save();
							}
						}
					}
					
					$cat_list = isset($all_input['cat_id_' . $item_lang->id]) ? $all_input['cat_id_' . $item_lang->id] : '';
                    if (count($cat_list) > 0 && $cat_list != '') {
                        foreach ($cat_list as $item) {
                            $tbl_news_view = new \tblNewsViewsModel();
                            $tbl_news_view->news_id = $tbl_news->id;
                            $tbl_news_view->cat_id = $item;
                            $tbl_news_view->lang_id = $item_lang->id;
                            $tbl_news_view->save();
                        }
                    }
					

               

                }
            }
            return \Redirect::action('\ADMIN\NewsController@getAdd');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }
    
    public function postQuickAddCategory() {
        
    }
    public function getEdit($lang, $id) {
        $data_news = \tblNewsModel::join('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')
                    ->leftJoin('tbl_lang', 'tbl_news_detail_lang.lang_id', '=', 'tbl_lang.id')
                    ->leftJoin('tbl_seo', 'tbl_news_detail_lang.seo_id', '=', 'tbl_seo.id')
                    ->where('tbl_news.id', $id)
                    ->select('tbl_lang.id as langid','tbl_news.images','tbl_news.type','tbl_news.user_id','tbl_news.time_post','tbl_news.comment_status','tbl_news.ping_status','tbl_news.view','tbl_news_detail_lang.*','tbl_seo.id as seoid','tbl_seo.title','tbl_seo.description as seodesc','tbl_seo.keyword','tbl_seo.robots_index','tbl_seo.robots_follow','tbl_seo.robots_advanced','tbl_seo.fb_title','tbl_seo.fb_description','tbl_seo.fb_image','tbl_seo.g_title','tbl_seo.g_description','tbl_seo.g_image')
                    ->get();
        $lang_code = \tblLangModel::where('id',$lang)->first();

        if (count($data_news) > 0) {
            $cat_select = \tblNewsViewsModel::select('cat_id')->where('lang_id', $lang)->where('news_id', $id)->get();
            $list_selected = [];
            foreach ($cat_select as $list_catvalue) {
                $list_selected[] = $list_catvalue->cat_id;
            }
            $all_category = \tblNewsCategoryModel::orderBy('name')->get();
		
            return \View::make('admin.news.edit')->with('lang_code', $lang_code)->with('list_selected', $list_selected)->with('data_news', $data_news)->with('all_category', $all_category);
            
        } else {
            return \Redirect::action('\ADMIN\NewsController@getView');
        }
    }

    public function postEdit() {

        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();
        $all_input = \Input::all();
		
        if(\Input::get('news_title_'.$tblLangModel->code)){
            $row_news = \tblNewsModel::find($all_input['id_news']);
            if ($all_input['image_hidden'] != $row_news->images) {
                $row_news->images = ($all_input['image_hidden'] != '') ? $all_input['image_hidden'] : '';
            }
            if (strtotime($all_input['news_time']) != $row_news->time_post && $all_input['news_time'] != '') {
               
                $row_news->time_post = ($all_input['news_time'] != '') ? strtotime($all_input['news_time']) : time();
            }
            $row_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['news_title_' . $item_lang->code]) {
                    $check = \tblNewsDetailLangModel::where('news_id', $all_input['id_news'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                    // neu co roi thi update
                    if(count($check)>0){
                        $tbl_seo = \tblSeoModel::find($check->seo_id);
                        if(count($tbl_seo)>0){
                            $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                            $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                            $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                            $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                            $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                            $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                            $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                            $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                            $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                            $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                            $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                            $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                            $tbl_seo->save();
                            $tbl_detail_news = \tblNewsDetailLangModel::where('news_id', $all_input['id_news'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->where('seo_id', $all_input['seo_id_'. $item_lang->code])->first();
                            if($tbl_detail_news->news_title != $all_input['news_title_' . $item_lang->code]){
                                $tbl_detail_news->news_slug = \Tree::gen_slug($all_input['news_title_' . $item_lang->code]) . '-' . $all_input['id_news'];                        
                            }
                            $tbl_detail_news->news_title = ($all_input['news_title_' . $item_lang->code] != '') ? strip_tags($all_input['news_title_' . $item_lang->code]) : '';
                            
                            $tbl_detail_news->news_excerpt = ($all_input['news_excerpt_' . $item_lang->code] != '') ? strip_tags($all_input['news_excerpt_' . $item_lang->code]) : '';
                            $tbl_detail_news->news_content = ($all_input['news_content_' . $item_lang->code] != '') ? $all_input['news_content_' . $item_lang->code] : '';
                            $tbl_detail_news->news_tags = ($all_input['news_tag_' . $item_lang->code] != '') ? $all_input['news_tag_' . $item_lang->code] : '';
                            $tbl_detail_news->source = ($all_input['source_' . $item_lang->code] != '') ? $all_input['source_' . $item_lang->code] : '';
                            $tbl_detail_news->save();
                        }else{
                            $tblSeoModel = new \tblSeoModel();
                            $tblSeoModel->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                            $tblSeoModel->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                            $tblSeoModel->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                            $tblSeoModel->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                            $tblSeoModel->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                            $tblSeoModel->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                            $tblSeoModel->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                            $tblSeoModel->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                            $tblSeoModel->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                            $tblSeoModel->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                            $tblSeoModel->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                            $tblSeoModel->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                            $tblSeoModel->save();
                            $tbl_detail_news = \tblNewsDetailLangModel::where('news_id', $all_input['id_news'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->first();
                            if($tbl_detail_news->news_title != $all_input['news_title_' . $item_lang->code]){
                                $tbl_detail_news->news_slug = \Tree::gen_slug($all_input['news_title_' . $item_lang->code]) . '-' . $all_input['id_news'];                        
                            }
                            $tbl_detail_news->seo_id = $tblSeoModel->id;
                            $tbl_detail_news->news_title = ($all_input['news_title_' . $item_lang->code] != '') ? strip_tags($all_input['news_title_' . $item_lang->code]) : '';
                            
                            $tbl_detail_news->news_excerpt = ($all_input['news_excerpt_' . $item_lang->code] != '') ? strip_tags($all_input['news_excerpt_' . $item_lang->code]) : '';
                            $tbl_detail_news->news_content = ($all_input['news_content_' . $item_lang->code] != '') ? $all_input['news_content_' . $item_lang->code] : '';
                            $tbl_detail_news->news_tags = ($all_input['news_tag_' . $item_lang->code] != '') ? $all_input['news_tag_' . $item_lang->code] : '';
                            $tbl_detail_news->source = ($all_input['source_' . $item_lang->code] != '') ? $all_input['source_' . $item_lang->code] : '';
                            $tbl_detail_news->save();
                        }
                       
                    }else{
                        $tbl_seo = new \tblSeoModel();
                        $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                        $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                        $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                        $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                        $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                        $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                        $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                        $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                        $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                        $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                        $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                        $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                        $tbl_seo->save();
                        $tbl_detail_news = new \tblNewsDetailLangModel();
                        $tbl_detail_news->news_id = $all_input['id_news'];
                        $tbl_detail_news->lang_id = $item_lang->id;
                        $tbl_detail_news->seo_id = $tbl_seo->id;
                        $tbl_detail_news->news_title = ($all_input['news_title_' . $item_lang->code] != '') ? strip_tags($all_input['news_title_' . $item_lang->code]) : '';
                        $tbl_detail_news->news_excerpt = ($all_input['news_excerpt_' . $item_lang->code] != '') ? strip_tags($all_input['news_excerpt_' . $item_lang->code]) : '';
                        $tbl_detail_news->news_content = ($all_input['news_content_' . $item_lang->code] != '') ? $all_input['news_content_' . $item_lang->code] : '';
                        $tbl_detail_news->news_tags = ($all_input['news_tag_' . $item_lang->code] != '') ? $all_input['news_tag_' . $item_lang->code] : '';
                        $tbl_detail_news->news_slug = \Tree::gen_slug($all_input['news_title_' . $item_lang->code]) . '-' . $all_input['id_news'];
                        $tbl_detail_news->source = ($all_input['source_' . $item_lang->code] != '') ? $all_input['source_' . $item_lang->code] : '';
                        $tbl_detail_news->status = 1;
                        $tbl_detail_news->save();
                    }

                    /* bang tag */
                    \tblTagsRelationshipModel::where('post_id',$tbl_detail_news->id)->where('type',2)->delete();
                    if(\Input::get('news_tag_' . $item_lang->code)!=''){
                        foreach(explode(',',\Input::get('news_tag_' . $item_lang->code)) as $i_tag){
                            $check = \tblTagsModel::where('slug', \Tree::gen_slug($i_tag))->first();
                            if(count($check)<=0){
                                $tblTagModel = new \tblTagsModel();
                                $tblTagModel->name = $i_tag;
                                $tblTagModel->slug = \Tree::gen_slug($i_tag);
                                $tblTagModel->save();
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $tblTagModel->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 2;
                                $tblTagViewModel->save();
                            }else{
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $check->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 2;
                                $tblTagViewModel->save();
                            }
                        }
                    }

                    $cat_list = isset($all_input['cat_id_' . $item_lang->id]) ? $all_input['cat_id_' . $item_lang->id] : '';
                    if (count($cat_list) > 0 && $cat_list != '') {
                        \tblNewsViewsModel::where('news_id',$all_input['id_news'])->where('lang_id',$item_lang->id)->delete();
                        foreach ($cat_list as $item) {
                            $tbl_news_view = new \tblNewsViewsModel();
                            $tbl_news_view->news_id = $all_input['id_news'];
                            $tbl_news_view->cat_id = $item;
                            $tbl_news_view->lang_id = $item_lang->id;
                            $tbl_news_view->save();
                        }
                    }

                }   
            }
    	
            return \Redirect::action('\ADMIN\NewsController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }

    public function postDelete() {

        if (\Request::ajax()) {
            $all_input = \Input::all();
            $tblNewsDetailLangModel = \tblNewsDetailLangModel::where('news_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 2));
            return $this->getView()->render();
        }
    }

    public function postPostConfirm() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblNewsDetailLangModel::where('news_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 1));
            return $this->getView()->render();
        }
    }

   

}
