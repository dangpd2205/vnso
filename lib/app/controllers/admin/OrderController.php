<?php

namespace ADMIN;

class OrderController extends \BaseController {

    private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý đơn vé';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
    }

    public function postPrintTicket(){
        $order = \tblOrderModel::where('id',\Input::get('id'))->first();
        $data_order = \tblOrderModel::where('id',\Input::get('id'))->first();
        $data_detail = \tblOrderDetailModel::where('order_id',$order->id)->get();
        $show = \tblShowModel::leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')
                    ->leftJoin('tbl_place_lang','tbl_show.place_id','=','tbl_place_lang.place_id')
                    ->select('tbl_show_lang.*','tbl_show.time_show','tbl_place_lang.name as place_name')->where('tbl_place_lang.lang_id',$this->d_lang)->where('tbl_show_lang.lang_id',$this->d_lang)->where('tbl_show_lang.show_id',$data_order->show_id)->first();
        $ticket = \tblTicketShowModel::leftJoin('tbl_ticket_lang','tbl_ticket_show.ticket_id','=','tbl_ticket_lang.ticket_id')->where('tbl_ticket_show.show_id',$data_order->show_id)->groupBy('tbl_ticket_show.ticket_id')->get();
        foreach($ticket as $key=>$value){
            $check = \tblOrderDetailModel::where('order_id',$order->id)->where('seat_type',$value->ticket_id)->count();
            if($check<=0){
                unset($ticket[$key]);
            }
        }
        return \View::make('admin.order.print_ticket')->with('data_order',$data_order)->with('data_detail',$data_detail)->with('show',$show)->with('ticket',$ticket);
    }

    /* xem nhan vien */
    public function getView() {
        \Session::forget('order_search_key');        
        \Session::forget('order_status_key');
        \Session::forget('order_start_key');
        \Session::forget('order_end_key');
		\Session::forget('order_type_key');
        \Session::forget('order_show_key');
		\Session::forget('order_payment_key');
		$data = \tblOrderModel::orderBy('id','desc')->where('payment_type',1)->where('order_type',1)->where('status',0)->paginate(10);
        $order = \tblOrderModel::where('payment_type',1)->where('order_type',1)->where('status',0)->count();
        $order_detail = \tblOrderModel::leftJoin('tbl_order_detail','tbl_order.id','=','tbl_order_detail.order_id')->where('tbl_order.payment_type',1)->where('tbl_order.order_type',1)->where('tbl_order.status',0)->select(\DB::raw("SUM(tbl_order_detail.seat_price) as total"),\DB::raw("COUNT(tbl_order_detail.id) as quantity"))->first();
        $show = \tblShowLangModel::where('status',1)->where('lang_id',$this->d_lang)->get();

        if (\Request::ajax()) {     
            return \View::make('admin.order.ajax')->with('data', $data)->with('order_detail', $order_detail)->with('order', $order);
        } else {
            return \View::make('admin.order.view')->with('data', $data)->with('show', $show)->with('order_detail', $order_detail)->with('order', $order);
        }
    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('order_search_key');
       \Session::forget('order_show_key');
        \Session::forget('order_status_key');
        \Session::forget('order_start_key');
        \Session::forget('order_end_key');
		\Session::forget('order_type_key');
		\Session::forget('order_payment_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword = 'null';
            
        }
        \Session::set('order_search_key', $keyword);
        return \Redirect::action('\ADMIN\OrderController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblOrderModel::orderBy('id', 'desc');
        $sql_data1 = \tblOrderModel::orderBy('id', 'desc');
        $sql_data2 = \tblOrderModel::leftJoin('tbl_order_detail','tbl_order.id','=','tbl_order_detail.order_id');
        $sql_data->where(function($query) use ($keyword) {
            $query->where('cus_email', 'LIKE', '%' . $keyword . '%')->orWhere('code', 'LIKE', '%' . $keyword . '%')
                                ->orWhere('cus_name', 'LIKE', '%' . $keyword . '%')
                                ->orWhere('cus_phone', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data1->where(function($query1) use ($keyword) {
            $query1->where('cus_email', 'LIKE', '%' . $keyword . '%')->orWhere('code', 'LIKE', '%' . $keyword . '%')
                                ->orWhere('cus_name', 'LIKE', '%' . $keyword . '%')
                                ->orWhere('cus_phone', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data2->where(function($query2) use ($keyword) {
            $query2->where('cus_email', 'LIKE', '%' . $keyword . '%')->orWhere('code', 'LIKE', '%' . $keyword . '%')
                                ->orWhere('cus_name', 'LIKE', '%' . $keyword . '%')
                                ->orWhere('cus_phone', 'LIKE', '%' . $keyword . '%');
        });
        $data_lang = $sql_data->paginate(10);
        $order = $sql_data1->count();
        $order_detail = $sql_data2->select(\DB::raw("SUM(tbl_order_detail.seat_price) as total"),\DB::raw("COUNT(tbl_order_detail.id) as quantity"))->first();
        return \View::make('admin.order.ajax')->with('data', $data_lang)->with('order_detail', $order_detail)->with('order', $order);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\OrderController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = $sql_data = \tblOrderModel::orderBy('id', 'desc');
        $sql_data1 = \tblOrderModel::orderBy('id', 'desc');
        $sql_data2 = \tblOrderModel::leftJoin('tbl_order_detail','tbl_order.id','=','tbl_order_detail.order_id');
        if(\Session::has('order_status_key') && \Session::get('order_status_key')!='null'){
            $sql_data->where('status', \Session::get('order_status_key'));
            $sql_data1->where('status', \Session::get('order_status_key'));
            $sql_data2->where('tbl_order.status', \Session::get('order_status_key'));
        }else{
			$sql_data->where('status', 0);
            $sql_data1->where('status', 0);
            $sql_data2->where('tbl_order.status',0);
		}
        if(\Session::has('order_start_key') && \Session::get('order_start_key')!='null'){
            $sql_data->where('created_at', '>=',date('Y-m-d h:i:s',strtotime(\Session::get('order_start_key'))));
            $sql_data1->where('created_at', '>=',date('Y-m-d h:i:s',strtotime(\Session::get('order_start_key'))));
            $sql_data2->where('tbl_order.created_at', '>=',date('Y-m-d h:i:s',strtotime(\Session::get('order_start_key'))));
        }
        if(\Session::has('order_end_key') && \Session::get('order_end_key')!='null'){
            $sql_data->where('created_at', '<=', date('Y-m-d h:i:s',strtotime(\Session::get('order_end_key'))));
            $sql_data1->where('created_at', '>=',date('Y-m-d h:i:s',strtotime(\Session::get('order_start_key'))));
            $sql_data2->where('tbl_order.created_at', '>=',date('Y-m-d h:i:s',strtotime(\Session::get('order_start_key'))));
        }
		if(\Session::has('order_type_key') && \Session::get('order_type_key')!='null'){
            $sql_data->where('order_type',\Session::get('order_type_key'));
            $sql_data1->where('order_type',\Session::get('order_type_key'));
            $sql_data2->where('tbl_order.order_type',\Session::get('order_type_key'));
        }else{
			$sql_data->where('order_type', 1);
            $sql_data1->where('order_type', 1);
            $sql_data2->where('tbl_order.order_type',1);
		}
        if(\Session::has('order_show_key') && \Session::get('order_show_key')!='null'){
            $sql_data->where('show_id',\Session::get('order_show_key'));
            $sql_data1->where('show_id',\Session::get('order_show_key'));
            $sql_data2->where('tbl_order.show_id',\Session::get('order_show_key'));
        }
		if(\Session::has('order_payment_key') && \Session::get('order_payment_key')!='null'){
            $sql_data->where('payment_type',\Session::get('order_payment_key'));
            $sql_data1->where('payment_type',\Session::get('order_payment_key'));
            $sql_data2->where('tbl_order.payment_type',\Session::get('order_payment_key'));
        }else{
			$sql_data->where('payment_type', 1);
            $sql_data1->where('payment_type', 1);
            $sql_data2->where('tbl_order.payment_type',1);
		}
        if(\Session::has('order_search_key') && \Session::get('order_search_key')!='null'){
            $keyword = \Session::get('order_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('cus_email', 'LIKE', '%' . $keyword . '%')->orWhere('code', 'LIKE', '%' . $keyword . '%')
                                    ->orWhere('cus_name', 'LIKE', '%' . $keyword . '%')
                                    ->orWhere('cus_phone', 'LIKE', '%' . $keyword . '%');
            });
            $sql_data1->where(function($query1) use ($keyword) {
                $query1->where('cus_email', 'LIKE', '%' . $keyword . '%')->orWhere('code', 'LIKE', '%' . $keyword . '%')
                                    ->orWhere('cus_name', 'LIKE', '%' . $keyword . '%')
                                    ->orWhere('cus_phone', 'LIKE', '%' . $keyword . '%');
            });
            $sql_data2->where(function($query2) use ($keyword) {
                $query2->where('cus_email', 'LIKE', '%' . $keyword . '%')->orWhere('code', 'LIKE', '%' . $keyword . '%')
                                    ->orWhere('cus_name', 'LIKE', '%' . $keyword . '%')
                                    ->orWhere('cus_phone', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->paginate($page);
        $order = $sql_data1->count();
        $order_detail = $sql_data2->select(\DB::raw("SUM(tbl_order_detail.seat_price) as total"),\DB::raw("COUNT(tbl_order_detail.id) as quantity"))->first();
        return \View::make('admin.order.ajax')->with('data', $data_lang)->with('order_detail', $order_detail)->with('order', $order);
    }

    public function postFilter(){
        $status = '';
        $start='';
        $end='';
		$type='';
        $show='';
		$payment='';
        \Session::forget('order_search_key');       
        \Session::forget('order_status_key');
        \Session::forget('order_start_key');
        \Session::forget('order_end_key');
        \Session::forget('order_type_key');
        \Session::forget('order_show_key');
		\Session::forget('order_payment_key');
        if (\Input::has('user_start_date_filter') || \Input::get('user_start_date_filter') != '') {
            $start=\Input::get('user_start_date_filter');
        }else{
            $start='null';
        }
        if (\Input::has('user_end_date_filter') || \Input::get('user_end_date_filter') != '') {
            $end=\Input::get('user_end_date_filter');
        }else{
            $end='null';
        }
        if (\Input::has('user_filter_status') || \Input::get('user_filter_status') != '') {
            $status=\Input::get('user_filter_status');
        }else{
            $status='null';
        }	
		if (\Input::has('user_filter_type') || \Input::get('user_filter_type') != '') {
            $type=\Input::get('user_filter_type');
        }else{
            $type='null';
        }
        if (\Input::has('user_filter_show') || \Input::get('user_filter_show') != '') {
            $show=\Input::get('user_filter_show');
        }else{
            $show='null';
        }
		if (\Input::has('user_payment_type') || \Input::get('user_payment_type') != '') {
            $payment=\Input::get('user_payment_type');
        }else{
            $payment='null';
        }
        \Session::set('order_status_key', $status);
        \Session::set('order_start_key', $start);
        \Session::set('order_end_key', $end);
		\Session::set('order_type_key', $type);
        \Session::set('order_show_key',$show);
		\Session::set('order_payment_key',$payment);
        return \Redirect::action('\ADMIN\OrderController@getFilter', array($status,$start,$end,$type,$show,$payment));
    }

    public function getFilter($status='',$start='',$end='',$type='',$show='',$payment=''){
         $sql_data = \tblOrderModel::orderBy('id', 'desc');
        $sql_data1 = \tblOrderModel::orderBy('id', 'desc');
        $sql_data2 = \tblOrderModel::leftJoin('tbl_order_detail','tbl_order.id','=','tbl_order_detail.order_id');
        if($status!='null'){
            $sql_data->where('status', $status);
            $sql_data1->where('status', $status);
            $sql_data2->where('tbl_order.status', $status);
        }
        if($start!='null'){
            $sql_data->where('created_at', '>=', date('Y-m-d h:i:s',strtotime($start)));
             $sql_data1->where('created_at', '>=', date('Y-m-d h:i:s',strtotime($start)));
              $sql_data2->where('tbl_order.created_at', '>=', date('Y-m-d h:i:s',strtotime($start)));
        }
        if($end!='null'){
            $sql_data->where('created_at', '<=', date('Y-m-d h:i:s',strtotime($end)));
            $sql_data1->where('created_at', '<=', date('Y-m-d h:i:s',strtotime($end)));
            $sql_data2->where('tbl_order.created_at', '<=', date('Y-m-d h:i:s',strtotime($end)));
        }
		if($type!='null'){
            $sql_data->where('order_type', $type);
             $sql_data1->where('order_type', $type);
              $sql_data2->where('tbl_order.order_type', $type);
        }
        if($show!='null'){
            $sql_data->where('show_id', $show);
            $sql_data1->where('show_id', $show);
            $sql_data2->where('tbl_order.show_id', $show);
        }
		if($payment!='null'){
            $sql_data->where('payment_type', $payment);
            $sql_data1->where('payment_type', $payment);
            $sql_data2->where('tbl_order.payment_type', $payment);
        }
        $data_lang = $sql_data->paginate(10);
        $order = $sql_data1->count();
        $order_detail = $sql_data2->select(\DB::raw("SUM(tbl_order_detail.seat_price) as total"),\DB::raw("COUNT(tbl_order_detail.id) as quantity"))->first();
        return \View::make('admin.order.ajax')->with('data', $data_lang)->with('order_detail', $order_detail)->with('order', $order);
    }
	public function postQuickView() {
        $all_input = \Input::all();
        $order_current = \tblOrderModel::find($all_input['id']);
        $order_detail_current = \tblOrderDetailModel::leftjoin('tbl_ticket_lang', 'tbl_order_detail.seat_type', '=', 'tbl_ticket_lang.ticket_id')
					->where('tbl_order_detail.order_id', $all_input['id'])
					->where('tbl_ticket_lang.lang_id', $this->d_lang)
					->orderBy('tbl_order_detail.id')->get();
        $show = \tblShowModel::leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')
						->leftJoin('tbl_place_lang','tbl_show.place_id','=','tbl_place_lang.place_id')
						->select('tbl_show_lang.*','tbl_show.time_show','tbl_place_lang.name as place_name')												->where('tbl_show_lang.show_id',$order_current->show_id)
						->where('tbl_place_lang.lang_id',$this->d_lang)
						->where('tbl_show_lang.lang_id',$this->d_lang)
						->first();
        return \View::make('admin.order.quick_detail')->with('show', $show)->with('order_current', $order_current)->with('order_detail_current', $order_detail_current);
    }
	/* active don hang dat hang online chua thanh toan */
	public function postActive() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblOrderModel::where('id', $all_input['id'])->update(array('status' => 1));            
            return $this->getView()->render();
        } else {
            return \App::abort(404);
        }
    }
  
    public function postDelete() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            /* cap nhat lai trang thái ghế */
            $data = \tblOrderModel::leftJoin('tbl_order_detail','tbl_order.id','=','tbl_order_detail.order_id')
                        ->where('tbl_order_detail.order_id',$all_input['id'])->get();
            if(count($data)>0){
                foreach($data as $item){
                    \tblTicketShowModel::where('show_id',$item->show_id)->where('ticket_id',$item->seat_type)->where('seat_name_orin',$item->seat_name_orin)->update(array('status' => 1,'time_reset'=>''));
                }
            }
            \tblOrderModel::where('id', $all_input['id'])->update(array('status' => 2));
            return $this->getView()->render();
        } else {
            return \App::abort(404);
        }
    }
	
	
}
