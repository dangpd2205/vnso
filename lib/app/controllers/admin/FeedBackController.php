<?php

namespace ADMIN;

use View;

class FeedBackController extends \BaseController {
    private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // H?m ch?y khi g?i class
    public function __construct() {
        $this->titlepage = 'Quản lý phản hồi';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
    }
	
	public function postDeleteMulti() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $comment = \tblFeedbackModel::whereIn('id', $all_input['list_id'])->delete();
            return $this->getView()->render();
        }
    }
	
	public function getView(){
		\Session::forget('fb_search_key');
        \Session::forget('fb_status_key');
		$data = \tblFeedbackModel::orderBy('id','desc')->where('status',0)->paginate(10);        
        if (\Request::ajax()) {
            return \View::make('admin.feedback.ajax')->with('data', $data);
        } else {
            return \View::make('admin.feedback.view')->with('data', $data);
        }
	}	
	public function postSearch(){
        $keyword = '';
        \Session::forget('fb_search_key');
        \Session::forget('fb_status_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword = 'null';
            
        }
        \Session::set('fb_search_key', $keyword);
        return \Redirect::action('\ADMIN\FeedBackController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblFeedbackModel::orderBy('id', 'desc');
        $sql_data->where(function($query) use ($keyword) {
            $query->where('email', 'LIKE', '%' . $keyword . '%')
                                ->orWhere('name', 'LIKE', '%' . $keyword . '%');
        });
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.feedback.ajax')->with('data', $data_lang);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\FeedBackController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = $sql_data = \tblFeedbackModel::orderBy('id', 'desc');
        
        if(\Session::has('fb_status_key') && \Session::get('fb_status_key')!='null'){
            $sql_data->where('status', \Session::get('fb_status_key'));
        }
        
        if(\Session::has('fb_search_key') && \Session::get('fb_search_key')!='null'){
            $keyword = \Session::get('fb_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('email', 'LIKE', '%' . $keyword . '%')
                                    ->orWhere('name', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.feedback.ajax')->with('data', $data_lang);
    }

    public function postFilter(){
        $status = '';
        \Session::forget('fb_search_key');
        \Session::forget('fb_status_key');
      
        if (\Input::has('fb_filter_status') || \Input::get('fb_filter_status') != '') {
            $status=\Input::get('fb_filter_status');
        }else{
            $status='null';
        }
        \Session::set('fb_status_key', $status);
        return \Redirect::action('\ADMIN\FeedBackController@getFilter', array($status));
    }

    public function getFilter($status=''){
		$sql_data = \tblFeedbackModel::orderBy('id', 'desc');
        
        if($status!='null'){
            $sql_data->where('status', $status);
        }
        
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.feedback.ajax')->with('data', $data_lang);
    }
}
