<?php

namespace ADMIN;

class PlaceController extends \BaseController {
	private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    
    public function __construct() {
        $this->titlepage = 'Quản lý địa điểm diễn';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }


    public function getView() {
		
        \Session::forget('place_status_key');
        \Session::forget('place_search_key');
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $obj_news = new \tblPlaceModel();
            $obj_news->resolveConnection()->getPaginator()->setBaseUrl(\URL::action('\ADMIN\PlaceController@getView'));
            $data_lang = $obj_news->join('tbl_place_lang', 'tbl_place.id', '=', 'tbl_place_lang.place_id')->where('tbl_place_lang.lang_id', $this->d_lang)->orderBy('tbl_place.id', 'desc')->where('tbl_place_lang.status',1)->paginate(10);
            
            return \View::make('admin.place.ajax')->with('data', $data_lang);
        } else {
            $data  = \tblPlaceModel::join('tbl_place_lang', 'tbl_place.id', '=', 'tbl_place_lang.place_id')->where('tbl_place_lang.lang_id', $this->d_lang)->orderBy('tbl_place.id', 'desc')->where('tbl_place_lang.status',1)->paginate(10);
                
            return \View::make('admin.place.view')->with('data', $data);
        }

    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('place_status_key');
        \Session::forget('place_search_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
            $keyword='null';
        }
        \Session::set('place_search_key', $keyword);
        return \Redirect::action('\ADMIN\PlaceController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblPlaceModel::join('tbl_place_lang', 'tbl_place.id', '=', 'tbl_place_lang.place_id')->where('tbl_place_lang.lang_id', $this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_place_lang.name', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_place.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.place.ajax')->with('data', $data_lang);
    }

    public function postFilter(){
        
        $status = '';
        \Session::forget('place_status_key');
        \Session::forget('place_search_key');
        
        if (\Input::has('news_status_filter') || \Input::get('news_status_filter') != '') {
            $status=\Input::get('news_status_filter');
        }else{
            $status='null';
        }
        \Session::set('place_status_key', $status);
        return \Redirect::action('\ADMIN\PlaceController@getFilter', array($status));
    }

    public function getFilter($status=''){
        $sql_data = \tblPlaceModel::join('tbl_place_lang', 'tbl_place.id', '=', 'tbl_place_lang.place_id')->where('tbl_place_lang.lang_id', $this->d_lang);
       
        if($status!='null'){
            $sql_data->where('tbl_place_lang.status', $status);
        }
        $sql_data->orderBy('tbl_place.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.place.ajax')->with('data', $data_lang);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\PlaceController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = \tblPlaceModel::join('tbl_place_lang', 'tbl_place.id', '=', 'tbl_place_lang.place_id')->where('tbl_place_lang.lang_id', $this->d_lang);
        
        if(\Session::has('place_status_key') && \Session::get('place_status_key')!='null'){
            $sql_data->where('tbl_place_lang.status', \Session::get('place_status_key'));
        }else{
            $sql_data->where('tbl_place_lang.status',1);
        }
        if(\Session::has('place_search_key') && \Session::get('place_search_key')!='null'){
            $keyword=\Session::get('place_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('tbl_place_lang.name', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->orderBy('tbl_place.id', 'desc')->paginate($page);
        return \View::make('admin.place.ajax')->with('data', $data_lang);
    }

    public function getAdd() {
        return \View::make('admin.place.add');
    }

    public function postAdd() {
        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();

        $log = [];
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){

            $tbl_news = new \tblPlaceModel();       
            $tbl_news->avatar = ($all_input['avatar'] != '') ? $all_input['avatar'] : '';
            $tbl_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    
                    $tbl_detail_news = new \tblPlaceLangModel();
                    $tbl_detail_news->place_id = $tbl_news->id;
                    $tbl_detail_news->lang_id = $item_lang->id;
                    $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';                    
					$tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? strip_tags($all_input['content_' . $item_lang->code]) : '';
					$tbl_detail_news->address = ($all_input['address_' . $item_lang->code] != '') ? strip_tags($all_input['address_' . $item_lang->code]) : '';
                    $tbl_detail_news->status = 1;
                    $tbl_detail_news->save();

                }
            }
            return \Redirect::action('\ADMIN\PlaceController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }
    
    public function getEdit($lang, $id) {
        $data_news = \tblPlaceModel::join('tbl_place_lang', 'tbl_place.id', '=', 'tbl_place_lang.place_id')
                    ->leftJoin('tbl_lang', 'tbl_place_lang.lang_id', '=', 'tbl_lang.id')
                    ->where('tbl_place.id', $id)
                    ->select('tbl_lang.id as langid','tbl_place.avatar','tbl_place_lang.*')
                    ->get();
        $lang_code = \tblLangModel::where('id',$lang)->first();
        if (count($data_news) > 0) {           
            return \View::make('admin.place.edit')->with('lang_code', $lang_code)->with('data_news', $data_news);            
        } else {
            return \Redirect::action('\ADMIN\PlaceController@getView');
        }
    }

    public function postEdit() {

        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();
        $all_input = \Input::all();
		
        if(\Input::get('name_'.$tblLangModel->code)){
            $row_news = \tblPlaceModel::find($all_input['id_place']);
         
            $row_news->avatar = ($all_input['avatar'] != '') ? $all_input['avatar'] : '';
            $row_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['name_' . $item_lang->code]) {
                    $check = \tblPlaceLangModel::where('place_id', $all_input['id_place'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                    // neu co roi thi update
                    if(count($check)>0){                        
                        $tbl_detail_news = \tblPlaceLangModel::where('place_id', $all_input['id_place'])->where('lang_id', $all_input['lang_id_'. $item_lang->code])->first();					
                        $tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';	                      
						$tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? strip_tags($all_input['content_' . $item_lang->code]) : '';
						$tbl_detail_news->address = ($all_input['address_' . $item_lang->code] != '') ? strip_tags($all_input['address_' . $item_lang->code]) : '';
                        $tbl_detail_news->save();
                    }else{
                       
                        $tbl_detail_news = new \tblPlaceLangModel();
                        $tbl_detail_news->place_id = $all_input['id_place'];
						$tbl_detail_news->lang_id = $item_lang->id; 
						$tbl_detail_news->name = ($all_input['name_' . $item_lang->code] != '') ? strip_tags($all_input['name_' . $item_lang->code]) : '';      
						$tbl_detail_news->content = ($all_input['content_' . $item_lang->code] != '') ? strip_tags($all_input['content_' . $item_lang->code]) : '';						
						$tbl_detail_news->address = ($all_input['address_' . $item_lang->code] != '') ? strip_tags($all_input['address_' . $item_lang->code]) : '';
						$tbl_detail_news->status = 1;
                        $tbl_detail_news->save();
                    }
                }   
            }
    	
            return \Redirect::action('\ADMIN\PlaceController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }

    public function postDelete() {

        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblPlaceLangModel::where('place_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 2));   
                  
            return $this->getView()->render();
        }
    }

    public function postPostConfirm() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
           \tblPlaceLangModel::where('place_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 1));
           
            return $this->getView()->render();
        }
    }
    private function insertMap($arr=[],$arr_orin=[],$class_name,$place_id,$start,$end,$check,$ticket_id){
        $data = [];
        $arr_class = [];           
        for($i=$start;$i<=$end;$i++){
            $arr_class[] = 'ss-'.$i;
        }
       
        if(count($arr)>0){            
            for($j=0;$j<count($arr);$j++){
                if(isset($arr_class[$j])){
                    $data[] = ['class_name'=>$class_name.' '.$arr_class[$j],'place_id'=>$place_id,'seat_name'=>$arr[$j],'seat_name_orin'=>$arr_orin[$j],'ticket_id'=>$ticket_id];    
                }               
            }
            \DB::table('tbl_place_map')->insert($data);
        }
        
    }

    /* insert place map */
    
    public function getMap1(){
        $b_seat = array('F17','F15','F13','F11','F09','F07','F05','F03','F01','F02','F04','F06','F08','F10','F12','F14','F16','F18','E31','E29','E27','E25','E23','E21','E19','E17','E15','E13','E11','E09','E07','E05','E03','E01','E02','E04','E06','E08','E10','E12','E14','E16','E18','E20','E22','E24','E26','E28','L02','E32','D31','D29','D27','D25','D23','D21','D19','D17','E30','D15','D13','D11','D09','D07','D05','D03','D01','D02','D04','D06','D08','D10','D12','D14','D16','D18','D20','D22','D24','D26','D28','D30','D32','C31','C29','C27','C25','C23','C21','C19','C17','C15','C13','C11','C09','C07','C05','C03','C01','C02','L04','C04','C06','C08','C10','C12','C14','C16','C18','C20','C22','C24','C26','C28','C30','C32','B29','L06','B27','D25','B23','B21','B19','B17','B15','B13','B11','B09','B07','B05','B03','B01','B02','B04','B06','B08','B10','B12','B14','B16','B18','B20','B22','B24','B26','B28','B30','A29','A27','A25','A23','A21','A19','A17','A15','A13','A11','A09','A07','A05','A03','A01','A02','A04','A06','A08','A10','A12','A14','A16','A18','A20','A22','A24','A26','A28','A30','L33','L31','L29','L27','L25','L23','L21','L19','L17','L15','L13','L11','L09','L07','L05','L03','L01','L34','L32','L30','L28','L26','L24','L22','L20','L18','L16','L14','L12','L10','L08'); /* 1-209 */
        $b_seat_orin = array('F17','F15','F13','F11','F09','F07','F05','F03','F01','F02','F04','F06','F08','F10','F12','F14','F16','F18','E31','E29','E27','E25','E23','E21','E19','E17','E15','E13','E11','E09','E07','E05','E03','E01','E02','E04','E06','E08','E10','E12','E14','E16','E18','E20','E22','E24','E26','E28','L02','E32','D31','D29','D27','D25','D23','D21','D19','D17','E30','D15','D13','D11','D09','D07','D05','D03','D01','D02','D04','D06','D08','D10','D12','D14','D16','D18','D20','D22','D24','D26','D28','D30','D32','C31','C29','C27','C25','C23','C21','C19','C17','C15','C13','C11','C09','C07','C05','C03','C01','C02','L04','C04','C06','C08','C10','C12','C14','C16','C18','C20','C22','C24','C26','C28','C30','C32','B29','L06','B27','D25','B23','B21','B19','B17','B15','B13','B11','B09','B07','B05','B03','B01','B02','B04','B06','B08','B10','B12','B14','B16','B18','B20','B22','B24','B26','B28','B30','A29','A27','A25','A23','A21','A19','A17','A15','A13','A11','A09','A07','A05','A03','A01','A02','A04','A06','A08','A10','A12','A14','A16','A18','A20','A22','A24','A26','A28','A30','L33','L31','L29','L27','L25','L23','L21','L19','L17','L15','L13','L11','L09','L07','L05','L03','L01','L34','L32','L30','L28','L26','L24','L22','L20','L18','L16','L14','L12','L10','L08'); /* 1-209 */
        $s_seat = array('P13','P11','P09','P07','P05','P03','P01','P02','P04','P06','P08','P10','P12','P14','O27','O25','023','O21','O19','017','O15','O13','O11','O09','O07','O05','O03','O01','O02','O04','O06','O08','O10','O12','O14','O16','O18','O20','O22','O24','O26','O28','N27','N25','N23','N21','N19','N17','N15','N13','N11','N09','N07','N05','N03','N01','N02','N04','N06','N08','N10','N12','N14','N16','N18','N20','N22','N24','N26','N28','M27','M25','N23','M21','M19','M17','M15','M13','M11','M09','M07','M05','M03','M01','M02','M04','M06','M08','M10','M12','M14','M16','M18','M20','M22','M24','M26','M28','L27','L25','L23','L21','L19','L17','L15','L13','L11','L09','L07','L05','A32','L03','L01','L02','L04','L06','L08','L10','L12','L14','L16','L18','L20','L22','L24','L26','L28','K31','K29','K27','K25','K23','K21','K19','K17','K15','K13','K11','K09','K07','K05','K03','K01','K02','A30','K04','K06','K08','K10','K12','K14','K16','K18','K20','A24','A26','A28','K22','K24','K26','K28','K30','K32','J31','J29','J27','J25','J23','J21','J19','J17','J15','J13','J11','J09','J07','J05','J03','J01','J02','J04','J06','J08','J10','J12','J14','J16','J18','J20','J22','J24','J26','J28','J30','J32','I31','I29','I27','I25','I23','I21','I19','I17','I15','I13','I11','I09','I07','I05','I03','I01','I02','I04','I06','I08','I10','I12','I14','I16','I18','I20','I22','I24','I26','I28','I30','I32','H31','H29','H27','H25','H23','H21','H19','H17','H15','H13','H11','H09','H07','H05','H03','H01','H02','H04','H06','H08','H10','H12','H14','H16','H18','H20','H22','H24','H26','H28','H30','H32','G31','G29','G27','G25','G23','G21','G19','G17','G15','G13','G11','G09','G07','G05','G03','G01','G02','G04','G06','G08','G10','G12','G14','G16','G18','G20','G22','G24','G26','G28','G30','G32','F31','F29','F27','F25','F23','F21','F19','F17','F15','F13','F11','F09','F07','F05','F03','F01','F02','F04','F06','F08','F10','F12','F14','F16','F18','F20','F22','F24','F26','F28','F30','F32','E31','E29','E27','E25','E23','E21','E19','E17','E15','E13','E11','E09','E07','E05','E03','E01','E02','E04','E06','E08','E10','E12','E14','E16','E18','E20','E22','E24','E26','E28','E30','E32','D31','D29','D27','D25','D23','D21','D19','D17','D15','D13','D11','D09','D07','D05','D03','D01','D02','D04','D06','D08','D10','D12','D14','D16','D18','D20','D22','D24','D26','D28','D30','D32','C31','C29','C27','C25','C23','C21','C19','C17','C15','C13','C11','C09','C07','C05','C03','C01','C02','C04','C06','C08','C10','C12','C14','C16','C18','C20','C22','C24','C26','C28','C30','C32','B31','B29','B27','B25','B23','B21','B19','B17','B15','B13','B11','B09','B07','B05','B03','B01','B02','B04','B06','B08','B10','B12','B14','B16','B18','B20','B22','B24','B26','B28','B30','B32','A31','A29','A27','A25'); 
        $s_seat_orin = array('P13','P11','P09','P07','P05','P03','P01','P02','P04','P06','P08','P10','P12','P14','O27','O25','023','O21','O19','017','O15','O13','O11','O09','O07','O05','O03','O01','O02','O04','O06','O08','O10','O12','O14','O16','O18','O20','O22','O24','O26','O28','N27','N25','N23','N21','N19','N17','N15','N13','N11','N09','N07','N05','N03','N01','N02','N04','N06','N08','N10','N12','N14','N16','N18','N20','N22','N24','N26','N28','M27','M25','N23','M21','M19','M17','M15','M13','M11','M09','M07','M05','M03','M01','M02','M04','M06','M08','M10','M12','M14','M16','M18','M20','M22','M24','M26','M28','L27','L25','L23','L21','L19','L17','L15','L13','L11','L09','L07','L05','A32','L03','L01','L02','L04','L06','L08','L10','L12','L14','L16','L18','L20','L22','L24','L26','L28','K31','K29','K27','K25','K23','K21','K19','K17','K15','K13','K11','K09','K07','K05','K03','K01','K02','A30','K04','K06','K08','K10','K12','K14','K16','K18','K20','A24','A26','A28','K22','K24','K26','K28','K30','K32','J31','J29','J27','J25','J23','J21','J19','J17','J15','J13','J11','J09','J07','J05','J03','J01','J02','J04','J06','J08','J10','J12','J14','J16','J18','J20','J22','J24','J26','J28','J30','J32','I31','I29','I27','I25','I23','I21','I19','I17','I15','I13','I11','I09','I07','I05','I03','I01','I02','I04','I06','I08','I10','I12','I14','I16','I18','I20','I22','I24','I26','I28','I30','I32','H31','H29','H27','H25','H23','H21','H19','H17','H15','H13','H11','H09','H07','H05','H03','H01','H02','H04','H06','H08','H10','H12','H14','H16','H18','H20','H22','H24','H26','H28','H30','H32','G31','G29','G27','G25','G23','G21','G19','G17','G15','G13','G11','G09','G07','G05','G03','G01','G02','G04','G06','G08','G10','G12','G14','G16','G18','G20','G22','G24','G26','G28','G30','G32','F31','F29','F27','F25','F23','F21','F19','F17','F15','F13','F11','F09','F07','F05','F03','F01','F02','F04','F06','F08','F10','F12','F14','F16','F18','F20','F22','F24','F26','F28','F30','F32','E31','E29','E27','E25','E23','E21','E19','E17','E15','E13','E11','E09','E07','E05','E03','E01','E02','E04','E06','E08','E10','E12','E14','E16','E18','E20','E22','E24','E26','E28','E30','E32','D31','D29','D27','D25','D23','D21','D19','D17','D15','D13','D11','D09','D07','D05','D03','D01','D02','D04','D06','D08','D10','D12','D14','D16','D18','D20','D22','D24','D26','D28','D30','D32','C31','C29','C27','C25','C23','C21','C19','C17','C15','C13','C11','C09','C07','C05','C03','C01','C02','C04','C06','C08','C10','C12','C14','C16','C18','C20','C22','C24','C26','C28','C30','C32','B31','B29','B27','B25','B23','B21','B19','B17','B15','B13','B11','B09','B07','B05','B03','B01','B02','B04','B06','B08','B10','B12','B14','B16','B18','B20','B22','B24','B26','B28','B30','B32','A31','A29','A27','A25'); /* 1-254/* 1-254 L17 283-363*/
        $s_seat1 = array('A23','A21','A19','A17','A15','A13','A11','A09','A07','A05','A03','A01','A02','A04','A06','A08','A10','A12','A14','A16','A18','A20','A22');
        $s_seat1_orin = array('A23','A21','A19','A17','A15','A13','A11','A09','A07','A05','A03','A01','A02','A04','A06','A08','A10','A12','A14','A16','A18','A20','A22');
        $a_seat = array('B15','B13','B11','B09','B07','B05','B03','B01','A35','A33','A31','A29','A27','A25','A23','A21','A19','A17','A15','A13','A11','A09','A07','A05','A03','A01','B16','B14','B12','B10','B08','B06','B04','B02','A36','A34','A32','A30','A28','A26','A24','A22','A20','A18','A16','A14','A12','A10','A08','A06','A04','A02','C27','C25','C23','C21','C19','C17','C15','C13','C11','C09','C07','C05','C03','C01','C02','C04','C06','C08','C10','C12','C14','C16','C18','C20','C22','C24','C26','C28','C30','D25','D23','D21','D19','D17','D15','D13','D11','D09','D07','D05','D03','D01','D02','D04','D06','D08','D10','D12','D14','D16','D18','D20','D22','D24','D26','D28','D30','E27','E25','E23','E21','E19','E17','E15','E13','E11','E09','E07','E05','E03','E01','E02','E04','E06','E08','E10','E12','E14','E16','E18','E20','E22','E24','E26','E28','E30','E32','F29','F27','F25','F23','F21','F19','F17','F15','F13','F11','F09','F07','F05','F03','F01','F02','F04','F06','F08','F10','F12','F14','F16','F18','F20','F22','F24','F26','F28','F30','F32','F34','D32','C32'); /* 364 - 489 */ /* NF1 255 - 282 */
        $a_seat_orin = array('B15','B13','B11','B09','B07','B05','B03','B01','A35','A33','A31','A29','A27','A25','A23','A21','A19','A17','A15','A13','A11','A09','A07','A05','A03','A01','B16','B14','B12','B10','B08','B06','B04','B02','A36','A34','A32','A30','A28','A26','A24','A22','A20','A18','A16','A14','A12','A10','A08','A06','A04','A02','AA27','AA25','AA23','AA21','AA19','AA17','AA15','AA13','AA11','AA09','AA07','AA05','AA03','AA01','AA02','AA04','AA06','AA08','AA10','AA12','AA14','AA16','AA18','AA20','AA22','AA24','AA26','AA28','AA30','BB25','BB23','BB21','BB19','BB17','BB15','BB13','BB11','BB09','BB07','BB05','BB03','D01','BB02','BB04','BB06','BB08','BB10','BB12','BB14','BB16','BB18','BB20','BB22','BB24','BB26','BB28','BB30','CC27','CC25','CC23','CC21','CC19','CC17','CC15','CC13','CC11','CC09','CC07','CC05','CC03','CC01','CC02','CC04','CC06','CC08','CC10','CC12','CC14','CC16','CC18','CC20','CC22','CC24','CC26','CC28','CC30','CC32','DD29','DD27','DD25','DD23','DD21','DD19','DD17','DD15','DD13','DD11','DD09','DD07','DD05','DD03','DD01','DD02','DD04','DD06','DD08','DD10','DD12','DD14','DD16','DD18','DD20','DD22','DD24','DD26','DD28','DD30','DD32','DD34','BB32','AA32');
        
                $this->insertMap($b_seat,$b_seat_orin,'seat seat-b',2,1,209,'b_seat',1);
                $this->insertMap($s_seat,$s_seat_orin,'seat seat-s',2,213,667,'s_seat',3);
                $this->insertMap($a_seat,$a_seat_orin,'seat seat-a',2,706,878,'a_seat',2);   
                $this->insertMap($s_seat1,$s_seat1_orin,'seat seat-s',2,678,700,'s_seat1',3);
    }


}
