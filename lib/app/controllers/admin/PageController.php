<?php

namespace ADMIN;

class PageController extends \BaseController {
	private $titlepage = 'Pubweb.vn';
    private $listLang;
    private $d_lang;

    // Hạm chạy khi gọi class
    public function __construct() {
        $this->titlepage = 'Quản lý trang tĩnh';
        \View::composer(array('admin.template.header'), function($view) {
            $view->with('titlepage', $this->titlepage);
        });
        $this->listLang = \Config::get('all.all_lang');
        $this->d_lang = \Config::get('all.all_config')->website_lang;
        $this->beforeFilter('check_role');
    }
   
    /* tin tuc */
    public function getView() {
        \Session::forget('page_search_key');
        \Session::forget('page_status_key');
        if (\Request::ajax()) {
            $all_input = \Input::all();
            $obj_news = new \tblPageModel();
            $obj_news->resolveConnection()->getPaginator()->setBaseUrl(\URL::action('\ADMIN\PageController@getView'));
            $data_lang = $obj_news->join('tbl_page_lang', 'tbl_page.id', '=', 'tbl_page_lang.page_id')->where('tbl_page_lang.lang_id', $this->d_lang)->orderBy('tbl_page.id', 'desc')->where('tbl_page_lang.status', '!=', 2)->paginate(10);
                    
             
            return \View::make('admin.page.ajax')->with('data', $data_lang);
        } else {
            $data  = \tblPageModel::join('tbl_page_lang', 'tbl_page.id', '=', 'tbl_page_lang.page_id')->where('tbl_page_lang.lang_id', $this->d_lang)->where('tbl_page_lang.status', '!=', 2)->orderBy('tbl_page.id', 'desc')->paginate(10);
                
            return \View::make('admin.page.view')->with('data', $data);
        }
    }

    public function postSearch(){
        $keyword = '';
        \Session::forget('page_search_key');
        \Session::forget('page_status_key');
        if (\Input::has('search_key') || @\Input::get('search_key') == '') {
            $keyword = \Input::get('search_key');
        } else {
                $keyword = 'null';
            
        }
        \Session::set('page_search_key', $keyword);
        return \Redirect::action('\ADMIN\PageController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        $sql_data = \tblPageModel::join('tbl_page_lang', 'tbl_page.id', '=', 'tbl_page_lang.page_id')->where('tbl_page_lang.lang_id', $this->d_lang);
        $sql_data->where(function($query) use ($keyword) {
            $query->where('tbl_page_lang.page_name', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('tbl_page_lang.page_excerpt', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('tbl_page_lang.page_content', 'LIKE', '%' . $keyword . '%');
        });
        $sql_data->orderBy('tbl_page.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        return \View::make('admin.page.ajax')->with('data', $data_lang);
    }

    public function postFilter(){
        $status = '';
        \Session::forget('page_search_key');
        \Session::forget('page_status_key');
        if (\Input::has('news_status_filter') || \Input::get('news_status_filter') != '') {
            $status=\Input::get('news_status_filter');
        }else{
            $status='null';
        }
        \Session::set('page_status_key', $status);
        return \Redirect::action('\ADMIN\PageController@getFilter', array($status));
    }

    public function getFilter($status=''){
        $sql_data = \tblPageModel::join('tbl_page_lang', 'tbl_page.id', '=', 'tbl_page_lang.page_id')->where('tbl_page_lang.lang_id', $this->d_lang);
        
        if($status!='null'){
            $sql_data->where('tbl_page_lang.status', $status);
        }
        $sql_data->orderBy('tbl_page.id', 'desc');
        $data_lang = $sql_data->paginate(10);
        
        return \View::make('admin.page.ajax')->with('data', $data_lang);
    }

    public function postShow(){
        $page = '';
        if (\Input::has('row_table_setting') || \Input::get('row_table_setting') == '') {
            $page = \Input::get('row_table_setting');
        } 
        return \Redirect::action('\ADMIN\PageController@getShow', array($page));
    }

    public function getShow($page=''){
        $sql_data = \tblPageModel::join('tbl_page_lang', 'tbl_page.id', '=', 'tbl_page_lang.page_id')->where('tbl_page_lang.lang_id', $this->d_lang)->orderBy('tbl_page.id', 'desc');
        if(\Session::has('page_status_key') && \Session::get('page_status_key')!='null'){
            $sql_data->where('tbl_page_lang.status', $status);
        }
        if(\Session::has('page_search_key') && \Session::get('page_search_key')!='null'){
            $keyword=\Session::get('page_search_key');
            $sql_data->where(function($query) use ($keyword) {
                $query->where('tbl_page_lang.page_name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('tbl_page_lang.page_excerpt', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('tbl_page_lang.page_content', 'LIKE', '%' . $keyword . '%');
            });
        }
        $data_lang = $sql_data->paginate($page);
      
        return \View::make('admin.page.ajax')->with('data', $data_lang);
    }

    public function getAdd() {
        return \View::make('admin.page.add');
    }

    public function postAdd() {
        $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();

        $log = [];
        $all_input = \Input::all();
        if(\Input::get('page_name_'.$tblLangModel->code)){

            $tbl_news = new \tblPageModel();
        
            $tbl_news->comment_status = ($all_input['comment'] != '') ? $all_input['comment'] : 0;
            $tbl_news->ping_status = ($all_input['ping'] != '') ? $all_input['ping'] : 0;
            $tbl_news->save();

            foreach ($this->listLang as $item_lang) {
                if ($all_input['page_name_' . $item_lang->code]) {
                    $tbl_seo = new \tblSeoModel();
                    $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                    $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                    $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                    $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                    $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                    $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                    $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                    $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                    $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                    $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                    $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                    $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                    $tbl_seo->save();
                    $tbl_detail_news = new \tblPageLangModel();
                    $tbl_detail_news->page_id = $tbl_news->id;
                    $tbl_detail_news->lang_id = $item_lang->id;
                    $tbl_detail_news->seo_id = $tbl_seo->id;
                    $tbl_detail_news->page_name = ($all_input['page_name_' . $item_lang->code] != '') ? strip_tags($all_input['page_name_' . $item_lang->code]) : '';
                    $tbl_detail_news->page_excerpt = ($all_input['page_excerpt_' . $item_lang->code] != '') ? strip_tags($all_input['page_excerpt_' . $item_lang->code]) : '';
                    $tbl_detail_news->page_content = ($all_input['page_content_' . $item_lang->code] != '') ? $all_input['page_content_' . $item_lang->code] : '';                    
                    $tbl_detail_news->page_tag = ($all_input['page_tag_' . $item_lang->code] != '') ? $all_input['page_tag_' . $item_lang->code] : '';
                    $tbl_detail_news->page_slug = \Tree::gen_slug($all_input['page_name_' . $item_lang->code]) . '-' . $tbl_news->id;
                    $tbl_detail_news->status = 1;
                    $tbl_detail_news->save();

                    /* bang tag */
                    if(\Input::get('page_tag_' . $item_lang->code)!=''){
                        foreach(explode(',',\Input::get('page_tag_' . $item_lang->code)) as $i_tag){
                            $check = \tblTagsModel::where('slug', \Tree::gen_slug($i_tag))->first();
                            if(count($check)<=0){
                                $tblTagModel = new \tblTagsModel();
                                $tblTagModel->name = $i_tag;
                                $tblTagModel->slug = \Tree::gen_slug($i_tag);
                                $tblTagModel->save();
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $tblTagModel->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 3;
                                $tblTagViewModel->save();
                            }else{
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $check->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 3;
                                $tblTagViewModel->save();
                            }
                        }
                    }
                }
            }
            return \Redirect::action('\ADMIN\PageController@getAdd');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }
    }
    
    public function getEdit($lang, $id) {
        $data_news = \tblPageModel::join('tbl_page_lang', 'tbl_page.id', '=', 'tbl_page_lang.page_id')
                    ->leftJoin('tbl_lang', 'tbl_page_lang.lang_id', '=', 'tbl_lang.id')
                    ->leftJoin('tbl_seo', 'tbl_page_lang.seo_id', '=', 'tbl_seo.id')
                    ->where('tbl_page.id', $id)
                    ->select('tbl_lang.id as langid','tbl_page.time_post','tbl_page.comment_status','tbl_page.ping_status','tbl_page_lang.*','tbl_seo.id as seoid','tbl_seo.title','tbl_seo.description as seodesc','tbl_seo.keyword','tbl_seo.robots_index','tbl_seo.robots_follow','tbl_seo.robots_advanced','tbl_seo.fb_title','tbl_seo.fb_description','tbl_seo.fb_image','tbl_seo.g_title','tbl_seo.g_description','tbl_seo.g_image')
                    ->get();
        $lang_code = \tblLangModel::where('id',$lang)->first();
        if (count($data_news) > 0) {
            
            return \View::make('admin.page.edit')->with('lang_code', $lang_code)->with('data_news', $data_news);
            
        } else {
            return \Redirect::action('\ADMIN\PageController@getView');
        }
    }

    public function postEdit() {
         $tblLangModel = \tblLangModel::select('code','name')->where('id',$this->d_lang)->first();
        $all_input = \Input::all();
        if(\Input::get('page_name_'.$tblLangModel->code)){
            $row_news = \tblPageModel::find($all_input['id_page']);
           
            if ($all_input['comment'] != $row_news->comment_status) {
               
                $row_news->comment_status = ($all_input['comment'] != '') ? $all_input['comment'] : 0;
            }
            if ($all_input['ping'] != $row_news->ping_status) {
               
                $row_news->ping_status = ($all_input['ping'] != '') ? $all_input['ping'] : 0;
            }
            $row_news->save();
            foreach ($this->listLang as $item_lang) {
                if ($all_input['page_name_' . $item_lang->code]) {
                    $check = \tblPageLangModel::where('page_id', $all_input['id_page'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                    // neu co roi thi update
                    if(count($check)>0){
                        $tbl_seo = \tblSeoModel::find($check->seo_id);
                        if(count($tbl_seo)>0){
                            $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                            $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                            $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                            $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                            $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                            $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                            $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                            $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                            $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                            $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                            $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                            $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                            $tbl_seo->save();
                            
                            $tbl_detail_news = \tblPageLangModel::where('page_id', $all_input['id_page'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                            if($tbl_detail_news->page_name != $all_input['page_name_' . $item_lang->code]){
                                $tbl_detail_news->page_slug = \Tree::gen_slug($all_input['page_name_' . $item_lang->code]) . '-' . $all_input['id_page'];                        
                            }
                            $tbl_detail_news->page_name = ($all_input['page_name_' . $item_lang->code] != '') ? strip_tags($all_input['page_name_' . $item_lang->code]) : '';
                            $tbl_detail_news->page_excerpt = ($all_input['page_excerpt_' . $item_lang->code] != '') ? strip_tags($all_input['page_excerpt_' . $item_lang->code]) : '';
                            $tbl_detail_news->page_content = ($all_input['page_content_' . $item_lang->code] != '') ? $all_input['page_content_' . $item_lang->code] : '';                    
                            $tbl_detail_news->page_tag = ($all_input['page_tag_' . $item_lang->code] != '') ? $all_input['page_tag_' . $item_lang->code] : '';
                            
                            $tbl_detail_news->save();
                        }else{
                            $tblSeoModel = new \tblSeoModel();
                            $tblSeoModel->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                            $tblSeoModel->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                            $tblSeoModel->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                            $tblSeoModel->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                            $tblSeoModel->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                            $tblSeoModel->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                            $tblSeoModel->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                            $tblSeoModel->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                            $tblSeoModel->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                            $tblSeoModel->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                            $tblSeoModel->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                            $tblSeoModel->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                            $tblSeoModel->save();

                            $tbl_detail_news = \tblPageLangModel::where('page_id', $all_input['id_page'])->where('lang_id', $all_input['lang_id_' . $item_lang->code])->first();
                            if($tbl_detail_news->page_name != $all_input['page_name_' . $item_lang->code]){
                                $tbl_detail_news->page_slug = \Tree::gen_slug($all_input['page_name_' . $item_lang->code]) . '-' . $all_input['id_page'];                        
                            }
                            $tbl_detail_news->seo_id = $tblSeoModel->id;
                            $tbl_detail_news->page_name = ($all_input['page_name_' . $item_lang->code] != '') ? strip_tags($all_input['page_name_' . $item_lang->code]) : '';
                            $tbl_detail_news->page_excerpt = ($all_input['page_excerpt_' . $item_lang->code] != '') ? strip_tags($all_input['page_excerpt_' . $item_lang->code]) : '';
                            $tbl_detail_news->page_content = ($all_input['page_content_' . $item_lang->code] != '') ? $all_input['page_content_' . $item_lang->code] : '';                    
                            $tbl_detail_news->page_tag = ($all_input['page_tag_' . $item_lang->code] != '') ? $all_input['page_tag_' . $item_lang->code] : '';
                            
                            $tbl_detail_news->save();
                        }   
                        
                    }else{
                        $tbl_seo = new \tblSeoModel();
                        $tbl_seo->title = isset($all_input['seo_title_' . $item_lang->code]) ? $all_input['seo_title_' . $item_lang->code] : '';
                        $tbl_seo->description = isset($all_input['seo_description_' . $item_lang->code]) ? $all_input['seo_description_' . $item_lang->code] : '';
                        $tbl_seo->keyword = isset($all_input['seo_keyword_' . $item_lang->code]) ? $all_input['seo_keyword_' . $item_lang->code] : '';
                        $tbl_seo->robots_index = isset($all_input['seo_index']) ? $all_input['seo_index'] : 'index';
                        $tbl_seo->robots_follow = isset($all_input['seo_follow']) ? $all_input['seo_follow'] : 'Follow';
                        $tbl_seo->robots_advanced = isset($all_input['seo_advanced']) ? implode(",", $all_input['seo_advanced']) : 'NONE';
                        $tbl_seo->fb_title = isset($all_input['seo_f_title_' . $item_lang->code]) ? $all_input['seo_f_title_' . $item_lang->code] : '';
                        $tbl_seo->fb_description = isset($all_input['seo_f_description_' . $item_lang->code]) ? $all_input['seo_f_description_' . $item_lang->code] : '';
                        $tbl_seo->fb_image = isset($all_input['seo_f_image_' . $item_lang->code]) ? $all_input['seo_f_image_' . $item_lang->code] : '';
                        $tbl_seo->g_title = isset($all_input['seo_g_title_' . $item_lang->code]) ? $all_input['seo_g_title_' . $item_lang->code] : '';
                        $tbl_seo->g_description = isset($all_input['seo_g_description_' . $item_lang->code]) ? $all_input['seo_g_description_' . $item_lang->code] : '';
                        $tbl_seo->g_image = isset($all_input['seo_g_image_' . $item_lang->code]) ? $all_input['seo_g_image_' . $item_lang->code] : '';
                        $tbl_seo->save();
                        $tbl_detail_news = new \tblPageLangModel();
                        $tbl_detail_news->page_id = $all_input['id_page'];
                        $tbl_detail_news->lang_id = $item_lang->id;
                        $tbl_detail_news->seo_id = $tbl_seo->id;
                        $tbl_detail_news->page_name = ($all_input['page_name_' . $item_lang->code] != '') ? strip_tags($all_input['page_name_' . $item_lang->code]) : '';
                        $tbl_detail_news->page_excerpt = ($all_input['page_excerpt_' . $item_lang->code] != '') ? strip_tags($all_input['page_excerpt_' . $item_lang->code]) : '';
                        $tbl_detail_news->page_content = ($all_input['page_content_' . $item_lang->code] != '') ? $all_input['page_content_' . $item_lang->code] : '';                    
                        $tbl_detail_news->page_tag = ($all_input['page_tag_' . $item_lang->code] != '') ? $all_input['page_tag_' . $item_lang->code] : '';
                        $tbl_detail_news->page_slug = \Tree::gen_slug($tbl_detail_news->page_name) . '-' . $all_input['id_page'];
                        $tbl_detail_news->status =1;
                        $tbl_detail_news->save();
                    }
                    /* bang tag */
                    \tblTagsRelationshipModel::where('post_id',$tbl_detail_news->id)->where('type',3)->delete();
                    if(\Input::get('page_tag_' . $item_lang->code)!=''){
                        foreach(explode(',',\Input::get('page_tag_' . $item_lang->code)) as $i_tag){
                            $check = \tblTagsModel::where('slug', \Tree::gen_slug($i_tag))->first();
                            if(count($check)<=0){
                                $tblTagModel = new \tblTagsModel();
                                $tblTagModel->name = $i_tag;
                                $tblTagModel->slug = \Tree::gen_slug($i_tag);
                                $tblTagModel->save();
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $tblTagModel->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 3;
                                $tblTagViewModel->save();
                            }else{
                                $tblTagViewModel = new \tblTagsRelationshipModel();
                                $tblTagViewModel->tags_id = $check->id;
                                $tblTagViewModel->post_id = $tbl_detail_news->id;
                                $tblTagViewModel->type = 3;
                                $tblTagViewModel->save();
                            }
                        }
                    }
                }
            }
            
            return \Redirect::action('\ADMIN\PageController@getView');
        }else{
            return \Redirect::back()->withInput()->withErrors('<li>Bạn chưa nhập ngôn ngữ '.$tblLangModel->name.'</li>');
        }   
    }

    public function postDelete() {

        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblPageLangModel::where('page_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 2));
            
           
            return $this->getView()->render();
        }
    }

    public function postPostConfirm() {
        if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblPageLangModel::where('page_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('status' => 1));
            return $this->getView()->render();
        }
    }
    public function postDelete1() {
       if (\Request::ajax()) {
            $all_input = \Input::all();
            \tblPageLangModel::where('news_id', $all_input['id'])->where('lang_id', $this->d_lang)->update(array('checker_id1' => \Auth::user()->id));
            return $this->getView()->render();
        }
    }
   

}
