<?php



namespace FontEnd;



use FontEnd,

    View,

    Lang,

    Redirect,

    Session,

    Input,

    Cart,

    Validator;



class BookingController extends \BaseController {

	private $lang_code = '';

    // Hạm chạy khi gọi class

    public function __construct() {

        if(\Cookie::has('cookie_lang')){

            $this->lang_code = \Cookie::get('cookie_lang');

        }else{

            $this->lang_code = \Config::get('all.all_config')->website_lang;        

        }            

    }

	private function postSendSMSVMGCSKH($phone_list, $brandname, $message, $type, $user, $key) {

        $wsdl_url = 'http://brandsms.vn:8018/VMGAPI.asmx?wsdl';
        $client = new \nusoap_client($wsdl_url, 'wsdl');

        $err = $client->getError();
		
        if ($err) {

            // Display the error

            echo '<h2>Constructor error</h2>' . $err;

            // At this point, you know the call that follows will fail

                exit();

        }

        $params = array(

            'msisdn' => $phone_list,

            'alias' => $brandname,

            'message' => $message,

            'sendTime'=>'',

            'authenticateUser' => $user,

            'authenticatePass' => $key,

        );
        $return = $client->call('BulkSendSms',$params);
		return $return;  

            

    }

    public function getSeat($show_id){

        if(\Session::has('session_data')){

            \Session::forget('session_data');

        }

        $show = \tblShowModel::leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')->where('tbl_show_lang.show_id',$show_id)->where('tbl_show.is_sell',0)->where('tbl_show_lang.status',1)->where('tbl_show_lang.lang_id',$this->lang_code)->first();

        if(count($show)>0){

            \tblTicketShowModel::where('time_reset','<=',time())->where('time_reset','!=','')->where('status',2)->where('show_id',$show_id)->update(['time_reset'=>'','status'=>1]);

            $data = \tblTicketShowModel::leftJoin('tbl_ticket_lang','tbl_ticket_show.ticket_id','=','tbl_ticket_lang.ticket_id')->where('show_id',$show_id)->select('tbl_ticket_show.*','tbl_ticket_lang.name')->where('tbl_ticket_lang.lang_id',$this->lang_code)->get();        

            $place = \tblPlaceLangModel::leftJoin('tbl_place_map','tbl_place_lang.place_id','=','tbl_place_map.place_id')->where('tbl_place_lang.lang_id',$this->lang_code)->where('tbl_place_lang.place_id',$show->place_id)->get();



            $ticket = \tblTicketShowModel::leftJoin('tbl_ticket_lang','tbl_ticket_show.ticket_id','=','tbl_ticket_lang.ticket_id')->where('tbl_ticket_show.show_id',$show_id)->groupBy('tbl_ticket_show.ticket_id')->get();

            return \View::make('frontend.booking')->with('show', $show)->with('data', $data)->with('ticket', $ticket)->with('place', $place);

        }else{

            return \Redirect::to(\URL::route('404'));

        }        

    }

	public function testsms(){
		//$send = $this->postSendSMSVMGCSKH('84988337906','VNSO.ORG.VN','dgfdg',0,'buffco','huysoi1985');
		//dd($send);
		
		exit();
	}

    public function get404(){
		
        return \View::make('frontend.404');

    }



    /* checkout */

    public function getConfirm(){

        if(\Session::has('session_data')){

            $session = \Session::get('session_data');

            $seat_name = $session['seat_name'];

            $ticket_type = $session['ticket_type'];

            $show_id = $session['show_id'];

            $show = \tblShowModel::leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')->where('tbl_show_lang.show_id',$show_id)->where('tbl_show.is_sell',0)->where('tbl_show_lang.status',1)->where('tbl_show_lang.lang_id',$this->lang_code)->first();

            if(count($show)>0){

                $data = \tblTicketShowModel::leftJoin('tbl_ticket_lang','tbl_ticket_show.ticket_id','=','tbl_ticket_lang.ticket_id')->where('show_id',$show_id)->select('tbl_ticket_show.*','tbl_ticket_lang.name')->where('tbl_ticket_lang.lang_id',$this->lang_code)->get();        

                $place = \tblPlaceLangModel::leftJoin('tbl_place_map','tbl_place_lang.place_id','=','tbl_place_map.place_id')->where('tbl_place_lang.lang_id',$this->lang_code)->where('tbl_place_lang.place_id',$show->place_id)->get();



                $ticket = \tblTicketShowModel::leftJoin('tbl_ticket_lang','tbl_ticket_show.ticket_id','=','tbl_ticket_lang.ticket_id')->where('tbl_ticket_show.show_id',$show_id)->groupBy('tbl_ticket_show.ticket_id')->get();

                $i=0;

                $total=0;

                foreach($seat_name as $item){

                    if(isset($ticket_type[$i])){

                        $ticket_id=$ticket_type[$i];

                    }else{

                        $ticket_id=0;

                    }   

                    $check = \tblTicketShowModel::where('show_id',$show_id)->where('ticket_id',$ticket_id)->where('seat_name_orin',$item)->first();

                    if(count($check)>0){

                        $total+= $check->price;

                    }

                    \tblTicketShowModel::where('ticket_id',$ticket_id)->where('seat_name_orin',$item)->where('show_id',$show_id)->update(['time_reset'=>strtotime('+5 minutes',time())]);

                    $i++;

                }



                return \View::make('frontend.confirm')->with('seat_name', $seat_name)->with('ticket_type', $ticket_type)->with('total', $total)->with('show', $show)->with('data', $data)->with('ticket', $ticket)->with('place', $place);

            }else{

                return \Redirect::to(\URL::route('404'));

            }

        }else{

            return \Redirect::to(\URL::route('404'));

        }

        

    }

	/* link tra ve cod */

    public function getCOD(){

		$setting = \FontEnd\tblSettingModel::getAll();

        $lang_cod_success = 'lang_cod_success_'.$this->lang_code;  

		if(\Session::has('session_cod')){

			$order_id = \Session::get('session_cod');

			$msg = $setting->$lang_cod_success;

			$data_order = \tblOrderModel::find($order_id);

			$data_detail = \tblOrderDetailModel::where('order_id',$order_id)->get();

			$show = \tblShowModel::leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')

						->leftJoin('tbl_place_lang','tbl_show.place_id','=','tbl_place_lang.place_id')

						->select('tbl_show_lang.*','tbl_show.time_show','tbl_place_lang.name as place_name')->where('tbl_place_lang.lang_id',$this->lang_code)->where('tbl_show_lang.lang_id',$this->lang_code)->where('tbl_show_lang.show_id',$data_order->show_id)->first();

			$ticket = \tblTicketShowModel::leftJoin('tbl_ticket_lang','tbl_ticket_show.ticket_id','=','tbl_ticket_lang.ticket_id')->where('tbl_ticket_show.show_id',$data_order->show_id)->groupBy('tbl_ticket_show.ticket_id')->get();

			foreach($ticket as $key=>$value){

				$check = \tblOrderDetailModel::where('order_id',$order_id)->where('seat_type',$value->ticket_id)->count();

				if($check<=0){

					unset($ticket[$key]);

				}

			}		
			/* gui tin nhan */
			$sms_content='';
			$sms_content.="Dat ve thanh cong.";
			$sms_content.="Ma don ve:".$data_order->code.",";
			$sms_content.="Ct:".$show->name.",";
			$sms_content.="Ngay:".date('d/m/Y H:i',$show->time_show).",";
			$sms_content.="Dia diem:".$show->place_name.",";
			$html='';
			$html.='<table style="margin-top: 15px" class="table-ticket">';
			$html.='<tr><td>Mã đơn vé</td><td>'.$data_order->code.'</td></tr>';
			$html.='<tr><td>Chương trình</td><td>'.$show->name.'</td></tr>';
			$html.='<tr><td>Thời gian</td><td>'.date('d/m/Y H:i',$show->time_show).'</td></tr>';
			$html.='<tr><td>Địa điểm</td><td>'.$show->place_name.'</td></tr>';    
			if(count($data_detail)>0){
				$sms_content.="Ghe:";
				$html.='<tr><td>Ghế</td><td>';
				foreach($data_detail as $item){
					$sms_content.=$item->seat_name_orin.' ';
					$html.='<span>'.$item->seat_name_orin.'</span> ';
				}
				$html.='</td></tr>';
			}       
			if(!\Session::has('session_email')){
				$list_email = array($data_order->cus_email,'dophuong0880@gmail.com');  
				\Mailgun::send('emails.auth.infor', array('noidung' => $html), function($message) use($list_email){
					$message->to($list_email)->subject('Thư thông báo');
				});
				$total_sms = \tblCustomerModel::orderBy('id','asc')->first();
				if(count($total_sms)>0){
					if($total_sms->total > 1100){
						$this->postSendSMSVMGCSKH($data_order->cus_phone,'VNSO.ORG.VN',$sms_content,0,'buffco','huysoi1985');
						
						$old_total = $total_sms->total;
						$total_sms->total = $old_total-1100;
						$total_sms->save();
					}
				}
				\Session::put('session_email',$data_order->code);
			}
			return \View::make('frontend.cod')->with('msg',$msg)->with('data_order',$data_order)->with('data_detail',$data_detail)->with('show',$show)->with('ticket',$ticket);            

		}else{

			return \Redirect::to(\URL::route('404'));

		}

	}

	public function getIpn(){
		$inputData = array();
        $returnData = array();
        $data = $_REQUEST;
        foreach ($data as $key => $value) {
            if (substr($key, 0, 4) == "vnp_") {
                $inputData[$key] = $value;
            }
        }
        if (isset($_GET['vnp_SecureHash']) && isset($_GET['vnp_ResponseCode']) && isset($_GET['vnp_SecureHashType'])) {
            $vnp_SecureHash = $inputData['vnp_SecureHash'];
            $hashSecret = 'UJJAENBFZDVLOQBCRVFHZWMGAJNSVWLM';
            unset($inputData['vnp_SecureHashType']);
            unset($inputData['vnp_SecureHash']);
            ksort($inputData);
            $i = 0;
            $hashData = "";
            foreach ($inputData as $key => $value) {
                if ($i == 1) {
                    $hashData = $hashData . '&' . $key . "=" . $value;
                } else {
                    $hashData = $hashData . $key . "=" . $value;
                    $i = 1;
                }
            }
            $vnpTranId = $inputData['vnp_TransactionNo'];
            $vnp_BankCode = $inputData['vnp_BankCode']; 
            $secureHash = md5($hashSecret . $hashData);

            $orderId = $inputData['vnp_TxnRef'];
            try {
                if ($secureHash == $vnp_SecureHash) {
                    $order = \tblOrderModel::where('code',$orderId)->first();
                    if (count($order) > 0) {
                        if ($order->status == 0) {
                            if ($inputData['vnp_ResponseCode'] == '00') {
								/* lay thog tin hien thi ra ngoai */
								$data_order = \tblOrderModel::where('code',$_GET['vnp_TxnRef'])->first();
								$data_detail = \tblOrderDetailModel::where('order_id',$order->id)->get();
								$show = \tblShowModel::leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')
											->leftJoin('tbl_place_lang','tbl_show.place_id','=','tbl_place_lang.place_id')
											->select('tbl_show_lang.*','tbl_show.time_show','tbl_place_lang.name as place_name')->where('tbl_place_lang.lang_id',$this->lang_code)->where('tbl_show_lang.lang_id',$this->lang_code)->where('tbl_show_lang.show_id',$data_order->show_id)->first();

								$ticket = \tblTicketShowModel::leftJoin('tbl_ticket_lang','tbl_ticket_show.ticket_id','=','tbl_ticket_lang.ticket_id')->where('tbl_ticket_show.show_id',$data_order->show_id)->groupBy('tbl_ticket_show.ticket_id')->get();

								foreach($ticket as $key=>$value){
									$check = \tblOrderDetailModel::where('order_id',$order->id)->where('seat_type',$value->ticket_id)->count();
									if($check<=0){
										unset($ticket[$key]);
									}
								}
                                /* gui tin nhan */
								$sms_content='';
								$sms_content.="Dat ve thanh cong.";
								$sms_content.="Ma don ve:".$order->code.",";
								$sms_content.="Ct:".$show->name.",";
								$sms_content.="Ngay:".date('d/m/Y H:i',$show->time_show).",";
								$sms_content.="Dia diem:".$show->place_name.",";
								$html='';
								$html.='<table style="margin-top: 15px" class="table-ticket">';
								$html.='<tr><td>Mã đơn vé</td><td>'.$order->code.'</td></tr>';
								$html.='<tr><td>Chương trình</td><td>'.$show->name.'</td></tr>';
								$html.='<tr><td>Thời gian</td><td>'.date('d/m/Y H:i',$show->time_show).'</td></tr>';
								$html.='<tr><td>Địa điểm</td><td>'.$show->place_name.'</td></tr>';       
								/* update status order */
								\tblOrderModel::where('code',$orderId)->update(['status'=>1]);
								/* update status ghe */
								$detail = \tblOrderDetailModel::where('order_id',$order->id)->get();
								if(count($detail)>0){
									$sms_content.="Ghe:";
									$html.='<tr><td>Ghế</td><td>';
									foreach($detail as $item){
										\tblTicketShowModel::where('ticket_id',$item->seat_type)->where('seat_name_orin',$item->seat_name)->where('show_id',$order->show_id)->update(['status'=>3,'time_reset'=>'']);

										$sms_content.=$item->seat_name_orin.' ';
										$html.='<span>'.$item->seat_name_orin.'</span> ';
									}
									$html.='</td></tr>';
								}       
								$list_email = array($order->cus_email,'dophuong0880@gmail.com');  
								\Mailgun::send('emails.auth.infor', array('noidung' => $html), function($message) use($list_email){
									$message->to($list_email)->subject('Thư thông báo');
								});
								$total_sms = \tblCustomerModel::orderBy('id','asc')->first();
								if(count($total_sms)>0){
									if($total_sms->total > 1100){
										$this->postSendSMSVMGCSKH($order->cus_phone,'VNSO.ORG.VN',$sms_content,0,'buffco','huysoi1985');
										
										$old_total = $total_sms->total;
										$total_sms->total = $old_total-1100;
										$total_sms->save();
									}
								}
								
                            }

                            $returnData['RspCode'] = '00';
                            $returnData['Message'] = 'Confirm Success';
                        } else {
                            $returnData['RspCode'] = '02';
                            $returnData['Message'] = 'Order already confirmed';
                        }
                    } else {
                        $returnData['RspCode'] = '01';
                        $returnData['Message'] = 'Order not found';
                    }
                } else {
                    $returnData['RspCode'] = '97';
                    $returnData['Message'] = 'Chu ky khong hop le';
                }
            } catch (Exception $e) {
                $returnData['RspCode'] = '99';
                $returnData['Message'] = 'Unknow error';
            }
        } else {
            $returnData['RspCode'] = '97';
            $returnData['Message'] = 'Chu ky khong hop le';
        }
        echo json_encode($returnData);
	}

    /* url return tu vnpay*/

    public function getReturn(){

        $setting = \FontEnd\tblSettingModel::getAll();

        $lang_success_trade = 'lang_success_trade_'.$this->lang_code;  

        $lang_fail_trade= 'lang_fail_trade_'.$this->lang_code;

        $lang_key_trade= 'lang_key_trade_'.$this->lang_code;

		

        if(isset($_GET['vnp_SecureHash']) && isset($_GET['vnp_ResponseCode']) && isset($_GET['vnp_SecureHashType'])){

            $hashSecret = "UJJAENBFZDVLOQBCRVFHZWMGAJNSVWLM";  

            $vnp_SecureHash = $_GET['vnp_SecureHash'];

            $params = array();

            foreach ($_GET as $key => $value) {

                $params[$key] = $value;

            }

            unset($params['vnp_SecureHashType']);

            unset($params['vnp_SecureHash']);

            ksort($params);

            $i = 0;

            $hashData = "";

            foreach ($params as $key => $value) {

                if ($i == 1) {

                    $hashData = $hashData . '&' . $key . "=" . $value;

                } else {

                    $hashData = $hashData . $key . "=" . $value;

                    $i = 1;

                }

            }



            $secureHash = md5($hashSecret . $hashData);

            if ($secureHash == $vnp_SecureHash) {

                if ($_GET['vnp_ResponseCode'] == '00') {

                    $order = \tblOrderModel::where('code',$_GET['vnp_TxnRef'])->first();

                    /* lay thog tin hien thi ra ngoai */

                    $data_order = \tblOrderModel::where('code',$_GET['vnp_TxnRef'])->first();

                    $data_detail = \tblOrderDetailModel::where('order_id',$order->id)->get();

                    $show = \tblShowModel::leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')

                                ->leftJoin('tbl_place_lang','tbl_show.place_id','=','tbl_place_lang.place_id')

                                ->select('tbl_show_lang.*','tbl_show.time_show','tbl_place_lang.name as place_name')->where('tbl_place_lang.lang_id',$this->lang_code)->where('tbl_show_lang.lang_id',$this->lang_code)->where('tbl_show_lang.show_id',$data_order->show_id)->first();

                    $ticket = \tblTicketShowModel::leftJoin('tbl_ticket_lang','tbl_ticket_show.ticket_id','=','tbl_ticket_lang.ticket_id')->where('tbl_ticket_show.show_id',$data_order->show_id)->groupBy('tbl_ticket_show.ticket_id')->get();

                    foreach($ticket as $key=>$value){

                        $check = \tblOrderDetailModel::where('order_id',$order->id)->where('seat_type',$value->ticket_id)->count();

                        if($check<=0){

                            unset($ticket[$key]);

                        }

                    }
                    

                    $msg = $setting->$lang_success_trade;

                } else {

                    $data_order=[];

                    $data_detail=[];

                    $show=[];

                    $msg = $setting->$lang_fail_trade;

                }

            } else {

                $data_order=[];

                $data_detail=[];

                $show=[];

                $msg = $setting->$lang_key_trade;

            }             

            return \View::make('frontend.return')->with('msg',$msg)->with('data_order',$data_order)->with('data_detail',$data_detail)->with('show',$show)->with('ticket',$ticket);            

        }else{

            return \Redirect::to(\URL::route('404'));

        }

        

    }

    function generateRandomString($length = 10) {

        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {

            $randomString .= $characters[rand(0, strlen($characters) - 1)];

        }

        return $randomString;



    }

    public function postPayment(){

        $setting = \FontEnd\tblSettingModel::getAll();

        $lang_session_invalid = 'lang_session_invalid_'.$this->lang_code;  

        $lang_rules_invalid= 'lang_rules_invalid_'.$this->lang_code;

        $lang_info_invalid= 'lang_info_invalid_'.$this->lang_code;

		$lang_ptype_err= 'lang_ptype_err_'.$this->lang_code;

		$lang_ptype_required= 'lang_ptype_required_'.$this->lang_code;

        $checkempty='';

        $out_put = '';

        if(Input::get('name')==''){

            $checkempty.='0';

        }else{

            $checkempty.='1';

        }

        if(!preg_match('/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/', Input::get('email'))){

            $checkempty.='0';

        }else{

            $checkempty.='1';

        }

        if(Input::get('phone')==''){

            $checkempty.='0';

        }else{

            $checkempty.='1';

        }

        $flagempty = strpos($checkempty, '0');

        if ($flagempty !== false) {

            $out_put = array(

                'check' => 0,

                'content' => $setting->$lang_info_invalid,

                'alert'=>'msg_notify'

            );

            $return_output = json_encode($out_put);

        }else{

			if(!\Input::has('p_type')){

				$out_put = array(

					'check' => 0,

					'content' => $setting->$lang_ptype_required,

					'alert'=>'msg_notify'

				);

				$return_output = json_encode($out_put);

			}else{		

				$arr_p_type=array('0','1');

				if(!in_array(\Input::get('p_type'),$arr_p_type)){

					$out_put = array(

						'check' => 0,

						'content' => $setting->$lang_ptype_err,

						'alert'=>'msg_notify'

					);

					$return_output = json_encode($out_put);

				}else{					

					if(Input::get('agree')=='false'){

						$out_put = array(

							'check' => 0,

							'content' => $setting->$lang_rules_invalid,

							'alert'=>'msg_notify'

						);

						$return_output = json_encode($out_put);

					}else{

						if(\Session::has('session_data')){

							$session = \Session::get('session_data');

							$seat_name = $session['seat_name'];

							$ticket_type = $session['ticket_type'];

							$show_id = $session['show_id'];

							$i=0;

							$total=0;

							/* lap order */

							$order = new \tblOrderModel();

							$code = $this->generateRandomString() . rand(10000000, 100000000);

							$check = \tblOrderModel::where('code',$code)->first();

							$count = \tblOrderModel::count();

							if(count($check)>0){

								$code .= $code.$count;

							}

							$order->code=$code;

							$order->show_id=$show_id;

							$order->cus_name=\Input::get('name');

							$order->cus_phone=\Input::get('phone');

							$order->cus_email=\Input::get('email');

							$order->cus_address=\Input::get('address');

							$order->order_type=1;

							$order->payment_type=\Input::get('p_type'); /* 0: vnpay 1:cod*/

							$order->status=0;

							/* */

							$order->save();

							$data = [];

							foreach($seat_name as $item){

								$seat_name_orin= $item;

								if(isset($ticket_type[$i])){

									$ticket_id=$ticket_type[$i];

								}else{

									$ticket_id=0;

								}   

								$check = \tblTicketShowModel::where('show_id',$show_id)->where('ticket_id',$ticket_id)->where('seat_name_orin',$item)->first();

								if(count($check)>0){

									$total+= $check->price;

									$seat_name_orin = $check->seat_name;

								}

								if(\Input::get('p_type')==0){

									\tblTicketShowModel::where('ticket_id',$ticket_id)->where('seat_name_orin',$item)->where('show_id',$show_id)->update(['status'=>2,'time_reset'=>strtotime('+15 minutes',time())]);

								}else{

									\tblTicketShowModel::where('ticket_id',$ticket_id)->where('seat_name_orin',$item)->where('show_id',$show_id)->update(['status'=>3,'time_reset'=>'']);

								}

								$i++;

								$data[] = ['order_id'=>$order->id,'seat_name_orin'=>$item,'seat_name'=>$seat_name_orin,'seat_price'=>$check->price,'seat_type'=>$check->ticket_id];

							}

							\DB::table('tbl_order_detail')->insert($data);

							if(\Input::get('p_type')==0){

								$vnp_Url = "https://pay.vnpay.vn/vpcpay.html";

								$inputData = array(

									"vnp_TmnCode" => "KENNYVNS", 

									"vnp_Amount" => $total*100,

									"vnp_Command" => "pay",

									"vnp_CreateDate" => date('YmdHis'),

									"vnp_CurrCode" => "VND",

									"vnp_IpAddr" => $_SERVER['REMOTE_ADDR'],

									"vnp_Locale" => "vn",

									"vnp_OrderInfo" => "Thanh toan online",

									"vnp_OrderType" => "topup",

									"vnp_ReturnUrl" => \URL::route('return'),

									"vnp_TxnRef" => $code,

									"vnp_Version" => "2.0.0",

								);

								$vnpay_hash_secret= "UJJAENBFZDVLOQBCRVFHZWMGAJNSVWLM";

								$bankcode = "";

								$out = $inputData;

								if ($bankcode!='' && $bankcode != NULL) {

									$out = array_merge($inputData, array("vnp_BankCode" => $bankcode));

								}

								$query = "";

								$i = 0;

								$hashdata = "";

								ksort($out);

								foreach ($out as $key => $value) {

									if ($i == 1) {

										$hashdata .= '&' . $key . "=" . $value;

									} else {

										$hashdata .= $key . "=" . $value;

										$i = 1;

									}

									$query .= urlencode($key) . "=" . urlencode($value) . '&';

								}

								$vnp_Url = $vnp_Url . "?" . $query;

								if (isset($vnpay_hash_secret)) {

									$vnpSecureHash = md5($vnpay_hash_secret . $hashdata);

									$vnp_Url .= 'vnp_SecureHashType=MD5&vnp_SecureHash=' . $vnpSecureHash;

								}

								/* xoa session */

								if(\Session::has('session_data')){

									\Session::forget('session_data');

								}

								$out_put = array(

									'check' => 2,

									'content' => '',

									'redirect'=> $vnp_Url,

									'alert'=>'msg_notify'

								);

								$return_output = json_encode($out_put);

							}else{

								/* xoa session */

								if(\Session::has('session_data')){

									\Session::forget('session_data');

								}

								if(\Session::has('session_cod')){

									\Session::forget('session_cod');

								}

								\Session::put('session_cod',$order->id);

								$out_put = array(

									'check' => 2,

									'content' => '',

									'redirect'=> \URL::route('cod'),

									'alert'=>'msg_notify'

								);

								$return_output = json_encode($out_put);

							}

						}else{

							$out_put = array(

								'check' => 1,

								'content' => $setting->$lang_session_invalid,

								'redirect'=> \URL::route('404'),

								'alert'=>'msg_notify'

							);

							$return_output = json_encode($out_put);

						}

						

					}

				}

			}

        }

        return $return_output;      

      

    }



    public function postClear(){

        $setting = \FontEnd\tblSettingModel::getAll();

        $lang_session_invalid = 'lang_session_invalid_'.$this->lang_code;  

        if(\Session::has('session_data')){

            \Session::forget('session_data');

        }

        $out_put = '';

        $out_put = array(

            'check' => 1,

            'content' => $setting->$lang_session_invalid,

            'redirect'=> \URL::route('chuongtrinh'),

            'alert'=>'msg_notify'

        );

        $return_output = json_encode($out_put);

        return $return_output;

    }



    public function postHoldseat(){

      

     // dd($check);

        /*

        $captcha =  \Input::get('g-recaptcha-response');

        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Ld84xETAAAAAOKsRhnLt2a8iFuoZTaZjwDaOwY_&response=".$captcha);

        $response = json_decode($response);

        

        */

        $setting = \FontEnd\tblSettingModel::getAll();

        $lang_seat_invalid = 'lang_seat_invalid_'.$this->lang_code;

        $lang_show_invalid = 'lang_show_invalid_'.$this->lang_code;  

       

        $out_put = '';

        /*check show id*/

        $show = \tblShowModel::leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')->where('tbl_show_lang.show_id',\Input::get('show'))->where('tbl_show.is_sell',0)->where('tbl_show_lang.status',1)->where('tbl_show_lang.lang_id',$this->lang_code)->first();

        if(count($show)>0){

            if(\Input::has('seats') && count(\Input::get('seats')>0)){

                /* check xem ghe co ton tai ko */

                $check_seat_name='';

                $i=0;

                /* loai ve */

                $explode_type = \Input::get('zones');

                foreach(\Input::get('seats') as $item){

                    if(isset($explode_type[$i])){

                        $ticket_id=$explode_type[$i];

                    }else{

                        $ticket_id=0;

                    }   

                    $check = \tblTicketShowModel::where('show_id',\Input::get('show'))->where('ticket_id',$ticket_id)->where('seat_name_orin',$item)->where('status',1)->first();

                    if(count($check)>0){

                        $check_seat_name.='1';

                    }else{

                        $check_seat_name.='0';

                    }

                    $i++;

                }

                if (strpos($check_seat_name, '0') !== false) {

                    $out_put = array(

                        'check' => 0,

                        'content' => $setting->$lang_seat_invalid,

                        'alert'=>'msg_notify'

                    );

                    $return_output = json_encode($out_put);

                }else{

                    $j=0;

                    /* update status ghế sang đang chờ */

                    foreach(\Input::get('seats') as $i_seat){

                        if(isset($explode_type[$j])){

                            \tblTicketShowModel::where('ticket_id',$explode_type[$j])->where('seat_name_orin',$i_seat)->where('show_id',\Input::get('show'))->update(['status'=>2]);

                            /* update thoi gian hold */

                            \tblTicketShowModel::where('ticket_id',$explode_type[$j])->where('seat_name_orin',$i_seat)->where('show_id',\Input::get('show'))->update(['time_reset'=>strtotime('+5 minutes',time())]);

                            

                        }

                        $j++;

                    }

                    /* luu session */

                    $arr_session = ['seat_name'=>\Input::get('seats'),'ticket_type'=>\Input::get('zones'),'show_id'=>\Input::get('show')];

                    if(\Session::has('session_data')){

                        \Session::forget('session_data');

                    }

                    \Session::put('session_data',$arr_session);

                    $out_put = array(

                        'check' => 1,

                        'content' => 'Next',

                        'redirect'=> \URL::route('checkout'),

                        'alert'=>'msg_notify'

                    );

                    $return_output = json_encode($out_put);

                }

            }else{

                $out_put = array(

                    'check' => 0,

                    'content' => $setting->$lang_seat_invalid,

                    'alert'=>'msg_notify'

                );

                $return_output = json_encode($out_put);

            }

        }else{

            $out_put = array(

                'check' => 0,

                'content' => $setting->$lang_show_invalid,

                'alert'=>'msg_notify'

            );

            $return_output = json_encode($out_put);

        }

        return $return_output;

    }

   

}