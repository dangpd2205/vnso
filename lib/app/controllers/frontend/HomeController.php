<?php

namespace FontEnd;

use FontEnd,
    View,
    Lang,
    Redirect,
    Session,
    Input,
    Cart,
    Validator;

class HomeController extends \BaseController {
	private $lang_code = '';
    // Hạm chạy khi gọi class
    public function __construct() {
        if(\Cookie::has('cookie_lang')){
            $this->lang_code = \Cookie::get('cookie_lang');
        }else{
            $this->lang_code = \Config::get('all.all_config')->website_lang;        
        }            
    }
	public function getManuf($id=''){
		$data = \tblManufModel::find($id);
		if(count($data)>0){
			return View::make('frontend.manuf')->with('data',$data);
		}
	}
    public function getCookie($id=''){
        
        if(\Cookie::has('cookie_lang')){
            \Cookie::forget('cookie_lang');
            \Cookie::queue('cookie_lang',$id,60);
        }else{
            \Cookie::queue('cookie_lang',$id,60);
        }
       return Redirect::route("home");
    }
	public function getSponsor(){
		return View::make('frontend.sponsor');
	}
	public function postSponsor(){
		$rules = array(
			"name" => "required",
			"address" => "required",
			"phone" => "required|numeric",
			"email" => "required|email"           
		);
		$setting = \FontEnd\tblSettingModel::getAll();
		$lang_name = 'lang_name_'.$this->lang_code;
		$lang_address = 'lang_address_'.$this->lang_code;
		$lang_phone = 'lang_phone_'.$this->lang_code;
		$lang_email_required = 'lang_email_required_'.$this->lang_code;
		$lang_email_format = 'lang_email_format_'.$this->lang_code;  
		$lang_name_required = 'lang_name_required_'.$this->lang_code;  
		$lang_address_required = 'lang_address_required_'.$this->lang_code;  
		$lang_phone_required = 'lang_phone_required_'.$this->lang_code;  
		$lang_phone_format = 'lang_phone_format_'.$this->lang_code;  
		$lang_changeprofile_success = 'lang_changeprofile_success_'.$this->lang_code;  
		$attr = array(
            'name.required' => $setting->$lang_name_required,
            'email.email' => $setting->$lang_email_format,
            'email.required' => $setting->$lang_email_required,
            'phone.required' => $setting->$lang_phone_required,
            'phone.numeric' => $setting->$lang_phone_format,
            'address.required' => $setting->$lang_address_required,
        );
		$field = array(
            'name' => $setting->$lang_name,
            'email' => "Email",
            'phone' => $setting->$lang_phone,
            'address' => $setting->$lang_address,
        );
		$input_all = \Input::all();
        $validate = \Validator::make($input_all, $rules, $attr, $field);
        if (!$validate->fails()) {
			$sponsor = new \tblSponsorModel();
			$sponsor->name=\Input::get('name');
			$sponsor->bod=strtotime(\Input::get('dob'));
			$sponsor->sex=\Input::get('sex');
			$sponsor->city=\Input::get('city');
			$sponsor->job=\Input::get('job');
			$sponsor->address=\Input::get('address');
			$sponsor->phone=\Input::get('phone');
			$sponsor->email=\Input::get('email');
			$sponsor->fax=\Input::get('fax');
			$sponsor->note=\Input::get('note');
			$sponsor->type=\Input::get('customsign');
			$sponsor->save();
			\Session::flash('sucessful',$setting->$lang_changeprofile_success);
			return \Redirect::back();
		}else{
			return \Redirect::back()->withInput()->withErrors($validate->messages()->all('<li>:message</li>'));
		}
		
	}

	public function postSub(){
		$setting = \FontEnd\tblSettingModel::getAll();
		$lang_email_required = 'lang_email_required_'.$this->lang_code;
		$lang_email_format = 'lang_email_format_'.$this->lang_code;  
		$lang_email_exist = 'lang_email_exist_'.$this->lang_code;  
		$lang_changeprofile_success= 'lang_changeprofile_success_'.$this->lang_code;
		$lang_changeprofile_fail= 'lang_changeprofile_fail_'.$this->lang_code;
        $out_put = '';
        if (Input::get('email') == '') {
            $out_put = array(
                'check' => 0,
                'content' => $setting->$lang_email_required,
                'alert'=>'messages_sub'
            );
            $return_output = json_encode($out_put);
        }else if(!preg_match('/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/', Input::get('email') )){
            $out_put = array(
                'check' => 0,
                'content' => $setting->$lang_email_format,
                'alert'=>'messages_sub'
            );
            $return_output = json_encode($out_put);
        }else {

            $check = \FontEnd\tblSubcribeModel::where('email',Input::get('email'))->first();
            if(count($check)>0){
                $out_put = array(
                    'check' => 0,
                    'content' => $setting->$lang_email_exist,
                    'alert'=>'messages_sub'
                );
                $return_output = json_encode($out_put);
            }else{
                $tblSubcribeModel = new \FontEnd\tblSubcribeModel();
                $tblSubcribeModel->email = Input::get('email');
                $tblSubcribeModel->save();               
                
                $out_put = array(
                    'check' => 1,
                    'content' => $setting->$lang_changeprofile_success,
                    'alert'=>'messages_sub',
                    'form'=>'sub-form'
                );
                $return_output = json_encode($out_put);
            }           
            
        }
        echo $return_output;
        
    }

    public function postVote(){
    	
    }

    public function postLike(){
    	
    }

    public function postLike1(){
    	
    }

    public function postLike2(){
      
    }
  
    public function getTrangChu(){  
		$setting = \FontEnd\tblSettingModel::getAll();
        $website_cateshow = 'website_cate_show_'.$this->lang_code;        
        if(isset($setting->$website_cateshow)){
            $data_cateshow = $this->getCate($setting->$website_cateshow);
            if(count($data_cateshow)>0){
                foreach($data_cateshow as $item){
                    $artist[] = \tblArtistLangModel::leftJoin('tbl_artist_view','tbl_artist_lang.artist_id','=','tbl_artist_view.artist_id')->leftJoin('tbl_artist_category','tbl_artist_view.cat_id','=','tbl_artist_category.id')->select('tbl_artist_lang.*','tbl_artist_category.name as cate_name')->whereIn('tbl_artist_lang.artist_id',explode(',',$item->artist_id))->where('tbl_artist_lang.status',1)->groupBy('tbl_artist_lang.artist_id')->get();
                    $conductor[] = \tblArtistLangModel::where('tbl_artist_lang.artist_id',$item->conductor_id)->first();
                }
                
            }else{
                $artist=[];
                $conductor=[];
            }
            $cateshow = \tblShowCategoryModel::find($setting->$website_cateshow);
        }else{
            $data_cateshow=[];
            $cateshow=[];
            $artist=[];
            $conductor=[];
        }        
        $website_cateshow1 = 'website_cate_show1_'.$this->lang_code;
        if(isset($setting->$website_cateshow1)){
            $data_cateshow1 = $this->getCate($setting->$website_cateshow1);
            if(count($data_cateshow1)>0){
                foreach($data_cateshow1 as $item){
                    $artist1[] = \tblArtistLangModel::leftJoin('tbl_artist_view','tbl_artist_lang.artist_id','=','tbl_artist_view.artist_id')->leftJoin('tbl_artist_category','tbl_artist_view.cat_id','=','tbl_artist_category.id')->select('tbl_artist_lang.*','tbl_artist_category.name as cate_name')->whereIn('tbl_artist_lang.artist_id',explode(',',$item->artist_id))->where('tbl_artist_lang.status',1)->groupBy('tbl_artist_lang.artist_id')->get();
                    $conductor1[]= \tblArtistLangModel::where('tbl_artist_lang.artist_id',$item->conductor_id)->first();
                }
                
            }else{
                $artist1=[];
                $conductor1=[];
            }
            $cateshow1 = \tblShowCategoryModel::find($setting->$website_cateshow1);
        }else{
            $data_cateshow1=[];
            $cateshow1=[];
            $artist1=[];
            $conductor1=[];
        }   
        $website_cateshow2 = 'website_cate_show2_'.$this->lang_code;
        if(isset($setting->$website_cateshow2)){
            $data_cateshow2 = $this->getCate($setting->$website_cateshow2);
            if(count($data_cateshow2)>0){
                foreach($data_cateshow2 as $item){
                    $artist2[] = \tblArtistLangModel::leftJoin('tbl_artist_view','tbl_artist_lang.artist_id','=','tbl_artist_view.artist_id')->leftJoin('tbl_artist_category','tbl_artist_view.cat_id','=','tbl_artist_category.id')->select('tbl_artist_lang.*','tbl_artist_category.name as cate_name')->whereIn('tbl_artist_lang.artist_id',explode(',',$item->artist_id))->where('tbl_artist_lang.status',1)->groupBy('tbl_artist_lang.artist_id')->get();
                    $conductor2[] = \tblArtistLangModel::where('tbl_artist_lang.artist_id',$item->conductor_id)->first();
                }
                
            }else{
                $artist2=[];
                $conductor2=[];
            }
            $cateshow2 = \tblShowCategoryModel::find($setting->$website_cateshow2);
        }else{
            $data_cateshow2=[];
            $cateshow2=[];
            $artist2=[];
            $conductor2=[];
        }   
        $website_cateshow3 = 'website_cate_show3_'.$this->lang_code;
        if(isset($setting->$website_cateshow3)){
            $data_cateshow3 = $this->getCate($setting->$website_cateshow3);
            if(count($data_cateshow3)>0){
                foreach($data_cateshow3 as $item){
                    $artist3[] = \tblArtistLangModel::leftJoin('tbl_artist_view','tbl_artist_lang.artist_id','=','tbl_artist_view.artist_id')->leftJoin('tbl_artist_category','tbl_artist_view.cat_id','=','tbl_artist_category.id')->select('tbl_artist_lang.*','tbl_artist_category.name as cate_name')->whereIn('tbl_artist_lang.artist_id',explode(',',$item->artist_id))->where('tbl_artist_lang.status',1)->groupBy('tbl_artist_lang.artist_id')->get();
                    $conductor3[] = \tblArtistLangModel::where('tbl_artist_lang.artist_id',$item->conductor_id)->first();
                }
                
            }else{
                $artist3=[];
                $conductor3=[];
            }
            $cateshow3 = \tblShowCategoryModel::find($setting->$website_cateshow3);
        }else{
            $data_cateshow3=[];
            $cateshow3=[];
            $artist3=[];
            $conductor3=[];
        }   
        
        $gallery = \tblGalleryModel::leftJoin('tbl_gallery_lang','tbl_gallery.id','=','tbl_gallery_lang.gallery_id')
                                    ->where('tbl_gallery_lang.lang_id',$this->lang_code)
                                    ->where('tbl_gallery_lang.status',1)
									->where('tbl_gallery.pin',1)
                                    ->select('tbl_gallery.avatar','tbl_gallery_lang.name','tbl_gallery.pin')
                                    ->orderBy('tbl_gallery_lang.created_at','desc')->limit(2)->get();
        $news = \tblNewsModel::leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')
                        ->where('tbl_news_detail_lang.lang_id',$this->lang_code)
                        ->where('tbl_news_detail_lang.status',1)
                        ->where('tbl_news.time_post','<=',time())
                        ->orderBy('tbl_news.time_post','desc')->limit(6)->get();
        $sponsor = \tblManufModel::orderBy('position','asc')->get();
        $list_show = \tblShowModel::leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')
                        ->leftJoin('tbl_place_lang','tbl_show.place_id','=','tbl_place_lang.place_id')
                        ->select('tbl_show_lang.*','tbl_show.time_show','tbl_place_lang.name as place_name')
                        ->where('tbl_place_lang.lang_id',$this->lang_code)
                        ->where('tbl_show_lang.lang_id',$this->lang_code)
                        ->where('tbl_show_lang.status',1)
                        ->where('tbl_show.time_show','>',time())
                        ->orderBy('tbl_show.time_show','asc')->limit(3)->get();
        $last_day_year = date('Y-m-d 23:59:59', strtotime('last day of december next year'));
        $first_day_year = date('Y-m-d 00:00:00', strtotime('first day of january this year'));
        $calendar_show = \tblShowModel::leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')
                        ->select('tbl_show_lang.*','tbl_show.time_show')
                        ->where('tbl_show_lang.lang_id',$this->lang_code)
                        ->where('tbl_show_lang.status',1)
                        ->whereBetween('tbl_show.time_show',array(strtotime($first_day_year),strtotime($last_day_year)))
                        ->orderBy('tbl_show.time_show','asc')->get();
        $inday_show = \tblShowModel::leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')
                        ->select('tbl_show_lang.*','tbl_show.time_show')
                        ->where('tbl_show_lang.lang_id',$this->lang_code)
                        ->where('tbl_show_lang.status',1)
                        ->whereBetween('tbl_show.time_show',array(strtotime(date('Y-m-d 00:00:00',time())),strtotime(date('Y-m-d 23:59:59',time()))))
                        ->orderBy('tbl_show.time_show','asc')->get();  
		$website_slider = 'website_slider_'.$this->lang_code;
		$LS_Controller = new \FontEnd\LS_Controller();
		if(isset($setting->$website_slider)){
			$slide = $LS_Controller->layerslider($setting->$website_slider);
		}else{
			$slide='';
		}
		$video = \tblVideoModel::leftJoin('tbl_video_lang','tbl_video.id','=','tbl_video_lang.video_id')
                                    ->where('tbl_video_lang.lang_id',$this->lang_code)
                                    ->where('tbl_video_lang.status',1)
                                    ->select('tbl_video.avatar','tbl_video_lang.*')
                                    ->orderBy('tbl_video_lang.created_at','desc')->limit(5)->get();
        return View::make('frontend.index')->with('video',$video)->with('slide',$slide)->with('inday_show',$inday_show)->with('calendar_show',$calendar_show)->with('conductor',$conductor)->with('conductor1',$conductor1)->with('conductor2',$conductor2)->with('conductor3',$conductor3)->with('artist',$artist)->with('artist1',$artist1)->with('artist2',$artist2)->with('artist3',$artist3)->with('cateshow',$cateshow)->with('cateshow1',$cateshow1)->with('cateshow2',$cateshow2)->with('cateshow3',$cateshow3)->with('data_cateshow',$data_cateshow)->with('data_cateshow1',$data_cateshow1)->with('data_cateshow2',$data_cateshow2)->with('data_cateshow3',$data_cateshow3)->with('list_show',$list_show)->with('data_cateshow',$data_cateshow)->with('gallery',$gallery)->with('news',$news)->with('sponsor',$sponsor);
    }

    private function getCate($cate_id=''){
        $data = \tblShowModel::leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')
                                    ->leftJoin('tbl_show_views','tbl_show.id','=','tbl_show_views.show_id')
                                    ->leftJoin('tbl_show_category','tbl_show_views.cat_id','=','tbl_show_category.id')
                                    ->select('tbl_show_lang.*','tbl_show.*','tbl_show_category.background','tbl_show_category.avatar as avt_cate','tbl_show_category.name as name_cate','tbl_show_category.id as id_cate','tbl_show_category.slug as slug_cate')
                                    ->where('tbl_show_lang.lang_id',$this->lang_code)
                                    ->where('tbl_show_views.cat_id',$cate_id)
                                    ->where('tbl_show_lang.status',1)
                                    ->where('tbl_show.time_show','>',time())
                                    ->groupBy('tbl_show_views.show_id')
                                    ->orderBy('tbl_show.time_show','asc')->limit(3)->get();
        return $data;
    }
	
	public function getContact(){
		return View::make('frontend.contact');
	}
	
   
    public function postContact(){
        $setting = \FontEnd\tblSettingModel::getAll();
        $lang_feedback_success = 'lang_changeprofile_success_' . $this->lang_code;
        $lang_email_required = 'lang_email_required_'.$this->lang_code;
		$lang_name_required = 'lang_name_required_'.$this->lang_code;  
		$lang_content_required = 'lang_content_required_'.$this->lang_code;
		$lang_subject_required = 'lang_subject_required_'.$this->lang_code;		
		$lang_email_format = 'lang_email_format_'.$this->lang_code;  
        $out_put = '';
        
        if(Input::get('name') == ''){
            $out_put = array(
                'check' => 0,
                'content' => $setting->$lang_name_required,
                'alert'=>'messages_contact_notify'
            );
            $return_output = json_encode($out_put);
        }else if(Input::get('message') == ''){
            $out_put = array(
                'check' => 0,
                'content' => $setting->$lang_content_required,
                'alert'=>'messages_contact_notify'
            );
            $return_output = json_encode($out_put);
        }else if(Input::get('subject') == ''){
            $out_put = array(
                'check' => 0,
                'content' => $setting->$lang_subject_required,
                'alert'=>'messages_contact_notify'
            );
            $return_output = json_encode($out_put);
        }else if (Input::get('email') == '') {
            $out_put = array(
                'check' => 0,
                'content' => $setting->$lang_email_required,
                'alert'=>'messages_contact_notify'
            );
            $return_output = json_encode($out_put);
        }else if(!preg_match('/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/', Input::get('email') )){
            $out_put = array(
                'check' => 0,
                'content' => $setting->$lang_email_format,
                'alert'=>'messages_contact_notify'
            );
            $return_output = json_encode($out_put);
        }else {
				$feedback = new \tblFeedbackModel();
                $feedback->email = Input::get('email');
				$feedback->name = Input::get('name');
				
				$feedback->subject = Input::get('subject');
				$feedback->content = Input::get('message');
				$feedback->status=0;
                $feedback->save();
           
			$out_put = array(
				'check' => 1,
				'content' => $setting->$lang_feedback_success,
				'alert'=>'messages_contact_notify',
				'form'=>'contactForm'
			);
			$return_output = json_encode($out_put);
            
        }
        echo $return_output;
    }
	public function getVideo(){
		$video = \tblVideoModel::leftJoin('tbl_video_lang','tbl_video.id','=','tbl_video_lang.video_id')
								->where('tbl_video_lang.status',1)
								->where('tbl_video_lang.lang_id',$this->lang_code)
								->select('tbl_video_lang.*','tbl_video.avatar')
								->paginate(12);
		
		return View::make('frontend.video')->with('video',$video);
	}
	public function getGallery(){
		$gallery = \tblGalleryModel::leftJoin('tbl_gallery_lang','tbl_gallery.id','=','tbl_gallery_lang.gallery_id')
								->where('tbl_gallery_lang.status',1)
								->where('tbl_gallery_lang.lang_id',$this->lang_code)
								->paginate(12);
		return View::make('frontend.gallery')->with('gallery',$gallery);
	}
	
	public function getArtist(){
        $cate = \tblArtistCategoryModel::where('lang_id',$this->lang_code)->get();
        if(count($cate)>0){
            foreach($cate as $item){
                $artist[] = \tblArtistModel::leftJoin('tbl_artist_lang','tbl_artist.id','=','tbl_artist_lang.artist_id')
                        ->leftJoin('tbl_artist_view','tbl_artist.id','=','tbl_artist_view.artist_id')
                        ->where('tbl_artist_view.cat_id',$item->id)
                        ->where('tbl_artist_lang.lang_id',$this->lang_code)
                        ->where('tbl_artist_lang.status',1)
						->orderBy('tbl_artist.position','asc')
                        ->get();
            }
        }else{
            $artist=[];
        }
        
        return View::make('frontend.orchestra')->with('artist',$artist)->with('cate',$cate);
    }
    public function postArtist(){
        $artist = \tblArtistModel::leftJoin('tbl_artist_lang','tbl_artist.id','=','tbl_artist_lang.artist_id')
                    ->where('tbl_artist_lang.lang_id',$this->lang_code)
                    ->where('tbl_artist_lang.status',1)
                    ->where('tbl_artist_lang.artist_id',\Input::get('id'))
                    ->first();
        return View::make('frontend.orchestra_ajax')->with('artist',$artist);
    }


	/* tin tuc*/
	public function getTinTuc() {   
        $data = \tblNewsModel::leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')
                            ->where('tbl_news_detail_lang.lang_id',$this->lang_code)
							->where('tbl_news_detail_lang.status',1)
							->where('tbl_news.time_post','<=',time())
							->orderBy('tbl_news_detail_lang.created_at','desc')
                            ->paginate(10);
        $arrCate = \tblNewsCategoryModel::where('lang_id',$this->lang_code)->get();
		$random = \tblNewsModel::leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')
                            ->where('tbl_news_detail_lang.lang_id',$this->lang_code)
                            ->where('tbl_news_detail_lang.status',1)
                            ->where('tbl_news.time_post','<=',time())
                            ->orderBy(\DB::raw('RAND()'))
                            ->limit(5)->get();
        $tags = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->where('tbl_tags_relationship.type',2)
                            ->groupBy('tbl_tags_relationship.tags_id')
                            ->limit(20)
                            ->get();
		return View::make('frontend.news.view')->with('tags',$tags)->with('random',$random)->with('arrCate',$arrCate)->with('data',$data);
        
    }

    
    
    public function getDanhMuc($cateslug = '') {
	
        $catnew = \tblNewsCategoryModel::leftJoin('tbl_lang','tbl_news_category.lang_id','=','tbl_lang.id')   
                ->select('tbl_news_category.*')
                ->where('slug', substr($cateslug, 0, -2))
                ->first();
				
		if(count($catnew)>0){
			$catnewlist = \tblNewsCategoryModel::leftJoin('tbl_lang','tbl_news_category.lang_id','=','tbl_lang.id') 
					->select('tbl_news_category.parent','tbl_news_category.id')
					->where('parent', $catnew->id)
					->get();
			$listcatid = array();
			$listcatid[] = $catnew->id;
			foreach ($catnewlist as $value) {
				$listcatid[] = $value->id;
			}
						  
			$arrTinTuc = \DB::table('tbl_news')
					->leftJoin('tbl_news_views','tbl_news.id','=','tbl_news_views.news_id')
					->leftJoin('tbl_news_category','tbl_news_views.cat_id','=','tbl_news_category.id')
					->leftJoin('tbl_lang','tbl_news_category.lang_id','=','tbl_lang.id')
					->leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')
					->select('tbl_news.*','tbl_news_detail_lang.id as news_detail_lang_id', 'tbl_news_detail_lang.news_id', 'tbl_news_detail_lang.seo_id', 'tbl_news_detail_lang.news_title', 'tbl_news_detail_lang.news_excerpt', 'tbl_news_detail_lang.news_tags', 'tbl_news_detail_lang.news_content', 'tbl_news_detail_lang.news_slug', 'tbl_news_category.name as cateName', 'tbl_news_category.description as cateDescription')
					->whereIn('tbl_news_views.cat_id', $listcatid)
					->where('tbl_news_detail_lang.status',1)
					->where('tbl_news_views.lang_id',$this->lang_code)
					->where('tbl_news_detail_lang.lang_id',$this->lang_code)
					->where('tbl_news.time_post','<=',time())
					->orderBy('tbl_news.time_post','desc')
					->groupBy('tbl_news_views.news_id')
					->paginate(5);		
			
		}else{
			$arrTinTuc='404';
		}				
		
        $arrCate = \tblNewsCategoryModel::where('lang_id',$this->lang_code)->get();
		$random = \tblNewsModel::leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')
                            ->where('tbl_news_detail_lang.lang_id',$this->lang_code)
                            ->where('tbl_news_detail_lang.status',1)
                            ->where('tbl_news.time_post','<=',time())
                            ->orderBy(\DB::raw('RAND()'))
                            ->limit(5)->get();
        if ($arrTinTuc == '404') {			
            return View::make('frontend.404');
        } else{  
            $catenewsname = \tblNewsCategoryModel::leftJoin('tbl_seo', 'tbl_news_category.seo_id', '=', 'tbl_seo.id')
							->leftJoin('tbl_lang','tbl_news_category.lang_id','=','tbl_lang.id') 
							->select('tbl_news_category.*','tbl_seo.title as seotitle','tbl_seo.description as seodesc','tbl_seo.keyword as seokeyword','tbl_seo.fb_title as seofbtitle','tbl_seo.fb_description as seofbdesc','tbl_seo.fb_image as seofbimg','tbl_seo.g_title as seogtitle','tbl_seo.g_description as seogdesc','tbl_seo.g_image as seogimg')                 
							->where('slug','=',substr($cateslug, 0, -2))
							->where('tbl_lang.id',$this->lang_code)
							->get();
            $tags = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->where('tbl_tags_relationship.type',2)
                            ->groupBy('tbl_tags_relationship.tags_id')
                            ->limit(20)
                            ->get();
            if(count($catenewsname)>0){
				return View::make('frontend.news.view')->with('tags',$tags)->with('random',$random)->with('catenewsname',$catenewsname[0])->with('data',$arrTinTuc)->with('arrCate',$arrCate);                        
            }else{
                return View::make('frontend.404');
            }
        }
    }
    
    public function getChiTiet($slug=''){        
        $data = \tblNewsModel::leftJoin('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')
						->leftJoin('tbl_seo', 'tbl_news_detail_lang.seo_id', '=', 'tbl_seo.id')
						->leftJoin('tbl_lang', 'tbl_news_detail_lang.lang_id', '=', 'tbl_lang.id')
						->select('tbl_news_detail_lang.*','tbl_news.time_post','tbl_news.comment_status','tbl_news.ping_status','tbl_news.images','tbl_news.user_id','tbl_lang.id as langid','tbl_seo.title as seotitle','tbl_seo.description as seodesc','tbl_seo.keyword as seokeyword','tbl_seo.fb_title as seofbtitle','tbl_seo.fb_description as seofbdesc','tbl_seo.fb_image as seofbimg','tbl_seo.g_title as seogtitle','tbl_seo.g_description as seogdesc','tbl_seo.g_image as seogimg') 
						->where('tbl_news_detail_lang.lang_id',$this->lang_code)
						->where('tbl_news_detail_lang.news_slug',substr($slug, 0, -2))
						->get();
        $arrCate = \tblNewsCategoryModel::where('lang_id',$this->lang_code)->get();
		
        if(count($data)>0){
            
			$random = \tblNewsModel::leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')
                            ->where('tbl_news_detail_lang.lang_id',$this->lang_code)
                            ->where('tbl_news_detail_lang.status',1)
                            ->where('tbl_news.time_post','<=',time())
                            ->orderBy(\DB::raw('RAND()'))
                            ->limit(5)->get();  		
			
            $tags = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->where('tbl_tags_relationship.type',2)
                            ->groupBy('tbl_tags_relationship.tags_id')
                            ->limit(20)
                            ->get();
            $comment = \tblCommentModel::leftJoin('tbl_news_detail_lang', 'tbl_comment.post_id_lang', '=', 'tbl_news_detail_lang.id')
                            ->select('tbl_comment.*')
                            ->where('tbl_comment.post_id_lang',$data[0]->id)
                            ->where('tbl_comment.post_type',4)
                            ->where('tbl_comment.status',1)
                            ->get();
            return View::make('frontend.news.detail')->with('comment',$comment)->with('tags',$tags)->with('random',$random)->with('data',$data)->with('arrCate',$arrCate);            
        }else{
            return View::make('frontend.404');
        }
    }
	
    public function getTag($slug){
        $arrCate = \tblNewsCategoryModel::where('lang_id',$this->lang_code)->get();
		$random = \tblNewsModel::leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')
                            ->where('tbl_news_detail_lang.lang_id',$this->lang_code)
                            ->where('tbl_news_detail_lang.status',1)
                            ->where('tbl_news.time_post','<=',time())
                            ->orderBy(\DB::raw('RAND()'))
                            ->limit(5)->get();	
        $list_id = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->select('tbl_tags_relationship.post_id')
                            ->where('tbl_tags.slug',substr($slug, 0, -2))
                            ->where('tbl_tags_relationship.type',2)
                            ->get();

        $list_data=[];
        if(count($list_id)>0){
            $id_list=[];
            foreach($list_id as $i_list_id){
                $id_list[] = $i_list_id->post_id;
            }
            $list_data =  \tblNewsModel::leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')
                    ->whereIn('tbl_news_detail_lang.id', $id_list)
                    ->where('tbl_news_detail_lang.status', '=', 1)
					->where('tbl_news.time_post','<=',time())
                    ->orderBy('tbl_news.time_post', 'desc')
                    ->paginate(5);
        }
        $tags = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->where('tbl_tags_relationship.type',2)
                            ->groupBy('tbl_tags_relationship.tags_id')
                            ->limit(20)
                            ->get();
        return View::make('frontend.news.view')->with('tags',$tags)->with('random',$random)->with('arrCate',$arrCate)->with('data',$list_data);
    }
	public function postSearch(){
		$keyword = \Input::get('keyword');      
		if ($keyword == '') {
            $keyword = 'null';
        }
        return \Redirect::action('\FontEnd\HomeController@getSearch', array($keyword));
	}

	public function getSearch($keyword=''){
		if ($keyword == 'null') {
            $keyword = '';
        }
		$data = \tblNewsModel::leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')                            
                            ->where('tbl_news_detail_lang.lang_id',$this->lang_code)
							->where('tbl_news_detail_lang.status',1)
							->where('tbl_news.time_post','<=',time());
                if($keyword!='' || $keyword!='null'){  
                    $data->where(function($query) use ($keyword){
                        $query->where('tbl_news_detail_lang.news_title', 'LIKE', '%' . $keyword . '%');
                    });   
                } 
		$return = $data->orderBy('tbl_news.time_post','desc')->paginate(5);
        $arrCate = \tblNewsCategoryModel::where('lang_id',$this->lang_code)->get();
		$random = \tblNewsModel::leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')
                            ->where('tbl_news_detail_lang.lang_id',$this->lang_code)
                            ->where('tbl_news_detail_lang.status',1)
                            ->where('tbl_news.time_post','<=',time())
                            ->orderBy(\DB::raw('RAND()'))
                            ->limit(5)->get();
        $tags = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->where('tbl_tags_relationship.type',2)
                            ->groupBy('tbl_tags_relationship.tags_id')
                            ->limit(20)
                            ->get();
        return View::make('frontend.news.view')->with('tags',$tags)->with('random',$random)->with('arrCate',$arrCate)->with('data',$return);
	}

	public function getPage($slug=''){
		$data = \tblPageModel::leftJoin('tbl_page_lang','tbl_page.id','=','tbl_page_lang.page_id')
                            ->leftJoin('tbl_seo','tbl_page_lang.seo_id','=','tbl_seo.id')
                            ->select('tbl_page_lang.*','tbl_seo.title as seotitle','tbl_seo.description as seodesc','tbl_seo.keyword as seokeyword','tbl_seo.fb_title as seofbtitle','tbl_seo.fb_description as seofbdesc','tbl_seo.fb_image as seofbimg','tbl_seo.g_title as seogtitle','tbl_seo.g_description as seogdesc','tbl_seo.g_image as seogimg')
                            ->where('tbl_page_lang.lang_id',$this->lang_code)
                            ->where('tbl_page_lang.status',1)
                            ->where('tbl_page_lang.page_slug',substr($slug, 0, -2))
                            ->first();
        if(count($data)>0){
        	$timeline = \tblHistoryModel::leftJoin('tbl_history_lang','tbl_history.id','=','tbl_history_lang.history_id')				->where('tbl_history_lang.lang_id',$this->lang_code)				->where('tbl_history_lang.status',1)				->orderBy('tbl_history.timeline','desc')				->limit(5)				->get();            	return View::make('frontend.page')->with('data',$data)->with('timeline',$timeline);
		}else{
			return \Redirect::route('home');
		}
	}	public function postTimelineajax(){        $data = \tblHistoryModel::leftJoin('tbl_history_lang','tbl_history.id','=','tbl_history_lang.history_id')				->where('tbl_history_lang.lang_id',$this->lang_code)				->where('tbl_history_lang.status',1)				->orderBy('tbl_history.timeline','desc')				->skip(\Input::get('skip'))				->take(5)				->get();                      return View::make('frontend.timeline_ajax')->with('data',$data);    }
}