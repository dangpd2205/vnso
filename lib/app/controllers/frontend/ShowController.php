<?php

namespace FontEnd;

use FontEnd,
    View,
    Lang,
    Redirect,
    Session,
    Input,
    Cart,
    Validator;

class ShowController extends \BaseController {
	private $lang_code = '';
    public function __construct() {
        if(\Cookie::has('cookie_lang')){
            $this->lang_code = \Cookie::get('cookie_lang');
        }else{
            $this->lang_code = \Config::get('all.all_config')->website_lang;        
        }            
    }
	
   public function getShowAll(){
        $data = \tblShowModel::leftJoin('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')
                        ->select('tbl_show_lang.*','tbl_show.artist_id','tbl_show.conductor_id','tbl_show.place_id','tbl_show.time_show','tbl_show.sponsor_id','tbl_show.avatar','tbl_show.period','tbl_show.is_sell','tbl_show.sell_status') 
                        ->where('tbl_show_lang.lang_id',$this->lang_code)
                        ->where('tbl_show_lang.status',1)
                        ->where('tbl_show.time_show','>',time())
                        ->orderBy('tbl_show.time_show','asc')
                        ->paginate(5);
        $all_place = \tblPlaceLangModel::where('lang_id',$this->lang_code)->where('status',1)->get();
        if(count($data)>0){
            foreach($data as $item){
                $place[] = \tblPlaceLangModel::where('place_id',$item->place_id)->where('lang_id',$this->lang_code)->first();
                $cate[] = \tblShowCategoryModel::leftJoin('tbl_show_views','tbl_show_category.id','=','tbl_show_views.cat_id')->where('tbl_show_views.show_id',$item->show_id)->where('tbl_show_views.lang_id',$this->lang_code)->get();
                $artist[] = \tblArtistModel::leftJoin('tbl_artist_lang','tbl_artist.id','=','tbl_artist_lang.artist_id')
                    ->where('tbl_artist_lang.lang_id',$this->lang_code)
                    ->where('tbl_artist_lang.status',1)
                    ->whereIn('tbl_artist_lang.artist_id',explode(',',$item->artist_id))
                    ->get();
				$conductor[] = \tblArtistModel::leftJoin('tbl_artist_lang','tbl_artist.id','=','tbl_artist_lang.artist_id')
                    ->where('tbl_artist_lang.lang_id',$this->lang_code)
                    ->where('tbl_artist_lang.status',1)
                    ->where('tbl_artist_lang.artist_id',$item->conductor_id)
                    ->first();
            }
        }else{
            $place=[];
            $cate=[];
            $artist=[];
			$conductor=[];
        }
        $arrCate = \tblShowCategoryModel::where('lang_id',$this->lang_code)->get();
        $random = \tblShowModel::leftJoin('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')
                        ->select('tbl_show_lang.*','tbl_show.place_id','tbl_show.time_show','tbl_show.sponsor_id','tbl_show.avatar') 
                        ->where('tbl_show_lang.lang_id',$this->lang_code)
                        ->where('tbl_show_lang.status',1)
                        ->where('tbl_show.time_show','>',time())
                        ->orderBy(\DB::raw('RAND()'))
                        ->limit(5)->get();
        $tags = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->where('tbl_tags_relationship.type',4)
                            ->groupBy('tbl_tags_relationship.tags_id')
                            ->limit(20)
                            ->get();
        return View::make('frontend.show')->with('conductor',$conductor)->with('all_place',$all_place)->with('artist',$artist)->with('cate',$cate)->with('place',$place)->with('data',$data)->with('arrCate',$arrCate)->with('random',$random)->with('tags',$tags);
    }
    public function getShow($slug=''){
        $data = \tblShowModel::leftJoin('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')
                        ->leftJoin('tbl_seo', 'tbl_show_lang.seo_id', '=', 'tbl_seo.id')
                        ->select('tbl_show_lang.*','tbl_show.sell_status','tbl_show.conductor_id','tbl_show.artist_id','tbl_show.place_id','tbl_show.is_sell','tbl_show.time_show','tbl_show.sponsor_id','tbl_show.avatar','tbl_seo.title as seotitle','tbl_seo.description as seodesc','tbl_seo.keyword as seokeyword','tbl_seo.fb_title as seofbtitle','tbl_seo.fb_description as seofbdesc','tbl_seo.fb_image as seofbimg','tbl_seo.g_title as seogtitle','tbl_seo.g_description as seogdesc','tbl_seo.g_image as seogimg') 
						->where('tbl_show_lang.status',1)
                        ->where('tbl_show_lang.lang_id',$this->lang_code)
                        ->where('tbl_show_lang.slug',substr($slug, 0, -2))
                        ->first();
        $tags = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->where('tbl_tags_relationship.type',4)
                            ->groupBy('tbl_tags_relationship.tags_id')
                            ->limit(20)
                            ->get();

        if(count($data)>0){
            $place = \tblPlaceModel::leftJoin('tbl_place_lang','tbl_place.id','=','tbl_place_lang.place_id')
                        ->where('tbl_place_lang.place_id',$data->place_id)
                        ->where('tbl_place_lang.lang_id',$this->lang_code)
                        ->first();
            if($data->sponsor_id!=''){
                $sponsor = \tblManufModel::whereIn('tbl_manuf.id',explode(',',$data->sponsor_id))->orderBy('tbl_manuf.position','asc')->get();
            }else{
                $sponsor=[];
            }
            $upcoming = \tblShowModel::leftJoin('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')
                        ->leftJoin('tbl_place_lang','tbl_show.place_id','=','tbl_place_lang.place_id')
                        ->select('tbl_show_lang.*','tbl_show.*','tbl_place_lang.name as place_name')                         
                        ->where('tbl_show.time_show','>',$data->time_show)
                        ->where('tbl_show_lang.lang_id',$this->lang_code)
                        ->where('tbl_place_lang.lang_id',$this->lang_code)
                        ->orderBy('tbl_show.time_show','asc')
                        ->limit(5)->get();
            $comment = \tblCommentModel::leftJoin('tbl_show_lang', 'tbl_comment.post_id_lang', '=', 'tbl_show_lang.id')
                            ->select('tbl_comment.*')
                            ->where('tbl_comment.post_id_lang',$data->id)
                            ->where('tbl_comment.post_type',5)
                            ->where('tbl_comment.status',1)
                            ->get();
            $ticket = \tblTicketShowModel::leftJoin('tbl_ticket_lang','tbl_ticket_show.ticket_id','=','tbl_ticket_lang.ticket_id')->where('tbl_ticket_lang.lang_id',$this->lang_code)->where('tbl_ticket_show.show_id',$data->show_id)->groupBy('tbl_ticket_show.ticket_id')->get();			$artist = \tblArtistLangModel::leftJoin('tbl_artist_view','tbl_artist_lang.artist_id','=','tbl_artist_view.artist_id')->leftJoin('tbl_artist_category','tbl_artist_view.cat_id','=','tbl_artist_category.id')->select('tbl_artist_lang.*','tbl_artist_category.name as cate_name')->whereIn('tbl_artist_lang.artist_id',explode(',',$data->artist_id))->where('tbl_artist_lang.status',1)->where('tbl_artist_lang.lang_id',$this->lang_code)->groupBy('tbl_artist_lang.artist_id')->get();			$conductor = \tblArtistLangModel::where('tbl_artist_lang.artist_id',$data->conductor_id)->where('tbl_artist_lang.lang_id',$this->lang_code)->first();			
            return View::make('frontend.show_detail')->with('artist',$artist)->with('conductor',$conductor)->with('ticket',$ticket)->with('comment',$comment)->with('upcoming',$upcoming)->with('data',$data)->with('place',$place)->with('sponsor',$sponsor)->with('tags',$tags);
        }        
    }
    public function getDanhMuc($cateslug = '') {
        $all_place = \tblPlaceLangModel::where('lang_id',$this->lang_code)->where('status',1)->get();
        $catnew = \tblShowCategoryModel::leftJoin('tbl_lang','tbl_show_category.lang_id','=','tbl_lang.id')   
                ->select('tbl_show_category.*')
                ->where('slug', substr($cateslug, 0, -2))
                ->first();
                
        if(count($catnew)>0){
            $catnewlist = \tblShowCategoryModel::leftJoin('tbl_lang','tbl_show_category.lang_id','=','tbl_lang.id') 
                    ->select('tbl_show_category.parent','tbl_show_category.id')
                    ->where('parent', $catnew->id)
                    ->get();
            $listcatid = array();
            $listcatid[] = $catnew->id;
            foreach ($catnewlist as $value) {
                $listcatid[] = $value->id;
            }
                          
            $data = \DB::table('tbl_show')
                    ->leftJoin('tbl_show_views','tbl_show.id','=','tbl_show_views.show_id')
                    ->leftJoin('tbl_show_category','tbl_show_views.cat_id','=','tbl_show_category.id')
                    ->leftJoin('tbl_lang','tbl_show_category.lang_id','=','tbl_lang.id')
                    ->leftJoin('tbl_show_lang','tbl_show.id','=','tbl_show_lang.show_id')
                    ->select('tbl_show.*','tbl_show_lang.*', 'tbl_show_category.name as cateName', 'tbl_show_category.description as cateDescription')
                    ->whereIn('tbl_show_views.cat_id', $listcatid)
                    ->where('tbl_show_lang.status',1)
                    ->where('tbl_show_views.lang_id',$this->lang_code)
                    ->where('tbl_show_lang.lang_id',$this->lang_code)
                    ->where('tbl_show.time_show','>',time())
                    ->orderBy('tbl_show.time_show','asc')
                    ->groupBy('tbl_show_views.show_id')
                    ->paginate(5);      
            
        }else{
            $data='404';
        }               
        
        $arrCate = \tblShowCategoryModel::where('lang_id',$this->lang_code)->get();
        $random = \tblShowModel::leftJoin('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')
                        ->select('tbl_show_lang.*','tbl_show.place_id','tbl_show.time_show','tbl_show.sponsor_id','tbl_show.avatar') 
                        ->where('tbl_show_lang.lang_id',$this->lang_code)
                        ->where('tbl_show_lang.status',1)
                        ->where('tbl_show.time_show','>',time())
                        ->orderBy(\DB::raw('RAND()'))
                        ->limit(5)->get();
        if ($data == '404') {          
            return View::make('frontend.404');
        } else{  
            if(count($data)>0){
                foreach($data as $item){
                    $place[] = \tblPlaceLangModel::where('place_id',$item->place_id)->where('lang_id',$this->lang_code)->first();
                    $cate[] = \tblShowCategoryModel::leftJoin('tbl_show_views','tbl_show_category.id','=','tbl_show_views.cat_id')->where('tbl_show_views.show_id',$item->show_id)->where('tbl_show_views.lang_id',$this->lang_code)->get();
                    $artist[] = \tblArtistModel::leftJoin('tbl_artist_lang','tbl_artist.id','=','tbl_artist_lang.artist_id')
                        ->where('tbl_artist_lang.lang_id',$this->lang_code)
                        ->where('tbl_artist_lang.status',1)
                        ->whereIn('tbl_artist_lang.artist_id',explode(',',$item->artist_id))
                        ->get();
					$conductor[] = \tblArtistModel::leftJoin('tbl_artist_lang','tbl_artist.id','=','tbl_artist_lang.artist_id')
                    ->where('tbl_artist_lang.lang_id',$this->lang_code)
                    ->where('tbl_artist_lang.status',1)
                    ->where('tbl_artist_lang.artist_id',$item->conductor_id)
                    ->first();
                }
            }else{
                $place=[];
                $cate=[];
                $artist=[];
				$conductor=[];
            }
            $catenewsname = \tblShowCategoryModel::leftJoin('tbl_seo', 'tbl_show_category.seo_id', '=', 'tbl_seo.id')
                            ->leftJoin('tbl_lang','tbl_show_category.lang_id','=','tbl_lang.id') 
                            ->select('tbl_show_category.*','tbl_seo.title as seotitle','tbl_seo.description as seodesc','tbl_seo.keyword as seokeyword','tbl_seo.fb_title as seofbtitle','tbl_seo.fb_description as seofbdesc','tbl_seo.fb_image as seofbimg','tbl_seo.g_title as seogtitle','tbl_seo.g_description as seogdesc','tbl_seo.g_image as seogimg')                 
                            ->where('slug','=',substr($cateslug, 0, -2))
                            ->where('tbl_lang.id',$this->lang_code)
                            ->get();
            $tags = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->where('tbl_tags_relationship.type',4)
                            ->groupBy('tbl_tags_relationship.tags_id')
                            ->limit(20)
                            ->get();
            if(count($catenewsname)>0){
                return View::make('frontend.show')->with('conductor',$conductor)->with('all_place',$all_place)->with('tags',$tags)->with('random',$random)->with('catenewsname',$catenewsname[0])->with('data',$data)->with('arrCate',$arrCate)->with('artist',$artist)->with('cate',$cate)->with('place',$place);                        
            }else{
                return View::make('frontend.404');
            }
        }
    }
    public function getTag($slug){
        $all_place = \tblPlaceLangModel::where('lang_id',$this->lang_code)->where('status',1)->get();
        $arrCate = \tblShowCategoryModel::where('lang_id',$this->lang_code)->get();
        $random = \tblShowModel::leftJoin('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')
                        ->select('tbl_show_lang.*','tbl_show.place_id','tbl_show.time_show','tbl_show.sponsor_id','tbl_show.avatar','tbl_show.period','tbl_show.is_sell','tbl_show.sell_status') 
                        ->where('tbl_show_lang.lang_id',$this->lang_code)
                        ->where('tbl_show_lang.status',1)
                        ->where('tbl_show.time_show','>',time())
                        ->orderBy(\DB::raw('RAND()'))
                        ->limit(5)->get();
        $list_id = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->select('tbl_tags_relationship.post_id')
                            ->where('tbl_tags.slug',substr($slug, 0, -2))
                            ->where('tbl_tags_relationship.type',4)
                            ->get();

        $list_data=[];
        if(count($list_id)>0){
            $id_list=[];
            foreach($list_id as $i_list_id){
                $id_list[] = $i_list_id->post_id;
            }
            $list_data =  \tblShowModel::leftJoin('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')
                        ->select('tbl_show_lang.*','tbl_show.place_id','tbl_show.time_show','tbl_show.sponsor_id','tbl_show.avatar','tbl_show.conductor_id') 
                    ->whereIn('tbl_show_lang.id', $id_list)
                    ->where('tbl_show_lang.status', '=', 1)
                    ->where('tbl_show.time_show','>',time())
                    ->orderBy('tbl_show.time_show', 'asc')
                    ->paginate(5);
        }
        if(count($list_data)>0){
            foreach($list_data as $item){
                $place[] = \tblPlaceLangModel::where('place_id',$item->place_id)->where('lang_id',$this->lang_code)->first();
                $cate[] = \tblShowCategoryModel::leftJoin('tbl_show_views','tbl_show_category.id','=','tbl_show_views.cat_id')->where('tbl_show_views.show_id',$item->show_id)->where('tbl_show_views.lang_id',$this->lang_code)->get();
                $artist[] = \tblArtistModel::leftJoin('tbl_artist_lang','tbl_artist.id','=','tbl_artist_lang.artist_id')
                    ->where('tbl_artist_lang.lang_id',$this->lang_code)
                    ->where('tbl_artist_lang.status',1)
                    ->whereIn('tbl_artist_lang.artist_id',explode(',',$item->artist_id))
                    ->get();
				$conductor[] = \tblArtistModel::leftJoin('tbl_artist_lang','tbl_artist.id','=','tbl_artist_lang.artist_id')
                    ->where('tbl_artist_lang.lang_id',$this->lang_code)
                    ->where('tbl_artist_lang.status',1)
                    ->where('tbl_artist_lang.artist_id',$item->conductor_id)
                    ->first();
            }
        }else{
            $place=[];
            $cate=[];
            $artist=[];
			$conductor=[];
        }
        $tags = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->where('tbl_tags_relationship.type',4)
                            ->groupBy('tbl_tags_relationship.tags_id')
                            ->limit(20)
                            ->get();
        $tag = \tblTagsModel::where('slug',substr($slug, 0, -2))->first();
       
        return View::make('frontend.show')->with('conductor',$conductor)->with('all_place',$all_place)->with('tags',$tags)->with('random',$random)->with('arrCate',$arrCate)->with('data',$list_data)->with('artist',$artist)->with('cate',$cate)->with('place',$place)->with('tag',$tag);
    }
    public function postSearch(){
        $keyword = \Input::get('keyword');      
        if ($keyword == '') {
            $keyword = 'null';
        }
        return \Redirect::action('\FontEnd\ShowController@getSearch', array($keyword));
    }

    public function getSearch($keyword=''){
        if ($keyword == 'null') {
            $keyword = '';
        }
        $all_place = \tblPlaceLangModel::where('lang_id',$this->lang_code)->where('status',1)->get();
        $data = \tblShowModel::leftJoin('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')                            
                            ->where('tbl_show_lang.lang_id',$this->lang_code)
                            ->where('tbl_show_lang.status',1)
                            ->where('tbl_show.time_show','>',time());
                if($keyword!='' || $keyword!='null'){  
                    $data->where(function($query) use ($keyword){
                        $query->where('tbl_show_lang.name', 'LIKE', '%' . $keyword . '%');
                    });   
                } 
        $return = $data->orderBy('tbl_show.time_show','asc')->paginate(5);
        if(count($return)>0){
            foreach($return as $item){
                $place[] = \tblPlaceLangModel::where('place_id',$item->place_id)->where('lang_id',$this->lang_code)->first();
                $cate[] = \tblShowCategoryModel::leftJoin('tbl_show_views','tbl_show_category.id','=','tbl_show_views.cat_id')->where('tbl_show_views.show_id',$item->show_id)->where('tbl_show_views.lang_id',$this->lang_code)->get();
                $artist[] = \tblArtistModel::leftJoin('tbl_artist_lang','tbl_artist.id','=','tbl_artist_lang.artist_id')
                    ->where('tbl_artist_lang.lang_id',$this->lang_code)
                    ->where('tbl_artist_lang.status',1)
                    ->whereIn('tbl_artist_lang.artist_id',explode(',',$item->artist_id))
                    ->get();
				$conductor[] = \tblArtistModel::leftJoin('tbl_artist_lang','tbl_artist.id','=','tbl_artist_lang.artist_id')
                    ->where('tbl_artist_lang.lang_id',$this->lang_code)
                    ->where('tbl_artist_lang.status',1)
                    ->where('tbl_artist_lang.artist_id',$item->conductor_id)
                    ->first();
            }
        }else{
            $place=[];
            $cate=[];
            $artist=[];
			$conductor=[];
        }
        $arrCate = \tblShowCategoryModel::where('lang_id',$this->lang_code)->get();
        $random = \tblShowModel::leftJoin('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')
                        ->select('tbl_show_lang.*','tbl_show.place_id','tbl_show.time_show','tbl_show.sponsor_id','tbl_show.avatar','tbl_show.is_sell') 
                        ->where('tbl_show_lang.lang_id',$this->lang_code)
                        ->where('tbl_show_lang.status',1)
                        ->where('tbl_show.time_show','>',time())
                        ->orderBy(\DB::raw('RAND()'))
                        ->limit(5)->get();
        $tags = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->where('tbl_tags_relationship.type',4)
                            ->groupBy('tbl_tags_relationship.tags_id')
                            ->limit(20)
                            ->get();
        return View::make('frontend.show')->with('conductor',$conductor)->with('all_place',$all_place)->with('tags',$tags)->with('random',$random)->with('arrCate',$arrCate)->with('data',$return)->with('artist',$artist)->with('cate',$cate)->with('place',$place);
    }
    public function postFilter(){
        if (\Input::has('place') && \Input::get('place')!=0) {
            $place = \Input::get('place');
        }else{
            $place='null';
        }
        if (\Input::has('datefrom') && \Input::get('datefrom')!='') {
            $from = strtotime(\Input::get('datefrom')." 00:00");
        }else{
            $from='null';
        }
        if (\Input::has('dateto') && \Input::get('dateto')!='') {
            $to = strtotime(\Input::get('dateto')." 23:59");
        }else{
            $to='null';
        }
        return \Redirect::action('\FontEnd\ShowController@getFilter', array($place,$from,$to));
    }

    public function getFilter($placeid='',$from='',$to=''){
        if ($placeid == 'null') {
            $placeid = '';
        }
        if ($from == 'null') {
            $from = '';
        }
        if ($to == 'null') {
            $to = '';
        }
        $all_place = \tblPlaceLangModel::where('lang_id',$this->lang_code)->where('status',1)->get();
        $data = \tblShowModel::leftJoin('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')                           
                            ->where('tbl_show_lang.lang_id',$this->lang_code)
                            ->where('tbl_show_lang.status',1);
                if($placeid!=''){
                    $data->where('tbl_show.place_id',$placeid);
                }
                if($from!=''){
                    $data->where('tbl_show.time_show','>=',$from);
                }
                if($to!=''){
                    $data->where('tbl_show.time_show','<=',$to);
                }
        $return = $data->orderBy('tbl_show.time_show','asc')->paginate(5);
        if(count($return)>0){
            foreach($return as $item){
                $place[] = \tblPlaceLangModel::where('place_id',$item->place_id)->where('lang_id',$this->lang_code)->first();
                $cate[] = \tblShowCategoryModel::leftJoin('tbl_show_views','tbl_show_category.id','=','tbl_show_views.cat_id')->where('tbl_show_views.show_id',$item->show_id)->where('tbl_show_views.lang_id',$this->lang_code)->get();
                $artist[] = \tblArtistModel::leftJoin('tbl_artist_lang','tbl_artist.id','=','tbl_artist_lang.artist_id')
                    ->where('tbl_artist_lang.lang_id',$this->lang_code)
                    ->where('tbl_artist_lang.status',1)
                    ->whereIn('tbl_artist_lang.artist_id',explode(',',$item->artist_id))
                    ->get();
				$conductor[] = \tblArtistModel::leftJoin('tbl_artist_lang','tbl_artist.id','=','tbl_artist_lang.artist_id')
                    ->where('tbl_artist_lang.lang_id',$this->lang_code)
                    ->where('tbl_artist_lang.status',1)
                    ->where('tbl_artist_lang.artist_id',$item->conductor_id)
                    ->first();
					
            }
        }else{
            $place=[];
            $cate=[];
            $artist=[];
			$conductor=[];
        }
        $arrCate = \tblShowCategoryModel::where('lang_id',$this->lang_code)->get();
        $random = \tblShowModel::leftJoin('tbl_show_lang', 'tbl_show.id', '=', 'tbl_show_lang.show_id')
                        ->select('tbl_show_lang.*','tbl_show.place_id','tbl_show.time_show','tbl_show.sponsor_id','tbl_show.avatar','tbl_show.is_sell') 
                        ->where('tbl_show_lang.lang_id',$this->lang_code)
                        ->where('tbl_show_lang.status',1)
                        ->where('tbl_show.time_show','>',time())
                        ->orderBy(\DB::raw('RAND()'))
                        ->limit(5)->get();
        $tags = \tblTagsRelationshipModel::leftJoin('tbl_tags','tbl_tags_relationship.tags_id','=','tbl_tags.id')
                            ->where('tbl_tags_relationship.type',4)
                            ->groupBy('tbl_tags_relationship.tags_id')
                            ->limit(20)
                            ->get();
        return View::make('frontend.show')->with('conductor',$conductor)->with('all_place',$all_place)->with('tags',$tags)->with('random',$random)->with('arrCate',$arrCate)->with('data',$return)->with('artist',$artist)->with('cate',$cate)->with('place',$place);
    }
}