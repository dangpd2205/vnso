<?php

namespace FontEnd;

use View;

class LS_Controller extends \BaseController {
 
    
    public function layerslider_check_unit($str) {

        if (strstr($str, 'px') == false && strstr($str, '%') == false) {
            if (is_numeric($str)) {
                return $str . 'px';
            } else {
                return $str;
            }
        } else {
            return $str;
        }
    }

    public function layerslider_init($atts) {
// Get slider
        $slider = \tblLayersliderModel::where('id', $atts['id'])->first();
        if ($slider) {
            $slider = $slider->toArray();
// Get slider if any
            if (!$slider || $slider['flag_deleted'] == '1') {
                return '[LayerSliderWP] Slider not found';
            }

// Slider and markup data
            $slides = json_decode($slider['data'], true);
            $id = $slider['id'];
            $data = '';

// Include slider file
            if (is_array($slides)) {

                $lsDefaults = \Config::get('slider.lsDefaults');
                if (isset($slides['properties']['forceresponsive'])) {
                    $data[] = '<div class="ls-wp-fullwidth-container" style="height:' . ($this->layerslider_check_unit($slides['properties']['height'])) . ';">';
                    $data[] = '<div class="ls-wp-fullwidth-helper">';
                }
// Get slider style
                $sliderStyleAttr = array();
                if (isset($slides['properties']['width'])) {
                    $sliderStyleAttr[] = 'width:' . $this->layerslider_check_unit($slides['properties']['width']) . ';';
                }
                if (isset($slides['properties']['height'])) {
                    $sliderStyleAttr[] = 'height:' . $this->layerslider_check_unit($slides['properties']['height']) . ';';
                }
                if (!empty($slides['properties']['maxwidth'])) {
                    $sliderStyleAttr[] = 'max-width:' . ($this->layerslider_check_unit($slides['properties']['maxwidth'])) . ';';
                }

                $sliderStyleAttr[] = 'margin:0 auto;';
                if (isset($slides['properties']['sliderStyle'])) {
                    $sliderStyleAttr[] = $slides['properties']['sliderStyle'];
                }
                $data[] = '<div id="layerslider_' . $id . '" class="ls-wp-container" style="' . implode('', $sliderStyleAttr) . '">';
// Add slides

                if (!empty($slides['layers']) && is_array($slides['layers'])) {
                    foreach ($slides['layers'] as $slidekey => $slide) {
                        if (!isset($slide['properties']['skip'])) {
                            $slideId = !empty($slide['properties']['id']) ? ' id="' . $slide['properties']['id'] . '"' : '';
                            $slideAttrs = '';
                            if (isset($slide['properties']['slidedelay'])) {
                                $slideAttrs .= 'slidedelay:' . $slide['properties']['slidedelay'] . ';';
                            }
                            if (isset($slide['properties']['2d_transitions'])) {
                                if ($slide['properties']['2d_transitions'] != '') {
                                    $slideAttrs .= ' transition2d:' . $slide['properties']['2d_transitions'] . ';';
                                }
                            }
                            if (isset($slide['properties']['3d_transitions'])) {
                                if ($slide['properties']['3d_transitions'] != '') {
                                    $slideAttrs .= ' transition3d:' . $slide['properties']['3d_transitions'] . ';';
                                }
                            }
                            if (isset($slide['properties']['3d_transitions']) && isset($slide['properties']['2d_transitions'])) {
                                if ($slide['properties']['3d_transitions'] != '' && $slide['properties']['2d_transitions'] != '') {
                                    $slideAttrs .= ' transition2d: all;';
                                }
                            }
// Start of slide
                            $slideAttrs = !empty($slideAttrs) ? 'data-ls="' . $slideAttrs . '"' : '';
                            $data[] = '<div class="ls-slide"' . $slideId . ' ' . $slideAttrs . '>';
                            if (!empty($slide['properties']['background'])) {
                                $data[] = '<img src="' . asset('asset/slider') . '/img/blank.gif" data-src="' . $slide['properties']['background'] . '" class="ls-bg" alt="bg" />';
                            }
                            if (!isset($slides['thumb_nav']) || $slides['thumb_nav'] != 'disabled') {
                                if (!empty($slide['properties']['thumbnail'])) {
                                    $src = $slide['properties']['thumbnail'];
                                    $data[] = '<img src="' . asset('asset/slider') . '/img/blank.gif" data-src="' . $src . '" class="ls-tn" alt="Slide thumbnail" />';
                                }
                            }
                            if (!empty($slide['sublayers']) && is_array($slide['sublayers'])) {
                                foreach ($slide['sublayers'] as $layerkey => $layer) {
                                    if ($layer['media'] == 'img') {
                                        $style_list = json_decode($layer['styles'], true);
                                        $style = '';
                                        $style .= $layer['style'];
                                        if ($style_list != '') {
                                            foreach ($style_list as $key => $value) {
                                                $style .= $key . ':' . $this->layerslider_check_unit($value) . ';';
                                            }
                                        }
                                        if ($layer['top'] != '') {
                                            $style .= 'top:' . $this->layerslider_check_unit($layer['top']) . ';';
                                        }
                                        if ($layer['left'] != '') {
                                            $style .= 'left:' . $this->layerslider_check_unit($layer['left']) . ';';
                                        }
                                        $transition_list = json_decode($layer['transition'], true);
                                        $transition = '';
                                        if ($transition_list != '') {
                                            foreach ($transition_list as $key => $value) {
                                                $transition .= $key . ':' . $value . ';';
                                            }
                                        }
                                        if ($layer['url'] != '') {
                                            $data[] = '<a href="' . $layer['url'] . '" target="' . $layer['target'] . '" class="ls-l"  style="' . $style . '"  data-ls="' . $transition . '">' . '<img class="ls-l ls-preloaded"  src="' . $layer['image'] . '" data-src="' . $layer['image'] . '" alt=""></a>';
                                        } else {
                                            $data[] = '<img class="ls-l ls-preloaded" style="' . $style . '" data-ls="' . $transition . '" src="' . $layer['image'] . '" data-src="' . $layer['image'] . '" alt="">';
                                        }
                                    }
                                    if ($layer['media'] == 'text') {
                                        $style_list = json_decode($layer['styles'], true);
                                        $style = '';
                                        $style .= $layer['style'];
                                        if ($style_list != '') {
                                            foreach ($style_list as $key => $value) {
                                                $style .= $key . ':' . $this->layerslider_check_unit($value) . ';';
                                            }
                                        }
                                        if ($layer['top'] != '') {
                                            $style .= 'top:' . $this->layerslider_check_unit($layer['top']) . ';';
                                        }
                                        if ($layer['left'] != '') {
                                            $style .= 'left:' . $this->layerslider_check_unit($layer['left']) . ';';
                                        }
                                        $transition_list = json_decode($layer['transition'], true);
                                        $transition = '';
                                        if ($transition_list != '') {
                                            foreach ($transition_list as $key => $value) {
                                                $transition .= $key . ':' . $value . ';';
                                            }
                                        }

                                        if ($layer['url'] != '') {
                                            $data[] = '<a href="' . $layer['url'] . '" target="' . $layer['target'] . '" class="ls-l" style="' . $style . '"  data-ls="' . $transition . '">' . '<' . $layer['type'] . '   >' . $layer['html'] . '</' . $layer['type'] . '></a>';
                                        } else {
                                            $data[] = '<' . $layer['type'] . ' class="ls-l" style="' . $style . '" data-ls="' . $transition . '" >' . $layer['html'] . '</' . $layer['type'] . '>';
                                        }
                                    }
                                    if ($layer['media'] == 'html') {
                                        $style_list = json_decode($layer['styles'], true);
                                        $style = '';
                                        $style .= $layer['style'];
                                        if ($style_list != '') {
                                            foreach ($style_list as $key => $value) {
                                                $style .= $key . ':' . $this->layerslider_check_unit($value) . ';';
                                            }
                                        }
                                        if ($layer['top'] != '') {
                                            $style .= 'top:' . $this->layerslider_check_unit($layer['top']) . ';';
                                        }
                                        if ($layer['left'] != '') {
                                            $style .= 'left:' . $this->layerslider_check_unit($layer['left']) . ';';
                                        }
                                        $transition_list = json_decode($layer['transition'], true);
                                        $transition = '';
                                        if ($transition_list != '') {
                                            foreach ($transition_list as $key => $value) {
                                                $transition .= $key . ':' . $value . ';';
                                            }
                                        }

                                        if ($layer['url'] != '') {
                                            $data[] = '<a href="' . $layer['url'] . '" target="' . $layer['target'] . '" class="ls-l" style="' . $style . '"  data-ls="' . $transition . '">' . '<div  >' . $layer['html'] . '</div></a>';
                                        } else {
                                            $data[] = '<div class="ls-l" style="' . $style . '" data-ls="' . $transition . '" >' . $layer['html'] . '</div>';
                                        }
                                    }
                                }
                            }
                            $data[] = '</div>';
                        }
                    }
                }
                $data[] = '</div>';

// Full-width slider
                if (isset($slides['properties']['forceresponsive'])) {
                    $data[] = '</div>';
                    $data[] = '</div>';
                }
                $data[] = '<link rel="stylesheet" id="layerslider-css"  href="' . asset('asset') . '/slider/css/layerslider.css" type="text/css" media="all" />';
                $data[] = '<link rel="stylesheet" id="layerslider-css"  property="stylesheet"  href="' . asset('asset') . '/slider/css/layerslider.css" type="text/css" media="all" />';
                $data[] = '<script data-cfasync="false" type="text/javascript" src="' . asset('asset') . '/slider/js/greensock.js"></script>';
                $data[] = '<link rel="stylesheet" id="skin-css" property="stylesheet" href="' . asset('asset') . '/slider/skins/fullwidth/skin.css" type="text/css" media="all" />';
                $data[] = '<script data-cfasync="false" type="text/javascript" src="' . asset('asset') . '/slider/js/layerslider.kreaturamedia.jquery.js"></script>';
                $data[] = '<script data-cfasync="false" type="text/javascript" src="' . asset('asset') . '/slider/js/layerslider.transitions.js"></script>';
                $data[] = '<script data-cfasync = "false" type = "text/javascript">';
                $data[] = 'var lsjQuery = jQuery;';
                $data[] = '</script>';
                $data[] = '<script data-cfasync="false" type="text/javascript">';
                $data[] = 'lsjQuery(document).ready(function() {';
                $data[] = 'if(typeof lsjQuery.fn.layerSlider == "undefined") { lsShowNotice(\'layerslider_' . $id . '\',\'jquery\'); }';
                $data[] = 'else {';
                $data[] = 'lsjQuery(document).ready(function() { if(typeof lsjQuery.fn.layerSlider == \'undefined\') { lsShowNotice(\'layerslider_' . $id . '\',\'jquery\'); } else { lsjQuery(\'#layerslider_' . $id . '\').layerSlider({';
                $data[] = 'startInViewport:\'' . ((isset($slides['properties']['startinviewport'])) ? 'true' : 'false') . '\',';
                $data[] = 'autoStart:\'' . ((isset($slides['properties']['autostart'])) ? 'true' : 'false') . '\',';
                $data[] = 'responsive:\'' . ((isset($slides['properties']['responsive'])) ? 'true' : 'false') . '\',';
//            $data[] = 'responsiveUnder:\'' . ((isset($slides['properties']['responsiveunder'])) ? $slides['properties']['responsiveunder'] : '0') . '\',';
                $data[] = 'sublayerContainer:\'' . ((isset($slides['properties']['sublayercontainer'])) ? $slides['properties']['sublayercontainer'] : '0') . '\',';
                $data[] = 'firstLayer:\'' . ((isset($slides['properties']['firstlayer'])) ? '1' : '0') . '\',';
                $data[] = 'twoWaySlideshow:\'' . ((isset($slides['properties']['twowayslideshow'])) ? 'true' : 'false') . '\',';
                $data[] = 'randomSlideshow:\'' . ((isset($slides['properties']['randomslideshow'])) ? 'true' : 'false') . '\',';
                $data[] = 'keybNav:\'' . ((isset($slides['properties']['keybnav'])) ? 'true' : 'false') . '\',';
                $data[] = 'touchNav:\'' . ((isset($slides['properties']['touchnav'])) ? 'true' : 'false') . '\',';
                $data[] = 'imgPreload:\'' . ((isset($slides['properties']['imgpreload'])) ? 'true' : 'false') . '\',';
                $data[] = 'navPrevNext:\'' . ((isset($slides['properties']['navprevnext'])) ? 'true' : 'false') . '\',';
                $data[] = 'navStartStop  :\'' . ((isset($slides['properties']['navstartstop'])) ? 'true' : 'false') . '\',';
                $data[] = 'navButtons:\'' . ((isset($slides['properties']['navbuttons'])) ? 'true' : 'false') . '\',';
                $data[] = 'thumbnailNavigation:\'' . ((isset($slides['properties']['thumb_nav'])) ? $slides['properties']['thumb_nav'] : 'false') . '\',';
                $data[] = 'tnWidth:\'' . ((isset($slides['properties']['thumb_width'])) ? $slides['properties']['thumb_width'] : '100') . '\',';
                $data[] = 'tnHeight:\'' . ((isset($slides['properties']['thumb_height'])) ? $slides['properties']['thumb_height'] : '60') . '\',';
                $data[] = 'tnContainerWidth :\'' . ((isset($slides['properties']['thumb_container_width'])) ? $slides['properties']['thumb_container_width'] : '60%') . '\',';
                $data[] = 'tnActiveOpacity:\'' . ((isset($slides['properties']['thumb_active_opacity'])) ? $slides['properties']['thumb_active_opacity'] : '35') . '\',';
                $data[] = 'tnInactiveOpacity :\'' . ((isset($slides['properties']['thumb_inactive_opacity'])) ? $slides['properties']['thumb_inactive_opacity'] : '100') . '\',';
                $data[] = 'hoverPrevNext:\'' . ((isset($slides['properties']['hoverprevnext'])) ? 'true' : 'false') . '\',';
                $data[] = 'hoverBottomNav:\'' . ((isset($slides['properties']['hoverbottomnav'])) ? 'true' : 'false') . '\',';
                $data[] = 'skin:\'' . ((isset($slides['properties']['skin'])) ? $slides['properties']['skin'] : 'v5') . '\',';
                $data[] = 'skinsPath :\'' . \Config::get('slider.lsPluginDefaults.paths.skins') . '\',';
                $data[] = 'pauseOnHover:\'' . ((isset($slides['properties']['pauseonhover'])) ? 'true' : 'false') . '\',';
                $data[] = 'globalBGColor:\'' . ((isset($slides['properties']['backgroundcolor'])) ? $slides['properties']['backgroundcolor'] : 'transparent') . '\',';
                $data[] = 'globalBGImage:\'' . ((isset($slides['properties']['backgroundimage'])) ? $slides['properties']['backgroundimage'] : 'false') . '\',';
                $data[] = 'animateFirstLayer:\'' . ((isset($slides['properties']['animatefirstlayer'])) ? 'true' : 'false') . '\',';
                $data[] = 'yourLogo:\'' . ((isset($slides['properties']['yourlogo'])) ? $slides['properties']['yourlogo'] : 'false') . '\',';
                $data[] = 'yourLogoStyle:\'' . ((isset($slides['properties']['yourlogostyle'])) ? $slides['properties']['yourlogostyle'] : 'position: absolute; z-index: 1001; left: 10px; top: 10px;') . '\',';
                $data[] = 'yourLogoLink:\'' . ((isset($slides['properties']['yourlogolink'])) ? $slides['properties']['yourlogolink'] : 'false') . '\',';
                $data[] = 'yourLogoTarget:\'' . ((isset($slides['properties']['yourlogotarget'])) ? $slides['properties']['yourlogotarget'] : '_blank') . '\',';
                $data[] = 'loops:\'' . ((isset($slides['properties']['loops'])) ? $slides['properties']['loops'] : '0') . '\',';
                $data[] = 'forceLoopNum:\'' . ((isset($slides['properties']['forceloopnum'])) ? 'true' : 'false') . '\',';
                $data[] = 'autoPlayVideos:\'' . ((isset($slides['properties']['autoplayvideos'])) ? 'true' : 'false') . '\',';
                $data[] = 'autoPauseSlideshow:\'' . ((isset($slides['properties']['autopauseslideshow'])) ? 'true' : 'false') . '\',';
                $data[] = 'youtubePreview:\'' . ((isset($slides['properties']['youtubepreview'])) ? $slides['properties']['youtubepreview'] : 'maxresdefault.jpg') . '\',';
                $data[] = 'showBarTimer:\'' . ((isset($slides['properties']['bartimer'])) ? 'true' : 'false') . '\',';
                $data[] = 'showCircleTimer:\'' . ((isset($slides['properties']['circletimer'])) ? 'true' : 'false') . '\',';
                $data[] = 'slideDirection:\'' . ((isset($slides['properties']['slidedirection'])) ? $slides['properties']['slidedirection'] : 'right') . '\',';
                $data[] = 'slideDelay:\'' . ((isset($slides['properties']['slidedelay'])) ? $slides['properties']['slidedelay'] : '4000') . '\',';
                $data[] = 'durationIn:\'' . ((isset($slides['properties']['durationin'])) ? $slides['properties']['durationin'] : '1000') . '\',';
                $data[] = 'durationOut:\'' . ((isset($slides['properties']['durationout'])) ? $slides['properties']['durationout'] : '1000') . '\',';
                $data[] = 'easingIn:\'' . ((isset($slides['properties']['easingin'])) ? $slides['properties']['easingin'] : 'easeInOutQuint') . '\',';
                $data[] = 'easingOut:\'' . ((isset($slides['properties']['easingout'])) ? $slides['properties']['easingout'] : 'easeInOutQuint') . '\',';
                $data[] = 'delayIn:\'' . ((isset($slides['properties']['delayin'])) ? $slides['properties']['delayin'] : '0') . '\',';
                $data[] = 'delayOut:\'' . ((isset($slides['properties']['slidedidelayoutrection'])) ? $slides['properties']['slidedidelayoutrection'] : '0') . '\'';
                $data[] = '}); } }); ';
                $data[] = '}';
                $data[] = '});';
                $data[] = '</script>';
                $data = implode('', $data);
            }

            return $data;
        }
    }

    public function layerslider($id = 0, $page = '') {
        return $this->layerslider_init(array('id' => $id));
    }
}
