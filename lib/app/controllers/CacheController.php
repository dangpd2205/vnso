<?php

use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CacheController extends BaseController {

    public function fetch(Route $route, Request $request) {
        $key = $this->makCacheKey($request->url());
        if (Cache::has($key)) {
            return Cache::get($key);
        }
    }

    public function put(Route $route, Request $request, Response $response) {
        $key = $this->makCacheKey($request->url());
        if (!Cache::has($key)) {
            Cache::put($key, $response->getContent(), 15);
        }
    }

    protected function makCacheKey($url) {
        return 'route_' . Str::slug($url);
    }

}
