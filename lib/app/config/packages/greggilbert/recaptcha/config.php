<?php

return array(
    /*
      |--------------------------------------------------------------------------
      | API Keys
      |--------------------------------------------------------------------------
      |
      | Set the public and private API keys as provided by reCAPTCHA.
      |
     */
    'public_key' => '6Lc8aPMSAAAAAIIamTeqAUiu_rlEmD9PxjvzvGbO',
    'private_key' => '6Lc8aPMSAAAAAPn4TvL6ityZj7X51FqLDNJY-MXC',
    /*
      |--------------------------------------------------------------------------
      | Template
      |--------------------------------------------------------------------------
      |
      | Set a template to use if you don't want to use the standard one.
      |
     */
    'template' => '',
    'options' => array(
        'theme' => 'white',
    ),
);
