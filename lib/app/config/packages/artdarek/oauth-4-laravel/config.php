<?php

return array(
    /*
      |--------------------------------------------------------------------------
      | oAuth Config
      |--------------------------------------------------------------------------
     */

    /**
     * Storage
     */
    'storage' => 'Session',
    /**
     * Consumers
     */
    'consumers' => array(
        /**
         * Facebook
         */
        'Facebook' => array(
            'client_id' => '1434857600097289',
            'client_secret' => 'fada5b3ffebfba09b8c6cbc921f4fb8d',
            'scope' => array('email', 'read_friendlists', 'user_online_presence'),
        ),
    )
);
