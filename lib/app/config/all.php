<?php

$config_all = new \stdClass();
$allset = tblSettingModel::rememberForever()->get();
foreach ($allset as $item) {
    if ($item->key && $item->value) {
        $config_all->{$item->key} = $item->value;
    }
}
$lang_all = tblLangModel::orderBy('name')->rememberForever()->get();
$user_all = tblUsersModel::rememberForever()->get();

if (!Cache::has('all_group_role')) {
    $all_group = tblGroupAdminModel::all();
    $all_group_role = [];
    foreach ($all_group as $item) {
        $code = '';
        foreach (explode(',', $item->roles_code) as $item1) {
            $role = tblRolesModel::find($item1);
            if (count($role) > 0) {
                $code .= $role->code . ',';
            }
        }
        $all_group_role[] = [
            'id' => $item->id,
            'code' => $code
        ];
    }
    Cache::put('all_group_role', $all_group_role, 1500);
}
return array(
    'all_config' => $config_all,
    'all_lang' => $lang_all,
    'all_user' => $user_all,
  
    'all_group_role' => Cache::get('all_group_role'),
);
