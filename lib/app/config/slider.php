<?php

return array(
    'lsDefaults' => array(
        'slider' => array(
// ============= //
// |   Layout  | //
// ============= //
// The width of a new slider.
            'skin_list' => array(
                'borderlessdark' => 'Borderless Dark',
                'borderlessdark3d' => 'Borderless Dark for 3D sliders',
                'borderlesslight' => 'Borderless Light',
                'borderlesslight3d' => 'Borderless Light for 3D sliders',
                'carousel' => 'Carousel',
                'darkskin' => 'Dark',
                'defaultskin' => 'Default',
                'fullwidth' => 'Full-width',
                'fullwidthdark' => 'Full-width Dark',
                'glass' => 'Glass',
                'lightskin' => 'Light',
                'minimal' => 'Minimal',
                'noskin' => 'No skin - Hides everything, including buttons',
                'v5' => 'v5',
            ),
            'width' => array(
                'value' => 600,
                'name' => 'Slider width',
                'keys' => 'width',
                'desc' => 'The width of the slider in pixels. Accepts percents, but is only recommended for full-width layout.',
                'attrs' => array(
                    'type' => 'text'
                ),
                'props' => array(
                    'meta' => true
                )
            ),
            // The height of a new slider.
            'height' => array(
                'value' => 300,
                'name' => 'Slider height',
                'keys' => 'height',
                'desc' => 'The height of the slider in pixels.',
                'attrs' => array(
                    'type' => 'text'
                ),
                'props' => array(
                    'meta' => true
                )
            ),
            // Whether use or not responsiveness.
            'responsiveness' => array(
                'value' => true,
                'name' => 'Responsive',
                'keys' => 'responsive',
                'desc' => 'Responsive mode provides optimal viewing experience across a wide range of devices (from desktop to mobile) by adapting and scaling your sliders for the viewing environment.',
            ),
            // The maximum width that the slider can get in responsive mode.
            'maxWidth' => array(
                'value' => '',
                'name' => 'Max-width',
                'keys' => 'maxwidth',
                'desc' => 'The maximum width your slider can take in pixels when responsive mode is enabled.',
                'attrs' => array(
                    'type' => 'number',
                    'min' => 0
                ),
                'props' => array(
                    'meta' => true
                )
            ),
            // Force the slider to stretch to full-width,
// even when the theme doesn't designed that way.
            'fullWidth' => array(
                'value' => false,
                'name' => 'Full-width',
                'keys' => 'forceresponsive',
                'desc' => 'Enable this option to force the slider to become full-width, even if your theme does not support such layout.',
                'props' => array(
                    'meta' => true
                )
            ),
            // Turn on responsiveness under a given width of the slider.
// Depends on: enabled fullWidth option. Defaults to: 0
            'responsiveUnder' => array(
                'value' => 0,
                'name' => 'Responsive under',
                'keys' => array('responsiveunder', 'responsiveUnder'),
                'desc' => 'Turns on responsive mode in a full-width slider under the specified value in pixels. Can only be used with full-width mode.',
                'attrs' => array(
                    'min' => 0
                )
            ),
            // Creates an inner area for sublayers that will be centered
// regardless the size of the slider.
// Depends on: enabled fullWidth option. Defaults to: 0
            'layersContainer' => array(
                'value' => 0,
                'name' => 'Layers container',
                'keys' => array('sublayercontainer', 'layersContainer'),
                'desc' => 'Creates an invisible inner container with the given dimension in pixels to hold and center your layers.',
                'attrs' => array(
                    'min' => 0
                )
            ),
            // Hides the slider on mobile devices.
// Defaults to: false
            'hideOnMobile' => array(
                'value' => false,
                'name' => 'Hide on mobile',
                'keys' => array('hideonmobile', 'hideOnMobile'),
                'desc' => 'Hides the slider on mobile devices.',
            ),
            // Hides the slider under the given value of browser width in pixels.
// Defaults to: 0
            'hideUnder' => array(
                'value' => 0,
                'name' => 'Hide under',
                'keys' => array('hideunder', 'hideUnder'),
                'desc' => 'Hides the slider under the given value of browser width in pixels.',
                'attrs' => array(
                    'min' => 0
                )
            ),
            // Hides the slider over the given value of browser width in pixel.
// Defaults to: 100000
            'hideOver' => array(
                'value' => 100000,
                'name' => 'Hide over',
                'keys' => array('hideover', 'hideOver'),
                'desc' => 'Hides the slider over the given value of browser width in pixel.',
                'attrs' => array(
                    'min' => 0
                )
            ),
            // ================ //
// |   Slideshow  | //
// ================ //
// Automatically start slideshow.
            'autoStart' => array(
                'value' => true,
                'name' => 'Start slideshow',
                'keys' => array('autostart', 'autoStart'),
                'desc' => 'Slideshow will automatically start after pages have loaded.',
            ),
            // The slider will start only if it enters in the viewport.
            'startInViewport' => array(
                'value' => true,
                'name' => 'Start in viewport',
                'keys' => array('startinviewport', 'startInViewport'),
                'desc' => 'The slider will start only if it enters into the viewport.',
            ),
            // Temporarily pauses the slideshow while you are hovering over the slider.
            'pauseOnHover' => array(
                'value' => true,
                'name' => 'Pause on hover',
                'keys' => array('pauseonhover', 'pauseOnHover'),
                'desc' => 'Slideshow will temporally pause when someone moves the mouse cursor over the slider.',
            ),
            // The starting slide of a slider. Non-index value, starts with 1.
            'firstSlide' => array(
                'value' => 1,
                'name' => 'Start with slide',
                'keys' => array('firstlayer', 'firstSlide'),
                'desc' => 'The slider will start with the specified slide. You can use the value "random".',
                'attrs' => array(
                    'type' => 'text'
                )
            ),
            // Whether animate or show the ending position of the first slide.
            'animateFirstSlide' => array(
                'value' => true,
                'name' => 'Animate starting slide',
                'keys' => array('animatefirstlayer', 'animateFirstSlide'),
                'desc' => 'Disabling this option will result a static starting slide for the fisrt time on page load.',
            ),
            // The slideshow will change slides in random order.
            'shuffle' => array(
                'value' => false,
                'name' => 'Shuffle mode',
                'keys' => array('randomslideshow', 'randomSlideshow'),
                'desc' => 'Slideshow will proceed in random order. This feature does not work with looping.',
            ),
            // Whether slideshow should goind backwards or not
// when you switch to a previous slide.
            'twoWaySlideshow' => array(
                'value' => false,
                'name' => 'Two way slideshow',
                'keys' => array('twowayslideshow', 'twoWaySlideshow'),
                'desc' => 'Slideshow can go backwards if someone switches to a previous slide.',
            ),
            // Number of loops taking by the slideshow.
// Depends on: shuffle. Defaults to: 0 => infinite
            'loops' => array(
                'value' => 0,
                'name' => 'Loops',
                'keys' => 'loops',
                'desc' => 'Number of loops if slideshow is enabled. Zero means infinite loops.',
            ),
            // The slideshow will always stop at the given number of
// loops, even when the user restarts slideshow.
// Depends on: loop. Defaults to: true
            'forceLoopNumber' => array(
                'value' => true,
                'name' => 'Force the number of loops',
                'keys' => array('forceloopnum', 'forceLoopNum'),
                'desc' => 'The slider will always stop at the given number of loops, even if someone restarts slideshow.',
            ),
            // Use global shortcuts to control the slider.
            'keybNavigation' => array(
                'value' => true,
                'name' => 'Keyboard navigation',
                'keys' => array('keybnav', 'keybNav'),
                'desc' => 'You can navigate through slides with the left and right arrow keys.',
            ),
            // Accepts touch gestures if enabled.
            'touchNavigation' => array(
                'value' => true,
                'name' => 'Touch navigation',
                'keys' => array('touchnav', 'touchNav'),
                'desc' => 'Gesture-based navigation when swiping on touch-enabled devices.',
            ),
            // ================= //
// |   Appearance  | //
// ================= //
// The default skin.
            'skin' => array(
                'value' => 'v5',
                'name' => 'Skin',
                'keys' => 'skin',
                'desc' => "The skin used for this slider. The 'noskin' skin is a border- and buttonless skin. Your custom skins will appear in the list when you create their folders.",
            ),
            // Global background color on all slides.
            'globalBGColor' => array(
                'value' => '',
                'name' => 'Background color',
                'keys' => array('backgroundcolor', 'globalBGColor'),
                'desc' => 'Global background color of the slider. Slides with non-transparent background will cover this one. You can use all CSS methods such as HEX or RGB(A) values.',
            ),
            // Global background image on all slides.
            'globalBGImage' => array(
                'value' => '',
                'name' => 'Background image',
                'keys' => array('backgroundimage', 'globalBGImage'),
                'desc' => 'Global background image of the slider. Slides with non-transparent backgrounds will cover it. This image will not scale in responsive mode.',
            ),
            'sliderFadeInDuration' => array(
                'value' => 350,
                'name' => 'Initial fade duration',
                'keys' => array('sliderfadeinduration', 'sliderFadeInDuration'),
                'desc' => 'Change the duration of the initial fade animation when the page loads. Enter 0 to disable fading.',
                'attrs' => array(
                    'min' => 0
                )
            ),
            // Some CSS values you can append on each slide individually
// to make some adjustments if needed.
            'sliderStyle' => array(
                'value' => 'margin-bottom: 0px;',
                'name' => 'Slider CSS',
                'keys' => array('sliderstyle', 'sliderStyle'),
                'desc' => 'You can enter custom CSS to change some style properties on the slider wrapper element. More complex CSS should be applied with the Custom Styles Editor.',
                'props' => array(
                    'meta' => true
                )
            ),
            // ================= //
// |   Navigation  | //
// ================= //
// Show the next and previous buttons.
            'navPrevNextButtons' => array(
                'value' => true,
                'name' => 'Show Prev & Next buttons',
                'keys' => array('navprevnext', 'navPrevNext'),
                'desc' => 'Disabling this option will hide the Prev and Next buttons.',
            ),
            // Show the next and previous buttons
// only when hovering over the slider.
            'hoverPrevNextButtons' => array(
                'value' => true,
                'name' => 'Show Prev & Next buttons on hover',
                'keys' => array('hoverprevnext', 'hoverPrevNext'),
                'desc' => 'Show the buttons only when someone moves the mouse cursor over the slider. This option depends on the previous setting.',
            ),
            // Show the start and stop buttons
            'navStartStopButtons' => array(
                'value' => true,
                'name' => 'Show Start & Stop buttons',
                'keys' => array('navstartstop', 'navStartStop'),
                'desc' => 'Disabling this option will hide the Start & Stop buttons.',
            ),
            // Show the slide buttons or thumbnails.
            'navSlideButtons' => array(
                'value' => true,
                'name' => 'Show slide navigation buttons',
                'keys' => array('navbuttons', 'navButtons'),
                'desc' => 'Disabling this option will hide slide navigation buttons or thumbnails.',
            ),
            // Show the slider buttons or thumbnails
// ony when hovering over the slider.
            'hoverSlideButtons' => array(
                'value' => false,
                'name' => 'Slide navigation on hover',
                'keys' => array('hoverbottomnav', 'hoverBottomNav'),
                'desc' => 'Slide navigation buttons (including thumbnails) will be shown on mouse hover only.',
            ),
            // Show bar timer
            'barTimer' => array(
                'value' => false,
                'name' => 'Show bar timer',
                'keys' => array('bartimer', 'showBarTimer'),
                'desc' => 'Show the bar timer to indicate slideshow progression.',
            ),
            // Show circle timer. Requires CSS3 capable browser.
// This setting will overrule the 'barTimer' option.
            'circleTimer' => array(
                'value' => true,
                'name' => 'Show circle timer',
                'keys' => array('circletimer', 'showCircleTimer'),
                'desc' => 'Use circle timer to indicate slideshow progression.',
            ),
            // ========================== //
// |  Thumbnail navigation  | //
// ========================== //
// Use thumbnails for slide buttons
// Depends on: navSlideButtons.
// Possible values: 'disabled', 'hover', 'always'
            'thumbnailNavigation' => array(
                'value' => 'hover',
                'name' => 'Thumbnail navigation',
                'keys' => array('thumb_nav', 'thumbnailNavigation'),
                'desc' => 'Use thumbnail navigation instead of slide bullet buttons.',
                'options' => array(
                    'disabled' => 'Disabled',
                    'hover' => 'Hover',
                    'always' => 'Always',
                )
            ),
            // The width of the thumbnail area in percents.
            'thumbnailAreaWidth' => array(
                'value' => '60%',
                'name' => 'Thumbnail container width',
                'keys' => array('thumb_container_width', 'tnContainerWidth'),
                'desc' => 'The width of the thumbnail area.',
            ),
            // Thumbnails' width in pixels.
            'thumbnailWidth' => array(
                'value' => 100,
                'name' => 'Thumbnail width',
                'keys' => array('thumb_width', 'tnWidth'),
                'desc' => 'The width of thumbnails in the navigation area.',
                'attrs' => array(
                    'min' => 0
                )
            ),
            // Thumbnails' height in pixels.
            'thumbnailHeight' => array(
                'value' => 60,
                'name' => 'Thumbnail height',
                'keys' => array('thumb_height', 'tnHeight'),
                'desc' => 'The height of thumbnails in the navigation area.',
                'attrs' => array(
                    'min' => 0
                )
            ),
            // The opacity of the active thumbnail in percents.
            'thumbnailActiveOpacity' => array(
                'value' => 35,
                'name' => 'Active thumbnail opacity',
                'keys' => array('thumb_active_opacity', 'tnActiveOpacity'),
                'desc' => "Opacity in percentage of the active slide's thumbnail.",
                'attrs' => array(
                    'min' => 0,
                    'max' => 100
                )
            ),
            // The opacity of inactive thumbnails in percents.
            'thumbnailInactiveOpacity' => array(
                'value' => 100,
                'name' => 'Inactive thumbnail opacity',
                'keys' => array('thumb_inactive_opacity', 'tnInactiveOpacity'),
                'desc' => 'Opacity in percentage of inactive slide thumbnails.',
                'attrs' => array(
                    'min' => 0,
                    'max' => 100
                )
            ),
            // ============ //
// |  Videos  | //
// ============ //
// Automatically starts vidoes on the given slide.
            'autoPlayVideos' => array(
                'value' => true,
                'name' => 'Automatically play videos',
                'keys' => array('autoplayvideos', 'autoPlayVideos'),
                'desc' => 'Videos will be automatically started on the active slide.',
            ),
            // Automatically pauses the slideshow when a video is playing.
// Auto means it only pauses the slideshow while the video is playing.
// Possible values: 'auto', 'enabled', 'disabled'
            'autoPauseSlideshow' => array(
                'value' => 'auto',
                'name' => 'Pause slideshow',
                'keys' => array('autopauseslideshow', 'autoPauseSlideshow'),
                'desc' => 'The slideshow can temporally be paused while videos are playing. You can choose to permanently stop the pause until manual restarting.',
                'options' => array(
                    'auto' => 'While playing',
                    'enabled' => 'Permanently',
                    'disabled' => 'No action',
                )
            ),
            // The preview image quality of a YouTube video.
// Some videos doesn't have HD preview images and
// you may have to lower the quality settings.
// Possible values:
// 'maxresdefault.jpg',
// 'hqdefault.jpg',
// 'mqdefault.jpg',
// 'default.jpg'
            'youtubePreviewQuality' => array(
                'value' => 'maxresdefault.jpg',
                'name' => 'Youtube preview',
                'keys' => array('youtubepreview', 'youtubePreview'),
                'desc' => 'The preview image quaility for YouTube videos. Please note, some videos do not have HD previews, and you may need to choose a lower quaility.',
                'options' => array(
                    'maxresdefault.jpg' => 'Maximum quality',
                    'hqdefault.jpg' => 'High quality',
                    'mqdefault.jpg' => 'Medium quality',
                    'default.jpg' => 'Default quality',
                )
            ),
            // ========== //
// |  Misc  | //
// ========== //
// Preloads images from the first slide before displaying the slider.
            'imagePreload' => array(
                'value' => true,
                'name' => 'Image preload',
                'keys' => array('imgpreload', 'imgPreload'),
                'desc' => 'Preloads images used in the next slides for seamless animations.',
            ),
            'lazyLoad' => array(
                'value' => true,
                'name' => 'Lazy load images',
                'keys' => array('lazyload', 'lazyLoad'),
                'desc' => 'Loads images only when needed to save bandwidth and server resources. Relies on the preload feature.',
            ),
            // Ignores the host/domain names in URLS by converting the to
// relative format. Useful when you move your site.
// Prevents linking content from 3rd party servers.
            'relativeURLs' => array(
                'value' => false,
                'name' => 'Use relative URLs',
                'keys' => 'relativeurls',
                'desc' => 'Use relative URLs for local images. This setting could be important when moving your WP installation.',
                'props' => array(
                    'meta' => true
                )
            ),
            // ============== //
// |  YourLogo  | //
// ============== //
// Places a fixed image on the top of the slider.
            'yourLogoImage' => array(
                'value' => '',
                'name' => 'YourLogo',
                'keys' => array('yourlogo', 'yourLogo'),
                'desc' => 'A fixed image layer can be shown above the slider that remains still during slide progression. Can be used to display logos or watermarks.',
            ),
            // Custom CSS style settings for the YourLogo image.
// Depends on: yourLogoImage
            'yourLogoStyle' => array(
                'value' => 'left: -10px; top: -10px;',
                'name' => 'YourLogo style',
                'keys' => array('yourlogostyle', 'yourLogoStyle'),
                'desc' => 'CSS properties to control the image placement and appearance.',
            ),
            // Linking the YourLogo image to a given URL.
// Depends on: yourLogoImage
            'yourLogoLink' => array(
                'value' => '',
                'name' => 'YourLogo link',
                'keys' => array('yourlogolink', 'yourLogoLink'),
                'desc' => 'Enter an URL to link the YourLogo image.',
            ),
            // Link target for yourLogoLink.
// Depends on: yourLogoLink
            'yourLogoTarget' => array(
                'value' => '_self',
                'name' => 'Link target',
                'keys' => array('yourlogotarget', 'yourLogoTarget'),
                'desc' => '',
                'options' => array(
                    '_self' => 'Open on the same page',
                    '_blank' => 'Open on new page',
                    '_parent' => 'Open in parent frame',
                    '_top' => 'Open in main frame'
                ),
            ),
            // Post options
            'postType' => array(
                'value' => '',
                'keys' => 'post_type',
                'props' => array(
                    'meta' => true
                )
            ),
            'postOrderBy' => array(
                'value' => 'date',
                'keys' => 'post_orderby',
                'options' => array(
                    'date' => 'Date Created',
                    'modified' => 'Last Modified',
                    'ID' => 'Post ID',
                    'title' => 'Post Title',
                    'comment_count' => 'Number of Comments',
                    'rand' => 'Random'
                ),
                'props' => array(
                    'meta' => true
                )
            ),
            'postOrder' => array(
                'value' => 'DESC',
                'keys' => 'post_order',
                'options' => array(
                    'ASC' => 'Ascending',
                    'DESC' => 'Descending'
                ),
                'props' => array(
                    'meta' => true
                )
            ),
            'postCategories' => array(
                'value' => '',
                'keys' => 'post_categories',
                'props' => array(
                    'meta' => true
                )
            ),
            'postTags' => array(
                'value' => '',
                'keys' => 'post_tags',
                'props' => array(
                    'meta' => true
                )
            ),
            'postTaxonomy' => array(
                'value' => '',
                'keys' => 'post_taxonomy',
                'props' => array(
                    'meta' => true
                )
            ),
            'postTaxTerms' => array(
                'value' => '',
                'keys' => 'post_tax_terms',
                'props' => array(
                    'meta' => true
                )
            ),
            'cbInit' => array(
                'value' => "function(element) {\r\n\r\n}",
                'keys' => array('cbinit', 'cbInit')
            ),
            'cbStart' => array(
                'value' => "function(data) {\r\n\r\n}",
                'keys' => array('cbstart', 'cbStart')
            ),
            'cbStop' => array(
                'value' => "function(data) {\r\n\r\n}",
                'keys' => array('cbstop', 'cbStop')
            ),
            'cbPause' => array(
                'value' => "function(data) {\r\n\r\n}",
                'keys' => array('cbpause', 'cbPause')
            ),
            'cbAnimStart' => array(
                'value' => "function(data) {\r\n\r\n}",
                'keys' => array('cbanimstart', 'cbAnimStart')
            ),
            'cbAnimStop' => array(
                'value' => "function(data) {\r\n\r\n}",
                'keys' => array('cbanimstop', 'cbAnimStop')
            ),
            'cbPrev' => array(
                'value' => "function(data) {\r\n\r\n}",
                'keys' => array('cbprev', 'cbPrev')
            ),
            'cbNext' => array(
                'value' => "function(data) {\r\n\r\n}",
                'keys' => array('cbnext', 'cbNext')
            ),
        ),
        'slides' => array(
// The background image of slides
// Defaults to: void
            'image' => array(
                'value' => '',
                'name' => 'Set a slide image',
                'keys' => 'background',
                'tooltip' => 'The slide image/background. Click on the image to open the WordPress Media Library to choose or upload an image.',
                'props' => array('meta' => true)
            ),
            'imageId' => array(
                'value' => '',
                'keys' => 'backgroundId',
                'props' => array('meta' => true)
            ),
            'thumbnail' => array(
                'value' => '',
                'name' => 'Set a slide thumbnail',
                'keys' => 'thumbnail',
                'tooltip' => 'The thumbnail image of this slide. Click on the image to open the WordPress Media Library to choose or upload an image. If you leave this field empty, the slide image will be used.',
                'props' => array('meta' => true)
            ),
            'thumbnailId' => array(
                'value' => '',
                'keys' => 'thumbnailId',
                'props' => array('meta' => true)
            ),
            // Default slide delay in millisecs.
// Defaults to: 4000 (ms) => 4secs
            'delay' => array(
                'value' => 4000,
                'name' => 'Slide delay',
                'keys' => 'slidedelay',
                'tooltip' => "Here you can set the time interval between slide changes, this slide will stay visible for the time specified here. This value is in millisecs, so the value 1000 means 1 second. Please don't use 0 or very low values.",
                'attrs' => array(
                    'min' => 0,
                    'step' => 500
                )
            ),
            '2dTransitions' => array(
                'value' => '',
                'keys' => array('2d_transitions', 'transition2d')
            ),
            '3dTransitions' => array(
                'value' => '',
                'keys' => array('3d_transitions', 'transition3d')
            ),
            'custom2dTransitions' => array(
                'value' => '',
                'keys' => array('custom_2d_transitions', 'customtransition2d')
            ),
            'custom3dTransitions' => array(
                'value' => '',
                'keys' => array('custom_3d_transitions', 'customtransition3d')
            ),
            'timeshift' => array(
                'value' => 0,
                'name' => 'Time Shift',
                'keys' => 'timeshift',
                'tooltip' => 'You can control here the timing of the layer animations when the slider changes to this slide with a 3D/2D transition. Zero means that the layers of this slide will animate in when the slide transition ends. You can time-shift the starting time of the layer animations with positive or negative values.',
                'attrs' => array(
                    'step' => 50
                )
            ),
            'linkUrl' => array(
                'value' => '',
                'name' => 'Enter URL',
                'keys' => array('layer_link', 'linkUrl'),
                'tooltip' => 'If you want to link the whole slide, enter the URL of your link here.',
                'props' => array(
                    'meta' => true
                )
            ),
            'linkTarget' => array(
                'value' => '_self',
                'name' => 'Link Target',
                'keys' => array('layer_link_target', 'linkTarget'),
                'options' => array(
                    '_self' => 'Open on the same page',
                    '_blank' => 'Open on new page',
                    '_parent' => 'Open in parent frame',
                    '_top' => 'Open in main frame'
                ),
                'props' => array(
                    'meta' => true
                )
            ),
            'ID' => array(
                'value' => '',
                'name' => '#ID',
                'keys' => 'id',
                'tooltip' => 'You can apply an ID attribute on the HTML element of this slide to work with it in your custom CSS or Javascript code.',
                'props' => array(
                    'meta' => true
                )
            ),
            'deeplink' => array(
                'value' => '',
                'name' => 'Deeplink',
                'keys' => 'deeplink',
                'tooltip' => 'You can specify a slide alias name which you can use in your URLs with a hash mark, so LayerSlider will start with the correspondig slide.',
            ),
            'postOffset' => array(
                'value' => '',
                'keys' => 'post_offset',
                'props' => array(
                    'meta' => true
                )
            ),
            'skipSlide' => array(
                'value' => false,
                'name' => 'Hidden',
                'keys' => 'skip',
                'tooltip' => "If you don't want to use this slide in your front-page, but you want to keep it, you can hide it with this switch.",
                'props' => array(
                    'meta' => true
                )
            ),
            //  DEPRECATED OLD TRANSITIONS
            'slidedirection' => array('value' => 'right', 'keys' => 'slidedirection'),
            'durationin' => array('value' => 1500, 'keys' => 'durationin'),
            'durationout' => array('value' => 1500, 'keys' => 'durationout'),
            'easingin' => array('value' => 'easeInOutQuint', 'keys' => 'easingin'),
            'easingout' => array('value' => 'easeInOutQuint', 'keys' => 'easingout'),
            'delayin' => array('value' => 0, 'keys' => 'delayin'),
            'delayout' => array('value' => 0, 'keys' => 'slidedidelayoutrection')
        ),
        'layers' => array(
// ======================= //
// |  Content  | //
// ======================= //

            'type' => array(
                'value' => '',
                'keys' => 'type',
                'props' => array(
                    'meta' => true
                )
            ),
            'media' => array(
                'value' => '',
                'keys' => 'media',
                'props' => array(
                    'meta' => true
                )
            ),
            'image' => array(
                'value' => '',
                'keys' => 'image',
                'props' => array(
                    'meta' => true
                )
            ),
            'imageId' => array(
                'value' => '',
                'keys' => 'imageId',
                'props' => array('meta' => true)
            ),
            'html' => array(
                'value' => '',
                'keys' => 'html',
                'props' => array(
                    'meta' => true
                )
            ),
            'postTextLength' => array(
                'value' => '',
                'keys' => 'post_text_length',
                'props' => array(
                    'meta' => true
                )
            ),
            // ======================= //
// |  Animation options  | //
// ======================= //
            'transition' => array('value' => '', 'keys' => 'transition', 'props' => array('meta' => true)),
            'transitionInOffsetX' => array(
                'value' => '80',
                'name' => 'OffsetX',
                'keys' => 'offsetxin',
                'tooltip' => "The horizontal offset to align the starting position of layers. Positive and negative numbers are allowed or enter left / right to position the layer out of the frame.",
            ),
            'transitionInOffsetY' => array(
                'value' => '0',
                'name' => 'OffsetY',
                'keys' => 'offsetyin',
                'tooltip' => "The vertical offset to align the starting position of layers. Positive and negative numbers are allowed or enter top / bottom to position the layer out of the frame.",
            ),
            // Duration of the transition in millisecs when a layer animates in.
// Original: durationin
// Defaults to: 1000 (ms) => 1sec
            'transitionInDuration' => array(
                'value' => 1000,
                'name' => 'Duration',
                'keys' => 'durationin',
                'tooltip' => 'The transition duration in milliseconds when the layer enters into the slide. A second equals to 1000 milliseconds.',
                'attrs' => array('min' => 0, 'step' => 50)
            ),
            // Delay before the transition in millisecs when a layer animates in.
// Original: delayin
// Defaults to: 0 (ms)
            'transitionInDelay' => array(
                'value' => 0,
                'name' => 'Delay',
                'keys' => 'delayin',
                'tooltip' => 'Delays the transition with the given amount of milliseconds before the layer enters into the slide. A second equals to 1000 milliseconds.',
                'attrs' => array('min' => 0, 'step' => 50)
            ),
            // Easing of the transition when a layer animates in.
// Original: easingin
// Defaults to: 'easeInOutQuint'
            'transitionInEasing' => array(
                'value' => 'easeInOutQuint',
                'name' => 'Easing',
                'keys' => 'easingin',
                'tooltip' => "The timing function of the animation. With this function you can manipulate the movement of the animated object. Please click on the link next to this select field to open easings.net for more information and real-time examples.",
            ),
            'transitionInFade' => array(
                'value' => true,
                'name' => 'Fade',
                'keys' => 'fadein',
                'tooltip' => 'Fade the layer during the transition.',
            ),
            // Initial rotation degrees when a layer animates in.
// Original: rotatein
// Defaults to: 0 (deg)
            'transitionInRotate' => array(
                'value' => 0,
                'name' => 'Rotate',
                'keys' => 'rotatein',
                'tooltip' => 'Rotates the layer clockwise from the given angle to zero degree. Negative values are allowed for counterclockwise rotation.',
            ),
            'transitionInRotateX' => array(
                'value' => 0,
                'name' => 'RotateX',
                'keys' => 'rotatexin',
                'tooltip' => 'Rotates the layer along the X (horizontal) axis from the given angle to zero degree. Negative values are allowed for reverse direction.',
            ),
            'transitionInRotateY' => array(
                'value' => 0,
                'name' => 'RotateY',
                'keys' => 'rotateyin',
                'tooltip' => 'Rotates the layer along the Y (vertical) axis from the given angle to zero degree. Negative values are allowed for reverse direction.',
            ),
            'transitionInSkewX' => array(
                'value' => 0,
                'name' => 'SkewX',
                'keys' => 'skewxin',
                'tooltip' => 'Skews the layer along the X (horizontal) axis from the given angle to 0 degree. Negative values are allowed for reverse direction.',
            ),
            'transitionInSkewY' => array(
                'value' => 0,
                'name' => 'SkewY',
                'keys' => 'skewyin',
                'tooltip' => 'Skews the layer along the Y (vertical) axis from the given angle to 0 degree. Negative values are allowed for reverse direction.',
            ),
            'transitionInScaleX' => array(
                'value' => 1,
                'name' => 'ScaleX',
                'keys' => 'scalexin',
                'tooltip' => "Scales the layer's width from the given value to its original size.",
                'attrs' => array('step' => 0.1)
            ),
            'transitionInScaleY' => array(
                'value' => 1,
                'name' => 'ScaleY',
                'keys' => 'scaleyin',
                'tooltip' => "Scales the layer's height from the given value to its original size.",
                'attrs' => array('step' => 0.1)
            ),
            'transitionInTransformOrigin' => array(
                'value' => '50% 50% 0',
                'name' => 'TransformOrigin',
                'keys' => 'transformoriginin',
                'tooltip' => 'This option allows you to modify the origin for transformations of the layer according to its position. The three values represent the X, Y and Z axis in 3D space. OriginX can be left, center, right, a number or a percentage value. OriginY can be top, center, bottom, a number or a percentage value. OriginZ can be a number and corresponds the depth in 3D space.',
            ),
            // ======
            'transitionOutOffsetX' => array(
                'value' => '-80',
                'name' => 'OffsetX',
                'keys' => 'offsetxout',
                'tooltip' => "The horizontal offset to align the ending position of layers. Positive and negative numbers are allowed or write left / right to position the layer out of the frame.",
            ),
            'transitionOutOffsetY' => array(
                'value' => '0',
                'name' => 'OffsetY',
                'keys' => 'offsetyout',
                'tooltip' => "The vertical offset to align the starting position of layers. Positive and negative numbers are allowed or write top / bottom to position the layer out of the frame.",
            ),
            // Duration of the transition in millisecs when a layer animates out.
// Original: durationout
// Defaults to: 1000 (ms) => 1sec
            'transitionOutDuration' => array(
                'value' => 400,
                'name' => 'Duration',
                'keys' => 'durationout',
                'tooltip' => 'The transition duration in milliseconds when the layer leaves the slide. A second equals to 1000 milliseconds.',
                'attrs' => array('min' => 0, 'step' => 50)
            ),
            // You can create timed layers by specifing their time they can take on a slide in millisecs.
// Original: showuntil
// Defaults to: 0 (ms)
            'showUntil' => array(
                'value' => 0,
                'name' => 'Show until',
                'keys' => 'showuntil',
                'tooltip' => 'The layer will be visible for the time you specify here, then it will slide out. You can use this setting for layers to leave the slide before the slide itself animates out, or for example before other layers will slide in. This value in millisecs, so the value 1000 means 1 second.',
                'attrs' => array('min' => 0, 'step' => 50)
            ),
            // Easing of the transition when a layer animates out.
// Original: easingout
// Defaults to: 'easeInOutQuint'
            'transitionOutEasing' => array(
                'value' => 'easeInOutQuint',
                'name' => 'Easing',
                'keys' => 'easingout',
                'tooltip' => "The timing function of the animation. With this function you can manipulate the movement of the animated object. Please click on the link next to this select field to open easings.net for more information and real-time examples.",
            ),
            'transitionOutFade' => array(
                'value' => true,
                'name' => 'Fade',
                'keys' => 'fadeout',
                'tooltip' => 'Fade the layer during the transition.',
            ),
            // Initial rotation degrees when a layer animates out.
// Original: rotateout
// Defaults to: 0 (deg)
            'transitionOutRotate' => array(
                'value' => 0,
                'name' => 'Rotate',
                'keys' => 'rotateout',
                'tooltip' => 'Rotates the layer clockwise by the given angle from its original position. Negative values are allowed for counterclockwise rotation.',
            ),
            'transitionOutRotateX' => array(
                'value' => 0,
                'name' => 'RotateX',
                'keys' => 'rotatexout',
                'tooltip' => 'Rotates the layer along the X (horizontal) axis by the given angle from its original state. Negative values are allowed for reverse direction.',
            ),
            'transitionOutRotateY' => array(
                'value' => 0,
                'name' => 'RotateY',
                'keys' => 'rotateyout',
                'tooltip' => 'Rotates the layer along the Y (vertical) axis by the given angle from its orignal state. Negative values are allowed for reverse direction.',
            ),
            'transitionOutSkewX' => array(
                'value' => 0,
                'name' => 'SkewX',
                'keys' => 'skewxout',
                'tooltip' => 'Skews the layer along the X (horizontal) axis by the given angle from its orignal state. Negative values are allowed for reverse direction.',
            ),
            'transitionOutSkewY' => array(
                'value' => 0,
                'name' => 'SkewY',
                'keys' => 'skewyout',
                'tooltip' => 'Skews the layer along the Y (vertical) axis by the given angle from its original state. Negative values are allowed for reverse direction.',
            ),
            'transitionOutScaleX' => array(
                'value' => 1,
                'name' => 'ScaleX',
                'keys' => 'scalexout',
                'tooltip' => "Scales the layer's width by the given value from its original size.",
                'attrs' => array('step' => 0.1)
            ),
            'transitionOutScaleY' => array(
                'value' => 1,
                'name' => 'ScaleY',
                'keys' => 'scaleyout',
                'tooltip' => "Scales the layer's height by the given value from its original size.",
                'attrs' => array('step' => 0.1)
            ),
            'transitionOutTransformOrigin' => array(
                'value' => '50% 50% 0',
                'name' => 'TransformOrigin',
                'keys' => 'transformoriginout',
                'tooltip' => 'This option allows you to modify the origin for transformations of the layer according to its position. The three values represent the X, Y and Z axis in 3D space. OriginX can be left, center, right, a number or a percentage value. OriginY can be top, center, bottom, a number or a percentage value. OriginZ can be a number and corresponds the depth in 3D space.',
            ),
            'transitionParallaxLevel' => array(
                'value' => 0,
                'name' => 'Parallax Level',
                'keys' => 'parallaxlevel',
                'tooltip' => 'Applies a parallax effect on layers when you move your mouse over the slider. Higher values make the layer more sensitive to mouse move. Negative values are allowed.',
            ),
            // == Compatibility ==
            'transitionInType' => array(
                'value' => 'auto',
                'name' => 'Type',
                'keys' => 'slidedirection'
            ),
            'transitionOutType' => array(
                'value' => 'auto',
                'name' => 'Type',
                'keys' => 'slideoutdirection'
            ),
            'transitionOutDelay' => array(
                'value' => 0,
                'name' => 'Delay',
                'keys' => 'delayout',
                'tooltip' => 'Delay before the animation start when the layer slides out. This value is in millisecs, so the value 1000 means 1 second.',
                'attrs' => array(
                    'min' => 0,
                    'step' => 50
                )
            ),
            'transitionInScale' => array(
                'value' => '1.0',
                'name' => 'Scale',
                'keys' => 'scalein',
                'tooltip' => 'You can set the initial scale of this layer here which will be animated to the default (1.0) value.',
                'attrs' => array(
                    'step' => 0.1
                )
            ),
            'transitionOutScale' => array(
                'value' => '1.0',
                'name' => 'Scale',
                'keys' => 'scaleout',
                'tooltip' => 'You can set the ending scale value here, this sublayer will be animated from the default (1.0) value to yours.',
                'attrs' => array(
                    'step' => 0.1
                )
            ),
            'skipLayer' => array(
                'value' => false,
                'name' => 'Hidden',
                'keys' => 'skip',
                'tooltip' => "If you don't want to use this layer, but you want to keep it, you can hide it with this switch.",
                'props' => array(
                    'meta' => true
                )
            ),
            // ======
// Distance level determines the starting and ending position of a layer out of the frame.
// The value of -1 means automatic positioning outside of the frame. In some cases you might
// have to use a manual value greater than 3.
// Original: level
// Defaults to: -1
            'distanceLevel' => array(
                'value' => -1,
                'name' => 'Distance',
                'keys' => 'level',
                'tooltip' => 'The default value is -1 which means that the layer will be positioned exactly outside of the slide container. You can use the default setting in most of the cases. If you need to set the start or end position of the layer from further of the edges of the slide container, you can use 2, 3 or higher values.',
                'attrs' => array(
                    'min' => -1
                ),
                'props' => array(
                    'meta' => true
                )
            ),
            'linkURL' => array(
                'value' => '',
                'name' => 'Enter URL',
                'keys' => 'url',
                'tooltip' => 'If you want to link your layer, type here the URL. You can use a hash mark followed by a number to link this layer to another slide. Example: #3 - this will switch to the third slide.',
                'props' => array(
                    'meta' => true
                )
            ),
            'linkTarget' => array(
                'value' => '_self',
                'name' => 'URL target',
                'keys' => 'target',
                'options' => array(
                    '_self' => 'Open on the same page',
                    '_blank' => 'Open on new page',
                    '_parent' => 'Open in parent frame',
                    '_top' => 'Open in main frame'
                ),
                'props' => array(
                    'meta' => true
                )
            ),
            // Styles
            'width' => array(
                'value' => '',
                'name' => 'Width',
                'keys' => 'width',
                'tooltip' => "You can set the width of your layer. You can use pixels, percentage, or the default value 'auto'. Examples: 100px, 50% or auto.",
                'props' => array(
                    'meta' => true
                )
            ),
            'height' => array(
                'value' => '',
                'name' => 'Height',
                'keys' => 'height',
                'tooltip' => "You can set the height of your layer. You can use pixels, percentage, or the default value 'auto'. Examples: 100px, 50% or auto",
                'props' => array(
                    'meta' => true
                )
            ),
            'top' => array(
                'value' => '0px',
                'name' => 'Top',
                'keys' => 'top',
                'tooltip' => "The layer position from the top of the slide. You can use pixels and percentage. Examples: 100px or 50%. You can move your layers in the preview above with a drag n' drop, or set the exact values here.",
                'props' => array(
                    'meta' => true
                )
            ),
            'left' => array(
                'value' => '0px',
                'name' => 'Left',
                'keys' => 'left',
                'tooltip' => "The layer position from the left side of the slide. You can use pixels and percentage. Examples: 100px or 50%. You can move your layers in the preview above with a drag n' drop, or set the exact values here.",
                'props' => array(
                    'meta' => true
                )
            ),
            'paddingTop' => array(
                'value' => '',
                'name' => 'Top',
                'keys' => 'padding-top',
                'tooltip' => 'Padding on the top of the layer. Example: 10px',
                'props' => array(
                    'meta' => true
                )
            ),
            'paddingRight' => array(
                'value' => '',
                'name' => 'Right',
                'keys' => 'padding-right',
                'tooltip' => 'Padding on the right side of the layer. Example: 10px',
                'props' => array(
                    'meta' => true
                )
            ),
            'paddingBottom' => array(
                'value' => '',
                'name' => 'Bottom',
                'keys' => 'padding-bottom',
                'tooltip' => 'Padding on the bottom of the layer. Example: 10px',
                'props' => array(
                    'meta' => true
                )
            ),
            'paddingLeft' => array(
                'value' => '',
                'name' => 'Left',
                'keys' => 'padding-left',
                'tooltip' => 'Padding on the left side of the layer. Example: 10px',
                'props' => array(
                    'meta' => true
                )
            ),
            'borderTop' => array(
                'value' => '',
                'name' => 'Top',
                'keys' => 'border-top',
                'tooltip' => 'Border on the top of the layer. Example: 5px solid #000',
                'props' => array(
                    'meta' => true
                )
            ),
            'borderRight' => array(
                'value' => '',
                'name' => 'Right',
                'keys' => 'border-right',
                'tooltip' => 'Border on the right side of the layer. Example: 5px solid #000',
                'props' => array(
                    'meta' => true
                )
            ),
            'borderBottom' => array(
                'value' => '',
                'name' => 'Bottom',
                'keys' => 'border-bottom',
                'tooltip' => 'Border on the bottom of the layer. Example: 5px solid #000',
                'props' => array(
                    'meta' => true
                )
            ),
            'borderLeft' => array(
                'value' => '',
                'name' => 'Left',
                'keys' => 'border-left',
                'tooltip' => 'Border on the left side of the layer. Example: 5px solid #000',
                'props' => array(
                    'meta' => true
                )
            ),
            'fontFamily' => array(
                'value' => '',
                'name' => 'Family',
                'keys' => 'font-family',
                'tooltip' => 'List of your chosen fonts separated with a comma. Please use apostrophes if your font names contains white spaces. Example: Helvetica, Arial, sans-serif',
                'props' => array(
                    'meta' => true
                )
            ),
            'fontSize' => array(
                'value' => '',
                'name' => 'Size',
                'keys' => 'font-size',
                'tooltip' => 'The font size in pixels. Example: 16px.',
                'props' => array(
                    'meta' => true
                )
            ),
            'lineHeight' => array(
                'value' => '',
                'name' => 'Line-height',
                'keys' => 'line-height',
                'tooltip' => "The line height of your text. The default setting is 'normal'. Example: 22px",
                'props' => array(
                    'meta' => true
                )
            ),
            'color' => array(
                'value' => '',
                'name' => 'Color',
                'keys' => 'color',
                'tooltip' => 'The color of your text. You can use color names, hexadecimal, RGB or RGBA values. Example: #333',
                'props' => array(
                    'meta' => true
                )
            ),
            'background' => array(
                'value' => '',
                'name' => 'Background',
                'keys' => 'background',
                'tooltip' => "The background color of your layer. You can use color names, hexadecimal, RGB or RGBA values as well as the 'transparent' keyword. Example: #FFF",
                'props' => array(
                    'meta' => true
                )
            ),
            'borderRadius' => array(
                'value' => '',
                'name' => 'Rounded corners',
                'keys' => 'border-radius',
                'tooltip' => 'If you want rounded corners, you can set its radius here. Example: 5px',
                'props' => array(
                    'meta' => true
                )
            ),
            'wordWrap' => array(
                'value' => false,
                'name' => 'Word-wrap',
                'keys' => 'wordwrap',
                'tooltip' => 'If you use custom sized layers, you have to enable this setting to wrap your text.',
                'props' => array(
                    'meta' => true
                )
            ),
            'style' => array(
                'value' => '',
                'name' => 'Custom styles',
                'keys' => 'style',
                'tooltip' => 'If you want to set style settings other then above, you can use here any CSS codes. Please make sure to write valid markup.',
                'props' => array(
                    'meta' => true
                )
            ),
            'styles' => array(
                'value' => '',
                'keys' => 'styles',
                'props' => array(
                    'meta' => true,
                    'raw' => true
                )
            ),
            // Attributes
            'ID' => array(
                'value' => '',
                'name' => 'ID',
                'keys' => 'id',
                'tooltip' => 'You can apply an ID attribute on the HTML element of this layer to work with it in your custom CSS or Javascript code.',
                'props' => array(
                    'meta' => true
                )
            ),
            'class' => array(
                'value' => '',
                'name' => 'Classes',
                'keys' => 'class',
                'tooltip' => 'You can apply classes on the HTML element of this layer to work with it in your custom CSS or Javascript code.',
                'props' => array(
                    'meta' => true
                )
            ),
            'title' => array(
                'value' => '',
                'name' => 'Title',
                'keys' => 'title',
                'tooltip' => 'You can add a title to this layer which will display as a tooltip if someone holds his mouse cursor over the layer.',
                'props' => array(
                    'meta' => true
                )
            ),
            'alt' => array(
                'value' => '',
                'name' => 'Alt',
                'keys' => 'alt',
                'tooltip' => 'You can add an alternative text to your layer which is indexed by search engine robots and it helps people with certain disabilities.',
                'props' => array(
                    'meta' => true
                )
            ),
            'rel' => array(
                'value' => '',
                'name' => 'Rel',
                'keys' => 'rel',
                'tooltip' => 'Some plugin may use the rel attribute of a linked content, here you can specify it to make interaction with these plugins.',
                'props' => array(
                    'meta' => true
                )
            )
        ),
        'easings' => array(
            'linear',
            'swing',
            'easeInQuad',
            'easeOutQuad',
            'easeInOutQuad',
            'easeInCubic',
            'easeOutCubic',
            'easeInOutCubic',
            'easeInQuart',
            'easeOutQuart',
            'easeInOutQuart',
            'easeInQuint',
            'easeOutQuint',
            'easeInOutQuint',
            'easeInSine',
            'easeOutSine',
            'easeInOutSine',
            'easeInExpo',
            'easeOutExpo',
            'easeInOutExpo',
            'easeInCirc',
            'easeOutCirc',
            'easeInOutCirc',
            'easeInElastic',
            'easeOutElastic',
            'easeInOutElastic',
            'easeInBack',
            'easeOutBack',
            'easeInOutBack',
            'easeInBounce',
            'easeOutBounce',
            'easeInOutBounce'
        )
    ),
    'lsPluginDefaults' => array(
        // PATHS
        'paths' => array(
            'rootUrl' => '/asset/slider/',
            'skins' => '/asset/slider/skins/',
            'transitions' => '/asset/slider/demos/transitions.json'
        ),
        'features' => array(
            'autoupdate' => true
        ),
        // INTERFACE
        'interface' => array(
            'settings' => array(
            ),
            'fonts' => array(
            ),
            'news' => array(
                'display' => true,
                'collapsed' => false
            ),
        ),
        // Settings
        'settings' => array(
            'scriptsInFooter' => false,
            'conditionalScripts' => false,
            'concatenateOutput' => true,
            'cacheOutput' => false
        )
    ),
    'lsPluginDefaults' => array(
        // PATHS
        'paths' => array(
            'rootUrl' => '/asset/slider/',
            'skins' => '/asset/slider/skins/',
            'transitions' => '/asset/slider/demos/transitions.json'
        ),
        'features' => array(
            'autoupdate' => true
        ),
        // INTERFACE
        'interface' => array(
            'settings' => array(
            ),
            'fonts' => array(
            ),
            'news' => array(
                'display' => true,
                'collapsed' => false
            ),
        ),
        // Settings
        'settings' => array(
            'scriptsInFooter' => false,
            'conditionalScripts' => false,
            'concatenateOutput' => true,
            'cacheOutput' => false
        )
    ),
);
?>