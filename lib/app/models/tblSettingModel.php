<?php

class tblSettingModel extends Eloquent {

    protected $table = 'tbl_setting';
	static function getAll() {
		$allset = tblSettingModel::all();
		$data_s = new stdClass();
        foreach ($allset as $item) {
			if($item->key && $item->value){
				$data_s->{$item->key} = $item->value;           
			}
        }
		if(isset($data_s)){
			return $data_s;
		}else{
			return '';
		}		
	}
	static function getTitleLang($title){
		if (\Cookie::has('cookie_lang')) {
			$lang_config = \Cookie::get('cookie_lang');
		} else {
			$lang_config = \Config::get('all.all_config')->website_lang;
		}
		$setting = \tblSettingModel::getAll();
		$title_lang = $title . '_' . $lang_config;
		if(isset($setting->$title_lang)){
			return $setting->$title_lang;
		}else{
			return "";
		}
	}
}
