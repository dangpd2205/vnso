<?php


namespace FontEnd;

use FontEnd,
    View,
    Lang,
    Redirect,
    Session,
    Input,
    DB,
    Validator;

class tblCustomerModel extends \Eloquent {
    
    protected $table = 'tbl_customer';
    public $timestamps = false;

    public function insertData($email,$name,$phone){
    	$this->email = $email;
        $this->name = $name;
        $this->phone = $phone; 
        $this->save();
        return $this->id;
    }
}