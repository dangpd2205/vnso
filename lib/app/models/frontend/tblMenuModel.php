<?php

namespace FontEnd;

use FontEnd,
    View,
    Lang,
    Redirect,
    Session,
    Input,
    DB,
    Validator;

class tblMenuModel extends \Eloquent {
    protected $table = 'tbl_menu';
    
    public function getMenu($group){
        $data = DB::table('tbl_menu')
                ->leftJoin('tbl_menu_group', 'tbl_menu.group_id', '=', 'tbl_menu_group.id')                
                ->select('tbl_menu.*','tbl_menu_group.name as group_name')
                ->where('tbl_menu_group.id', $group)
                ->orderBy('tbl_menu.position', 'asc')
                ->get();
        return $data;
    }
}