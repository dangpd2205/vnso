<?php

namespace FontEnd;

use FontEnd,
    View,
    Lang,
    Redirect,
    Session,
    Input,
    DB,
    Validator;

class tblNewsModel extends \Eloquent {
    protected $table = 'tbl_news';
    
    public function getTinTuc($per_page,$orderby,$limit,$lang){
        $data = DB::table('tbl_news')
				->leftJoin('tbl_users', 'tbl_news.user_id', '=', 'tbl_users.id')
                ->leftJoin('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')
                ->leftJoin('tbl_lang', 'tbl_news_detail_lang.lang_id', '=', 'tbl_lang.id')
                ->select('tbl_news_detail_lang.*','tbl_users.full_name','tbl_news.avt_icon','tbl_news.video_url','tbl_news.type','tbl_news.time_post','tbl_news.comment_status','tbl_news.ping_status','tbl_news.images','tbl_news.user_id','tbl_lang.id as langid') 
                ->where('tbl_news_detail_lang.lang_id',$lang)
				->where('tbl_news.time_post','<=',time())
                ->where('tbl_news_detail_lang.status',1);
                if($orderby==''){
                    $arrNews = $data->orderBy(DB::raw('RAND()'));
                }else{
                    $arrNews = $data->orderBy('tbl_news.'.$orderby,'desc');
                }
                if($limit==''){
                    $arrNews = $data->remember(10)->paginate($per_page);                    
                }else{
                    $arrNews=$data->remember(10)->limit($limit)->get();
                }
        return $arrNews;
    }
    
    public function getChiTiet($slug,$lang){
        $data =  DB::table('tbl_news')
				->leftJoin('tbl_users', 'tbl_news.user_id', '=', 'tbl_users.id')
                ->leftJoin('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')
				->leftJoin('tbl_seo', 'tbl_news_detail_lang.seo_id', '=', 'tbl_seo.id')
                ->leftJoin('tbl_lang', 'tbl_news_detail_lang.lang_id', '=', 'tbl_lang.id')
                ->select('tbl_news_detail_lang.*','tbl_users.full_name','tbl_news.avt_icon','tbl_news.video_url','tbl_news.type','tbl_news.time_post','tbl_news.comment_status','tbl_news.ping_status','tbl_news.images','tbl_news.user_id','tbl_lang.id as langid','tbl_seo.title as seotitle','tbl_seo.description as seodesc','tbl_seo.keyword as seokeyword','tbl_seo.fb_title as seofbtitle','tbl_seo.fb_description as seofbdesc','tbl_seo.fb_image as seofbimg','tbl_seo.g_title as seogtitle','tbl_seo.g_description as seogdesc','tbl_seo.g_image as seogimg') 
                ->where('tbl_news_detail_lang.lang_id',$lang)
                ->where('tbl_news_detail_lang.news_slug',$slug)
                ->get();
        return $data;
    }
    
    public function getDanhMuc($slugcate = '', $per_page,$limit,$lang) {
        $catnew = DB::table('tbl_news_category')
                ->leftJoin('tbl_lang','tbl_news_category.lang_id','=','tbl_lang.id')   
                ->select('tbl_news_category.*')
                ->where('slug', $slugcate)
                ->first();
            if(count($catnew)>0){
                $catnewlist = DB::table('tbl_news_category')
                        ->leftJoin('tbl_lang','tbl_news_category.lang_id','=','tbl_lang.id') 
                        ->select('tbl_news_category.parent','tbl_news_category.id')
                        ->where('parent', $catnew->id)
                        ->get();
                $listcatid = array();
                $listcatid[] = $catnew->id;
                foreach ($catnewlist as $value) {
                    $listcatid[] = $value->id;
                }
                if($limit!=''){
                    $datanews = \DB::table('tbl_news')
							->leftJoin('tbl_users', 'tbl_news.user_id', '=', 'tbl_users.id')
                            ->leftJoin('tbl_news_views','tbl_news.id','=','tbl_news_views.news_id')
                            ->leftJoin('tbl_news_category','tbl_news_views.cat_id','=','tbl_news_category.id')
                            ->leftJoin('tbl_lang','tbl_news_category.lang_id','=','tbl_lang.id')
                            ->leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')
                            ->select('tbl_news.*','tbl_users.full_name','tbl_news_detail_lang.id as news_detail_lang_id', 'tbl_news_detail_lang.news_id', 'tbl_news_detail_lang.seo_id', 'tbl_news_detail_lang.news_title', 'tbl_news_detail_lang.news_excerpt', 'tbl_news_detail_lang.news_tags', 'tbl_news_detail_lang.news_content', 'tbl_news_detail_lang.news_slug', 'tbl_news_category.name as cateName', 'tbl_news_category.description as cateDescription')
                            ->whereIn('tbl_news_views.cat_id', $listcatid)
                            ->where('tbl_news_detail_lang.status',1)
                            ->where('tbl_news_views.lang_id',$lang)
                            ->where('tbl_news_detail_lang.lang_id',$lang)
							->where('tbl_news.time_post','<=',time())
                            ->orderBy('tbl_news.created_at','desc')
                            ->groupBy('tbl_news_views.news_id')
                            ->remember(10)
                            ->limit($limit)->get();
                    return $datanews;
                }else{                    
                    $datanews = \DB::table('tbl_news')
							->leftJoin('tbl_users', 'tbl_news.user_id', '=', 'tbl_users.id')
                            ->leftJoin('tbl_news_views','tbl_news.id','=','tbl_news_views.news_id')
                            ->leftJoin('tbl_news_category','tbl_news_views.cat_id','=','tbl_news_category.id')
                            ->leftJoin('tbl_lang','tbl_news_category.lang_id','=','tbl_lang.id')
                            ->leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')
                            ->select('tbl_news.*','tbl_users.full_name','tbl_news_detail_lang.id as news_detail_lang_id', 'tbl_news_detail_lang.news_id', 'tbl_news_detail_lang.seo_id', 'tbl_news_detail_lang.news_title', 'tbl_news_detail_lang.news_excerpt', 'tbl_news_detail_lang.news_tags', 'tbl_news_detail_lang.news_content', 'tbl_news_detail_lang.news_slug', 'tbl_news_category.name as cateName', 'tbl_news_category.description as cateDescription')
                            ->whereIn('tbl_news_views.cat_id', $listcatid)
                            ->where('tbl_news_detail_lang.status',1)
                            ->where('tbl_news_views.lang_id',$lang)
                            ->where('tbl_news_detail_lang.lang_id',$lang)
							->where('tbl_news.time_post','<=',time())
                            ->orderBy('tbl_news.created_at','desc')
                            ->groupBy('tbl_news_views.news_id')
                            ->remember(10)
                            ->paginate($per_page);
                    return $datanews;
                }
            }else{
                return '404';
            }
    }
    
    public function getCatename($cateslug,$lang){
        $data = DB::table('tbl_news_category')
				->leftJoin('tbl_seo', 'tbl_news_category.seo_id', '=', 'tbl_seo.id')
                ->leftJoin('tbl_lang','tbl_news_category.lang_id','=','tbl_lang.id') 
                ->select('tbl_news_category.*','tbl_seo.title as seotitle','tbl_seo.description as seodesc','tbl_seo.keyword as seokeyword','tbl_seo.fb_title as seofbtitle','tbl_seo.fb_description as seofbdesc','tbl_seo.fb_image as seofbimg','tbl_seo.g_title as seogtitle','tbl_seo.g_description as seogdesc','tbl_seo.g_image as seogimg')                 
                ->where('slug','=',$cateslug)
                ->where('tbl_lang.id',$lang)
                ->get();
        return $data;
    }
    
    public function getNewsByTags($tags_news = '', $per_page,$lang) {
        $arrTags = DB::table('tbl_news')
                ->leftJoin('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')
                ->leftJoin('tbl_lang', 'tbl_news_detail_lang.lang_id', '=', 'tbl_lang.id')
                ->select('tbl_news_detail_lang.news_tags', 'tbl_news.id')
                ->where('tbl_news_detail_lang.status', '=', 1)
                ->where('tbl_lang.id', $lang)
                ->get();
        $arrayidnew = array();
        foreach ($arrTags as $value) {
            $tag = explode(',', $value->news_tags);
            foreach ($tag as $item) {
                if ($tags_news == \Str::slug($item)) {
                    $arrayidnew[] = $value->id;
                }else{
                    $arrayidnew[]=null;
                }
            }
        }
        $datanews = DB::table('tbl_news')
                ->leftJoin('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')
                ->leftJoin('tbl_lang', 'tbl_news_detail_lang.lang_id', '=', 'tbl_lang.id')
                ->select('tbl_news.avt_icon','tbl_news.video_url','tbl_news.type','tbl_news.id', 'tbl_news.time_post', 'tbl_news.images', 'tbl_news.comment_status', 'tbl_news.ping_status', 'tbl_news.created_at', 'tbl_news.updated_at', 'tbl_news_detail_lang.id as news_detail_lang_id', 'tbl_news_detail_lang.news_id', 'tbl_news_detail_lang.seo_id', 'tbl_news_detail_lang.news_title', 'tbl_news_detail_lang.news_excerpt', 'tbl_news_detail_lang.news_tags', 'tbl_news_detail_lang.news_content', 'tbl_news_detail_lang.news_slug','tbl_news_detail_lang.status','tbl_lang.code', 'tbl_lang.name','tbl_lang.id as langid')
                ->where('tbl_news_detail_lang.status', 1)->where('tbl_lang.id', $lang)
                ->whereIn('tbl_news.id', $arrayidnew)     
                ->orderBy('tbl_news.created_at', 'desc')
                ->paginate($per_page);
        return $datanews;
    }
    
    public function searchNews($keyword, $per_page,$lang) {
        $arrNews = DB::table('tbl_news')
                ->leftJoin('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')
                ->leftJoin('tbl_lang', 'tbl_news_detail_lang.lang_id', '=', 'tbl_lang.id')
                ->select('tbl_news.id', 'tbl_news.time_post', 'tbl_news.images', 'tbl_news.comment_status', 'tbl_news.ping_status', 'tbl_news.created_at', 'tbl_news.updated_at', 'tbl_news_detail_lang.id as news_detail_lang_id', 'tbl_news_detail_lang.news_id', 'tbl_news_detail_lang.seo_id', 'tbl_news_detail_lang.news_title', 'tbl_news_detail_lang.news_excerpt', 'tbl_news_detail_lang.news_tags', 'tbl_news_detail_lang.news_content', 'tbl_news_detail_lang.news_slug','tbl_news_detail_lang.status','tbl_lang.code', 'tbl_lang.name','tbl_lang.id as langid')
                ->orderBy('tbl_news.created_at', 'desc')
                ->groupBy('tbl_news.id')
                ->where('tbl_lang.id', $lang)
                ->where('tbl_news_detail_lang.status', 1);
                if($keyword!='' || $keyword!=null){  
                    $arrNews->where(function($query) use ($keyword){
                        $query->where('tbl_news_detail_lang.news_title', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('tbl_news_detail_lang.news_excerpt', 'LIKE', '%' . $keyword . '%');
                    });                                           
                } 
                $data = $arrNews->paginate($per_page);
        return $data;
    }

    public function getAllTags($lang) {
        $arrTags = DB::table('tbl_news_detail_lang')->select('news_tags')->where('status', '=', 1)->where('lang_id',$lang)->limit(10)->get();
        $tags = array();
        foreach ($arrTags as $value) {
            $tag = explode(',', $value->news_tags);
            foreach ($tag as $item) {
                if (!in_array($item, $tags)) {
                    $tags[] = $item;
                }
            }
        }
        return $tags;
    }

    public function getNewsByTagsList($arrTag = '', $lang) {

        $arrTags = DB::table('tbl_news_detail_lang')->select('news_tags', 'news_id')->where('status', '=', 1)->where('lang_id',$lang)->limit(10)->get();

        $arrayidproduct = array();

        $tag_name = explode(',', $arrTag);

        foreach ($tag_name as $item_tag) {

            foreach ($arrTags as $value) {

                $tag = explode(',', $value->news_tags);

                foreach ($tag as $item) {

                    if (\Str::slug($item_tag) == \Str::slug($item)) {

                        if (!in_array($value->news_id, $arrayidproduct)) {

                            $arrayidproduct[] = $value->news_id;

                        }

                    }

                }

            }

        }
        if(count($arrayidproduct)>0){
        $datanews = DB::table('tbl_news')->where('status', '=', 1)
                    ->leftJoin('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')
                ->leftJoin('tbl_lang', 'tbl_news_detail_lang.lang_id', '=', 'tbl_lang.id')
                ->select('tbl_news_detail_lang.*','tbl_news.avt_icon','tbl_news.video_url','tbl_news.type','tbl_news.time_post','tbl_news.comment_status','tbl_news.ping_status','tbl_news.images','tbl_news.user_id','tbl_lang.id as langid') 
        ->whereIn('tbl_news.id', $arrayidproduct)->where('lang_id',$lang)->where('tbl_news_detail_lang.status', '=', 1)
        ->orderBy('tbl_news.created_at', 'desc')->limit(10)->get();
        }else{
            $datanews=array();
        }
        return $datanews;

    }
}