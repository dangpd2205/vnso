<?php


namespace FontEnd;

use FontEnd,
    View,
    Lang,
    Redirect,
    Session,
    Input,
    DB,
    Validator;

class tblGroupBranchModel extends \Eloquent {
    
    protected $table = 'tbl_group_branches';
    
    public function getAll($per_page,$lang){
        $data = DB::table('tbl_group_branches')
                ->leftJoin('tbl_group_branches_lang', 'tbl_group_branches.id','=','tbl_group_branches_lang.group_branch_id')
                ->leftJoin('tbl_lang', 'tbl_group_branches_lang.lang_id','=','tbl_lang.id')
                ->select('tbl_group_branches_lang.*') 
                ->where('tbl_group_branches_lang.lang_id',$lang)
                ->where('tbl_group_branches_lang.status',1);
                if($per_page!=''){
                    $return = $data->paginate($per_page);
                }else{
                    $return = $data->get();
                }
        return $return;
    }
  
}