<?php
namespace FontEnd;
use FontEnd,
    View,
    Lang,
    Redirect,
    Session,
    Input,
    Validator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class tblFeedbackModel extends \Eloquent {

    protected $table = 'tbl_feedback';
    public $timestamps = false;

    public function addFeedback($email,$name,$toname,$subject,$content,$parent,$status){
        $this->email = $email;
        $this->name = $name;
        $this->subject = $subject;
        $this->content = $content;
        $this->parent = $parent;
        $this->status = $status;        
        $result = $this->save();
        return $this->id;
    }
}
