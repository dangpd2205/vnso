<?php
namespace FontEnd;
use FontEnd,
    View,
    Lang,
    Redirect,
    Session,
    Input,
    Validator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class tblCommentModel extends \Eloquent {

    protected $table = 'tbl_comment';
    public $timestamps = false;

    public function addComment($user_id,$post_id,$name,$reply_name,$email,$content,$parent_id,$post_type){
        $this->user_id = $user_id;
        $this->post_id_lang = $post_id;
        $this->name = $name;
        $this->reply_name = $reply_name;
        $this->email = $email;
        $this->content = $content;
        $this->parent_id = $parent_id;
        $this->post_type = $post_type;
        $this->status = 0;        
        $this->save();
        return $this->id;
    }
    
    public function updateLike($id,$like){
        $data = $this->where('id', '=', $id);
        $arraysql = array('id' => $id);
        
        $arraysql = array_merge($arraysql, array("liked" => $like));

        $checku = $data->update($arraysql);
        
        return $checku;
    }
    
    public function getCommentByPostID($id,$type,$lang=''){
        $data=\DB::table('tbl_comment');
                if($type=='2'){
                     $data->leftJoin('tbl_event_lang', 'tbl_comment.post_id_lang', '=', 'tbl_event_lang.id')
                            ->leftJoin('tbl_event', 'tbl_event_lang.event_id','=','tbl_event.id')
                            ->leftJoin('tbl_lang', 'tbl_event_lang.lang_id','=','tbl_lang.id')
                            ->select('tbl_comment.*','tbl_event_lang.event_id')
                            ->where('tbl_event_lang.id',$id)
							->where('tbl_comment.post_type',$type)
                            ->where('tbl_comment.status',1);
                }else if($type=='4'){
                     $data->leftJoin('tbl_news_detail_lang', 'tbl_comment.post_id_lang', '=', 'tbl_news_detail_lang.id')
                            ->leftJoin('tbl_news', 'tbl_news_detail_lang.news_id', '=', 'tbl_news.id')
                            ->leftJoin('tbl_lang', 'tbl_news_detail_lang.lang_id','=','tbl_lang.id')
                            ->select('tbl_comment.*','tbl_news_detail_lang.news_id')
                            ->where('tbl_news_detail_lang.id',$id)
							->where('tbl_comment.post_type',$type)
                            ->where('tbl_comment.status',1);
                }else if($type=='1'){
                     $data->leftJoin('tbl_product_detail_lang', 'tbl_comment.post_id_lang', '=', 'tbl_product_detail_lang.id')
                            ->leftJoin('tbl_product', 'tbl_product_detail_lang.p_id', '=', 'tbl_product.id')
                            ->leftJoin('tbl_lang', 'tbl_product_detail_lang.lang_id','=','tbl_lang.id')
                            ->select('tbl_comment.*','tbl_product_detail_lang.p_id')
                            ->where('tbl_product_detail_lang.id',$id)
                            ->where('tbl_comment.status',1);
                }
        $comment = $data->orderBy('tbl_comment.created_at','desc')->get();
        return $comment;
    }
    
    public function getCommentByID($id){
        $data=\DB::table('tbl_comment')
                ->select('tbl_comment.id','tbl_comment.name','tbl_comment.parent_id','tbl_comment.liked')
                ->where('tbl_comment.id',$id)
                ->get();
        return $data;
    }
    
    public function getCommentByUserID($id,$type,$lang){
        $data=\DB::table('tbl_comment');
                if($type==1){
                     $data->leftJoin('tbl_event', 'tbl_comment.post_id', '=', 'tbl_event.id')
                            ->leftJoin('tbl_event_lang', 'tbl_event.id','=','tbl_event_lang.event_id')
                            ->leftJoin('tbl_lang', 'tbl_event_lang.lang_id','=','tbl_lang.id')
                            ->leftJoin('tbl_student', 'tbl_comment.user_id','=','tbl_student.id')
                            ->select('tbl_comment.*','tbl_event.id as eventid','tbl_event_lang.name as eventname','tbl_event_lang.slug',\DB::raw('count(tbl_comment.post_id) as soluong'))
                             ->where('tbl_lang.id',$lang)
                            ->where('tbl_comment.user_id',$id)
                            ->where('tbl_comment.post_type',$type)
                            ->where('tbl_comment.status',1);
                }else if($type==4){
                     $data->leftJoin('tbl_news', 'tbl_comment.post_id', '=', 'tbl_news.id')
                            ->leftJoin('tbl_news_detail_lang', 'tbl_news.id', '=', 'tbl_news_detail_lang.news_id')
                            ->leftJoin('tbl_lang', 'tbl_news_detail_lang.lang_id', '=', 'tbl_lang.id')
                            ->leftJoin('tbl_student', 'tbl_comment.user_id','=','tbl_student.id')
                            ->select('tbl_comment.*','tbl_news.id as newsid','tbl_news_detail_lang.news_title', 'tbl_news_detail_lang.news_slug',\DB::raw('count(tbl_comment.post_id) as soluong'))
                             ->where('tbl_lang.id',$lang)
                            ->where('tbl_comment.user_id',$id)
                            ->where('tbl_comment.post_type',$type)
                            ->where('tbl_comment.status',1);
                }
                $comment = $data->groupBy('tbl_comment.post_id')->orderBy('tbl_comment.created_at','desc')
                ->get();
        return $comment;
    }
    
}
