<?php

namespace FontEnd;

use FontEnd,
    View,
    Lang,
    Redirect,
    Session,
    Input,
    DB,
    Validator;

class tblPageModel extends \Eloquent {
    protected $table = 'tbl_page';
 
    public function getPageBySlug($slug,$lang) {
        $data = \DB::table('tbl_page_lang')
                ->leftJoin('tbl_page', 'tbl_page_lang.page_id', '=', 'tbl_page.id')
                ->leftJoin('tbl_lang', 'tbl_page_lang.lang_id', '=', 'tbl_lang.id')
				->leftJoin('tbl_seo', 'tbl_page_lang.seo_id', '=', 'tbl_seo.id')
                ->select('tbl_page_lang.*','tbl_lang.id as langid','tbl_lang.code','tbl_lang.name','tbl_page.id as pageid','tbl_page.time_post','tbl_page.comment_status','tbl_page.ping_status','tbl_seo.title as seotitle','tbl_seo.description as seodesc','tbl_seo.keyword as seokeyword','tbl_seo.fb_title as seofbtitle','tbl_seo.fb_description as seofbdesc','tbl_seo.fb_image as seofbimg','tbl_seo.g_title as seogtitle','tbl_seo.g_description as seogdesc','tbl_seo.g_image as seogimg')
                ->where('tbl_page_lang.page_slug', $slug)
                ->where('tbl_lang.id', $lang)
                ->where('tbl_page_lang.status', 1)
                ->first();
        return $data;
    
    }
}