<?php

namespace FontEnd;

use FontEnd,
    View,
    Lang,
    Redirect,
    Session,
    Input,
    DB,
    Validator;

class tblCateNewsModel extends \Eloquent {

    protected $table = 'tbl_news_category';
    public $timestamps = false;
    
    /* lay danh muc tin tuc theo lang */
    public function getCategory($lang_code){
        $arrCate = \DB::table('tbl_news_category')
                ->leftJoin('tbl_lang','tbl_news_category.lang_id','=','tbl_lang.id')
                ->where('tbl_lang.id',$lang_code)
                ->select('tbl_news_category.name','tbl_news_category.description','tbl_news_category.slug','tbl_news_category.created_at','tbl_news_category.updated_at','tbl_news_category.parent','tbl_news_category.id')
                ->get();
        return $arrCate;
    }

    public function getParent($lang_code){
        $arrCate = \DB::table('tbl_news_category')
                ->leftJoin('tbl_lang','tbl_news_category.lang_id','=','tbl_lang.id')
                ->where('tbl_lang.id',$lang_code)
                ->where('tbl_news_category.parent',0)
                ->select('tbl_news_category.name','tbl_news_category.description','tbl_news_category.slug','tbl_news_category.created_at','tbl_news_category.updated_at','tbl_news_category.parent','tbl_news_category.id')
                ->get();
        return $arrCate;
    }
}