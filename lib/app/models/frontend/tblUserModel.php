<?php


namespace FontEnd;

use FontEnd,
    View,
    Lang,
    Redirect,
    Session,
    Input,
    DB,
    Validator;

class tblUserModel extends \Eloquent {

    protected $table = 'tbl_users';
    
    public function LoginUser($username, $password) {
        $data = DB::table('tbl_users')->whereRaw('email = ? and password = ?', array($username, \Hash::make($password)))->get();
        return $data;
    }
    
    public function RegisterUser($uemail, $upassword, $ufname,$uDOB, $uaddress, $uphone,$verify) {
        $this->email = $uemail;
        $this->password = \Hash::make($upassword);
        $this->full_name = $ufname;
        $this->date_of_birth = $uDOB;
        $this->address = $uaddress;
        $this->phone = $uphone;
        $this->verify = $verify;
        $this->status = 1;
        $this->admin=0;
        $check = $this->save();
        return $check;
    }
    
    public function UpdateUser($id, $upassword, $ufname,$uDOB, $uaddress, $uphone,$avatar) {
        $user = $this->where('id', '=', $id);
        $arraysql = array('id' => $id);
        if ($upassword != '') {
            $arraysql = array_merge($arraysql, array("password" => \Hash::make($upassword)));
        }
        if ($ufname != '') {
            $arraysql = array_merge($arraysql, array("full_name" => $ufname));
        }
        if ($uDOB != '') {
            $arraysql = array_merge($arraysql, array("date_of_birth" => $uDOB));
        }
        if ($uaddress != '') {
            $arraysql = array_merge($arraysql, array("address" => $uaddress));
        }
        if ($uphone != '') {
            $arraysql = array_merge($arraysql, array("phone" => $uphone));
        }
        if ($avatar != '') {
            $arraysql = array_merge($arraysql, array("avatar" => $avatar));
        }
        $checku = $user->update($arraysql);
        if ($checku > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function checkUserExist($uemailf) {
        $checku = $this->where('email', '=', $uemailf)->count();
        if ($checku > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function ChangePass($uemailf, $pass) {
        $test = DB::update('update tbl_users set password = ? where md5(email) = ?', array(\Hash::make($pass), $uemailf));
        return $test;
    }
    
    public function getUserByEmail($email) {
        $objectUser = DB::table('tbl_users')->where('email', '=', $email)->first();
        return $objectUser;
    }
    
    public function getUserById($userId) {
        $objectUser = DB::table('tbl_users')->where('id', '=', $userId)->first();
        return $objectUser;
    }
}