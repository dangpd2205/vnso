<?php

class tblLayersliderModel extends Eloquent {

    protected $table = 'tbl_layerslider';

    public function slider_add($title = 'Unnamed', $data = array(), $slug = '') {
        $this->name = $title;
        $this->data = json_encode($data);
        $this->date_c = time();
        $this->date_m = time();
        $this->slug = $slug;
        $this->save();
        return $this->id;
    }

    public function slider_update($id = 0, $title = 'Unnamed', $data = array(), $slug = '') {
        $layer = $this::find($id);
        $layer->name = $title;
        $layer->data = json_encode($data);
        $layer->date_m = time();
        $layer->slug = $slug;
        $layer->save();
    }

    public function add_slider_name($name = 'No name') {
        if ($name == '') {
            $name = 'No name';
        }
        $count = $this->where('name', 'LIKE', $name . '%')->count();
        if ($count > 0) {
            $name = trim($name . ' ' . $count);
        }
        $data = array(
            'properties' => array('title' => $name),
            'layers' => array(array()),
        );
        $this->data = json_encode($data);
        $this->name = $name;
        $this->date_c = time();
        $this->author = \Auth::user()->id;
        $this->save();
        return $this->id;
    }

    public function slider_remove() {
        
    }

    public function slider_delete($id) {
        $this->where('id', $id)->delete();
    }

    public function slider_restore() {
        
    }

}
