<?php

namespace ADMIN;

use Form;

class TranslateGoogle {

    /**
     * Last translation
     * @var string
     * @access private
     */
    public $lastResult = "";

    /**
     * Language translating from
     * @var string
     * @access private
     */
    private $langFrom;

    /**
     * Language translating to
     * @var string
     * @access private
     */
    private $langTo;
    private static $urlFormat = "https://translate.google.com.vn/translate_a/single?client=t&sl=%s&tl=%s&hl=%s&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&tk=1&q=%s";

    public function __construct($from = "en", $to = "ka") {
        $this->setLangFrom($from)->setLangTo($to);
    }

    public function setLangFrom($lang) {
        $this->langFrom = $lang;
        return $this;
    }

    public function setLangTo($lang) {
        $this->langTo = $lang;
        return $this;
    }

    public function translate($string) {
        return $this->lastResult = self::staticTranslate($string, $this->langFrom, $this->langTo);
    }

    public static function staticTranslate($string, $from, $to) {
        $url = sprintf(self::$urlFormat, $from, $to, $to, rawurlencode(strip_tags($string)));
        $result = preg_replace('!,+!', ',', file_get_contents($url));
        $result = str_replace("[,", "[", $result);
        $resultArray = json_decode($result, true);
        $finalResult = "";
        if (!empty($resultArray[0])) {
            foreach ($resultArray[0] as $results) {
                $finalResult .= $results[0];
            }
            return $finalResult;
        }
        return false;
    }

}
