<?php

class Tree extends Eloquent {

    public function __construct() {
        
    }

    public static function get_days(){
        $vi_arr = ['Chủ nhật','Thứ hai','Thứ ba','Thứ tư','Thứ năm','Thứ sáu','Thứ bảy'];
        $en_arr = ['Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'];
        $cur_day = date('w',time());
        if(\Cookie::has('cookie_lang')){
            $lang = \Cookie::get('cookie_lang');
        }else{
            $lang = \Config::get('all.all_config')->website_lang;        
        }  
        $data = \tblLangModel::find($lang);
        if(count($data)>0){
            if($data->code=='vi'){
                return $vi_arr[$cur_day];
            }else{
                return $en_arr[$cur_day];
            }
        }else{
            return false;
        }
    }

	public static function gen_slug($str){		
		$str = trim(mb_strtolower($str));
		$str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
		$str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
		$str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
		$str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
		$str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
		$str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
		$str = preg_replace('/(đ)/', 'd', $str);
		$str = preg_replace('/[^a-z0-9-\s]/', '', $str);
		$str = preg_replace('/([\s]+)/', '-', $str);
		return $str;
	}
    public static function checkChild($id){
        
        $data = \tblProjectCategoryModel::where('parent',$id)->count();
        
        return $data;
    }
    public static function showCategories($categories,$parent_id = 0,$stt=0,$position=0,$checkurl='')
    {                           
        $cate_child = [];
        $check_child = [];
        foreach ($categories as $key => $item)
        {                                   
            if ($item['parent'] == $parent_id)
            {
                $cate_child[] = $item;   
                $check = \Tree::checkChild($item['id']);
                $check_child[] = $check;             
                unset($categories[$key]);
            }
        }   
        if (count($cate_child)>0)
        {
            $stt+=1;
            if($stt==1){
                echo '<ul class="list-one">';
            }else if($stt==2){
                
                echo '<ul class="list-second">';
            }else if($stt==3){
               
                echo '<ul class="list-third">';
            }else if($stt==4){
                
                echo '<ul class="list-four ">';
            }else if($stt==5){
                
                echo '<ul class="list-five ">';
            }              
            foreach ($cate_child as $key => $item)
            {         
                if($stt==1){
                    if(isset($check_child[$key]) && $check_child[$key]>0){
                        if(URL::route('route_data',$item['slug'].'pj') == $checkurl){
                            $li_one = 'li-muti ul-active';
                        }else{
                            $li_one = "";
                        }                          
                    }else{
                        $li_one="";
                    }                    
                    echo '<li class="'.$li_one.'"><a href="'.URL::route('route_data',$item['slug'].'pj').'">'.$item['name'].'</a>';
                                                               
                }else if($stt==2){     
                                   
                    if(URL::route('route_data',$item['slug'].'pj') == $checkurl){
                        $li_one = 'ul-active';
                    }else{
                        $li_one = "li-mutil";
                    }   
                    
                    echo '<li class="'.$li_one.'"><a href="'.URL::route('route_data',$item['slug'].'pj').'">'.$item['name'].'</a>';
                }else if($stt==3){
                    if(URL::route('route_data',$item['slug'].'pj') == $checkurl){
                        $li_one = 'ul-active-third';
                    }else{
                        $li_one = "";
                    }  
                    echo '<li class="'.$li_one.'"><a href="'.URL::route('route_data',$item['slug'].'pj').'">'.$item['name'].'</a>';
                }else if($stt==4){
                    if(URL::route('route_data',$item['slug'].'pj') == $checkurl){
                        $li_one = 'ul-active-four active-one';
                    }else{
                        $li_one = "";
                    }  
                    echo '<li class="'.$li_one.'"><a href="'.URL::route('route_data',$item['slug'].'pj').'">'.$item['name'].'</a>';
                }else if($stt==5){
                    if(URL::route('route_data',$item['slug'].'pj') == $checkurl){
                        $li_one = 'ul-active-five';
                    }else{
                        $li_one = "";
                    }  
                    echo '<li class="'.$li_one.'"><a href="'.URL::route('route_data',$item['slug'].'pj').'">'.$item['name'].'</a>';
                }                                                                      
                \Tree::showCategories($categories, $item['id'],$stt,$position++,$checkurl);
                
                echo '</li>';
                
            }
            
            echo '</ul>';                                                        
            
        }
        
    }
    public static function showComment($arr_cmt,$parent_id = 0,$stt=0){
        $menu_child = [];
        foreach ($arr_cmt as $key => $item)
        {                                   
            if ($item->parent_id == $parent_id)
            {
                $menu_child[] = $item;                 
                unset($arr_cmt[$key]);
            }
        }  

        if (count($menu_child)>0){

            $stt+=1;
            if($stt==1){
                echo '<ol class="comment-list">';
            }else if($stt==2){
                echo '<ol class="comment-list child-comments">';
            }
            foreach ($menu_child as $key => $item)
            {
                
                $reply="";
                $p_id="";
                if ($item->parent_id == $parent_id)
                {
                    if($parent_id==0){
                        $p_id = $item->id;
                    }else{
                        $p_id = $parent_id;
                    }
                    if($item->reply_name!=''){
                        $reply=$item->reply_name;
                    }
                    echo '<li class="comment">';
                    echo '<div class="comment-wrap"><div class="commenter-thumb"> <img class="img-responsive" src="'.Asset('asset/frontend').'/images/2.jpg" alt="Avatar"> </div>';
                    echo '<div class="comment-body">';
                    echo '<h6 class="comment-title"><span class="commenter-name">'.$item->name.' '.$reply.'</span><span class="comment-date"> /'.date("d M Y",strtotime($item->created_at)).'</span><a class="comment-reply reply" href="javascript:void(0)" data-id="'.$item->id.'" data-parent="'.$p_id.'" data-url="'.URL::action("\FontEnd\CommentController@postGetName").'"><i class="fa fa-reply"></i></a></h6>';
                    echo '<div class="comment-content"><p>'.$item->content.'</p></div>';
                    echo '</div>';  
                    echo '</div>';  
                    \Tree::showComment($arr_cmt, $item->id,$stt);    
                    echo '</li>';  
                    
                }
            }
            if($stt==1){
                echo '</ol>';                                                        
            }else if($stt==2){
                echo '</ol>';
            }
        }
           
    }
    public static function tree_no_lang_radio($listcateproduct, $selected = [], $name = '') {
        foreach ($listcateproduct as $item) {
            $check = false;
            if ($item->parent == 0) {
                if (in_array($item->id, $selected)) {
                    $check = true;
                }
                ?>
                <li class="checkbox_child">                          
                    <label class="mt-radio">
                        <?php echo Form::radio($name, $item->id, $check); ?><?php echo $item->name; ?> <span></span></label>
                    <ul>                                       
                        <?php
                        //Cấp 2
                        foreach ($listcateproduct as $item_node_one) {
                            $check = false;
                            if ($item_node_one->parent == $item->id) {
                                if (in_array($item_node_one->id, $selected)) {
                                    $check = true;
                                }
                                ?>
                                <li class="checkbox_child">
                                    <label class="mt-radio">
                                        <?php echo Form::radio($name, $item_node_one->id, $check); ?>  <?php echo $item_node_one->name; ?><span></span> 
                                    </label>
                                    <ul>                                       
                                        <?php
                                        //Cấp 3
                                        foreach ($listcateproduct as $item_node_two) {
                                            $check = false;
                                            if ($item_node_two->parent == $item_node_one->id) {
                                                if (in_array($item_node_two->id, $selected)) {
                                                    $check = true;
                                                }
                                                ?>
                                                <li class="checkbox_child">
                                                    <label class="mt-radio">
                                                        <?php echo Form::radio($name, $item_node_two->id, $check); ?>  <?php echo $item_node_two->name; ?> <span></span></label>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </li>
                <?php
            }
        }
    }

    public static function tree($listcateproduct, $selected = [], $lang_id = '') {
        foreach ($listcateproduct as $item) {
            if ($item->lang_id == $lang_id) {
                $check = false;
                if ($item->parent == 0) {
                    if (in_array($item->id, $selected)) {
                        $check = true;
                    }
                    ?>
                    <li class="checkbox_child">                          
                        <label class="mt-checkbox">
                            <?php echo Form::checkbox('cat_id_' . $lang_id . '[]', $item->id, $check); ?><?php echo $item->name; ?> <span></span></label>
                        <ul>                                       
                            <?php
                            //Cấp 2
                            foreach ($listcateproduct as $item_node_one) {
                                if ($item_node_one->lang_id == $lang_id) {
                                    $check = false;
                                    if ($item_node_one->parent == $item->id) {
                                        if (in_array($item_node_one->id, $selected)) {
                                            $check = true;
                                        }
                                        ?>
                                        <li class="checkbox_child">
                                            <label class="mt-checkbox">
                                                <?php echo Form::checkbox('cat_id_' . $lang_id . '[]', $item_node_one->id, $check); ?>  <?php echo $item_node_one->name; ?><span></span> 
                                            </label>
                                            <ul>                                       
                                                <?php
                                                //Cấp 3
                                                foreach ($listcateproduct as $item_node_two) {
                                                    if ($item_node_two->lang_id == $lang_id) {
                                                        $check = false;
                                                        if ($item_node_two->parent == $item_node_one->id) {
                                                            if (in_array($item_node_two->id, $selected)) {
                                                                $check = true;
                                                            }
                                                            ?>
                                                            <li class="checkbox_child">
                                                                <label class="mt-checkbox">
                                                                    <?php echo Form::checkbox('cat_id_' . $lang_id . '[]', $item_node_two->id, $check); ?> <?php echo $item_node_two->name; ?><span></span> </label>
                                                            </li>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </li>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </ul>
                    </li>
                    <?php
                }
            }
        }
    }

    public static function tree_no_lang($listcateproduct, $selected = [], $name = '') {
        foreach ($listcateproduct as $item) {
            $check = false;
            if ($item->parent == 0) {
                if (in_array($item->id, $selected)) {
                    $check = true;
                }
                ?>
                <li class="checkbox_child">                          
                    <label class="mt-checkbox">
                        <?php echo Form::checkbox($name . '[]', $item->id, $check); ?>  <?php echo $item->name; ?> </label><span></span>
                    <ul>                                       
                        <?php
                        //Cấp 2
                        foreach ($listcateproduct as $item_node_one) {
                            $check = false;
                            if ($item_node_one->parent == $item->id) {
                                if (in_array($item_node_one->id, $selected)) {
                                    $check = true;
                                }
                                ?>
                                <li class="checkbox_child">
                                    <label class="mt-checkbox">
                                        <?php echo Form::checkbox($name . '[]', $item_node_one->id, $check); ?> <?php echo $item_node_one->name; ?><span></span> 
                                    </label>
                                    <ul>                                       
                                        <?php
                                        //Cấp 3
                                        foreach ($listcateproduct as $item_node_two) {
                                            $check = false;
                                            if ($item_node_two->parent == $item_node_one->id) {
                                                if (in_array($item_node_two->id, $selected)) {
                                                    $check = true;
                                                }
                                                ?>
                                                <li class="checkbox_child">
                                                    <label class="mt-checkbox">
                                                        <?php echo Form::checkbox($name . '[]', $item_node_two->id, $check); ?> <?php echo $item_node_two->name; ?> <span></span></label>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </li>
                <?php
            }
        }
    }

    public static function tree_name($listcateproduct, $selected = [], $lang_id, $name = '') {
        foreach ($listcateproduct as $item) {
            if ($item->lang_id == $lang_id) {
                $check = false;
                if ($item->parent == 0) {
                    if (in_array($item->id, $selected)) {
                        $check = true;
                    }
                    ?>
                    <li class="checkbox_child">                          
                        <label class="mt-checkbox">
                            <?php echo Form::checkbox($name . '[]', $item->id, $check); ?> <?php echo $item->name; ?> <span></span></label>
                        <ul>                                       
                            <?php
                            //Cấp 2
                            foreach ($listcateproduct as $item_node_one) {
                                if ($item_node_one->lang_id == $lang_id) {
                                    $check = false;
                                    if ($item_node_one->parent == $item->id) {
                                        if (in_array($item_node_one->id, $selected)) {
                                            $check = true;
                                        }
                                        ?>
                                        <li class="checkbox_child">
                                            <label class="mt-checkbox">
                                                <?php echo Form::checkbox($name . '[]', $item_node_one->id, $check); ?> <?php echo $item_node_one->name; ?><span></span> 
                                            </label>
                                            <ul>                                       
                                                <?php
                                                //Cấp 3
                                                foreach ($listcateproduct as $item_node_two) {
                                                    if ($item_node_two->lang_id == $lang_id) {
                                                        $check = false;
                                                        if ($item_node_two->parent == $item_node_one->id) {
                                                            if (in_array($item_node_two->id, $selected)) {
                                                                $check = true;
                                                            }
                                                            ?>
                                                            <li class="checkbox_child">
                                                                <label class="mt-checkbox">
                                                                    <?php echo Form::checkbox($name . '[]', $item_node_two->id, $check); ?>  <?php echo $item_node_two->name; ?> <span></span></label>
                                                            </li>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </li>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </ul>
                    </li>
                    <?php
                }
            }
        }
    }

    public static function treereturn($listcateproduct, $selected = [], $lang_id = '') {
        $html = '';
        foreach ($listcateproduct as $item) {
            if ($item->lang_id == $lang_id) {
                $check = false;
                if ($item->parent == 0) {
                    if (in_array($item->id, $selected)) {
                        $check = true;
                    }
                    ?>
                    <?php
                    $html .= '<li class="checkbox_child">                          
                        <label class="mt-checkbox">';
                    ?>
                    <?php $html .= Form::checkbox('cat_id_' . $lang_id . '[]', $item->id, $check); ?> <?php $html .=''; ?> <?php $html .=$item->name; ?> <?php $html .='<span></span></label>'; ?>
                    <?php $html .= '<ul>'; ?>                                       
                    <?php
                    //Cấp 2
                    foreach ($listcateproduct as $item_node_one) {
                        if ($item_node_one->lang_id == $lang_id) {
                            $check = false;
                            if ($item_node_one->parent == $item->id) {
                                if (in_array($item_node_one->id, $selected)) {
                                    $check = true;
                                }
                                ?>
                                <?php $html .='<li class="checkbox_child">
                                            <label class="mt-checkbox">'; ?>
                                <?php $html .= Form::checkbox('cat_id_' . $lang_id . '[]', $item_node_one->id, $check); ?><?php $html .=''; ?> <?php $html .= $item_node_one->name; ?> 
                                <?php $html .='<span></span></label>
                                <ul>'; ?>                                       
                                <?php
                                //Cấp 3
                                foreach ($listcateproduct as $item_node_two) {
                                    if ($item_node_two->lang_id == $lang_id) {
                                        $check = false;
                                        if ($item_node_two->parent == $item_node_one->id) {
                                            if (in_array($item_node_two->id, $selected)) {
                                                $check = true;
                                            }
                                            ?>
                                            <?php $html .='<li class="checkbox_child">
                                                    <label class="mt-checkbox">'; ?>
                                            <?php $html .= Form::checkbox('cat_id_' . $lang_id . '[]', $item_node_two->id, $check); ?> <?php $html .=''; ?> <?php $html .= $item_node_two->name; ?> <?php $html .= '<span></span></label>'; ?>
                                            <?php $html .='</li>'; ?>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <?php $html .='</ul>'; ?>
                                <?php $html .='</li>'; ?>
                                <?php
                            }
                        }
                    }
                    ?>
                    <?php $html .='</ul>'; ?>
                    <?php $html .='</li>'; ?>
                    <?php
                }
            }
        }
        return $html;
    }

    public static function tree_name_cate($menus, $id_parent = 0, $posi = 0) {
        $menu_tmp = array();
        foreach ($menus as $key => $item) {
            if ((int) $item['parent'] == (int) $id_parent) {
                $menu_tmp[] = $item;
                unset($menus[$key]);
            }
        }
        if ($menu_tmp) {
            foreach ($menu_tmp as $item) {
                if ($item['parent'] == 0) {
                    echo '<option "value="' . URL::route('product_route', $item['slug'] . '-c' . $item['id']) . '">' . $item['name'];
                    echo '</option>';
                    Tree::tree_name_cate($menus, $item['id']);
                } else {

                    $fix = "—";
                    for ($i = 0; $i < $posi; $i++) {
                        $fix.='—';
                    }
                    $fix.=" ";
                    echo ' <option value="' . URL::route('product_route', $item['slug'] . '-c' . $item['id']) . '">' . $fix . $item['name'] . '</option>';
                    Tree::tree_name_cate($menus, $item['id'], $posi++);
                }
            }
        }
    }

    public static function tree_name_cate1($menus, $id_parent = 0, $posi = 0) {
        $menu_tmp = array();
        foreach ($menus as $key => $item) {
            if ((int) $item['parent'] == (int) $id_parent) {
                $menu_tmp[] = $item;
                unset($menus[$key]);
            }
        }
        if ($menu_tmp) {
            foreach ($menu_tmp as $item) {
                if ($item['parent'] == 0) {
                    echo '<optgroup label="' . $item['name'] . '">';
                    Tree::tree_name_cate1($menus, $item['id']);
                    echo '</optgroup>';
                } else {

                    $fix = " ";
                    for ($i = 0; $i < $posi; $i++) {
                        $fix.='—';
                    }
                    $fix.=" ";
                    echo ' <option value="' . URL::route('news_route', $item['slug']) . '">' . $fix . $item['name'] . '</option>';
                    Tree::tree_name_cate1($menus, $item['id'], $posi++);
                }
            }
        }
    }

    public static function tree_loop($listcateproduct, $selected = [], $lang_id = 1, $id_parent = 0, $level = 0) {
        $menu_tmp = array();
        foreach ($listcateproduct as $key => $item) {
            if ($item->lang_id == $lang_id) {
                if ((int) $item->parent == (int) $id_parent) {
                    $menu_tmp[] = $item;
                    unset($listcateproduct[$key]);
                }
            }
        }
        if ($menu_tmp) {
            if ($level > 0) {
                echo '<ul>';
            }
            $level_next = $level + 1;
            foreach ($menu_tmp as $item) {
                $check = FALSE;
                if (in_array($item->id, $selected)) {
                    $check = true;
                }
                if ($id_parent == 0) {
                    ?>
                    <li class="checkbox_child">                          
                        <label class="mt-checkbox">
                            <?php echo Form::checkbox('cat_id_' . $lang_id . '[]', $item->id, $check); ?> <?php echo $item->name; ?> <span></span></label>
                            <?php Tree::tree_loop($listcateproduct, $selected, $lang_id, $item->id, $level_next); ?>
                    </li>                                       

                    <?php
                } else {
                    ?>
                    <li class="checkbox_child">
                        <label class="mt-checkbox">
                            <?php echo Form::checkbox('cat_id_' . $lang_id . '[]', $item->id, $check); ?> <?php echo $item->name; ?> <span></span>
                        </label>
                        <?php Tree::tree_loop($listcateproduct, $selected, $lang_id, $item->id, $level_next); ?>
                    </li>
                    <?php
                }
            }
            if ($level > 0) {
                echo '</ul>';
            }
        }
    }

}
