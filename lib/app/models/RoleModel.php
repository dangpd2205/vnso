<?php

class RoleModel extends Eloquent {

    public function __construct() {
        
    }

    public static function attributes($attributes) {
        $html = array();

        // For numeric keys we will assume that the key and the value are the same
        // as this will convert HTML attributes such as "required" to a correct
        // form like required="required" instead of using incorrect numerics.
        foreach ((array) $attributes as $key => $value) {
            $element = RoleModel::attributeElement($key, $value);

            if (!is_null($element))
                $html[] = $element;
        }

        return count($html) > 0 ? ' ' . implode(' ', $html) : '';
    }

    public static function attributeElement($key, $value) {
        if (is_numeric($key))
            $key = $value;

        if (!is_null($value))
            return $key . '="' . e($value) . '"';
    }

    public static function GenLink($action = '', $title = '', $parameters = [], $attributes = []) {
        $all_role = tblGroupAdminModel::find(Auth::user()->group_admin_id);
        $check = false;
        if (Auth::user()->root != 1) {
            if ($all_role) {
                if (Auth::user()->admin == 1) {
                    if ($all_role->roles_code) {
                        $array_role = explode(',', $all_role->roles_code);
                        if (in_array(str_replace("\\ADMIN\\", '', $action), $array_role)) {
                            $check = true;			
                        }
                    }
                }
            }
        } else {
            $check = true;
        }

        if ($check == true) {
            return '<a href="' . action($action, $parameters) . '"' . RoleModel::attributes($attributes) . '>' . $title . '</a>';
        }
    }

}
