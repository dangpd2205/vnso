<?php

class Ping extends Eloquent {

    public function __construct() {
        
    }

    public static function ping($title = '', $url = '') {
        //  var_dump(\Config::get('all.all_config')->ping_host);
        $showDebugInfo = FALSE;
        $replacementCount = 0;
        $userAgent = "pingrpc.php by tayefeh";
        foreach (explode(PHP_EOL, Config::get('all.all_config')->ping_host) as $item) {
            $host = $item; // Make a copy of $line
            $host = preg_replace('/^.*http:\/\//', '', $host); // Delete anything before http://
            $host = preg_replace('/\/.*$/', '', $host); // Delete anything after behind the hostname
            // get the path 
            $path = $item; // Make another copy of $line
            $path = preg_replace('/^.*http:\/\/[a-zA-Z0-9\-_\.]*\.[a-zA-Z]{1,3}\//', '', $path, -1, $replacementCount); // Delete anything before the path
            if (!$replacementCount)
                $path = ''; // if there was no replacement (i.e. no explicit path), act appropiately
            if ($host)
                $myList[$host] = $path;
        }
        // Use DOM to create the XML-File
        $xml = new DOMDocument('1.0');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        $xml->substituteEntities = false;

// Create the xml structure
        $methodCall = $xml->appendChild($xml->createElement('methodCall'));
        $methodName = $methodCall->appendChild($xml->createElement('methodName'));
        $params = $methodCall->appendChild($xml->createElement('params'));
        $param[1] = $params->appendChild($xml->createElement('param'));
        $value[1] = $param[1]->appendChild($xml->createElement('value'));
        $param[2] = $params->appendChild($xml->createElement('param'));
        $value[2] = $param[2]->appendChild($xml->createElement('value'));

// Set the node values
        $methodName->nodeValue = "weblogUpdates.ping";
        $value[1]->nodeValue = $title;
        $value[2]->nodeValue = $url;

        $xmlrpcReq = $xml->saveXML(); // Write the document into a string
        $xmlrpcLength = strlen($xmlrpcReq); // Get the string length.
        foreach ($myList as $host => $path) {
            $httpReq = "POST /" . $path . " HTTP/1.0\r\n";
            $httpReq .= "User-Agent: " . $userAgent . "\r\n";
            $httpReq .= "Host: " . $host . "\r\n";
            $httpReq .= "Content-Type: text/xml\r\n";
            $httpReq .= "Content-length: $xmlrpcLength\r\n\r\n";
            $httpReq .= "$xmlrpcReq\r\n";
            if ($pinghandle = @fsockopen($host, 80)) {
                @fputs($pinghandle, $httpReq);
                while (!feof($pinghandle)) {
                    $pingresponse = @fgets($pinghandle, 128);
                    if ($showDebugInfo)
                        echo htmlentities($pingresponse);
                }
                @fclose($pinghandle);
            }
        }
        return true;
    }

}
