<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    'department' => array(
        'code' => 'Mã ngành',
        'name' => 'Tên ngành',
        'profit' => 'Lợi nhuận bán lẻ',
        'profit_wholes' => 'Lợi nhuận bán buôn',
        'tax' => 'Thuế',
        'noti_inventory' => 'Báo hàng tồn',
        'noti_exp' => 'Báo hết hạn',
        'noti_outstock' => 'Báo hết hàng'
    ),
    'tbl_ship' => array(
        'ship_name' => 'tên'
    ),
    'tbl_branches' => array(
        'code' => 'Mã quản lý'
    ),
	'tbl_group' => array(
        'name' => 'Tên'
    ),
    'tbl_vendor' => array(
        'code' => 'Mã quản lý'
    ),
    'tbl_manufacture' => array(
        'manufacture_name' => 'tên',
        'code' => 'Mã quản lý'
    ),
    'tbl_properties' => array(
        'name' => 'tên'
    ),
    'tbl_category' => array(
        'product_add_cat_name' => 'tên nhóm'
    ),
    'tbl_bank_acc' => array(
        'name' => 'tên ngân hàng',
        'value' => 'số tài khoản'
    ),
    'tbl_location' => array(
        'name' => 'tên địa điểm',
        'description' => 'mô tả địa điểm'
    ),
    'tbl_comment' => array(
        'name' => 'Người gửi',
        'reply_name' => 'Người trả lời',
        'email' => 'Email',
        'phone' => 'Số điện thoại',
        'content' => 'Nội dung',
        'liked' => 'Lượt thích',
        'status' => 'Trạng thái'
    ),
    'tbl_coupons' => array(
        'p_id_list' => 'Danh sách sản phẩm',
        'coupons_amount' => 'Số lượng mã',
        'name' => 'Tên coupons',
        'description' => 'Mô tả',
        'coupons_discount' => 'Giảm giá',
        'coupons_limit_discount' => 'Giới hạn giảm giá',
        'coupons_condition' => 'Điều kiện áp dụng',
        'type_coupons' => 'Loại',
        'branch_id_select' => 'chi nhánh',
        'currency_id' => 'đơn vị tiền tệ',
        'coupons_note' => 'Ghi chú',
        'coupons_exp_date' => 'Ngày hết hạn',
        'status' => 'Trạng thái'
    ),
    'tbl_currency' => array(
        'name' => 'mã tiền tệ',
        'ex_rate' => 'tỷ giá'
    ),
    'tbl_customer' => array(
        'name' => 'Họ tên',
        'address' => 'Địa chỉ',
        'phone' => 'Số điện thoại',
        'identity' => 'CMND'
    ),
    'tbl_partner' => array(
        'name' => 'tên hạng thẻ',
        'discount' => 'chiết khấu',
        'point' => 'điểm áp dụng'
    ),
    'tbl_event' => array(
        'start' => 'Ngày bắt đầu',
        'end' => 'Ngày kết thúc',
        'hour_start' => 'Giờ áp dụng',
        'hour_end' => 'Giờ kết thúc',
        'name' => 'Tên chương trình',
        'content' => 'Nội dung',
        'status' => 'Trạng thái'
    ),
    'tbl_event_order_product' => array(
        'price' => 'Giá sản phẩm',
        'price_sales' => 'Giá chương trình',
        'amount' => 'Số lượng',
        'revenue' => 'Doanh thu',
        'profit' => 'Lợi nhuận'
    ),
    'tbl_event_order_product' => array(
        'price' => 'Giá sản phẩm',
        'price_sales' => 'Giá chương trình',
        'amount' => 'Số lượng',
        'revenue' => 'Doanh thu',
        'profit' => 'Lợi nhuận'
    ),
	'tbl_users' => array(
        'code' => 'Mã tài khoản',
        'full_name' => 'Họ tên',
        'address' => 'Địa chỉ',
        'phone' => 'Số điện thoại',
        'tax_number' => 'Mã số thuế',
        'email' => 'Email',
        'password' => 'Mật khẩu',
        'branches_id' => 'Chi nhánh'
    ),
    'tbl_expenditure_note' => array(
        'code' => 'Mã quản lý',
        'receiver_id' => 'Chi nhánh',
        'branches_id' => 'Chi nhánh',
        'expenditure_group_id' => 'Nhóm chi'
    ),
	'tbl_tweet' => array(
        'name' => 'Tên',
        'content' => 'Nội dung'
    ),
);
