<?php

return array(
    /*seo*/
    'time_post' => array(
        'question'=>'Thời gian đăng là gì',
        'answer'=>'',
    ),
    'comment_status' =>  array(
        'question'=>'Bật/tắt comment là gì',
        'answer'=>'',
    ),
    'ping_status' =>  array(
        'question'=>'Bật/tắt ping là gì',
        'answer'=>'',
    ),
    'robot_follow' =>  array(
        'question'=>'Robot follow là gì',
        'answer'=>'',
    ),
    'robot_index' =>  array(
        'question'=>'Robot index là gì',
        'answer'=>'',
    ),
    'robot_advanced'=>array(
        'question'=>'Robot advanced là gì',
        'answer'=>'',
    ),
    'news' => array(
        'images' => array(
            'question'=>'Ảnh tin tức là gì',
            'answer'=>'',
        ),
        'category' => array(
            'question'=>'Danh mục tin tức là gì',
            'answer'=>'',
        ),
        'title'=>array(
            'question'=>'Tiêu đề tin tức là gì',
            'answer'=>'',
        ),
        'description'=>array(
            'question'=>'Mô tả tin tức là gì',
            'answer'=>'',
        ),
        'content'=>array(
            'question'=>'Nội dung tin tức là gì',
            'answer'=>'',
        ),
        'tag'=>array(
            'question'=>'Tag bài viết là gì',
            'answer'=>'',
        ),        
    ),
    
    'project' => array(
        'images' => array(
            'question'=>'Ảnh dự án là gì',
            'answer'=>'',
        ),
        'category' => array(
            'question'=>'Danh mục dự án là gì',
            'answer'=>'',
        ),
        'title'=>array(
            'question'=>'Tiêu đề dự án là gì',
            'answer'=>'',
        ),
        'description'=>array(
            'question'=>'Mô tả dự án là gì',
            'answer'=>'',
        ),
        'content'=>array(
            'question'=>'Nội dung dự án là gì',
            'answer'=>'',
        ),
        'tag'=>array(
            'question'=>'Tag du an là gì',
            'answer'=>'',
        ),        
        'start'=>array(
            'question'=>'Ngày dự án bắt đầu là gì',
            'answer'=>'',
        ),  
        'end'=>array(
            'question'=>'Ngày dự án kết thúc là gì',
            'answer'=>'',
        ),
    ),
    
    'page' => array(
        'title'=>array(
            'question'=>'Tiêu đề trang là gì',
            'answer'=>'Vivamus sagittis lacus vel augue laoreet rutrum fVivamus sagittis lacus vel augue laoreet rutrum faucibus.aucibus.',
        ),
        'description'=>array(
            'question'=>'Mô tả trang là gì',
            'answer'=>'sdfsdfsdfsd',
        ),
        'content'=>array(
            'question'=>'Nội dung trang là gì',
            'answer'=>'sdfsdfdf',
        ),        
    ),
    
    'seo' => array( 
        'title'=>array(
            'question'=>'Tiêu đề seo là gì',
            'answer'=>'',
        ),
        'description'=>array(
            'question'=>'Mô tả seo là gì',
            'answer'=>'',
        ),
        'keyword'=>array(
            'question'=>'Từ khóa seo là gì',
            'answer'=>'sdfdfsd',
        ),
        'facebook' => array(
            'images' => array(
                'question'=>'Ảnh đại diện facebook là gì',
                'answer'=>'',
            ),
            'title' =>  array(
                'question'=>'Tiêu đề seo facebook là gì',
                'answer'=>'',
            ),           
            'description'=>array(
                'question'=>'Mô tả seo facebook là gì',
                'answer'=>'',
            ),                  
        ),  
        'google' => array(
            'images' => array(
                'question'=>'Ảnh đại diện google là gì',
                'answer'=>'',
            ),
            'title' =>  array(
                'question'=>'Tiêu đề seo google là gì',
                'answer'=>'',
            ),           
            'description'=>array(
                'question'=>'Mô tả seo google là gì',
                'answer'=>'',
            ), 
            
        ),
    ),
    
    'feedback' => array(
        'subject'=>array(
            'question'=>'Tiêu đề phản hồi là gì',
            'answer'=>'Vivamus sagittis lacus vel augue laoreet rutrum fVivamus sagittis lacus vel augue laoreet rutrum faucibus.aucibus.',
        ),
        'email'=>array(
            'question'=>'Email là gì',
            'answer'=>'sdfsdfsdfsd',
        ),
        'name'=>array(
            'question'=>'Người gửi là gì',
            'answer'=>'sdfsdfdf',
        ), 
        'reply'=>array(
            'question'=>'Người trả lời là gì',
            'answer'=>'sdfsdfdf',
        ), 
        'content'=>array(
            'question'=>'Nội dung phản hồi là gì',
            'answer'=>'sdfsdfdf',
        ), 
        'content_reply'=>array(
            'question'=>'Nội dung trả lời là gì',
            'answer'=>'sdfsdfdf',
        ), 
    ),
    
);

