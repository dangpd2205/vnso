<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    
    'title_product' => array(
        'product_name' => 'Tên sản phẩm',
        'product_img' => 'Ảnh đại diện',
        'product_price' => 'Giá bán',
        'product_wholed_price' => 'Giá buôn',
        'product_sales_price' => 'Giá khuyến mại',
        'product_start_date' => 'Từ ngày',
        'product_end_date' => 'Đến ngày',
        'product_time_post' => 'Ngày đăng',
        'product_description' => 'Mô tả',
        'product_attitudes' => 'Thuộc tính',
        'product_keywords' => 'Từ khóa',
        'product_tag' => 'Tag sản phẩm',
    ),
    //title setting
    'title_setting' => array(
        'company_name' => 'Tên công ty',
        'company_address' => 'Địa chỉ',
        'company_email' => 'Email',
        'company_phone' => 'Điện thoại cố định',
        'company_mobile' => 'Điện thoại di động',
        'company_hotline' => 'Đường dây nóng',
        'website_name' => 'Tên website',
        'fb_fanpage' => 'Facebook Fanpage',
        'tweet_fanpage' => 'Tweet Fanpage',
        'gplus_fanpage' => 'Tài khoản G+',
        'linkedin_fanpage' => 'Tài khoản Linkedin',
        'account_payment' => 'Tài khoản thanh toán',
        'title_website' => 'Tiêu đề website',
        'logo_website' => 'Logo website',
        'website_description' => 'Mô tả website',
        'seo_keyword_search' => 'Từ khóa SEO',
        'google_analytics' => 'Google Analytics Code',
    ),
    //title news 
    'title_news' => array(
        'images' => 'Ảnh bài viết',
        'news_title' => 'Tiêu đề',
        'news_excerpt' => 'Mô tả',
        'news_content' => 'Nội dung',
        'news_tags' => 'Tag',
        'time_post' => 'Thời gian đăng',
        'status' => 'Trạng thái',
        'created_at' => 'Khởi tạo',
        'comment_status' => 'Bật/tắt comment bài viết',
        'ping_status' => 'Bật/tắt ping bài viết',
        'robots_index' => 'Robot index',
        'robots_follow' => 'Robot follow',
        'title' => 'Tiêu đề seo',
        'description' => 'Mô tả seo',
        'keyword' => 'Từ khóa seo',
        'fb_title' => 'Tiêu đề facebook',
        'fb_description' => 'Mô tả facebook',
        'fb_image' => 'Ảnh facebook',
        'g_title' => 'Tiêu đề g+',
        'g_description' => 'Mô tả g+',
        'g_image' => 'Ảnh g+',
    ),
    //title cate news
    'title_cate_news' => array(
        'cate_news'=>'Danh mục tin tức'
    ),
    //title cate project
    'title_cate_project' => array(
        'cate_project'=>'Danh mục dự án'
    ),
    //title lang
    'title_lang' => array(
        'other'=>'Ngôn ngữ khác'
    ),
    //title seo
    'title_seo' => array(
        'title'=>'Tiêu đề',
        'description'=>'Mô tả',
        'keyword'=>'Từ khóa',
        'fb_title'=>'Tiêu đề facebook',
        'fb_description'=>'Mô tả facebook',
        'fb_image'=>'Ảnh facebook',
        'g_title'=>'Tiêu đề g+',
        'g_description'=>'Mô tả g+',
        'g_image'=>'Ảnh g+',
    ),
    //title page 
    'title_page' => array(
        'page_name' => 'Tiêu đề',
        'page_excerpt' => 'Mô tả',
        'page_content' => 'Nội dung',
        'page_slug' => 'Slug',
        'created_at' => 'Khởi tạo',
        'status' => 'Trạng thái',
        'time_post' => 'Thời gian đăng',
        'comment_status' => 'Bật/tắt comment',
        'ping_status' => 'Bật/tắt ping',
    ),
    
    //title project 
    'title_project' => array(
        'images' => 'Ảnh',
        'project_title' => 'Tiêu đề',
        'project_excerpt' => 'Mô tả',
        'project_content' => 'Nội dung',
        'project_slug' => 'Slug',
        'project_tags' => 'Tag',
        'created_at' => 'Khởi tạo',
        'time_post' => 'Thời gian đăng',
        'start' => 'Ngày bắt đầu',
        'end' => 'Ngày kết thúc',
        'from' => 'Từ',
        'to' => 'Tới',
        'status' => 'Trạng thái',
        'created_at' => 'Khởi tạo',
        'comment_status' => 'Bật/tắt comment',
        'ping_status' => 'Bật/tắt ping',
        'robots_index' => 'Robot index',
        'robots_follow' => 'Robot follow',
        'title' => 'Tiêu đề seo',
        'description' => 'Mô tả seo',
        'keyword' => 'Từ khóa seo',
        'fb_title' => 'Tiêu đề facebook',
        'fb_description' => 'Mô tả facebook',
        'fb_image' => 'Ảnh facebook',
        'g_title' => 'Tiêu đề g+',
        'g_description' => 'Mô tả g+',
        'g_image' => 'Ảnh g+',
        'time' => 'Thời gian',
    ),
    
    //title supporter
    'title_supporter' => array(
        'supporter_name' => 'Tên hỗ trợ viên',
        'supporter_yahoo' => 'Yahoo',
        'supporter_skype' => 'Skype',
        'supporter_phone' => 'Điện thoại',
        'created_at' => 'Khởi tạo',
        'supporter_group_id' => 'Nhóm hỗ trợ viên',
    ),
    //title user
    'title_user' => array(
        'tax_number' => 'Mã số thuế',
        'gender' => 'Giới tính',
        'type' => 'Loại KH',
        'code' => 'Mã KH',
        'email' => 'Email',
        'password' => 'Mật khẩu',
        'full_name' => 'Họ tên',
        'date_of_birth' => 'Ngày sinh',
        'address' => 'Địa chỉ',
        'phone' => 'Số điện thoại',
        'avatar' => 'Ảnh đại diện',
        'partner_id' =>'Nhóm khách hàng',
        'created_at' =>'Khởi tạo',
        'status' =>'Trạng thái',
        'sum_money' =>'Tổng bán',
        'sum_money_paid' =>'Tổng trả',
        'money' => 'Số dư',
        'point' => 'Điểm',
    ),
    
    //title partner
    'title_partner' => array(
        'partner_name' => 'Tên',
        'partner_discount' => 'Chiết khấu',
        
    ),
    'title_card' => array(
        'card_number' => 'Mã thẻ',
        'partner_id' => 'Hạng thẻ',
		'user_id' => 'Khách hàng',
    ),
    //title feedback
    'title_feedback' => array(
        'email' => 'Email',
        'name' => 'Người gửi',
        'reply' => 'Người trả lời',
        'subject' => 'Tiêu đề',
        'content' => 'Nội dung phản hồi',
        'content_reply' => 'Nội dung trả lời',
        'created_at' => 'Thời gian',
        'status' => 'Trạng thái',
    ),
);
