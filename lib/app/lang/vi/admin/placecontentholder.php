<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    //place holder setting
    'placeholder_setting' => array(
        'company_name' => 'Nhập tên công ty hoặc tên cửa hàng',
        'company_address' => 'Nhập địa chỉ công ty hoặc địa chỉ cửa hàng',
        'company_email' => 'Nhập email công ty hoặc email cửa hàng',
        'company_phone' => 'Nhập số điện thoại cố định',
        'company_mobile' => 'Nhập số điện thoại di động',
        'company_hotline' => 'Nhập số điện thoại đường dây nóngF',
        'website_name' => 'Nhập tên website',
        'fb_fanpage' => 'Nhập Facebook Fanpage',
        'tweet_fanpage' => 'Nhập Tweet Fanpage',
        'gplus_fanpage' => 'Nhập tài khoản G+',
        'linkedin_fanpage' => 'Nhập tài khoản Linkedin',
        'account_payment' => 'Nhập tài khoản thanh toán',
        'title_website' => 'PUBWEB.VN',
        'logo_website' => 'Nhập đường dẫn ảnh',
        'website_description' => 'Mô tả website',
        'seo_keyword_search' => 'Nhập từ khóa SEO',
        'google_analytics' => 'Google Analytics Code',
    ),
    //place holder news 
    'placeholder_news' => array(
        'news_name' => 'Nhập tiêu đề bài viết',
        'news_descriptions' => 'Nhập mô tả bài viết',
        'news_content' => 'Nhập nội dung bài viết',
        'news_tags' => 'Nhập tag bài viết',
        'news_image' => 'Nhập ảnh',
        'news_title' => 'Tiêu đề',
    ),
    //place holder page 
    'placeholder_page' => array(
        'page_name' => 'Nhập tiêu đề bài viết',
        'page_excerpt' => 'Nhập mô tả bài viết',
        'page_content' => 'Nhập nội dung bài viết',
    ),
    //place holder project
    'placeholder_project' => array(
        'images' => 'Ảnh',
        'project_name' => 'Tiêu đề',
        'project_excerpt' => 'Mô tả',
        'project_content' => 'Nội dung',
        'project_slug' => 'Slug',
        'project_tags' => 'Tag',
        'created_at' => 'Khởi tạo',
        'time_post' => 'Thời gian đăng',
        'project_start' => 'Ngày bắt đầu',
        'project_end' => 'Ngày kết thúc',
        'status' => 'Trạng thái',
        'created_at' => 'Khởi tạo',
        'comment_status' => 'Bật/tắt comment',
        'ping_status' => 'Bật/tắt ping',
        'robots_index' => 'Robot index',
        'robots_follow' => 'Robot follow',
        'title' => 'Tiêu đề seo',
        'description' => 'Mô tả seo',
        'keyword' => 'Từ khóa seo',
        'fb_title' => 'Tiêu đề facebook',
        'fb_description' => 'Mô tả facebook',
        'fb_image' => 'Ảnh facebook',
        'g_title' => 'Tiêu đề g+',
        'g_description' => 'Mô tả g+',
        'g_image' => 'Ảnh g+',
    ),
    
    //place holder supporter 
    'placeholder_supporter' => array(
        'supporter_name' => 'Tên hỗ trợ viên',
        'supporter_yahoo' => 'Yahoo',
        'supporter_skype' => 'Skype',
        'supporter_phone' => 'Điện thoại',
        'created_at' => 'Khởi tạo',
        'supporter_group_id' => 'Nhóm hỗ trợ viên',
    ),
    'placeholder_content' => array(
        'keyword' => 'Nhập từ khóa',
        'empty_no_change' => 'Để trống nếu không thay đổi',
        'personal'=>'Cá nhân',
        'business'=>'Doanh nghiệp',
        'male'=>'Nam',
        'female'=>'Nữ',
        'na'=>'N/A',
        'social'=>'Mạng xã hội',
        'empty'=>'Không có dữ liệu',
        'from'=>'Từ',
        'to'=>'Tới',
    ),
    'placeholder_feedback' => array(
        'content_reply' => 'Nội dung trả lời',
        
    ),
	'product' => array(
        'code' => 'Mã',
        'product_tax' => 'Thuế',
        'noti_inventory' => 'Báo hàng tồn',
        'noti_exp' => 'Báo hết hạn',
        'noti_outstock' => 'Báo hết hàng'
    ),
);
