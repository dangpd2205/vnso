<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    'bt_back' => 'Quay lại',
    'bt_add' => 'Thêm mới',
    'bt_edit' => 'Sửa',
    'bt_filter' => 'Lọc',
    'bt_update' => 'Cập nhật',
    'bt_quick_update' => 'Cập nhật nhanh',
    'bt_delete' => 'Xoá',
    'bt_reset' => 'Làm lại',
    'bt_deactive' => 'Chờ kích hoạt',
    'bt_lock' => 'Khóa',
    'bt_active' => 'Kích hoạt',
    'bt_save' => 'Lưu lại',
    'bt_post' => 'Đăng bài',
    'bt_unpost' => 'Gỡ bài',
    'bt_read' => 'Đã xem',
    'bt_unread' => 'Chưa đọc',
    'bt_yes' => 'Có',
    'bt_no' => 'Không',
    'bt_login' => 'Đăng nhập',
    'bt_forgot_password' => 'Quên mật khẩu',
    'bt_click_here' => 'Click vào đây',
    'bt_send_email' => 'Gửi email',
    'bt_choose_image' => 'Chọn hình ảnh',
    'bt_cancel' => 'Hủy',
    'bt_ok' => 'Đồng ý',
    'bt_addImage' => 'Thêm ảnh',
    'bt_upload' => 'Tải lên',
    'bt_reply' => 'Trả lời',
);
