<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
     'product_status' => array(
        '0' => 'Chờ',
        '1' => 'Đã đăng',
        '2' => 'Ngừng bán',
        '3' => 'Xóa',
    ),
    'coupons_status' => array(
        '0' => 'Chờ',
        '1' => 'Đã phát hành',
        '2' => 'Xóa',
    ),
	   'coupons_type' => array(
        '0' => 'Coupons',
        '1' => 'Giftcard',
    ),
    'code' => array(
        '0' => 'Chưa duyệt',
        '1' => 'Đã phát hành',
        '2' => 'Xóa',
        '3' => 'Đã sử dụng'
    ),
    'product_history' => array(
        '1' => 'Bán',
        '2' => 'Nhập',
        '3' => 'Trả',
        '4' => 'Mất',
        '5' => 'Hủy',
        '6' => 'Xóa',
    ),
    'product_order' => array(
        '0' => 'Chờ',
        '1' => 'Hoàn tất',
        '2' => 'Hủy',
        '3' => 'Xóa',
    ),
    'product_import' => array(
        '0' => 'Chờ cập nhật',
        '1' => 'Đã xong',
        '2' => 'Hủy',
    ),
	'partner_status' => array(
        '1' => 'Kích hoạt',
        '2' => 'Xóa',
    ),
	 'store_check' => array(
        '0' => 'Lưu tạm',
        '1' => 'Đã hoàn tất',
        '2' => 'Xóa',
    ),
	'partner_status' => array(
        '1' => 'Kích hoạt',
        '2' => 'Xóa',
    ),
    'income_status' => array(
		'0'=>'Chờ',
        '1' => 'Kích hoạt',
        '2' => 'Xóa',
    ),
    'expenditure_status' => array(
		'0'=>'Chờ',
        '1' => 'Kích hoạt',
        '2' => 'Xóa',
    ),
	  'shift_status' => array(
        '0' => 'Đang bán hàng ',
        '1' => 'Đã hoàn thành',
        '2' => 'Hủy',
    ),
	'order_status' => array(
        '0' => 'Chờ xử lý ',
        '1' => 'Hoàn tất',
        '2' => 'Hủy',
		'3' => 'Xóa',
    ),
);
