<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    'msg_error' => 'Có lỗi xảy ra trong quá trình quy xuất dữ liệu',
    'msg_alert' => 'Thông báo!',
    'msg_delete_confirm' => 'Bạn có chắc chắn muốn xóa ?',
    'msg_data_loading' => 'Đang tải dữ liệu ...',
    'msg_data_load_success' => 'Đã tải dữ liệu thành công ...',
    'msg_select_empty' => 'Bạn chưa chọn giá trị nào',
    'msg_date_begin_end' => 'Ngày kết thúc lớn hơn ngày bắt đầu.',
    'msg_add' => array(
        'success' => 'Thêm mới thành công.',
        'error' => 'Có lỗi xảy ra trong quá trình thêm mới.'
    ),
    'msg_save' => array(
        'success' => 'Lưu tạm thành công.',
        'error' => 'Có lỗi xảy ra trong quá trình lưu tạm.'
    ),
    'msg_delete' => array(
        'success' => 'Xóa thành công.',
        'error' => 'Có lỗi xảy ra trong quá trình xóa.'
    ),
    'msg_update' => array(
        'success' => 'Cập nhật thành công.',
        'error' => 'Có lỗi xảy ra trong quá trình cập nhật.'
    ),
    'msg_news' => array(
        'cancel' => 'Tin đã được hủy',
        'posted' => 'Tin đã được đăng',
        'wait' => 'Tin đang chờ đăng',
        'save' => 'Lưu tạm'
    ),
    'msg_login' => array(
        'success' => 'Đăng nhập thành công',
        'error' => '<strong> Lỗi!</strong> Tài khoản hoặc mật khẩu không đúng !'
    ),
    'msg_reply' => array(
        'success' => 'Gửi trả lời phản hồi thành công.',
        'error' => 'Có lỗi xảy ra trong quá trình trả lời phản hồi.'
    ),
    'msg_forgot_password' => array(
        'locked' => 'Tài khoản của bạn đã bị khoá!',
        'login_error' => 'Email hoặc mật khẩu sai !',
        'back_to_login' => 'Quay về trang đăng nhập?',
        'email_exist' => '<strong>Thông báo !</strong> Email không tôn tại trên hệ thống!',
        'forgot_success' => '<strong>Thông báo !</strong> Bạn check email để lấy lại mật khẩu!'
    ),
);
