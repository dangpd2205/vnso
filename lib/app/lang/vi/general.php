<?php

return array(
//    news
    'news_status' => array(
        '' => 'Tất cả',
        0 => 'Chờ đăng',
        1 => 'Đã đăng',
        2 => 'Xoá',
        3 => 'Lưu tạm',
    ),
    //    project
    'project_status' => array(
        '' => 'Tất cả',
        0 => 'Chờ đăng',
        1 => 'Đã đăng',
        2 => 'Xoá',
        3 => 'Lưu tạm',
    ),
    //    user
    'user_status' => array(
        '' => 'Tất cả',
        0 => 'Chờ kích hoạt',
        1 => 'Đã kích hoạt',
        2 => 'Đã khóa',
    ),
    //    page
    'page_status' => array(
        '' => 'Tất cả',
        0 => 'Chờ đăng',
        1 => 'Đã đăng',
        2 => 'Xoá',
        3 => 'Lưu tạm',
    ),
    //    feedback
    'feedback_status' => array(        
        0 => 'Chờ trả lời',
        1 => 'Đã trả lời',
        2 => 'Đã xóa',
    ),
	'thanhtoan_status' => array(        
        0 => 'Chờ xử lý',
        1 => 'Hoàn tất',
        2 => 'Đã xóa',
    ),
	'xuathang_status' => array(
        '0' => 'Chờ',
        '1' => 'Hoàn tất',
        '2' => 'Hủy',
        '3' => 'Xóa',
    ),
    'xuathang_type' => array(
        '0' => 'Nội bộ',
        '1' => 'Ngoại giao',
    ),
    'nccorder_status' => array(
        '0' => 'Chờ',
        '1' => 'Hoàn tất',
        '2' => 'Hủy',
        '3' => 'Xóa',
    ),
	'chuyenkho_status' => array(        
        0 => 'Chờ xử lý',
        1 => 'Hoàn tất',
        2 => 'Đã xóa',
    ),
	'partner_status' => array(
        '1' => 'Kích hoạt',
        '2' => 'Xóa',
    ),
);

