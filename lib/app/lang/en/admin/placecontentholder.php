<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    //place holder setting
    'placeholder_setting' => array(
        'company_name' => 'Nhập tên công ty hoặc tên cửa hàng',
        'company_address' => 'Nhập địa chỉ công ty hoặc địa chỉ cửa hàng',
        'company_email' => 'Nhập email công ty hoặc email cửa hàng',
        'company_phone' => 'Nhập số điện thoại cố định',
        'company_mobile' => 'Nhập số điện thoại di động',
        'company_hotline' => 'Nhập số điện thoại đường dây nóngF',
        'website_name' => 'Nhập tên website',
        'fb_fanpage' => 'Nhập Facebook Fanpage',
        'tweet_fanpage' => 'Nhập Tweet Fanpage',
        'gplus_fanpage' => 'Nhập tài khoản G+',
        'linkedin_fanpage' => 'Nhập tài khoản Linkedin',
        'account_payment' => 'Nhập tài khoản thanh toán',
        'title_website' => 'PUBWEB.VN',
        'logo_website' => 'Nhập đường dẫn ảnh',
        'website_description' => 'Mô tả website',
        'seo_keyword_search' => 'Nhập từ khóa SEO',
        'google_analytics' => 'Google Analytics Code',
    ),
   //place holder news 
       'placeholder_news' => array(
        'news_name' => 'Nhập tiêu đề bài viết',
        'news_descriptions' => 'Nhập mô tả bài viết',
        'news_content' => 'Nhập nội dung bài viết',
        'news_tags' => 'Nhập tag bài viết',
    ),
    
);
