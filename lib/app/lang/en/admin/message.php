<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    'msg_error' => 'There is something wrong when request data',
    'msg_alert' => 'Notice!',
    'msg_delete_confirm' => 'Are you sure ?',
    'msg_data_loading' => 'Loading data ...',
    'msg_data_load_success' => 'Data loaded ...',
    'msg_select_empty' => 'Nothing is chose',
    'msg_date_begin_end' => 'End date more than begin date!',
    'msg_add' => array(
        'success' => 'Add new success.',
        'error' => 'There is something wrong when add new data.'
    ),
    'msg_update' => array(
        'success' => 'Updated success',
        'error' => 'There is something wrong when update data.'
    ),
     'msg_news' => array(
    ),
    'msg_login' => array(
        'success' => 'Login success',
        'error' => 'Account name or password incorrect !'
    ),
);
