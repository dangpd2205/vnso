<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    'bt_back' => 'Back',
    'bt_add' => 'Add',
    'bt_edit' => 'Edit',
    'bt_update' => 'Update',
    'bt_delete' => 'Delete',
    'bt_reset' => 'Reset',
    'bt_active' => 'Active',
    'bt_save' => 'Save',
    'bt_post' => 'Publish',
    'bt_read' => 'Read',
    'bt_unread' => 'Unread',
    'bt_yes' => 'Yes',
    'bt_no' => 'No',
    'bt_login' => 'Login',
    'bt_forgot_password' => 'Forgot Password',
    'bt_click_here' => 'Click here',
    'bt_send_email' => 'Send',
    'bt_choose_image' => 'Choose',
    'bt_cancel' => 'Cancel',
    'bt_ok' => 'Ok',
    'bt_addImage' => 'Add Image',
    'bt_upload' => 'Upload',
);
