<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    'title_user' => array(
        'full_name' => 'Họ và tên',
        'address' => 'Địa chỉ',
        'password' => 'Mật khẩu',
        'phone' => 'Điện thoại',
        'cell_phone' => 'Di động',
        'date_of_birth' => 'Ngày sinh',
        'avatar' => 'Ảnh đại diện',
        'money' => 'Số dư',
        'point' => 'Điểm',
        'email' => 'Email',
    ),
    'title_product' => array(
        'product_name' => 'Tên sản phẩm',
        'product_img' => 'Ảnh đại diện',
        'product_price' => 'Giá bán',
        'product_wholed_price' => 'Giá buôn',
        'product_sales_price' => 'Giá khuyến mại',
        'product_start_date' => 'Từ ngày',
        'product_end_date' => 'Đến ngày',
        'product_time_post' => 'Ngày đăng',
        'product_description' => 'Mô tả',
        'product_attitudes' => 'Thuộc tính',
        'product_keywords' => 'Từ khóa',
        'product_tag' => 'Tag sản phẩm',
    ),
    //title setting
    'title_setting' => array(
        'company_name' => 'Tên công ty',
        'company_address' => 'Địa chỉ',
        'company_email' => 'Email',
        'company_phone' => 'Điện thoại cố định',
        'company_mobile' => 'Điện thoại di động',
        'company_hotline' => 'Đường dây nóng',
        'website_name' => 'Tên website',
        'fb_fanpage' => 'Facebook Fanpage',
        'tweet_fanpage' => 'Tweet Fanpage',
        'gplus_fanpage' => 'Tài khoản G+',
        'linkedin_fanpage' => 'Tài khoản Linkedin',
        'account_payment' => 'Tài khoản thanh toán',
        'title_website' => 'Tiêu đề website',
        'logo_website' => 'Logo website',
        'website_description' => 'Mô tả website',
        'seo_keyword_search' => 'Từ khóa SEO',
        'google_analytics' => 'Google Analytics Code',
    ),
    //title news 
    'title_news' => array(
        'news_name' => 'Tiêu đề',
        'news_descriptions' => 'Mô tả',
        'news_content' => 'Nội dung',
        'news_tags' => 'Tag bài viết',
        'created_at' => 'Khởi tạo',
        'status' => 'Trạng thái',
    ),
    
    
);
