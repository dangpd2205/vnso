<?php

/*
  |--------------------------------------------------------------------------
  | Application & Route Filters
  |--------------------------------------------------------------------------
  |
  | Below you will find the "before" and "after" events for the application
  | which may be used to do any work before or after a request into your
  | application. Here you may also register your custom route filters.
  |
 */

App::before(function($request) {
    //
});


App::after(function($request, $response) {
    //
});

/*
  |--------------------------------------------------------------------------
  | Authentication Filters
  |--------------------------------------------------------------------------
  |
  | The following filters are used to verify that the user of the current
  | session is logged into this application. The "basic" filter easily
  | integrates HTTP Basic authentication for quick, simple checking.
  |
 */

Route::filter('auth', function() {
    if (Auth::guest()) {
        if (Request::ajax()) {
            return Response::make('Unauthorized', 401);
        } else {
            return Redirect::guest('login');
        }
    }
});


Route::filter('auth.basic', function() {
    return Auth::basic();
});

/*
  |--------------------------------------------------------------------------
  | Guest Filter
  |--------------------------------------------------------------------------
  |
  | The "guest" filter is the counterpart of the authentication filters as
  | it simply checks that the current user is not logged in. A redirect
  | response will be issued if they are, which you may freely change.
  |
 */

Route::filter('guest', function() {
    if (Auth::check())
        return Redirect::to('/');
});

/*
  |--------------------------------------------------------------------------
  | CSRF Protection Filter
  |--------------------------------------------------------------------------
  |
  | The CSRF filter is responsible for protecting your application against
  | cross-site request forgery attacks. If this special token in a user
  | session does not match the one given in this request, we'll bail.
  |
 */

Route::filter('csrf', function() {
    if (Request::isMethod('post')) {
        if (!Request::ajax()) {
            if (Session::token() != Input::get('_token')) {
                return View::make('admin.404');
            } else {
                if (Input::get('_token') != '') {
                    if (Session::token() != Input::get('_token')) {
                        return FALSE;
                    }
                }
            }
        }
    }
});
Route::filter('check_user_login', function() {
    if (Auth::guest()) {
        Session::forget('url_back');
        Session::push('url_back', URL::current());
        return Redirect::action('\FontEnd\UsersController@getDangNhap');
    }
});
Route::filter('check_role', function() {
    $controllername = Route::currentRouteAction();
    $controllername = substr($controllername, 12);
    if (!Auth::guest()) {
        $user = Auth::user();
        $list_role = $user->group_admin_id;
        if($list_role!=''){
          $list_roles = DB::table('tbl_group_admin')
                    ->select('tbl_group_admin.roles_code')
                    ->whereIn('tbl_group_admin.id', explode(',', $list_role))
                    ->get();
        }else{
          $list_roles=[];
        }
        $str_roles='';
        if(count($list_roles)>0){
          foreach($list_roles as $item){
            $str_roles .= $item->roles_code;            
          }
        }
            if (strpos($str_roles, $controllername) >= 0 && strpos($str_roles, $controllername) !== false) {
                $check = true;
            } else {
                $check = false;
            }
    } else {
        
    }
    if (Auth::user()->root == 1) {
        $check = true;
    }
    if ($check == false) {
        return View::make('admin.index.404');
    }
});

Route::filter('check_admin_login', function() {
    if (!Auth::guest()) {
        $user = Auth::user();
        if ($user->admin != 1) {
            return Redirect::action('\ADMIN\HomeController@get404Admin');
        }
    } else {
        Session::forget('url_back');
        Session::push('url_back', URL::current());
        return Redirect::action('\ADMIN\HomeController@getLogin');
    }
});Route::filter('close_web', function() {	return View::make('frontend.baotri');});
