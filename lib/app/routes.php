<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

	Route::get('/', array('as' => 'home', 'uses' => '\FontEnd\HomeController@getTrangChu'));
	Route::get('/home2', array('as' => 'home2', 'uses' => '\FontEnd\BookingController@testsms'));
	Route::get('/sponsor-subcription.html', array('as' => 'sponsor', 'uses' => '\FontEnd\HomeController@getSponsor'));
	Route::post('/sponsor.html', array('as' => 'postsponsor', 'uses' => '\FontEnd\HomeController@postSponsor'));
	Route::get('/filter/{param}/{param1}/{param2}', array('as' => 'filter', 'uses' => '\FontEnd\ShowController@getFilter'));
	Route::post('/filter', array('as' => 'postfilter', 'uses' => '\FontEnd\ShowController@postFilter'));
	Route::get('/search/{keyword}', array('as' => 'search', 'uses' => '\FontEnd\HomeController@getSearch'));
	Route::post('/search', array('as' => 'search', 'uses' => '\FontEnd\HomeController@postSearch'));
	Route::get('/searchshow/{keyword}', array('as' => 'search', 'uses' => '\FontEnd\ShowController@getSearch'));
	Route::post('/searchshow', array('as' => 'searchshow', 'uses' => '\FontEnd\ShowController@postSearch'));
	Route::post('/sub', array('as' => 'sub', 'uses' => '\FontEnd\HomeController@postSub'));
	Route::post('/vote', array('as' => 'vote', 'uses' => '\FontEnd\HomeController@postVote'));
	Route::get('/lienhe.html', array('as' => 'contact', 'uses' => '\FontEnd\HomeController@getContact'));
	Route::post('/lienhe', array('as' => 'postcontact', 'uses' => '\FontEnd\HomeController@postContact'));
	Route::post('/comment', array('as' => 'postcomment', 'uses' => '\FontEnd\CommentController@postComment'));
	Route::get('/gallery.html', array('as' => 'gallery', 'uses' => '\FontEnd\HomeController@getGallery'));
	Route::get('/video.html', array('as' => 'video', 'uses' => '\FontEnd\HomeController@getVideo'));
	Route::get('/tintuc.html', array('as' => 'tintuc', 'uses' => '\FontEnd\HomeController@getTinTuc'));
	Route::get('/chuongtrinh.html', array('as' => 'chuongtrinh', 'uses' => '\FontEnd\ShowController@getShowAll'));
	Route::get('/img.html', array('as' => 'img', 'uses' => '\FontEnd\HomeController@getImg'));
	Route::get('/orchestra.html', array('as' => 'orchestra', 'uses' => '\FontEnd\HomeController@getArtist'));
	Route::post('/orchestra', array('as' => 'postartist', 'uses' => '\FontEnd\HomeController@postArtist'));
	Route::get('/seat/{id}.html', array('as' => 'seat', 'uses' => '\FontEnd\BookingController@getSeat'));
	Route::post('/holdseat', array('as' => 'holdseat', 'uses' => '\FontEnd\BookingController@postHoldseat'));
	Route::post('/clear', array('as' => 'clear', 'uses' => '\FontEnd\BookingController@postClear'));
	Route::post('/payment', array('as' => 'payment', 'uses' => '\FontEnd\BookingController@postPayment'));
	Route::get('/checkout.html', array('as' => 'checkout', 'uses' => '\FontEnd\BookingController@getConfirm'));
	Route::get('/404.html', array('as' => '404', 'uses' => '\FontEnd\BookingController@get404'));
	Route::get('/return.html', array('as' => 'return', 'uses' => '\FontEnd\BookingController@getReturn'));Route::get('/ipn.html', array('as' => 'ipn', 'uses' => '\FontEnd\BookingController@getIpn'));
	Route::get('/cod.html', array('as' => 'cod', 'uses' => '\FontEnd\BookingController@getCOD'));
	Route::post('/timeline', array('as' => 'posttimeline', 'uses' => '\FontEnd\HomeController@postTimelineajax'));
	Route::get('/doanh-nghiep-tai-tro/{id?}.html', array('as' => 'getmanuf', 'uses' => '\FontEnd\HomeController@getManuf'));
	Route::get('{slug?}.html', array(function($slug = '') {
        /* check slug */
        $last_c = substr($slug, -2);
        /* danh muc tin tuc */
        if ($last_c == "cn") {
            $news = new \FontEnd\HomeController();
            return $news->getDanhMuc($slug);
            /* chi tiet tin tuc */
        } else if ($last_c == "dn") {
            $news = new \FontEnd\HomeController();
            return $news->getChiTiet($slug);
            /* amp tin tuc */
        }else if ($last_c == "tn") {
            $news = new \FontEnd\HomeController();
            return $news->getTag($slug);           
        }else if ($last_c == "ds") {
            $news = new \FontEnd\ShowController();
            return $news->getShow($slug);
            /* amp tin tuc */
        }else if ($last_c == "cs") {
            $news = new \FontEnd\ShowController();
            return $news->getDanhMuc($slug);
            /* amp tin tuc */
        }else if ($last_c == "ts") {
            $news = new \FontEnd\ShowController();
            return $news->getTag($slug);
            /* amp tin tuc */
        }else if ($last_c == "dv") {
            $news = new \FontEnd\HomeController();
            return $news->getVideo($slug);           
        }else if ($last_c == "pg") {
            $news = new \FontEnd\HomeController();
            return $news->getPage($slug);           
        }
         
    },
    'as' => 'route_data'));

Route::post('/sub', array('as' => 'postsub', 'uses' => '\FontEnd\HomeController@postSub'));
Route::post('/reservation', array('as' => 'postreservation', 'uses' => '\FontEnd\HomeController@postContact'));
View::composer('frontend.header', function($view) {
    if (\Cookie::has('cookie_lang')) {
        $lang_code = \Cookie::get('cookie_lang');
    } else {
        $lang_code = \Config::get('all.all_config')->website_lang;
    }
    $tblMenuModel = new \FontEnd\tblMenuModel();
	$website_menu = \tblSettingModel::where('key','website_menu_'.$lang_code)->first();
    if ($website_menu->value) {
        $menu = $tblMenuModel->getMenu($website_menu->value);
    } else {
        $menu = array();
    }
    $view->with('menu', $menu);
});
View::composer('frontend.footer', function($view) {
    if (\Cookie::has('cookie_lang')) {
        $lang_code = \Cookie::get('cookie_lang');
    } else {
        $lang_code = \Config::get('all.all_config')->website_lang;
    }
    $tblMenuModel = new \FontEnd\tblMenuModel();
	$website_menu = \tblSettingModel::where('key','website_menu1_'.$lang_code)->first();
    if ($website_menu->value) {
        $footer = $tblMenuModel->getMenu($website_menu->value);
    } else {
        $footer = array();
    }
	$website_menu2 = \tblSettingModel::where('key','website_menu2_'.$lang_code)->first();
    if ($website_menu2->value) {
        $footer1 = $tblMenuModel->getMenu($website_menu2->value);
    } else {
        $footer1 = array();
    }
    $view->with('footer', $footer)->with('footer1', $footer1);
});


View::composer('frontend.news_header', function($view) {
    $setting = \FontEnd\tblSettingModel::getAll();
    if (\Cookie::has('cookie_lang')) {
        $lang_code = \Cookie::get('cookie_lang');
    } else {
        $lang_code = \Config::get('all.all_config')->website_lang;
    }
    $list_id_autokeyword = 'list_id_autokeyword_'.$lang_code;
    if (isset($setting->$list_id_autokeyword) && $setting->$list_id_autokeyword!='') {
        $data = \tblNewsModel::leftJoin('tbl_news_detail_lang','tbl_news.id','=','tbl_news_detail_lang.news_id')
                    ->whereIn('tbl_news_detail_lang.news_id',explode(',',$setting->$list_id_autokeyword))
                    ->where('tbl_news_detail_lang.lang_id',$lang_code)
                    ->where('tbl_news_detail_lang.status',1)
                    ->get();
    } else {
        $data = [];
    }
    $view->with('data', $data);
});
Route::controller('comment', '\FontEnd\CommentController');
Route::controller('home', '\FontEnd\HomeController');
Route::get('rezise', array('as' => 'rezise_image', 'uses' => 'ImageController@getResize'));
Route::controller('rezise', 'ImageController');

View::share('data_setting', \FontEnd\tblSettingModel::getSetting());
View::share('setting', \FontEnd\tblSettingModel::getAll());
$consfig_website = Config::get('all.all_config');
if (\Cookie::has('cookie_lang')) {
    $lang_config = Cookie::get('cookie_lang');
} else {
    $lang_config = Config::get('all.all_config')->website_lang;
}
$arrayLang = tblLangModel::orderBy('name')->rememberForever()->get();
View::share('lang_config', $lang_config);
View::share('array_lang',$arrayLang);
View::share('pub_config', $consfig_website);
App::missing(function($exception) {
    return \View::make('frontend.404');
});

// ADMINISTRATOR
Route::group(array('prefix' => 'pub-admin', 'before' => array('csrf')), function() {
    Route::get('screen-lock', array('as' => 'lock_scrreen', 'uses' => '\ADMIN\HomeController@getLock'));
    Route::post('screen-lock', '\ADMIN\HomeController@postLock');
    Route::get('login', '\ADMIN\HomeController@getLogin');
    Route::post('login', '\ADMIN\HomeController@postLogin');
    Route::get('profile', array('as' => 'getprofile', 'uses' => '\ADMIN\HomeController@getProfile'));
    Route::post('profile', array('as' => 'postprofile', 'uses' => '\ADMIN\HomeController@postProfile'));
    Route::get('404', '\ADMIN\HomeController@get404Admin');
    Route::get('forgot-password', '\ADMIN\HomeController@getForGotPassword');
    Route::post('forgot-password', '\ADMIN\HomeController@postForGotPassword');
    Route::get('change-password/{token}', '\ADMIN\HomeController@getChangePassword');
    Route::post('change-password', '\ADMIN\HomeController@postChangePassword');

    Route::group(['before' => array('check_admin_login')], function() {
        $listLang = Config::get('all.all_lang');
        View::share('l_lang', $listLang);
        
        View::share('g_config_all', Config::get('all.all_config'));
        View::share('g_user_all', Config::get('all.all_user'));
        View::composer(array('admin.template_admin.header'), function($view) {
			/* lay order cod */
			$order_count = \tblOrderModel::where('payment_type',1)->where('order_type',1)->where('status',0)->get();
            $comment_count = \tblCommentModel::where('status', 0)->count();
			$sms = \tblCustomerModel::orderBy('id','asc')->first();
			if(count($sms)>0){
				$total = $sms->total;
			}else{
				$total=0;
			}
            $view->with('comment_count', $comment_count)->with('order_count', $order_count)->with('total', $total);
        });
        Route::get('', '\ADMIN\NewsController@getView');
        Route::get('logout', array('as' => 'admin_logout', 'uses' => '\ADMIN\HomeController@getLogout'));
		Route::get('/order/filter/{param?}/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}', array('as' => 'filter', 'uses' => '\ADMIN\OrderController@getFilter'));
		Route::post('/order/filter/{param?}/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}', array('as' => 'postfiler', 'uses' => '\ADMIN\OrderController@postFilter'));
        Route::controller('feedback', '\ADMIN\FeedBackController');
		Route::controller('tweet', '\ADMIN\TweetController');
        Route::controller('home', '\ADMIN\HomeController');
        Route::controller('lang', '\ADMIN\LangController');
        Route::controller('menu', '\ADMIN\MenuController');
        Route::controller('news', '\ADMIN\NewsController');
        Route::controller('setting', '\ADMIN\SettingController');
        Route::controller('slider', '\ADMIN\SliderController');
        Route::controller('user', '\ADMIN\UserController');
        Route::controller('quick', '\ADMIN\QuickController');
        Route::controller('translate', '\ADMIN\TranslateGoogleController');
        Route::controller('page', '\ADMIN\PageController');
        Route::controller('comment', '\ADMIN\CommentController');
		Route::controller('order', '\ADMIN\OrderController');
		Route::controller('video', '\ADMIN\VideoController');
		Route::controller('gallery', '\ADMIN\GalleryController');
		Route::controller('manuf', '\ADMIN\ManufController');
		Route::controller('sheet', '\ADMIN\SheetController');
        Route::controller('show', '\ADMIN\ShowController');
        Route::controller('author', '\ADMIN\AuthorController');
		Route::controller('history', '\ADMIN\HistoryController');
		Route::controller('artist', '\ADMIN\ArtistController');
		Route::controller('place', '\ADMIN\PlaceController');
		Route::controller('ticket', '\ADMIN\TicketController');
		Route::controller('subcribe', '\ADMIN\SubcribeController');
        Route::controller('bookshow', '\ADMIN\BookShowController');
		Route::controller('sponsor', '\ADMIN\SponsorController');
    });
});
