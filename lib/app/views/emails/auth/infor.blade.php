<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Thông tin đặt vé</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top" bgcolor="#7c210e" style="background-color:#f3f3f3;"><br>
    <br>
    <table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="left" valign="top"><img src="{{Asset('asset/frontend')}}/images/top.png" width="600" height="191" style="display:block;"></td>
      </tr>
      <tr>
        <td align="center" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:0px 15px 0px 15px;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td align="left" valign="top" style="font-size:13px; font-family:Arial, Helvetica, sans-serif; color:#000000;">
             	<div style="font-family:Arial, Helvetica, sans-serif; font-size:28px; color:#555555;text-transform: uppercase;font-weight: bold;text-align: center ">Cám ơn
	              </div>
	              <small style="text-align:center;width: 100%;display: block;">Thông tin đơn vé của bạn</small>
	               <style type="text/css">
		             .table-ticket{
		              margin:0 auto;
		             }
		              .table-ticket td{
		                padding: 9px 20px;
		    			border: 1px solid #ebebeb;
		              }
		              .table-ticket tr td:first-child{
		              color: #666;
		              }
		              .table-ticket tr td:last-child{
		                
		                font-weight: bold;
		    			color: #343434;
		              }
		              .table-ticket tr td span{
		                color: #b11500;
		              }
		             </style>
			
              	{{$noidung}}
               
                <div>
                	<br/>
                	<br/>
                	<div style="text-align: center;color: #4b4b4b;">
                		{{\tblSettingModel::getTitleLang('lang_website_title')}} <br/>
                		{{\tblSettingModel::getTitleLang('lang_addr')}} <br/>
                	Phone: @if(isset($setting->contact_phone)){{$setting->contact_phone}}@endif <br/>
                	Fax: @if(isset($setting->contact_fax)){{$setting->contact_fax}}@endif <br/>
                	Email: @if(isset($setting->contact_email)){{$setting->contact_email}}@endif
                	</div><br/>
                </div>
              </td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td align="left" valign="top"><img src="{{Asset('asset/frontend')}}/images/bot.png" width="600" height="191" style="display:block;"></td>
      </tr>
  </table>
    <br>
    <br></td>
  </tr>
</table>
</body>
</html>
