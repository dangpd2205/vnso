<header id="top" class="top-header">
  <!-- Navigation -->
  <nav class="navbar navbar-default" data-spy="affix" data-offset-top="400">
      <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              </button>
              <a href="{{URL::route('chuongtrinh')}}" class="ticket-mobile"><img src="{{Asset('asset/frontend')}}/img/bg-ticket.svg" alt="img"></a>
              <a class="navbar-brand" href="{{Asset('')}}"><img src="{{Asset($setting->website_logo_default)}}" alt="Site Logo"></a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="navbar-collapse">
              <div class="nav navbar-nav navbar-right search-nav">
                  <a id="search-toggle" class="search-toggle" href="#"><i class="fa fa-search"></i></a>
                  <div id="header-search-wrap" class="header-search-wrap">
                      <form class="header-search" action="{{URL::route('searchshow')}}" method="post">
                        {{Form::token()}}
                          <input type="search" name="keyword" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_keyword')}}"/>
                          <button type="submit" name="searchsubmit"><i class="fa fa-search"></i></button>
                      </form>
                  </div>
              </div>
              <ul class="nav navbar-nav navbar-right">
                  @if(isset($menu) && count($menu)>0)
                  <?php $i=0; ?>
                  @foreach($menu as $i_menu)
                  @if($i_menu->parent_id==0)
                  <?php
                  $check_one=false;
                  foreach($menu as $check){
                      if($check->parent_id==$i_menu->id){
                          $check_one=true;
                      }
                  }
                  ?>

                  <li @if($check_one==true) class="dropdown" @endif>
                     <a href="{{$i_menu->url}}">{{$i_menu->name}}</a>
                     @if($check_one==true)
                     <ul class="dropdown-menu">  
                          @foreach($menu as $i_menu1)
                          @if($i_menu1->parent_id == $i_menu->id)
                          <li><a href="{{$i_menu1->url}}">{{$i_menu1->name}}</a></li>
                          @endif
                          @endforeach          
                      </ul>                      
                      @endif
                  </li>    
                  @endif
                   @endforeach
                  @endif       
                  <li class="ticket-menu"><a href="{{URL::route('chuongtrinh')}}"><img src="{{Asset('asset/frontend')}}/img/bg-ticket.svg" alt="img"></a></li>
				@if(isset($array_lang))				  @foreach($array_lang as $item)				  @if(isset($lang_config) && $lang_config != $item->id)				  @if($item->code=='vi')				  <li><a onclick="change_lang('{{URL::action('\FontEnd\HomeController@getCookie')}}/{{$item->id}}')">VN</a></li>				  @elseif($item->code=='en')				  <li><a onclick="change_lang('{{URL::action('\FontEnd\HomeController@getCookie')}}/{{$item->id}}')">EN</a></li>				 				  @endif				  @endif				  @endforeach				  @endif
              </ul>
          </div>
          <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
  </nav>
  <!-- Navigation End -->
  <!-- Banner Slider -->
  
  <!-- Banner Slider End -->
</header>
