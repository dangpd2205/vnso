@extends("frontend.hometemplate")

@section("title")

{{\tblSettingModel::getTitleLang('lang_website_title')}}

@endsection

@section("desc")

{{\tblSettingModel::getTitleLang('lang_website_description')}}

@endsection

@section("keyword")

{{\tblSettingModel::getTitleLang('lang_website_keyword')}}

@endsection



@section("css")

<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">

<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">

<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">

<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">

<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">

<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">

<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">

<link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
<link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/icofont.min.css">

<link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/modal-video.min.css">
<!-- Custom CSS -->

<link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">

<link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">

<link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">

<!-- Google Fonts -->

<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">

<style>
    

    .tab-gallery .box {
        transition: 0.2s;
        box-shadow: 0 8px 22px rgba(0, 0, 0, 0.2);
            margin-bottom: 20px;
        text-align: center;
    }

    .tab-gallery .box-icon {
        font-size: 78px;
        max-height: 217px;
        overflow: hidden;
    }

    .tab-gallery .box-text {
        font-size: 19px;
        line-height: 1em;
        font-weight: 600;
        margin-top: 10px;
        color: #444;
    }
    .tab-gallery .box{
        overflow: hidden;
    }
    .tab-gallery .box a img {
        -moz-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        -webkit-transition: all .3s ease-in-out;
        -ms-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
        -webkit-transform: scale(1.05);
        -moz-transform: scale(1.05);
        -ms-transform: scale(1.05);
        -o-transform: scale(1.05);
        transform: scale(1.05);
    }


    .tab-gallery .box a:hover img {
        -webkit-transform: scale(1);
        -moz-transform: scale(1);
        -ms-transform: scale(1);
        -o-transform: scale(1);
        transform: scale(1);
    }
    
    .video-btn-popup .wrapper {
        width: 100%;
        background: 0 0;
        background: -webkit-linear-gradient(top,transparent,#000);
        background: linear-gradient(to bottom,transparent,#000);
        position: absolute;
        left: 0;
        bottom: 0;
        line-height: 1;
        padding: 170px 20px 20px 20px;
    }
    .video-btn-popup i {
        font-size: 60px;
        z-index: 2;
    }
</style>





@endsection



@section("js")

<script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>

<script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>

<script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>

<script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>

<script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>

<script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>

<script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>

<script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>

<script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>

<script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>

<script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>



<script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>

<script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>

<script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>

<script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>

<script src="{{Asset('asset')}}/frontend/js/jquery-modal-video.min.js"></script>

<script src="{{Asset('asset')}}/frontend/js/custom.js"></script>

<script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>

<script type="text/javascript">

//  $('.parallax-window-1').parallax({imageSrc: "{{Asset($setting->website_image_adv7)}}"});

$('.parallax-window-2').parallax({imageSrc: "{{Asset($setting->website_image_adv1)}}"});

</script>

<script>
		$(".video-btn-popup").modalVideo({
			youtube:{
				controls:0,
				nocookie: true
			}
		});
	</script>

<script type="text/javascript">

    $(document).ready(function () {



        $('.calendar').pignoseCalendar({

            scheduleOptions: {

                colors: {

                    offer: '#931a45',

                    ad: '#5c6270'

                }

            },

            schedules: [

<?php

if (isset($calendar_show) && count($calendar_show) > 0) {

    foreach ($calendar_show as $item) {

        ?>

                        {

                            name: 'offer',

                            hour: "{{{date('H:i', $item->time_show)}}}",

                            text: "<a href='{{URL::route('route_data',$item->slug.'ds')}}'>{{{$item->name}}}</a>",

                            date: '<?php echo date('Y-m-d', $item->time_show); ?>'



                        },

        <?php

    }

}

?>

            ],

            select: function (date, context) {

                var message = `<h3>${(date[0] === null ? '' : date[0].format('dddd <br> MMMM DD'))}</h3>

        

<div class="schedules-date"></div>`;

                var $target = $('.text-day').html(message);

                for (var idx in context.storage.schedules) {

                    var schedule = context.storage.schedules[idx];

                    if (typeof schedule !== 'object') {

                        continue;

                    }

                    $target.find('.schedules-date').append('<p class="clock"> ' + schedule.hour + '</p>');

                    $target.find('.schedules-date').append('<p class="event">' + schedule.text + '</p>');

                }

            }

        });

    });



</script>

@endsection





@section("content")



<div class="fix-space"></div>

<div class="main-wrap">

    <!-- Upcoming Stage -->

    <!-- About Singer -->
	@if(isset($slide))
	{{$slide}}
	@endif
    

    <!-- About Singer End -->

    <div class="section date-time ">

        <div class="container">

            <div class="row">
                <div class="col-md-12">
                      <div class="wholes-season">
                         <a href="{{\tblSettingModel::getTitleLang('lang_text_index2')}}"><h3 class="stage-title">{{\tblSettingModel::getTitleLang('lang_text_index')}}</h3></a> | <a href="{{\tblSettingModel::getTitleLang('lang_text_index2')}}" class="read_more wholes-season-btn" >{{\tblSettingModel::getTitleLang('lang_text_index1')}}</a>
                      </div>
                </div>
                <div class="col-md-6">

                    <!-- Upcoming Album -->

                    <div class="section  upcoming-stage-section text-white" style="background-image:url(@if(isset($setting->website_image_adv17)){{$setting->website_image_adv17}}@endif)">
                        <div class="upcoming-stage-section-child">

                        <h3 class="section-title">{{\tblSettingModel::getTitleLang('lang_upcoming_home')}}</h3>

                        @if(isset($list_show) && count($list_show)>0)

                        @if(isset($list_show[0]))

                        <a href="{{URL::route('route_data',$list_show[0]->slug.'ds')}}"><h3 class="stage-title">{{$list_show[0]->name}}</h3></a>

                        <p class="text-upper">{{date('d/m/Y',$list_show[0]->time_show)}}</p>

                        <p class="text-upper">
						@if($list_show[0]->place_text=='')
									{{$list_show[0]->place_name}}
								@else
									{{$list_show[0]->place_text}}
								@endif
						</p>

                        <a href="{{URL::route('route_data',$list_show[0]->slug.'ds')}}" class="read_more" >{{\tblSettingModel::getTitleLang('lang_readmore')}}</a>

                        @endif

                        @endif
                        </div>




                    </div>

                    <div class="section upcoming-album-section">

                        <div class="fluid-container">

                            <div class="upcoming-album  bl-bg">



                                @if(isset($list_show) && count($list_show)>0)

                                @if(isset($list_show[1]))

                                <a href="{{URL::route('route_data',$list_show[1]->slug.'ds')}}"><h3 class="album-title">{{$list_show[1]->name}}</h3></a>

                                <p class="text-upper">{{date('d/m/Y',$list_show[1]->time_show)}}</p>

                                <p class="text-upper">{{date('H:i',$list_show[1]->time_show)}} | 
								@if($list_show[1]->place_text=='')
									{{$list_show[1]->place_name}}
								@else
									{{$list_show[1]->place_text}}
								@endif
								</p>

                                <a href="{{URL::route('route_data',$list_show[1]->slug.'ds')}}" class="read_more" >{{\tblSettingModel::getTitleLang('lang_readmore')}}</a>

                                @endif

                                @endif

                            </div>

                            <div class="upcoming-album  ye-bg">

                                @if(isset($list_show) && count($list_show)>0)

                                @if(isset($list_show[2]))

                                <a href="{{URL::route('route_data',$list_show[2]->slug.'ds')}}"><h3 class="album-title">{{$list_show[2]->name}}</h3></a>

                                <p class="text-upper">{{date('d/m/Y',$list_show[2]->time_show)}}</p>

                                <p class="text-upper">{{date('H:i',$list_show[2]->time_show)}} | 
								@if($list_show[2]->place_text=='')
									{{$list_show[2]->place_name}}
								@else
									{{$list_show[2]->place_text}}
								@endif
								</p> 

                                <a href="{{URL::route('route_data',$list_show[2]->slug.'ds')}}" class="read_more" >{{\tblSettingModel::getTitleLang('lang_readmore')}}</a>

                                @endif

                                @endif

                            </div>

                        </div>

                    </div>

                    <!-- Upcoming Album End -->



                </div>

                <div class="col-md-6">

                    <div class="calendar-index">

                        <div class="bg-calender">

                            <div class="calendar"></div>

                            <div class="text-day">

                                <h3>{{date('D',time())}}<br>

                                    {{date('M d',time())}}</h3>

                                <?php

                                if (isset($inday_show) && count($inday_show) > 0) {

                                    foreach ($inday_show as $i_inday_show) {

                                        ?>

                                        <p class="clock">{{date('H:i',$i_inday_show->time_show)}}</p>

                                        <p class="event">{{$i_inday_show->name}}</p>

                                        <?php

                                    }

                                }

                                ?>

                            </div>

                        </div>

                    </div>



                </div>

            </div>

        </div>

    </div>

    <!-- Upcoming Stage End -->







    <!-- Upcoming Music Show -->

    <div class="section section-padding">

        <div class="container">

            <div class="row">

                <!-- <div class="col-md-8 col-md-offset-2 col-xs-12">

                    <div class="section-header text-center">

                        <h3 class="section-title">Upcoming Show</h3>

                        

                    </div>

                </div> -->

            </div>

            <div class="row">

                <div class="col-md-12">

                    <ul class="nav nav-tabs">

                        @if(isset($cateshow) && count($cateshow)>0)

                        <li class="active"><a data-toggle="tab" href="#tabshow{{$cateshow->id}}">

                                <img src="{{$cateshow->avatar}}" alt="img">                        

                                {{$cateshow->name}}</a></li>

                        @endif

                        @if(isset($cateshow1) && count($cateshow1)>0)

                        <li><a data-toggle="tab" href="#tabshow{{$cateshow1->id}}">

                                <img src="{{$cateshow1->avatar}}" alt="img">                        

                                {{$cateshow1->name}}</a></li>

                        @endif

                        @if(isset($cateshow2) && count($cateshow2)>0)

                        <li><a data-toggle="tab" href="#tabshow{{$cateshow2->id}}">

                                <img src="{{$cateshow2->avatar}}" alt="img">                        

                                {{$cateshow2->name}}</a></li>

                        @endif

                        @if(isset($cateshow3) && count($cateshow3)>0)

                        <li><a data-toggle="tab" href="#tabshow{{$cateshow3->id}}">

                                <img src="{{$cateshow3->avatar}}" alt="img">                        

                                {{$cateshow3->name}}</a></li>    

                        @endif                  

                    </ul>

                </div>

                <div class="col-xs-12">

                    <div class="tab-content">

                        @if(isset($data_cateshow) && count($data_cateshow)>0)

                        <style>

                            #show-table-{{$data_cateshow[0]->id_cate}} tr:hover {

                                background-image: url('https://vnso.org.vn/asset/upload/vnso/show-table-bg.png');

                            }

                        </style>

                        <?php $i = 0; ?>

                        <div id="tabshow{{$data_cateshow[0]->id_cate}}" class="tab-pane fade in active">

                            <div class="table-responsive">

                                <table class="show-table" id="show-table-{{$data_cateshow[0]->id_cate}}">

                                    @foreach($data_cateshow as $i_data_cateshow)

                                    <tr>

                                        <td class="show-date color-1">

                                            <span class="date">{{date('d M',$i_data_cateshow->time_show)}}</span>

                                            <span class="day">{{date('H:i D',$i_data_cateshow->time_show)}}</span>

                                        </td>

                                        <td class="show-hall">

                                            <a href="{{URL::route('route_data',$i_data_cateshow->slug.'ds')}}"><span class="hall-name">{{str_limit($i_data_cateshow->name,45,'...')}}</span></a>

                                        </td>

                                        <td class="show-name">

                                            @if(isset($i_data_cateshow->artist_content) && $i_data_cateshow->artist_content!='')

                                            {{$i_data_cateshow->artist_content}}

                                            @else

                                            @if(isset($conductor[$i]) && count($conductor[$i])>0)

                                            {{\tblSettingModel::getTitleLang('lang_conductor')}} - {{$conductor[$i]->a_title}} <br/>

                                            @endif

                                            @if(isset($artist[$i]) && count($artist[$i])>0)

                                            @foreach($artist[$i] as $item)

                                            {{$item->a_title}} - {{$item->a_instrument}} <br/>

                                            @endforeach

                                            @endif

                                            @endif

                                        </td>

                                        <td class="show-ticket">

                                            @if($i_data_cateshow->is_sell==0)

                                            <a class="btn btn-border btn-black" href="{{URL::route('seat',$i_data_cateshow->show_id)}}">Mua vé</a>

                                            @endif

                                        </td>

                                    </tr>

                                    <?php $i++; ?>

                                    @endforeach

                                </table>

                            </div>

                        </div>

                        @endif

                        @if(isset($data_cateshow1) && count($data_cateshow1)>0)

                        <style>

                            #show-table-{{$data_cateshow1[0]->id_cate}} tr:hover {

                                background-image: url('{{$data_cateshow1[0]->background}}');

                            }

                        </style>

                        <?php $i1 = 0; ?>

                        <div id="tabshow{{$data_cateshow1[0]->id_cate}}" class="tab-pane fade">

                            <div class="table-responsive">

                                <table class="show-table" id="show-table-{{$data_cateshow1[0]->id_cate}}">

                                    @foreach($data_cateshow1 as $i_data_cateshow1)

                                    <tr>

                                        <td class="show-date color-1">

                                            <span class="date">{{date('d M',$i_data_cateshow1->time_show)}}</span>

                                            <span class="day">{{date('H:i D',$i_data_cateshow1->time_show)}}</span>

                                        </td>

                                        <td class="show-hall">

                                            <a href="{{URL::route('route_data',$i_data_cateshow1->slug.'ds')}}"><span class="hall-name">{{str_limit($i_data_cateshow1->name,45,'...')}}</span></a>

                                        </td>

                                        <td class="show-name">

                                            @if(isset($i_data_cateshow1->artist_content) && $i_data_cateshow1->artist_content!='')

                                            {{$i_data_cateshow1->artist_content}}

                                            @else

                                            @if(isset($conductor[$i1]) && count($conductor[$i1])>0)

                                            {{\tblSettingModel::getTitleLang('lang_conductor')}} - {{$conductor[$i1]->a_title}} <br/>

                                            @endif

                                            @if(isset($artist[$i1]) && count($artist[$i1])>0)

                                            @foreach($artist[$i1] as $item)

                                            {{$item->a_title}} - {{$item->a_instrument}} <br/>

                                            @endforeach

                                            @endif

                                            @endif

                                        </td>

                                        <td class="show-ticket">

                                            @if($i_data_cateshow1->is_sell==0)

                                            <a class="btn btn-border btn-black" href="{{URL::route('seat',$i_data_cateshow1->show_id)}}">Mua vé</a>

                                            @endif

                                        </td>

                                    </tr>

                                    <?php $i1++; ?>

                                    @endforeach

                                </table>

                            </div>

                        </div>

                        @endif
						@if(isset($cateshow2) && count($cateshow2)>0)
                        

                        <?php $i2 = 0; ?>

                        <div id="tabshow{{$cateshow2->id}}" class="tab-pane fade">
							@if(isset($data_cateshow2) && count($data_cateshow2)>0)

							<style>

								#show-table-{{$data_cateshow2[0]->id_cate}} tr:hover {

									background-image: url('{{$data_cateshow2[0]->background}}');

								}

							</style>
                            <div class="table-responsive">

                                <table class="show-table" id="show-table-{{$data_cateshow2[0]->id_cate}}">

                                    @foreach($data_cateshow2 as $i_data_cateshow2)

                                    <tr>

                                        <td class="show-date color-1">

                                            <span class="date">{{date('d M',$i_data_cateshow2->time_show)}}</span>

                                            <span class="day">{{date('H:i D',$i_data_cateshow2->time_show)}}</span>

                                        </td>

                                        <td class="show-hall">

                                            <a href="{{URL::route('route_data',$i_data_cateshow2->slug.'ds')}}"><span class="hall-name">{{str_limit($i_data_cateshow2->name,45,'...')}}</span></a>

                                        </td>

                                        <td class="show-name">

                                            @if(isset($i_data_cateshow2->artist_content) && $i_data_cateshow2->artist_content!='')

                                            {{$i_data_cateshow2->artist_content}}

                                            @else

                                            @if(isset($conductor[$i2]) && count($conductor[$i2])>0)

                                            {{\tblSettingModel::getTitleLang('lang_conductor')}} - {{$conductor[$i2]->a_title}} <br/>

                                            @endif

                                            @if(isset($artist[$i2]) && count($artist[$i2])>0)

                                            @foreach($artist[$i2] as $item)

                                            {{$item->a_title}} - {{$item->a_instrument}} <br/>

                                            @endforeach

                                            @endif

                                            @endif

                                        </td>

                                        <td class="show-ticket">

                                            @if($i_data_cateshow2->is_sell==0)

                                            <a class="btn btn-border btn-black" href="{{URL::route('seat',$i_data_cateshow2->show_id)}}">Mua vé</a>

                                            @endif

                                        </td>

                                    </tr>

                                    <?php $i2++; ?>

                                    @endforeach

                                </table>

                            </div>
							@endif
                        </div>

                        @endif
						@if(isset($cateshow3) && count($cateshow3)>0)
                        

                        <?php $i3 = 0; ?>

                        <div id="tabshow{{$cateshow3->id}}" class="tab-pane fade">
							@if(isset($data_cateshow3) && count($data_cateshow3)>0)

							<style>

								#show-table-{{$data_cateshow3[0]->id_cate}} tr:hover {

									background-image: url('{{$data_cateshow3[0]->background}}');

								}

							</style>
                            <div class="table-responsive">

                                <table class="show-table" id="show-table-{{$data_cateshow3[0]->id_cate}}">

                                    @foreach($data_cateshow3 as $i_data_cateshow3)

                                    <tr>

                                        <td class="show-date color-1">

                                            <span class="date">{{date('d M',$i_data_cateshow3->time_show)}}</span>

                                            <span class="day">{{date('H:i D',$i_data_cateshow3->time_show)}}</span>

                                        </td>

                                        <td class="show-hall">

                                            <a href="{{URL::route('route_data',$i_data_cateshow3->slug.'ds')}}"><span class="hall-name">{{str_limit($i_data_cateshow3->name,45,'...')}}</span></a>

                                        </td>

                                        <td class="show-name">

                                            @if(isset($i_data_cateshow3->artist_content) && $i_data_cateshow3->artist_content!='')

                                            {{$i_data_cateshow3->artist_content}}

                                            @else

                                            @if(isset($conductor[$i3]) && count($conductor[$i3])>0)

                                            {{\tblSettingModel::getTitleLang('lang_conductor')}} - {{$conductor[$i3]->a_title}} <br/>

                                            @endif

                                            @if(isset($artist[$i3]) && count($artist[$i3])>0)

                                            @foreach($artist[$i3] as $item)

                                            {{$item->a_title}} - {{$item->a_instrument}} <br/>

                                            @endforeach

                                            @endif

                                            @endif

                                        </td>

                                        <td class="show-ticket">

                                            @if($i_data_cateshow3->is_sell==0)

                                            <a class="btn btn-border btn-black" href="{{URL::route('seat',$i_data_cateshow3->show_id)}}">Mua vé</a>

                                            @endif

                                        </td>

                                    </tr>

                                    <?php $i3++; ?>

                                    @endforeach

                                </table>

                            </div>
							 @endif
                        </div>

                        @endif

                    </div>



                </div>

                <div class="col-xs-12 text-center">

                    <a class="btn btn-black btn-lg" href="{{URL::route('chuongtrinh')}}">{{\tblSettingModel::getTitleLang('lang_viewall')}}</a>

                </div>

            </div>

        </div>

    </div>

    <!-- Upcoming Music Show End -->



	<div class="section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    @if(isset($video) && count($video)>0)
					

                    <div id="recent-album-carousel" class="recent-album-carousel owl-carousel">

                        @foreach($video as $item)
						<?php 
						$url = '';
						$exp=explode('watch?v=',$item->url);
						if(isset($exp[1])){
							$url=$exp[1];
						}
						?>
                        <a href="#" class="video-btn-popup" data-video-id="{{$url}}">
                        	<img src="{{action('ImageController@getResize')}}?src={{Asset($item->avatar)}}&w=930&h=527" alt="{{$item->name}}" style="width: 100%; height: fit-content;" class="img-video">
                        	<i class="icofont-play"></i>
                            <div class="wrapper">
                                <h4>{{$item->name}}</h4>
                            </div>
                        </a>

                        @endforeach

                    </div>

                    @endif
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="tab-gallery">
						@if(isset($gallery) && count($gallery)>0)
						@foreach($gallery as $item)
                        <div class="box">
                            <a href="{{route('gallery')}}" title="{{$item->name}}">
                                <div class="box-icon">
                                    <img src="{{action('ImageController@getResize')}}?src={{Asset($item->avatar)}}&w=450&h=217" alt="{{$item->name}}">
                                </div>
                                <div class="box-text" >
                                    {{str_limit($item->name,45,'...')}}
                                </div>
                            </a>
                        </div>
						@endforeach
						@endif
                    </div>
                </div>
            </div>
        </div>
		
	</div>
    





    <!-- Latest Album and single -->

   
    <!-- Latest Album and single end -->

    <!-- Music Shop -->

    <div class="section section-padding">

        <div class="container">

            <div class="row">

                <div class="col-md-8 col-md-offset-2 col-xs-12">

                    <div class="section-header text-center">

                        <h3 class="section-title">{{\tblSettingModel::getTitleLang('lang_sponsor')}} </h3>



                    </div>

                </div>

            </div>

            <div class="row">

                <div class="products">

                    @if(isset($sponsor) && count($sponsor)>0)

                    @foreach($sponsor as $i_sponsor)

                    <div class="col-md-2 col-xs-4">

                        <div class="product">

                            <div class="product-thumb">
								<a href="<?php if($i_sponsor->url!=''){ echo $i_sponsor->url; }else{echo \URL::route('getmanuf',array($i_sponsor->id));} ?>">
                                <img class="img-responsive" src="{{action('ImageController@getResize')}}?src={{Asset($i_sponsor->avatar)}}&w=360&h=360" alt="{{$i_sponsor->name}}">
								</a>


                            </div>



                        </div>

                    </div>

                    @endforeach

                    @endif



                </div>

            </div>

        </div>

    </div>

    <!-- Music Shop End -->



</div>

@endsection