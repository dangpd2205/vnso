@extends("frontend.hometemplate")
@section("title")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_title')}}
@endsection
@section("desc")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_description')}}
@endsection
@section("keyword")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}
@endsection

@section("css")
   <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">
@endsection

@section("js")
    <script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
        
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
        <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>		<script>		function removethis(t){				$(t).parent().remove();			}		</script>
@endsection

@section("content")
<div class="fix-space"></div>
<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
            <li class="active">{{\FontEnd\tblSettingModel::getTitleLang('lang_artist')}}</li>
        </ol>
    </div>
</div>
<!-- Page Header End -->
<div class="main-wrap">
    <div class="section section-padding member-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="banner-member">
                        <img src="{{Asset($setting->website_image_adv3)}}" alt="img">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box-name" id="box-name">
                        <ul>
                            @if(isset($cate) && count($cate)>0)
                            @foreach($cate as $i_cate)
                            <li ><a href="#cate{{$i_cate->id}}"><i class="fa fa-angle-down" aria-hidden="true"></i>  {{$i_cate->name}}</a></li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(isset($cate) && count($cate)>0)
                    <?php $i=0; ?>
                    @foreach($cate as $i_cate1)
                    <div class="list-member" id="cate{{$i_cate1->id}}">
                        <h3 class="head-member">{{$i_cate1->name}}</h3>
                        <div class="row-member">
                            @if(isset($artist[$i]) && count($artist[$i])>0)
                            @foreach($artist[$i] as $item)
                            <div class="artist" >
                                <a class="artist-avatar modal_artist" data-url="{{URL::route('postartist')}}" data-id="{{$item->artist_id}}">
                                    <img onerror="removethis(this)" src="{{action('ImageController@getResize')}}?src={{Asset($item->avatar)}}&w=345&h=393" alt="img">
                                </a>
                                <div class="artist-des">
                                <h4 class="artist-name"><a class="modal_artist" data-url="{{URL::route('postartist')}}" data-id="{{$item->artist_id}}">{{$item->a_title}}</a></h4>
                                <p class="artist-role">{{$item->a_instrument}}</p>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <?php $i++; ?>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade member-detail" id="myModal" role="dialog">
    <div class="container  ">
        
        <!-- Modal content-->
        <div class="modal-content">
            
            <div class="modal-body">
                <div class="row content_modal">
                    
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection