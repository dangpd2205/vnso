@extends("frontend.hometemplate")
<?php
if (isset($data)) {
    if($data->page_excerpt!=''){
        $des_character = strip_tags($data->page_excerpt);
    }else {
        $des_character = "";
    }
    if($data->page_name!=''){
        $title_character = strip_tags($data->page_name);
    }else {
        $title_character = "";
    }
} else {
    $des_character = "";
    $title_character = "";
}

if (strlen($des_character) > 160) {
    $des_page = substr($des_character, 0, 155);
} else {
    $des_page = $des_character;
}

if (strlen($title_character) > 70) {
    $tit_page = substr($title_character, 0, 70);
} else {
    $tit_page = $title_character;
}
?>
@section("title")
<?php echo $tit_page; ?>
@endsection
@section("desc")
<?php echo $des_page; ?>
@endsection
@section("keyword")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}
@endsection
@section("facebooktag")

<meta property="og:url" content="{{Request::url()}}" />

<meta property="og:type" content="article" />

<meta property="og:title" content="<?php if($data->seofbtitle!=''){echo $data->seofbtitle;}else{echo $data->page_name;} ?>" />

<meta property="og:description" content="<?php if($data->seofbdesc!=''){echo $data->seofbdesc;}else{echo $data->page_excerpt;} ?>" />

<meta property="og:image" content="{{Asset($setting->website_logo_default)}}" />

@endsection
@section("css")
   <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">
		<style>
		.p_content{
			display:none;
		}
		</style>
@endsection

@section("js")
    <script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
        
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
        <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>
		<script>
			var url_loadmore = "{{URL::route('posttimeline')}}";
			function readmore(t){				
				$(t).parent().find(".p_content").show();
			}
			
			$(document).ready(function(){
				
				$(".fa-plus").click(function(){
					$.ajax({
						url: url_loadmore+"?skip="+$("input[name='skipvalue']").val()*5,
						type: "POST",
						dataType: 'html',
						success: function(data) {
							$(".content_timeline").append(data);
						}
					});
					$("input[name='skipvalue']").val(parseInt($("input[name='skipvalue']").val())+1);
				});
			});
			
		</script>
@endsection
@section("content")
<div class="fix-space"></div>
<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <ol class="breadcrumb">
             <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
            <li class="active">{{$data->page_name}}</li>
            
        </ol>
    </div>
</div>
<!-- Page Header End -->
<div class="main-wrap">
    <div class="section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="blog-single">
                        <article class="post">
                            <a href="#"><h2 class="title-head">{{$data->page_name}} </h2></a>
                            <div class="post-content">
                                <div class="post-metas">
                                    <p class="post-date post-meta">{{$data->created_at}}</p>
                                </div>
                                <div class="post-entry">
                                  {{$data->page_content}}
								  </div>
                            </div>
                        </div>
                        <div class="share-post">
                            <label>{{\FontEnd\tblSettingModel::getTitleLang('lang_share')}}</label>
                            <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js"></script>
                            <div class="d_inline_middle m_left_5 addthis_widget_container">
                            <!-- AddThis Button BEGIN -->
                            <div class="addthis_toolbox addthis_default_style addthis_16x16_style">
                                <a class="addthis_button_preferred_1"></a>
                                <a class="addthis_button_preferred_2"></a>
                                <a class="addthis_button_preferred_3"></a>
                                <a class="addthis_button_preferred_4"></a>
                                <a class="addthis_button_compact"></a>
                                <a class="addthis_counter addthis_bubble_style"></a>
                            </div>
                            </div>
                            
                        </div>
                    </article>
                    <!-- Article Content End here -->
                    
                </div>
            </div>
        </div>
    </div>
    @if(isset($timeline) && count($timeline)>0)
	<section id="cd-timeline" class="cd-container section-padding section timeline-about">
		<div class="content_timeline">
			@if(isset($timeline) && count($timeline)>0)
			<?php $i=0; ?>
			@foreach($timeline as $item)
			<div class="cd-timeline-block">
				<div class="cd-timeline-img cd-picture">
					<i class="fa fa-chevron-down" aria-hidden="true"></i>
				</div> <!-- cd-timeline-img -->

				<div class="cd-timeline-content">
					<h2>{{$item->name}}</h2>
					<p class="p_desc">{{$item->description}}</p>
					<p class="p_content">{{$item->content}}</p>
					<a href="javascript:void(0)" onclick="readmore(this)" class="cd-read-more">{{\FontEnd\tblSettingModel::getTitleLang('lang_readmore')}}</a>
					<span class="cd-date">{{date('M d',$item->timeline)}}</span>
				</div> <!-- cd-timeline-content -->
			</div> <!-- cd-timeline-block -->
			<?php $i++; ?>
			@endforeach
			@endif
		</div>
        <div class="cd-timeline-block">
            <div class="cd-timeline-img cd-picture">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </div> <!-- cd-timeline-img -->
           
        </div> <!-- cd-timeline-block -->
    </section> <!-- cd-timeline -->
    @endif
	<input type="hidden"name="skipvalue" value="1"/>
</div>

@endsection

