@extends("frontend.hometemplate")


<?php
if (isset($data)) {
	if($data->description!=''){
		$des_character = strip_tags($data->description);
	}else {
		$des_character = "";
	}
	if($data->name!=''){
		$title_character = strip_tags($data->name);
	}else {
		$title_character = "";
	}
} else {
    $des_character = "";
    $title_character = "";
}
$title_character.='-'.\FontEnd\tblSettingModel::getTitleLang('lang_website_title');
if (strlen($des_character) > 160) {
    $des_page = substr($des_character, 0, 155);
} else {
    $des_page = $des_character;
}

if (strlen($title_character) > 70) {
    $tit_page = substr($title_character, 0, 70);
} else {
    $tit_page = $title_character;
}
?>
@section("title")
<?php echo $tit_page; ?>
@endsection
@section("desc")
<?php echo $des_page; ?>
@endsection

@section("keyword")

{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}

@endsection

@section("facebooktag")

<meta property="og:url" content="{{Request::url()}}" />

<meta property="og:type" content="article" />

<meta property="og:title" content="<?php echo $data->name; ?>" />

<meta property="og:description" content="<?php echo $data->description; ?>" />

<meta property="og:image" content="{{Asset($data->avatar)}}" />

@endsection
@section("css")
   <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">
@endsection

@section("js")
    <script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
        
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
        <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>
@endsection
@section("content")
<div class="fix-space"></div>
<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
            <li><a href="#">{{\FontEnd\tblSettingModel::getTitleLang('lang_show')}}</a></li>
            <li class="active">{{$data->name}}</li>
        </ol>
    </div>
</div>
<div class="main-wrap">
    <!-- Single concert section -->
    <div class="section section-padding single-concert-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <article class="concert-single">
                        <div class="concert-thumb">
                            <img class="img-responsive" src="{{action('ImageController@getResize')}}?src={{Asset($data->avatar)}}&w=750&h=390" alt="{{$data->name}}">
                        </div>
                        <h3 class="concert-title">{{$data->name}}</h3>
                        <p class="concert-metas">
                            <span class="concert-meta concert-date"><label>{{\FontEnd\tblSettingModel::getTitleLang('lang_time')}}:</label> {{date('d M Y',$data->time_show)}}</span>
                            <span class="concert-meta concert-venue"><label>{{\FontEnd\tblSettingModel::getTitleLang('lang_location')}}:</label> @if(isset($place) && count($place)>0){{$place->name}} @endif</span>
                        </p>
                        <div class="concert-entry">
                           {{$data->content}}
                        </div>
                    </article>
                     <div class="comments-section">
                        <p class="lead">{{\FontEnd\tblSettingModel::getTitleLang('lang_comment')}}</p>
                        
                        <?php                        
                        if(isset($comment) && count($comment)>0){
                            \Tree::showComment($comment);
                        }
                        ?>   
                     
                        
                    </div>
                    <div class="give-your-reply">
                        <div class="commentform-header">
                           
                        </div>
                        <form id="comment-form" class="comment-form" action="{{URL::route('postcomment')}}" method="post">
                            {{Form::token()}}
                            <div class="row">
                                <div class="col-md-6">
                                    <label>{{\FontEnd\tblSettingModel::getTitleLang('lang_name')}} <br/>
                                        <input type="text" name="name" id="name" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_name')}}" required>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>Email <br/>
                                        <input type="text" name="email" id="email" placeholder="Email" required>
                                    </label>
                                </div>
                            </div>
                            <div class="row" style="display:none" id="li_hidden">
                                <div class="col-md-6">
                                    <label>{{\FontEnd\tblSettingModel::getTitleLang('lang_reply')}} <a onclick="hide_div()" style="padding:5px">X</a><br/> 
                                        <input readonly="true" type="text" id="reply_name" name="reply_name" />
                                    </label>
                                </div>
                            </div>
                            <label>{{\FontEnd\tblSettingModel::getTitleLang('lang_content')}}<br/>
                                <textarea name="message" id="comment" rows="5" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_content')}}"></textarea>
                            </label>
                            <input type="hidden" name="parent" id="parent"/>
                            <input type="hidden" name="post_type" id='post_type' value="5"/>
                            <input type="hidden" name="post_id" value="{{$data->id}}"/>
                            <div id="messages_notify">

                        </div>
                            <button type="button" onclick="submitForm('comment-form')" class="btn btn-black">{{\FontEnd\tblSettingModel::getTitleLang('lang_send')}}</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="sidebar concert-sidebar">
                        <!-- Ticket widget -->
                        <aside class="widget ticket-widget">
                            <h4 class="widget-title">Ticket Pricing</h4>
                            <div class="ticket-pricing">
                                <div class="ticket-pricing-item seat-1">
                                    <p class="ticket-pack-name"> S Seat</p>
                                    <p class="ticket-price">500.000 VNĐ</p>
                                    
                                </div>
                                <div class="ticket-pricing-item seat-2">
                                    <p class="ticket-pack-name">A Seat</p>
                                    <p class="ticket-price">350.000 VNĐ</p>
                                    
                                </div>
                                <div class="ticket-pricing-item seat-3">
                                    <p class="ticket-pack-name">B Seat</p>
                                    <p class="ticket-price">200.000 VNĐ</p>
                                    
                                </div>
                                <div class="ticket-pricing-item seat-1">
                                    <p class="ticket-pack-name">VIP Seat</p>
                                    <p class="ticket-price">500.000 VNĐ</p>
                                    
                                </div>
                                <div class="book-ticket text-center">
                                    <a class="ticket-btn btn-black" href="#">Buy Now</a>
                                </div>
                            </div>
                        </aside>
                        <!-- Upcoming music show -->
                        <aside class="widget upcoming-show-widget">
                            <h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_upcoming_show')}}</h4>
                            <div class="upcoming-shows">
                                @if(isset($upcoming) && count($upcoming)>0)
                                @foreach($upcoming as $i_upcoming)
                                <div class="upcoming-show-item">
                                    <div class="show-content">
                                        <h5 class="show-title">{{date('d M Y',$i_upcoming->time_show)}}</h5>
                                        
                                        <p class="show-type"><a href="{{URL::route('route_data',$i_upcoming->slug.'ds')}}">{{$i_upcoming->name}}</a></p>
                                        <p class="show-venue"><i class="fa fa-map-marker"></i> {{$i_upcoming->place_name}}</p>   
                                    </div>
                                    <a class="ticket-btn" href="#">Buy Now</a>
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </aside>
                        <!-- Sponsors -->
                        <aside class="widget sponsors-widget">
                            <h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_sponsor')}}</h4>
                            <div class="sponsors">
                                @if(isset($sponsor) && count($sponsor)>0)
                                @foreach($sponsor as $i_sponsor)
                                <a class="sponsor-item" href="{{$i_sponsor->url}}"><img src="{{$i_sponsor->avatar}}" alt="{{$i_sponsor->name}}"></a>
                                @endforeach
                                @endif
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Single concert section End -->
</div>
@endsection