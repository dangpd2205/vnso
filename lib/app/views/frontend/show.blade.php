@extends("frontend.hometemplate")

@section("title")
@if(isset($tag) && count($tag)>0)
{{$tag->name}}
@elseif(isset($catenewsname))
<?php
if ($catenewsname->seotitle != '') {
    echo $catenewsname->seotitle;
} else {
    echo $catenewsname->name;
}
?>
@else
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_title')}}
@endif
@endsection

@section("desc")
@if(isset($tag) && count($tag)>0)
{{$tag->name}}
@elseif(isset($catenewsname))
<?php
if ($catenewsname->seodesc != '') {
    echo $catenewsname->seodesc;
} else {
    echo $catenewsname->name;
}
?>
@else
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_description')}}
@endif
@endsection

@section("keyword")
@if(isset($tag) && count($tag)>0)
{{$tag->name}}
@elseif(isset($catenewsname))
<?php
if ($catenewsname->seokeyword != '') {
    echo $catenewsname->seokeyword;
} else {
    echo $catenewsname->name;
}
?>
@else
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}
@endif
@endsection

@section("css")
<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
<link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
<link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/datepicker.min.css">
<link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
<link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">
@endsection

@section("js")
<script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
<script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
<script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
<script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
<script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
<script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
<script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
<script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
<script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
<script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
<script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>

<script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
<script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
<script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
<script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>

<script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
<script src="{{Asset('asset')}}/frontend/js/datepicker.min.js"></script>
<script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>
<script>
function removethis(t){
	$(t).parent().remove();
}
$('.filter-list input').datepicker({format: 'yyyy/mm/dd'});
</script>
@endsection

@section("content")
<div class="fix-space"></div>
<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
            <li class="active">
                @if(isset($catenewsname))
                {{$catenewsname->name}}
                @else
                {{\FontEnd\tblSettingModel::getTitleLang('lang_show')}}
                @endif
            </li>

        </ol>
    </div>
</div>
<!-- Page Header End -->
<div class="main-wrap">
    <div class="section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-7 col-xs-12">
                    <div class="filter-list">
                        <form method="post" action="{{URL::route('postfilter')}}" id="filter-form">
                            {{Form::token()}}
                            <ul>
                                <li>
                                    <label for="place"><i class="fa fa-map-marker" aria-hidden="true"></i> {{\FontEnd\tblSettingModel::getTitleLang('lang_location')}}</label>
                                    <select id="place" name="place">
                                        <option value="">{{\FontEnd\tblSettingModel::getTitleLang('lang_location')}}</option>
                                        @if(isset($all_place) && count($all_place)>0)
                                        @foreach($all_place as $i_all_place)
                                        <option value="{{$i_all_place->place_id}}">{{$i_all_place->name}}</option>
                                        @endforeach
                                        @endif                                   

                                    </select>
                                </li>
                                <li>
                                    <label>  <i class="fa fa-clock-o" aria-hidden="true"></i> {{\FontEnd\tblSettingModel::getTitleLang('lang_from')}}</label>
                                    <input type="text" name="datefrom" data-toggle="datepicker" placeholder="YYYY/MM/dd"/>
                                    <label>{{\FontEnd\tblSettingModel::getTitleLang('lang_to')}}</label>
                                    <input type="text" name="dateto" data-toggle="datepicker" placeholder="YYYY/MM/dd"/>
                                </li>
                                <li><i class="fa fa-filter" onclick="$('#filter-form').submit()" aria-hidden="true"></i></li>
                            </ul>
                        </form>
                    </div>
                    <div class="blog-list show-list">
                        @if(isset($data) && count($data)>0)    
                        <?php $i = 0; ?>           
                        @foreach($data as $i_data)
                        <article class="post">
                            <a class="post-thumb" href="{{URL::route('route_data',$i_data->slug.'ds')}}">
                                <img onerror="removethis(this)" class="img-responsive" src="{{action('ImageController@getResize')}}?src={{Asset($i_data->avatar)}}&w=424&h=594" alt="img">
                            </a>
                            <div class="post-content">
                                <h4 class="post-title"><a href="{{URL::route('route_data',$i_data->slug.'ds')}}">{{$i_data->name}} </a></h4>
                                @if(isset($i_data) && $i_data->period!='')
                                <p class="time"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$i_data->period}} {{\FontEnd\tblSettingModel::getTitleLang('lang_minutes')}}</p>
                                @endif
                                <ul class="list-info">
									@if($i_data->place_text=='')
										@if(isset($place[$i]) && count($place[$i])>0)
										<li><span>{{\FontEnd\tblSettingModel::getTitleLang('lang_location')}}:</span> {{$place[$i]->name}}</li>
										@endif
									@else
										<li><span>{{\FontEnd\tblSettingModel::getTitleLang('lang_location')}}:</span> {{$i_data->place_text}}</li>
									@endif
                                    <li><span>{{\FontEnd\tblSettingModel::getTitleLang('lang_time')}}:</span> {{date('d/m/Y',$i_data->time_show)}}</li>
                                    @if(isset($cate[$i]) && count($cate[$i])>0)
                                    <li><span>{{\FontEnd\tblSettingModel::getTitleLang('lang_category')}}:</span> @foreach($cate[$i] as $i_artcate) {{$i_artcate->name}} @endforeach</li>
                                    @endif
                                     @if(isset($conductor[$i]) && count($conductor[$i])>0) <li><span>{{\FontEnd\tblSettingModel::getTitleLang('lang_conductor')}}:</span>{{$conductor[$i]->a_title}} </li>@endif
                                    @if(isset($artist[$i]) && count($artist[$i])>0)
                                    <li><span>{{\FontEnd\tblSettingModel::getTitleLang('lang_artist_other')}}:</span> @foreach($artist[$i] as $i_artist) {{$i_artist->a_title}} @endforeach</li>
                                    @endif

                                </ul>
                                <div class="book-more">
                                    <ul>
                                        @if($i_data->is_sell==0)
											@if($i_data->sell_status==0)
												<li><a href="{{URL::route('seat',$i_data->show_id)}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_buyticket')}}</a></li>
											@endif
                                        @endif
                                        <li><a href="{{URL::route('route_data',$i_data->slug.'ds')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_readmore')}}</a></li>
                                    </ul>
                                </div>
                                <div class="event-date">
                                    <div class="relative">
                                        <div class="date-month">
                                            Tháng {{date('m',$i_data->time_show)}}
                                        </div>
                                        <div class="date-detail">
                                            <div class="date-num color-6">
                                                {{date('d',$i_data->time_show)}}
                                            </div>
                                            <div class="date-day">
                                                <?php
                                                if (date('N', $i_data->time_show) == 7) {
                                                    echo 'CN';
                                                } else {
                                                    echo 'Thứ ' . (date('N', $i_data->time_show) + 1);
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
<?php $i++; ?>
                        @endforeach
                        <!-- Pagination -->
                        <nav class="navigation post-pagination" role="navigation">
                            @if($data->links()!='')
                            {{$data->links('frontend.pani')}}
                            @endif
                        </nav>
                        <!-- Pagination End -->
                        @else
                        <h3>{{\FontEnd\tblSettingModel::getTitleLang('lang_empty')}}</h3>
                        @endif
                    </div>
                </div>
                <!-- Sidebar -->
                <div class="col-md-3 col-sm-5 col-xs-12">
                    <div class="sidebar">
                        <!-- Search widget -->
                        <aside class="widget search-widget">
                            <form id="widgetSearch" class="searchform" action="{{URL::route('searchshow')}}" method="post">
                                {{Form::token()}}
                                <input type="search" name="keyword" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_keyword')}}">
                                <button type="submit" name="searchsubmit"><i class="fa fa-search"></i></button>
                            </form>
                        </aside>
                        <!-- Category widget -->
                        <aside class="widget category-widget">
                            <h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_category')}}</h4>
                            @if(isset($arrCate) && count($arrCate)>0)
                            <ul class="widget-category">
                                @foreach($arrCate as $i_arrCate)
                                @if($i_arrCate->parent==0)
                                <?php
                                $check_one = false;
                                foreach ($arrCate as $check) {
                                    if ($check->parent == $i_arrCate->id) {
                                        $check_one = true;
                                    }
                                }
                                ?>
                                <li @if($check_one==true) data-toggle="collapse" data-target="#subMenu{{$i_arrCate->id}}" @endif>
                                     <a @if($check_one!=true) href="{{URL::route('route_data',$i_arrCate->slug.'cs')}}" @endif>{{$i_arrCate->name}}</a>
                                    @if($check_one==true)
                                    <ul class="list-second collapse" id="subMenu{{$i_arrCate->id}}" >
                                        @foreach($arrCate as $i_arrCate1)
                                        @if($i_arrCate1->parent == $i_arrCate->id)
                                        <li><a href="{{URL::route('route_data',$i_arrCate1->slug.'cs')}}">{{$i_arrCate1->name}}</a></li>
                                        @endif
                                        @endforeach
                                    </ul>
                                    @endif
                                </li>
                                @endif
                                @endforeach
                            </ul>
                            @endif
                        </aside>
                        <!-- Post widget -->
                        <aside class="widget post-widget">
                            <h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_random')}}</h4>
                            <div class="widget-posts">
                                @if(isset($random) && count($random)>0)               
                                @foreach($random as $i_random)
                                <div class="widget-post">
                                    <a class="widget-post-thumb" href="{{URL::route('route_data',$i_random->slug.'dn')}}"><img onerror="removethis(this)" class="img-responsive" src="{{action('ImageController@getResize')}}?src={{Asset($i_random->avatar)}}&w=70&h=70" alt="blog thumb"></a>
                                    <div class="widget-post-body">
                                        <a class="widget-post-title" title="{{$i_random->name}}" href="{{URL::route('route_data',$i_random->slug.'ds')}}">{{str_limit($i_random->name,25,'...')}}</a>
                                        <span class="widget-post-date">{{date('d M Y',$i_random->time_show)}}</span>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </aside>
                        <!-- Tag widget -->
                        <aside class="widget tag-widget">
                            <h4 class="widget-title">Tag</h4>
                            <div class="widget-tag">
                                @if(isset($tags) && count($tags)>0)               
                                @foreach($tags as $i_tags)
                                <a href="{{URL::route('route_data',$i_tags->slug.'ts')}}">{{$i_tags->name}}</a>
                                @endforeach
                                @endif                                
                            </div>
                        </aside>
                        <!-- Social Media -->
                        <aside class="widget social-link-widget">
                            <h4 class="widget-title">Follow us in Social</h4>
                            <div class="social-media">
                                <a class="@if(isset($setting->facebook_fanpage)){{$setting->facebook_fanpage}}@endif"><i class="fa fa-facebook"></i></a>
                                <a class="@if(isset($setting->twitter_fanpage)){{$setting->twitter_fanpage}}@endif"><i class="fa fa-twitter"></i></a>
                                <a class="@if(isset($setting->google_fanpage)){{$setting->google_fanpage}}@endif"><i class="fa fa-google-plus"></i></a>
                                <a class="@if(isset($setting->youtube)){{$setting->youtube}}@endif"><i class="fa fa-youtube-play"></i></a>
                                <a class="@if(isset($setting->linkedin_fanpage)){{$setting->linkedin_fanpage}}@endif"><i class="fa fa-linkedin"></i></a>
                                <a class="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection