
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('desc')"/>
	<meta name="keywords" content="@yield('keyword')"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">	<link rel="icon" href="favicon.png"/>
	@yield('facebooktag')
		@yield('css')
		<script src="{{Asset('asset')}}/frontend/js/jquery-2.2.3.min.js"></script>
		<link href="https://fonts.googleapis.com/css?family=Saira+Condensed" rel="stylesheet">
		
		
    </head>
    <body>
    		<div class="notification"></div>
			@include('frontend.header')
			@yield("content")
			@include('frontend.footer')
			
		</div>
@yield('js')

    </body>
</html>
	