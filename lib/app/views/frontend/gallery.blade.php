@extends("frontend.hometemplate")
@section("title")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_title')}}
@endsection
@section("desc")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_description')}}
@endsection
@section("keyword")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}
@endsection
@section("css")
	        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/lightbox.min.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
		<link rel="stylesheet" href="{{Asset('asset')}}/frontend/lightgallery/css/lightgallery.min.css">
@endsection

@section("js")
	<script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
    <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
    <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
    <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
    <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/lightbox.js"></script>
    <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
    <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
    <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>	<script>		function removethis(t){				$(t).parent().remove();			}		</script>
	<script src="{{Asset('asset')}}/frontend/lightgallery/js/lightgallery.min.js"></script>
		<script src="{{Asset('asset')}}/frontend/lightgallery/js/lightgallery-plugins.js"></script>
		<script>

        $(document).ready(function () {
			$(".lightgallery").lightGallery({

				// Please read about gallery options here: http://sachinchoolur.github.io/lightGallery/docs/api.html

				// lightgallery core 
				selector: '.lg-trigger',
				mode: 'lg-fade', // Type of transition between images ('lg-fade' or 'lg-slide').
				height: '100%', // Height of the gallery (ex: '100%' or '300px').
				width: '100%', // Width of the gallery (ex: '100%' or '300px').
				iframeMaxWidth: '100%', // Set maximum width for iframe.
				loop: true, // If false, will disable the ability to loop back to the beginning of the gallery when on the last element.
				speed: 600, // Transition duration (in ms).
				closable: true, // Allows clicks on dimmer to close gallery.
				escKey: true, // Whether the LightGallery could be closed by pressing the "Esc" key.
				keyPress: true, // Enable keyboard navigation.
				hideBarsDelay: 5000, // Delay for hiding gallery controls (in ms).
				controls: true, // If false, prev/next buttons will not be displayed.
				mousewheel: true, // Chane slide on mousewheel.
				download: false, // Enable download button. By default download url will be taken from data-src/href attribute but it supports only for modern browsers. If you want you can provide another url for download via data-download-url.
				counter: true, // Whether to show total number of images and index number of currently displayed image.
				swipeThreshold: 50, // By setting the swipeThreshold (in px) you can set how far the user must swipe for the next/prev image.
				enableDrag: true, // Enables desktop mouse drag support.
				enableTouch: true, // Enables touch support.

				// thumbnial plugin
				thumbnail: true, // Enable thumbnails for the gallery.
				showThumbByDefault: false, // Show/hide thumbnails by default.
				thumbMargin: 5, // Spacing between each thumbnails.
				toogleThumb: true, // Whether to display thumbnail toggle button.
				enableThumbSwipe: true, // Enables thumbnail touch/swipe support for touch devices.
				exThumbImage: 'data-exthumbnail', // If you want to use external image for thumbnail, add the path of that image inside "data-" attribute and set value of this option to the name of your custom attribute.

				// autoplay plugin
				autoplay: false, // Enable gallery autoplay.
				autoplayControls: true, // Show/hide autoplay controls.
				pause: 6000, // The time (in ms) between each auto transition.
				progressBar: true, // Enable autoplay progress bar.
				fourceAutoplay: false, // If false autoplay will be stopped after first user action

				// fullScreen plugin
				fullScreen: true, // Enable/Disable fullscreen mode.

				// zoom plugin
				zoom: true, // Enable/Disable zoom option.
				scale: 0.5, // Value of zoom should be incremented/decremented.
				enableZoomAfter: 50, // Some css styles will be added to the images if zoom is enabled. So it might conflict if you add some custom styles to the images such as the initial transition while opening the gallery. So you can delay adding zoom related styles to the images by changing the value of enableZoomAfter.


				// hash plugin (unique url for each slides)
				hash: true, // Enable/Disable hash plugin.
				hgalleryId: 1, // Unique id for each gallery. It is mandatory when you use hash plugin for multiple galleries on the same page.


				});
           
        });
    </script>
@endsection
@section("content")
<div class="fix-space"></div>
<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
            <li><a href="#">{{\FontEnd\tblSettingModel::getTitleLang('lang_gallery')}}</a></li>
            <li class="active">{{\FontEnd\tblSettingModel::getTitleLang('lang_photo')}}</li>
        </ol>
    </div>
</div>
<!-- Page Header End -->
<div class="main-wrap">

    <div class="section section-padding gallery-page">
        <div class="container">
            @if(isset($gallery) && count($gallery)>0)
            <div class="row">
                <div class="gallery column-4">
                    @foreach($gallery as $item)
                    <div class="col-md-4 col-xs-6 lightgallery">
                        @if($item->avatar!='')
                        <a class="lg-trigger" data-exthumbnail="{{$item->avatar}}" href="{{$item->avatar}}">
                            <div class="gallery-item">
                                <img onerror="removethis(this)" src="{{action('ImageController@getResize')}}?src={{Asset($item->avatar)}}&w=358&h=239" alt="{{$item->name}}">
                                <div class="gallery-text">
                                    <h4 class="head-gallery">{{$item->name}}</h4>
                                </div>
                            </div>
                        </a>
                        <?php 
                        if($item->images!=''){
                            $data_img = explode(',',$item->images);
                        }else{
                            $data_img=[];
                        }
                        ?>
                            @if(count($data_img)>0)
                            @foreach($data_img as $img)
                            <a class="lg-trigger" data-exthumbnail="{{$img}}" href="{{$img}}"></a>
                            @endforeach
                            @endif
                        @endif
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <nav class="navigation post-pagination" role="navigation">
                                @if($gallery->links()!='')
                                {{$gallery->links('frontend.pani')}}
                                @endif
                    </nav>
                </div>
            </div>
            @else
                    <div class="row"><h3>{{\FontEnd\tblSettingModel::getTitleLang('lang_empty')}}</h3></div>
            @endif
        </div>
    </div>

</div>
@endsection

