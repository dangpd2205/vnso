<footer>
  <!-- Footer Subscribe -->
  <!--<div class="subscription-area section-padding theme-bg ye-bg">
      <div class="container">
          <div class="row">
              <div class="col-sm-6">
                  <div class="section-header left-style">
                      <h3 class="section-title">{{\tblSettingModel::getTitleLang('lang_subcribe')}}</h3>
                  </div>
              </div>
              <div class="col-sm-6">
                  <form class="subscription" action="{{URL::route('sub')}}" method="post" id="sub-form">
                    {{Form::token()}}
                      <input type="email" name="email" placeholder="Email" required>
                      <button type="button" class="btn btn-white" onclick="submitForm('sub-form')">{{\tblSettingModel::getTitleLang('lang_register')}}</button>
                      <div id="messages_sub"></div>
                  </form>
              </div>
          </div>
      </div>
  </div> -->
  <!-- Footer Subscribe -->
  <!-- Footer logo and social media button -->
  <div class="logo-social-area section-padding text-white">
      <div class="container text-center">
          <a class="logo logo-footer" href="{{Asset('')}}">
              <img style="width: 100%;" src="{{Asset($setting->website_logo_default)}}" alt="Site Logo">
          </a>
          <div class="footer-contact">
              <p class="phone"><i class="fa fa-map-marker" aria-hidden="true"></i> {{\tblSettingModel::getTitleLang('lang_addr')}}  </p>
              <p class="phone"><i class="fa fa-phone"></i> @if(isset($setting->contact_phone)){{$setting->contact_phone}}@endif </p>
              <p class="phone"><i class="fa fa-fax" aria-hidden="true"></i> @if(isset($setting->contact_fax)){{$setting->contact_fax}}@endif</p>
              <p class="email"><i class="fa fa-envelope-o"></i> @if(isset($setting->contact_email)){{$setting->contact_email}}@endif</p>
          </div>
          <div class="socials">
              <a href="@if(isset($setting->facebook_fanpage)){{$setting->facebook_fanpage}}@endif"><i class="fa fa-facebook"></i></a>
              
              <a href="@if(isset($setting->google_fanpage)){{$setting->google_fanpage}}@endif"><i class="fa fa-google-plus"></i></a>
              <a href="@if(isset($setting->youtube)){{$setting->youtube}}@endif"><i class="fa fa-youtube-play"></i></a>
              
          </div>
      </div>
  </div>
  <!-- Footer logo and social media button -->
  <!-- Footer copyrgiht and navigation -->
  <div class="copyright-footer">
      <div class="container">
          <div class="row">
              <div class="col-sm-6 col-xs-12">
                  <p class="copyright">Copyright &copy; 2017 VNSO </p>
              </div>
              <div class="col-sm-6 col-xs-12">
                  <p class="credit-text"><a href="https://pubweb.vn/"> Thiết kế website chuyên nghiệp</a> Pubweb</p>
              </div>
          </div>
      </div>
  </div>
  <!-- Footer copyrgiht and navigation -->
</footer>
