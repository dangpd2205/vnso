@if(isset($artist) && count($artist)>0)
<div class="col-md-6">
    <div class="img-member">
        <img src="{{action('ImageController@getResize')}}?src={{Asset($artist->avatar)}}&w=539&h=527" alt="member">
    </div>
</div>
<div class="col-md-6">
    <div class="info-member">
        <h4 class="name-member">{{$artist->a_title}}<br>
        <small>{{$artist->a_instrument}}</small>
        </h4>
        {{$artist->a_content}}
        <ul>
            <li>{{\FontEnd\tblSettingModel::getTitleLang('lang_contact')}}</li>
            @if($artist->a_phone!='')<li><i class="fa fa-phone" aria-hidden="true"></i> <a href="#">{{$artist->a_phone}}</a></li>@endif
            @if($artist->a_email!='')<li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="#">{{$artist->a_email}}</a></li>@endif
        </ul>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
</div>
@endif
                