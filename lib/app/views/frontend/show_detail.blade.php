@extends("frontend.hometemplate")

<?php
if (isset($data)) {
    if($data->seodesc!=''){
        $des_character = strip_tags($data->seodesc);     
    }else if($data->description!=''){
        $des_character = strip_tags($data->description);
    }else {
        $des_character = "";
    }
    if($data->seotitle!=''){
        $title_character = strip_tags($data->seotitle);
    }else if($data->name!=''){
        $title_character = strip_tags($data->name);
    }else {
        $title_character = "";
    }
} else {
    $des_character = "";
    $title_character = "";
}
$title_character.='-'.\FontEnd\tblSettingModel::getTitleLang('lang_website_title');
if (strlen($des_character) > 160) {
    $des_page = substr($des_character, 0, 155);
} else {
    $des_page = $des_character;
}

if (strlen($title_character) > 70) {
    $tit_page = substr($title_character, 0, 70);
} else {
    $tit_page = $title_character;
}
?>
@section("title")
<?php echo $tit_page; ?>
@endsection
@section("desc")
<?php echo $des_page; ?>
@endsection

@section("keyword")

{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}

@endsection
@section("facebooktag")

<meta property="og:url" content="{{Request::url()}}" />

<meta property="og:type" content="article" />

<meta property="og:title" content="<?php if($data->seofbtitle!=''){echo $data->seofbtitle;}else{echo $data->name;} ?>" />

<meta property="og:description" content="<?php if($data->seofbdesc!=''){echo $data->seofbdesc;}else{echo $data->description;} ?>" />

<meta property="og:image" content="{{Asset($data->avatar)}}" />

@endsection
@section("css")
   <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">
@endsection

@section("js")
    <script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
        
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
        <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>
@endsection

@section("content")
<div class="fix-space"></div>
<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
            <li><a href="{{Asset('chuongtrinh')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_show')}}</a></li>
            <li class="active">
             {{$data->name}}
            </li>
            
        </ol>
    </div>
</div>
<!-- Page Header End -->
<div class="main-wrap">
    <div class="section section-padding single-concert-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <article class="concert-single">
                        <h3 class="concert-title">{{$data->name}}</h3>
                        <div class="concert-table">
                                    <table class="concert-metas">
                                        <tbody>
                                            <tr>
                                            <td rowspan="2" style=" width: 50%;">
                                               <span class="concert-meta concert-date"><label>{{\FontEnd\tblSettingModel::getTitleLang('lang_time')}}:</label> {{date('d m Y H:i',$data->time_show)}}</span>
                             
                                            </td>

                                            <td style=" width: 50%;">
                                               <span class="concert-meta concert-date"><label>{{\FontEnd\tblSettingModel::getTitleLang('lang_location')}}:</label> 
												@if($data->place_text=='')
													@if(isset($place) && count($place)>0){{$place->name}}@endif
												@else
													{{$data->place_text}}
												@endif
											   </span>
                                                
                                            </td>
                                          
                                            
                                        </tr>

                                    </tbody></table>
                                </div>
                        <p class="concert-metas">
                           
                        </p>
						
						
                        <div class="concert-entry">
                            {{$data->content}}
                        </div>
                    </article>
					@if(isset($comment) && count($comment)>0)
                    <div class="comments-section">
                        <p class="lead">{{\FontEnd\tblSettingModel::getTitleLang('lang_comment')}}</p>
                        
                        <?php                        
                        
                            \Tree::showComment($comment);
                        
                        ?>   
                     
                        
                    </div>
					@endif
                    <div class="give-your-reply">
                        <div class="commentform-header">
                            <h4>{{\FontEnd\tblSettingModel::getTitleLang('lang_comment')}}</h4>
                        </div>
                        <form id="comment-form" class="comment-form" action="{{URL::route('postcomment')}}" method="post">
                            {{Form::token()}}
                            <div class="row">
                                <div class="col-md-6">
                                    <label>{{\FontEnd\tblSettingModel::getTitleLang('lang_name')}} <br/>
                                        <input type="text" name="name" id="name" required>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>Email <br/>
                                        <input type="text" name="email" id="email" required>
                                    </label>
                                </div>
                            </div>
                            <div class="row" style="display:none" id="li_hidden">
                                <div class="col-md-6">
                                    <label>{{\FontEnd\tblSettingModel::getTitleLang('lang_reply')}} <a onclick="hide_div()" style="padding:5px">X</a><br/> 
                                        <input readonly="true" type="text" id="reply_name" name="reply_name" />
                                    </label>
                                </div>
                            </div>
                            <label>{{\FontEnd\tblSettingModel::getTitleLang('lang_content')}}<br/>
                                <textarea name="message" id="comment" rows="5" ></textarea>
                            </label>
                            <input type="hidden" name="parent" id="parent"/>
                            <input type="hidden" name="post_type" id='post_type' value="5"/>
                            <input type="hidden" name="post_id" value="{{$data->id}}"/>
                            <div id="messages_notify">

                        </div>
                            <button type="button" class="btn btn-black" onclick="submitForm('comment-form')">{{\FontEnd\tblSettingModel::getTitleLang('lang_send')}}</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="sidebar concert-sidebar">
                        <!-- Ticket widget -->
                        <aside class="widget ticket-widget">
                            @if($data->is_sell!=0)								@if($data->sell_status!=0)									<h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_noticket1')}}</h4>								@else									<h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_noticket')}}</h4>								@endif
                            
							@else
								@if($data->sell_status!=0)									<h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_noticket1')}}</h4>	@endif
                            @endif
                            <div class="ticket-pricing">
							
                                @if($data->is_sell==0)
									@if($data->sell_status!=0)
										<div class="book-ticket text-center">
											<a class="ticket-btn btn-black" href="{{URL::route('contact')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_contact')}}</a>
										</div>
										
									@else
										@if(isset($ticket) && count($ticket)>0)
										@foreach($ticket as $i_ticket)
										<div class="ticket-pricing-item @if($i_ticket->ticket_id==3)seat-1 @elseif($i_ticket->ticket_id==2)seat-2 @else seat-3 @endif">
											<p class="ticket-pack-name"> {{$i_ticket->name}}</p>
											<p class="ticket-price">{{number_format($i_ticket->price)}} VNĐ</p>
											
										</div>
										@endforeach
										@endif                              
										
										<div class="book-ticket text-center">
											<a class="ticket-btn btn-black" href="{{URL::route('seat',$data->show_id)}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_buyticket')}}</a>
										</div>
									@endif
                                @else
									
									<div class="book-ticket text-center">
										<a class="ticket-btn btn-black" href="{{URL::route('contact')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_contact')}}</a>
									</div>
									
                                @endif
                            </div>
                        </aside>
                        <!-- Upcoming music show -->
                        <aside class="widget upcoming-show-widget">
                            <h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_upcoming_show')}}</h4>
                            <div class="upcoming-shows">
                                @if(isset($upcoming) && count($upcoming)>0)               
                                  @foreach($upcoming as $item)
                                <div class="upcoming-show-item">
                                    <div class="show-content">
                                        <h5 class="show-title">{{date('d M',$item->time_show)}}</h5>
                                        <p class="show-type">{{$item->name}}</p>

                                        <p class="show-venue"><i class="fa fa-map-marker"></i> 
										@if($item->place_text=='')
											{{$item->place_name}}
										@else
											{{$item->place_text}}
										@endif
										</p>   
                                    </div>
									@if($item->is_sell==0)
                                    <a class="ticket-btn" href="{{URL::route('seat',$item->show_id)}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_buyticket')}}</a>
									@endif
                                </div>
                                 @endforeach
                                @endif
                              
                            </div>
                        </aside>
                        <!-- Sponsors -->
                        <aside class="widget sponsors-widget">
                            <h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_sponsor')}}</h4>
                            <div class="sponsors">
                                @if(isset($sponsor) && count($sponsor)>0)               
                                  @foreach($sponsor as $i_sponsor)
                                <a class="sponsor-item" href="{{$i_sponsor->url}}"><img src="{{$i_sponsor->avatar}}" alt="{{$i_sponsor->name}}"></a>
                                 @endforeach
                                @endif
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection