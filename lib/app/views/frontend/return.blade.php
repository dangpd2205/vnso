
@extends("frontend.hometemplate")

@section("title")

{{\FontEnd\tblSettingModel::getTitleLang('lang_website_title')}}

@endsection

@section("desc")

{{\FontEnd\tblSettingModel::getTitleLang('lang_website_description')}}

@endsection

@section("keyword")

{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}

@endsection
@section("css")
   <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">
@endsection

@section("js")
    <script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
        
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
        
        <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/jquery.countdown.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>
        
@endsection
@section("content")
<script type="text/javascript">
    function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
    }
</script>
<div class="fix-space"></div>
    <!-- Page Header -->
    <div class="page-header">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
                <li class="active">              
                  {{\FontEnd\tblSettingModel::getTitleLang('lang_confirm')}}              
                </li>
            </ol>
        </div>
    </div>
    <div class="main-wrap">
        <div class="section section-padding book-seat book-info">
            <div class="container">
            
                <div class="row">
                    <div class="col-md-12">
                        <div class="tks">
                            <div class="bg-ticket text-center">
                                <img src="{{Asset('asset/frontend')}}/img/bg-ticket.svg" alt="img">
                            </div>
                            <h2 class="text-tks">@if(isset($msg)){{$msg}}@endif</h2>
                            <div id="tks">
                                @if(isset($show) && count($show)>0)
                                <div class="last-time">
                                    <div class="bor-time">
                                        <div class="info-left">
                                        <ul>
                                            <li> {{\FontEnd\tblSettingModel::getTitleLang('lang_order_code')}}  <span>{{$data_order->code}}</span></li>
                                            <li>{{date('d/m/Y',$show->time_show)}} &nbsp&nbsp&nbsp {{date('H:i',$show->time_show)}}</li>
                                            <li>{{$show->place_name}}</li>
                                            <?php
                                            $total=0;
                                            foreach($data_detail as $item){
                                                $total+=$item->seat_price;
                                            }
                                            ?>
                                            
                                            <li><span>{{\FontEnd\tblSettingModel::getTitleLang('lang_total')}}: {{number_format($total)}} VNĐ</span></li>
                                        </ul>
                                    </div>
                                    <div class="info-right">
                                        <div class="up-info">
                                            <h4 class="text-1">{{\FontEnd\tblSettingModel::getTitleLang('lang_show')}} </h4>
                                            <h5 class="text-2">{{$show->name}}</h5>
                                        </div>
                                        <div class="down-info">
                                            <h4 class="text-seat">
                                                {{\FontEnd\tblSettingModel::getTitleLang('lang_seatnumber')}} 
                                                @if(isset($ticket) && count($ticket)>0)
                                                @foreach($ticket as $i_ticket)
                                                <li>{{$i_ticket->name}}: 
                                                    @foreach($data_detail as $item)
                                                    @if($item->seat_type==$i_ticket->ticket_id)
                                                    <span>{{$item->seat_name}}({{$item->seat_name_orin}})</span> &nbsp;
                                                    @endif
                                                    @endforeach
                                                </li> 
                                                @endforeach
                                                @endif
                                                
                                            </h4>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>

                            <div class="print">
                                <ul>
                                    <li>
                                        <a href="#" onclick="printDiv('tks')"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
                                    </li>
                                </ul>
                            </div>
                            <h4 class="send-mail">{{\FontEnd\tblSettingModel::getTitleLang('lang_order_hint')}} {{$data_order->cus_email}}</h4>
                             @endif
                            
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                            <a class="btn btn-black btn-lg" href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a>
                        </div>
                </div>
            </div>
            
        </div>
    </div>
@endsection