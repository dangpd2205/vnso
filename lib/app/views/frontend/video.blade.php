@extends("frontend.hometemplate")
@section("title")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_title')}}
@endsection
@section("desc")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_description')}}
@endsection
@section("keyword")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}
@endsection
@section("css")
   <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/lightbox.min.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/YouTubePopUp.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">
@endsection

@section("js")
    <script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
    <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
    <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
    <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
    <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
    <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/lightbox.js"></script>
    <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
    <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
    <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>
@endsection
@section("content")
<div class="fix-space"></div>
<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
            <li><a href="#">{{\FontEnd\tblSettingModel::getTitleLang('lang_gallery')}}</a></li>
            <li class="active">Video</li>
        </ol>
    </div>
</div>
<!-- Page Header End -->
 <div class="main-wrap">

    <div class="section section-padding video-page">
        <div class="container">
            @if(isset($video) && count($video)>0)
            <div class="row">
                <div class="gallery column-4">
                    @foreach($video as $item)
                    <div class="col-md-4 col-xs-6">
                        <a class="video-popup" href="{{$item->url}}">
                            <div class="gallery-item">
                                <img src="{{$item->avatar}}" alt="Gallery Thumb">
                                <div class="gallery-text">
                                    <h4 class="head-gallery">{{$item->name}} </h4>
                                </div>
                        </div>
                        </a>
                        
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <nav class="navigation post-pagination" role="navigation">
                        @if($video->links()!='')
                                    {{$video->links('frontend.pani')}}
                                    @endif
                    </nav>
                </div>
            </div>
            @else
                        <div class="row"><h3>{{\FontEnd\tblSettingModel::getTitleLang('lang_empty')}}</h3></div>
                @endif
        </div>
    </div>

</div>


@endsection

