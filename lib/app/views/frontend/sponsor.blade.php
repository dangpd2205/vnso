@extends("frontend.hometemplate")
@section("title")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_title')}}
@endsection
@section("desc")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_description')}}
@endsection
@section("keyword")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}
@endsection

@section("css")
   <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
		<link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/datepicker.min.css"/>
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
		<style>
			.sec-input input{
				float:left;
			}
		</style>
@endsection

@section("js")
    <script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
        
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
		<script type="text/javascript" src="{{Asset('asset')}}/frontend/js/datepicker.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
        
        <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>
<script type="text/javascript">
   function removethis(t){	$(t).remove();}
	$('.date-birth').datepicker({format: 'yyyy/mm/dd'});
    
</script>
@endsection

@section("content")
<div class="fix-space"></div>
<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
            <li class="active">
              {{\FontEnd\tblSettingModel::getTitleLang('lang_regsponsor')}}
              
            </li>
            
        </ol>
    </div>
</div>
<div class="main-wrap">
	<div class="section custom-sign ">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="img-sign">
						@if(isset($setting->website_image_adv9) && $setting->website_image_adv9!='')
						<img src="{{$setting->website_image_adv9}}" onerror="removethis(this)" alt="img"/>
						@endif
						@if(isset($setting->website_image_adv8) && $setting->website_image_adv8!='')
						<img src="{{$setting->website_image_adv8}}" onerror="removethis(this)" alt="img"/>
						@endif
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="sign-form">
						<div class="detail-form">
							<h4 class="head-form">
								{{\FontEnd\tblSettingModel::getTitleLang('lang_title_sponsor')}}
							</h4>
							<div class="form-content">
							<form action="{{URL::route('postsponsor')}}" method="post">
							{{Form::token()}}
								<div class="row">
									<div class="col-md-12">
									{{Form::text('name','',array('placeholder'=>\FontEnd\tblSettingModel::getTitleLang('lang_name')))}}
										
									</div>
									<div class="col-md-12  sec-input">
										<input type="text" class="date-birth" data-toggle="datepicker" placeholder="YYYY/MM/DD" readonly='true' name="dob"/>
										
										{{Form::text('sex','',array('placeholder'=>\FontEnd\tblSettingModel::getTitleLang('lang_sex')))}}
									</div>
								  
									<div class="col-md-12 sec-input">
										{{Form::text('city','',array('placeholder'=>\FontEnd\tblSettingModel::getTitleLang('lang_city')))}}
										{{Form::text('job','',array('placeholder'=>\FontEnd\tblSettingModel::getTitleLang('lang_job')))}}
									</div>
								   
									<div class="col-md-12">
										{{Form::text('address','',array('placeholder'=>\FontEnd\tblSettingModel::getTitleLang('lang_address')))}}
									</div>
									<div class="col-md-12 sec-input">
										{{Form::text('phone','',array('placeholder'=>\FontEnd\tblSettingModel::getTitleLang('lang_phone')))}}
										{{Form::text('email','',array('placeholder'=>'Email'))}}</div>
									
									<div class="col-md-12 sec-input ">
										{{Form::text('fax','',array('placeholder'=>'Fax'))}}
										{{Form::text('note','',array('placeholder'=>\FontEnd\tblSettingModel::getTitleLang('lang_note')))}}</div>
								   
									<div class="col-md-12">
										<div class="box-bg">
											<p>{{\FontEnd\tblSettingModel::getTitleLang('lang_title_sponsor1')}}</p>
										<div class="step-info">
											<ul>
												<li>
													<input type="radio" checked="checked" value="{{\FontEnd\tblSettingModel::getTitleLang('lang_diamond')}}" name="customsign" id="diamond">
													<label for="diamond"> {{\FontEnd\tblSettingModel::getTitleLang('lang_diamond')}}</label>
												</li>
												 <li>
													<input type="radio" value="{{\FontEnd\tblSettingModel::getTitleLang('lang_gold')}}" name="customsign" id="memgold">
													<label for="memgold"> {{\FontEnd\tblSettingModel::getTitleLang('lang_gold')}}</label>
												</li>
												<li>
													<input type="radio" value="{{\FontEnd\tblSettingModel::getTitleLang('lang_silver')}}" name="customsign" id="gold">
													<label for="gold"> {{\FontEnd\tblSettingModel::getTitleLang('lang_silver')}}</label>
												</li>
												<li>
													<input type="radio" value="{{\FontEnd\tblSettingModel::getTitleLang('lang_gold1')}}" name="customsign" id="memsil">
													<label for="memsil"> {{\FontEnd\tblSettingModel::getTitleLang('lang_gold1')}}</label>
												</li>
												<li>
													<input type="radio" value="{{\FontEnd\tblSettingModel::getTitleLang('lang_silver1')}}" name="customsign" id="silver">
													<label for="silver"> {{\FontEnd\tblSettingModel::getTitleLang('lang_silver1')}}</label>
												</li>
											   
												
											</ul>
										</div>
										</div>

									</div>
									<div class="col-md-12">
										@if(\Session::has('sucessful'))
										  {{\Session::get('sucessful')}}
										  @endif
										<?php
										if ($errors->has()) {
										  ?>
										  <div class="alert alert-error alert-block">     
											<ul class="mes_error">
											  <?php
											  foreach ($errors->all() as $errors_item) {
												echo $errors_item;
											  }
											  ?>
											</ul>
											</p>
										  </div>
										  <?php
										}
										?>
									</div>
									<div class="col-md-3">
										<input type="submit" value="{{\FontEnd\tblSettingModel::getTitleLang('lang_send')}}" style="background: #f4e685;border-radius: 5px;font-weight: bold;"/>
									</div>
									<div class="col-md-12">
										<div class="box-bg">
											{{\FontEnd\tblSettingModel::getTitleLang('lang_add_receive')}}
										</div>
									</div>
									<div class="col-md-12">
										<div class="box-bg">
											{{\FontEnd\tblSettingModel::getTitleLang('lang_contact_info')}}
										</div>
									</div>
									
								</div>
							</form>
						</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
              

@endsection