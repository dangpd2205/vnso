<?php
$presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);
?>

<?php if ($paginator->getLastPage() > 1): ?>

        
        <div class="nav-links">
            <?php
            /* How many pages need to be shown before and after the current page */
            $showBeforeAndAfter = 5;

            /* Current Page */
            $currentPage = $paginator->getCurrentPage();
            $lastPage = $paginator->getLastPage();
            $start = 0;
            if ($currentPage - $showBeforeAndAfter < -1 || $lastPage < $showBeforeAndAfter) {
                $start = 1;
                if ($lastPage < $showBeforeAndAfter) {
                    $end = $lastPage;
                } else {
                    $end = 5;
                }
            } else {
                if ($lastPage - $currentPage > 2) {
                    $start = $currentPage - 2;
                    $end = $currentPage + 2;
                } else {
					if($lastPage - 5>0){				
						$start = $lastPage - 5;
					}else{
						$start=1;
					}
                    $end = $lastPage;
                }
            }
            if ($currentPage != 1) {
                ?>
                <a href="{{$paginator->getUrl(1)}}" class="prev page-numbers">‹‹</a>
                <?php
            } else {
                ?>
                <a href="javascript: void(0)" class="prev page-numbers">‹</a>
                <?php
            }
            if ($lastPage != 1) {
                for ($i = $start; $i <= $end; $i++) {
                    if ($i != $currentPage) {
                        ?>
                        <a href="{{$paginator->getUrl($i)}}" class="page-numbers"><?php echo $i; ?></a>
                        <?php
                    } else {
                        ?>
                        <span class="page-numbers current"><?php echo $i; ?></span>
                        <?php
                    }
                }
                //  echo $presenter->getPageRange($start, $end);
            }
            if ($currentPage != $lastPage) {
                ?>
                <a href="{{$paginator->getUrl($lastPage)}}" class="next page-numbers">››</a>
                    <?php
                }
                ?>
        </div>

<?php endif; ?>
