@extends("frontend.hometemplate")

@section("title")

{{\FontEnd\tblSettingModel::getTitleLang('lang_website_title')}}

@endsection

@section("desc")

{{\FontEnd\tblSettingModel::getTitleLang('lang_website_description')}}

@endsection

@section("keyword")

{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}

@endsection

@section("css")
   <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">
@endsection

@section("js")
    <script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
        
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
        
        <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/jquery.countdown.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>
        
         <script type="text/javascript">
            var url_payment="{{URL::route('payment')}}";
            function payment(){                
                $.ajax({
                    type: 'POST',
                    url: url_payment,
                    data: {address: $("#address").val(),name: $("#name").val(),phone: $("#phone").val(),email: $("#email").val(),agree: $("#agree").is(':checked'),p_type:$('input[name=p_type]:checked').val()}
                }).done(function(result) {
                    var msg = $.parseJSON(result);
                    if(msg.check==0){
                        $('.notification').html(msg.content);
                        $('.notification').slideDown('fast');
                        $('.notification').delay(1000);
                        $('.notification').slideUp('fast');
                    }else if(msg.check==1){
                        $('.notification').html(msg.content);
                        $('.notification').slideDown('fast');
                        $('.notification').delay(1000);
                        $('.notification').slideUp('fast');
                        setTimeout(function(){ window.location.href = msg.redirect; }, 2000);
                    }else{
                        window.location.replace(msg.redirect);
                    }
                });
            }
            

            var url_clear = "{{URL::route('clear')}}";
            //countdown
            // Set the date we're counting down to
            var countDownDate = new Date();
            countDownDate.setMinutes(countDownDate.getMinutes() + 5);
            // Update the count down every 1 second
            var x = setInterval(function() {
              // Get todays date and time
              var now = new Date().getTime();

              // Find the distance between now an the count down date
              var distance = countDownDate - now;

              // Time calculations for days, hours, minutes and seconds
              var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
              var seconds = Math.floor((distance % (1000 * 60)) / 1000);

              // Display the result in the element with id="demo"
              document.getElementById("getting-started").innerHTML = '<div class="minut">' +minutes + '</div><div class="sec">' + seconds + '</div>';

              // If the count down is finished, write some text
              if ((minutes == 0) && (seconds==0)) {
                clearInterval(x);
                $.ajax({
                    type: 'POST',
                    url: url_clear,
                    data: {}
                }).done(function(result) {
                    var msg = $.parseJSON(result);
                    if(msg.check==1){
                        $('.notification').html(msg.content);
                        $('.notification').slideDown('fast');
                        $('.notification').delay(1000);
                        $('.notification').slideUp('fast');
                        setTimeout(function(){ window.location.href = msg.redirect; }, 2000);
                    }          
                });
              }
            }, 1000);
        </script>
@endsection

@section("content")
<div class="fix-space"></div>
<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
            <li class="active">              
              {{\FontEnd\tblSettingModel::getTitleLang('lang_checkout')}}              
            </li>
            
        </ol>
    </div>
</div>
<!-- Page Header End -->
<div class="main-wrap">
    <div class="section section-padding book-seat book-info">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="head-book">
                        <h4>{{\FontEnd\tblSettingModel::getTitleLang('lang_checkout')}}  </h4>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="info-book">
                        <p>@if(isset($place) && count($place)>0){{$place[0]->name}}@endif | {{\FontEnd\tblSettingModel::getTitleLang('lang_seatnumber')}} ({{count($place)}})</p>
                        <p>@if(isset($show) && count($show)>0){{date('d/m/Y H:i',$show->time_show)}}@endif</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="check-all">
                        <div class="col-md-8">
                            <div class="step-info">
                                <div class="title-step">
                                    <h4 class="text"> <strong>{{\FontEnd\tblSettingModel::getTitleLang('lang_information')}}</strong></h4>
                                </div>
                                <div class="form-step">
                                    <p>
                                        <input type="text" name="name" id="name" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_name')}}" required>
                                    </p>
                                    <p>
                                        <input type="text" name="phone" id="phone" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_phone')}}" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
                                    </p>
                                    <p>
                                        <input type="text" name="email" id="email" placeholder="Email" required>
                                    </p>
                                    <p>
                                        <input type="text" name="address" id="address" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_address')}}" >
                                    </p>
                                </div>
                            </div>
							<div class="agree-check">
                                <ul>
                                    <li>
                                        <input type="radio" name="agree" id="agree">
                                        <label for="agree">{{\FontEnd\tblSettingModel::getTitleLang('lang_rules')}}</label>
                                    </li>
                                </ul>
                            </div>
                            <div class="step-info">
                                <div class="title-step">
                                    <h4 class="text"> <strong>{{\FontEnd\tblSettingModel::getTitleLang('lang_payment')}}</strong></h4>
                                </div>
                                <div class="form-step">
                                    <ul>										
										<li>                                        
											<input type="radio" name="p_type" id="onepay1" value="1" checked>       
											<label for="onepay1"><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Đặt vé (Thanh toán COD)</label>        
										</li>
										@if(\Auth::check())
										@if(\Auth::user()->root==1)
                                        <li>
                                            <input type="radio" name="p_type" id="onepay" value="0" >
                                            <label for="onepay"><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Credit Card</label>
                                        </li>
										@endif
										@endif
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-4">
                            <div class="money-check">
                                <h4 class="title-money">{{\FontEnd\tblSettingModel::getTitleLang('lang_total')}}</h4>
                                <p class="money">{{number_format($total)}}<span> VNĐ</span></p>
                            </div>
                            <div class="count-down">
                                <h4 class="title-count">Countdown clock</h4>
                                <div class="bor-count">
                                    <div id="getting-started"></div>

                                </div>
                                <ul>
                                    <li>{{\FontEnd\tblSettingModel::getTitleLang('lang_minutes')}} </li>
                                    <li>{{\FontEnd\tblSettingModel::getTitleLang('lang_seconds')}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="check-seat">
                        <div class="col-md-2">
                            <div class="back-page">
                                <a href="{{URL::route('seat',$show->show_id)}}"><i class="fa fa-step-backward" aria-hidden="true"></i> {{\FontEnd\tblSettingModel::getTitleLang('lang_prev')}}</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="banner-show">
                                 <img src="{{$show->avatar}}" alt="img"/> 
                                <h4 class="title-show">@if(isset($show) && count($show)>0){{$show->name}}@endif</h4>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="last-check">
                                <table>
                                    <tr>
                                        <td>{{\FontEnd\tblSettingModel::getTitleLang('lang_location')}}</td>
                                        <td>@if(isset($place) && count($place)>0){{$place[0]->name}}@endif</td>

                                    </tr>
                                    <tr>
                                        <td>{{\FontEnd\tblSettingModel::getTitleLang('lang_time')}}</td>
                                        <td> @if(isset($show) && count($show)>0){{date('d/m/Y H:i',$show->time_show)}}@endif</td>
                                    </tr>
                                </table>
                                <ul>
                                    <li>
                                        {{\FontEnd\tblSettingModel::getTitleLang('lang_seatnumber')}}
                                    </li>
                                    @if(isset($ticket) && count($ticket)>0)
                                    
                                    @foreach($ticket as $i_ticket)
                                    <li><span>{{$i_ticket->name}}: </span> 
                                        @if(isset($ticket_type) && count($ticket_type)>0)
                                        <?php $i=0; ?>
                                        @foreach($ticket_type as $i_type)
                                        @if($i_type==$i_ticket->ticket_id)
                                        @if(isset($seat_name[$i]))
                                        {{$seat_name[$i]}}
                                        @endif
                                        @endif
                                        <?php $i++; ?>
                                        @endforeach
                                        @endif
                                    </li>
                                    
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="cast-check">
                                <table>
                                    <tr>
                                        <td>{{\FontEnd\tblSettingModel::getTitleLang('lang_total')}}</td>
                                        <td><small class="cast-total">{{number_format($total)}} </small> <span>VNĐ</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="back-page next-page">
                                <a href="javascript:void(0)" onclick="payment()">{{\FontEnd\tblSettingModel::getTitleLang('lang_next')}} <i class="fa fa-step-forward" aria-hidden="true"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
              

@endsection