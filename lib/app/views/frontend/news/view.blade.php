@extends("frontend.hometemplate")

@section("title")
@if(isset($tag))
{{$tag->name}}
@elseif(isset($catenewsname))
<?php if($catenewsname->seotitle!=''){echo $catenewsname->seotitle;}else{echo $catenewsname->name;} ?>
@else
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_title')}}
@endif
@endsection

@section("desc")
@if(isset($tag))
{{$tag->name}}
@elseif(isset($catenewsname))
<?php if($catenewsname->seodesc!=''){echo $catenewsname->seodesc;}else{echo $catenewsname->name;} ?>
@else
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_description')}}
@endif
@endsection

@section("keyword")
@if(isset($tag))
{{$tag->name}}
@elseif(isset($catenewsname))
<?php if($catenewsname->seokeyword!=''){echo $catenewsname->seokeyword;}else{echo $catenewsname->name;} ?>
@else
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}
@endif
@endsection

@section("css")
   <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">
@endsection

@section("js")
    <script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
        
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
        <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>
@endsection

@section("content")
<div class="fix-space"></div>
<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
            <li class="active">
              @if(isset($catenewsname))
              {{$catenewsname->name}}
              @else
              {{\FontEnd\tblSettingModel::getTitleLang('lang_news')}}
              @endif
            </li>
            
        </ol>
    </div>
</div>
<!-- Page Header End -->
<div class="main-wrap">
    <div class="section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <div class="blog-list">
                        @if(isset($data) && count($data)>0)               
                        @foreach($data as $i_data)
                        <article class="post">
                            <a class="post-thumb" href="{{URL::route('route_data',$i_data->news_slug.'dn')}}">
                                <img class="img-responsive" src="{{action('ImageController@getResize')}}?src={{Asset($i_data->images)}}&w=450&h=292" alt="img">
                            </a>
                            <div class="post-content">
                                <h4 class="post-title"><a href="{{URL::route('route_data',$i_data->news_slug.'dn')}}">{{$i_data->news_title}} </a></h4>
                                <p class="post-date">{{date('d M Y',$i_data->time_post)}}</p>
                                <div class="post-excerpt">
                                    <p>{{str_limit($i_data->news_excerpt,350,'...')}}</p>
                                </div>
                            </div>
                        </article>
                        @endforeach
                        <!-- Pagination -->
                        <nav class="navigation post-pagination" role="navigation">
                            @if($data->links()!='')
                            {{$data->links('frontend.pani')}}
                            @endif
                        </nav>
                        <!-- Pagination End -->
                        @else
                        <h3>{{\FontEnd\tblSettingModel::getTitleLang('lang_empty')}}</h3>
                        @endif
                    </div>
                </div>
                <!-- Sidebar -->
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="sidebar">
                        <!-- Search widget -->
                        <aside class="widget search-widget">
                            <form id="widgetSearch" class="searchform" action="{{URL::route('search')}}" method="post">
                              {{Form::token()}}
                                <input type="search" name="keyword" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_keyword')}}">
                                <button type="submit" name="searchsubmit"><i class="fa fa-search"></i></button>
                            </form>
                        </aside>
                        <!-- Category widget -->
                        <aside class="widget category-widget">
                            <h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_category')}}</h4>
                            @if(isset($arrCate) && count($arrCate)>0)
                            <ul class="widget-category">
                                @foreach($arrCate as $i_arrCate)
                                @if($i_arrCate->parent==0)
                                <?php
                                $check_one=false;
                                foreach($arrCate as $check){
                                    if($check->parent==$i_arrCate->id){
                                        $check_one=true;
                                    }
                                }
                                ?>
                                <li @if($check_one==true) data-toggle="collapse" data-target="#subMenu{{$i_arrCate->id}}" @endif>
                                    <a @if($check_one!=true) href="{{URL::route('route_data',$i_arrCate->slug.'cn')}}" @endif>{{$i_arrCate->name}}</a>
                                    @if($check_one==true)
                                    <ul class="list-second collapse" id="subMenu{{$i_arrCate->id}}" >
										@foreach($arrCate as $i_arrCate1)
                                        @if($i_arrCate1->parent == $i_arrCate->id)
                                        <li><a href="{{URL::route('route_data',$i_arrCate1->slug.'cn')}}">{{$i_arrCate1->name}}</a></li>
                                        @endif
                                        @endforeach
                                    </ul>
                                    @endif
                                </li>
                                @endif
                                @endforeach
                            </ul>
                            @endif
                        </aside>
                        <!-- Post widget -->
                        <aside class="widget post-widget">
                            <h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_random')}}</h4>
                            <div class="widget-posts">
                                 @if(isset($random) && count($random)>0)               
                                  @foreach($random as $i_random)
                                <div class="widget-post">
                                    <a class="widget-post-thumb" href="{{URL::route('route_data',$i_random->news_slug.'dn')}}"><img class="img-responsive" src="{{action('ImageController@getResize')}}?src={{Asset($i_random->images)}}&w=70&h=70" alt="blog thumb"></a>
                                    <div class="widget-post-body">
                                        <a class="widget-post-title" title="{{$i_random->news_title}}" href="{{URL::route('route_data',$i_random->news_slug.'dn')}}">{{str_limit($i_random->news_title,25,'...')}}</a>
                                        <span class="widget-post-date">{{date('d M Y',$i_random->time_post)}}</span>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </aside>
                        <!-- Tag widget -->
                        <aside class="widget tag-widget">
                            <h4 class="widget-title">Tag</h4>
                            <div class="widget-tag">
                              @if(isset($tags) && count($tags)>0)               
                                  @foreach($tags as $i_tags)
                                <a href="{{URL::route('route_data',$i_tags->slug.'tn')}}">{{$i_tags->name}}</a>
                                @endforeach
                                @endif                                
                            </div>
                        </aside>
                        <!-- Social Media -->
                        <aside class="widget social-link-widget">
                            <h4 class="widget-title">Follow us in Social</h4>
                            <div class="social-media">
                                <a class="@if(isset($setting->facebook_fanpage)){{$setting->facebook_fanpage}}@endif"><i class="fa fa-facebook"></i></a>
                                <a class="@if(isset($setting->twitter_fanpage)){{$setting->twitter_fanpage}}@endif"><i class="fa fa-twitter"></i></a>
                                <a class="@if(isset($setting->google_fanpage)){{$setting->google_fanpage}}@endif"><i class="fa fa-google-plus"></i></a>
                                <a class="@if(isset($setting->youtube)){{$setting->youtube}}@endif"><i class="fa fa-youtube-play"></i></a>
                                <a class="@if(isset($setting->linkedin_fanpage)){{$setting->linkedin_fanpage}}@endif"><i class="fa fa-linkedin"></i></a>
                                <a class="#"><i class="fa fa-vimeo"></i></a>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection