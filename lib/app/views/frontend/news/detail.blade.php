@extends("frontend.hometemplate")


<?php
if (isset($data)) {
	if($data[0]->seodesc!=''){
		$des_character = strip_tags($data[0]->seodesc);		
	}else if($data[0]->news_excerpt!=''){
		$des_character = strip_tags($data[0]->news_excerpt);
	}else {
		$des_character = "";
	}
	if($data[0]->seotitle!=''){
		$title_character = strip_tags($data[0]->seotitle);
	}else if($data[0]->news_title!=''){
		$title_character = strip_tags($data[0]->news_title);
	}else {
		$title_character = "";
	}
} else {
    $des_character = "";
    $title_character = "";
}
$title_character.='-'.\FontEnd\tblSettingModel::getTitleLang('lang_website_title');
if (strlen($des_character) > 160) {
    $des_page = substr($des_character, 0, 155);
} else {
    $des_page = $des_character;
}

if (strlen($title_character) > 70) {
    $tit_page = substr($title_character, 0, 70);
} else {
    $tit_page = $title_character;
}
?>
@section("title")
<?php echo $tit_page; ?>
@endsection
@section("desc")
<?php echo $des_page; ?>
@endsection

@section("keyword")
@if(isset($data) && $data[0]->seokeyword!='')
{{$data[0]->seokeyword}}
@else
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}
@endif
@endsection

@section("facebooktag")

<meta property="og:url" content="{{Request::url()}}" />

<meta property="og:type" content="article" />

<meta property="og:title" content="<?php if($data[0]->seofbtitle!=''){echo $data[0]->seofbtitle;}else{echo $data[0]->news_title;} ?>" />

<meta property="og:description" content="<?php if($data[0]->seofbdesc!=''){echo $data[0]->seofbdesc;}else{echo $data[0]->news_excerpt;} ?>" />

<meta property="og:image" content="{{Asset($data[0]->images)}}" />

@endsection
@section("css")
   <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">
@endsection

@section("js")
    <script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
        
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
        <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>
@endsection
@section("content")
<div class="fix-space"></div>
<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <ol class="breadcrumb">
             <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
            <li class="active">{{$data[0]->news_title}}</li>
            
        </ol>
    </div>
</div>
<!-- Page Header End -->
<div class="main-wrap">
    <div class="section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <div class="blog-single">
                        <article class="post">
                            <a href="#"><h2 class="title-head">{{$data[0]->news_title}} </h2></a>
                            <div class="post-content">
                                <div class="post-metas">
                                    <p class="post-date post-meta">{{date('d M Y',$data[0]->time_post)}}</p>
                                    <a class="post-meta comment-count" href="#">{{count($comment)}} {{\FontEnd\tblSettingModel::getTitleLang('lang_comment')}}</a>
                                </div>
                                <div class="post-entry">
                                  {{$data[0]->news_content}}
                            </div>
                        </div>
                        <div class="share-post">
                            <label>{{\FontEnd\tblSettingModel::getTitleLang('lang_share')}}</label>
                            <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js"></script>
                            <div class="d_inline_middle m_left_5 addthis_widget_container">
                            <!-- AddThis Button BEGIN -->
                            <div class="addthis_toolbox addthis_default_style addthis_16x16_style">
                                <a class="addthis_button_preferred_1"></a>
                                <a class="addthis_button_preferred_2"></a>
                                <a class="addthis_button_preferred_3"></a>
                                <a class="addthis_button_preferred_4"></a>
                                <a class="addthis_button_compact"></a>
                                <a class="addthis_counter addthis_bubble_style"></a>
                            </div>
                            </div>
                            
                        </div>
                    </article>
                    <!-- Article Content End here -->
                    
                    <!-- Given comments are here -->
                    <div class="comments-section">
                        <p class="lead">{{\FontEnd\tblSettingModel::getTitleLang('lang_comment')}}</p>
                        
                        <?php                        
                        if(isset($comment) && count($comment)>0){
                            \Tree::showComment($comment);
                        }
                        ?>   
                     
                        
                    </div>
                    <!-- Given comments end -->
                    <!-- Comment Form -->
                    <div class="give-your-reply">
                        <div class="commentform-header">
                           
                        </div>
                        <form id="comment-form" class="comment-form" action="{{URL::route('postcomment')}}" method="post">
                            {{Form::token()}}
                            <div class="row">
                                <div class="col-md-6">
                                    <label>{{\FontEnd\tblSettingModel::getTitleLang('lang_name')}} <br/>
                                        <input type="text" name="name" id="name" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_name')}}" required>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>Email <br/>
                                        <input type="text" name="email" id="email" placeholder="Email" required>
                                    </label>
                                </div>
                            </div>
                            <div class="row" style="display:none" id="li_hidden">
                                <div class="col-md-6">
                                    <label>{{\FontEnd\tblSettingModel::getTitleLang('lang_reply')}} <a onclick="hide_div()" style="padding:5px">X</a><br/> 
                                        <input readonly="true" type="text" id="reply_name" name="reply_name" />
                                    </label>
                                </div>
                            </div>
                            <label>{{\FontEnd\tblSettingModel::getTitleLang('lang_content')}}<br/>
                                <textarea name="message" id="comment" rows="5" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_content')}}"></textarea>
                            </label>
                            <input type="hidden" name="parent" id="parent"/>
                            <input type="hidden" name="post_type" id='post_type' value="4"/>
                            <input type="hidden" name="post_id" value="{{$data[0]->id}}"/>
                            <div id="messages_notify">

                        </div>
                            <button type="button" onclick="submitForm('comment-form')" class="btn btn-black">{{\FontEnd\tblSettingModel::getTitleLang('lang_send')}}</button>
                        </form>
                    </div>
                    <!-- Comment Form End -->
                </div>
            </div>
            <!-- Sidebar -->
            <div class="col-md-4 col-sm-5 col-xs-12">
                <div class="sidebar">
                    <!-- Search widget -->
                    <aside class="widget search-widget">
                        <form id="widgetSearch" class="searchform" action="{{URL::route('search')}}" method="post">
                          {{Form::token()}}
                            <input type="search" name="keyword" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_keyword')}}">
                            <button type="submit" name="searchsubmit"><i class="fa fa-search"></i></button>
                        </form>
                    </aside>
                    <!-- Category widget -->
                    <aside class="widget category-widget">
                        <h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_category')}}</h4>
                        @if(isset($arrCate) && count($arrCate)>0)
                        <ul class="widget-category">
                            @foreach($arrCate as $i_arrCate)
                            @if($i_arrCate->parent==0)
                            <?php
                            $check_one=false;
                            foreach($arrCate as $check){
                                if($check->parent==$i_arrCate->id){
                                    $check_one=true;
                                }
                            }
                            ?>
                            <li @if($check_one==true) data-toggle="collapse" data-target="#subMenu{{$i_arrCate->id}}" @endif>
                                <a @if($check_one!=true) href="{{URL::route('route_data',$i_arrCate->slug.'cn')}}" @endif>{{$i_arrCate->name}}</a>
                                @if($check_one==true)
                                <ul class="list-second collapse" id="subMenu{{$i_arrCate->id}}" >
                                   @foreach($arrCate as $i_arrCate1)
                                    @if($i_arrCate1->parent == $i_arrCate->id)
                                    <li><a href="{{URL::route('route_data',$i_arrCate1->slug.'cn')}}">{{$i_arrCate1->name}}</a></li>
                                    @endif
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @endif
                            @endforeach
                        </ul>
                        @endif
                    </aside>
                    <!-- Post widget -->
                    <aside class="widget post-widget">
                        <h4 class="widget-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_random')}}</h4>
                        <div class="widget-posts">
                             @if(isset($random) && count($random)>0)               
                              @foreach($random as $i_random)
                            <div class="widget-post">
                                <a class="widget-post-thumb" href="{{URL::route('route_data',$i_random->news_slug.'dn')}}"><img class="img-responsive" src="{{action('ImageController@getResize')}}?src={{Asset($i_random->images)}}&w=70&h=70" alt="blog thumb"></a>
                                <div class="widget-post-body">
                                    <a class="widget-post-title" title="{{$i_random->news_title}}" href="{{URL::route('route_data',$i_random->news_slug.'dn')}}">{{str_limit($i_random->news_title,25,'...')}}</a>
                                    <span class="widget-post-date">{{date('d M Y',$i_random->time_post)}}</span>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </aside>
                    <!-- Tag widget -->
                    <aside class="widget tag-widget">
                        <h4 class="widget-title">Tag</h4>
                        <div class="widget-tag">
                          @if(isset($tags) && count($tags)>0)               
                              @foreach($tags as $i_tags)
                            <a href="{{URL::route('route_data',$i_tags->slug.'tn')}}">{{$i_tags->name}}</a>
                            @endforeach
                            @endif                                
                        </div>
                    </aside>
                    <!-- Social Media -->
                    <aside class="widget social-link-widget">
                        <h4 class="widget-title">Follow us in Social</h4>
                        <div class="social-media">
                            <a class="@if(isset($setting->facebook_fanpage)){{$setting->facebook_fanpage}}@endif"><i class="fa fa-facebook"></i></a>
                            <a class="@if(isset($setting->twitter_fanpage)){{$setting->twitter_fanpage}}@endif"><i class="fa fa-twitter"></i></a>
                            <a class="@if(isset($setting->google_fanpage)){{$setting->google_fanpage}}@endif"><i class="fa fa-google-plus"></i></a>
                            <a class="@if(isset($setting->youtube)){{$setting->youtube}}@endif"><i class="fa fa-youtube-play"></i></a>
                            <a class="@if(isset($setting->linkedin_fanpage)){{$setting->linkedin_fanpage}}@endif"><i class="fa fa-linkedin"></i></a>
                            <a class="#"><i class="fa fa-vimeo"></i></a>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection