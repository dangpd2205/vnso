@extends("frontend.hometemplate")
@section("title")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_title')}}
@endsection
@section("desc")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_description')}}
@endsection
@section("keyword")
{{\FontEnd\tblSettingModel::getTitleLang('lang_website_keyword')}}
@endsection

@section("css")
   <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
        <link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">
@endsection

@section("js")
    <script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
        
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
        <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>
@endsection

@section("content")
<div class="fix-space"></div>
        
<!-- Footer Subscribe -->
<!-- Page Header -->
<div class="page-header">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{Asset('')}}">{{\FontEnd\tblSettingModel::getTitleLang('lang_home')}}</a></li>
            <li class="active">{{\FontEnd\tblSettingModel::getTitleLang('lang_contact')}}</li>
        </ol>
    </div>
</div>
<!-- Page Header End -->
<div class="main-wrap">
    <!-- Contact Information -->
    <div class="section">
        <div class="fluid-container">
            <div class="fluid-col-1 contact-info-item">
                <h3 class="contact-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_address')}}</h3>
                
                <p class="contact-info"><i class="fa fa-map-marker" aria-hidden="true"></i> {{\tblSettingModel::getTitleLang('lang_addr')}} </p>
            </div>
            <div class="fluid-col-1 contact-info-item">
                <h3 class="contact-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_phone')}}</h3>
                
                <p class="contact-info"><i class="fa fa-phone"></i>@if(isset($setting->contact_phone)){{$setting->contact_phone}}@endif </p>
                <p class="contact-info"><i class="fa fa-fax" aria-hidden="true"></i> @if(isset($setting->contact_fax)){{$setting->contact_fax}}@endif </p>
            </div>
            <div class="fluid-col-1 contact-info-item">
                <h3 class="contact-title">Email</h3>
                
                <p class="contact-info"><i class="fa fa-envelope-o"></i> @if(isset($setting->contact_email)){{$setting->contact_email}}@endif</p>
            </div>
        </div>
    </div>
    <!-- Contact Information End -->
    <!-- Contact form and map -->
    <div class="section form-map-section">
        <div class="fluid-container">
            <div class="fluid-col-2 contact-form-wrap">
                <div class="overlay-black">
                    <div class="section-header">
                        <h3 class="section-title">{{\FontEnd\tblSettingModel::getTitleLang('lang_contact')}}</h3>
                        
                    </div>
                    <form id="contactForm" class="contact-form" action="{{URL::route('postcontact')}}" method="post">
                    	{{Form::token()}}
                        <p>
                            <input type="text" name="name" id="name" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_name')}}" required>
                        </p>
                        <p>
                            <input type="email" name="email" id="email" placeholder="Email" required>
                        </p>
                        <p>
                            <input type="text" name="subject" id="subject" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_subject')}}" required>
                        </p>
                        <p>
                            <textarea rows="5" name="message" id="message" placeholder="{{\FontEnd\tblSettingModel::getTitleLang('lang_content')}}" required></textarea>
                        </p>
                        <div id="messages_contact_notify"></div>
                        <p>
                            <button type="button" class="btn btn-black" onclick="submitForm('contactForm')">{{\FontEnd\tblSettingModel::getTitleLang('lang_send')}}</button>
                        </p>
                      
                    </form>
                </div>
            </div>
            <div class="fluid-col-2 map-area">
                <iframe src="https://www.google.com/maps/embed?pb=@if(isset($data_setting->website_map)){{$data_setting->website_map}}@endif"  frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <!-- Contact form and map end -->
</div>

@endsection

  