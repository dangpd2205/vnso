@section("title")
Danh mục
@endsection
@section("description")
Danh mục
@endsection
@section("modal_option")
@include('admin.artist.add_modal')
@include('admin.template.alert_ajax')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Danh mục nghệ sỹ
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")


    {{Form::open(array('action' => '\ADMIN\ArtistController@postEditCate','id'=>'edit_news','class'=>'form-horizontal'))}}
    
    {{Form::hidden('id_cate', $dataedit->id)}}
    {{Form::hidden('lang_id', $dataedit->lang_id)}}
<div class="row">
	
    <div class="col-md-12"> 
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#content-tab1" data-toggle="tab"><i class="fa fa-globe fa-lg text-default"></i>  Nội dung chính</a>
            </li>
			<li><a href="#seo" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> SEO</a>
            </li>
        </ul>
		@include('admin.template.error')
        <div class="tab-content">
            <div class="tab-pane fade  active in" id="content-tab1">
					<div class="form-group">
						<label class="col-lg-2 control-label">Tên</label>
						<div class="col-lg-6">
							{{Form::text('cate_name', isset($dataedit->name) ? $dataedit->name : '', array('class'=>'bg-focus form-control', 'id'=>'cate_name'))}}
						</div> 
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Mô tả</label>
						<div class="col-lg-6">
							{{Form::textarea('cate_des',isset($dataedit->description) ? $dataedit->description : '', ['class'=>'bg-focus form-control tinymce', 'id'=>'cate_des'])}}
						</div> 
					</div>
			
            </div>
			<div class="tab-pane fade" id="seo">
				<input type="hidden" name="seo_id" value="@if(isset($seo) && count($seo)>0){{$dataedit->seo_id}}@endif"/>
				<div class="panel">
					<div class="panel-body">
						<div class="form-horizontal">
							<div class="form-group"> 
								<label class="col-lg-2 control-label" for="branches_id">Meta Robots Index</label> 
								<div class="col-lg-7">
									<label class="mt-radio">
										
										@if(isset($seo->robots_index) && $seo->robots_index=='index')
										{{Form::radio('seo_index','index', 1)}}
										Index
										@else
										{{Form::radio('seo_index','index',0)}}
										Index
										@endif	
										<span></span>
									</label>
									<label class="mt-radio">
										@if(isset($seo->robots_index) && $seo->robots_index=='noindex')
										{{Form::radio('seo_index','noindex', 1)}}
										Noindex
										@else
										{{Form::radio('seo_index','noindex',0)}}
										Noindex
										@endif	
										<span></span>
									</label>
								</div>
							</div>
							 <div class="form-group"> 
								<label class="col-lg-2 control-label" for="branches_id">Meta Robots Follow</label> 
								<div class="col-lg-7">
									<label class="mt-radio">
										@if(isset($seo->robots_follow) && $seo->robots_follow=='Follow')
										{{Form::radio('seo_follow','Follow', 1)}}
										Nofollow
										@else
										{{Form::radio('seo_follow','Follow',0)}}
										Nofollow
										@endif
										<span></span>
									</label>
									<label class="mt-radio">
										@if(isset($seo->robots_follow) && $seo->robots_follow=='Nofollow')
										{{Form::radio('seo_follow','Nofollow', 1)}}
										Nofollow
										@else
										{{Form::radio('seo_follow','Nofollow',0)}}
										Nofollow
										@endif
										
										<span></span>
									</label>
								</div>
							</div>
							<div class="form-group"> 
								<label class="col-lg-2 control-label" for="branches_id">Meta Robots Advance</label> 
								<div class="col-lg-9">
									<?php
									if (isset($seo->robots_advanced) && in_array('NO ODP', explode(',', $seo->robots_advanced))) {
										$noodp = true;
									} else {
										$noodp = false;
									}
									if (isset($seo->robots_advanced) && in_array('NO YDIR', explode(',', $seo->robots_advanced))) {
										$noydir = true;
									} else {
										$noydir = false;
									}
									if (isset($seo->robots_advanced) && in_array('NO Image Index', explode(',', $seo->robots_advanced))) {
										$noimageindex = true;
									} else {
										$noimageindex = false;
									}
									if (isset($seo->robots_advanced) && in_array('NO Archive', explode(',', $seo->robots_advanced))) {
										$noarchive = true;
									} else {
										$noarchive = false;
									}
									if (isset($seo->robots_advanced) && in_array('NO Snippet', explode(',', $seo->robots_advanced))) {
										$nonippet = true;
									} else {
										$nonippet = false;
									}
									?>      
									<label class="mt-checkbox">
										<?php echo Form::checkbox('seo_advanced[]', 'NO ODP', $noodp); ?> NO ODP
										<span></span>
									</label>
									<label class="mt-checkbox">
										<?php echo Form::checkbox('seo_advanced[]', 'NO YDIR', $noydir); ?> NO YDIR
										<span></span>
									</label>
									<label class="mt-checkbox">
										<?php echo Form::checkbox('seo_advanced[]', 'NO Image Index', $noimageindex); ?> NO Image Index
										<span></span>
									</label>
									<label class="mt-checkbox">
										<?php echo Form::checkbox('seo_advanced[]', 'NO Archive', $noarchive); ?> NO Archive
										<span></span>
									</label>
									<label class="mt-checkbox">
										<?php echo Form::checkbox('seo_advanced[]', 'NO Snippet', $nonippet); ?> NO Snippet
										<span></span>
									</label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">SEO Keyword</label>
								<div class="col-lg-5">
									<?php 
									$keyword='';
									if(count($seo)>0 && isset($seo->keyword)){
										$keyword=$seo->keyword;
									}
									?>
									{{Form::text('seo_keyword', $keyword, array('class'=>'bg-focus form-control tags_input input-sm', 'id'=>'seo_keyword'))}}
								</div> 
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">SEO Title</label>
								<div class="col-lg-9">
									<?php 
									$title='';
									if(count($seo)>0 && isset($seo->title)){
										$title=$seo->title;
									}
									?>
									{{Form::text('seo_title', $title, array('class'=>'bg-focus form-control input-sm seo_title_keyup', 'id'=>'seo_title'))}}
									<small class="notify_char"></small>
								</div> 
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Meta Description</label>
								<div class="col-lg-9">
									<?php 
									$description='';
									if(count($seo)>0 && isset($seo->description)){
										$description=$seo->description;
									}
									?>
									{{Form::textarea('seo_description',$description,['id'=>'seo_description','class'=>'bg-focus form-control seo_desc_keyup','rows'=>"2"])}}
									<small class="notify_char"></small>
								</div> 
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Facebook Title</label>
								<div class="col-lg-9">
									<?php 
									$fb_title='';
									if(count($seo)>0 && isset($seo->fb_title)){
										$fb_title=$seo->fb_title;
									}
									?>
									{{Form::text('seo_f_title', $fb_title, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_f_title'))}}
								</div> 
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Facebook Description</label>
								<div class="col-lg-9">
									<?php 
									$fb_description='';
									if(count($seo)>0 && isset($seo->fb_description)){
										$fb_description=$seo->fb_description;
									}
									?>
									{{Form::textarea('seo_f_description',$fb_description,['id'=>'seo_f_description' ,'class'=>'bg-focus form-control input-sm','rows'=>"2"])}}
								</div> 
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Facebook Image</label>
								<div class="col-lg-9">
									<div class="input-group">
										<?php 
										$fb_image='';
										if(count($seo)>0 && isset($seo->fb_image)){
											$fb_image=$seo->fb_image;
										}
										?>
										{{Form::text('seo_f_image', $fb_image, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_f_image'))}}
										<div class="input-group-btn"> <button class="btn btn-white select_image input-sm"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=seo_f_image"> Thêm ảnh </button></div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Google+ Title</label>
								<div class="col-lg-9">
									<?php 
									$g_title_='';
									if(count($seo)>0 && isset($seo->g_title)){
										$g_title_=$seo->g_title;
									}
									?>
									{{Form::text('seo_g_title', $g_title_, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_g_title'))}}
								</div> 
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Google+ Description</label>
								<div class="col-lg-9">
									<?php 
									$g_des='';
									if(count($seo)>0 && isset($seo->g_description)){
										$g_des=$seo->g_description;
									}
									?>
									{{Form::textarea('seo_g_description',$g_des,['id'=>'seo_g_description','class'=>'bg-focus form-control ','rows'=>"2"])}}
								</div> 
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Google+ Image</label>
								<div class="col-lg-9">
									<div class="input-group">
										<?php 
										$g_image='';
										if(count($seo)>0 && isset($seo->g_image)){
											$g_image=$seo->g_image;
										}
										?>
										{{Form::text('seo_g_image', $g_image, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_g_image'))}}
										<div class="input-group-btn"> <button class="btn btn-white select_image input-sm" data-toggle="dropdown" data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=seo_g_image"> Thêm ảnh </button></div>
									</div>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
   
</div>
 {{Form::close()}}
<style>
    .popover .arrow{
        display: none;
    }
</style>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#edit_news').submit()" target="_blank" class="active">
                <span>Cập nhật</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection