@section("title")
PUBWEB.VN
@endsection
@section("description")
@endsection
@section("modal_option")

@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Danh mục nghệ sỹ
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
@include('admin.modal.popup')
@include('admin.template.alert_ajax')
<div class="row">
    <div class="col-md-12">  
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database"></i>Dữ liệu</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <!--                    <a href="javascript:;" class="reload" data-url="/hahahah" data-error-display="toastr"  data-error-display-text="FUCK YOU" data-original-title="WHAT THE FUCK" title=""> </a>-->
                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
             
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <?php
                            foreach ($l_lang as $item) {
                                if ($item->id == $g_config_all->website_lang) {
                                    ?>
                                    <a href="#lang_p_view_cat_{{$item->id}}" data-toggle="tab" data-lang="{{$item->id}}"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                    <?php
                                }
                            }
                            ?>
                        </li>
                        <?php if (count($l_lang) > 1) { ?>
                            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                <ul class="dropdown-menu text-left">
                                    <?php
                                    foreach ($l_lang as $item_child) {
                                        if ($item_child->id != $g_config_all->website_lang) {
                                            ?>
                                            <li> <a href="#lang_p_view_cat_{{$item_child->id}}" data-toggle="tab" data-lang="{{$item_child->id}}">{{$item_child->name}}</a> </li>
                                            <?php
                                        }
                                    }
                                    ?>
                            </li>
                        </ul>
                        </li>
                    <?php } ?>
                    </ul>
                    <div class="panel-body">
                        <div class="tab-content">
                            <?php
                            foreach ($l_lang as $item_content) {
                                ?>
                                <div class="tab-pane fade in <?php
                            $tran_class = '';
                            if ($item_content->id == $g_config_all->website_lang) {
                                $tran_class = 'translate_google';
                                echo ' active';
                            }
                                ?> " id="lang_p_view_cat_{{$item_content->id}}">
                                    @include('admin.artist.category.add_category_ajax')
                                </div>
                                <?php
                            }
                            ?>


                        </div>
                    </div>
               
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    /**
     * Nestable
     */

    .dd { position: relative; display: block; margin: 0; padding: 0; list-style: none; font-size: 13px; line-height: 20px; }

    .dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
    .dd-list .dd-list { padding-left: 30px; }
    .dd-collapsed .dd-list { display: none; }

    .dd-item,
    .dd-empty,
    .dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }

    .dd-handle { display: block;
                 height: 30px;
                 margin: 5px 0; 
                 padding: 4px 10px; 
                 color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
                 background: #F5F6F7;
                 -webkit-border-radius: 3px;
                 border-radius: 3px;
                 box-sizing: border-box; -moz-box-sizing: border-box;
    }


    .dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
    .dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
    .dd-item > button[data-action="collapse"]:before { content: '-'; }

    .dd-placeholder,
    .dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
    .dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
                background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                    -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
                background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
                background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                    linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
                background-size: 60px 60px;
                background-position: 0 0, 30px 30px;
    }

    .dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
    .dd-dragel > .dd-item .dd-handle { margin-top: 0; }
    .dd-dragel .dd-handle {
        -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
        box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
    }

    .action_menu .fa:first-child {
        margin-right: 5px;
    }
    .action_menu {
        border-left: 1px solid #ccc;
        float: right;
        width: 80px;
        padding-left: 20px;
    }
    .link_menu {
        border-left: 1px solid #ccc;
        float: right;
        padding: 0 5px 0px 15px;
        white-space: nowrap;
        width: 500px;
        overflow: hidden;
        font-weight: normal;
    }
    .thVal{
        height: 20px !important;
    }
    @media (max-width: 768px){

        .link_menu {
            display: none;
        }  
    }
    /**
* Nestable Draggable Handles
*/

    .dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
                   background: #fafafa;
                   background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
                   background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
                   background:         linear-gradient(top, #fafafa 0%, #eee 100%);
                   -webkit-border-radius: 3px;
                   border-radius: 3px;
                   box-sizing: border-box; -moz-box-sizing: border-box;
    }
    .dd3-content:hover {background: #fff; }

    .dd-dragel > .dd3-item > .dd3-content { margin: 0; }

    .dd3-item > button { margin-left: 30px; }

    .dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
                  border: 1px solid #1DB399;
                  background: #23d4b5;
                  border-top-right-radius: 0;
                  border-bottom-right-radius: 0;
    }
    .dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
    .dd3-handle:hover { background: #23d4b5; }
</style>

<script>
    var url_tran = "{{action('\ADMIN\TranslateGoogleController@postTranslateGoogle')}}";
    var json_lang_string = '{{json_encode($l_lang)}}';
    var url_delete = "{{action('\ADMIN\ArtistController@postDeleteCategory')}}";
    var url_update_position = "{{action('\ADMIN\ArtistController@postUpdatePosCategory')}}";
</script>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="{{URL::action('\ADMIN\ArtistController@getAddCate')}}" class="active">
                <span>Thêm mới</span>
                <i class="icon-plus"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection
