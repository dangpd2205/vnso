@section("title")
Danh mục
@endsection
@section("description")
Danh mục
@endsection
@section("modal_option")
@include('admin.artist.add_modal')
@include('admin.template.alert_ajax')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Danh mục nghệ sỹ
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")



    {{Form::open(array('action'=>'\ADMIN\ArtistController@postAddCate', 'class'=>'form-horizontal', 'id'=>'form_add_news'))}}
<div class="row">
	
    <div class="col-md-12"> 
		@include('admin.template.error')
		<input type="hidden" name="type_status" id="type_status"/>
        <ul class="nav nav-tabs ">
            <li class="active"><a href="#main-content" data-toggle="tab"><i class="fa fa-globe fa-lg text-default"></i> Nội dung chính</a>
            </li>
			<li><a href="#seo" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> SEO</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="main-content">
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">

                                <?php
                                foreach ($l_lang as $item) {
                                    if ($item->id == $g_config_all->website_lang) {
                                        ?>
                                        <a href="#lang_news_view_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                        <?php
                                    }
                                }
                                ?>

                            </li>
                            <?php if (count($l_lang) > 1) { ?>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                    <ul class="dropdown-menu text-left">
                                        <?php
                                        foreach ($l_lang as $item_child) {
                                            if ($item_child->id != $g_config_all->website_lang) {
                                                ?>
                                                <li> <a href="#lang_news_view_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>   
                        <?php } ?>
                        </ul>  
                    
                        <div class="panel-body">

							<div class="tab-content">
								<?php
								$active = '';
								$tran_class = '';
								foreach ($l_lang as $item_content) {
									if ($item_content->id == $g_config_all->website_lang) {
										$tran_class = 'translate_google';
										$active='active';
									}else{
										$active='';
									}
								?>
								<div class="tab-pane fade {{$active}} in" id="lang_news_view_{{$item_content->id}}">

									<div class="panel-body">
										<div class="form-horizontal">
											<h2 class="cus-align">{{$item_content->name}}</h2>
										</div>
										<div class="form-horizontal">
											<div class="form-group">
												<label class="col-lg-2 control-label">Tên nhóm</label>
												<div class="col-lg-6">
													<input type="text" class="bg-focus form-control" name="cate_name_{{$item_content->code}}" id="cate_name_{{$item_content->code}}"/>
													
												</div> 
											</div>
											<div class="form-group">
												<label class="col-lg-2 control-label" for="news_add_cat_parent_{{$item_content->code}}">Cha</label>
												<div class="col-lg-6">
													<select id="news_add_cat_parent_{{$item_content->code}}" class="bg-focus form-control" name="news_add_cat_parent_{{$item_content->code}}">
														<option value="">None</option>                                              
														<?php
														foreach ($all_category as $item_cat) {
															if ($item_cat->lang_id == $item_content->id) {
																if ($item_cat->parent == 0) {
																	?>
																	<option value="<?php echo $item_cat->id ?>"><?php echo $item_cat->name ?></option>
																	<?php
																	foreach ($all_category as $item_cat1) {
																		if ($item_cat1->parent == $item_cat->id) {
																			echo '<option value="' . $item_cat1->id . '"> — ' . $item_cat1->name . '</option>';
		//                                                                  
																		}
																	}
																}
															}
														}
														?>
													</select>
												</div> 
											</div>
											<div class="form-group">
												<label class="col-lg-2 control-label" for="news_add_cat_description_{{$item_content->code}}">Mô tả</label>
												<div class="col-lg-9">
													{{Form::textarea('news_add_cat_description_'.$item_content->code, null, array('class'=>'bg-focus form-control tinymce', 'id'=>'news_add_cat_description_'.$item_content->code))}}
												</div> 
											</div>
										</div>
									</div>

								</div>
								<?php }?>
							</div> 

						</div>
					</div>
				</div>
            </div>
            <div class="tab-pane fade" id="seo">
                <div class="panel">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group"> 
								<label class="col-lg-2 control-label" for="branches_id">Meta Robots Index</label> 
								<div class="col-lg-7">
									<label class="mt-radio">
										{{Form::radio('seo_index','index', 1)}}
										Index
										<span></span>
									</label>
									<label class="mt-radio">
										{{Form::radio('seo_index','noindex',0)}} Noindex
										<span></span>
									</label>
								</div>
							</div>
							<div class="form-group"> 
								<label class="col-lg-2 control-label" for="branches_id">Meta Robots Follow</label> 
								<div class="col-lg-7">
									<label class="mt-radio">
										{{Form::radio('seo_follow','Follow', 1)}}
										Follow
										<span></span>
									</label>
									<label class="mt-radio">
										{{Form::radio('seo_follow','Nofollow',0)}} Nofollow
										<span></span>
									</label>
								</div>
							</div>
							<div class="form-group"> 
								<label class="col-lg-2 control-label" for="branches_id">Meta Robots Advance</label> 
								<div class="col-lg-9">
									<label class="mt-checkbox">
										<input type="checkbox" name="seo_advanced[]" value="NO ODP"/> NO ODP
										<span></span>
									</label>
									<label class="mt-checkbox">
										<input type="checkbox" name="seo_advanced[]" value="NO YDIR"/> NO YDIR
										<span></span>
									</label>
									<label class="mt-checkbox">
										<input type="checkbox" name="seo_advanced[]" value="NO Image Index"/> NO Image Index
										<span></span>
									</label>
									<label class="mt-checkbox">
										<input type="checkbox" name="seo_advanced[]" value="NO Archive"/> NO Archive
										<span></span>
									</label>
									<label class="mt-checkbox">
										<input type="checkbox" name="seo_advanced[]" value="NO Snippet"/> NO Snippet
										<span></span>
									</label>
								</div>
							</div>
							<div class="portlet box green-meadow">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
									<div class="tools">
										<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
									</div>
								</div>
								<div class="portlet-body">
									<ul class="nav nav-tabs">
										<li class="active">

											<?php
											foreach ($l_lang as $item) {
												if ($item->id == $g_config_all->website_lang) {
													?>
													<a href="#lang_p_seo_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
													<?php
												}
											}
											?>

										</li>
										<?php if (count($l_lang) > 1) { ?>
											<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
												<ul class="dropdown-menu text-left">
													<?php
													foreach ($l_lang as $item_child) {
														if ($item_child->id != $g_config_all->website_lang) {
															?>
															<li> <a href="#lang_p_seo_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
															<?php
														}
													}
													?>
												</ul>
											</li>
										<?php } ?>
									</ul>
									<div class="panel-body">
										<div class="tab-content">
											<?php
											foreach ($l_lang as $item_content) {
												?>
												<div class="tab-pane fade in <?php
												$tran_class = '';
												if ($item_content->id == $g_config_all->website_lang) {
													echo ' active';
													$tran_class = 'translate_google';
												}
												?> " id="lang_p_seo_{{$item_content->id}}">
													<h3>{{$item_content->name}}</h3>

													<div class="form-group">
														<label class="col-lg-2 control-label" for="seo_keyword_{{$item_content->code}}">SEO Keyword</label>
														<div class="col-lg-5">
															{{Form::text('seo_keyword_'.$item_content->code, null, array('class'=>'bg-focus form-control tags_input input-sm', 'id'=>'seo_keyword_'.$item_content->code))}}
														</div> 
													</div>
													<div class="form-group">
														<label class="col-lg-2 control-label" for="seo_title_{{$item_content->code}}">SEO Title</label>
														<div class="col-lg-9">
															{{Form::text('seo_title_'.$item_content->code, null, array('class'=>'seo_title_keyup bg-focus form-control input-sm', 'id'=>'seo_title_'.$item_content->code))}}
															<small class="notify_char"></small>
														</div> 
													</div>
													<div class="form-group">
														<label class="col-lg-2 control-label" for="seo_description_{{$item_content->code}}">Meta Description</label>
														<div class="col-lg-9">
															{{Form::textarea('seo_description_'.$item_content->code,'',['id'=>'seo_description_'.$item_content->code,'class'=>'bg-focus form-control seo_desc_keyup','rows'=>"2"])}}
															<small class="notify_char"></small>
														</div> 
													</div>
													<div class="form-group">
														<label class="col-lg-2 control-label" for="seo_f_title_{{$item_content->code}}">Facebook Title</label>
														<div class="col-lg-9">
															{{Form::text('seo_f_title_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_f_title_'.$item_content->code))}}
														</div> 
													</div>
													<div class="form-group">
														<label class="col-lg-2 control-label" for="seo_f_description_{{$item_content->code}}">Facebook Description</label>
														<div class="col-lg-9">
															{{Form::textarea('seo_f_description_'.$item_content->code,'',['id'=>'seo_f_description_'.$item_content->code ,'class'=>'bg-focus form-control input-sm','rows'=>"2"])}}
														</div> 
													</div>
													<div class="form-group">
														<label class="col-lg-2 control-label" for="seo_f_image_{{$item_content->code}}">Facebook Image</label>
														<div class="col-lg-9">
															<div class="input-group">
																{{Form::text('seo_f_image_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_f_image_'.$item_content->code))}}
																<div class="input-group-btn"> <button class="btn btn-white select_image input-sm"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=seo_f_image_{{$item_content->code}}"> Thêm ảnh </button></div>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-lg-2 control-label" for="seo_g_title_{{$item_content->code}}">Google+ Title</label>
														<div class="col-lg-9">
															{{Form::text('seo_g_title_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_g_title_'.$item_content->code))}}
														</div> 
													</div>
													<div class="form-group">
														<label class="col-lg-2 control-label" for="seo_g_description_{{$item_content->code}}">Google+ Description</label>
														<div class="col-lg-9">
															{{Form::textarea('seo_g_description_'.$item_content->code,'',['id'=>'seo_g_description_'.$item_content->code,'class'=>'bg-focus form-control ','rows'=>"2"])}}
														</div> 
													</div>
													<div class="form-group">
														<label class="col-lg-2 control-label" for="seo_g_image_{{$item_content->code}}">Google+ Image</label>
														<div class="col-lg-9">
															<div class="input-group">
																{{Form::text('seo_g_image_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_g_image_'.$item_content->code))}}
																<div class="input-group-btn"> <button class="btn btn-white select_image input-sm" data-toggle="dropdown" data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=seo_g_image_{{$item_content->code}}"> Thêm ảnh </button></div>
															</div>
														</div>
													</div>
												</div>
												<?php
											}
											?>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>            
    </div>
</div>
    {{Form::close()}}
<style>
    .popover .arrow{
        display: none;
    }
</style>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#form_add_news').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-plus"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection