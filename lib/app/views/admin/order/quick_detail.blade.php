<td colspan="8">
    <section class="panel" style="margin-bottom: 0px;">
       
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tabs-des-{{$order_current->id}}-2" data-toggle="tab"><i class="fa fa-user fa-lg text-default"></i> Thông tin đơn vé </a></li>
            </ul> 
        
        <div class="panel-body" style="padding: 0px;">
            <div class="tab-content">
                <div class="tab-pane fade active in" id="tabs-des-{{$order_current->id}}-2">
                    <div class="panel">
                        @if(isset($show) && count($show)>0)
                    <label class="col-lg-6 control-label">Tên chương trình: {{$show->name}}</label>
                    <label class="col-lg-6 control-label">Thời gian: {{date('d/m/Y H:i',$show->time_show)}}</label>
                    <label class="col-lg-6 control-label">Địa điểm: {{$show->place_name}}</label>
                    @endif
                    </div>
                    <table class="table table-striped m-b-none text-small" style="table-layout: fixed;">
                        <thead>
                            <tr>
                                <th>Tên ghế</th>
                                <th>Loại ghế</th>
                                <th>Giá tiền</th>
                            </tr>
                        </thead>
                    </table>
                    <div class="scroll-y" style="max-height: 250px;">
                        <table class="table table-striped m-b-none text-small" style="table-layout: fixed;">
                            <tbody>
                                <?php
                                $total_amount=0;
                                if (count($order_detail_current) == 0) {
                                    ?>
                                    <tr>    <td colspan="3">No data</td>  </tr>
                                    <?php
                                } else {
                                    foreach ($order_detail_current as $item) {
                                        $total_amount+=$item->seat_price;
                                        ?>
                                        <tr>   
                                            <td>
                                                {{$item->seat_name}} ({{$item->seat_name_orin}})
                                            </td>
                                            <td>
                                                {{$item->name}}
                                            </td>
                                            <td>{{number_format($item->seat_price)}} VNĐ</td>
                                            
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot class="bg-primary">
                                <tr> 
                                    <td colspan="2"  class="text-left"><strong>Tổng số: {{count($order_detail_current)}}</strong></td>
                                    <td class="text-left"><strong>Tổng tiền: {{number_format($total_amount)}} VNĐ</strong></td>
                                    
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</td>