<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>In vé</title>
	 <link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.carousel.css">
	<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/owl.transitions.css">
	<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/magnific-popup.css">
	<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/apps.css">
	<link rel="stylesheet" href="{{Asset('asset')}}/frontend/assets/css/plyr.css">
	<link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pignose.calendar.css">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/style.css">
	<link rel="stylesheet" href="{{Asset('asset')}}/frontend/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="{{Asset('asset')}}/frontend/css/pubweb.css">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">
	<script src="{{Asset('asset')}}/frontend/assets/js/bootstrap.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/owl.carousel.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.magnific-popup.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/plyr.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.ajaxchimp.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/isotope.pkgd.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.countdown.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/tether.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/jquery.slimscroll.min.js"></script>
        <script src="{{Asset('asset')}}/frontend/assets/js/amplitude.js"></script>
        
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/moment.latest.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pignose.calendar.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/parallax.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/YouTubePopUp.jquery.js"></script>
        
        <script src="{{Asset('asset')}}/frontend/js/custom.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/jquery.countdown.min.js"></script>
        <script type="text/javascript" src="{{Asset('asset')}}/frontend/js/pubweb.js"></script>
</head>

<body>
	<div class="main-wrap">
        <div class="section section-padding book-seat book-info">
            <div class="container">
            
                <div class="row">
                    <div class="col-md-12">
                        <div class="tks">
                            @if(isset($show) && count($show)>0)
                            <div class="last-time">
                                <div class="bor-time">
                                    <div class="info-left">
										<ul>
											<li> {{\FontEnd\tblSettingModel::getTitleLang('lang_order_code')}}  <span>{{$data_order->code}}</span></li>
                                            <li>{{date('d/m/Y',$show->time_show)}} &nbsp&nbsp&nbsp {{date('H:i',$show->time_show)}}</li>
                                            <li>{{$show->place_name}}</li>
                                            <?php
                                            $total=0;
                                            foreach($data_detail as $item){
                                                $total+=$item->seat_price;
                                            }
                                            ?>
                                            
                                            <li><span>{{number_format($total)}} VNĐ</span></li>
										</ul>
									</div>
									<div class="info-right">
										<div class="up-info">
                                            <h4 class="text-1">{{\FontEnd\tblSettingModel::getTitleLang('lang_show')}} </h4>
                                            <h5 class="text-2">{{$show->name}}</h5>
                                        </div>
                                        <div class="down-info">
                                            <h4 class="text-seat">
                                                {{\FontEnd\tblSettingModel::getTitleLang('lang_seatnumber')}} 
                                                @if(isset($ticket) && count($ticket)>0)
                                                @foreach($ticket as $i_ticket)
                                                <li>{{$i_ticket->name}}: 
                                                    @foreach($data_detail as $item)
                                                    @if($item->seat_type==$i_ticket->ticket_id)
                                                    <span>{{$item->seat_name}}({{$item->seat_name_orin}})</span> &nbsp;
                                                    @endif
                                                    @endforeach
                                                </li> 
                                                @endforeach
                                                @endif
                                            </h4>
                                        </div>
									</div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</body>

</html>