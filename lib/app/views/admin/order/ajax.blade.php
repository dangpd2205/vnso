<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
				<th>Mã đơn vé</th>
                <th>E-mail</th>
                <th>Tên</th>                
                <th>Số điện thoại</th>				<th>Địa chỉ</th>
				<th>Hình thức</th>
                <th>Thời gian</th>
				<th>Trạng thái</th>
                <th style="width: 10%;">Xóa</th>
            </tr>
        </thead>
        <tbody id="tbl_content_ajax">

            @if(count($data)==0)
            <tr>
                <td colspan="9">NO DATA</td>
            </tr>
            @endif
            <?php $i=0; ?>
            <?php foreach ($data as $item): ?>
                <tr>
					<td>
						<a href="javascript:void(0)" data-url-post="{{action('\ADMIN\OrderController@postQuickView')}}" data-current-id="{{$item->id}}" data-togglediv="tog-{{$item->id}}" class="clicktoggle_normal">
							{{$item->code}}
						</a>
                    </td>
                    <td>
                        {{$item->cus_email}}
                    </td>
                    <td>
                        {{$item->cus_name}}
                    </td>
                    <td>
                        {{$item->cus_phone}}
                    </td>					<td>                        {{$item->cus_address}}                    </td>
					<td>
                        @if($item->order_type==0)
						Mua trực tiếp
						@else
						Mua online
						@if($item->payment_type==0) (Vnpay) @elseif($item->payment_type==1) (COD) @endif
						@endif
                    </td>
                    <td>
                        {{$item->created_at}}
                    </td>
                    
					<td>
						
						@if($item->payment_type==0)
							@if($item->status==0)
							Lỗi<br/>(Chưa thanh toán)
							@elseif($item->status==1)
							Thành công <br/>(Đã thanh toán)
							@else
							Đã xóa
							@endif
						@else
							@if($item->status==0)
							(Chưa thanh toán)
							@elseif($item->status==1)
							Thành công <br/>(Đã thanh toán)
							@else
							Đã xóa
							@endif
						@endif
					
                    </td>
                    <td>
                        <a href="javascript:void(0)" onclick="print_ticket('<?php echo action("\ADMIN\OrderController@postPrintTicket"); ?>',{{$item->id}})"><i class="fa fa-print"></i></a> 
                        @if($item->order_type==0 && $item->status!=2 || $item->order_type==1 && $item->status!=2 && $item->payment_type==1)
                        <a href="javascript:;;" onclick="delete_one({{$item->id}},'{{action('\ADMIN\OrderController@postDelete')}}', 'dataTables_content')"><i class="fa fa-trash-o"></i></a>
                        @endif
						@if($item->payment_type==1 && $item->status==0 && $item->order_type==1)
                        <a href="javascript:void(0)" title="Duyệt đơn hàng" onclick="delete_one({{$item->id}},'{{action('\ADMIN\OrderController@postActive')}}', 'dataTables_content')"><i class="fa fa-check"></i></a>
                        @endif
                    </td>
                </tr>
				<tr class="togglediv non_hover" id="tog-{{$item->id}}" style="display: none">

				</tr>
                <?php $i++; ?>
            <?php endforeach; ?>

            </tbody>
            <tfoot class="bg-primary">
                <tr>
                    <td colspan="2">Tổng số đơn hàng: {{$order}}</td>
                    <td colspan="3">Tổng tiền: {{number_format($order_detail->total)}} VNĐ</td>
                    <td colspan="4">Tổng số ghế: {{$order_detail->quantity}}</td>
                </tr>
            </tfoot>
    </table>
</div>
@if($data->links()!='')
<div class="row">
    <div class="col-md-5 col-sm-12">
        <div class="dataTables_info" id="sample_2_info" role="status" aria-live="polite">Đang hiển thị trang {{$data->getCurrentPage()}} đến {{$data->getLastPage()}} của {{$data->getTotal()}} dữ liệu</div>
    </div>
    <div class="col-md-7 col-sm-12">
        <div class="dataTables_paginate paging_bootstrap_number paginate_normal" >
            {{$data->links('admin.template_admin.pagination')}} 
        </div>
    </div>
</div>
@endif