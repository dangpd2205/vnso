@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.alert_ajax')
@include('admin.users.users.file')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Phản hồi trang chủ
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")



<div class="row">
    {{Form::open(array('action' => '\ADMIN\TweetController@postAdd','id'=>'add_user','class'=>'form-horizontal'))}}
    <div class="col-md-12">  
        @include('admin.template.error')
		<div class="form-group">
			<label class="col-lg-2 control-label">Tên</label>
			<div class="col-lg-6">
				{{Form::text('name',  '', array('class'=>'bg-focus form-control input-sm', 'id'=>'name'))}}
			</div> 
		</div>
		<div class="form-group">
			<label class="col-lg-2 control-label">Nội dung</label>
			<div class="col-lg-6">
				{{Form::textarea('content', '', ['class'=>'bg-focus form-control','rows'=>'4', 'id'=>'content'])}}
			</div> 
		</div>
		<div class="form-group">
			<label class="col-lg-2 control-label">
				Ảnh
				<a href="javascript:;;" title="Thêm ảnh đại diện" data-img-div="div_image_selected" data-img-id="image_hidden" data-img-check="1" data-modal-id="Selectfile" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=imageselect" class="btn btn-white select_image"><i class="fa fa-plus text"></i></a>
			</label>
			<div class="col-lg-9">
				<div id="div_image_selected">
				</div>
				{{Form::hidden('image_hidden', '', array('id'=>'image_hidden'))}}
				{{Form::hidden('imageselect', null, array('id'=>'imageselect'))}}
			</div> 
		</div>
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a onclick="$('#add_user').submit()" target="_blank" class="active">
                <span>Thêm mới</span>
                <i class="icon-plus"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection