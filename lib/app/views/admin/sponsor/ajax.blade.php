<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th scope="col" style="width: 1%;" ></th>
					<th>Họ tên</th>
                    <th>Email</th>
                    <th>SĐT</th>
					<th>Địa chỉ</th>
					<th>Giới tính</th>
					<th>Ngày sinh</th>
					<th>Loại</th>
					<th style="width: 10%;">Xóa</th>
            </tr>
        </thead>
        <tbody id="tbl_content_ajax">

@if(count($data)==0)
<tr>
    <td colspan="9">NO DATA</td>
</tr>
@endif
<?php $i=0; ?>
<?php foreach ($data as $item): ?>
    <tr>
        <td class="text-center">
            <input type="checkbox" name="id[]" value="{{$item->id}}">
        </td>
        <td>
            {{$item->name}}
        </td>
        <td>
            {{$item->email}}
        </td>
       <td>
            {{$item->phone}}
        </td>
		<td>
            {{$item->address}}
        </td>
		<td>
            {{$item->sex}}
        </td>
		<td>
            {{date('d/m/Y',$item->bod)}}
        </td>
		<td>
            {{$item->type}}
        </td>
        <td>
            
            <a href="javascript:void(0)" onclick="delete_reload({{$item->id}},'{{action('\ADMIN\SponsorController@postDelete')}}')"><i class="fa fa-trash-o"></i></a>&nbsp;&nbsp;            
        </td>
    </tr>
    <?php $i++; ?>
<?php endforeach; ?>
        </tbody>
    </table>
</div>
@if($data->links()!='')
<div class="row">
    <div class="col-md-5 col-sm-12">
        <div class="dataTables_info" id="sample_2_info" role="status" aria-live="polite">Đang hiển thị trang {{$data->getCurrentPage()}} đến {{$data->getLastPage()}} của {{$data->getTotal()}} dữ liệu</div>
    </div>
    <div class="col-md-7 col-sm-12">
        <div class="dataTables_paginate paging_bootstrap_number paginate_normal" >
            {{$data->links('admin.template_admin.pagination')}} 
        </div>
    </div>
</div>
@endif