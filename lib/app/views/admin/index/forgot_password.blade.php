<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="utf-8">
        <title>Mobile first web app theme | first</title>
        <meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="{{Asset('asset/admin')}}/css/font.css">
        <link rel="stylesheet" href="{{Asset('asset/admin')}}/css/app.v2.css" type="text/css" />
        <link rel="stylesheet" href="{{Asset('asset/admin')}}/css/custom.css" type="text/css" />
        <!--[if lt IE 9]>
        <script src="{{Asset('asset/admin')}}/js/ie/respond.min.js">
        </script>
        <script src="{{Asset('asset/admin')}}/js/ie/html5.js">
        </script>
        <script src="{{Asset('asset/admin')}}/js/ie/excanvas.js">
        </script>
        <![endif]-->
    </head>
    <body class="navbar-fixed">
        <header id="header" class="navbar"> 
            <a href="docs.html" class="btn btn-link pull-right m-t-mini">
                <i class="fa fa-question fa-lg text-default"></i>
            </a> 
            <a class="navbar-brand" href="{{action('\ADMIN\HomeController@getHomeAdmin')}}">Pubweb.vn</a> 
        </header>
        <section id="content">
            <div class="main padder"> 
                <div class="row"> 
                    <div class="col-lg-4 col-lg-offset-4 m-t-large">
                        <section class="panel">
                            <header class="panel-heading text-center" style="background-color: #22baa0;color: #FFF;"> <strong>Quên mật khẩu</strong> </header> 

                            {{Form::open(array('action'=>'\ADMIN\HomeController@postForGotPassword', 'class'=>'panel-body', 'id'=>'form_add_menu_group'))}}
                            @if($errors->has())
                            <div class="alert <?php
                            if ($errors->first() == Lang::get('admin/message.msg_forgot_password.forgot_success')) {
                                echo 'alert-success';
                            } else {
                                ?>alert-warning<?php } ?>"> 
                                <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
                                <i class="fa fa-ban-circle fa-lg"></i>
                                {{$errors->first()}}  
                            </div>    
                            @endif
                            <div class="block"> 
                                <label class="control-label" for="email">Tài khoản hoặc E-mail</label> 
                                {{Form::email("email",null,["class"=>"bg-focus form-control","id"=>"email","required"=>"required","placeholder"=>"test@example.com"])}}
                            </div> 
                            <button type="submit" class="btn btn-primary">Gửi lại mật khẩu</button>   
                            {{Form::close()}}
                        </section> 
                    </div>
                </div>
            </div> 
        </section>
        <footer id="footer">
            <div class="text-center padder clearfix">
                <p>
                    <small>© <a href="http://pubweb.vn" target="_blank">Pubweb.vn</a> 2014, Hệ thống quản lý website chuyên nghiệp</small>
                    <br><br>
                    <a href="#" class="btn btn-xs btn-circle btn-twitter">
                        <i class="fa fa-twitter"></i>
                    </a> 
                    <a href="#" class="btn btn-xs btn-circle btn-facebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="btn btn-xs btn-circle btn-gplus">
                        <i class="fa fa-google-plus"></i>
                    </a>
                </p>
            </div> 
        </footer>
        <script src="{{Asset('asset/admin')}}/js/app.v2.js"></script>
    </body>
</html>