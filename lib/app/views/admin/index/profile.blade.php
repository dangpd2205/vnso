@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@endsection

@extends("admin.template_admin.index")
@section("content")
@include('admin.template.alert_ajax')
@include('admin.users.users.file')
<div class="row">
    {{Form::open(array('action' => '\ADMIN\HomeController@postProfile','id'=>'add_user','class'=>'form-horizontal'))}}
	{{Form::hidden('id',$detail->id)}}
    <div class="col-md-12">  
        @include('admin.template.error')
        <div class="col-lg-8">
            <div class="col-lg-7">
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Email</label>
                    <div class="col-lg-6">
                        {{Form::email('email',  $detail->email, array('class'=>'bg-focus form-control', 'id'=>'email', 'readonly'=>'true'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Mật khẩu</label>
                    <div class="col-lg-6">
                        {{Form::password('password', null,array('class'=>'bg-focus form-control', 'id'=>'password','placeholder'=>'Để trống nếu không thay đổi'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Họ tên</label>
                    <div class="col-lg-6">
                        {{Form::text('full_name',  $detail->full_name, array('class'=>'bg-focus form-control', 'id'=>'full_name'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Số điện thoại</label>
                    <div class="col-lg-6">
                        {{Form::text('phone',  $detail->phone, array('class'=>'bg-focus form-control', 'id'=>'phone'))}}
                    </div> 
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        <a href="javascript:;;" title="Thêm ảnh đại diện" data-img-div="div_image_selected" data-img-id="image_hidden" data-img-check="1" data-modal-id="Selectfile" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=imageselect" class="btn btn-white select_image"><i class="fa fa-plus text"></i></a>
                    </label>
                    <div class="col-lg-9">
                        <div id="div_image_selected">
						@if($detail->avatar!='')
						<div class="thumbnail  pull-left m-l-small item">
                            <a href="{{Asset($detail->avatar)}}" target="_blank">
                            <img src="{{Asset($detail->avatar)}}"></a><span data-img-id="image_hidden"> <i class="fa fa-times"></i> </span>
						</div>
						@endif
                        </div>
                        {{Form::hidden('image_hidden', $detail->avatar, array('id'=>'image_hidden'))}}
                        {{Form::hidden('imageselect', $detail->avatar, array('id'=>'imageselect'))}}
                    </div> 
                </div>
            </div>
            <div class="form-group col-lg-12">
                <label class="col-lg-3 control-label" for="news_excerpt">Địa chỉ</label>
                <div class="col-lg-8">
                    {{Form::textarea('address', $detail->address, ['class'=>'bg-focus form-control','rows'=>'4', 'id'=>'address'])}}
                </div> 
            </div>
        </div>
        
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a onclick="$('#add_user').submit()" target="_blank" class="active">
                <span>Cập nhật</span>
                <i class="icon-basket"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection