<?php
$data_content = $data
?>    
<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
              
                <th scope="col">Tiêu đề</th>
                <th scope="col">Thời gian</th>
                <th scope="col">Trạng thái</th>
                <th style="width: 10%;">Edit</th>
            </tr>
        </thead>
        <tbody id="tbl_content_ajax">
            @if(count($data_content)==0)
            <tr>
                <td colspan="4">NO DATA</td>
            </tr>
            @endif
            <?php

            foreach ($data_content as $item):
                ?>
                <tr>
                  
                    <td>
                        <a href="{{action('\ADMIN\PageController@getEdit')}}/{{$item->lang_id}}/{{$item->page_id}}"> {{$item->page_name}}</a>
                    </td>
                  
                    <td>
                        {{date('d/m/Y H:i:s',$item->time_post)}}
                    </td>
                    <td style="@if($item->status == 3) color: #ff5f5f ; @endif @if($item->status == 1) color: #3fcf7f ; @endif  @if($item->status == 0) color: #f4c414 ; @endif">
                        {{Lang::get('general.news_status')[$item->status]}} @if($item->status == 3)<i class="fa fa-pencil-square-o note_comfirm_view" data-toggle="popover" data-html="true" data-placement="top" data-content="" title="" data-original-title="<button type=&quot;button&quot; class=&quot;close pull-right&quot; data-dismiss=&quot;popover&quot;><i class=&quot;fa fa-times&quot;></i></button>Lý do" aria-describedby="popover751928"></i> @endif
                     
                    </td>
                    <td>
                        <a href="{{action('\ADMIN\PageController@getEdit')}}/{{$item->lang_id}}/{{$item->page_id}}" title="Chỉnh sửa bài viết"> <i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                       
                        <a href="javascript:void(0)" title="Xóa bài viết" onclick="delete_one({{$item->page_id}},'{{action('\ADMIN\PageController@postDelete')}}', 'dataTables_content')"><i class="fa fa-trash-o"></i></a>&nbsp;&nbsp;
                        @if($item->status!=1)  <a href="javascript:void(0)" title="Duyệt bài" onclick="delete_one({{$item->page_id}},'{{action('\ADMIN\PageController@postPostConfirm')}}', 'dataTables_content')"><i class="fa fa-gavel"></i></a>&nbsp;&nbsp; @endif
                       
                        
                    </td>
                </tr>
                <tr class="togglediv" id="tog-{{$item->id.$item->lang_id}}" style="display: none">

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
@if($data_content->links()!='')
<div class="row">
    <div class="col-md-5 col-sm-12">
        <div class="dataTables_info" id="sample_2_info" role="status" aria-live="polite">Đang hiển thị trang {{$data_content->getCurrentPage()}} đến {{$data_content->getLastPage()}} của {{$data_content->getTotal()}} dữ liệu</div>
    </div>
    <div class="col-md-7 col-sm-12">
        <div class="dataTables_paginate paging_bootstrap_number paginate_normal" >
            {{$data_content->links('admin.template_admin.pagination')}} 
        </div>
    </div>
</div>
@endif