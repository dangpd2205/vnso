@section("title")
Thêm trang tĩnh
@endsection
@section("description")
Thêm trang tĩnh
@endsection
@section("modal_option")
@include('admin.template.alert_ajax')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>
	Trang tĩnh
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
{{Form::open(array('action' => '\ADMIN\PageController@postAdd','id'=>'add_news','class'=>'form-horizontal'))}}

<div class="row">
    <div class="col-md-12">  
	@include('admin.template.error')
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#content-tab1" data-toggle="tab"><i class="fa fa-globe fa-lg text-default"></i> Nội dung chính</a>
            </li>
            <li >
                <a href="#content-tab3" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> SEO</a>
            </li>
            <li>
                <a href="#content-tab4" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> Cấu hình</a>
            </li>
        </ul>
    </header>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane fade  active in" id="content-tab1">
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">

                                <?php
                                foreach ($l_lang as $item) {
                                    if ($item->id == $g_config_all->website_lang) {
                                        ?>
                                        <a href="#lang_p_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                        <?php
                                    }
                                }
                                ?>

                            </li>
                            <?php if (count($l_lang) > 1) { ?>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                    <ul class="dropdown-menu text-left">
                                        <?php
                                        foreach ($l_lang as $item_child) {
                                            if ($item_child->id != $g_config_all->website_lang) {
                                                ?>
                                                <li> <a href="#lang_p_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>   
                        <?php } ?>
                        </ul>   
                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                $arr_lang = [];
                                foreach ($l_lang as $item_content) {
                                    $arr_lang += array($item_content->id => $item_content->code);
                                    ?>
                                    <div class="tab-pane fade in <?php
                                    $tran_class = '';
                                    if ($item_content->id == $g_config_all->website_lang) {
                                        echo ' active';
                                        $tran_class = 'translate_google';
                                    }
                                    ?> " id="lang_p_{{$item_content->id}}">
                                        <h3>{{$item_content->name}}</h3>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="news_title_{{$item_content->code}}">Tiêu đề</label>
                                            <div class="col-lg-6">
                                                {{Form::text('page_name_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'page_name_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="news_excerpt_{{$item_content->code}}">Mô tả</label>
                                            <div class="col-lg-6">
                                                {{Form::textarea('page_excerpt_'.$item_content->code,'', ['class'=>'bg-focus form-control ','rows'=>'4', 'id'=>'page_excerpt_'.$item_content->code])}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="news_content_{{$item_content->code}}">Nội dung</label>
                                            <div class="col-lg-9">
                                                {{Form::textarea('page_content_'.$item_content->code,'', ['class'=>'tinymce ', 'id'=>'page_content_'.$item_content->code])}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="news_tag_{{$item_content->code}}">Tags</label>
                                            <div class="col-lg-6">
                                                {{Form::text('page_tag_'.$item_content->code, null, array('class'=>'bg-focus tags_input ', 'id'=>'page_tag_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="content-tab3">
                <div class="form-group"> 
                    <label class="col-lg-2 control-label no-padding-top">Meta Robots Index</label> 
                    <div class="col-lg-7">
                        <label class="mt-radio">
                            {{Form::radio('seo_index','index', 1)}}
                            Index
                            <span></span>
                        </label>
                        <label class="mt-radio">
                            {{Form::radio('seo_index','noindex',0)}} Noindex
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group"> 
                    <label class="col-lg-2 control-label no-padding-top">Meta Robots Follow</label> 
                    <div class="col-lg-7">
                        <label class="mt-radio">
                            {{Form::radio('seo_follow','Follow', 1)}}
                            Follow
                            <span></span>
                        </label>
                        <label class="mt-radio">
                            {{Form::radio('seo_follow','Nofollow',0)}} Nofollow
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group"> 
                    <label class="col-lg-2 control-label no-padding-top">Meta Robots Follow</label> 
                    <div class="col-lg-9">
                        <label class="mt-checkbox">
                            <input type="checkbox" name="seo_advanced[]" value="NO ODP"/> NO ODP
                            <span></span>
                        </label>
                        <label class="mt-checkbox">
                            <input type="checkbox" name="seo_advanced[]" value="NO YDIR"/> NO YDIR
                            <span></span>
                        </label>
                        <label class="mt-checkbox">
                            <input type="checkbox" name="seo_advanced[]" value="NO Image Index"/> NO Image Index
                            <span></span>
                        </label>
                        <label class="mt-checkbox">
                            <input type="checkbox" name="seo_advanced[]" value="NO Archive"/> NO Archive
                            <span></span>
                        </label>
                        <label class="mt-checkbox">
                            <input type="checkbox" name="seo_advanced[]" value="NO Snippet"/> NO Snippet
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">

                                <?php
                                foreach ($l_lang as $item) {
                                    if ($item->id == $g_config_all->website_lang) {
                                        ?>
                                        <a href="#lang_p_seo_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                        <?php
                                    }
                                }
                                ?>

                            </li>
                            <?php if (count($l_lang) > 1) { ?>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                    <ul class="dropdown-menu text-left">
                                        <?php
                                        foreach ($l_lang as $item_child) {
                                            if ($item_child->id != $g_config_all->website_lang) {
                                                ?>
                                                <li> <a href="#lang_p_seo_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                foreach ($l_lang as $item_content) {
                                    ?>
                                    <div class="tab-pane fade in <?php
                                    $tran_class = '';
                                    if ($item_content->id == $g_config_all->website_lang) {
                                        echo ' active';
                                        $tran_class = 'translate_google';
                                    }
                                    ?> " id="lang_p_seo_{{$item_content->id}}">
                                        <h3>{{$item_content->name}}</h3>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_keyword_{{$item_content->code}}">SEO Keyword</label>
                                            <div class="col-lg-5">
                                                {{Form::text('seo_keyword_'.$item_content->code, null, array('class'=>'bg-focus form-control tags_input ', 'id'=>'seo_keyword_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_title_{{$item_content->code}}">SEO Title</label>
                                            <div class="col-lg-9">
                                                {{Form::text('seo_title_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_title_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_description_{{$item_content->code}}">Meta Description</label>
                                            <div class="col-lg-9">
                                                {{Form::textarea('seo_description_'.$item_content->code,'',['id'=>'seo_description_'.$item_content->code,'class'=>'bg-focus form-control ','rows'=>"2"])}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_f_title_{{$item_content->code}}">Facebook Title</label>
                                            <div class="col-lg-9">
                                                {{Form::text('seo_f_title_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_f_title_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_f_description_{{$item_content->code}}">Facebook Description</label>
                                            <div class="col-lg-9">
                                                {{Form::textarea('seo_f_description_'.$item_content->code,'',['id'=>'seo_f_description_'.$item_content->code ,'class'=>'bg-focus form-control ','rows'=>"2"])}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_f_image_{{$item_content->code}}">Facebook Image</label>
                                            <div class="col-lg-9">
                                                <div class="input-group">
                                                    {{Form::text('seo_f_image_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_f_image_'.$item_content->code))}}
                                                    <div class="input-group-btn"> <button class="btn btn-white select_image input-sm"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=seo_f_image_{{$item_content->code}}"> Thêm ảnh </button></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_g_title_{{$item_content->code}}">Google+ Title</label>
                                            <div class="col-lg-9">
                                                {{Form::text('seo_g_title_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_g_title_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_g_description_{{$item_content->code}}">Google+ Description</label>
                                            <div class="col-lg-9">
                                                {{Form::textarea('seo_g_description_'.$item_content->code,'',['id'=>'seo_g_description_'.$item_content->code,'class'=>'bg-focus form-control ','rows'=>"2"])}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_g_image_{{$item_content->code}}">Google+ Image</label>
                                            <div class="col-lg-9">
                                                <div class="input-group">
                                                    {{Form::text('seo_g_image_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_g_image_'.$item_content->code))}}
                                                    <div class="input-group-btn"> <button class="btn btn-white select_image input-sm" data-toggle="dropdown" data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=seo_g_image_{{$item_content->code}}"> Thêm ảnh </button></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="content-tab4">
                <div class="form-group"> 
                    <label class="col-lg-2 control-label no-padding-top">Bật/Tắt comment</label> 
                    <div class="col-lg-7">
                        <label class="mt-radio">
                            {{Form::radio('comment','1', 1)}}
                            Bật
                            <span></span>
                        </label>
                        <label class="mt-radio">
                            {{Form::radio('comment','0',0)}} Tắt
                            <span></span>
                        </label>
                    </div>
                   
                </div>
                <div class="form-group"> 
                    <label class="col-lg-2 control-label no-padding-top">Ping</label> 
                    <div class="col-lg-7">
                        <label class="mt-radio">
                            {{Form::radio('ping','1', 1)}}
                            Bật
                            <span></span>
                        </label>
                        <label class="mt-radio">
                            {{Form::radio('ping','0',0)}} Tắt
                            <span></span>
                        </label>
                    </div>
                </div>
            
               
            </div>
        </div>
    </div>
  
</div>
  {{Form::close()}}
<style>
    .popover .arrow{
        display: none;
    }
</style>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#add_news').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection