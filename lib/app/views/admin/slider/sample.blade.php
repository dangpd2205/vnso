<div id="ls-sample">
    <div class="ls-box ls-layer-box">
        <input type="hidden" name="layerkey" value="0">
        <table>
            <thead class="ls-layer-options-thead">
                <tr>
                    <td colspan="3">
                        <i class="dashicons dashicons-welcome-write-blog"></i>
                        <h4><?php echo 'Slide Options'; ?></h4>
                    </td>
                </tr>
            </thead>
            <tbody class="ls-slide-options">
            <input type="hidden" name="post_offset" value="-1">
            <input type="hidden" name="3d_transitions">
            <input type="hidden" name="2d_transitions">
            <input type="hidden" name="custom_3d_transitions">
            <input type="hidden" name="custom_2d_transitions">
            <tr>
                <td valign="top">
                    <h3 class="subheader">Slide image &amp; thumbnail</h3>
                    <div class="inner slide-image">
                        <input type="hidden" name="backgroundId">
                        <input type="hidden" name="background" id="background-hidden-demslider">
                        <div class="ls-image ls-upload" data-img-id="background-img-demslider" data-modal-id="Selectfile" data-link="<?php echo asset('');?>/asset/filemanager/dialog.php?type=1&amp;field_id=background-hidden-demslider" data-help="<?php echo $lsDefaults['slides']['image']['tooltip'] ?>">
                            <div><img id="background-img-demslider" src="<?php echo asset('asset/slider') . '/img/not_set.png' ?>" alt=""></div>
                            <a href="#" class="dashicons dashicons-dismiss"></a>
                        </div>
                        <?php echo 'or'; ?> <a href="#" class="ls-url-prompt"><?php echo 'enter URL'; ?></a> <br>
                        <?php echo 'or'; ?> <a href="#" class="ls-post-image"><?php echo 'use post image'; ?></a>
                    </div>
                    <div class="hsep"></div>
                    <div class="inner slide-image">
                        <input type="hidden" name="thumbnailId">
                        <input type="hidden" name="thumbnail" id="thumbnail-hidden-demslider">
                        <div class="ls-image ls-upload" data-img-id="thumbnail-img-demslider" data-modal-id="Selectfile" data-link="<?php echo asset('');?>/asset/filemanager/dialog.php?type=1&amp;field_id=thumbnail-hidden-demslider" data-help="<?php echo $lsDefaults['slides']['thumbnail']['tooltip'] ?>">
                            <div><img id="thumbnail-img-demslider" src="<?php echo asset('asset/slider') . '/img/not_set.png' ?>" alt=""></div>
                            <a href="#" class="dashicons dashicons-dismiss"></a>
                        </div>
                        <?php echo 'or'; ?> <a href="#" class="ls-url-prompt"><?php echo 'enter URL'; ?></a>
                    </div>
                </td>
                <td valign="top" class="second">
                    <h3 class="subheader">Duration</h3>
                    <?php \ADMIN\SliderController::lsGetInput($lsDefaults['slides']['delay'], null, array('class' => 'layerprop')) ?> ms
                    <h3 class="subheader">
                        Transition
                    </h3>
                    <button type="button" class="button ls-select-transitions new" data-help="<?php echo 'You can select your desired slide transitions by clicking on this button.'; ?>">Select transitions</button><br><br>
                    <?php echo $lsDefaults['slides']['timeshift']['name'] ?><br>
                    <?php \ADMIN\SliderController::lsGetInput($lsDefaults['slides']['timeshift'], null, array('class' => 'layerprop')) ?> ms
                </td>
                <td valign="top">
                    <h3 class="subheader">Linking</h3>
                    <div class="ls-slide-link">
                        <?php \ADMIN\SliderController::lsGetInput($lsDefaults['slides']['linkUrl'], null, array('placeholder' => $lsDefaults['slides']['linkUrl']['name'])) ?>
                        <br> <?php \ADMIN\SliderController::lsGetSelect($lsDefaults['slides']['linkTarget'], null) ?>
                        <span> or <a href="#"><?php echo 'use post URL'; ?></a></span>
                    </div>
                    <h3 class="subheader">Misc</h3>
                    <table class="noborder">
                        <tr>
                            <td>
                                <?php echo $lsDefaults['slides']['ID']['name'] ?>
                                <?php \ADMIN\SliderController::lsGetInput($lsDefaults['slides']['ID'], null) ?>
                            </td>
                            <td>
                                <?php echo $lsDefaults['slides']['deeplink']['name'] ?>
                                <?php \ADMIN\SliderController::lsGetInput($lsDefaults['slides']['deeplink'], null) ?>
                            </td>
                            <td>
                                <?php echo 'Hidden'; ?>
                                <input type="checkbox" name="skip" class="checkbox" data-help="<?php echo "If you don't want to use this slide in your front-page, but you want to keep it, you can hide it with this switch."; ?>">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        <table>
            <thead>
                <tr>
                    <td>
                        <i class="dashicons dashicons-editor-video ls-preview-icon"></i>
                        <h4>
                            <span><?php echo 'Preview'; ?></span>
                            <div class="ls-editor-zoom">
                                <span class="ls-editor-slider-text">Size:</span>
                                <div class="ls-editor-slider"></div>
                                <span class="ls-editor-slider-val">100%</span>
                            </div>
                        </h4>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="ls-preview-td">
                        <div class="ls-preview-wrapper">
                            <div class="ls-preview">
                                <div class="draggable ls-layer"></div>
                            </div>
                            <div class="ls-real-time-preview"></div>
                        </div>
                        <button type="button" class="button btn btn-primary ls-preview-button"><?php echo 'Enter Preview'; ?></button>
                    </td>
                </tr>
            </tbody>
        </table>
        <table>
            <thead>
                <tr>
                    <td>
                        <div class="dashicons dashicons-images-alt ls-layers-icon"></div>
                        <h4><?php echo 'Layers'; ?><a href="#" class="ls-tl-toggle">[ <?php echo 'timeline view'; ?> ]</a></h4>
                    </td>
                </tr>
            </thead>
            <tbody class="ls-sublayers ls-sublayer-sortable">
                <tr class="active">
                    <td>
                        <div class="ls-sublayer-wrapper">
                            <span class="ls-sublayer-sortable-handle dashicons dashicons-menu"></span>
                            <span class="ls-sublayer-number">1</span>
                            <input type="text" name="subtitle" class="ls-sublayer-title" value="Layer #1">

                            <div class="ls-tl">
                                <table>
                                    <tr>
                                        <td data-help="Delay in: " class="ls-tl-delayin"></td>
                                        <td data-help="Duration in: " class="ls-tl-durationin"></td>
                                        <td data-help="Show Until: " class="ls-tl-showuntil"></td>
                                        <td data-help="Duration out: " class="ls-tl-durationout"></td>
                                    </tr>
                                </table>
                            </div>

                            <span class="ls-sublayer-controls">
                                <span class="ls-highlight dashicons dashicons-lightbulb" data-help="<?php echo 'Highlight layer in the editor.'; ?>"></span>
                                <span class="ls-icon-lock dashicons dashicons-lock" data-help="<?php echo 'Prevent layer from dragging in the editor.'; ?>"></span>
                                <span class="ls-icon-eye dashicons dashicons-visibility" data-help="<?php echo 'Hide layer in the editor.'; ?>"></span>
                            </span>
                            <div class="clear"></div>
                            <div class="ls-sublayer-nav">
                                <a href="#" class="active"><?php echo 'Content'; ?></a>
                                <a href="#"><?php echo 'Transition'; ?></a>
                                <a href="#"><?php echo 'Link'; ?></a>
                                <a href="#"><?php echo 'Styles'; ?></a>
                                <a href="#"><?php echo 'Attributes'; ?></a>
                                <a href="#" title="<?php echo 'Remove this layer'; ?>" class="dashicons dashicons-dismiss remove"></a>
                            </div>
                            <div class="ls-sublayer-pages">
                                <div class="ls-sublayer-page ls-sublayer-basic active">

                                    <input type="hidden" name="media" value="img">
                                    <div class="ls-layer-kind">
                                        <ul>
                                            <li data-section="img" class="active"><span class="dashicons dashicons-format-image"></span><?php echo 'Image'; ?></li>
                                            <li data-section="text"><span class="dashicons dashicons-text"></span><?php echo 'Text'; ?></li>
                                            <li data-section="html"><span class="dashicons dashicons-video-alt3"></span><?php echo 'HTML / Video / Audio'; ?></li>
                                        </ul>
                                    </div>
                                    <!-- End of Layer Media Type -->

                                    <!-- Layer Element Type -->
                                    <input type="hidden" name="type" value="p">
                                    <ul class="ls-sublayer-element ls-hidden">
                                        <li class="ls-type active" data-element="p"><?php echo 'Paragraph'; ?></li>
                                        <li class="ls-type" data-element="h1"><?php echo 'H1'; ?></li>
                                        <li class="ls-type" data-element="h2"><?php echo 'H2'; ?></li>
                                        <li class="ls-type" data-element="h3"><?php echo 'H3'; ?></li>
                                        <li class="ls-type" data-element="h4"><?php echo 'H4'; ?></li>
                                        <li class="ls-type" data-element="h5"><?php echo 'H5'; ?></li>
                                        <li class="ls-type" data-element="h6"><?php echo 'H6'; ?></li>
                                    </ul>
                                    <!-- End of Layer Element Type -->

                                    <div class="ls-layer-sections">

                                        <!-- Image Layer -->
                                        <div class="ls-image-uploader slide-image clearfix">
                                            <input type="hidden" name="imageId">
                                            <input type="hidden" name="image" id="image_id_numberchild">
                                            <div class="ls-image ls-upload " data-img-id="image-img-numberchild" data-modal-id="Selectfile" data-link="<?php echo asset(''); ?>/asset/filemanager/dialog.php?type=1&amp;field_id=image_id_numberchild">
                                                <div><img id="image-img-numberchild" src="<?php echo asset('asset/slider') . '/img/not_set.png' ?>" alt=""></div>
                                                <a href="#" class="dashicons dashicons-dismiss"></a>
                                            </div>
                                            <p>
                                                <?php echo 'Click on the image preview to open WordPress Media Library or'; ?>
                                                <a href="#" class="ls-url-prompt"><?php echo 'insert from URL'; ?></a> or
                                                <a href="#" class="ls-post-image"><?php echo 'use post image'; ?></a>.
                                            </p>
                                        </div>

                                        <!-- Text/HTML Layer -->
                                        <div class="ls-html-code ls-hidden">
                                            <textarea name="html" cols="50" rows="5" placeholder="Enter layer content here" data-help="<?php echo 'Type here the contents of your layer. You can use any HTML codes in this field to insert content others then text. This field is also shortcode-aware, so you can insert content from other plugins as well as video embed codes.'; ?>"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="ls-sublayer-page ls-sublayer-options">
                                    <input type="hidden" name="transition">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td rowspan="3"><?php echo 'Transition in'; ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionInOffsetX']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInOffsetX'], null, array('class' => 'sublayerprop')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionInOffsetY']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInOffsetY'], null, array('class' => 'sublayerprop')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionInDuration']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInDuration'], null, array('class' => 'sublayerprop')) ?> ms</td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionInDelay']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInDelay'], null, array('class' => 'sublayerprop')) ?> ms</td>
                                                <td class="right"><a href="http://easings.net/" target="_blank"><?php echo $lsDefaults['layers']['transitionInEasing']['name'] ?></a></td>
                                                <td><?php \ADMIN\SliderController::lsGetSelect($lsDefaults['layers']['transitionInEasing'], null, array('class' => 'sublayerprop', 'options' => $lsDefaults['easings'])) ?></td>
                                            </tr>
                                            <tr>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionInFade']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['layers']['transitionInFade'], null, array('class' => 'sublayerprop')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionInRotate']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInRotate'], null, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionInRotateX']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInRotateX'], null, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionInRotateY']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInRotateY'], null, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                <td colspan="2" rowspan="2" class="center">
                                                    <?php echo $lsDefaults['layers']['transitionInTransformOrigin']['name'] ?><br>
                                                    <?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInTransformOrigin'], null, array('class' => 'sublayerprop')) ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionInSkewX']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInSkewX'], null, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionInSkewY']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInSkewY'], null, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionInScaleX']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInScaleX'], null, array('class' => 'sublayerprop')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionInScaleY']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInScaleY'], null, array('class' => 'sublayerprop')) ?></td>
                                            </tr>
                                            <tr class="ls-separator"><td colspan="11"></td></tr>
                                            <tr>
                                                <td rowspan="3"><?php echo 'Transition out'; ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionOutOffsetX']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutOffsetX'], null, array('class' => 'sublayerprop')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionOutOffsetY']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutOffsetY'], null, array('class' => 'sublayerprop')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionOutDuration']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutDuration'], null, array('class' => 'sublayerprop')) ?> ms</td>
                                                <td class="right"><?php echo $lsDefaults['layers']['showUntil']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['showUntil'], null, array('class' => 'sublayerprop')) ?> ms</td>
                                                <td class="right"><a href="http://easings.net/" target="_blank"><?php echo $lsDefaults['layers']['transitionOutEasing']['name'] ?></a></td>
                                                <td><?php \ADMIN\SliderController::lsGetSelect($lsDefaults['layers']['transitionOutEasing'], null, array('class' => 'sublayerprop', 'options' => $lsDefaults['easings'])) ?></td>
                                            </tr>
                                            <tr>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionOutFade']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['layers']['transitionOutFade'], null, array('class' => 'sublayerprop')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionOutRotate']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutRotate'], null, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionOutRotateX']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutRotateX'], null, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionOutRotateY']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutRotateY'], null, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                <td colspan="2" rowspan="2" class="center">
                                                    <?php echo $lsDefaults['layers']['transitionOutTransformOrigin']['name'] ?><br>
                                                    <?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutTransformOrigin'], null, array('class' => 'sublayerprop')) ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionOutSkewX']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutSkewX'], null, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionOutSkewY']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutSkewY'], null, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionOutScaleX']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutScaleX'], null, array('class' => 'sublayerprop')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionOutScaleY']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutScaleY'], null, array('class' => 'sublayerprop')) ?></td>
                                            </tr>
                                            <tr class="ls-separator"><td colspan="11"></td></tr>
                                            <tr>
                                                <td rowspan="3"><?php echo 'Other options'; ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['transitionParallaxLevel']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionParallaxLevel'], null, array('class' => 'sublayerprop')) ?></td>
                                                <td class="right"><?php echo 'Hidden'; ?></td>
                                                <td><input type="checkbox" name="skip" class="checkbox" data-help="<?php echo "If you don't want to use this layer, but you want to keep it, you can hide it with this switch."; ?>"></td>
                                                <td colspan="6"><button type="button" class="button duplicate" data-help="<?php echo 'If you would like to use similar settings for other layers or to experiment on a copy, you can duplicate this layer.'; ?>"><?php echo 'Duplicate this layer'; ?></button></td>
                                            </tr>
                                    </table>
                                </div>
                                <div class="ls-sublayer-page ls-sublayer-link">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="ls-slide-link">
                                                        <?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['linkURL'], null, array('placeholder' => $lsDefaults['layers']['linkURL']['name'])) ?>
                                                        <br> <?php \ADMIN\SliderController::lsGetSelect($lsDefaults['layers']['linkTarget'], null) ?>
                                                        <span> or <a href="#"><?php echo 'use post URL'; ?></a></span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="ls-sublayer-page ls-sublayer-style">
                                    <input type="hidden" name="styles">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td><?php echo 'Layout & Positions'; ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['width']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['width'], null, array('class' => 'auto')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['height']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['height'], null, array('class' => 'auto')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['top']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['top'], null) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['left']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['left'], null) ?></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo 'Padding'; ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['paddingTop']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['paddingTop'], null, array('class' => 'auto')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['paddingRight']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['paddingRight'], null, array('class' => 'auto')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['paddingBottom']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['paddingBottom'], null, array('class' => 'auto')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['paddingLeft']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['paddingLeft'], null, array('class' => 'auto')) ?></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo 'Border'; ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['borderTop']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['borderTop'], null, array('class' => 'auto')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['borderRight']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['borderRight'], null, array('class' => 'auto')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['borderBottom']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['borderBottom'], null, array('class' => 'auto')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['borderLeft']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['borderLeft'], null, array('class' => 'auto')) ?></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo 'Font'; ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['fontFamily']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['fontFamily'], null, array('class' => 'auto')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['fontSize']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['fontSize'], null, array('class' => 'auto')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['lineHeight']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['lineHeight'], null, array('class' => 'auto')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['color']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['color'], null, array('class' => 'auto ls-colorpicker')) ?></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo 'Misc'; ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['background']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['background'], null, array('class' => 'auto ls-colorpicker')) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['borderRadius']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['borderRadius'], null, array('class' => 'auto')) ?></td>
                                                <td class="right"><?php echo 'Word-wrap'; ?></td>
                                                <td colspan="3"><input type="checkbox" name="wordwrap" data-help="<?php echo 'If you use custom sized layers, you have to enable this setting to wrap your text.'; ?>" class="checkbox"></td>
                                            </tr>
                                            <tr class="ls-separator"><td colspan="11"></td></tr>
                                            <tr>
                                                <td><?php echo 'Custom CSS'; ?></td>
                                                <td colspan="8"><textarea rows="5" cols="50" name="style" class="style" data-help="<?php echo 'If you want to set style settings other then above, you can use here any CSS codes. Please make sure to write valid markup.'; ?>"></textarea></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="ls-sublayer-page ls-sublayer-attributes">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td><?php echo 'Attributes'; ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['ID']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['ID'], null) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['class']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['class'], null) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['title']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['title'], null) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['alt']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['alt'], null) ?></td>
                                                <td class="right"><?php echo $lsDefaults['layers']['rel']['name'] ?></td>
                                                <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['rel'], null) ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <a href="#" class="ls-add-sublayer">
            <i class="dashicons dashicons-plus"></i> <?php echo 'Add new layer'; ?>
        </a>
    </div>
</div>
