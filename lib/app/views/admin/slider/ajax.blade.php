@if(count($data)==0)
<tr>
    <td colspan="3">NO DATA</td>
</tr>
@endif
<?php foreach ($data as $item): ?>
    <tr>
        <td class="text-center">
            <input type="checkbox" name="id[]" value="{{$item->id}}">
        </td>
        <td>
            <a href="<?php echo action('\ADMIN\SliderController@getSliderEdit') . '/' . $item->id; ?>">{{$item->name}}</a>
        </td>
        <td>
            <a href="<?php echo action('\ADMIN\SliderController@getSliderEdit') . '/' . $item->id; ?>"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
            <a href="javascript:;;" onclick="delete_slider({{$item->id}},'{{action('\ADMIN\SliderController@postDeleteSlider')}}', 'tbl_content_menu_ajax')"><i class="fa fa-trash-o"></i></a>
        </td>
    </tr>
<?php endforeach; ?>