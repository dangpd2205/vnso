<div class="modal fade"  id="add_slider" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Thêm</h4>
            </div>
            <div class="modal-body">
                {{Form::open(array('action'=>'\ADMIN\SliderController@postAddSlider', 'class'=>'form-horizontal', 'id'=>'form_add_slider'))}}
                <div class="form-group">
                    <label class="col-lg-3 control-label">Tên slider</label>
                    <div class="col-lg-8">
                        {{Form::text('name', '', ['id'=>'name','class'=>'bg-focus form-control'])}}
                    </div>
                </div>
                {{Form::close()}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="btnsubmitadd_slider('form_add_slider', 'add_slider')">Thêm mới</button>
            </div>
        </div>
    </div>
</div>