<!-- Modal -->
<div class="modal fade" id="Selectfile" tabindex="-1" role="dialog" aria-labelledby="SelectfileLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ls">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="SelectfileLabel">Chọn file</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" class="input_hidden_img_id">
                <script>
                    function responsive_filemanager_callback(fieldID) {
                        $("#" + $('.input_hidden_img_id').val()).attr('src', $("#" + fieldID).val());
                        LayerSlider.generatePreview($('.ls-layer-box.active').index());
                        $('#Selectfile').modal('hide');
                    }
                </script>
                <iframe width="100%" height="400" src="" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; " __idm_frm__="29">
                </iframe>
            </div>
        </div>
    </div>
</div>