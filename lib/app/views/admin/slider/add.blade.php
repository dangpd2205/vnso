@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.alert_ajax')
<link rel='stylesheet' id='layerslider-global-css'  href='{{Asset('asset/slider')}}/css/load_css.css' type='text/css' media='all' />
<link rel='stylesheet' id='layerslider-global-css'  href='{{Asset('asset/slider')}}/css/global.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='layerslider-admin-css'  href='{{Asset('asset/slider')}}/css/admin.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='layerslider-admin-new-css'  href='{{Asset('asset/slider')}}/css/admin_new.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='layerslider-css'  href='{{Asset('asset/slider')}}/css/layerslider.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='layerslider-tr-gallery-css'  href='{{Asset('asset/slider')}}/css/layerslider.transitiongallery.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='minicolor-css'  href='{{Asset('asset/slider')}}/js/minicolors/jquery.minicolors.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='codemirror-css'  href='{{Asset('asset/slider')}}/codemirror/lib/codemirror.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='codemirror-solarized-css'  href='{{Asset('asset/slider')}}/codemirror/theme/solarized.mod.css?ver=5.3.2' type='text/css' media='all' />
@include('admin.slider.modal')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Cấu hình <i class="fa fa-circle"></i></li>
<li>
	Slider
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")

<?php
$id = '';
if (isset($id_slider)) {
    $id = $id_slider;
}

$slider = $data;
$lsDefaults = Config::get('slider.lsDefaults');
?>

<div id="ls-transition-window" class="ls-modal ls-box">
    <header>
        <h2>Select slide transitions</h2>
        <div class="filters">
            <span>Show:</span>
            <ul>
                <li class="active">2D</li>
                <li>3D</li>
            </ul>
        </div>
        <b class="dashicons dashicons-no"></b>
        <i>Apply to others</i>
        <i class="off">Select all</i>
    </header>
    <div class="inner">
        <table></table>
    </div>
</div>
@include('admin.slider.sample')
<form action="{{action('\ADMIN\SliderController@postSaveSlider')}}" method="post" class="wrap" id="ls-slider-form" novalidate="novalidate">
    <input type="hidden" name="slider_id" value="<?php echo $id ?>">
    <input type="hidden" name="action" value="ls_save_slider">
    <h2>
        Editing slider
    </h2>
    <div id="ls-main-nav-bar">
        <a href="#" class="settings active">
            <i class="dashicons dashicons-admin-tools"></i>
            Slider Settings
        </a>
        <a href="#" class="layers">
            <i class="dashicons dashicons-images-alt"></i>
            Slides
        </a>
        <a href="#" class="clear unselectable"></a>
    </div>
    <script type="text/javascript" class="ls-hidden" id="ls-posts-json">window.lsPostsJSON = [];</script>
    <div id="ls-pages">
        <div class="ls-page ls-settings ls-slider-settings active">
            <!-- Slider title -->
            <div class="ls-slider-titlewrap">
                <input type="text" name="title" value="<?php echo $slider['properties']['title'] ?>" id="title" autocomplete="off" placeholder="Type your slider name here">
            </div>
            <div class="ls-box ls-settings">
                <h3 class="header medium">Slider Settings</h3>
                <div class="inner">
                    <ul class="ls-settings-sidebar">
                        <li class="active"><i class="dashicons dashicons-editor-distractionfree"></i>Layout</li>
                        <li><i class="dashicons dashicons-editor-video"></i>Slideshow</li>
                        <li class="codemirror"><i class="dashicons dashicons-admin-appearance"></i>Appearance</li>
                        <li><i class="dashicons dashicons-image-flip-horizontal"></i>Navigation Area</li>
                        <li><i class="dashicons dashicons-screenoptions"></i>Thumbnail Navigation</li>
                        <li><i class="dashicons dashicons-video-alt3"></i>Videos</li>
                        <li><i class="dashicons dashicons-admin-generic"></i>Misc</li>
                        <li class="codemirror"><i class="dashicons dashicons-admin-post"></i>YourLogo</li>
                    </ul>

                    <div class="ls-settings-contents">
                        <table>

                            <!-- Layout -->
                            <tbody class="active">
                                <tr><th colspan="3"><?php echo 'Slider dimensions'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['width']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['width'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['width']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['height']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['height'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['height']['desc']; ?></td>
                                </tr>
                                <tr><th colspan="3"><?php echo 'Responsive mode'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['responsiveness']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['responsiveness'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['responsiveness']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['maxWidth']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['maxWidth'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['maxWidth']['desc']; ?></td>
                                </tr>
                                <tr><th colspan="3"><?php echo 'Full-width slider settings'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['fullWidth']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['fullWidth'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['fullWidth']['desc']; ?></td>
                                </tr>
<!--                                <tr>
                                    <td><?php echo $lsDefaults['slider']['responsiveUnder']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['responsiveUnder'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['responsiveUnder']['desc']; ?></td>
                                </tr>-->
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['layersContainer']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['layersContainer'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['layersContainer']['desc']; ?></td>
                                </tr>
                                <tr><th colspan="3"><?php echo 'Other settings'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['hideOnMobile']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['hideOnMobile'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['hideOnMobile']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['hideUnder']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['hideUnder'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['hideUnder']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['hideOver']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['hideOver'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['hideOver']['desc']; ?></td>
                                </tr>
                            </tbody>

                            <!-- Slideshow -->
                            <tbody>
                                <tr><th colspan="3"><?php echo 'Slideshow behavior'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['autoStart']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['autoStart'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['autoStart']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['startInViewport']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['startInViewport'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['startInViewport']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['pauseOnHover']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['pauseOnHover'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['pauseOnHover']['desc']; ?></td>
                                </tr>
                                <tr><th colspan="3"><?php echo 'Starting slide options'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['firstSlide']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['firstSlide'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['firstSlide']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['animateFirstSlide']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['animateFirstSlide'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['animateFirstSlide']['desc']; ?></td>
                                </tr>
                                <tr><th colspan="3"><?php echo 'Slideshow navigation'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['keybNavigation']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['keybNavigation'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['keybNavigation']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['touchNavigation']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['touchNavigation'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['touchNavigation']['desc']; ?></td>
                                </tr>
                                <tr><th colspan="3"><?php echo 'Looping'; ?></th></tr>
                                <tr>
                                    <td><?php echo 'Loops'; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['loops'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo 'Number of loops if automatically start slideshow is enabled (0 means infinite!)'; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['forceLoopNumber']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['forceLoopNumber'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['forceLoopNumber']['desc']; ?></td>
                                </tr>
                                <tr><th colspan="3"><?php echo 'Other settings'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['twoWaySlideshow']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['twoWaySlideshow'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['twoWaySlideshow']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['shuffle']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['shuffle'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['shuffle']['desc']; ?></td>
                                </tr>
                            </tbody>
                            <!-- Appearance -->
                            <tbody>
                                <tr>
                                    <td><?php echo 'Skin'; ?></td>
                                    <td>

                                        <?php $slider['properties']['skin'] = empty($slider['properties']['skin']) ? $lsDefaults['slider']['skin']['value'] : $slider['properties']['skin'] ?>
                                        <?php
                                        $select_skin = 'v5';
                                        if (isset($slider['properties']['skin'])) {
                                            $select_skin = $slider['properties']['skin'];
                                        }
                                        echo Form::select('skin', $lsDefaults['slider']['skin_list'], $select_skin);
                                        ?>

                                    </td>
                                    <td class="desc"><?php echo "You can change the skin of the slider. The 'noskin' skin is a border- and buttonless skin. Your custom skins will appear in the list when you create their folders as well."; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['globalBGColor']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['globalBGColor'], $slider['properties'], array('class' => 'input ls-colorpicker minicolors-input')) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['globalBGColor']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo 'Background image'; ?></td>
                                    <td>
                                        <?php $bgImage = !empty($slider['properties']['backgroundimage']) ? $slider['properties']['backgroundimage'] : null; ?>
                                        <?php $bgImageId = !empty($slider['properties']['backgroundimageId']) ? $slider['properties']['backgroundimageId'] : null; ?>
                                        <input type="hidden" name="backgroundimageId" value="333333">
                                        <input type="hidden" name="backgroundimage" id="backgroundimageselec" value="<?php echo!empty($slider['properties']['backgroundimage']) ? $slider['properties']['backgroundimage'] : '' ?>">
                                        <div class="ls-image ls-upload" data-img-id="backgroundimage-img" data-modal-id="Selectfile" data-link="http://pub.com//asset/filemanager/dialog.php?type=1&amp;field_id=backgroundimageselec">
                                            <div><img id="backgroundimage-img" src="<?php echo!empty($bgImage) ? $bgImage : asset('asset/slider/img/not_set.png') ?>" alt=""></div>
                                            <a href="#" class="dashicons dashicons-dismiss"></a>
                                        </div>
                                    </td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['globalBGImage']['desc']; ?></td>
                                </tr>

                                <tr>
                                    <td><?php echo $lsDefaults['slider']['sliderFadeInDuration']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['sliderFadeInDuration'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['sliderFadeInDuration']['desc']; ?></td>
                                </tr>

                                <tr>
                                    <td>
                                        <?php echo 'Custom slider CSS'; ?> <br>
                                    </td>
                                    <td colspan="2">
                                        <textarea name="sliderstyle" class="ls-codemirror" cols="30" rows="10"><?php echo!empty($slider['properties']['sliderstyle']) ? $slider['properties']['sliderstyle'] : $lsDefaults['slider']['sliderStyle']['value'] ?></textarea>
                                    </td>
                                </tr>
                            </tbody>
                            <!-- Navigation Area -->
                            <tbody>
                                <tr><th colspan="3"><?php echo 'Show navigation buttons'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['navPrevNextButtons']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['navPrevNextButtons'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['navPrevNextButtons']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['navStartStopButtons']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['navStartStopButtons'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['navStartStopButtons']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['navSlideButtons']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['navSlideButtons'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['navSlideButtons']['desc']; ?></td>
                                </tr>
                                <tr><th colspan="3"><?php echo 'Navigation buttons on hover'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['hoverPrevNextButtons']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['hoverPrevNextButtons'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['hoverPrevNextButtons']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['hoverSlideButtons']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['hoverSlideButtons'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['hoverSlideButtons']['desc']; ?></td>
                                </tr>
                                <tr><th colspan="3"><?php echo 'Slideshow timers'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['barTimer']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['barTimer'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['barTimer']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['circleTimer']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['circleTimer'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['circleTimer']['desc']; ?></td>
                                </tr>
                            </tbody>
                            <!-- Thumbnail navigation -->
                            <tbody>
                                <tr><th colspan="3"><?php echo 'Appearance'; ?></th></tr>
                                <tr>
                                    <td><?php echo 'Thumbnail navigation'; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetSelect($lsDefaults['slider']['thumbnailNavigation'], $slider['properties']) ?></td>
                                    <td class="desc"></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['thumbnailAreaWidth']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['thumbnailAreaWidth'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['thumbnailAreaWidth']['desc']; ?></td>
                                </tr>
                                <tr><th colspan="3"><?php echo 'Thumbnail dimensions'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['thumbnailWidth']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['thumbnailWidth'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['thumbnailWidth']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['thumbnailHeight']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['thumbnailHeight'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['thumbnailHeight']['desc']; ?></td>
                                </tr>
                                <tr><th colspan="3"><?php echo 'Thumbnail appearance'; ?></th></tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['thumbnailActiveOpacity']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['thumbnailActiveOpacity'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['thumbnailActiveOpacity']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['thumbnailInactiveOpacity']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['thumbnailInactiveOpacity'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['thumbnailInactiveOpacity']['desc']; ?></td>
                                </tr>
                            </tbody>
                            <!-- Videos -->
                            <tbody>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['autoPlayVideos']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['autoPlayVideos'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['autoPlayVideos']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['autoPauseSlideshow']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetSelect($lsDefaults['slider']['autoPauseSlideshow'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['autoPauseSlideshow']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['youtubePreviewQuality']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetSelect($lsDefaults['slider']['youtubePreviewQuality'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['youtubePreviewQuality']['desc']; ?></td>
                                </tr>
                            </tbody>
                            <!-- Misc -->
                            <tbody>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['imagePreload']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['imagePreload'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['imagePreload']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['lazyLoad']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['lazyLoad'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['lazyLoad']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['relativeURLs']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['slider']['relativeURLs'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['relativeURLs']['desc']; ?></td>
                                </tr>
                            </tbody>

                            <!-- YourLogo -->
                            <tbody>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['yourLogoImage']['name']; ?></td>
                                    <td>
                                        <?php $slider['properties']['yourlogo'] = !empty($slider['properties']['yourlogo']) ? $slider['properties']['yourlogo'] : null; ?>
                                        <?php $slider['properties']['yourlogoId'] = !empty($slider['properties']['yourlogoId']) ? $slider['properties']['yourlogoId'] : null; ?>
                                        <input type="hidden" name="yourlogoId" value="<?php echo!empty($slider['properties']['yourlogoId']) ? $slider['properties']['yourlogoId'] : '' ?>">
                                        <input type="hidden" name="yourlogo" id="yourlogo-hidden" value="<?php echo!empty($slider['properties']['yourlogo']) ? $slider['properties']['yourlogo'] : '' ?>">
                                        <div class="ls-image ls-upload" data-img-id="yourlogo-img" data-modal-id="Selectfile" data-link="{{Asset('')}}/asset/filemanager/dialog.php?type=1&amp;field_id=yourlogo-hidden">
                                            <div><img id="yourlogo-img" src="<?php echo!empty($slider['properties']['yourlogo']) ? $slider['properties']['yourlogo'] : asset('asset/slider/img/not_set.png') ?>" alt=""></div>
                                            <a href="#" class="dashicons dashicons-dismiss"></a>
                                        </div>
                                    </td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['yourLogoImage']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['yourLogoStyle']['name']; ?></td>
                                    <td colspan="2">
                                        <textarea name="yourlogostyle" class="ls-codemirror" cols="30" rows="10"><?php echo!empty($slider['properties']['yourlogostyle']) ? $slider['properties']['yourlogostyle'] : $lsDefaults['slider']['yourLogoStyle']['value'] ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['yourLogoLink']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['slider']['yourLogoLink'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['yourLogoLink']['desc']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $lsDefaults['slider']['yourLogoTarget']['name']; ?></td>
                                    <td><?php \ADMIN\SliderController::lsGetSelect($lsDefaults['slider']['yourLogoTarget'], $slider['properties']) ?></td>
                                    <td class="desc"><?php echo $lsDefaults['slider']['yourLogoTarget']['desc']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="ls-page">
            <!-- Slide tabs -->
            <div id="ls-layer-tabs">
                <?php foreach ($slider['layers'] as $key => $layer) : ?>
                    <?php $active = empty($key) ? 'active' : '' ?>
                    <a href="#" class="<?php echo $active ?>">Slide #<?php echo ($key + 1) ?><span class="dashicons dashicons-dismiss"></span></a>
                <?php endforeach; ?>
                <a href="#"  title="Add new slide" class="unsortable" id="ls-add-layer"><i class="dashicons dashicons-plus"></i></a>
                <div class="unsortable clear"></div>
            </div>
            <!-- Slides -->
            <div id="ls-layers">
                <?php if (!empty($slider['layers'])) : ?>
                    <?php
                    $count_slide = 0;
                    foreach ($slider['layers'] as $key => $layer) :
                        ?>
                        <?php
                        $layerProps = !empty($layer['properties']) ? $layer['properties'] : array();
                        $active = empty($key) ? 'active' : '';
                        $layerProps['background'] = !empty($layerProps['background']) ? $layerProps['background'] : null;
                        $layerProps['backgroundId'] = !empty($layerProps['backgroundId']) ? $layerProps['backgroundId'] : null;
                        $layerProps['thumbnailId'] = !empty($layerProps['thumbnailId']) ? $layerProps['thumbnailId'] : null;
                        $layerProps['thumbnail'] = !empty($layerProps['thumbnail']) ? $layerProps['thumbnail'] : null;
                        ?>
                        <div class="ls-box ls-layer-box <?php echo $active ?>">
                            <table>
                                <thead class="ls-layer-options-thead">
                                    <tr>
                                        <td colspan="3">
                                            <i class="dashicons dashicons-welcome-write-blog"></i>
                                            <h4><?php echo 'Slide Options'; ?></h4>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody class="ls-slide-options">
                                <input type="hidden" name="post_offset" value="<?php echo isset($layerProps['post_offset']) ? $layerProps['post_offset'] : '-1' ?>">
                                <input type="hidden" name="3d_transitions" value="<?php echo isset($layerProps['3d_transitions']) ? $layerProps['3d_transitions'] : '' ?>">
                                <input type="hidden" name="2d_transitions" value="<?php echo isset($layerProps['2d_transitions']) ? $layerProps['2d_transitions'] : '' ?>">
                                <input type="hidden" name="custom_3d_transitions" value="<?php echo isset($layerProps['custom_3d_transitions']) ? $layerProps['custom_3d_transitions'] : '' ?>">
                                <input type="hidden" name="custom_2d_transitions" value="<?php echo isset($layerProps['custom_2d_transitions']) ? $layerProps['custom_2d_transitions'] : '' ?>">
                                <tr>
                                    <td valign="top">
                                        <h3 class="subheader">Slide image &amp; thumbnail</h3>
                                        <div class="inner slide-image">

                                            <?php $background = !empty($layerProps['background']) ? $layerProps['background'] : '' ?>
                                            <?php
                                            if ($background == '[image-url]') {
                                                $src = asset('asset/slider') . '/img/blank.gif';
                                            } else {
                                                $src = $layerProps['background'];
                                            }
                                            ?>

                                            <input type="hidden" name="backgroundId" value="<?php echo!empty($layerProps['backgroundId']) ? $layerProps['backgroundId'] : '' ?>">
                                            <input type="hidden" name="background" id="background-hidden-<?php echo $count_slide; ?>" value="<?php echo!empty($layerProps['background']) ? $layerProps['background'] : '' ?>">
                                            <div class="ls-image ls-upload" data-img-id="background-img-<?php echo $count_slide; ?>" data-modal-id="Selectfile" data-link="{{Asset('')}}/asset/filemanager/dialog.php?type=1&amp;field_id=background-hidden-<?php echo $count_slide; ?>" data-help="<?php echo $lsDefaults['slides']['image']['tooltip'] ?>">
                                                <div><img id="background-img-<?php echo $count_slide; ?>" src="<?php echo!empty($src) ? $src : asset('asset/slider/img/not_set.png') ?>" alt=""></div>
                                                <a href="#" class="dashicons dashicons-dismiss"></a>
                                            </div>
                                            <?php echo 'or'; ?> <a href="#" class="ls-url-prompt"><?php echo 'enter URL'; ?></a> 
                                        </div>
                                        <div class="hsep"></div>
                                        <div class="inner slide-image">
                                            <input type="hidden" name="thumbnailId" value="<?php echo!empty($layerProps['thumbnailId']) ? $layerProps['thumbnailId'] : '' ?>">
                                            <input type="hidden" name="thumbnail" id="thumbnail-hidden-<?php echo $count_slide; ?>" value="<?php echo!empty($layerProps['thumbnail']) ? $layerProps['thumbnail'] : '' ?>">
                                            <div class="ls-image ls-upload"  data-img-id="thumbnail-img-<?php echo $count_slide; ?>" data-modal-id="Selectfile" data-link="{{Asset('')}}/asset/filemanager/dialog.php?type=1&amp;field_id=thumbnail-hidden-<?php echo $count_slide; ?>" data-help="<?php echo $lsDefaults['slides']['thumbnail']['tooltip'] ?>">
                                                <div><img id="thumbnail-img-<?php echo $count_slide; ?>" src="<?php echo!empty($layerProps['thumbnailId']) ? $layerProps['thumbnailId'] : asset('asset/slider/img/not_set.png') ?>" alt=""></div>
                                                <a href="#"  class="dashicons dashicons-dismiss"></a>
                                            </div>
                                            <?php echo 'or'; ?> <a href="#" class="ls-url-prompt"><?php echo 'enter URL'; ?></a>
                                        </div>
                                    </td>
                                    <td valign="top" class="second">
                                        <h3 class="subheader">Duration</h3>
                                        <?php \ADMIN\SliderController::lsGetInput($lsDefaults['slides']['delay'], $layerProps, array('class' => 'layerprop')) ?> ms
                                        <h3 class="subheader">
                                            Transition
                                        </h3>
                                        <button type="button" class="button ls-select-transitions new" data-help="<?php echo 'You can select your desired slide transitions by clicking on this button.'; ?>">Select transitions</button><br><br>
                                        <?php echo $lsDefaults['slides']['timeshift']['name'] ?><br>
                                        <?php \ADMIN\SliderController::lsGetInput($lsDefaults['slides']['timeshift'], $layerProps, array('class' => 'layerprop')) ?> ms
                                    </td>
                                    <td valign="top">
                                        <h3 class="subheader">Linking</h3>
                                        <div class="ls-slide-link">
                                            <?php \ADMIN\SliderController::lsGetInput($lsDefaults['slides']['linkUrl'], $layerProps, array('placeholder' => $lsDefaults['slides']['linkUrl']['name'])) ?>
                                            <br> <?php \ADMIN\SliderController::lsGetSelect($lsDefaults['slides']['linkTarget'], $layerProps) ?>
                                            <span> or <a href="#"><?php echo 'use post URL'; ?></a></span>
                                        </div> 
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <table>
                                <thead>
                                    <tr>
                                        <td>
                                            <i class="dashicons dashicons-editor-video ls-preview-icon"></i>
                                            <h4>
                                                <span><?php echo 'Preview'; ?></span>
                                                <div class="ls-editor-zoom">
                                                    <span class="ls-editor-slider-text">Size:</span>
                                                    <div class="ls-editor-slider"></div>
                                                    <span class="ls-editor-slider-val">100%</span>
                                                </div>
                                            </h4>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="ls-preview-td">
                                            <div class="ls-preview-wrapper">
                                                <div class="ls-preview">
                                                    <div class="draggable ls-layer"></div>
                                                </div>
                                                <div class="ls-real-time-preview"></div>
                                            </div>
                                            <button type="button" class="button btn btn-primary ls-preview-button"><?php echo 'Enter Preview'; ?></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table>
                                <thead>
                                    <tr>
                                        <td>
                                            <div class="dashicons dashicons-images-alt ls-layers-icon"></div>
                                            <h4><?php echo 'Layers'; ?><a href="#" class="ls-tl-toggle">[ <?php echo 'timeline view'; ?> ]</a></h4>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody class="ls-sublayers ls-sublayer-sortable">
                                    <?php if (!empty($layer['sublayers'])) : ?>
                                        <?php
                                        $dem = 0;
                                        foreach ($layer['sublayers'] as $key => $sublayer) :
                                            ?>
                                            <?php $active = (count($layer['sublayers']) == ($key + 1)) ? ' class="active"' : '' ?>
                                            <?php $title = empty($sublayer['subtitle']) ? 'Layer #' . ($key + 1) . '' : htmlspecialchars(stripslashes($sublayer['subtitle'])); ?>
                                            <tr<?php echo $active ?>>
                                                <td>
                                                    <div class="ls-sublayer-wrapper">
                                                        <span class="ls-sublayer-sortable-handle dashicons dashicons-menu"></span>
                                                        <span class="ls-sublayer-number"><?php echo ($key + 1) ?></span>
                                                        <input type="text" name="subtitle" class="ls-sublayer-title" value="<?php echo $title ?>">

                                                        <div class="ls-tl">
                                                            <table>
                                                                <tr>
                                                                    <td data-help="Delay in: " class="ls-tl-delayin"></td>
                                                                    <td data-help="Duration in: " class="ls-tl-durationin"></td>
                                                                    <td data-help="Show Until: " class="ls-tl-showuntil"></td>
                                                                    <td data-help="Duration out: " class="ls-tl-durationout"></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <span class="ls-sublayer-controls">
                                                            <span class="ls-highlight dashicons dashicons-lightbulb" data-help="<?php echo 'Highlight layer in the editor.'; ?>"></span>
                                                            <span class="ls-icon-lock dashicons dashicons-lock" data-help="<?php echo 'Prevent layer from dragging in the editor.'; ?>"></span>
                                                            <span class="ls-icon-eye dashicons dashicons-visibility" data-help="<?php echo 'Hide layer in the editor.'; ?>"></span>
                                                        </span>
                                                        <div class="clear"></div>
                                                        <div class="ls-sublayer-nav">
                                                            <a href="#" class="active"><?php echo 'Content'; ?></a>
                                                            <a href="#"><?php echo 'Transition'; ?></a>
                                                            <a href="#"><?php echo 'Link'; ?></a>
                                                            <a href="#"><?php echo 'Styles'; ?></a>
                                                            <a href="#"><?php echo 'Attributes'; ?></a>
                                                            <a href="#" title="<?php echo 'Remove this layer'; ?>" class="dashicons dashicons-dismiss remove"></a>
                                                        </div>
                                                        <div class="ls-sublayer-pages">
                                                            <div class="ls-sublayer-page ls-sublayer-basic active">

                                                                <!-- Layer Media Type -->
                                                                <?php
                                                                if (empty($sublayer['media'])) {
                                                                    switch ($sublayer['type']) {
                                                                        case 'img': $sublayer['media'] = 'img';
                                                                            break;
                                                                        case 'div': $sublayer['media'] = 'html';
                                                                            break;
                                                                        default: $sublayer['media'] = 'text';
                                                                            break;
                                                                    }
                                                                }
                                                                $sublayer['type'] = ($sublayer['type'] == 'span') ? 'p' : $sublayer['type'];
                                                                $sublayer['type'] = ($sublayer['type'] == 'img') ? 'p' : $sublayer['type'];
                                                                ?>
                                                                <input type="hidden" name="media" value="<?php echo $sublayer['media'] ?>">
                                                                <div class="ls-layer-kind">
                                                                    <ul>
                                                                        <li data-section="img"><span class="dashicons dashicons-format-image"></span><?php echo 'Image'; ?></li>
                                                                        <li data-section="text"><span class="dashicons dashicons-text"></span><?php echo 'Text'; ?></li>
                                                                        <li data-section="html"><span class="dashicons dashicons-video-alt3"></span><?php echo 'HTML / Video / Audio'; ?></li>

                                                                    </ul>
                                                                </div>
                                                                <!-- End of Layer Media Type -->

                                                                <!-- Layer Element Type -->
                                                                <input type="hidden" name="type" value="<?php echo $sublayer['type'] ?>">
                                                                <ul class="ls-sublayer-element">
                                                                    <li class="ls-type" data-element="p"><?php echo 'Paragraph'; ?></li>
                                                                    <li class="ls-type" data-element="h1"><?php echo 'H1'; ?></li>
                                                                    <li class="ls-type" data-element="h2"><?php echo 'H2'; ?></li>
                                                                    <li class="ls-type" data-element="h3"><?php echo 'H3'; ?></li>
                                                                    <li class="ls-type" data-element="h4"><?php echo 'H4'; ?></li>
                                                                    <li class="ls-type" data-element="h5"><?php echo 'H5'; ?></li>
                                                                    <li class="ls-type" data-element="h6"><?php echo 'H6'; ?></li>
                                                                </ul>
                                                                <!-- End of Layer Element Type -->
                                                                <div class="ls-layer-sections">
                                                                    <!-- Image Layer -->
                                                                    <div class="ls-image-uploader slide-image clearfix ls-hidden">
                                                                        <?php
                                                                        if ($sublayer['image'] == '[image-url]') {
                                                                            $src = asset('asset/slider') . '/img/blank.gif';
                                                                        } else {
                                                                            $src = $sublayer['image'];
                                                                        }
                                                                        ?>
                                                                        <?php $sublayer['imageId'] = !empty($sublayer['imageId']) ? $sublayer['imageId'] : null; ?>
                                                                        <input type="hidden" name="imageId"  value="<?php echo!empty($sublayer['imageId']) ? $sublayer['imageId'] : '' ?>">
                                                                        <input type="hidden" name="image" id="image_id_<?php echo $count_slide; ?>_<?php echo $dem; ?>" value="<?php echo!empty($sublayer['image']) ? $sublayer['image'] : '' ?>">
                                                                        <div class="ls-image ls-upload " data-img-id="image-img-<?php echo $count_slide; ?>-<?php echo $dem; ?>" data-modal-id="Selectfile" data-link="{{Asset('')}}/asset/filemanager/dialog.php?type=1&amp;field_id=image_id_<?php echo $count_slide; ?>_<?php echo $dem; ?>">
                                                                            <div><img id="image-img-<?php echo $count_slide; ?>-<?php echo $dem; ?>" src="<?php echo!empty($src) ? $src : asset('asset/slider/img/not_set.png') ?>" alt=""></div>
                                                                            <a href="#"  class="dashicons dashicons-dismiss"></a>
                                                                        </div>
                                                                        <p>
                                                                            <?php echo 'Click on the image preview to open WordPress Media Library or'; ?>
                                                                            <a href="#" class="ls-url-prompt"><?php echo 'insert from URL'; ?></a> or
                                                                            <a href="#" class="ls-post-image"><?php echo 'use post image'; ?></a>.
                                                                        </p>
                                                                    </div>
                                                                    <!-- Text/HTML Layer -->
                                                                    <div class="ls-html-code">
                                                                        <textarea name="html" cols="50" rows="5" placeholder="Enter layer content here" data-help="<?php echo 'Type here the contents of your layer. You can use any HTML codes in this field to insert content others then text. This field is also shortcode-aware, so you can insert content from other plugins as well as video embed codes.'; ?>"><?php echo stripslashes($sublayer['html']) ?></textarea>

                                                                    </div>

                                                                </div>

                                                            </div>
                                                            <div class="ls-sublayer-page ls-sublayer-options">
                                                                <input type="hidden" name="transition">
                                                                <?php
                                                                if (!isset($sublayer['transition'])) {
                                                                    switch ($sublayer['slidedirection']) {
                                                                        case 'fade': $sublayer['fadein'] = true;
                                                                            $sublayer['offsetxin'] = '0';
                                                                            $sublayer['offsetyin'] = '0';
                                                                            break;
                                                                        case 'auto': $sublayer['offsetxin'] = 'right';
                                                                            $sublayer['offsetyin'] = '0';
                                                                            break;
                                                                        default:
                                                                            $sublayer['offsetxin'] = ($sublayer['slidedirection'] == 'left' || $sublayer['slidedirection'] == 'right') ? $sublayer['slidedirection'] : 0;
                                                                            $sublayer['offsetyin'] = ($sublayer['slidedirection'] == 'top' || $sublayer['slidedirection'] == 'bottom') ? $sublayer['slidedirection'] : 0;
                                                                            break;
                                                                    }

                                                                    switch ($sublayer['slideoutdirection']) {
                                                                        case 'fade': $sublayer['fadeout'] = true;
                                                                            $sublayer['offsetxout'] = '0';
                                                                            $sublayer['offsetyout'] = '0';
                                                                            break;
                                                                        case 'auto':
                                                                            if ($sublayer['slidedirection'] == 'fade') {
                                                                                $sublayer['fadeout'] = true;
                                                                                $sublayer['offsetxout'] = '0';
                                                                            } else {
                                                                                $sublayer['offsetxout'] = 'right';
                                                                            }
                                                                            break;
                                                                        default:
                                                                            $sublayer['offsetxout'] = ($sublayer['slideoutdirection'] == 'left' || $sublayer['slideoutdirection'] == 'right') ? $sublayer['slideoutdirection'] : 0;
                                                                            $sublayer['offsetyout'] = ($sublayer['slideoutdirection'] == 'top' || $sublayer['slideoutdirection'] == 'bottom') ? $sublayer['slideoutdirection'] : 0;
                                                                            break;
                                                                    }

                                                                    $sublayer['scalexin'] = $sublayer['scalein'];
                                                                    $sublayer['scaleyin'] = $sublayer['scalein'];
                                                                    $sublayer['scalexout'] = $sublayer['scaleout'];
                                                                    $sublayer['scaleyout'] = $sublayer['scaleout'];
                                                                }

                                                                $sublayer['transition'] = !empty($sublayer['transition']) ? json_decode(stripslashes($sublayer['transition']), true) : array();
                                                                $sublayer = array_merge($sublayer, $sublayer['transition']);
                                                                ?>
                                                                <table>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td rowspan="3"><?php echo 'Transition in'; ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionInOffsetX']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInOffsetX'], $sublayer, array('class' => 'sublayerprop')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionInOffsetY']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInOffsetY'], $sublayer, array('class' => 'sublayerprop')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionInDuration']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInDuration'], $sublayer, array('class' => 'sublayerprop')) ?> ms</td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionInDelay']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInDelay'], $sublayer, array('class' => 'sublayerprop')) ?> ms</td>
                                                                            <td class="right"><a href="http://easings.net/" target="_blank"><?php echo $lsDefaults['layers']['transitionInEasing']['name'] ?></a></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetSelect($lsDefaults['layers']['transitionInEasing'], $sublayer, array('class' => 'sublayerprop', 'options' => $lsDefaults['easings'])) ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionInFade']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['layers']['transitionInFade'], $sublayer, array('class' => 'sublayerprop')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionInRotate']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInRotate'], $sublayer, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionInRotateX']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInRotateX'], $sublayer, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionInRotateY']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInRotateY'], $sublayer, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                                            <td colspan="2" rowspan="2" class="center">
                                                                                <?php echo $lsDefaults['layers']['transitionInTransformOrigin']['name'] ?><br>
                                                                                <?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInTransformOrigin'], $sublayer, array('class' => 'sublayerprop')) ?>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionInSkewX']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInSkewX'], $sublayer, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionInSkewY']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInSkewY'], $sublayer, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionInScaleX']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInScaleX'], $sublayer, array('class' => 'sublayerprop')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionInScaleY']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionInScaleY'], $sublayer, array('class' => 'sublayerprop')) ?></td>
                                                                        </tr>
                                                                        <tr class="ls-separator"><td colspan="11"></td></tr>
                                                                        <tr>
                                                                            <td rowspan="3"><?php echo 'Transition out'; ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionOutOffsetX']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutOffsetX'], $sublayer, array('class' => 'sublayerprop')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionOutOffsetY']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutOffsetY'], $sublayer, array('class' => 'sublayerprop')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionOutDuration']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutDuration'], $sublayer, array('class' => 'sublayerprop')) ?> ms</td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['showUntil']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['showUntil'], $sublayer, array('class' => 'sublayerprop')) ?> ms</td>
                                                                            <td class="right"><a href="http://easings.net/" target="_blank"><?php echo $lsDefaults['layers']['transitionOutEasing']['name'] ?></a></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetSelect($lsDefaults['layers']['transitionOutEasing'], $sublayer, array('class' => 'sublayerprop', 'options' => $lsDefaults['easings'])) ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionOutFade']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetCheckbox($lsDefaults['layers']['transitionOutFade'], $sublayer, array('class' => 'sublayerprop')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionOutRotate']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutRotate'], $sublayer, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionOutRotateX']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutRotateX'], $sublayer, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionOutRotateY']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutRotateY'], $sublayer, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                                            <td colspan="2" rowspan="2" class="center">
                                                                                <?php echo $lsDefaults['layers']['transitionOutTransformOrigin']['name'] ?><br>
                                                                                <?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutTransformOrigin'], $sublayer, array('class' => 'sublayerprop')) ?>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionOutSkewX']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutSkewX'], $sublayer, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionOutSkewY']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutSkewY'], $sublayer, array('class' => 'sublayerprop')) ?> &deg;</td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionOutScaleX']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutScaleX'], $sublayer, array('class' => 'sublayerprop')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionOutScaleY']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionOutScaleY'], $sublayer, array('class' => 'sublayerprop')) ?></td>
                                                                        </tr>
                                                                        <tr class="ls-separator"><td colspan="11"></td></tr>
                                                                        <tr>
                                                                            <td rowspan="3"><?php echo 'Other options'; ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['transitionParallaxLevel']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['transitionParallaxLevel'], $sublayer, array('class' => 'sublayerprop')) ?></td>
                                                                            <td class="right"><?php echo 'Hidden'; ?></td>
                                                                            <td><input type="checkbox" name="skip" class="checkbox" data-help="<?php echo "If you don't want to use this layer, but you want to keep it, you can hide it with this switch."; ?>" <?php echo isset($sublayer['skip']) ? 'checked="checked"' : '' ?>></td>
                                                                            <td colspan="6"><button type="button" class="button duplicate" data-help="<?php echo 'If you would like to use similar settings for other layers or to experiment on a copy, you can duplicate this layer.'; ?>"><?php echo 'Duplicate this layer'; ?></button></td>
                                                                        </tr>
                                                                </table>
                                                            </div>
                                                            <div class="ls-sublayer-page ls-sublayer-link">
                                                                <table>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="ls-slide-link">
                                                                                    <?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['linkURL'], $sublayer, array('placeholder' => $lsDefaults['layers']['linkURL']['name'])) ?>
                                                                                    <br> <?php \ADMIN\SliderController::lsGetSelect($lsDefaults['layers']['linkTarget'], $sublayer) ?>
                                                                                    <span> or <a href="#"><?php echo 'use post URL'; ?></a></span>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="ls-sublayer-page ls-sublayer-style">
                                                                <?php $layerStyles = !empty($sublayer['styles']) ? json_decode($sublayer['styles'], true) : array(); ?>
                                                                <?php
                                                                if ($layerStyles === null) {
                                                                    $layerStyles = json_decode(stripslashes($sublayer['styles']), true);
                                                                }
                                                                ?>
                                                                <?php $sublayer['styles'] = $layerStyles; ?>
                                                                <input type="hidden" name="styles">
                                                                <table>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td><?php echo 'Layout & Positions'; ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['width']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['width'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['height']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['height'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['top']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['top'], $sublayer) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['left']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['left'], $sublayer) ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?php echo 'Padding'; ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['paddingTop']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['paddingTop'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['paddingRight']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['paddingRight'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['paddingBottom']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['paddingBottom'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['paddingLeft']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['paddingLeft'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?php echo 'Border'; ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['borderTop']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['borderTop'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['borderRight']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['borderRight'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['borderBottom']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['borderBottom'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['borderLeft']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['borderLeft'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?php echo 'Font'; ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['fontFamily']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['fontFamily'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['fontSize']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['fontSize'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['lineHeight']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['lineHeight'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['color']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['color'], $sublayer['styles'], array('class' => 'auto ls-colorpicker')) ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?php echo 'Misc'; ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['background']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['background'], $sublayer['styles'], array('class' => 'auto ls-colorpicker')) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['borderRadius']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['borderRadius'], $sublayer['styles'], array('class' => 'auto')) ?></td>
                                                                            <td class="right"><?php echo 'Word-wrap'; ?></td>
                                                                            <td colspan="3"><input type="checkbox" name="wordwrap" data-help="<?php echo 'If you use custom sized layers, you have to enable this setting to wrap your text.'; ?>" class="checkbox"<?php echo isset($sublayer['wordwrap']) ? ' checked="checked"' : '' ?>></td>
                                                                        </tr>
                                                                        <tr class="ls-separator"><td colspan="11"></td></tr>
                                                                        <tr>
                                                                            <td><?php echo 'Custom CSS'; ?></td>
                                                                            <td colspan="8"><textarea rows="5" cols="50" name="style" class="style" data-help="<?php echo 'If you want to set style settings other then above, you can use here any CSS codes. Please make sure to write valid markup.'; ?>"><?php echo!empty($sublayer['style']) ? stripslashes($sublayer['style']) : '' ?></textarea></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="ls-sublayer-page ls-sublayer-attributes">
                                                                <table>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td><?php echo 'Attributes'; ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['ID']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['ID'], $sublayer) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['class']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['class'], $sublayer) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['title']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['title'], $sublayer) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['alt']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['alt'], $sublayer) ?></td>
                                                                            <td class="right"><?php echo $lsDefaults['layers']['rel']['name'] ?></td>
                                                                            <td><?php \ADMIN\SliderController::lsGetInput($lsDefaults['layers']['rel'], $sublayer) ?></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                </td>
                                            </tr>
                                            <?php
                                            $dem++;
                                        endforeach;
                                        ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            <a href="#" class="ls-add-sublayer">
                                <i class="dashicons dashicons-plus"></i> <?php echo 'Add new layer'; ?>
                            </a>
                        </div>
                        <?php
                        $count_slide++;
                    endforeach;
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="ls-publish">
        <button type="submit" class="button btn btn-primary button-primary button-hero">Save changes</button>
    </div>
</form>
<script type="text/javascript">

    // Plugin path
    var pluginPath = '<?php echo asset(''); ?>asset/slider/';
    // Transition images
    var lsTrImgPath = '<?php echo asset(''); ?>asset/slider/img/';
    // New Media Library
    var newMediaUploader = true;
    var ajaxurl = '<?php echo action('\ADMIN\SliderController@postSaveSlider'); ?>';
</script>
@endsection