@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.users.users.file')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Quản lý lịch sử hình thành
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")

{{Form::open(array('action' => '\ADMIN\HistoryController@postEdit','id'=>'edit_news','class'=>'form-horizontal'))}}
    
    {{Form::hidden('id_history', $data_news[0]->history_id)}}
<div class="row">
    <div class="col-md-12">  
	@include('admin.template.error')
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#content-tab1" data-toggle="tab"><i class="fa fa-globe fa-lg text-default"></i> Nội dung chính</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade  active in" id="content-tab1"> 	
				<div class="form-group">
                    <label class="col-lg-2 control-label">
                        <a href="javascript:void(0)" data-img-div="div_image_selected" data-img-id="image_hidden" data-img-check="1" data-modal-id="Selectfile" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=imageselect" class="btn btn-white select_image"><i class="fa fa-plus text"></i> Ảnh đại diện (345x393)</a>
                    </label>
                    <div class="col-lg-9">
                        <div id="div_image_selected">
                            <div class="thumbnail  pull-left m-l-small item">
                                <a href="{{$data_news[0]->avatar}}" target="_blank">
                                    <img src="{{$data_news[0]->avatar}}"></a><span data-img-id="image_hidden"> <i class="fa fa-times"></i> </span>
                            </div>
                        </div>
                        {{Form::hidden('image_hidden', $data_news[0]->avatar, array('id'=>'image_hidden'))}}
                        {{Form::hidden('imageselect', $data_news[0]->avatar, array('id'=>'imageselect'))}}
                    </div> 
                </div>
                <div class="form-group">
                        <label class="col-lg-4 control-label">Thời gian</label>
                        <div class="col-lg-8">
                            <div class="input-group input-medium date date-picker" data-date="{{($data_news[0]->timeline!=0)?date('d-m-Y',$data_news[0]->timeline):date('d-m-Y',time())}}" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                <input type="text" class="form-control" name="news_time" value="{{($data_news[0]->timeline!=0)?date('d-m-Y',$data_news[0]->timeline):date('d-m-Y',time())}}"/>
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>  
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">

                                <?php
                                foreach ($l_lang as $item) {
                                    if ($item->id == $g_config_all->website_lang) {
                                        ?>
                                        <a href="#lang_p_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                        <?php
                                    }
                                }
                                ?>

                            </li>
                            <?php if (count($l_lang) > 1) { ?>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                    <ul class="dropdown-menu text-left">
                                        <?php
                                        foreach ($l_lang as $item_child) {
                                            if ($item_child->id != $g_config_all->website_lang) {
                                                ?>
                                                <li> <a href="#lang_p_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>   
                        <?php } ?>
                        </ul>  
                    
                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                $tran_class='';
                                    foreach ($l_lang as $item_content) {
                                        $data=[];
                                        foreach($data_news as $item_data){
                             
                                            if($item_data->langid==$item_content->id){
                                            
                                                $data=$item_data;
                                            }
                                        }
                                        ?>
                                        <div class="tab-pane fade in <?php
                                        if ($item_content->id == $g_config_all->website_lang) {
                                            echo ' active';
                                        }
                                        ?> " id="lang_p_{{$item_content->id}}">
                                        <h3>{{$item_content->name}}</h3>               
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label" for="name_{{$item_content->code}}">Tiêu đề</label>
                                                    <div class="col-lg-6">
                                                        <?php 
                                                        $name='';
                                                        if(count($data)>0 && isset($data->name)){
                                                            $name=$data->name;
                                                        }
                                                        ?>
                                                        {{Form::text('name_'.$item_content->code, $name, array('class'=>'bg-focus form-control input-sm', 'id'=>'name_'.$item_content->code))}}
                                                        <input type="hidden" name="lang_id_{{$item_content->code}}" value="{{$item_content->id}}"/>
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label" for="content_{{$item_content->code}}">Nội dung</label>
                                                    <div class="col-lg-6">
                                                        <?php 
                                                        $content='';
                                                        if(count($data)>0 && isset($data->content)){
                                                            $content=$data->content;
                                                        }
                                                        ?>
                                                        {{Form::textarea('content_'.$item_content->code, $content, array('class'=>'bg-focus form-control input-sm tinymce', 'id'=>'content_'.$item_content->code))}}
                                                      
                                                    </div> 
                                                </div>
                                            </div>
                                            
                                        </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
                     
        </div>
    </div>
  
</div>
{{Form::close()}}
<style>
    .popover .arrow{
        display: none;
    }
</style>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#edit_news').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection