<?php
if (count($data_news_log) == 0) {
    ?>
    <tr>    <td colspan="5">No data</td>  </tr>
    <?php
} else {
    foreach ($data_news_log as $item) {
        ?>
        <tr>   
            <td>{{date('d/m/Y H:i:s',strtotime($item->created_at))}}</td>
            <td>
                <?php
                foreach (Config::get('all.all_user') as $item_user) {
                    if ($item_user->id == $item->user_id) {
                        echo $item_user->full_name;
                    }
                }
                ?>
            </td>
            <td>{{$item->action}}</td>
            <td>
                <?php if (!empty($item->content)) { ?>
                    <a href="javascript:;;" class="view_detail_log">Chi tiết</a>
                    <input type="hidden" value='{{$item->content}}'/>
                <?php } ?>
            </td>
        </tr>
        <tr class="pagination_child">  
            <td colspan="4">
                <?php
                echo $data_news_log->links();
                ?>
            </td>
        </tr>
        <?php
    }
}
?>