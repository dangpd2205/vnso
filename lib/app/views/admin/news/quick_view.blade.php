<td colspan="10">
    <section class="panel" style="margin-bottom: 0px;">
        <header class="panel-heading text-right">
            <ul class="nav nav-tabs pull-left">
                <li class="active"><a href="#tabs-des-{{$data_news->news_id}}-1" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> Lịch sử tác động</a></li>
            </ul> 
        </header>
        <div class="panel-body" style="padding: 0px;">
            <div class="tab-content">
                <div class="tab-pane fade active in scroll-y" style="max-height: 250px;" id="tabs-des-{{$data_news->news_id}}-1">
                    <table class="table table-striped m-b-none text-small">
                        <thead>
                            <tr>
                                <th>Thời gian</th>
                                <th>Tài khoản</th>
                                <th>Sự kiện</th>
                                <th>Chi tiết</th>
                            </tr>
                        </thead>
                        <tbody data-id-post="{{$data_news->id}}" data-lang-id="{{$data_news->lang_id}}">
                                @include('admin.news.quick_view_ajax')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</td>