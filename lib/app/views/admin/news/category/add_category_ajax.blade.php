<h3>{{$item_content->name}}</h3>
<div class="dd nestable" id="nestable_{{$item_content->id}}">
    <ol class="dd-list">
        <?php
        foreach ($data as $item) {
            if ($item->lang_id == $item_content->id) {
                if ($item->parent == 0) {
                    ?>
                    <li class="dd-item dd3-item" data-id="{{$item->id}}">
                        <div class="dd-handle dd3-handle"></div>
                        <div class="dd3-content">
                            <input type="hidden"  data-edit="id"  name="id" value="{{$item->id}}"/>
                            <input type="hidden" data-edit="product_add_cat_name"  name="name" value="{{str_limit($item->name,70,'...')}}"/>
                            
                            <input type="hidden" data-edit="product_add_cat_description" name="description" value="{{strip_tags($item->description)}}"/>

                            <strong id="{{$item->id}}" data-id="{{$item->id}}"  data-insert="name"  class="edit_inline"> {{str_limit($item->name,40,'...')}}</strong>
                            <span class="action_menu">
                                <a href="{{URL::action('\ADMIN\NewsController@getEditCate')}}/{{$item->id}}" class=""><i class="fa fa-pencil"></i></a>
                                <a href="javascript:void(0);" class="delete_row_table" data-id="{{$item->id}}"><i class="fa fa-trash-o"></i></a>     
                            </span>
                            <span id="des_{{$item->id}}" data-id="{{$item->id}}" data-insert="description"  class="link_menu edit_inline">{{strip_tags(str_limit($item->description,150,'...'))}}</span>
                        </div>
                        <?php
                        $check_child_1 = 0;
                        foreach ($data as $item_check) {
                            if ($item_check->parent == $item->id) {
                                $check_child_1++;
                            }
                        }
                        if ($check_child_1 > 0) {
                            echo '<ol class="dd-list">';
                            foreach ($data as $item1) {
                                if ($item1->parent == $item->id) {
                                    ?>
                                <li class="dd-item dd3-item" data-id="{{$item1->id}}">
                                    <div class="dd-handle dd3-handle"></div>
                                    <div class="dd3-content">
                                        <input type="hidden"  data-edit="id"  name="id" value="{{$item1->id}}"/>
                                        <input type="hidden" data-edit="product_add_cat_name"  name="name" value="{{str_limit($item1->name,70,'...')}}"/>
                                        
                                        <input type="hidden" data-edit="product_add_cat_description" name="description" value="{{strip_tags($item1->description)}}"/>
                                        <strong id="{{$item1->id}}" data-id="{{$item1->id}}"  data-insert="name"  class="edit_inline"> {{str_limit($item1->name,40,'...')}}</strong>
                                        <span class="action_menu">
                                            <a href="{{URL::action('\ADMIN\NewsController@getEditCate')}}/{{$item1->id}}" class=""><i class="fa fa-pencil"></i></a>
                                            <a href="javascript:void(0);" class="delete_row_table" data-id="{{$item1->id}}"><i class="fa fa-trash-o"></i></a>     
                                        </span>
                                        <span id="des_{{$item1->id}}" data-id="{{$item1->id}}" data-insert="description"  class="link_menu edit_inline">{{strip_tags(str_limit($item1->description,150,'...'))}}</span>
                                    </div>
                                    <?php
                                    $check_child_2 = 0;
                                    foreach ($data as $item_check1) {
                                        if ($item_check1->parent == $item1->id) {
                                            $check_child_2++;
                                        }
                                    }
                                    if ($check_child_2 > 0) {
                                        echo '<ol class="dd-list">';
                                        foreach ($data as $item2) {
                                            if ($item2->parent == $item1->id) {
                                                ?>
                                            <li class="dd-item dd3-item" data-id="{{$item2->id}}">
                                                <div class="dd-handle dd3-handle"></div>
                                                <div class="dd3-content">
                                                    <input type="hidden"  data-edit="id"  name="id" value="{{$item2->id}}"/>
                                                    <input type="hidden" data-edit="product_add_cat_name"  name="name" value="{{str_limit($item2->name,70,'...')}}"/>
                                                    
                                                    <input type="hidden" data-edit="product_add_cat_description" name="description" value="{{strip_tags($item2->description)}}"/>
                                                    <strong id="{{$item2->id}}" data-id="{{$item2->id}}"  data-insert="name"  class="edit_inline"> {{str_limit($item2->name,40,'...')}}</strong>
                                                    <span class="action_menu">
                                                        <a href="{{URL::action('\ADMIN\NewsController@getEditCate')}}/{{$item2->id}}" class=""><i class="fa fa-pencil"></i></a>
                                                        <a href="javascript:void(0);" class="delete_row_table" data-id="{{$item2->id}}"><i class="fa fa-trash-o"></i></a>
                                                    </span>
                                                    <span id="des_{{$item2->id}}" data-id="{{$item2->id}}" data-insert="description"  class="link_menu edit_inline">{{strip_tags(str_limit($item2->description,150,'...'))}}</span>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                    }
                                    echo '</ol>';
                                }
                                ?>
                                </li>
                                <?php
                            }
                        }
                        echo '</ol>';
                    }
                    ?>
                    </li>

                    <?php
                }
            }
        }
        ?>
    </ol>
</div>
@if(count($data)==0)
<tr>
    <td colspan="5">NO DATA</td>
</tr>
@endif
