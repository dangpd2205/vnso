<!-- Thêm nhóm sản phẩm -->
<div class="modal fade"  id="add_new_category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Thêm danh mục</h4>
            </div>
            <div class="modal-body">
                {{Form::open(array('action'=>'\ADMIN\NewsController@postQuickAddNewsCategory', 'class'=>'form-horizontal', 'id'=>'form_add_category'))}}
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <?php
                                foreach ($l_lang as $item) {
                                    if ($item->id == $g_config_all->website_lang) {
                                        ?>
                                        <a href="#lang_p_add_cat_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                        <?php
                                    }
                                }
                                ?>
                            </li>
                            <?php if (count($l_lang) > 1) { ?>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                    <ul class="dropdown-menu text-left">
                                        <?php
                                        foreach ($l_lang as $item_child) {
                                            if ($item_child->id != $g_config_all->website_lang) {
                                                ?>
                                                <li> <a href="#lang_p_add_cat_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>
                        <?php } ?>
                        </ul>
                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                foreach ($l_lang as $item_content) {
                                    ?>
                                    <div class="tab-pane fade in <?php
                                    if ($item_content->id == $g_config_all->website_lang) {
                                        echo ' active';
                                    }
                                    ?> " id="lang_p_add_cat_{{$item_content->id}}">
                                        <h3>{{$item_content->name}}</h3>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="product_add_cat_name_{{$item_content->code}}">Tên nhóm</label>
                                            <div class="col-lg-9">
                                                {{Form::text('product_add_cat_name_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'product_add_cat_name_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="product_add_cat_parent_{{$item_content->code}}">Cha</label>
                                            <div class="col-lg-9">
                                                <select id="product_add_cat_parent_{{$item_content->code}}" class="bg-focus form-control" name="product_add_cat_parent_{{$item_content->code}}">
                                                    <option value="">None</option>                                              
                                                    <?php
                                                    foreach ($all_category as $item_cat) {
                                                        if ($item_cat->lang_id == $item_content->id) {
                                                            if ($item_cat->parent == 0) {
                                                                ?>
                                                                <option value="<?php echo $item_cat->id ?>"><?php echo $item_cat->name ?></option>
                                                                <?php
                                                                foreach ($all_category as $item_cat1) {
                                                                    if ($item_cat1->parent == $item_cat->id) {
                                                                        echo '<option value="' . $item_cat1->id . '"> — ' . $item_cat1->name . '</option>';
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="product_add_cat_description_{{$item_content->code}}">Mô tả</label>
                                            <div class="col-lg-9">
                                                {{Form::textarea('product_add_cat_description_'.$item_content->code, null, array('class'=>'bg-focus form-control translate_google', 'id'=>'product_add_cat_description_'.$item_content->code,'rows'=>'2'))}}
                                            </div> 
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>


                            </div>
                        </div>
                    </div>
                </div>
                {{Form::close()}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" onclick="qickAddCat('form_add_category', 'add_new_category')">Thêm mới</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="Selectfile" tabindex="1" role="dialog" aria-labelledby="SelectfileLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ls">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="SelectfileLabel">Chọn file</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input_hidden_img_id">
                <input type="hidden" id="input_hidden_img_check">
                <input type="hidden" id="input_hidden_img_div">
                <script>
                    function responsive_filemanager_callback(fieldID) {
                        var id_img = $("#input_hidden_img_id").val();
                        var url_img = $("#" + fieldID).val();
                        var div_img = $("#input_hidden_img_div").val();
                        var check_img = $("#input_hidden_img_check").val();
                        var html = '<div class="thumbnail  pull-left m-l-small item"><a href="' + url_img + '" target="_blank"><img src="' + url_img + '"/></a><span data-img-id="' + id_img + '"> <i class="fa fa-times"></i> </span></div>';
                        if (check_img == 0) {
                            $('#Selectfile').modal('hide');
                        }
                        if (check_img == 1) {
                            $("#" + div_img).html(html);
                            $("#" + id_img).val(url_img.replace('http://' + window.location.hostname, ''));
                            $('#Selectfile').modal('hide');

                        } else {
                            $("#" + div_img).append(html);
                            var images = $(".thumbnail.pull-left.m-l-small").find("img").map(function () {
                                return this.getAttribute('src');
                            }).get();
                            for (i = 0; i < images.length; i++) {
                                images[i] = images[i].replace('http://' + window.location.hostname, '');
                            }
                            $("#" + id_img).val(images);
                            $('#Selectfile').modal('hide');
                        }
                    }
                </script>
                <iframe width="100%" height="400" src="" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; " __idm_frm__="29">
                </iframe>
            </div>
        </div>
    </div>
</div>