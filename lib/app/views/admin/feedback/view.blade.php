@section("title")
Quản lý phản hồi
@endsection
@section("description")
Quản lý phản hồi
@endsection
@section("modal_option")

@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Phản hồi
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
@include('admin.template.alert_ajax')
<div class="row">
    <div class="col-md-12">  
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database"></i>Dữ liệu</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <!--                    <a href="javascript:;" class="reload" data-url="/hahahah" data-error-display="toastr"  data-error-display-text="FUCK YOU" data-original-title="WHAT THE FUCK" title=""> </a>-->
                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-md-9 col-sm-12">
                                <div class="dataTables_length">
                                    {{Form::open(array('url' => '#','class'=>'row_setting_form'))}}
                                    <input type="hidden" name="url_search" value="{{action('\ADMIN\FeedBackController@postShow')}}"/>
                                    <label>
                                        <select name="row_table_setting" class="form-control input-sm input-xsmall input-inline">
                                            <option value="10">10</option>
                                            <option value="15">15</option>
                                            <option value="20">20</option>
                                            <option value="30">30</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select> 
                                        dòng</label>
                                    {{Form::close()}}
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                {{Form::open(array('url' => '#','class'=>'search_form'))}}
                                <input type="hidden" name="url_search" value="{{action('\ADMIN\FeedBackController@postSearch')}}"/>
                                <div class="input-group input-group-sm">
                                    <input type="text" class="form-control" name="search_key" placeholder="Search for...">
                                    <span class="input-group-btn">
                                        <button class="btn red" type="submit"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>  
                        <div id="dataTables_content" class="dataTables_wrapper no-footer dataTables_content" data-quick-url="">                 
                            @include('admin.feedback.ajax')         
                        </div>
                    </div>
          
            </div>
        </div>
    </div>
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="multi_delete('{{action('\ADMIN\FeedBackController@postDeleteMulti')}}', 'dataTables_content')" target="_blank" class="active">
                <span>Xóa</span>
                <i class="icon-trash"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection
