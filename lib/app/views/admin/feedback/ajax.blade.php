<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
				<th scope="col" style="width: 1%;" ></th>
                <th>E-mail</th>
                <th>Tên</th>
                <th>Tiêu đề</th>
                <th>Nội dung</th>
                <th>Thời gian</th>
            </tr>
        </thead>
        <tbody id="tbl_content_ajax">
            @if(count($data)==0)
            <tr>
                <td colspan="6">NO DATA</td>
            </tr>
            @endif
            <?php foreach ($data as $item): ?>
                <tr>
					<td class="text-center">
						<input type="checkbox" name="id[]" value="{{$item->id}}">
					</td>
                    <td>
                        {{$item->email}}
                    </td>
                    <td>
                        {{$item->name}}
                    </td>
                    <td>
                        {{$item->subject}}
                    </td>
                    <td>
                        {{$item->content}}
                    </td>
                    <td>
                        {{$item->created_at}}
                    </td>
                </tr>
            <?php endforeach; ?>

            </tbody>
    </table>
</div>
@if($data->links()!='')
<div class="row">
    <div class="col-md-5 col-sm-12">
        <div class="dataTables_info" id="sample_2_info" role="status" aria-live="polite">Đang hiển thị trang {{$data->getCurrentPage()}} đến {{$data->getLastPage()}} của {{$data->getTotal()}} dữ liệu</div>
    </div>
    <div class="col-md-7 col-sm-12">
        <div class="dataTables_paginate paging_bootstrap_number paginate_normal" >
            {{$data->links('admin.template_admin.pagination')}} 
        </div>
    </div>
</div>
@endif