@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")

@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Cấu hình<i class="fa fa-circle"></i></li>
<li>
	Menu
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
@include('admin.template.alert_ajax')
@include('admin.menu.modal')
@include('admin.modal.popup')
<div class="row">
    <div class="col-md-12">  
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database"></i>Dữ liệu</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <!--                    <a href="javascript:;" class="reload" data-url="/hahahah" data-error-display="toastr"  data-error-display-text="FUCK YOU" data-original-title="WHAT THE FUCK" title=""> </a>-->
                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="panel-body">
                        <style type="text/css">
                            /**
                             * Nestable
                             */

                            .dd { position: relative; display: block; margin: 0; padding: 0; list-style: none; font-size: 13px; line-height: 20px; }

                            .dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
                            .dd-list .dd-list { padding-left: 30px; }
                            .dd-collapsed .dd-list { display: none; }

                            .dd-item,
                            .dd-empty,
                            .dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }

                            .dd-handle { display: block;
                                         height: 30px;
                                         margin: 5px 0; 
                                         padding: 4px 10px; 
                                         color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
                                         background: #F5F6F7;
                                         -webkit-border-radius: 3px;
                                         border-radius: 3px;
                                         box-sizing: border-box; -moz-box-sizing: border-box;
                            }


                            .dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
                            .dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
                            .dd-item > button[data-action="collapse"]:before { content: '-'; }

                            .dd-placeholder,
                            .dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
                            .dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
                                        background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                                            -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
                                        background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                                            -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
                                        background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                                            linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
                                        background-size: 60px 60px;
                                        background-position: 0 0, 30px 30px;
                            }

                            .dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
                            .dd-dragel > .dd-item .dd-handle { margin-top: 0; }
                            .dd-dragel .dd-handle {
                                -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
                                box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
                            }

                            .action_menu .fa:first-child {
                                margin-right: 5px;
                            }
                            .action_menu {
                                border-left: 1px solid #ccc;
                                float: right;
                                width: 80px;
                                padding-left: 20px;
                            }
                            .link_menu {
                                border-left: 1px solid #ccc;
                                float: right;
                                padding: 0 5px 0px 15px;
                                white-space: nowrap;
                                width: 220px;
                                overflow: hidden;
                            }
                            @media (max-width: 768px){

                                .link_menu {
                                    display: none;
                                }  
                            }
                            /**
                 * Nestable Draggable Handles
                 */

                            .dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
                                           background: #fafafa;
                                           background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
                                           background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
                                           background:         linear-gradient(top, #fafafa 0%, #eee 100%);
                                           -webkit-border-radius: 3px;
                                           border-radius: 3px;
                                           box-sizing: border-box; -moz-box-sizing: border-box;
                            }
                            .dd3-content:hover {background: #fff; }

                            .dd-dragel > .dd3-item > .dd3-content { margin: 0; }

                            .dd3-item > button { margin-left: 30px; }

                            .dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
                                          border: 1px solid #1DB399;
                                          background: #23d4b5;
                                          border-top-right-radius: 0;
                                          border-bottom-right-radius: 0;
                            }
                            .dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
                            .dd3-handle:hover { background: #23d4b5; }
                        </style>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <input type="hidden" name="id_edit" id="id_edit">
                                <label class="col-lg-2 control-label" for="name_group"><strong style="line-height: 30px;">Tên menu</strong></label>
                                <div class="col-lg-10">
                                    <input id="name_group" class="bg-focus form-control" data-link="{{action('\ADMIN\MenuController@postRenameMenuGroup')}}" name="name_group" type="text" value="{{$data->name}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8" id="content_menu_edit">
                            @include('admin.menu.ajax_edit')
                        </div>
                        <div  class="col-lg-4">
                            <h4>Thêm menu</h4>
                            {{Form::open(array('action'=>'\ADMIN\MenuController@postAddMenuChild','id'=>'form_add_menu'))}}
                            {{Form::hidden('group_id', $data->id,['id'=>'group_id'])}}
                            {{Form::hidden('action_change', action('\ADMIN\MenuController@postUpdateMenuChild'),['id'=>'action_change'])}}
                            {{Form::hidden('menu_id_edit', '',['id'=>'menu_id_edit'])}}
                            <div class="form-group">
                                <label class="col-lg-12 control-label" for="menu_name">Title menu</label>
                                <div class="col-lg-12">   
                                    {{Form::text('name', null, array('class'=>'bg-focus form-control', 'id'=>'menu_name'))}}
                                </div> 
                            </div>
                            <div class="form-group">
                                <label class="col-lg-12 control-label" for="urlselectoption" style="padding-top: 5px;">Chọn</label>
                                <div class="col-lg-12">   
                                    <select id="urlselectoption" class="bg-focus form-control" onchange="$('#menu_url').val($(this).val())">
                                        <optgroup label="Trang chủ">
                                            <option value="{{asset('')}}">Trang chủ</option>
                                            <option value="{{URL::route('contact')}}">Liên hệ</option>
                                            <option value="{{URL::route('orchestra')}}">Nghệ sỹ</option>
											<option value="{{URL::route('sponsor')}}">Tài trợ</option>
                                        </optgroup>
										<optgroup label="Tin tức">
                                           <option value="{{URL::route('tintuc')}}">Tin tức</option>
										   <?php		
											function showCategories($categories, $parent_id = 0, $char = '')
											{
												foreach ($categories as $key => $item)
												{													if($item['lang_id'] == 1){														$slang = 'Vietnamese';													}else{														$slang = 'English';													}
													
													if ($item['parent'] == $parent_id)
													{
														$url = URL::route('route_data',$item["slug"].'cn');
														echo '<option value="'.$url.'">';															
															echo $char . $item['name'].'('.$slang.')';
														echo '</option>';
														 
														
														unset($categories[$key]);
														 
														
														showCategories($categories,$item['id'], $char.'|-');
													}
												}
											}
											showCategories($all_news_cate);
											?>
                                        </optgroup>
                                        <optgroup label="Chương trình">
                                           <option value="{{URL::route('chuongtrinh')}}">Chương trình</option>
                                           <?php        
                                            function showCategories1($categories, $parent_id = 0, $char = '')
                                            {
                                                foreach ($categories as $key => $item)
                                                {
                                                    if($item['lang_id'] == 1){														$slang = 'Vietnamese';													}else{														$slang = 'English';													}
                                                    if ($item['parent'] == $parent_id)
                                                    {
                                                        $url = URL::route('route_data',$item["slug"].'cs');
                                                        echo '<option value="'.$url.'">';
                                                            echo $char . $item['name'].'('.$slang.')';
                                                        echo '</option>';
                                                         
                                                        
                                                        unset($categories[$key]);
                                                         
                                                        
                                                        showCategories1($categories,$item['id'], $char.'|-');
                                                    }
                                                }
                                            }
                                            showCategories1($all_show_cate);
                                            ?>
                                        </optgroup>
                                        <optgroup label="Trang tĩnh">
                                            <?php
                                            if (isset($all_page) && count($all_page) > 0) {
                                                foreach ($all_page as $item) {
                                                    ?>
                                                    <option value="{{URL::route('route_data',$item->page_slug.'pg')}}">{{$item->page_name}} (@if($item->lang_id==1) Vietnamese @else English @endif) </option>
                                                <?php
                                                }
                                            }
                                            ?>
                                        </optgroup>  
                                        <optgroup label="Thư viện">
                                            <option value="{{URL::route('gallery')}}">Thư viện ảnh</option>
                                             <option value="{{URL::route('video')}}">Video</option>
                                        </optgroup>
                                        <optgroup label="Video">
                                            @if(isset($video) && count($video)>0)
                                            @foreach($video as $i_video)
                                            <option value="{{URL::route('route_data',$i_video->id.'dv')}}">{{$i_video->name}} (@if($i_video->lang_id==1) Vietnamese @else English @endif)</option>
                                            @endforeach
                                            @endif
                                            
                                        </optgroup>
                                    </select>
                                </div> 
                            </div>
                            <div class="form-group">
                                <label class="col-lg-12 control-label" for="menu_url" style="padding-top: 5px;">URL</label>
                                <div class="col-lg-12" style="margin-bottom:10px;">   
                                    {{Form::text('url', null, array('class'=>'bg-focus form-control', 'id'=>'menu_url'))}}
                                </div> 
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12 pull-right m-top15"> 
                                    <a href="javascript:;" class="btn btn-primary" onclick="add_menu('form_add_menu')" ><i class="fa fa-plus"></i> Áp dụng</a>
                                </div>
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="{{action('\ADMIN\MenuController@getView')}}" class="active">
                <span>Quay về</span>
                <i class="icon-arrow-left"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
<script>
    var url_update_position = "{{action('\ADMIN\MenuController@postUpdateMenuChild')}}";
</script>
@endsection
