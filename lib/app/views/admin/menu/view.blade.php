@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")

@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Cấu hình <i class="fa fa-circle"></i></li>
<li>
	Menu
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
@include('admin.template.alert_ajax')
@include('admin.menu.modal')
@include('admin.modal.popup')
<div class="row">
    <div class="col-md-12">  
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database"></i>Dữ liệu</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    
                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="dataTables_wrapper no-footer dataTables_content" data-quick-url="">                 
                        <div class="table-scrollable">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 1%;" ></th>
                                        <th scope="col">Tên</th>
                                        <th scope="col" style="width: 10%;">Edit</th>
                                    </tr>
                                </thead>
                                <tbody id="tbl_content_menu_ajax">
                                     @include('admin.menu.ajax')
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="open_modal('add_menu_group')" target="_blank" class="active">
                <span>Thêm mới</span>
                <i class="icon-plus"></i>
            </a>
        </li>
        <li>
            <a href="javascript:void(0)" onclick="multi_delete_menu('{{action('\ADMIN\MenuController@postDeleteMultiMenuGroup')}}', 'tbl_content_menu_ajax')" target="_blank" class="active">
                <span>Xóa</span>
                <i class="icon-trash"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>

@endsection
