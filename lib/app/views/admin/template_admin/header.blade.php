<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="vi">
    <head>
        <meta charset="utf-8" />
        <title>Admin Dashboard</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{Asset('asset/admin')}}/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/jquery-notific8/jquery.notific8.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css" />
  
        <!-- END PAGE LEVEL PLUGINS -->
		<link href="{{Asset('asset/admin')}}/global/plugins/bootstrap-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{Asset('asset/admin')}}/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{Asset('asset/admin')}}/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{Asset('asset/admin')}}/layouts/custom.min.css" rel="stylesheet" type="text/css" />
        <script src="{{Asset('asset/admin')}}/global/plugins/jquery.min.js" type="text/javascript"></script>
        
        <link href="{{Asset('asset/admin')}}/custom.css" rel="stylesheet" type="text/css" />
		
        <style>
            .checkbox_child{
                list-style:none;
            }
        </style>
        <!-- END THEME LAYOUT STYLES -->
     </head>
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <div class="page-header navbar navbar-fixed-top">
                <div class="page-header-inner ">
                    <div class="page-logo">
                        <a href="">
                            <img src="{{Asset('asset/admin')}}/layouts/layout/img/logo1.png" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>					<a class="sms_notify">Số dư SMS: {{number_format($total)}}</a>
                    <div class="top-menu">
                        
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default">{{count($order_count) + $comment_count}} </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>
                                           <span class="bold">Thông báo</span> </h3>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                            <li>
                                                <a href="{{action('\ADMIN\OrderController@getView')}}">
                                                    <span class="time"></span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-file"></i>
                                                        </span> {{count($order_count)}} đơn hàng COD
													</span>
                                                </a>
                                            </li>
											<li>
                                                <a href="{{action('\ADMIN\CommentController@getView')}}">
                                                    <span class="time"></span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-comment"></i>
                                                        </span> {{$comment_count}} bình luận mới
													</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN INBOX DROPDOWN -->
                          
                            <!-- BEGIN TODO DROPDOWN -->
                            
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="@if(\Auth::user()->avatar!='') {{\Auth::user()->avatar}} @else {{Asset('asset/admin')}}/layouts/layout/img/avatar3_small.jpg @endif" />
                                    <span class="username username-hide-on-mobile"> {{\Auth::user()->full_name}} </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="{{Route('getprofile')}}">
                                            <i class="icon-user"></i> Cá nhân </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="{{Route('lock_scrreen')}}">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li>
                                    <li>
                                        <a href="{{Route('admin_logout')}}">
                                            <i class="icon-key"></i> Đăng xuất </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
            @yield('modal_option')
            <div class="page-container">
                @include('admin.template_admin.menu')