<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
			<?php $current_url = \Request::url();   ?>
            <li class="nav-item start">
                <a href="{{Asset('pub-admin')}}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Trang chủ</span>
                    <span class="selected"></span>
                </a>
            </li>
		
			<?php
			$c1=$c2=$c3=$c4=$c5=$c6=$c7=$c8=$c9=$c10=$c11=$c12=$c13=$c14=$c15=$c16=$c17=$c18=$c19=$c20=$c21=$c22=$c23=$p1='';
			if($current_url == URL::action('\ADMIN\NewsController@getView')){
				$c1="open active";
				$p1="open active";
			}else if($current_url == URL::action('\ADMIN\NewsController@getCategoryView')){
				$c2="open active";
				$p1="open active";
			}else if($current_url == URL::action('\ADMIN\OrderController@getView')){
				$c10="open active";
				$p1="open active";
			}else if($current_url == URL::action('\ADMIN\VideoController@getView')){
				$c3="open active";
				$p1="open active";
			}else if($current_url == URL::action('\ADMIN\GalleryController@getView')){
				$c8="open active";
				$p1="open active";
			}else if($current_url == URL::action('\ADMIN\ManufController@getView')){
				$c11="open active";
				$p1="open active";
			}else if($current_url == URL::action('\ADMIN\PageController@getView')){
                $c7="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\AuthorController@getView')){
                $c9="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\ShowController@getView')){
                $c12="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\HistoryController@getView')){
                $c14="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\ArtistController@getView')){
                $c15="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\ArtistController@getCategoryView')){
                $c16="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\PlaceController@getView')){
                $c17="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\TicketController@getView')){
                $c18="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\SheetController@getView')){
                $c19="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\ShowController@getCategoryView')){
                $c20="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\CommentController@getView')){
                $c21="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\SubcribeController@getView')){
                $c22="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\BookShowController@getView')){
                $c4="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\OrderController@getView')){
                $c5="open active";
                $p1="open active";
            }else if($current_url == URL::action('\ADMIN\SponsorController@getView')){
                $c23="open active";
                $p1="open active";
            }

			?>
            <li class="nav-item {{$p1}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-feed"></i>
                    <span class="title">Nội dung</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{$c1}} ">
                        <a href="{{URL::action('\ADMIN\NewsController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý tin tức</span>
                        </a>
                    </li>
                    <li class="nav-item {{$c2}} ">
                        <a href="{{URL::action('\ADMIN\NewsController@getCategoryView')}}" class="nav-link ">
                            <span class="title">Quản lý danh mục tin tức</span>
                        </a>
                    </li>
                    <li class="nav-item {{$c12}} ">
                        <a href="{{URL::action('\ADMIN\ShowController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý chương trình</span>
                        </a>
                    </li>
                    <li class="nav-item {{$c20}} ">
                        <a href="{{URL::action('\ADMIN\ShowController@getCategoryView')}}" class="nav-link ">
                            <span class="title">Quản lý danh mục chương trình</span>
                        </a>
                    </li>
                    <li class="nav-item {{$c14}} ">
                        <a href="{{URL::action('\ADMIN\HistoryController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý lịch sử hình thành</span>
                        </a>
                    </li>
                    <li class="nav-item {{$c15}} ">
                        <a href="{{URL::action('\ADMIN\ArtistController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý nghệ sỹ</span>
                        </a>
                    </li>
                    <li class="nav-item {{$c14}} ">
                        <a href="{{URL::action('\ADMIN\ArtistController@getCategoryView')}}" class="nav-link ">
                            <span class="title">Quản lý danh mục nghệ sỹ
                    </li>
					<li class="nav-item {{$c3}} ">
                        <a href="{{URL::action('\ADMIN\VideoController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý Video</span>
                        </a>
                    </li>
					<li class="nav-item {{$c8}} ">
                        <a href="{{URL::action('\ADMIN\GalleryController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý Gallery</span>
                        </a>
                    </li>
                    <li class="nav-item {{$c7}} ">
                        <a href="{{URL::action('\ADMIN\PageController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý trang tĩnh</span>
                        </a>
                    </li>
					<li class="nav-item {{$c11}} ">
                        <a href="{{URL::action('\ADMIN\ManufController@getView')}}" class="nav-link ">
                            <span class="title">Doanh nghiệp tài trợ</span>
                        </a>
                    </li>
                    <li class="nav-item {{$c9}} ">
                        <a href="{{URL::action('\ADMIN\AuthorController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý tác giả</span>
                        </a>
                    </li>
					 <li class="nav-item {{$c17}} ">
                        <a href="{{URL::action('\ADMIN\PlaceController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý địa điểm diễn</span>
                        </a>
                    </li>
					 <li class="nav-item {{$c18}} ">
                        <a href="{{URL::action('\ADMIN\TicketController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý loại vé</span>
                        </a>
                    </li>
                    <li class="nav-item {{$c19}} " style="display:none;">
                        <a href="{{URL::action('\ADMIN\SheetController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý bản nhạc</span>
                        </a>
                    </li>
                    <li class="nav-item {{$c21}} ">
                        <a href="{{URL::action('\ADMIN\CommentController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý bình luận</span>
                        </a>
                    </li>
					<li class="nav-item {{$c22}} ">
                        <a href="{{URL::action('\ADMIN\SubcribeController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý subcribe</span>
                        </a>
                    </li>
					<li class="nav-item {{$c4}} ">
                        <a href="{{URL::action('\ADMIN\BookShowController@getView')}}" class="nav-link ">
                            <span class="title">Đặt vé</span>
                        </a>
                    </li>
                    <li class="nav-item {{$c5}} ">
                        <a href="{{URL::action('\ADMIN\OrderController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý đơn vé</span>
                        </a>
                    </li>
					<li class="nav-item {{$c23}} ">
                        <a href="{{URL::action('\ADMIN\SponsorController@getView')}}" class="nav-link ">
                            <span class="title">Đăng ký tài trợ</span>
                        </a>
                    </li>
                </ul>
            </li>
			<?php
			$c1=$c2=$c3=$c4=$c5=$c6=$p2=$p1='';
			if($current_url == URL::action('\ADMIN\UserController@getView')){
				$c2="open active";
				$p1="open active";
			}else if($current_url == URL::action('\ADMIN\UserController@getGroup')){
                $c1="open active";
                $p1="open active";
            }
			?>
            <li class="nav-item {{$p1}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Tài khoản</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
					
                    <li class="nav-item {{$c2}} ">
                        <a href="{{URL::action('\ADMIN\UserController@getView')}}" class="nav-link ">
                            <span class="title">Nhân viên</span>
                        </a>
                    </li>	
                     <li class="nav-item {{$c1}} ">
                        <a href="{{URL::action('\ADMIN\UserController@getGroup')}}" class="nav-link ">
                            <span class="title">Nhóm quyền</span>
                        </a>
                    </li>			
                </ul>
            </li>
			
			<?php
			$c1=$c2=$c3=$p1=$p2=$c11=$c12=$c13=$c14=$c15=$c16=$c17=$c18=$c19=$c20='';
			if($current_url == URL::action('\ADMIN\SettingController@getInfo').'/info'){
				$c11="open active";
				$p1="open active";
				$c1="open active";$p2="";
			}else if($current_url == URL::action('\ADMIN\SettingController@getInfo').'/shop'){
				$c13="open active";
				$p1="open active";
				$c1="open active";$p2="";
			}else if($current_url == URL::action('\ADMIN\SettingController@getInfo').'/website'){
				$c14="open active";
				$p1="open active";
				$c1="open active";$p2="";
			}else if($current_url == URL::action('\ADMIN\SettingController@getInfo').'/email'){
				$c15="open active";
				$p1="open active";
				$c1="open active";$p2="";
			}else if($current_url == URL::action('\ADMIN\SettingController@getInfo').'/social'){
				$c16="open active";
				$p1="open active";
				$c1="open active";$p2="";
			}else if($current_url == URL::action('\ADMIN\SettingController@getInfo').'/analyntic'){
				$c17="open active";
				$p1="open active";
				$c1="open active";$p2="";
			}else if($current_url == URL::action('\ADMIN\MenuController@getView')){
				$c2="open active";
				$p1="open active";
				$p2="";
			}else if($current_url == URL::action('\ADMIN\SliderController@getSliderView')){
				$c3="open active";
				$p1="open active";
				$p2="";
			}else if($current_url == URL::action('\ADMIN\SettingController@getInfo').'/layout'){
				$c19="open active";
				$p1="open active";
				$p2 = "open active";
				$c1="open active";
			}else if($current_url == URL::action('\ADMIN\SettingController@getInfo').'/layout1'){
				$c18="open active";
				$p1="open active";
				$p2 = "open active";
				$c1="open active";
			}else if($current_url == URL::action('\ADMIN\SettingController@getInfo').'/layout2'){
				$c20="open active";
				$p1="open active";
				$p2 = "open active";
				$c1="open active";
			}
			?>
			
            <li class="nav-item {{$p1}} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">Cấu hình</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{$c1}}">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <span class="title">Cấu hình</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item {{$c11}}">
                                <a href="{{URL::action('\ADMIN\SettingController@getInfo')}}/info" class="nav-link "> Thông tin liên hệ </a>
                            </li>
							<li class="nav-item  {{$p2}}">
								<a href="javascript:;" class="nav-link nav-toggle">
									<span class="title">Giao diện</span>
									<span class="arrow open"></span>
								</a>
								<ul class="sub-menu" style="display: block;">
									<li class="nav-item  {{$c19}}">
										<a href="{{URL::action('\ADMIN\SettingController@getInfo')}}/layout" class="nav-link ">
											<span class="title">Giao diện chung</span>
										</a>
									</li>
									<li class="nav-item {{$c18}} ">
										<a href="{{URL::action('\ADMIN\SettingController@getInfo')}}/layout1" class="nav-link ">
											<span class="title">Giao diện trang tài trợ</span>
										</a>
									</li>
									<li class="nav-item {{$c20}} ">
										<a href="{{URL::action('\ADMIN\SettingController@getInfo')}}/layout2" class="nav-link ">
											<span class="title">Giao diện thông báo</span>
										</a>
									</li>
								</ul>
                            </li>
                            <li class="nav-item {{$c13}} ">
								<a href="{{URL::action('\ADMIN\SettingController@getInfo')}}/shop" class="nav-link ">
									Logo
								</a>
							</li>
							<li class="nav-item {{$c14}} ">
								<a href="{{URL::action('\ADMIN\SettingController@getInfo')}}/website" class="nav-link ">
									Website
								</a>
							</li>
							<li class="nav-item {{$c15}} ">
								<a href="{{URL::action('\ADMIN\SettingController@getInfo')}}/email" class="nav-link ">
									Email
								</a>
							</li>
							<li class="nav-item {{$c16}} ">
								<a href="{{URL::action('\ADMIN\SettingController@getInfo')}}/social" class="nav-link ">
									Mạng xã hội
								</a>
							</li>
							<li class="nav-item {{$c17}} ">
								<a href="{{URL::action('\ADMIN\SettingController@getInfo')}}/analyntic" class="nav-link ">
									Mã Google Analytics
								</a>
							</li>
                        </ul>
                    </li>
                    <li class="nav-item {{$c2}} ">
                        <a href="{{URL::action('\ADMIN\MenuController@getView')}}" class="nav-link ">
                            <span class="title">Quản lý menu</span>
                        </a>
                    </li>					<li class="nav-item {{$c3}} ">                        <a href="{{URL::action('\ADMIN\SliderController@getSliderView')}}" class="nav-link ">                            <span class="title">Quản lý slide</span>                        </a>                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>