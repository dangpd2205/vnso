@include('admin.template_admin.header')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                @yield("breadcrumb")
            </ul>
            <div class="page-toolbar">
                <div id="" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs">{{date('d/m/Y h:i:s',time())}}</span>&nbsp;
                    
                </div>
            </div>
        </div>
        @yield("content")
    </div>
</div>
<!-- END CONTAINER -->
@include('admin.template_admin.footer')