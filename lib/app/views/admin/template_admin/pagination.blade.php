<?php
$presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);
?>

<?php if ($paginator->getLastPage() > 1): ?>
    <ul class="pagination">
        <?php
        /* How many pages need to be shown before and after the current page */
        $showBeforeAndAfter = 5;

        /* Current Page */
        $currentPage = $paginator->getCurrentPage();
        $lastPage = $paginator->getLastPage();
        $start = 0;
        if ($currentPage - $showBeforeAndAfter < -1 || $lastPage < $showBeforeAndAfter) {
            $start = 1;
            if ($lastPage < $showBeforeAndAfter) {
                $end = $lastPage;
            } else {
                $end = 5;
            }
        } else {
            if ($lastPage - $currentPage > 2) {
                $start = $currentPage - 2;
                $end = $currentPage + 2;
            } else {
                $start = $lastPage - 5;
                $end = $lastPage;
            }
        }
        if ($lastPage > $showBeforeAndAfter and $currentPage != 1 and $currentPage > 3) {
            ?>
            <?php echo $presenter->getLink(1); ?>
            <?php
        }
        if ($lastPage > $showBeforeAndAfter and $currentPage != 1) {
            ?>
            <?php echo $presenter->getPrevious('‹'); ?>
            <?php
        }
        if ($lastPage != 1) {
            for ($i = $start; $i <= $end; $i++) {
                if ($i != $currentPage) {
                    ?>
                    <?php echo $presenter->getLink($i); ?>
                    <?php
                } else {
                    ?>
                    <li class="active"><span><?php echo $i; ?></span></li>
                            <?php
                        }
                    }
                }
                if ($lastPage > $showBeforeAndAfter and $currentPage != $lastPage) {
                    ?>
                    <?php echo $presenter->getNext('›'); ?>
                    <?php
                }
                if ($lastPage > $showBeforeAndAfter and $currentPage != $lastPage and $lastPage - $currentPage > 2) {
                    ?>
                    <?php echo $presenter->getLink($lastPage); ?>
                    <?php
                }
                ?>
    </ul>
<?php endif; ?>