<td colspan="10">
    <section class="panel" style="margin-bottom: 0px;">
        <table class="table table-striped m-b-none text-small"  style="table-layout: fixed;">
            <thead>
                <tr>
                    <th>Mã đơn hàng</th>
                    <th>Tổng tiền</th>
                    <th>Hình thức</th>
                    <th>Trạng thái</th>
                    <th>Thời gian</th>
                </tr>
            </thead>
            <tbody>
                @if(count($data)==0)
                <tr>
                    <td colspan="5">NO DATA</td>
                </tr>
                @endif
                <?php foreach ($data as $item): ?>
                    <tr>   
                        <td>
                            {{$item->code}}
                        </td>
                        <td>
                            {{number_format($item->total_amount, 2, '.', ',')}}
                        </td>
                        <td>
                            <?php
                            if ($item->order_type == 1) {
                                echo 'Bán online';
                            } else {
                                echo 'Bán offline';
                            }
                            ?>
                        </td>
                        <td>
                            {{Lang::get('admin/status.product_order')[$item->status]}}
                        </td>
                        <td>
                            {{$item->created_at}}
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </section>
</td>