<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Tên</th>
                    <th>Ưu đãi</th>
                    <th>Điều kiện</th>
                    <th>Đăng ký</th>
                    <th>Trạng thái</th>
                <th style="width: 10%;">Edit</th>
            </tr>
        </thead>
        <tbody id="tbl_content_ajax">
            @if(count($data)==0)
            <tr>
                <td colspan="9">NO DATA</td>
            </tr>
            @else
            <?php foreach ($data as $item): ?>
                <tr>
                    <td>
						<a href="javascript:;" data-url-post="{{action('\ADMIN\UserController@postQuickView')}}" data-current-id="{{$item->id}}" data-togglediv="tog-{{$item->id}}" class="clicktoggle_normal"> {{$item->name}}</a>
					</td>
					<td>
						{{$item->discount}}
					</td>
					<td>
						{{$item->point}}
					</td>
					<td>
						{{$item->created_at}}
					</td>
					<td>
						{{Lang::get('general.partner_status')[$item->status]}}
					</td>
					<td>
						<a href="{{action('\ADMIN\PartnerController@getEdit')}}/{{$item->id}}"> <i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
						@if($item->status!=1)  <a href="javascript:;;" title="Kích hoạt" onclick="delete_one({{$item->id}},'{{action('\ADMIN\PartnerController@postConfirm','dataTables_content')}}', 'tbl_content_ajax')"><i class="fa fa-gavel"></i></a>&nbsp;&nbsp; @endif
						@if($item->status!=2)<a href="javascript:;;" onclick="delete_one({{$item->id}},'{{action('\ADMIN\PartnerController@postDelete')}}', 'dataTables_content')"><i class="fa fa-trash-o"></i></a>@endif
					</td>
                </tr>
                <tr class="togglediv non_hover" id="tog-{{$item->id}}" style="display: none">

                </tr>
            <?php endforeach; ?>
			@endif
            </tbody>
    </table>
</div>
@if($data->links()!='')
<div class="row">
    <div class="col-md-5 col-sm-12">
        <div class="dataTables_info" id="sample_2_info" role="status" aria-live="polite">Đang hiển thị trang {{$data->getCurrentPage()}} đến {{$data->getLastPage()}} của {{$data->getTotal()}} dữ liệu</div>
    </div>
    <div class="col-md-7 col-sm-12">
        <div class="dataTables_paginate paging_bootstrap_number paginate_normal" >
            {{$data->links('admin.template_admin.pagination')}} 
        </div>
    </div>
</div>
@endif