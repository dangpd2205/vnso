@section("title")
Sửa nhóm tài khoản
@endsection
@section("description")
Sửa nhóm tài khoản
@endsection
@section("modal_option")
@include('admin.template.alert_ajax')
@include('admin.users.users.file')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Tài khoản <i class="fa fa-circle"></i></li>
<li>Khách hàng thân thiết <i class="fa fa-circle"></i></li>
<li>
	Hạng thẻ
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")

<div class="row">
    {{Form::open(array('action' => '\ADMIN\PartnerController@postEdit','id'=>'add_user','class'=>'form-horizontal'))}}
    {{Form::hidden('id', $data->id)}}
    <div class="panel-body">
        @include('admin.template.error')
        <div class="col-lg-8">
            <div class="col-lg-7">
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Tên</label>
                    <div class="col-lg-6">
                        {{Form::email('name', $data->name, array('class'=>'bg-focus form-control input-sm', 'id'=>'name'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Chiết khấu</label>
                    <div class="col-lg-6">
                        {{Form::text('discount',  $data->discount, array('class'=>'bg-focus form-control auto_number input-sm', 'id'=>'discount'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Điểm áp dụng</label>
                    <div class="col-lg-6">
                        {{Form::text('point',  $data->point, array('class'=>'bg-focus form-control auto_number input-sm', 'id'=>'point'))}}
                    </div> 
                </div>
                
            </div>
           
            <div class="form-group col-lg-12"> 
                <label class="col-lg-3 control-label no-padding-top" for="branches_id">Trạng thái</label> 
                <div class="col-lg-2">
                    
                        @if($data->status=='1')
                        <label class="mt-radio">
                            {{Form::radio('status','1', 1)}} Kích hoạt<span></span> </label>
                        @else
                        <label class="mt-radio">
                            {{Form::radio('status','1', 0 )}}  Kích hoạt<span></span> </label>
                        @endif
                    
                </div>
                <div class="col-lg-2">
                    
                        @if($data->status=='2')
                        <label class="mt-radio">
                            {{Form::radio('status','2', 1)}} Xóa<span></span> </label>
                        @else
                        <label class="mt-radio">
                            {{Form::radio('status','2', 0 )}}  Xóa <span></span></label>
                        @endif
                    
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#add_user').submit()" target="_blank" class="active">
                <span>Cập nhật</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection