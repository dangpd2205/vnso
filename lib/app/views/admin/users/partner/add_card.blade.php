@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.alert_ajax')
@include('admin.users.users.file')
@include('admin.users.customer.modal')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Tài khoản <i class="fa fa-circle"></i></li>
<li>Khách hàng thân thiết <i class="fa fa-circle"></i></li>
<li>
	Thẻ
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")

<div class="row">
    {{Form::open(array('action' => '\ADMIN\UserController@postAddCard','id'=>'add_user','class'=>'form-horizontal'))}}
    <div class="col-md-12">  
        @include('admin.template.error')
		<div class="form-group">
			<label class="col-lg-3 control-label">Khách hàng</label>
			<div class="col-lg-4">
				<select id="select2-option" name="user_id" style="width:260px" class="form-control select2-multiple input-sm">
					@if(isset($customer) && count($customer)>0)
					@foreach($customer as $item)
					<option value="{{$item->id}}">{{$item->full_name}}</option>
					@endforeach
					@endif					
				</select>                    
			</div>
			<div class="col-lg-2">
				<a href="javascript:void(0)" onclick="open_modal('add_customer')" class="btn btn-sm btn-info"><i class="fa fa-plus"></i></a>
          
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-3 control-label" for="shift_name">Mã thẻ</label>
			<div class="col-lg-4">
				{{Form::text('card_number', null, array('class'=>'bg-focus form-control input-sm', 'id'=>'card_number'))}}
			</div> 
		</div>
		<div class="form-group">
			<label class="col-lg-3 control-label" for="shift_branch">Hạng thẻ</label>
			<div class="col-lg-4">
				{{Form::select('partner_id', $partnercarray, null, [ 'id'=>'partner_id','class' => 'bg-focus form-control input-sm'])}}
			</div> 
			<div class="col-lg-2">
				<a href="javascript:void(0)" onclick="open_modal('add_partner_card1')" class="btn btn-sm btn-info"><i class="fa fa-plus"></i></a>
          
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-3 control-label" for="card_date">Hạn sử dụng</label>
			<div class="col-lg-8">
				<div class="input-group input-medium date date-picker" data-date="{{date('d-m-Y',time())}}" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
					<input type="text" class="form-control" name="card_date" id="card_date"/>
					<span class="input-group-btn">
						<button class="btn default" type="button">
							<i class="fa fa-calendar"></i>
						</button>
					</span>
				</div>
			</div> 
		</div>
        
            

       
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a onclick="$('#add_user').submit()" target="_blank" class="active">
                <span>Thêm mới</span>
                <i class="icon-plus"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection