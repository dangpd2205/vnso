<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                 <th>Mã thẻ</th>
                    <th>Người dùng</th>
                    <th>Hạng thẻ</th>
                    <th>Điểm tích lũy</th>
                    <th>Hạn sử dụng</th>
                    <th>Ngày cấp</th>
                    <th>Trạng thái</th>
                <th style="width: 10%;">Edit</th>
            </tr>
        </thead>
        <tbody id="tbl_content_ajax">
            @if(count($data)==0)
            <tr>
                <td colspan="8">NO DATA</td>
            </tr>
            @endif
            <?php foreach ($data as $item): ?>
                <tr>
                    <td>
                        <a href="javascript:void(0)" data-url-post="{{action('\ADMIN\QuickController@postQuickOrderCard')}}" data-current-id="{{$item->card_number}}" data-togglediv="tog-{{$item->id}}" class="clicktoggle_normal">  {{$item->card_number}} </a> 
                    </td>
                    <td>
                        {{$item->full_name}}
                    </td>
                    <td>
                        {{$item->name}}
                    </td>
                    <td>
                        {{number_format($item->use_point)}}
                    </td>
                    <td>
                        {{date('d/m/Y',$item->exp)}}
                    </td>

                    <td>
                        {{$item->created_at}}
                    </td>
                    <td>
                        {{Lang::get('admin/status.partner_status')[$item->status]}}
                    </td>
                    <td>
                        <a href="{{action('\ADMIN\UserController@getEditCustomer')}}/{{$item->user_id}}"> <i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                        <a href="javascript:void(0);" onclick="delete_one({{$item->id}},'{{action('\ADMIN\UserController@postLockCard')}}', 'dataTables_content')"><i class="fa fa-lock"></i></a>
                    </td>
                </tr>
                <tr class="togglediv non_hover" id="tog-{{$item->id}}" style="display: none">

                </tr>
            <?php endforeach; ?>

            </tbody>
    </table>
</div>
@if($data->links()!='')
<div class="row">
    <div class="col-md-5 col-sm-12">
        <div class="dataTables_info" id="sample_2_info" role="status" aria-live="polite">Đang hiển thị trang {{$data->getCurrentPage()}} đến {{$data->getLastPage()}} của {{$data->getTotal()}} dữ liệu</div>
    </div>
    <div class="col-md-7 col-sm-12">
        <div class="dataTables_paginate paging_bootstrap_number paginate_normal" >
            {{$data->links('admin.template_admin.pagination')}} 
        </div>
    </div>
</div>
@endif