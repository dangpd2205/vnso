@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@endsection
@extends("admin.template.template")
@section("content")
<section class="panel m-top15"> 
    <header class="panel-heading">
        <div class="pull-right">
            
            <a href="javascript:;" class="btn btn-primary"   onclick="$('#add_group').submit()"><i class="fa fa-plus"></i> Cập nhật</a>
            <a href="#" class="btn btn-primary"><i class="fa fa-trash-o"></i> Hủy</a>
        </div>
    </header> 
</section>
<section class="panel m-top15"> 
    {{Form::open(array('action' => '\ADMIN\UserController@postEditGroup','id'=>'add_group','class'=>'form-horizontal'))}}
    {{Form::hidden('id', $data->id)}}
    <div class="panel-body">
        @include('admin.template.error')
        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-lg-3 control-label" for="news_title">Tên</label>
                <div class="col-lg-6">
                    {{Form::text('name', $data->name, array('class'=>'bg-focus form-control', 'id'=>'name', 'required'=>'required'))}}
                </div> 
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-lg-2 control-label" for="news_excerpt">Quyền</label>
                <div class="col-lg-9">
                    <?php
                    Tree::tree_no_lang($all_role, $selected, 'role_admin');
                    ?>
                </div> 
            </div>
        </div>
    </div>
    {{Form::close()}}
</section>
@endsection