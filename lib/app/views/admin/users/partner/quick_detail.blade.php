<td colspan="7">
    <section class="panel" style="margin-bottom: 0px;">
        <header class="panel-heading text-right">
            <ul class="nav nav-tabs pull-left">
<!--                <li class="active"><a href="#tabs-des-34-1" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> Chi tiết</a></li>-->
<!--                <li class=""><a href="#tabs-des-34-2" data-toggle="tab"><i class="fa fa-user fa-lg text-default"></i> Kho</a></li>-->
                <li class="active"><a href="#tabs-des-34-3" data-toggle="tab"><i class="fa fa-user fa-lg text-default"></i> Lịch sử giao dịch</a></li>
                <li class=""><a href="#tabs-des-34-4" data-toggle="tab"><i class="fa fa-user fa-lg text-default"></i> Lịch sử hoạt động</a></li>
            </ul> 
        </header>
        <div class="panel-body" style="padding: 0px;">
            <div class="tab-content">
                <div class="tab-pane fade  active in" id="tabs-des-34-3">
                    <table class="table table-striped m-b-none text-small" style="table-layout: fixed;">
                        <thead>
                            <tr>
                                <th>Order code</th>
                                <th>Số lượng</th>
                                <th>Trạng thái</th>
                                <th>Ghi chú</th>
                                <th>Thời gian</th>
                            </tr>
                        </thead>
                    </table>
                    <div class="scroll-y" style="max-height: 250px;">
                        <table class="table table-striped m-b-none text-small" style="table-layout: fixed;">
                            <tbody>
                                <tr>   
                                    <td><a href="javascipt:void(0)" onclick="open_modal('pop_up_large1')">DH43288147</a></td>
                                    <td>1</td>
                                    <td>Hoàn tất</td>
                                    <td></td>
                                    <td>2015-06-02 15:49:44</td>
                                </tr>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="tabs-des-34-4">
                    <table class="table table-striped m-b-none text-small" style="table-layout: fixed;">
                        <thead>
                            <tr>
                                <th>Thuộc tính</th>
                                <th>Số lượng</th>
                                <th>Hoạt động</th>
                                <th>Ghi chú</th>
                                <th>Chứng từ</th>
                                <th>Thời gian</th>
                            </tr>
                        </thead>
                    </table>
                    <div class="scroll-y" style="max-height: 250px;">
                        <table class="table table-striped m-b-none text-small" style="table-layout: fixed;">
                            <tbody>
                                <tr>   
                                    <td>
                                        Xanh,Vang                                            </td>
                                    <td>1</td>
                                    <td>Nhập</td>
                                    <td>Nhap Hang</td>
                                    <td>PN3126493</td>
                                    <td>2015-06-04 14:34:17</td>
                                </tr>
                                <tr>   
                                    <td>
                                        Xanh,Vang                                            </td>
                                    <td>1</td>
                                    <td>Nhập</td>
                                    <td>Nhap Hang</td>
                                    <td>PN6748192</td>
                                    <td>2015-06-04 14:31:35</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</td>