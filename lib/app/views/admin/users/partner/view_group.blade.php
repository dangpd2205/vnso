@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@endsection
@extends("admin.template.template")
@section("content")
<section class="panel m-top15"> 
    <header class="panel-heading" style="border-bottom:none;">
        <div class="pull-right">
            
            <a href="{{action('\ADMIN\UserController@getAddGroup')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới</a>
            <a href="javascript:;" class="btn btn-primary" onclick="multi_delete('{{action('\ADMIN\UserController@postDeleteMulti')}}', 'tbl_content_ajax')"><i class="fa fa-trash-o"></i> Xóa</a>
        </div>
    </header> 
</section>
<section class="panel"> 
    <div class="table-responsive"> 
        <table id="" class="table table-striped m-b-small"> 
            <thead> 
                <tr>
                    <th class="text-center" style="width: 1%;">
                        <input type="checkbox">
                    </th>
                    <th>Tên nhóm</th>
                    <th>Quyền</th>
                    <th style="width: 10%;">Edit</th>
                </tr>
            </thead>
            <tbody id="tbl_content_ajax">
                @include('admin.users.users.ajax_group')
            </tbody>
        </table>
    </div> 
</section>
@endsection
