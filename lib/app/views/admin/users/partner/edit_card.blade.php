@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")

@endsection
@extends("admin.template_admin.index")
@section("content")
@include('admin.template.alert_ajax')
@include('admin.users.users.file')
@include('admin.users.customer.modal')

<div class="row">
    {{Form::open(array('action' => '\ADMIN\UserController@postEditCustomer','id'=>'add_user','class'=>'form-horizontal'))}}
    {{Form::hidden('id', $data->id)}}
    <div class="col-md-12">  
        @include('admin.template.error')
        <div class="col-lg-8">
            <div class="col-lg-7">
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Email</label>
                    <div class="col-lg-6">
                        {{Form::email('email', $data->email, array('class'=>'bg-focus form-control input-sm', 'id'=>'email', 'required'=>'required'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Mã tài khoản</label>
                    <div class="col-lg-6">
                        {{Form::text('code',  $data->code, array('class'=>'bg-focus form-control input-sm', 'id'=>'code'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Mật khẩu</label>
                    <div class="col-lg-6">
                        {{Form::password('password', array('class'=>'bg-focus form-control input-sm', 'id'=>'password','placeholder'=>'Để trống nếu không thay đổi'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Họ tên</label>
                    <div class="col-lg-6">
                        {{Form::text('full_name',  $data->full_name, array('class'=>'bg-focus form-control input-sm', 'id'=>'full_name'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Số điện thoại</label>
                    <div class="col-lg-6">
                        {{Form::text('phone',  $data->phone, array('class'=>'bg-focus form-control input-sm', 'id'=>'phone'))}}
                    </div> 
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        <a href="javascript:;;" title="Thêm ảnh đại diện" data-img-div="div_image_selected" data-img-id="image_hidden" data-img-check="1" data-modal-id="Selectfile" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=imageselect" class="btn btn-white select_image"><i class="fa fa-plus text"></i></a>
                    </label>
                    <div class="col-lg-9">
                        <div id="div_image_selected">
                        </div>
                        {{Form::hidden('image_hidden', '', array('id'=>'image_hidden'))}}
                        {{Form::hidden('imageselect', $data->avatar, array('id'=>'imageselect'))}}
                    </div> 
                </div>
            </div>
            <div class="form-group col-lg-12">
                <label class="col-lg-3 control-label" for="news_excerpt">Địa chỉ</label>
                <div class="col-lg-8">
                    {{Form::textarea('address',   $data->address, ['class'=>'bg-focus form-control','rows'=>'4', 'id'=>'address'])}}
                </div> 
            </div>
            <div class="form-group col-lg-12">
                <label class="col-lg-3 control-label" for="partner_id">Khách hàng thân thiết</label>
                <div class="col-lg-3">
                
                    {{Form::select('partner_id', $partnercarray,$data->partner_id, array('class'=>'form-control input-sm', 'id'=>'partner_id'))}}  
                </div> 
            </div>
            <div class="form-group  col-lg-12"> 
                <label class="col-lg-3 control-label no-padding-top" for="branches_id">Giới tính</label>
              
                <div class="col-lg-2">
                    
                        @if($data->gender=='1')
                        <label class="mt-radio">  
                            {{Form::radio('gender','1', 1)}}  Nam <span></span></label>
                        @else
                        <label class="mt-radio">      
                            {{Form::radio('gender','1', 0)}} Nam <span></span></label>
                        @endif
             
                </div>
                <div class="col-lg-2">
                        @if($data->gender=='0')
                        <label class="mt-radio">              
                            {{Form::radio('gender','0', 1)}}  Nữ <span></span></label>
                        @else
                        <label class="mt-radio">              
                            {{Form::radio('gender','0', 0)}}  Nữ <span></span></label>
                        @endif
                  
                </div>
            </div>
            <div class="form-group col-lg-12"> 
                <label class="col-lg-3 control-label no-padding-top" for="branches_id">Trạng thái</label> 

                <div class="col-lg-2">
                    
                        @if($data->status=='0')
                        <label class="mt-radio">             
                            {{Form::radio('status','0', 1)}}  Khóa<span></span></label>
                        @else
                        <label class="mt-radio">             
                            {{Form::radio('status','0', 0)}}  Khóa <span></span></label>
                        @endif
              
                </div>
                <div class="col-lg-2">
                    
                        @if($data->status=='1')
                        <label class="mt-radio">
                            {{Form::radio('status','1', 1)}} Hoạt động <span></span></label>
                        @else
                        <label class="mt-radio">
                            {{Form::radio('status','1', 0 )}}  Hoạt động <span></span></label>
                        @endif
           
                </div>
                <div class="col-lg-2">
                   
                        @if($data->status=='2')
                        <label class="mt-radio">
                            {{Form::radio('status','2', 1)}} Xóa <span></span></label>
                        @else
                        <label class="mt-radio">
                            {{Form::radio('status','2', 0 )}} Xóa <span></span></label>
                        @endif
             
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label class="col-lg-2 control-label">
                    <a href="javascript:void(0)" class="btn btn-sm btn-info" onclick="open_modal('add_partner_card');document.getElementById('form_add_shift').reset();"><i class="fa fa-plus"></i> Thêm thẻ khách hàng</a>
                </label>
            </div>
            <div class="table-responsive clear"> 
                <table id="" class="table m-b-small"> 
                    <thead> 
                        <tr>
                            <th style="width: 1%;">STT</th>
                            <th>Mã thẻ</th>
                            <th>Hạng thẻ</th>
                            <th>Hạn sử dụng</th>
                            <th style="width: 10%;">Xóa</th>
                        </tr>
                    </thead>
                    <tbody id="tbl_user_partner_content">
                        <?php
                        if ($tbl_user_card_curent) {
                            ?>
                            <tr>
                                <td>#1 
                                    <input type="hidden" name="user_card" value="{{$tbl_user_card_curent->card_number}}">
                                    <input type="hidden" name="user_partner_id" value="{{$tbl_user_card_curent->partner_id}}">
                                    <input type="hidden" name="user_partner_exp" value="{{date('d/m/Y',$tbl_user_card_curent->exp)}}">
                                </td>
                                <td>{{$tbl_user_card_curent->card_number}}</td>
                                <td><?php
                                    foreach ($partnercarray as $key_item => $par_item) {
                                        if ($key_item == $tbl_user_card_curent->partner_id)
                                            echo $par_item;
                                    }
                                    ?></td>
                                <td>{{date('d/m/Y',$tbl_user_card_curent->exp)}}</td>
                                <td><a href="javascript:void(0);" class="edit_table_row"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0);" class="delete_table_row"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a onclick="$('#add_user').submit()" target="_blank" class="active">
                <span>Cập nhật</span>
                <i class="icon-basket"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection