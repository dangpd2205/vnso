<td colspan="9">
    
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tabs-des-34-{{$tabid}}" data-toggle="tab"><i class="fa fa-user fa-lg text-default"></i> Lịch sử giao dịch</a></li>
         
            </ul> 
     
        <div class="panel-body" style="padding: 0px;">
            <div class="tab-content">
                <div class="tab-pane fade  active in" id="tabs-des-34-{{$tabid}}">
                    <table class="table table-striped m-b-none text-small" style="table-layout: fixed;">
                        <thead>
                            <tr>
                                <th>Order code</th>
                                <th>Tổng tiền</th>
                                <th>Trạng thái</th>
                                <th>Ghi chú</th>
                                <th>Thời gian</th>
                            </tr>
                        </thead>
                    </table>
                    <div class="scroll-y" style="max-height: 250px;">
                        <table class="table table-striped m-b-none text-small" style="table-layout: fixed;">
                            <tbody>
                            <?php
                            $data_status = Lang::get('admin/status.product_order');
                            unset($data_status['']);
                            ?>
                                @if(isset($data) && count($data)>0)
                                @foreach($data as $item)
                                <tr>   
                                    <td><a class="clickorder_detail" data-togglediv="tog-{{$item->id}}" href="javascipt:void(0)" data-url-post="{{action('\ADMIN\UserController@postQuickOrderDetail')}}" data-current-id="{{$item->id}}">{{$item->code}}</a></td>
                                    <td>{{number_format($item->total_amount,0,',',',')}}</td>
                                    <td>
                                        <?php
                                        if (array_key_exists($item->status, $data_status)) {
                                            echo $data_status[$item->status];
                                        }
                                        ?>
                                    </td>
                                    <td>{{$item->note}}</td>
                                    <td>{{$item->created_at}}</td>
                                </tr>
                                @endforeach
                               @else
                               <tr>
                                   <td colspan="5">Không có dữ liệu</td>
                               </tr>
                               @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
       
</td>