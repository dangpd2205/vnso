<div class="modal fade"  id="add_partner_card" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Thêm thẻ khách hàng</h4>
            </div>
            <div class="modal-body">
                {{Form::open(array('url'=>'#', 'class'=>'form-horizontal', 'id'=>'form_add_shift'))}}
                <div class="alert alert-warning alert-block user_card_code_noti" style="display: none;"><h4><i class="fa fa-bell-o"></i>Cảnh báo!</h4> <p>Thay đổi hạng thẻ sẽ bị reset điểm tích lũy nâng hạng.</p> </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="shift_name">Mã thẻ</label>
                    <div class="col-lg-8">
                        {{Form::text('modal_card', null, array('class'=>'bg-focus form-control ', 'id'=>'modal_card'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="shift_branch">Hạng thẻ</label>
                    <div class="col-lg-8">
                        {{Form::select('modal_partner', $partnercarray, null, [ 'id'=>'modal_partner','class' => 'bg-focus form-control input-sm'])}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="shift_start_date">Hạn sử dụng</label>
                    <div class="col-lg-8">
                        <div class="input-group input-medium date date-picker" data-date="11-09-2016" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                            <input type="text" class="form-control" name="shift_start_date" id="shift_start_date"/>
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div> 
                </div>
                {{Form::close()}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary btl_add_partner_card">Thêm mới</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade"  id="add_partner_card1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Thêm hạng thẻ</h4>
            </div>
            <div class="modal-body">
                {{Form::open(array('action'=>'\ADMIN\UserController@postQuickPartner', 'class'=>'form-horizontal', 'id'=>'form_add_partner_card1'))}}
                
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Tên</label>
                    <div class="col-lg-6">
                        {{Form::text('name',  '', array('class'=>'bg-focus form-control', 'id'=>'name'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Chiết khấu</label>
                    <div class="col-lg-6">
                        {{Form::text('discount', null, array('class'=>'bg-focus form-control auto_number', 'id'=>'discount'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Điểm áp dụng</label>
                    <div class="col-lg-6">
                        {{Form::text('point', null, array('class'=>'bg-focus form-control auto_number', 'id'=>'point'))}}
                    </div> 
                </div>
                {{Form::close()}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" onclick="quick_card_form('form_add_partner_card1','add_partner_card1','partner_id')">Thêm mới</button> 
            </div>
        </div>
    </div>
</div>
<div class="modal fade"  id="add_customer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Thêm khách hàng</h4>
            </div>
            <div class="modal-body">
                {{Form::open(array('action'=>'\ADMIN\UserController@postQuickCustomer', 'class'=>'form-horizontal', 'id'=>'form_add_customer'))}}
                
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="news_title">Email</label>
                    <div class="col-lg-6">
                        {{Form::email('email',  '', array('class'=>'bg-focus form-control', 'id'=>'email'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="news_title">Mã tài khoản</label>
                    <div class="col-lg-6">
                        {{Form::text('code',  '', array('class'=>'bg-focus form-control', 'id'=>'code'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="news_title">Mật khẩu</label>
                    <div class="col-lg-6">
                        {{Form::password('password', array('class'=>'bg-focus form-control', 'id'=>'password'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="news_title">Họ tên</label>
                    <div class="col-lg-6">
                        {{Form::text('full_name',  '', array('class'=>'bg-focus form-control', 'id'=>'full_name'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="news_title">Số điện thoại</label>
                    <div class="col-lg-6">
                        {{Form::text('phone',  '', array('class'=>'bg-focus form-control', 'id'=>'phone','onkeypress'=>'return event.charCode > 47 && event.charCode < 58;'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="news_title">Mã số thuế</label>
                    <div class="col-lg-6">
                        {{Form::text('tax_number',  '', array('class'=>'bg-focus form-control', 'id'=>'tax_number'))}}
                    </div> 
                </div>
				<div class="form-group">
					<label class="col-lg-3 control-label" for="news_excerpt">Địa chỉ</label>
					<div class="col-lg-8">
						{{Form::textarea('address', '', ['class'=>'bg-focus form-control','rows'=>'4', 'id'=>'address'])}}
					</div> 
				</div>
				<div class="form-group"> 
					<label class="col-lg-3 control-label" for="branches_id">Giới tính</label> 
					<div class="col-lg-2">
						<label class="mt-radio">             
								{{Form::radio('gender','1', 1)}} Nam <span></span></label>
						
					</div>
					<div class="col-lg-2">
						<label class="mt-radio">
								{{Form::radio('gender','0', 0)}} Nữ <span></span></label>
						
					</div>
				</div>
                {{Form::close()}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" onclick="quick_card_form('form_add_customer','add_customer','select2-option')">Thêm mới</button>
            </div>
        </div>
    </div>
</div>