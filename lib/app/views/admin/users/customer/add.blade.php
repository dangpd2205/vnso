@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.alert_ajax')
@include('admin.users.users.file')

@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Tài khoản <i class="fa fa-circle"></i></li>
<li>
	Khách hàng
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")

<div class="row">
    {{Form::open(array('action' => '\ADMIN\UserController@postAddCustomer','id'=>'add_user','class'=>'form-horizontal'))}}
    <div class="col-md-12">  
        @include('admin.template.error')
        <div class="col-lg-8">
            <div class="col-lg-7">
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Email</label>
                    <div class="col-lg-6">
                        {{Form::email('email',  '', array('class'=>'bg-focus form-control input-sm', 'id'=>'email', 'required'=>'required'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Mã tài khoản</label>
                    <div class="col-lg-6">
                        {{Form::text('code',  '', array('class'=>'bg-focus form-control input-sm', 'id'=>'code'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Mật khẩu</label>
                    <div class="col-lg-6">
                        {{Form::password('password', array('class'=>'bg-focus form-control input-sm', 'id'=>'password'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Họ tên</label>
                    <div class="col-lg-6">
                        {{Form::text('full_name',  '', array('class'=>'bg-focus form-control input-sm', 'id'=>'full_name'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Số điện thoại</label>
                    <div class="col-lg-6">
                        {{Form::text('phone',  '', array('class'=>'bg-focus form-control input-sm', 'id'=>'phone'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Mã số thuế</label>
                    <div class="col-lg-6">
                        {{Form::text('tax_number',  '', array('class'=>'bg-focus form-control input-sm', 'id'=>'tax_number'))}}
                    </div> 
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        <a href="javascript:;;" title="Thêm ảnh đại diện" data-img-div="div_image_selected" data-img-id="image_hidden" data-img-check="1" data-modal-id="Selectfile" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=imageselect" class="btn btn-white select_image"><i class="fa fa-plus text"></i></a>
                    </label>
                    <div class="col-lg-9">
                        <div id="div_image_selected">
                        </div>
                        {{Form::hidden('image_hidden', '', array('id'=>'image_hidden'))}}
                        {{Form::hidden('imageselect', null, array('id'=>'imageselect'))}}
                    </div> 
                </div>
            </div>
            <div class="form-group col-lg-12">
                <label class="col-lg-3 control-label" for="news_excerpt">Địa chỉ</label>
                <div class="col-lg-8">
                    {{Form::textarea('address', '', ['class'=>'bg-focus form-control','rows'=>'4', 'id'=>'address'])}}
                </div> 
            </div>
            <div class="form-group  col-lg-12"> 
                <label class="col-lg-3 control-label no-padding-top" for="branches_id">Giới tính</label> 
                <div class="col-lg-2">
                    <label class="mt-radio">             
                            {{Form::radio('gender','1', 1)}} Nam <span></span></label>
                    
                </div>
                <div class="col-lg-2">
                    <label class="mt-radio">
                            {{Form::radio('gender','0', 0)}} Nữ <span></span></label>
                    
                </div>
            </div>
            
        </div>
        
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a onclick="$('#add_user').submit()" target="_blank" class="active">
                <span>Thêm mới</span>
                <i class="icon-plus"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection