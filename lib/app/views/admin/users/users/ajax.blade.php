<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>E-mail</th>
                <th>Tên</th>
                <th>Địa chỉ</th>
                <th>Số điện thoại</th>
                <th>Thời gian</th>
                <th style="width: 10%;">Edit</th>
            </tr>
        </thead>
        <tbody id="tbl_content_ajax">
            @if(count($data)==0)
            <tr>
                <td colspan="9">NO DATA</td>
            </tr>
            @endif
            <?php foreach ($data as $item): ?>
                <tr>
                    <td>
                        {{$item->email}}
                    </td>
                    <td>
                        {{$item->full_name}}
                    </td>
                    <td>
                        {{$item->address}}
                    </td>
                    <td>
                        {{$item->phone}}
                    </td>
                    <td>
                        {{$item->created_at}}
                    </td>
                    <td>
                        <a href="{{action('\ADMIN\UserController@getEdit')}}/{{$item->id}}"> <i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                        <a href="javascript:void(0)" onclick="delete_one({{$item->id}},'{{action('\ADMIN\UserController@postDelete')}}', 'dataTables_content')"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
                <tr class="togglediv non_hover" id="tog-{{$item->id}}" style="display: none">

                </tr>
            <?php endforeach; ?>

            </tbody>
    </table>
</div>
@if($data->links()!='')
<div class="row">
    <div class="col-md-5 col-sm-12">
        <div class="dataTables_info" id="sample_2_info" role="status" aria-live="polite">Đang hiển thị trang {{$data->getCurrentPage()}} đến {{$data->getLastPage()}} của {{$data->getTotal()}} dữ liệu</div>
    </div>
    <div class="col-md-7 col-sm-12">
        <div class="dataTables_paginate paging_bootstrap_number paginate_normal" >
            {{$data->links('admin.template_admin.pagination')}} 
        </div>
    </div>
</div>
@endif