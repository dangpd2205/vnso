@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@endsection
@extends("admin.template_admin.index")
@section("content")

<div class="row">
    {{Form::open(array('action' => '\ADMIN\UserController@postAddGroup','id'=>'add_group','class'=>'form-horizontal'))}}
    <div class="col-md-12">
        @include('admin.template.error')
        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-lg-3 control-label" for="news_title">Tên</label>
                <div class="col-lg-6">
                    {{Form::text('name',  '', array('class'=>'bg-focus form-control input-sm', 'id'=>'name', 'required'=>'required'))}}
                </div> 
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-lg-2 control-label" for="news_excerpt">Quyền</label>
                <div class="col-lg-9" style="height:400px;overflow: auto">
					<ul style="padding-left: 0px;height:400px;overflow: auto;display: inline">
						@foreach($all_role as $item)
						<?php
						$check = false;
						if ($item->roles_parent == 0) {
							?>
							<li class="checkbox" style="float:left;padding:5px;width:200px">                          
								
									<?php echo Form::checkbox('role_admin[]', $item->roles_code); ?>  <?php echo $item->roles_name; ?> 
								<ul>                                       
									<?php
									//Cấp 2
									foreach ($all_role as $item_node_one) {                                        
											$check = false;
											if ($item_node_one->roles_parent == $item->id) {
												?>
												<li class="checkbox">
														<?php echo Form::checkbox('role_admin[]', $item_node_one->roles_code); ?>  <?php echo $item_node_one->roles_name; ?>        
												</li>
												<?php
											}

									}
									?>
								</ul>
							</li>
							<?php
						}      
						?>

						@endforeach
					</ul>
				</div>
            </div>
        </div>
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#add_group').submit()" target="_blank" class="active">
                <span>Thêm mới</span>
                <i class="icon-plus"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection