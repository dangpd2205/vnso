@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.alert_ajax')
@include('admin.users.users.file')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Tài khoản <i class="fa fa-circle"></i></li>
<li>
	Nhân viên
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")


<div class="row">
    {{Form::open(array('action' => '\ADMIN\UserController@postEdit','id'=>'add_user','class'=>'form-horizontal'))}}
    {{Form::hidden('id', $data->id)}}
    <div class="col-md-12">  
        @include('admin.template.error')
        <div class="col-lg-8">
            <div class="col-lg-7">
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Email</label>
                    <div class="col-lg-6">
                        {{Form::text('email', $data->email, array('class'=>'bg-focus form-control input-sm', 'id'=>'email', 'required'=>'required'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Mật khẩu</label>
                    <div class="col-lg-6">
                        {{Form::password('password', array('class'=>'bg-focus form-control input-sm', 'id'=>'password','placeholder'=>'Để trống nếu không thay đổi'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Họ tên</label>
                    <div class="col-lg-6">
                        {{Form::text('full_name',  $data->full_name, array('class'=>'bg-focus form-control input-sm', 'id'=>'full_name'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label" for="news_title">Số điện thoại</label>
                    <div class="col-lg-6">
                        {{Form::text('phone',  $data->phone, array('class'=>'bg-focus form-control input-sm', 'id'=>'phone'))}}
                    </div> 
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        <a href="javascript:;;" title="Thêm ảnh đại diện" data-img-div="div_image_selected" data-img-id="image_hidden" data-img-check="1" data-modal-id="Selectfile" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=imageselect" class="btn btn-white select_image"><i class="fa fa-plus text"></i></a>
                    </label>
                    <div class="col-lg-9">
                        <div id="div_image_selected">
                        </div>
                        {{Form::hidden('image_hidden', $data->avatar, array('id'=>'image_hidden'))}}
                        {{Form::hidden('imageselect', $data->avatar, array('id'=>'imageselect'))}}
                    </div> 
                </div>
            </div>
            <div class="form-group col-lg-12">
                <label class="col-lg-3 control-label" for="news_excerpt">Địa chỉ</label>
                <div class="col-lg-8">
                    {{Form::textarea('address',   $data->address, ['class'=>'bg-focus form-control input-sm','rows'=>'4', 'id'=>'address'])}}
                </div> 
            </div>
            <div class="form-group  col-lg-12"> 
                <label class="col-lg-3 control-label no-padding-top" for="branches_id">Giới tính</label>
              
                <div class="col-lg-2">
                    
                        @if($data->gender=='1')
                        <label class="mt-radio">  
                            {{Form::radio('gender','1', 1)}}  Nam <span></span></label>
                        @else
                        <label class="mt-radio">      
                            {{Form::radio('gender','1', 0)}} Nam <span></span></label>
                        @endif
             
                </div>
                <div class="col-lg-2">
                        @if($data->gender=='0')
                        <label class="mt-radio">              
                            {{Form::radio('gender','0', 1)}}  Nữ <span></span></label>
                        @else
                        <label class="mt-radio">              
                            {{Form::radio('gender','0', 0)}}  Nữ <span></span></label>
                        @endif
                  
                </div>
            </div>
		</div>
        <div class="col-lg-4">
            <div class="form-group">
                <label class="col-lg-2 control-label" for="news_excerpt">Quyền</label>
                
                <div class="col-lg-9">
                     <ul id="cat_list_add" style="padding-left: 0px;overflow-y: auto;max-height: 700px;">           
                    @if(count($all_group)>0)
                   
                       @foreach($all_group as $group)
                       @if($data->group_admin_id!='')
                       <?php $arr = explode(',',$data->group_admin_id); ?>
                       @else
                       <?php $arr=[]; ?>
                       @endif
                       <li class="">    
                           <label class="mt-checkbox">
                                <input name="role_admin[]" type="checkbox" value="{{$group->id}}" @if(in_array($group->id,$arr)) checked @endif> {{$group->name}} <span></span>
                            </label>
                        </li>
                       @endforeach
                    </ul>
                   @endif
                </div> 
            </div>
        </div>
       
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a onclick="$('#add_user').submit()" target="_blank" class="active">
                <span>Cập nhật</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection