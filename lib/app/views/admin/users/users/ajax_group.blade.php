<?php foreach ($data as $item): ?>
    <tr>
        <td class="text-center">
            <input type="checkbox" name="id[]" value="{{$item->id}}">
        </td>
        <td>
            {{$item->name}}
        </td>
        <td>
            <a href="{{action('\ADMIN\UserController@getEditGroup')}}/{{$item->id}}" title="Chỉnh sửa bài viết"> <i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
            
            <a href="javascript:;;" onclick="delete_reload({{$item->id}},'{{action('\ADMIN\UserController@postDeleteGroup')}}')" title="Xoa"><i class="fa fa-trash-o"></i></a>
        </td>
    </tr>
    <tr class="togglediv" id="tog-{{$item->id}}" style="display: none">

    </tr>
<?php endforeach; ?>
@if(count($data)==0)
<tr>
    <td colspan="3">NO DATA</td>
</tr>
@endif
@if($data->links()!='')
<tr>
    <td colspan="3">
        {{$data->links()}} 
    </td>
</tr>
@endif
