@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@endsection
@extends("admin.template_admin.index")
@section("content")
<div class="row">
    <div class="col-md-12">  
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database"></i>Dữ liệu</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <!--                    <a href="javascript:;" class="reload" data-url="/hahahah" data-error-display="toastr"  data-error-display-text="FUCK YOU" data-original-title="WHAT THE FUCK" title=""> </a>-->
                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <div id="dataTables_content" class="dataTables_wrapper no-footer dataTables_content" data-quick-url="">                 
                        <div class="table-scrollable">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                         <th style="width: 1%;">
                                           
                                        </th>
                                        <th>Tên nhóm</th>
                                        <th style="width: 10%;">Edit</th>
                                    </tr>
                                </thead>
                                <tbody id="tbl_content_ajax">
                                    @include('admin.users.users.ajax_group')
                                </tbody>
                            </table>
                        </div>
                                
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="{{action('\ADMIN\UserController@getAddGroup')}}" target="_blank" class="active">
                <span>Thêm mới</span>
                <i class="icon-plus"></i>
            </a>
        </li>
        <li>
            <a href="javascript:void(0)" onclick="multi_delete('{{action('\ADMIN\UserController@postDeleteMulti')}}', 'tbl_content_ajax')" target="_blank" class="active">
                <span>Xóa</span>
                <i class="icon-trash"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection
