<!DOCTYPE html>
<html lang="vi">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <table id="" class="table table-striped m-b-small"> 
        <thead>           
            <tr>
                <th>E-mail</th>                <th>Thời gian</th>
            </tr>
        </thead>
        <tbody id="tbl_content_ajax">
            @if(isset($data) && count($data)>0)			@foreach($data as $item)
                <tr>   
                    <td>     
                        {{$item->email}}     
                    </td>
                    <td>     
                        {{$item->created_at}}
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>

        </table>
    </body>
    </html>
