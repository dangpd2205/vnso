@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.show.add_modal')
@include('admin.template.alert_ajax')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Quản lý chương trình
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
<script>
	function quickartist(id, id_modal) {
		$('#' + id_modal).modal('hide');
		$('#ajax_alert_loading').modal({
			backdrop: false
		})
		$('#' + id).submit(function (e)
		{
			    var postData = $(this).serializeArray();
			    var formURL = $(this).attr("action");
			    $.ajax(
					    {
						        url: formURL,
						        type: "POST",
						        data: postData,
						        success: function (data, textStatus, jqXHR)
						        {
									$('#ajax_alert_loading').modal('hide');
									msg = jQuery.parseJSON(data);
						
									if (msg.check == 0) {
										$('#ajax_alert_error').modal();
										$('#tbl_content_ajax_alert').html(msg.content);
									} else {
										$('#ajax_alert_success').modal();
										$('#tbl_content_ajax').html(msg.content);
									}
								
							            
						        },
						        error: function (jqXHR, textStatus, errorThrown)
						        {
							            
						        }
					    });
			    e.preventDefault(); 
			e.unbind();
		});
		$('#' + id).submit();

	}
	$('document').ready(function(){
        $('.add_quick').click(function(){
			$('#add_new_category').modal();             
		});
	});
</script>
<link href="{{Asset('asset/admin')}}/map.css" rel="stylesheet" type="text/css" />
{{Form::open(array('action' => '\ADMIN\ShowController@postEditInfo','id'=>'edit_news','class'=>'form-horizontal'))}}
    
    {{Form::hidden('id_show', $data_news[0]->show_id)}}
<div class="row">
    <div class="col-md-12">  
	@include('admin.template.error')
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#content-tab1" data-toggle="tab"><i class="fa fa-globe fa-lg text-default"></i> Nội dung chính</a>
            </li>
             <li >
                <a href="#content-tab2" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> Cấu hình</a>
            </li>
            <li >
                <a href="#content-tab3" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> Nghệ sỹ/ nhạc trưởng</a>
            </li>
             <li style="display:none;">
                <a href="#content-tab4" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> Bản nhạc</a>
            </li>
             <li >
                <a href="#content-tab5" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> Nhà tài trợ</a>
            </li>
             <li >
                <a href="#content-tab6" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> SEO</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade  active in" id="content-tab1"> 	
				<div class="form-group">
                    <label class="col-lg-2 control-label">
                        <a href="javascript:void(0)" data-img-div="div_image_selected" data-img-id="image_hidden" data-img-check="1" data-modal-id="Selectfile" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=imageselect" class="btn btn-white select_image"><i class="fa fa-plus text"></i> Ảnh đại diện (424x594)</a>
                    </label>
                    <div class="col-lg-9">
                        <div id="div_image_selected">
                            @if($data_news[0]->avatar!='')
                            <div class="thumbnail  pull-left m-l-small item">
                                <a href="{{$data_news[0]->avatar}}" target="_blank">
                                    <img src="{{$data_news[0]->avatar}}"></a><span data-img-id="image_hidden"> <i class="fa fa-times"></i> </span>
                            </div>
                            @endif
                        </div>
                        {{Form::hidden('image_hidden', $data_news[0]->avatar, array('id'=>'image_hidden'))}}
                        {{Form::hidden('imageselect', $data_news[0]->avatar, array('id'=>'imageselect'))}}
                    </div> 
                </div>
                
                <div class="form-group">
                    <label class="col-lg-2 control-label">Thời gian</label>
                    <div class="col-lg-9">
                        <div class="input-group input-medium date form_datetime form_datetime bs-datetime" data-date="{{($data_news[0]->time_show!=0)?date('d M Y - H:i',$data_news[0]->time_show):date('d M Y - H:i',time())}}" data-date-format="dd MM yyyy - HH:ii">
                            <input type="text" class="form-control" name="news_time" value="{{($data_news[0]->time_show!=0)?date('d M Y - H:i',$data_news[0]->time_show):date('d M Y - H:i',time())}}"/>
                            <span class="input-group-btn">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-lg-2 control-label">Thời lượng (phút)</label>
                    <div class="col-lg-3">
                        {{Form::text('period', $data_news[0]->period, array('class'=>'bg-focus form-control input-sm', 'id'=>'period'))}}
                    </div> 
                </div>
				<div class="form-group">
                    <label class="col-lg-2 control-label"></label>
                    <div class="col-lg-3">
						<label class="mt-checkbox">
							{{Form::checkbox('is_sell',1, $data_news[0]->is_sell)}} Không bán vé
							<span></span>
						</label>
						
                    </div> 					<div class="col-lg-3">						<label class="mt-checkbox">							{{Form::checkbox('sell_status',1, $data_news[0]->sell_status)}} Chưa bán vé							<span></span>						</label>						                    </div> 
                </div>
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">

                                <?php
                                foreach ($l_lang as $item) {
                                    if ($item->id == $g_config_all->website_lang) {
                                        ?>
                                        <a href="#lang_p_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                        <?php
                                    }
                                }
                                ?>

                            </li>
                            <?php if (count($l_lang) > 1) { ?>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                    <ul class="dropdown-menu text-left">
                                        <?php
                                        foreach ($l_lang as $item_child) {
                                            if ($item_child->id != $g_config_all->website_lang) {
                                                ?>
                                                <li> <a href="#lang_p_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>   
                        <?php } ?>
                        </ul>  
                    
                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                $tran_class='';
                                    foreach ($l_lang as $item_content) {
                                        $data=[];
                                        foreach($data_news as $item_data){
                             
                                            if($item_data->langid==$item_content->id){
                                            
                                                $data=$item_data;
                                            }
                                        }
                                        ?>
                                        <div class="tab-pane fade in <?php
                                        if ($item_content->id == $g_config_all->website_lang) {
                                            echo ' active';
                                        }
                                        ?> " id="lang_p_{{$item_content->id}}">
                                        <h3>{{$item_content->name}}</h3>               
                                            <div class="col-lg-9">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label" for="name_{{$item_content->code}}">Tiêu đề</label>
                                                    <div class="col-lg-6">
                                                        <?php 
                                                        $name='';
                                                        if(count($data)>0 && isset($data->name)){
                                                            $name=$data->name;
                                                        }
                                                        ?>
                                                        {{Form::text('name_'.$item_content->code, $name, array('class'=>'bg-focus form-control input-sm', 'id'=>'name_'.$item_content->code))}}
                                                        <input type="hidden" name="lang_id_{{$item_content->code}}" value="{{$item_content->id}}"/>
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Mô tả</label>
                                                    <div class="col-lg-6">
                                                        <?php 
                                                        $description='';
                                                        if(count($data)>0 && isset($data->description)){
                                                            $description=$data->description;
                                                        }
                                                        ?>
                                                        {{Form::textarea('description_'.$item_content->code, $description, array('class'=>'bg-focus form-control input-sm', 'id'=>'description_'.$item_content->code))}}
                                                     
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label" for="content_{{$item_content->code}}">Nội dung</label>
                                                    <div class="col-lg-6">
                                                        <?php 
                                                        $content='';
                                                        if(count($data)>0 && isset($data->content)){
                                                            $content=$data->content;
                                                        }
                                                        ?>
                                                        {{Form::textarea('content_'.$item_content->code, $content, array('class'=>'bg-focus form-control input-sm tinymce', 'id'=>'content_'.$item_content->code))}}
                                                     
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label" for="tags_{{$item_content->code}}">Tags</label>
                                                    <div class="col-lg-6">
                                                        <?php
                                                        $tag='';
                                                        if(count($data)>0 && isset($data->tags)){
                                                            $tag=$data->tags;
                                                        }
                                                        ?>
                                                        {{Form::text('tags_'.$item_content->code, $tag, array('class'=>'bg-focus tags_input ', 'id'=>'tags_'.$item_content->code))}}
                                                    </div> 
                                                </div>
												<div class="form-group">
                                                    <label class="col-lg-2 control-label">Địa điểm khác</label>
                                                    <div class="col-lg-6">
														<?php
                                                        $place_text='';
                                                        if(count($data)>0 && isset($data->place_text)){
                                                            $place_text=$data->place_text;
                                                        }
                                                        ?>
                                                        {{Form::text('place_text_'.$item_content->code, $place_text, array('class'=>'bg-focus form-control input-sm', 'id'=>'place_text_'.$item_content->code))}}
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label class="col-lg-12 control-label text-left"><strong>Danh mục</strong></label>
                                                    <div class="col-lg-12">
                                                        <ul id="cat_list_add_{{$item_content->code}}" style="padding-left: 0px;overflow-y: auto;max-height: 450px;">
                                                            <?php
                                                            echo Tree::tree_loop($all_category, $list_selected, $item_content->id);
                                                            ?>
                                                        </ul>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="tab-pane fade" id="content-tab2">
                <div class="form-group col-lg-6">
                    <div class="col-lg-12">
                        <label class="col-lg-12 control-label">Địa điểm tổ chức: {{$place[0]->name}}</label>
                        
                    </div>                    
                </div>
                <div class="form-group col-lg-6">                    
                    <img src="{{$place[0]->avatar}}" class="place_img" style="width:100%"/>                   
                </div>
            </div>
            <div class="tab-pane fade" id="content-tab3">
                <div class="form-group">
                    <div class="col-lg-12">
                        <label class="col-lg-2 control-label">Nhạc trưởng</label>
                        <div class="col-lg-4">
                            <input type="text" name="artist" class="bg-focus form-control input-sm" id="autokeyword3" name="autokeyword3" onkeyup="autofill('{{URL::action('\ADMIN\ShowController@postAutocomplete3')}}','autokeyword3','div_autokeyword3','1')" placeholder="Nhập tên nhạc trưởng ..." value="@if(isset($conductor) && count($conductor)>0){{$conductor->a_title}}@endif"/>
                            <div class="div_autocomplete" id="div_autokeyword3">

                            </div>
                            <input type="hidden" name="list_id_autokeyword3" id="list_id_autokeyword3" value="{{$data_news[0]->conductor_id}}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12">
                        <label class="col-lg-2 control-label">Nghệ sỹ</label>
                        <div class="col-lg-4">
                            <input type="text" name="autokeyword" class="bg-focus form-control input-sm" id="autokeyword" onkeyup="autofill('{{URL::action('\ADMIN\ShowController@postAutocomplete')}}','autokeyword','div_autokeyword','1')" placeholder="Nhập tên nghệ sỹ ..."/>
                            <div class="div_autocomplete" id="div_autokeyword">

                            </div>
                            <input type="hidden" name="list_id_autokeyword" id="list_id_autokeyword" value="{{$data_news[0]->artist_id}}"/>
                        </div>
						<div class="col-lg-4">
							<a href="javascript:void(0)" class="add_quick">Thêm mới</a>
						</div>
                    </div>
                    <div class="form-group">
                          <label class="col-lg-2 control-label"></label>
                          <div class="col-lg-6 table-responsive" style="height:150px;overflow-y:auto">
                             <table class="table col-lg-12 table-striped">
                                <thead>
                                   <tr>
                                      <td style="width: 80%;">Tên nghệ sỹ</td>
                                      <td style="width: 20%;">Xóa</td>
                                  </tr>
                              </thead>
                              <tbody id="body_list_autokeyword">
                              @if(isset($list_artist) && count($list_artist)>0)
                              @foreach($list_artist as $i_list_artist)
                              <tr id="{{$i_list_artist->artist_id}}"><td>{{$i_list_artist->a_title}} ({{$i_list_artist->a_instrument}})</td><td><a href="javascript:void(0);" onclick="removeThis(this,'list_id_autokeyword','{{$i_list_artist->artist_id}}');">x</a></td></tr>
                              @endforeach
                              @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
            <div class="tab-pane fade" id="content-tab4" style="display:none;">
                <div class="col-lg-12">
                    <label class="col-lg-2 control-label">Bản nhạc</label>
                    <div class="col-lg-4">
                        <input type="text" name="autokeyword1" class="bg-focus form-control input-sm" id="autokeyword1" onkeyup="autofill('{{URL::action('\ADMIN\ShowController@postAutocomplete1')}}','autokeyword1','div_autokeyword1','1')" placeholder="Nhập tên bản nhạc ..."/>
                        <div class="div_autocomplete" id="div_autokeyword1">

                        </div>
                        <input type="hidden" name="list_id_autokeyword1" id="list_id_autokeyword1" value="{{$data_news[0]->sheet_id}}"/>
                    </div>
                </div>
                <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                      <div class="col-lg-6 table-responsive" style="height:150px;overflow-y:auto">
                         <table class="table col-lg-12 table-striped">
                            <thead>
                               <tr>
                                  <td style="width: 80%;">Tên bản nhạc</td>
                                  <td style="width: 20%;">Xóa</td>
                              </tr>
                          </thead>
                          <tbody id="body_list_autokeyword1">
                            @if(isset($list_sheet) && count($list_sheet)>0)
                              @foreach($list_sheet as $i_list_sheet)
                              <tr id="{{$i_list_sheet->sheet_id}}"><td>{{$i_list_sheet->title}}</td><td><a href="javascript:void(0);" onclick="removeThis(this,'list_id_autokeyword1','{{$i_list_sheet->sheet_id}}');">x</a></td></tr>
                              @endforeach
                              @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="content-tab5">
                <div class="col-lg-12">
                    <label class="col-lg-2 control-label">Nhà tài trợ</label>
                    <div class="col-lg-4">
                        <input type="text" class="bg-focus form-control input-sm" id="autokeyword2" name="autokeyword2" onkeyup="autofill('{{URL::action('\ADMIN\ShowController@postAutocomplete2')}}','autokeyword2','div_autokeyword2','1')" placeholder="Nhập tên nhà tài trợ ..."/>
                        <div class="div_autocomplete" id="div_autokeyword2">

                        </div>
                        <input type="hidden" name="list_id_autokeyword2" id="list_id_autokeyword2" value="{{$data_news[0]->sponsor_id}}"/>
                    </div>
                </div>
                <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                      <div class="col-lg-6 table-responsive" style="height:150px;overflow-y:auto">
                         <table class="table col-lg-12 table-striped">
                            <thead>
                               <tr>
                                  <td style="width: 80%;">Nhà tài trợ</td>
                                  <td style="width: 20%;">Xóa</td>
                              </tr>
                          </thead>
                          <tbody id="body_list_autokeyword2">
                            @if(isset($list_sponsor) && count($list_sponsor)>0)
                              @foreach($list_sponsor as $i_list_sponsor)
                              <tr id="{{$i_list_sponsor->id}}"><td>{{$i_list_sponsor->name}}</td><td><a href="javascript:void(0);" onclick="removeThis(this,'list_id_autokeyword2','{{$i_list_sponsor->id}}');">x</a></td></tr>
                              @endforeach
                              @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="content-tab6">
                <div class="form-group"> 
                    <label class="col-lg-2 control-label no-padding-top">Meta Robots Index</label> 
                    <div class="col-lg-7">
                        <label class="mt-radio">
                            {{Form::radio('seo_index','index', ($data_news[0]->robots_index=='index')?1:0)}}
                            Index
                            <span></span>
                        </label>
                        <label class="mt-radio">
                            {{Form::radio('seo_index','noindex', ($data_news[0]->robots_index=='noindex')?1:0)}}
                            Noindex
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group"> 
                    <label class="col-lg-2 control-label no-padding-top" >Meta Robots Follow</label> 
                    <div class="col-lg-7">
                        <label class="mt-radio">
                            {{Form::radio('seo_follow','Follow', ($data_news[0]->robots_follow=='Follow')?1:0)}}
                            Follow
                            <span></span>
                        </label>
                        <label class="mt-radio">
                            {{Form::radio('seo_follow','Nofollow', ($data_news[0]->robots_follow=='Nofollow')?1:0)}}
                            Nofollow
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group"> 
                    <label class="col-lg-2 control-label no-padding-top">Meta Robots Follow</label> 
                    <div class="col-lg-9">
                        <label class="mt-checkbox">
                            <?php echo Form::checkbox('seo_advanced[]', 'NO ODP', in_array('NO ODP', explode(',', $data_news[0]->robots_advanced)) ? 1 : 0); ?> NO ODP
                            <span></span>
                        </label>
                        <label class="mt-checkbox">
                            <?php echo Form::checkbox('seo_advanced[]', 'NO YDIR', in_array('NO YDIR', explode(',', $data_news[0]->robots_advanced)) ? 1 : 0); ?> NO YDIR
                            <span></span>
                        </label>
                        <label class="mt-checkbox">
                            <?php echo Form::checkbox('seo_advanced[]', 'NO Image Index', in_array('NO Image Index', explode(',', $data_news[0]->robots_advanced)) ? 1 : 0); ?> NO Image Index
                            <span></span>
                        </label>
                        <label class="mt-checkbox">
                            <?php echo Form::checkbox('seo_advanced[]', 'NO Archive', in_array('NO Archive', explode(',', $data_news[0]->robots_advanced)) ? 1 : 0); ?> NO Archive
                            <span></span>
                        </label>
                        <label class="mt-checkbox">
                            <?php echo Form::checkbox('seo_advanced[]', 'NO Snippet', in_array('NO Snippet', explode(',', $data_news[0]->robots_advanced)) ? 1 : 0); ?> NO Snippet
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">

                                <?php
                                foreach ($l_lang as $item) {
                                    if ($item->id == $g_config_all->website_lang) {
                                        ?>
                                        <a href="#lang_p_seo_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                        <?php
                                    }
                                }
                                ?>

                            </li>
                            <?php if (count($l_lang) > 1) { ?>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                    <ul class="dropdown-menu text-left">
                                        <?php
                                        foreach ($l_lang as $item_child) {
                                            if ($item_child->id != $g_config_all->website_lang) {
                                                ?>
                                                <li> <a href="#lang_p_seo_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                foreach ($l_lang as $item_content) {
                                    $dataseo=[];
                                    foreach($data_news as $item_data){
                         
                                        if($item_data->langid==$item_content->id){
                                        
                                            $dataseo=$item_data;
                                        }
                                    }
                                    ?>
                                    <div class="tab-pane fade in <?php
                                    $tran_class = '';
                                    if ($item_content->id == $g_config_all->website_lang) {
                                        echo ' active';
                                    }
                                    ?> " id="lang_p_seo_{{$item_content->id}}">
                                        <?php
                                        if(count($dataseo)>0 && isset($dataseo->seoid)){
                                        ?>
                                        <input type="hidden" name="seo_id_{{$item_content->code}}" value="{{$dataseo->seoid}}"/>
                                        <?php } ?>
                                        <h3>{{$item_content->name}}</h3>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_keyword_{{$item_content->code}}">SEO Keyword</label>
                                            <div class="col-lg-5">
                                                <?php 
                                                $keyword='';
                                                if(count($dataseo)>0 && isset($dataseo->keyword)){
                                                    $keyword=$dataseo->keyword;
                                                }
                                                ?>
                                                {{Form::text('seo_keyword_'.$item_content->code, $keyword, array('class'=>'bg-focus form-control tags_input ', 'id'=>'seo_keyword_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_title_{{$item_content->code}}">SEO Title</label>
                                            <div class="col-lg-9">
                                                <?php 
                                                $title='';
                                                if(count($dataseo)>0 && isset($dataseo->title)){
                                                    $title=$dataseo->title;
                                                }
                                                ?>
                                                {{Form::text('seo_title_'.$item_content->code, $title, array('class'=>'bg-focus form-control input-sm seo_title_keyup', 'id'=>'seo_title_'.$item_content->code))}}
                                                <small class="notify_char"></small>
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_description_{{$item_content->code}}">Meta Description</label>
                                            <div class="col-lg-9">
                                                <?php 
                                                $description='';
                                                if(count($dataseo)>0 && isset($dataseo->seodesc)){
                                                    $description=$dataseo->seodesc;
                                                }
                                                ?>
                                                {{Form::textarea('seo_description_'.$item_content->code,$description,['id'=>'seo_description_'.$item_content->code,'class'=>'bg-focus form-control seo_desc_keyup','rows'=>"2"])}}
                                                <small class="notify_char"></small>
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_f_title_{{$item_content->code}}">Facebook Title</label>
                                            <div class="col-lg-9">
                                                <?php 
                                                $fb_title='';
                                                if(count($dataseo)>0 && isset($dataseo->fb_title)){
                                                    $fb_title=$dataseo->fb_title;
                                                }
                                                ?>
                                                {{Form::text('seo_f_title_'.$item_content->code, $fb_title, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_f_title_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_f_description_{{$item_content->code}}">Facebook Description</label>
                                            <div class="col-lg-9">
                                                <?php 
                                                $fb_description='';
                                                if(count($dataseo)>0 && isset($dataseo->fb_description)){
                                                    $fb_description=$dataseo->fb_description;
                                                }
                                                ?>
                                                {{Form::textarea('seo_f_description_'.$item_content->code,$fb_description,['id'=>'seo_f_description_'.$item_content->code ,'class'=>'bg-focus form-control ','rows'=>"2"])}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_f_image_{{$item_content->code}}">Facebook Image</label>
                                            <div class="col-lg-9">
                                                <div class="input-group">
                                                    <?php 
                                                    $fb_image='';
                                                    if(count($dataseo)>0 && isset($dataseo->fb_image)){
                                                        $fb_image=$dataseo->fb_image;
                                                    }
                                                    ?>
                                                    {{Form::text('seo_f_image_'.$item_content->code, $fb_image, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_f_image_'.$item_content->code))}}
                                                    <div class="input-group-btn"> <button class="btn btn-white select_image input-sm"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=seo_f_image_{{$item_content->code}}"> Thêm ảnh </button></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_g_title_{{$item_content->code}}">Google+ Title</label>
                                            <div class="col-lg-9">
                                                <?php 
                                                $g_des='';
                                                if(count($dataseo)>0&& isset($dataseo->g_description)){
                                                    $g_des=$dataseo->g_description;
                                                }
                                                ?>
                                                {{Form::text('seo_g_title_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_g_title_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_g_description_{{$item_content->code}}">Google+ Description</label>
                                            <div class="col-lg-9">
                                                <?php 
                                                $g_title_='';
                                                if(count($dataseo)>0 && isset($dataseo->g_title)){
                                                    $g_title_=$dataseo->g_title;
                                                }
                                                ?>
                                                {{Form::textarea('seo_g_description_'.$item_content->code,$g_title_,['id'=>'seo_g_description_'.$item_content->code,'class'=>'bg-focus form-control ','rows'=>"2"])}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_g_image_{{$item_content->code}}">Google+ Image</label>
                                            <div class="col-lg-9">
                                                <div class="input-group">
                                                    <?php 
                                                    $g_image='';
                                                    if(count($dataseo)>0 && isset($dataseo->g_image)){
                                                        $g_image=$dataseo->g_image;
                                                    }
                                                    ?>
                                                    {{Form::text('seo_g_image_'.$item_content->code, $g_image, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_g_image_'.$item_content->code))}}
                                                    <div class="input-group-btn"> <button class="btn btn-white select_image input-sm" data-toggle="dropdown" data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=seo_g_image_{{$item_content->code}}"> Thêm ảnh </button></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
</div>
{{Form::close()}}
<style>
    .popover .arrow{
        display: none;
    }
</style>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#edit_news').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection