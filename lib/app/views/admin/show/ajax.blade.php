<?php
$data_content = $data
?>    
<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th scope="col">Tiêu đề</th>
                <th scope="col">Nghệ sỹ tham gia</th>
                <th scope="col" style="width: 150px;">Ngày diễn ra</th>
                <th scope="col" style="width: 120px;">Trạng thái</th>
                <th style="width: 10%;">Edit</th>
            </tr>
        </thead>
        <tbody id="tbl_content_ajax">
            @if(count($data_content)==0)
            <tr>
                <td colspan="5">NO DATA</td>
            </tr>
            @endif
            <?php
            $data_status = Lang::get('general.news_status');
            unset($data_status['']);
            $i=0;
            foreach ($data_content as $item):
                ?>
            <tr>
                <td>
                    <a href="{{action('\ADMIN\ShowController@getEdit')}}/{{$item->lang_id}}/{{$item->show_id}}"> {{$item->name}}
						@if($item->is_sell==1) (Không bán vé) @endif
					</a>
                </td> 
                <td>
                    <?php if(isset($list_art[$i]) && count($list_art[$i])>0){
                        foreach($list_art[$i] as $art){
                            echo $art->a_title.'&nbsp;';
                        }

                    } ?>
                </td>               
                <td>
                   {{date('d M Y H:i',$item->time_show)}}
                </td>
                <td>
                    
        			<?php
                        if (array_key_exists($item->status, $data_status)) {
                            echo $data_status[$item->status];
                        }
                        ?>
        			
                </td>
                <td>
                    <a href="{{action('\ADMIN\ShowController@getEdit')}}/{{$item->lang_id}}/{{$item->show_id}}" title="Chỉnh sửa "> <i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                    <a href="javascript:;;" title="Xóa " onclick="delete_one({{$item->show_id}},'{{action('\ADMIN\ShowController@postDelete')}}', 'dataTables_content')"><i class="fa fa-trash-o"></i></a>&nbsp;&nbsp;
                    @if($item->status!=1)  <a href="javascript:;;" title="Duyệt " onclick="delete_one({{$item->show_id}},'{{action('\ADMIN\ShowController@postPostConfirm')}}', 'dataTables_content')"><i class="fa fa-gavel"></i></a>&nbsp;&nbsp; @endif
                   
                </td>
            </tr>
        <?php $i++;endforeach; ?>
        </tbody>
    </table>
</div>
@if($data_content->links()!='')
<div class="row">
    <div class="col-md-5 col-sm-12">
        <div class="dataTables_info" id="sample_2_info" role="status" aria-live="polite">Đang hiển thị trang {{$data_content->getCurrentPage()}} đến {{$data_content->getLastPage()}} của {{$data_content->getTotal()}} dữ liệu</div>
    </div>
    <div class="col-md-7 col-sm-12">
        <div class="dataTables_paginate paging_bootstrap_number paginate_normal" >
            {{$data_content->links('admin.template_admin.pagination')}} 
        </div>
    </div>
</div>
@endif

