@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.alert_ajax')
@include('admin.show.add_modal')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Quản lý chương trình
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
<script>
	function quickartist(id, id_modal) {
		$('#' + id_modal).modal('hide');
		$('#ajax_alert_loading').modal({
			backdrop: false
		})
		$('#' + id).submit(function (e)
		{
			    var postData = $(this).serializeArray();
			    var formURL = $(this).attr("action");
			    $.ajax(
					    {
						        url: formURL,
						        type: "POST",
						        data: postData,
						        success: function (data, textStatus, jqXHR)
						        {
									$('#ajax_alert_loading').modal('hide');
									msg = jQuery.parseJSON(data);
						
									if (msg.check == 0) {
										$('#ajax_alert_error').modal();
										$('#tbl_content_ajax_alert').html(msg.content);
									} else {
										$('#ajax_alert_success').modal();
										$('#tbl_content_ajax').html(msg.content);
									}
								
							            
						        },
						        error: function (jqXHR, textStatus, errorThrown)
						        {
							            
						        }
					    });
			    e.preventDefault(); 
			e.unbind();
		});
		$('#' + id).submit();

	}
    $('document').ready(function(){
		$('.add_quick').click(function(){
			$('#add_new_category').modal();             
		});
        $('.lock_seat').click(function(){
            $.ajax({
                url: $(this).data('url'),
                data: {id: $("select[name='place_id']").val()},
                type: 'POST',
                dataType: 'html',   
                success: function (msg) {
                    $('#lock_place_id').val($("select[name='place_id']").val());   
                    $('.content-seat').html(msg);   
                    $("#span_name span").each(function(){
                        var span_name = $(this).text();
                        $(".list-seat div").each(function(){
                            if($(this).find("input").val() == span_name){
                                if(!$(this).find("input").is(':checked')){
                                    $(this).find("input").attr("checked",'checked');
                                }
                            }
                        });
                    });
                    $('#modal_lock_seat').modal();             
                }
            });
           
        });
    });
    
</script>
<link href="{{Asset('asset/admin')}}/map.css" rel="stylesheet" type="text/css" />
{{Form::open(array('action' => '\ADMIN\ShowController@postAdd','id'=>'add_news','class'=>'form-horizontal'))}}
<div class="row">
    <div class="col-md-12">  
		@include('admin.template.error')
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#content-tab1" data-toggle="tab"><i class="fa fa-globe fa-lg text-default"></i> Nội dung chính</a>
            </li>
            <li >
                <a href="#content-tab2" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> Cấu hình</a>
            </li>
            <li >
                <a href="#content-tab3" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> Nghệ sỹ / nhạc trưởng</a>
            </li>
            <li style="display:none;">
                <a href="#content-tab4" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> Bản nhạc</a>
            </li>
            <li >
                <a href="#content-tab5" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> Nhà tài trợ</a>
            </li>
            <li >
                <a href="#content-tab6" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> SEO</a>
            </li>
        </ul>    
        <div class="tab-content">
            <div class="tab-pane fade  active in" id="content-tab1">
				<div class="form-group">
                    <label class="col-lg-2 control-label">
                        <a href="javascript:void(0)" data-img-div="div_image_selected" data-img-id="image_hidden" data-img-check="1" data-modal-id="Selectfile" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=imageselect" class="btn btn-white select_image"><i class="fa fa-plus text"></i> Ảnh đại diện (424x594)</a>
                    </label>
                    <div class="col-lg-9">
                        <div id="div_image_selected">
                        </div>
                        {{Form::hidden('image_hidden', null, array('id'=>'image_hidden'))}}
                        {{Form::hidden('imageselect', null, array('id'=>'imageselect'))}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Thời gian</label>
                    <div class="col-lg-9">
                        <div class="input-group input-medium date form_datetime form_datetime bs-datetime" data-date="{{date('Y-m-d H:i:s',time())}}" data-date-format="dd MM yyyy - HH:ii">
                            <input type="text" class="form-control" name="news_time"/>
                            <span class="input-group-btn">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Thời lượng (phút)</label>
                    <div class="col-lg-3">
                        {{Form::text('period', null, array('class'=>'bg-focus form-control input-sm', 'id'=>'period'))}}
                    </div> 
                </div>
				<div class="form-group">
                    <label class="col-lg-2 control-label"></label>
                    <div class="col-lg-3">
						<label class="mt-checkbox">
							<input type="checkbox" name="is_sell" id="is_sell" value="1"> Không bán vé
							<span></span>
						</label>
						
                    </div> 
					<div class="col-lg-3">
						<label class="mt-checkbox">
							<input type="checkbox" name="sell_status" id="sell_status" value="0"> Chưa bán vé
							<span></span>
						</label>
						
                    </div> 
                </div>
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">

                                <?php
                                foreach ($l_lang as $item) {
                                    if ($item->id == $g_config_all->website_lang) {
                                        ?>
                                        <a href="#lang_p_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                        <?php
                                    }
                                }
                                ?>

                            </li>
                            <?php if (count($l_lang) > 1) { ?>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                    <ul class="dropdown-menu text-left">
                                        <?php
                                        foreach ($l_lang as $item_child) {
                                            if ($item_child->id != $g_config_all->website_lang) {
                                                ?>
                                                <li> <a href="#lang_p_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>   
                        <?php } ?>
                        </ul>  
                    
                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                $tran_class='';
                                    foreach ($l_lang as $item_content) {

                                        ?>
                                        <div class="tab-pane fade in <?php
                                        if ($item_content->id == $g_config_all->website_lang) {
                                            echo ' active';
                                        }
                                        ?> " id="lang_p_{{$item_content->id}}">
                                        <h3>{{$item_content->name}}</h3>
                                            <div class="col-lg-9">
                                                
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Tiêu đề</label>
                                                    <div class="col-lg-6">
                                                        {{Form::text('name_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'name_'.$item_content->code))}}
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Mô tả</label>
                                                    <div class="col-lg-6">
                                                        {{Form::textarea('description_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'description_'.$item_content->code))}}
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Nội dung</label>
                                                    <div class="col-lg-6">
                                                        {{Form::textarea('content_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm tinymce', 'id'=>'content_'.$item_content->code))}}
                                                    </div> 
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label" for="tags_{{$item_content->code}}">Tags</label>
                                                    <div class="col-lg-6">
                                                        {{Form::text('tags_'.$item_content->code, null, array('class'=>'bg-focus tags_input input-sm', 'id'=>'tags_'.$item_content->code))}}
                                                    </div> 
                                                </div>
												<div class="form-group">
                                                    <label class="col-lg-2 control-label">Nghệ sỹ</label>
                                                    <div class="col-lg-6">
                                                        {{Form::textarea('artist_content_'.$item_content->code, null, array('class'=>'tinymce bg-focus form-control input-sm', 'id'=>'artist_content_'.$item_content->code))}}
                                                    </div> 
                                                </div>
												<div class="form-group">
                                                    <label class="col-lg-2 control-label">Địa điểm khác</label>
                                                    <div class="col-lg-6">
                                                        {{Form::text('place_text_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'place_text_'.$item_content->code))}}
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label class="col-lg-12 control-label text-left"><strong>Danh mục</strong></label>
                                                    <div class="col-lg-12">
                                                        <ul id="cat_list_add_{{$item_content->code}}" style="padding-left: 0px;overflow-y: auto;max-height: 700px;">
                                                            <?php
                                                            echo Tree::tree_loop($all_category, [], $item_content->id);
                                                            ?>
                                                        </ul>
                                                    </div> 
                                                    <div class="col-lg-1">
                                                        <a href="javascript:;" class="btn btn-primary" onclick="open_modal('add_new_category')"> <i class="fa fa-plus"> </i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="tab-pane fade" id="content-tab2">
                <div class="form-group col-lg-6">
                    <div class="col-lg-12">
                        <label class="col-lg-4 control-label">Địa điểm tổ chức</label>
                        <div class="col-lg-8">
                            <select name="place_id" class="bg-focus form-control input-sm" data-url="{{action('\ADMIN\ShowController@postPlace')}}">
                                @if(isset($place) && count($place)>0)
                                @foreach($place as $item)
                                <option value="{{$item->place_id}}">{{$item->name}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    @if(isset($ticket) && count($ticket)>0)
                    @foreach($ticket as $i_ticket)
                    <div class="col-lg-12">
                        <label class="col-lg-4 control-label">Giá {{$i_ticket->name}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="bg-focus form-control input-sm auto_number" name="ticket_{{$i_ticket->ticket_id}}"/>
                        </div>
                    </div>
                    @endforeach
                    @endif                    
                    <div class="col-lg-12">
                        <label class="col-lg-4 control-label lock_seat" data-url="{{action('\ADMIN\ShowController@postModal')}}">Khóa ghế</label>   
                        <input type="hidden" name="name_seat_lock" id="name_seat_lock"/>                 
                        <input type="hidden" name="type_seat_lock" id="type_seat_lock"/>
                        <div class="col-lg-8 control-label" id="span_name">
                           
                        </div>                 
                    </div>
                </div>
                <div class="form-group col-lg-6">                    
                    <img src="" class="place_img" style="width:100%"/>                   
                </div>
            </div>
            <div class="tab-pane fade" id="content-tab3">
                <div class="form-group">
                    <div class="col-lg-12">
                        <label class="col-lg-2 control-label">Nhạc trưởng</label>
                        <div class="col-lg-4">
                            <input type="text" name="artist" class="bg-focus form-control input-sm" id="autokeyword3" name="autokeyword3" onkeyup="autofill('{{URL::action('\ADMIN\ShowController@postAutocomplete3')}}','autokeyword3','div_autokeyword3','1')" placeholder="Nhập tên nhạc trưởng ..."/>
                            <div class="div_autocomplete" id="div_autokeyword3">

                            </div>
                            <input type="hidden" name="list_id_autokeyword3" id="list_id_autokeyword3"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12">
                        <label class="col-lg-2 control-label">Nghệ sỹ</label>
                        <div class="col-lg-4">
                            <input type="text" name="artist" class="bg-focus form-control input-sm" id="autokeyword" name="autokeyword" onkeyup="autofill('{{URL::action('\ADMIN\ShowController@postAutocomplete')}}','autokeyword','div_autokeyword','1')" placeholder="Nhập tên nghệ sỹ ..."/>
                            <div class="div_autocomplete" id="div_autokeyword">

                            </div>
                            <input type="hidden" name="list_id_autokeyword" id="list_id_autokeyword"/>
                        </div>
						<div class="col-lg-4">
							<a href="javascript:void(0)" class="add_quick">Thêm mới</a>
						</div>
                    </div>
                    <div class="form-group">
                          <label class="col-lg-2 control-label"></label>
                          <div class="col-lg-6 table-responsive" style="height:150px;overflow-y:auto">
                             <table class="table col-lg-12 table-striped">
                                <thead>
                                   <tr>
                                      <td style="width: 80%;">Tên nghệ sỹ</td>
                                      <td style="width: 20%;">Xóa</td>
                                  </tr>
                              </thead>
                              <tbody id="body_list_autokeyword">
                              
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="content-tab4" style="display:none;">
                <div class="col-lg-12">
                    <label class="col-lg-2 control-label">Bản nhạc</label>
                    <div class="col-lg-4">
                        <input type="text" name="sheet" class="bg-focus form-control input-sm" id="autokeyword1" name="autokeyword1" onkeyup="autofill('{{URL::action('\ADMIN\ShowController@postAutocomplete1')}}','autokeyword1','div_autokeyword1','1')" placeholder="Nhập tên bản nhạc ..."/>
                        <div class="div_autocomplete" id="div_autokeyword1">

                        </div>
                        <input type="hidden" name="list_id_autokeyword1" id="list_id_autokeyword1"/>
                    </div>
                </div>
                <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                      <div class="col-lg-6 table-responsive" style="height:150px;overflow-y:auto">
                         <table class="table col-lg-12 table-striped">
                            <thead>
                               <tr>
                                  <td style="width: 80%;">Tên bản nhạc</td>
                                  <td style="width: 20%;">Xóa</td>
                              </tr>
                          </thead>
                          <tbody id="body_list_autokeyword1">
                          
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="content-tab5">
                <div class="col-lg-12">
                    <label class="col-lg-2 control-label">Nhà tài trợ</label>
                    <div class="col-lg-4">
                        <input type="text" name="sheet" class="bg-focus form-control input-sm" id="autokeyword2" name="autokeyword2" onkeyup="autofill('{{URL::action('\ADMIN\ShowController@postAutocomplete2')}}','autokeyword2','div_autokeyword2','1')" placeholder="Nhập tên nhà tài trợ ..."/>
                        <div class="div_autocomplete" id="div_autokeyword2">

                        </div>
                        <input type="hidden" name="list_id_autokeyword2" id="list_id_autokeyword2"/>
                    </div>
                </div>
                <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                      <div class="col-lg-6 table-responsive" style="height:150px;overflow-y:auto">
                         <table class="table col-lg-12 table-striped">
                            <thead>
                               <tr>
                                  <td style="width: 80%;">Nhà tài trợ</td>
                                  <td style="width: 20%;">Xóa</td>
                              </tr>
                          </thead>
                          <tbody id="body_list_autokeyword2">
                          
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="content-tab6">
                <div class="form-group"> 
                    <label class="col-lg-2 control-label no-padding-top" for="branches_id">Meta Robots Index</label> 
                    <div class="col-lg-7">
                        <label class="mt-radio">
                            {{Form::radio('seo_index','index', 1)}}
                            Index
                            <span></span>
                        </label>
                        <label class="mt-radio">
                            {{Form::radio('seo_index','noindex',0)}} Noindex
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group"> 
                    <label class="col-lg-2 control-label no-padding-top" for="branches_id">Meta Robots Follow</label> 
                    <div class="col-lg-7">
                        <label class="mt-radio">
                            {{Form::radio('seo_follow','Follow', 1)}}
                            Follow
                            <span></span>
                        </label>
                        <label class="mt-radio">
                            {{Form::radio('seo_follow','Nofollow',0)}} Nofollow
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group"> 
                    <label class="col-lg-2 control-label no-padding-top" for="branches_id">Meta Robots Follow</label> 
                    <div class="col-lg-9">
                        <label class="mt-checkbox">
                            <input type="checkbox" name="seo_advanced[]" value="NO ODP"/> NO ODP
                            <span></span>
                        </label>
                        <label class="mt-checkbox">
                            <input type="checkbox" name="seo_advanced[]" value="NO YDIR"/> NO YDIR
                            <span></span>
                        </label>
                        <label class="mt-checkbox">
                            <input type="checkbox" name="seo_advanced[]" value="NO Image Index"/> NO Image Index
                            <span></span>
                        </label>
                        <label class="mt-checkbox">
                            <input type="checkbox" name="seo_advanced[]" value="NO Archive"/> NO Archive
                            <span></span>
                        </label>
                        <label class="mt-checkbox">
                            <input type="checkbox" name="seo_advanced[]" value="NO Snippet"/> NO Snippet
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">

                                <?php
                                foreach ($l_lang as $item) {
                                    if ($item->id == $g_config_all->website_lang) {
                                        ?>
                                        <a href="#lang_p_seo_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                        <?php
                                    }
                                }
                                ?>

                            </li>
                            <?php if (count($l_lang) > 1) { ?>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                    <ul class="dropdown-menu text-left">
                                        <?php
                                        foreach ($l_lang as $item_child) {
                                            if ($item_child->id != $g_config_all->website_lang) {
                                                ?>
                                                <li> <a href="#lang_p_seo_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                foreach ($l_lang as $item_content) {
                                    ?>
                                    <div class="tab-pane fade in <?php
                                    $tran_class = '';
                                    if ($item_content->id == $g_config_all->website_lang) {
                                        echo ' active';
                                        $tran_class = 'translate_google';
                                    }
                                    ?> " id="lang_p_seo_{{$item_content->id}}">
                                        <h3>{{$item_content->name}}</h3>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_keyword_{{$item_content->code}}">SEO Keyword</label>
                                            <div class="col-lg-5">
                                                {{Form::text('seo_keyword_'.$item_content->code, null, array('class'=>'bg-focus form-control tags_input input-sm', 'id'=>'seo_keyword_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_title_{{$item_content->code}}">SEO Title</label>
                                            <div class="col-lg-9">
                                                {{Form::text('seo_title_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm seo_title_keyup', 'id'=>'seo_title_'.$item_content->code))}}
                                                <small class="notify_char"></small>
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_description_{{$item_content->code}}">Meta Description</label>
                                            <div class="col-lg-9">
                                                {{Form::textarea('seo_description_'.$item_content->code,'',['id'=>'seo_description_'.$item_content->code,'class'=>'seo_desc_keyup bg-focus form-control ','rows'=>"2"])}}
                                                <small class="notify_char"></small>
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_f_title_{{$item_content->code}}">Facebook Title</label>
                                            <div class="col-lg-9">
                                                {{Form::text('seo_f_title_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_f_title_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_f_description_{{$item_content->code}}">Facebook Description</label>
                                            <div class="col-lg-9">
                                                {{Form::textarea('seo_f_description_'.$item_content->code,'',['id'=>'seo_f_description_'.$item_content->code ,'class'=>'bg-focus form-control input-sm','rows'=>"2"])}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_f_image_{{$item_content->code}}">Facebook Image</label>
                                            <div class="col-lg-9">
                                                <div class="input-group">
                                                    {{Form::text('seo_f_image_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_f_image_'.$item_content->code))}}
                                                    <div class="input-group-btn"> <button class="btn btn-white select_image input-sm"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=seo_f_image_{{$item_content->code}}"> Thêm ảnh </button></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_g_title_{{$item_content->code}}">Google+ Title</label>
                                            <div class="col-lg-9">
                                                {{Form::text('seo_g_title_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_g_title_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_g_description_{{$item_content->code}}">Google+ Description</label>
                                            <div class="col-lg-9">
                                                {{Form::textarea('seo_g_description_'.$item_content->code,'',['id'=>'seo_g_description_'.$item_content->code,'class'=>'bg-focus form-control ','rows'=>"2"])}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="seo_g_image_{{$item_content->code}}">Google+ Image</label>
                                            <div class="col-lg-9">
                                                <div class="input-group">
                                                    {{Form::text('seo_g_image_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'seo_g_image_'.$item_content->code))}}
                                                    <div class="input-group-btn"> <button class="btn btn-white select_image input-sm" data-toggle="dropdown" data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=seo_g_image_{{$item_content->code}}"> Thêm ảnh </button></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
{{Form::close()}}
<style>
    .popover .arrow{
        display: none;
    }
</style>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#add_news').submit()" class="active">
                <span>Lưu</span>
                <i class="icon-plus"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection