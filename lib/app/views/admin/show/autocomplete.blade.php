@if(isset($data) && count($data)>0)
<table class="table-responsive cus_table">
    <tr>
        <th class="auto_name">Tên nghệ sỹ</th>
    </tr>
    @foreach($data as $item)
    
    <?php $pname = str_replace('"',' ', $item->a_title); ?>
    <tr id="{{$item->artist_id}}" onclick="bindingdata('{{$item->artist_id}}','{{$pname}}','body_list_autokeyword','autokeyword','list_id_autokeyword','div_autokeyword')">       

        <td class="auto_name">{{$item->a_title}} ({{$item->a_instrument}})
        </td>       

    </tr>
    @endforeach
</table>
@else
<table class="table-responsive cus_table">
    <tr>
        <th class="auto_name">Tên nghệ sỹ</th>
    </tr>
    <tr>
        <td>Không có dữ liệu</td>
    </tr>
</table>
@endif



