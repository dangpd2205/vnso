<!-- Thêm nhóm sản phẩm -->
<div class="modal fade"  id="add_new_category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Thêm nghệ sỹ</h4>
            </div>
            <div class="modal-body">
                {{Form::open(array('action'=>'\ADMIN\ShowController@postQuickArtist', 'class'=>'form-horizontal', 'id'=>'form_add_category'))}}
                <div class="row">
                    <div class="col-md-12">
						<div class="form-group">
							<label class="col-lg-2 control-label">Ảnh</label>
							<div class="col-lg-6">
								<div class="input-group">
									{{Form::text('avatar','', array('class'=>'bg-focus form-control', 'id'=>'avatar'))}}
									<div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=avatar"> Thêm ảnh </button></div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label"></label>
							<div class="col-md-3">
								<div class="mt-checkbox-inline">
									<label class="mt-checkbox">
										<input type="checkbox" name="type" id="type" value="1"> Nghệ sỹ khách mời
										<span></span>
									</label>
								</div>
							</div>
						</div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <?php
                                foreach ($l_lang as $item) {
                                    if ($item->id == $g_config_all->website_lang) {
                                        ?>
                                        <a href="#lang_p_add_cat_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                        <?php
                                    }
                                }
                                ?>
                            </li>
                            <?php if (count($l_lang) > 1) { ?>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                    <ul class="dropdown-menu text-left">
                                        <?php
                                        foreach ($l_lang as $item_child) {
                                            if ($item_child->id != $g_config_all->website_lang) {
                                                ?>
                                                <li> <a href="#lang_p_add_cat_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>
                        <?php } ?>
                        </ul>
                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                foreach ($l_lang as $item_content) {
                                    ?>
                                    <div class="tab-pane fade in <?php
                                    if ($item_content->id == $g_config_all->website_lang) {
                                        echo ' active';
                                    }
                                    ?> " id="lang_p_add_cat_{{$item_content->id}}">
                                        <h3>{{$item_content->name}}</h3>
										
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Tiêu đề</label>
                                            <div class="col-lg-9">
                                                {{Form::text('name_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'name_'.$item_content->code))}}
                                            </div> 
                                        </div>                                        
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Mô tả</label>
                                            <div class="col-lg-9">
                                                {{Form::textarea('description_'.$item_content->code, null, array('class'=>'bg-focus form-control', 'id'=>'description_'.$item_content->code,'rows'=>'2'))}}
                                            </div> 
                                        </div>
										<div class="form-group">
                                            <label class="col-lg-2 control-label">Nội dung</label>
                                            <div class="col-lg-9">
                                                {{Form::textarea('content_'.$item_content->code, null, array('class'=>'bg-focus form-control tinymce', 'id'=>'content_'.$item_content->code))}}
                                            </div> 
                                        </div>
										<div class="form-group">
                                            <label class="col-lg-2 control-label">Vị trí</label>
                                            <div class="col-lg-9">
                                                {{Form::text('instrument_'.$item_content->code, null, array('class'=>'bg-focus form-control input-sm', 'id'=>'instrument_'.$item_content->code))}}
                                            </div> 
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>


                            </div>
                        </div>
                    </div>
                </div>
                {{Form::close()}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" onclick="quickartist('form_add_category', 'add_new_category')">Thêm mới</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="Selectfile" tabindex="1" role="dialog" aria-labelledby="SelectfileLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ls">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="SelectfileLabel">Chọn file</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input_hidden_img_id">
                <input type="hidden" id="input_hidden_img_check">
                <input type="hidden" id="input_hidden_img_div">
                <script>
                    function responsive_filemanager_callback(fieldID) {
                        var id_img = $("#input_hidden_img_id").val();
                        var url_img = $("#" + fieldID).val();
                        var url_replace = url_img.replace('http://' + window.location.hostname, '');
                        var div_img = $("#input_hidden_img_div").val();
                        var check_img = $("#input_hidden_img_check").val();
                        var html = '<div class="thumbnail  pull-left m-l-small item"><a href="' + url_img + '" target="_blank"><img src="' + url_img + '"/></a><span data-img-id="' + id_img + '"> <i class="fa fa-times"></i> </span></div>';
                        if (check_img == 0) {
                            $("#" + fieldID).val(url_replace);
                            $('#Selectfile').modal('hide');
                        }
                        if (check_img == 1) {
                            $("#" + div_img).html(html);
                            $("#" + id_img).val(url_img.replace('http://' + window.location.hostname, ''));
                            $('#Selectfile').modal('hide');

                        } else {
                            $("#" + div_img).append(html);
                            var images = $("#" + div_img).find("img").map(function () {
                                return this.getAttribute('src');
                            }).get();
                            for (i = 0; i < images.length; i++) {
                                images[i] = images[i].replace('http://' + window.location.hostname, '');
                            }
                            $("#" + id_img).val(images);
                            $('#Selectfile').modal('hide');
                        }
                    }
                </script>
                <iframe width="100%" height="400" src="" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; " __idm_frm__="29">
                </iframe>
            </div>
        </div>
    </div>
</div>


<div class="modal fade"  id="modal_lock_seat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:100%">
        <div class="modal-content">
            {{Form::open(array('action'=>'\ADMIN\ShowController@postCheckLock', 'class'=>'form-horizontal', 'id'=>'form_lock_seat'))}}
            <input type="hidden" name="lock_place_id" id="lock_place_id"/>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Khóa ghế</h4>
            </div>
            <div class="modal-body">
                <div class="main-wrap">
                    <div class="section section-padding book-seat">
                        <div class="container">                            
                            <div class="row">      
                                <div class="col-md-12">
                                    <div class="info-seat">
                                        <ul>
                                            <li class="list-1"><span></span>Ghế khóa</li>                                            
                                        </ul>
                                    </div>
                                </div>                          
                                <div class="col-md-12 content-seat">
                                   
                                </div>
                            
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" onclick="btnlockseat('modal_lock_seat')">Đồng ý</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>