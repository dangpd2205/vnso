@section("title")
Quản lý comment
@endsection
@section("description")
Quản lý comment
@endsection
@section("modal_option")

@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Bình luận
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
@include('admin.template.alert_ajax')
<div class="row">
    <div class="col-md-12">  
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database"></i>Dữ liệu</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <!--                    <a href="javascript:;" class="reload" data-url="/hahahah" data-error-display="toastr"  data-error-display-text="FUCK YOU" data-original-title="WHAT THE FUCK" title=""> </a>-->
                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
				<div class="tab-content">
					
						<div id="dataTables_content" class="dataTables_wrapper no-footer dataTables_content" data-quick-url="">   
						   
										 @include('admin.comment.ajax')
						
									
						</div>
					
				</div>
            </div>
        </div>
    </div>
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="multi_delete('{{action('\ADMIN\CommentController@postDeleteMulti')}}', 'dataTables_content')" target="_blank" class="active">
                <span>Xóa</span>
                <i class="icon-trash"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection
