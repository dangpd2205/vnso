<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th scope="col" style="width: 1%;" ></th>
                                        <th>Người gửi</th>
                    <th>Nội dung</th>
                    <th>Bài viết</th>
                    
                                        <th style="width: 10%;">Edit</th>
            </tr>
        </thead>
        <tbody id="tbl_content_ajax">

@if(count($data)==0)
<tr>
    <td colspan="5">NO DATA</td>
</tr>
@endif
<?php $i=0; ?>
<?php foreach ($data as $item): ?>
    <tr>
        <td class="text-center">
            <input type="checkbox" name="id[]" value="{{$item->id}}">
        </td>
        <td>
            {{$item->name}}
        </td>
        <td>
            {{$item->content}}
        </td>
        <td>
            <?php
            if(isset($arr_post[$i]) && count($arr_post[$i])>0){
				if(isset($arr_post[$i]->news_title)){
					?>
					<a href="{{URL::route('route_data', $arr_post[$i]->news_slug.'dn')}}" title="Xem bài viết" target="_blank">
					<?php
					echo $arr_post[$i]->news_title;
					?>
					</a>
					<?php
				}
                if(isset($arr_post[$i]->name)){
                    ?>
                    <a href="{{URL::route('route_data', $arr_post[$i]->slug.'ds')}}" title="Xem bài viết" target="_blank">
                    <?php
                    echo $arr_post[$i]->name;
                    ?>
                    </a>
                    <?php
                }
            }
             ?>
            
        </td>
        <td>
            <input type="hidden" data-edit="id" name="id" value="{{$item->id}}"/>
            &nbsp;&nbsp;
            <a href="javascript:void(0)" onclick="delete_reload({{$item->id}},'{{action('\ADMIN\CommentController@postDelete')}}')"><i class="fa fa-trash-o"></i></a>&nbsp;&nbsp;
            @if($item->status==0)<a href="javascript:void(0)" title="Duyệt" onclick="delete_reload({{$item->id}},'{{action('\ADMIN\CommentController@postPostConfirm')}}')"><i class="fa fa-gavel"></i></a>&nbsp;&nbsp; @endif
        </td>
    </tr>
    <?php $i++; ?>
<?php endforeach; ?>
        </tbody>
    </table>
</div>
@if($data->links()!='')
<div class="row">
    <div class="col-md-5 col-sm-12">
        <div class="dataTables_info" id="sample_2_info" role="status" aria-live="polite">Đang hiển thị trang {{$data->getCurrentPage()}} đến {{$data->getLastPage()}} của {{$data->getTotal()}} dữ liệu</div>
    </div>
    <div class="col-md-7 col-sm-12">
        <div class="dataTables_paginate paging_bootstrap_number paginate_normal" >
            {{$data->links('admin.template_admin.pagination')}} 
        </div>
    </div>
</div>
@endif