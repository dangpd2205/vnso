<?php
$data_content = $data
?>    
<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th scope="col">Tiêu đề</th>
               
                <th scope="col" style="width: 150px;">Thời gian</th>
                <th scope="col" style="width: 120px;">Trạng thái</th>
                <th style="width: 10%;">Edit</th>
            </tr>
        </thead>
        <tbody id="tbl_content_ajax">
            @if(count($data_content)==0)
            <tr>
                <td colspan="4">NO DATA</td>
            </tr>
            @endif
            <?php
            $data_status = Lang::get('general.news_status');
            unset($data_status['']);

            foreach ($data_content as $item):
                ?>
            <tr>
                <td>
                    <a href="{{action('\ADMIN\PlaceController@getEdit')}}/{{$item->lang_id}}/{{$item->place_id}}"> {{$item->name}}</a>
                </td>
               
                <td>
                   {{$item->created_at}}
                </td>
                <td>
                    
        			<?php
                        if (array_key_exists($item->status, $data_status)) {
                            echo $data_status[$item->status];
                        }
                        ?>
        			
                </td>
                <td>
                    <a href="{{action('\ADMIN\PlaceController@getEdit')}}/{{$item->lang_id}}/{{$item->place_id}}" title="Chỉnh sửa "> <i class="fa fa-pencil"></i></a>&nbsp;&nbsp;
                 
                  
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
@if($data_content->links()!='')
<div class="row">
    <div class="col-md-5 col-sm-12">
        <div class="dataTables_info" id="sample_2_info" role="status" aria-live="polite">Đang hiển thị trang {{$data_content->getCurrentPage()}} đến {{$data_content->getLastPage()}} của {{$data_content->getTotal()}} dữ liệu</div>
    </div>
    <div class="col-md-7 col-sm-12">
        <div class="dataTables_paginate paging_bootstrap_number paginate_normal" >
            {{$data_content->links('admin.template_admin.pagination')}} 
        </div>
    </div>
</div>
@endif

