@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.users.users.file')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Địa điểm diễn
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")

{{Form::open(array('action' => '\ADMIN\PlaceController@postEdit','id'=>'edit_news','class'=>'form-horizontal'))}}
    
    {{Form::hidden('id_place', $data_news[0]->place_id)}}
<div class="row">
    <div class="col-md-12">  
	@include('admin.template.error')
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#content-tab1" data-toggle="tab"><i class="fa fa-globe fa-lg text-default"></i> Nội dung chính</a>
            </li>
            <li >
                <a href="#content-tab2" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> Ảnh</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade  active in" id="content-tab1"> 	
				
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">

                                <?php
                                foreach ($l_lang as $item) {
                                    if ($item->id == $g_config_all->website_lang) {
                                        ?>
                                        <a href="#lang_p_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                        <?php
                                    }
                                }
                                ?>

                            </li>
                            <?php if (count($l_lang) > 1) { ?>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                    <ul class="dropdown-menu text-left">
                                        <?php
                                        foreach ($l_lang as $item_child) {
                                            if ($item_child->id != $g_config_all->website_lang) {
                                                ?>
                                                <li> <a href="#lang_p_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>   
                        <?php } ?>
                        </ul>  
                    
                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                $tran_class='';
                                    foreach ($l_lang as $item_content) {
                                        $data=[];
                                        foreach($data_news as $item_data){
                             
                                            if($item_data->langid==$item_content->id){
                                            
                                                $data=$item_data;
                                            }
                                        }
                                        ?>
                                        <div class="tab-pane fade in <?php
                                        if ($item_content->id == $g_config_all->website_lang) {
                                            echo ' active';
                                        }
                                        ?> " id="lang_p_{{$item_content->id}}">
                                        <h3>{{$item_content->name}}</h3>               
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label" for="name_{{$item_content->code}}">Tiêu đề</label>
                                                    <div class="col-lg-6">
                                                        <?php 
                                                        $name='';
                                                        if(count($data)>0 && isset($data->name)){
                                                            $name=$data->name;
                                                        }
                                                        ?>
                                                        {{Form::text('name_'.$item_content->code, $name, array('class'=>'bg-focus form-control input-sm', 'id'=>'name_'.$item_content->code))}}
                                                        <input type="hidden" name="lang_id_{{$item_content->code}}" value="{{$item_content->id}}"/>
                                                    </div> 
                                                </div>
												<div class="form-group">
                                                    <label class="col-lg-2 control-label">Địa chỉ</label>
                                                    <div class="col-lg-6">
														<?php 
                                                        $address='';
                                                        if(count($data)>0 && isset($data->address)){
                                                            $address=$data->address;
                                                        }
                                                        ?>
                                                        {{Form::text('address_'.$item_content->code, $address, array('class'=>'bg-focus form-control input-sm', 'id'=>'address_'.$item_content->code))}}
                                                    </div> 
                                                </div>
												<div class="form-group">
                                                    <label class="col-lg-2 control-label">Mô tả</label>
                                                    <div class="col-lg-6">
                                                        <?php 
                                                        $content='';
                                                        if(count($data)>0 && isset($data->content)){
                                                            $content=$data->content;
                                                        }
                                                        ?>
                                                        {{Form::textarea('content_'.$item_content->code, $name, array('class'=>'bg-focus form-control input-sm tinymce', 'id'=>'content_'.$item_content->code))}}
                                                        
                                                    </div> 
                                                </div>
                                            </div>
                                            
                                        </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="tab-pane fade" id="content-tab2">
                <div class="form-group">
                    <label class="col-lg-2 control-label">Ảnh đại diện (353x236)</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('avatar',$data_news[0]->avatar, array('class'=>'bg-focus form-control', 'id'=>'avatar'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=avatar"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>
    </div>
  
</div>
{{Form::close()}}
<style>
    .popover .arrow{
        display: none;
    }
</style>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#edit_news').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection