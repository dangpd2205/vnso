<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>Pubwebvn</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{Asset('asset/admin')}}/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{Asset('asset/admin')}}/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{{Asset('asset/admin')}}/pages/css/lock-2.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->
    <body class="">
        <div class="page-lock">
            <!--            <div class="page-logo">
                            <a class="brand" href="index.html">
                                <img src="{{Asset('asset/admin')}}/pages/img/logo-big.png" alt="logo" /> </a>
                        </div>-->
            <div class="page-body">
                <img class="page-lock-img" src="{{$curent_user_lock->avatar}}" alt="">
                <div class="page-lock-info">
                    <h1>{{$curent_user_lock->full_name}}</h1>
                    <span class="email"> {{$curent_user_lock->email}} </span>
                    <span class="locked"> Locked </span>
                    {{Form::open(array('action'=>'\ADMIN\HomeController@postLock', 'class'=>'form-inline', 'id'=>'submit_password_lock'))}}
                    <div class="input-group input-medium">
                        {{Form::password('password', ["class" => "form-control","required"=>"required", "id" => "password", "placeholder" => "Password"])}}
                        <span class="input-group-btn">
                            <button type="submit" class="btn green icn-only">
                                <i class="m-icon-swapright m-icon-white"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <div class="relogin">
                        <a href="login.html"> Not {{$curent_user_lock->full_name}} ? </a>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <div class="page-footer-custom">  <?php echo date('Y', time()); ?> &copy; <a target="_blank" href="https://pubweb.vn">Thi?t k? website chuy�n nghi?p</a>  Pubweb.vn   </div>
        </div>
        <!--[if lt IE 9]>
<script src="{{Asset('asset/admin')}}/global/plugins/respond.min.js"></script>
<script src="{{Asset('asset/admin')}}/global/plugins/excanvas.min.js"></script> 
<script src="{{Asset('asset/admin')}}/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{Asset('asset/admin')}}/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{Asset('asset/admin')}}/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{Asset('asset/admin')}}/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{Asset('asset/admin')}}/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{Asset('asset/admin')}}/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{Asset('asset/admin')}}/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{Asset('asset/admin')}}/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{Asset('asset/admin')}}/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{Asset('asset/admin')}}/pages/scripts/lock-2.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
    </body>
</html>