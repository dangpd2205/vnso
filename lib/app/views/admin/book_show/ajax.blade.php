<?php
$data_content = $data
?>    
<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th scope="col">Tiêu đề</th>
                <th scope="col">Nghệ sỹ tham gia</th>
                <th scope="col">Thông tin vé</th>
                <th scope="col" style="width: 150px;">Ngày diễn ra</th>
                <th scope="col" style="width: 120px;">Trạng thái</th>
                <th style="width: 10%;">Đặt vé</th>
            </tr>
        </thead>
        <tbody id="tbl_content_ajax">
            @if(count($data_content)==0)
            <tr>
                <td colspan="6">NO DATA</td>
            </tr>
            @endif
            <?php
            $data_status = Lang::get('general.news_status');
            unset($data_status['']);
            $i=0;
            foreach ($data_content as $item):
                ?>
            <tr>
                <td>
                    <a href="{{action('\ADMIN\BookShowController@getAdd')}}/{{$item->show_id}}"> {{$item->name}}</a>
                </td> 
                <td>
                    <?php 
					
					if(isset($list_art[$i]) && count($list_art[$i])>0){
                        foreach($list_art[$i] as $art){
                            echo $art->a_title.'&nbsp;';
                        }

                    } ?>
                </td>   
                <td>
                    <?php 
                    $total =0;
                    $sold=0;
                    if(isset($list_seat[$i]) && $list_seat[$i]>0){
                        $total = $list_seat[$i];
                    } 
                    if(isset($list_seat_sold[$i]) && $list_seat_sold[$i]>0){
                        $sold = $list_seat_sold[$i];
                    } 
                    ?>
                   Tổng ghế: {{$total}}<br/>
                   Ghế đã bán: {{$sold}}<br/>
                   Ghế còn: {{$total-$sold}}<br/>
                </td>            
                <td>
                   {{date('d M Y H:i',$item->time_show)}}
                </td>
                <td>
                    
        			<?php
                        if (array_key_exists($item->status, $data_status)) {
                            echo $data_status[$item->status];
                        }
                        ?>
        			
                </td>
                <td>
					@if($item->is_sell==0)
                    <a href="{{action('\ADMIN\BookShowController@getAdd')}}/{{$item->show_id}}" title="Đặt vé"> <i class="fa fa-shopping-cart"></i></a>&nbsp;&nbsp;                 
					@endif
                </td>
            </tr>
        <?php $i++;endforeach; ?>
        </tbody>
    </table>
</div>
@if($data_content->links()!='')
<div class="row">
    <div class="col-md-5 col-sm-12">
        <div class="dataTables_info" id="sample_2_info" role="status" aria-live="polite">Đang hiển thị trang {{$data_content->getCurrentPage()}} đến {{$data_content->getLastPage()}} của {{$data_content->getTotal()}} dữ liệu</div>
    </div>
    <div class="col-md-7 col-sm-12">
        <div class="dataTables_paginate paging_bootstrap_number paginate_normal" >
            {{$data_content->links('admin.template_admin.pagination')}} 
        </div>
    </div>
</div>
@endif

