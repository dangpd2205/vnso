@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.alert_ajax')
@include('admin.book_show.add_modal')
@endsection
@section("breadcrumb")
<li>
    <a href="{{Asset('')}}">Trang chủ</a>
    <i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
    Đặt vé
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
<link href="{{Asset('asset/admin')}}/map.css" rel="stylesheet" type="text/css" />
<script>
    $('document').ready(function(){
        
        $('.pick_seat').click(function(){
            $.ajax({
                url: $(this).data('url'),
                data: {id: $("#show_id").val()},
                type: 'POST',
                dataType: 'html',   
                success: function (msg) { 
                    $('.content-seat').html(msg);   
                    if($("#name_seat").val()!='' && $("#type_seat").val()!=''){
                        var name_seat_lock = $("#name_seat").val().split(",");
                        var type_seat_lock = $("#type_seat").val().split(",");
                        name_seat_lock.forEach(function(item,index){
                            var span_name = name_seat_lock[index];
                            var type_name = type_seat_lock[index];
                            $(".list-seat div").each(function(){
                                if($(this).find("input").val() == span_name && $(this).find("input").data('type') == type_name){
                                    if(!$(this).find("input").is(':checked')){
                                        $(this).find("input").attr("checked",'checked');
                                    }
                                }
                            });
                        })
                    }
                    $('#modal_lock_seat').modal();             
                }
            });
           
        });
    });
    
</script>
<link href="{{Asset('asset/admin')}}/map.css" rel="stylesheet" type="text/css" />
{{Form::open(array('action' => '\ADMIN\BookShowController@postEdit','id'=>'add_news','class'=>'form-horizontal'))}}
<input type="hidden" name="show_id" id="show_id" value="{{$show->show_id}}"/>
<input type="hidden" name="place_id" id="place_id" value="{{$show->place_id}}"/>
<div class="row">
    <div class="col-md-12">  
        @include('admin.template.error')
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#content-tab1" data-toggle="tab"><i class="fa fa-globe fa-lg text-default"></i> Thông tin đặt vé</a>
            </li>
            <li>
                <label class="col-lg-12 control-label">{{$show->name}} - {{date('d/m/Y H:i',$show->time_show)}}</label>
            </li>
        </ul>    
        <div class="tab-content">
            <div class="tab-pane fade  active in" id="content-tab1">
                  
                <div class="form-group col-lg-6">
                    <label class="col-lg-5 control-label">Họ tên</label>
                    <div class="col-lg-6">
                        {{Form::text('name',  $data->cus_name, array('class'=>'bg-focus form-control input-sm', 'id'=>'name'))}}
                    </div> 
                </div>
                <div class="form-group col-lg-6">
                    <label class="col-lg-5 control-label">Số điện thoại</label>
                    <div class="col-lg-6">
                        {{Form::text('phone',  $data->cus_phone, array('class'=>'bg-focus form-control input-sm', 'id'=>'phone','onkeypress'=>'return event.charCode > 47 && event.charCode < 58;'))}}
                    </div> 
                </div>
                <div class="form-group col-lg-6">
                    <label class="col-lg-5 control-label">Email</label>
                    <div class="col-lg-6">
                        {{Form::text('email',  $data->cus_email, array('class'=>'bg-focus form-control input-sm', 'id'=>'email'))}}
                    </div> 
                </div>
                <div class="form-group col-lg-6">
                    <label class="col-lg-5 control-label">Địa chỉ</label>
                    <div class="col-lg-6">
                        {{Form::text('address',  $data->cus_address, array('class'=>'bg-focus form-control input-sm', 'id'=>'address'))}}
                    </div> 
                </div>   
                
                <div class="col-lg-12">

                    <label class="col-lg-1 control-label pick_seat btn btn-danger" data-url="{{action('\ADMIN\BookShowController@postModal')}}">Chọn ghế:</label>   
                    <?php
                    $name_seat = '';
                    $type_seat = '';
                    $type='';
                    $total=0;
                    if(isset($order_detail) && count($order_detail)>0){
                        foreach($order_detail as $item){
                            if($name_seat!=''){
                                $name_seat.=','.$item->seat_name;
                            }else{
                                $name_seat.=$item->seat_name;
                            }
                            if($item->seat_type==1){
                                $type = 'seat-b';
                            }else if($item->seat_type==2){
                                $type = 'seat-a';
                            }else if($item->seat_type==3){
                                $type = 'seat-s';
                            }
                            if($type_seat!=''){
                                $type_seat.=','.$type;
                            }else{
                                $type_seat.=$type;
                            }
                            $total+=$item->seat_price;
                        }
                    }
                    ?>
                    
                    <input type="hidden" name="name_seat" id="name_seat" value="{{$name_seat}}"/>                 
                    <input type="hidden" name="type_seat" id="type_seat" value="{{$type_seat}}"/>
                    
                    <div class="col-lg-8 control-label" id="span_name">
                        @foreach($order_detail as $item)
                       <span class="label label-sm label-success" style="margin:2px">{{$item->seat_name}}</span>
                       @endforeach
                    </div>                 
                </div>
                <div class="col-lg-12">
                    <label class="col-lg-1 control-label btn btn-warning">Tổng tiền:</label> 
                    <div class="col-lg-8 control-label" style="text-align:left;">
                       <span id="span_total">{{number_format($total)}} </span> VNĐ
                    </div>
                </div>                         
            </div>
        </div>
    </div>
    
</div>
{{Form::close()}}
<style>
    .popover .arrow{
        display: none;
    }
</style>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#add_news').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-plus"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection