@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.alert_ajax')
@include('admin.book_show.add_modal')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Đặt vé
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
<style>

.tks .bg-ticket{
    width: 80px;
    margin:0 auto 10px auto;

}

.last-time{
    background-repeat: no-repeat;
    background-size: 100%;
    width: 100%;
    margin-top:40px;
    padding:54px 60px;
    text-align: center;
    margin-bottom: 40px;
}
.last-time .bor-time{
    display: inline-block;
    width: 88%;
   border:2px solid #e5e5e5;
}
.last-time .bor-time .info-left{
    float: left;

    padding: 50px;
}
.last-time .bor-time .info-left ul{
    list-style: none;
    padding-left:0px;
}
.last-time .bor-time .info-left ul li{
    text-align: left;
    font-weight: 400;
    color: #4e4e4e;
    margin-bottom: 7px;
    text-transform: uppercase;
}
.last-time .bor-time .info-left ul li:first-child{

    margin-bottom: 20px;
}
.last-time .bor-time .info-left ul li:first-child span,.last-time .bor-time .info-left ul li:last-child span{
    font-weight: 500;
    margin-left: 15px;
}
.last-time .bor-time .info-left ul li:last-child{
    margin-top:20px;
}
.last-time .bor-time .info-right{
    float:left;
    text-align: left;
    margin-top:40px;
        border-left: 2px solid #e5e5e5;
    padding:15px 40px 50px 40px;
}
.last-time .bor-time .info-right .up-info{
    padding-bottom: 10px;
    border-bottom:  2px solid #e5e5e5;
    margin-bottom: 20px;
}
.last-time .bor-time .info-right .up-info .text-1{
    margin-top:0px;
    text-transform: uppercase;
    font-weight: 500;
        color: #4e4e4e;
        font-size:17px;
        margin-bottom: 15px;
}
.last-time .bor-time .info-right .up-info .text-2{
    font-weight: 400;
    font-size:17px;
}
.last-time .bor-time .info-right .down-info .text-seat{
        margin-top: 0px;
    text-transform: uppercase;
    font-weight: 500;
    color: #4e4e4e;
    font-size: 17px;
    margin-bottom: 15px;
}
.last-time .bor-time .info-right .down-info .text-seat span{
    color:#b11500;
}
.print{
    text-align: center;
    margin-bottom: 30px;
}
.print ul{  
    list-style: none;
    padding-left:0px;
}
.print ul li{
    display: inline-block;
    margin-left: -5px;

}
.print ul li a{
        font-size: 13px;
    color: #4e4e4e;
    border: 1px solid #4e4e4e;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    width: 130px;
    position: relative;
    padding: 5px 14px 6px 14px;
    -webkit-transition: 0.5s;
    transition: 0.5s;
}
.print ul li a:hover{
    color:white;
    background: #4e4e4e;
}
</style>
<link href="{{Asset('asset/admin')}}/map.css" rel="stylesheet" type="text/css" />
<script>
    $('document').ready(function(){
        
        $('.pick_seat').click(function(){
            $.ajax({
                url: $(this).data('url'),
                data: {id: $("#show_id").val()},
                type: 'POST',
                dataType: 'html',   
                success: function (msg) { 
                    $('.content-seat').html(msg);   
                    if($("#name_seat").val()!='' && $("#type_seat").val()!=''){
                        var name_seat_lock = $("#name_seat").val().split(",");
                        var type_seat_lock = $("#type_seat").val().split(",");
                        name_seat_lock.forEach(function(item,index){
                            var span_name = name_seat_lock[index];
                            var type_name = type_seat_lock[index];
                            $(".list-seat div").each(function(){
                                if($(this).find("input").val() == span_name && $(this).find("input").data('type') == type_name){
                                    if(!$(this).find("input").is(':checked')){
                                        $(this).find("input").attr("checked",'checked');
                                    }
                                }
                            });
                        })
                    }
                    $('#modal_lock_seat').modal();             
                }
            });
           
        });
    });
    
</script>
<link href="{{Asset('asset/admin')}}/map.css" rel="stylesheet" type="text/css" />
{{Form::open(array('action' => '\ADMIN\BookShowController@postAdd','id'=>'add_news','class'=>'form-horizontal'))}}
<input type="hidden" name="show_id" id="show_id" value="{{$show->show_id}}"/>
<input type="hidden" name="place_id" id="place_id" value="{{$place->place_id}}"/>
<div class="row">
    <div class="col-md-12">  
		@include('admin.template.error')
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#content-tab1" data-toggle="tab"><i class="fa fa-globe fa-lg text-default"></i> Thông tin đặt vé</a>
            </li>
            <li>
                <label class="col-lg-12 control-label">{{$show->name}} - {{date('d/m/Y H:i',$place->time_show)}}</label>
            </li>
        </ul>    
        <div class="tab-content">
            <div class="tab-pane fade  active in" id="content-tab1">
				<div class="form-group col-lg-6">
                    <label class="col-lg-5 control-label">Họ tên</label>
                    <div class="col-lg-6">
                        {{Form::text('name',  '', array('class'=>'bg-focus form-control input-sm', 'id'=>'name'))}}
                    </div> 
                </div>
                <div class="form-group col-lg-6">
                    <label class="col-lg-5 control-label">Số điện thoại</label>
                    <div class="col-lg-6">
                        {{Form::text('phone',  '', array('class'=>'bg-focus form-control input-sm', 'id'=>'phone','onkeypress'=>'return event.charCode > 47 && event.charCode < 58;'))}}
                    </div> 
                </div>
                <div class="form-group col-lg-6">
                    <label class="col-lg-5 control-label">Email</label>
                    <div class="col-lg-6">
                        {{Form::text('email',  '', array('class'=>'bg-focus form-control input-sm', 'id'=>'email'))}}
                    </div> 
                </div>
                <div class="form-group col-lg-6">
                    <label class="col-lg-5 control-label">Địa chỉ</label>
                    <div class="col-lg-6">
                        {{Form::text('address',  '', array('class'=>'bg-focus form-control input-sm', 'id'=>'address'))}}
                    </div> 
                </div>   
                <div class="col-lg-12">
                    <label class="col-lg-1 control-label pick_seat btn btn-danger" data-url="{{action('\ADMIN\BookShowController@postModal')}}">Chọn ghế:</label>   
                    <input type="hidden" name="name_seat" id="name_seat"/>                 
                    <input type="hidden" name="type_seat" id="type_seat"/>
                    
                    <div class="col-lg-8 control-label" id="span_name">
                       
                    </div>                 
                </div>
                <div class="col-lg-12">
                    <label class="col-lg-1 control-label btn btn-warning">Tổng tiền:</label> 
                    <div class="col-lg-8 control-label" style="text-align:left;">
                       <span id="span_total">0 </span> VNĐ
                    </div>
                </div>  
                                     
            </div>
        </div>
    </div>
    
</div>
{{Form::close()}}
<style>
    .popover .arrow{
        display: none;
    }
</style>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#add_news').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-plus"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection