@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.alert_ajax')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Đặt vé
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")

<script>
    var list_category = '';
</script>
<div class="row">
    <div class="col-md-12">  
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-filter"></i>Bộ lọc</div>
                <div class="tools">
                    <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body display-hide form">
                {{Form::open(array('url' => '#','class'=>'form-horizontal filter_form'))}}
                <input type="hidden" name="url_filter" value="{{action('\ADMIN\BookShowController@postFilter')}}"/>
                <div class="form-body">
                    <div class="row">
                        
                        <div class="col-md-3">                            
                            <div class="col-md-12">
                                <div class="input-group input-medium date date-picker" data-date="{{date('Y-m-d',time())}}" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                    <input type="text" class="form-control" name="start_date_filter"/>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                                
                                
                            </div>
                        </div>
                        <div class="col-md-3">                            
                            <div class="col-md-12">                                
                                <div class="input-group input-medium date date-picker" data-date="{{date('Y-m-d',time())}}" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                    <input type="text" class="form-control" name="end_date_filter"/>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-10">                                   
                                    <select class="bg-focus form-control input-sm m-b" name="news_cat_filter">
                                        <option value="">Danh mục</option>                                              
                                        <?php
                                        foreach ($list_category as $item_cat) {
                                            if ($item_cat->parent == 0) {
                                                ?>
                                                <option value="<?php echo $item_cat->id ?>"><?php echo $item_cat->name ?></option>
                                                <?php
                                                foreach ($list_category as $item_cat1) {
                                                    if ($item_cat1->parent == $item_cat->id) {
                                                        echo '<option value="' . $item_cat1->id . '"> — ' . $item_cat1->name . '</option>';
                                                    }
                                                }
                                            }
                                            
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-10">
                                    <select class="bg-focus form-control input-sm m-b" name="news_status_filter">
                                        <option value="">Trạng thái</option>  
                                        <option value="1">Đã đăng</option> 
                                        <option value="0">Chờ đăng</option> 
                                        <option value="2">Xóa</option> 
                                    </select>
                                </div>
                            </div>
                        </div>
                         <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-10">
                                    <select class="bg-focus form-control input-sm m-b" name="news_place_filter">
                                        <option value="">Địa điểm</option>  
                                        @if(isset($place) && count($place)>0)
                                        @foreach($place as $i_place)
                                        <option value="{{$i_place->place_id}}">{{$i_place->name}}</option> 
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <a href="javascript:;" class="btn green-meadow btn_submit_filter"> <i class="fa fa-filter"></i> Lọc</a>
                </div>
                {{Form::close()}}
            </div>
        </div>
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-database"></i>Dữ liệu</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <!--                    <a href="javascript:;" class="reload" data-url="/hahahah" data-error-display="toastr"  data-error-display-text="FUCK YOU" data-original-title="WHAT THE FUCK" title=""> </a>-->
                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-md-9 col-sm-12">
                                <div class="dataTables_length">
                                    {{Form::open(array('url' => '#','class'=>'row_setting_form'))}}
                                    <input type="hidden" name="url_search" value="{{action('\ADMIN\BookShowController@postShow')}}"/>
                                    <label>
                                        <select name="row_table_setting" class="form-control input-sm input-xsmall input-inline">
                                            <option value="10">10</option>
                                            <option value="15">15</option>
                                            <option value="20">20</option>
                                            <option value="30">30</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select> 
                                        dòng</label>
                                    {{Form::close()}}
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                {{Form::open(array('url' => '#','class'=>'search_form'))}}
                                <input type="hidden" name="url_search" value="{{action('\ADMIN\BookShowController@postSearch')}}"/>

                                <div class="input-group input-group-sm">
                                    <input type="text" class="form-control" name="search_key" placeholder="Search for...">
                                    <span class="input-group-btn">
                                        <button class="btn red" type="submit"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>  
                        <div id="dataTables_content" class="dataTables_wrapper no-footer dataTables_content" data-quick-url="">                 
                            @include('admin.book_show.ajax')                                            
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>

@endsection
