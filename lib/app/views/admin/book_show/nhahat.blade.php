<div class="col-md-12">
    <div class="info-book">
        <p>@if(isset($place) && count($place)>0){{$place[0]->name}}@endif | Số ghế ({{count($place)}})</p>
        <p>@if(isset($show) && count($show)>0){{date('d/m/Y H:i',$show->time_show)}}@endif</p>
    </div>
</div> 
<div class="col-md-12">
    <div class="info-seat">
        <ul>
            <li class="list-1"><span></span> Đang chọn</li>
            <li class="list-3"><span></span> Không thể chọn</li>
            @if(isset($ticket) && count($ticket)>0)
            @foreach($ticket as $i_ticket)
            <li class="@if($i_ticket->ticket_id==1) list-6 @elseif($i_ticket->ticket_id==2) list-5 @else list-4 @endif"><span></span> {{$i_ticket->name}}
                <small>({{number_format($i_ticket->price)}} VNĐ)</small>           
            </li>
            @endforeach
            @endif
        </ul>
    </div>
</div>                         
<div class="col-md-12">
   	<div class="tick-seat seat-nhahat">
		<img src="/asset/admin/so-do-ghe-vnso-final.png" alt="img">
		<div class="list-seat">
			@if(isset($data) && count($data)>0)
			@foreach($data as $item)
			<div class="{{$item->seat_type_class}} @if($item->status!=1 && $item->status!=4) unvail @endif">
				<input data-price="{{$item->price}}" data-type="{{$item->seat_position_class}}" type="checkbox" name="seat[]" value="{{$item->seat_name_orin}}" id="s-{{$item->id}}" @if($item->status!=1 && $item->status!=4) disabled @endif>
				<label for="s-{{$item->id}}">{{$item->seat_name}}</label>
			</div>
			@endforeach
			@endif
			
		</div>
	</div>
</div>
