@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("breadcrumb")
<li>
    <a href="{{Asset('')}}">Trang chủ</a>
    <i class="fa fa-circle"></i>
</li>
<li>Cấu hình <i class="fa fa-circle"></i></li>
<li>
    Cấu hình
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
@include('admin.template.select_file_modal')
@include('admin.template.alert_ajax')
{{Form::open( array('action'=>'\ADMIN\SettingController@postInfo', 'class'=>'form-horizontal','id'=>'save_setting' ))}}
<?php
if ($data) {
    $data_setting = $data;
}
?>

<div class="row">
    <div class="col-md-12"> 
        <div>
            <div class="form-group">
                <label class="col-lg-2 control-label" for="from_mail">From mail</label>
                <div class="col-lg-6">
                    {{Form::text('from_mail', isset($data_setting->from_mail) ? $data_setting->from_mail: '', ['id'=>'from_mail','class'=>'bg-focus form-control'])}}                            
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label" for="mail_username">Username</label>
                <div class="col-lg-6">
                    {{Form::text('mail_username', isset($data_setting->mail_username) ? $data_setting->mail_username: '', ['id'=>'mail_username','class'=>'bg-focus form-control'])}}                            
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label" for="mail_password">Password</label>
                <div class="col-lg-6">
                    {{Form::text('mail_password', isset($data_setting->mail_password) ? $data_setting->mail_password: '', ['id'=>'mail_password','class'=>'bg-focus form-control'])}}                            
                </div>
            </div>
        </div>
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-envelope-o"></i>Mẫu email feedback</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="col-lg-9">
                            {{Form::textarea('website_template_email', isset($data_setting->website_template_email) ? $data_setting->website_template_email: '', ['class'=>'bg-focus form-control tinymce'])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#save_setting').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection
