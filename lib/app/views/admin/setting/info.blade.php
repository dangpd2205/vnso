@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("breadcrumb")
<li>
    <a href="{{Asset('')}}">Trang chủ</a>
    <i class="fa fa-circle"></i>
</li>
<li>Cấu hình <i class="fa fa-circle"></i></li>
<li>
    Cấu hình
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
@include('admin.template.select_file_modal')
@include('admin.template.alert_ajax')
{{Form::open( array('action'=>'\ADMIN\SettingController@postInfo', 'class'=>'form-horizontal','id'=>'save_setting' ))}}
<?php
if ($data) {
    $data_setting = $data;
}
?>

<div class="row">
    <div class="col-md-12"> 
        <div class="form-group">
            <label class="col-lg-2 control-label" for="contact_company">Website</label>
            <div class="col-lg-6">
                {{Form::text('contact_website', isset($data_setting->contact_website) ? $data_setting->contact_website: '', ['id'=>'contact_website','class'=>'bg-focus form-control'])}}
            </div> 
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="contact_phone">Số điện thoại</label>
            <div class="col-lg-6">
                {{Form::text('contact_phone', isset($data_setting->contact_phone) ? $data_setting->contact_phone: '', ['id'=>'contact_phone','class'=>'bg-focus form-control'])}}
            </div> 
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Mobile</label>
            <div class="col-lg-6">
                {{Form::text('contact_mobile', isset($data_setting->contact_mobile) ? $data_setting->contact_mobile: '', ['id'=>'contact_mobile','class'=>'bg-focus form-control'])}}
            </div> 
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="contact_fax">FAX</label>
            <div class="col-lg-6">
                {{Form::text('contact_fax', isset($data_setting->contact_fax) ? $data_setting->contact_fax: '', ['id'=>'contact_fax','class'=>'bg-focus form-control'])}}
            </div> 
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="contact_email">Email</label>
            <div class="col-lg-6">
                {{Form::text('contact_email', isset($data_setting->contact_email) ? $data_setting->contact_email: '', ['id'=>'contact_email','class'=>'bg-focus form-control'])}}
            </div> 
        </div>
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#save_setting').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection
