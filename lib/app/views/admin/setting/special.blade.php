@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("breadcrumb")
<li>
    <a href="{{Asset('')}}">Trang chủ</a>
    <i class="fa fa-circle"></i>
</li>
<li>Cấu hình <i class="fa fa-circle"></i></li>
<li>
    Cấu hình
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
@include('admin.template.select_file_modal')
@include('admin.template.alert_ajax')
{{Form::open( array('action'=>'\ADMIN\SettingController@postInfo', 'class'=>'form-horizontal','id'=>'save_setting' ))}}
<?php
if ($data) {
    $data_setting = $data;
}
?>

<div class="row">
    <div class="col-md-12"> 
        <div class="form-group">
			<label class="col-lg-2 control-label">Thứ 2</label>
			<div class="col-lg-4">
				
				<input type="text" class="form-control" value="@if(isset($monday) && count($monday)>0){{$monday->name}}@endif" id="monday" name="monday" onkeyup="fillone('{{URL::action('\ADMIN\SettingController@postSpecial')}}','monday','div_monday')" placeholder="Nhập tên sản phẩm ..."/>
				<div class="div_autocomplete" id="div_monday">

				</div>
				
				<input type="hidden" name="id_monday" id="id_monday" value="@if(isset($data_setting->id_monday)){{$data_setting->id_monday}}@endif"/>
			</div>                                      
		</div>
		<div class="form-group">
			<label class="col-lg-2 control-label">Thứ 3</label>
			<div class="col-lg-4">
				<input type="text" class="form-control" value="@if(isset($tuesday) && count($tuesday)>0){{$tuesday->name}}@endif" id="tuesday" name="tuesday" onkeyup="fillone('{{URL::action('\ADMIN\SettingController@postSpecial')}}','tuesday','div_tuesday')" placeholder="Nhập tên sản phẩm ..."/>
				<div class="div_autocomplete" id="div_tuesday">

				</div>
				
				<input type="hidden" name="id_tuesday" id="id_tuesday" value="@if(isset($data_setting->id_tuesday)){{$data_setting->id_tuesday}}@endif"/>
			</div>                                      
		</div>
		<div class="form-group">
			<label class="col-lg-2 control-label">Thứ 4</label>
			<div class="col-lg-4">
				<input type="text" class="form-control" value="@if(isset($wednesday) && count($wednesday)>0){{$wednesday->name}}@endif" id="wednesday" name="wednesday" onkeyup="fillone('{{URL::action('\ADMIN\SettingController@postSpecial')}}','wednesday','div_wednesday')" placeholder="Nhập tên sản phẩm ..."/>
				<div class="div_autocomplete" id="div_wednesday" >

				</div>
				
				<input type="hidden" name="id_wednesday" id="id_wednesday" value="@if(isset($data_setting->id_wednesday)){{$data_setting->id_wednesday}}@endif"/>
			</div>                                      
		</div>
		<div class="form-group">
			<label class="col-lg-2 control-label">Thứ 5</label>
			<div class="col-lg-4">
				<input type="text" class="form-control" value="@if(isset($thursday) && count($thursday)>0){{$thursday->name}}@endif" id="thursday" name="thursday" onkeyup="fillone('{{URL::action('\ADMIN\SettingController@postSpecial')}}','thursday','div_thursday')" placeholder="Nhập tên sản phẩm ..."/>
				<div class="div_autocomplete" id="div_thursday">

				</div>
				
				<input type="hidden" name="id_thursday" id="id_thursday" value="@if(isset($data_setting->id_thursday)){{$data_setting->id_thursday}}@endif"/>
			</div>                                      
		</div>
		<div class="form-group">
			<label class="col-lg-2 control-label">Thứ 6</label>
			<div class="col-lg-4">
				<input type="text" class="form-control" value="@if(isset($friday) && count($friday)>0){{$friday->name}}@endif" id="friday" name="friday" onkeyup="fillone('{{URL::action('\ADMIN\SettingController@postSpecial')}}','friday','div_friday')" placeholder="Nhập tên sản phẩm ..."/>
				<div class="div_autocomplete" id="div_friday">

				</div>
				
				<input type="hidden" name="id_friday" id="id_friday" value="@if(isset($data_setting->id_friday)){{$data_setting->id_friday}}@endif"/>
			</div>                                      
		</div>
		<div class="form-group">
			<label class="col-lg-2 control-label">Thứ 7</label>
			<div class="col-lg-4">
				<input type="text" class="form-control" value="@if(isset($saturday) && count($saturday)>0){{$saturday->name}}@endif" id="saturday" name="saturday" onkeyup="fillone('{{URL::action('\ADMIN\SettingController@postSpecial')}}','saturday','div_saturday')" placeholder="Nhập tên sản phẩm ..."/>
				<div class="div_autocomplete" id="div_saturday" >

				</div>
				
				<input type="hidden" name="id_saturday" id="id_saturday" value="@if(isset($data_setting->id_saturday)){{$data_setting->id_saturday}}@endif"/>
			</div>                                      
		</div>
		<div class="form-group">
			<label class="col-lg-2 control-label">Chủ nhật</label>
			<div class="col-lg-4">
				<input type="text" class="form-control" value="@if(isset($sunday) && count($sunday)>0){{$sunday->name}}@endif" id="sunday" name="sunday" onkeyup="fillone('{{URL::action('\ADMIN\SettingController@postSpecial')}}','sunday','div_sunday')" placeholder="Nhập tên sản phẩm ..."/>
				<div class="div_autocomplete" id="div_sunday">

				</div>
				
				<input type="hidden" name="id_sunday" id="id_sunday" value="@if(isset($data_setting->id_sunday)){{$data_setting->id_sunday}}@endif"/>
			</div>                                      
		</div>
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#save_setting').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection
