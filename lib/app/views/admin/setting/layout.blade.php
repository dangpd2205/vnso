@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.select_file_modal')
@include('admin.template.alert_ajax')
@endsection
@section("breadcrumb")
<li>
    <a href="{{Asset('')}}">Trang chủ</a>
    <i class="fa fa-circle"></i>
</li>
<li>Cấu hình <i class="fa fa-circle"></i></li>
<li>
    Cấu hình
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
<style>
    .ui-autocomplete {
        max-height: 200px;
        overflow-y: auto;
        overflow-x: hidden;
    }
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">

{{Form::open( array('action'=>'\ADMIN\SettingController@postInfo', 'class'=>'form-horizontal','id'=>'save_setting' ))}}
<?php
if ($data) {
    $data_setting = $data;
}
?>

<div class="row">
    <div class="col-md-12"> 
        <div class="row">
            <div class="form-group col-lg-6">
                <label class="col-lg-6 control-label no-padding-top" for="website_comment">Comment toàn hệ thống</label>
                <div class="col-lg-4">
                        <label class="mt-checkbox">
                            {{Form::checkbox('website_comment', 1, isset($data_setting->website_comment) ? $data_setting->website_comment: '')}} Bật/tắt <span></span></label>
                    
                </div> 
            </div>
        </div>
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <ul class="nav nav-tabs">
                    <li class="active">

                        <?php
                        foreach ($l_lang as $item) {
                            if ($item->id == $g_config_all->website_lang) {
                                ?>
                                <a href="#lang_p_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                <?php
                            }
                        }
                        ?>

                    </li>
                    <?php if (count($l_lang) > 1) { ?>
                        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                            <ul class="dropdown-menu text-left">
                                <?php
                                foreach ($l_lang as $item_child) {
                                    if ($item_child->id != $g_config_all->website_lang) {
                                        ?>
                                        <li> <a href="#lang_p_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </li>   
                    <?php } ?>
                </ul>
                <div class="panel-body">
                    <div class="tab-content">
                        <?php
                        foreach ($l_lang as $item_content) {
                            ?>
                            <div class="tab-pane fade in <?php
                            if ($item_content->id == $g_config_all->website_lang) {
                                echo ' active';
                            }
                            ?> " id="lang_p_{{$item_content->id}}">
                                <h3>{{$item_content->name}}</h3>
								
								
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="website_menu_{{$item_content->id}}">Menu</label>
                                    <div class="col-lg-4">
                                        <?php
                                        $arrmenu = array();
                                        foreach ($menu_list as $item) {
                                            $arrmenu = $arrmenu + array($item->id => $item->name);
                                        }
                                        $check = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'website_menu_' . $item_content->id) {
                                                $check = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::select('website_menu_'.$item_content->id, $arrmenu,$check,['class'=>'bg-focus form-control'])}}

                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label" for="website_slider_{{$item_content->id}}">Slider</label>
                                    <div class="col-lg-4">
                                        <?php
                                        $arrmenu = array();
                                        foreach ($slide as $item) {
                                            $arrmenu = $arrmenu + array($item->id => $item->name);
                                        }
                                        $check = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'website_slider_' . $item_content->id) {
                                                $check = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::select('website_slider_'.$item_content->id, $arrmenu,$check,['class'=>'bg-focus form-control'])}}

                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="website_cate_show_{{$item_content->id}}">Hòa nhạc đặt vé</label>
                                    <div class="col-lg-4">
                                        <?php
                                        $arrmenu = array();
                                        foreach ($show_category as $item) {
                                            $arrmenu = $arrmenu + array($item->id => $item->name);
                                        }
                                        $check = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'website_cate_show_' . $item_content->id) {
                                                $check = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::select('website_cate_show_'.$item_content->id, $arrmenu,$check,['class'=>'bg-focus form-control'])}}

                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="website_cate_show1_{{$item_content->id}}">Hòa nhạc đặc biệt</label>
                                    <div class="col-lg-4">
                                        <?php
                                        $arrmenu = array();
                                        foreach ($show_category as $item) {
                                            $arrmenu = $arrmenu + array($item->id => $item->name);
                                        }
                                        $check = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'website_cate_show1_' . $item_content->id) {
                                                $check = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::select('website_cate_show1_'.$item_content->id, $arrmenu,$check,['class'=>'bg-focus form-control'])}}

                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="website_cate_show2_{{$item_content->id}}">Hòa nhạc lưu diễn</label>
                                    <div class="col-lg-4">
                                        <?php
                                        $arrmenu = array();
                                        foreach ($show_category as $item) {
                                            $arrmenu = $arrmenu + array($item->id => $item->name);
                                        }
                                        $check = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'website_cate_show2_' . $item_content->id) {
                                                $check = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::select('website_cate_show2_'.$item_content->id, $arrmenu,$check,['class'=>'bg-focus form-control'])}}

                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="website_cate_show3_{{$item_content->id}}">Chùm tác phẩm</label>
                                    <div class="col-lg-4">
                                        <?php
                                        $arrmenu = array();
                                        foreach ($show_category as $item) {
                                            $arrmenu = $arrmenu + array($item->id => $item->name);
                                        }
                                        $check = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'website_cate_show3_' . $item_content->id) {
                                                $check = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::select('website_cate_show3_'.$item_content->id, $arrmenu,$check,['class'=>'bg-focus form-control'])}}

                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Text dưới slide</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_text_index_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_text_index_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_text_index_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Download</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_text_index1_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_text_index1_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_text_index1_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">

									<label class="col-lg-2 control-label">File</label>
									<div class="col-lg-6">
										<?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_text_index2_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
										<div class="input-group">
											{{Form::text('lang_text_index2_'.$item_content->id,$value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_text_index2_'.$item_content->id))}}

											<div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="100" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=lang_text_index2_{{$item_content->id}}"> Upload</button></div>

										</div>
									</div>
								</div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Text ảnh trang chủ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_textimg_home_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::textarea('lang_textimg_home_'.$item_content->id, $value_lang, array('class'=>'form-control tinymce', 'id'=>'lang_textimg_home_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Giới thiệu trang chủ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_about_home_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::textarea('lang_about_home_'.$item_content->id, $value_lang, array('class'=>'form-control tinymce', 'id'=>'lang_about_home_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="lang_home_{{$item_content->id}}">Trang chủ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_home_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_home_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_home_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="lang_website_title_{{$item_content->id}}">Tiêu đề website</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_website_title_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_website_title_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_website_title_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Mô tả website</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_website_description_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::textarea('lang_website_description_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_website_description_'.$item_content->id,'rows'=>'5'))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Keyword</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_website_keyword_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::textarea('lang_website_keyword_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_website_keyword_'.$item_content->id,'rows'=>'5'))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Đ/c công ty</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_addr_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_addr_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_addr_'.$item_content->id,'rows'=>'5'))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Trang 404</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_text404_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::textarea('lang_text404_'.$item_content->id, $value_lang, array('class'=>'form-control tinymce', 'id'=>'lang_text404_'.$item_content->id,'rows'=>'5'))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Đăng ký</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_register_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_register_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_register_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Đăng ký nhận tin</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_subcribe_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_subcribe_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_subcribe_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Doanh nghiệp tài trợ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_sponsor_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_sponsor_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_sponsor_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Đăng ký tài trợ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_sponsor1_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_sponsor1_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_sponsor1_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Upcoming stage</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_upcoming_home_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_upcoming_home_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_upcoming_home_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Upcoming music show</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_upcoming_show_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_upcoming_show_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_upcoming_show_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">View all</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_viewall_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_viewall_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_viewall_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tin tức</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_news_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_news_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_news_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Gửi</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_send_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_send_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_send_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Ngày diễn ra</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_time_show_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_time_show_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_time_show_'.$item_content->id))}}
                                    </div> 
                                </div>
                                 <div class="form-group">
                                    <label class="col-lg-2 control-label">Không có dữ liệu</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_empty_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_empty_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_empty_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Ngẫu nhiên</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_random_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_random_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_random_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Từ khóa</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_keyword_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_keyword_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_keyword_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Chia sẻ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_share_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_share_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_share_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Họ tên</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_name_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_name_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_name_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Trả lời</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_reply_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_reply_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_reply_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nội dung</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_content_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_content_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_content_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Bình luận</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_comment_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_comment_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_comment_'.$item_content->id))}}
                                    </div> 
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Liên hệ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_contact_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_contact_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_contact_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Địa chỉ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_address_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_address_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_address_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">SĐT</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_phone_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_phone_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_phone_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tiêu đề</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_subject_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_subject_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_subject_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nghệ sỹ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_artist_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_artist_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_artist_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Gallery</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_gallery_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_gallery_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_gallery_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Photo</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_photo_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_photo_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_photo_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Chương trình</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_show_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_show_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_show_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Thời gian</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_time_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_time_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_time_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Địa điểm</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_location_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_location_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_location_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nhạc trưởng</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_conductor_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_conductor_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_conductor_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Danh mục</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_category_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_category_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_category_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Xem thêm</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_readmore_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_readmore_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_readmore_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nghệ sỹ khác</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_artist_other_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_artist_other_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_artist_other_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Phút</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_minutes_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_minutes_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_minutes_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Giây</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_seconds_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_seconds_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_seconds_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Từ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_from_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_from_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_from_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Đến</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_to_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_to_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_to_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Giá vé</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_ticketprice_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_ticketprice_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_ticketprice_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Mua vé</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_buyticket_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_buyticket_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_buyticket_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Quay về</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_prev_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_prev_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_prev_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tiếp tục</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_next_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_next_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_next_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Sân khấu</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_stage_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_stage_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_stage_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tầng 1</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_floor1_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_floor1_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_floor1_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tầng 2</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_floor2_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_floor2_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_floor2_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tầng 3</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_floor3_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_floor3_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_floor3_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Người/ghế</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_headerbook_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_headerbook_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_headerbook_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Số ghế</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_seatnumber_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_seatnumber_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_seatnumber_'.$item_content->id))}}
                                    </div> 
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tổng</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_total_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_total_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_total_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Thanh toán</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_checkout_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_checkout_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_checkout_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nhập thông tin</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_information_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_information_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_information_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Phương thức thanh toán</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_payment_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_payment_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_payment_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Điều khoản sử dụng</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_rules_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_rules_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_rules_'.$item_content->id))}}
                                    </div> 
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Đồng ý điều khoản</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_rules_invalid_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_rules_invalid_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_rules_invalid_'.$item_content->id))}}
                                    </div> 
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Thông báo</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_confirm_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_confirm_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_confirm_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Mã đơn vé</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_order_code_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_order_code_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_order_code_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Gợi ý</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_order_hint_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_order_hint_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_order_hint_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Đăng ký tài trợ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_regsponsor_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_regsponsor_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_regsponsor_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Show không bán vé</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_noticket_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_noticket_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_noticket_'.$item_content->id))}}
                                    </div> 
                                </div>								<div class="form-group">                                    <label class="col-lg-2 control-label">Show chưa bán vé</label>                                    <div class="col-lg-8">                                        <?php                                        $value_lang = '';                                        foreach ($data_setting as $key => $value) {                                            if ($key == 'lang_noticket1_' . $item_content->id) {                                                $value_lang = $value;                                            }                                        }                                        ?>                                        {{Form::text('lang_noticket1_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_noticket1_'.$item_content->id))}}                                    </div>                                 </div>
                            </div>
                            <?php
                        }
                        ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
</div>
<style>
    .ui-state-focus{
        background: #E3E8ED !important;
        border: none  !important;
        margin: 0  !important;
    }
    .ui-menu-item{
        height: auto !important;

    }
    .ui-menu-item img{
        width: 60px  !important;
        height: 60px  !important;
        float: left;
    }
    .ui-menu-item span:last-child{
        font-size: 10px;
        color: #C0c0c0;
        margin-top: 45px;
    }
    .ui-menu-item span{
        margin-left: 5px !important;
        float: left !important;
        position: absolute;
    }
</style>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#save_setting').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection
