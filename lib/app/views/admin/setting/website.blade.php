@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("breadcrumb")
<li>
    <a href="{{Asset('')}}">Trang chủ</a>
    <i class="fa fa-circle"></i>
</li>
<li>Cấu hình <i class="fa fa-circle"></i></li>
<li>
    Cấu hình
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
@include('admin.template.select_file_modal')
@include('admin.template.alert_ajax')
{{Form::open( array('action'=>'\ADMIN\SettingController@postInfo', 'class'=>'form-horizontal','id'=>'save_setting' ))}}
<?php
if ($data) {
    $data_setting = $data;
}
?>

<div class="row">
    <div class="col-md-12"> 
       
        <div class="form-group">
            <label class="col-lg-2 control-label">Link video</label>
            <div class="col-lg-6">
                {{Form::text('website_video',isset($data_setting->website_video) ? $data_setting->website_video: '', ['class'=>'bg-focus form-control','col'=>'20'])}}
            </div> 
        </div>
		<div class="form-group">
            <label class="col-lg-2 control-label">Google map</label>
            <div class="col-lg-6">
                {{Form::textarea('website_map',isset($data_setting->website_map) ? $data_setting->website_map: '', ['class'=>'bg-focus form-control','col'=>'20','rows'=>'2'])}}
            </div> 
        </div>
		
		<div class="form-group">
            <label class="col-lg-2 control-label no-padding-top">Đổi ngôn ngữ</label>
            <div class="col-lg-9">
                <label class="mt-radio">
					<input @if(isset($data_setting->website_btnlang) && $data_setting->website_btnlang==1) checked="checked" @endif name="website_btnlang" type="radio" value="1">
					Bật
					<span></span>
				</label>
				<label class="mt-radio">
					<input @if(isset($data_setting->website_btnlang) && $data_setting->website_btnlang==2) checked="checked" @endif name="website_btnlang" type="radio" value="2">
					Tắt
					<span></span>
				</label>
            </div> 
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="website_lang">Ngôn ngữ mặc định</label>
            <div class="col-lg-2">
                <?php
                $arrmu = array();
                foreach ($l_lang as $item) {
                    $arrmu = $arrmu + array($item->id => $item->name);
                }
                ?>
                {{Form::select('website_lang', $arrmu,isset($data_setting->website_lang) ? $data_setting->website_lang: '',['class'=>'bg-focus form-control'])}}
            </div> 
        </div>		<div class="form-group">            <label class="col-lg-2 control-label">                <a href="javascript:;;" data-img-div="div_image_selected10" data-img-id="image_hidden10" data-img-check="100" data-modal-id="Selectfile" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=imageselect10" class="btn btn-white select_image"><i class="fa fa-plus text"></i> Slider ảnh giàn nhạc</a> (1921px x 527px )            </label>            <div class="col-lg-9">                <div id="div_image_selected10">                    @if(isset($data_setting->image_hidden10))                        @foreach(explode(',',$data_setting->image_hidden10) as $i_img)                        <div class="thumbnail  pull-left m-l-small item">                            <a href="{{$i_img}}" target="_blank">                                <img src="{{$i_img}}"></a><span data-img-id="image_hidden10"> <i class="fa fa-times"></i> </span>                        </div>                        @endforeach                    @endif                </div>                @if(isset($data_setting->image_hidden10))                {{Form::hidden('image_hidden10',$data_setting->image_hidden10, array('id'=>'image_hidden10'))}}                {{Form::hidden('imageselect10',$data_setting->image_hidden10, array('id'=>'imageselect10'))}}                @else                    {{Form::hidden('image_hidden10','', array('id'=>'image_hidden10'))}}                    {{Form::hidden('imageselect10','', array('id'=>'imageselect10'))}}                @endif            </div>        </div>
		<div class="form-group">
            <label class="col-lg-2 control-label">
                <a href="javascript:;;" data-img-div="div_image_selected1" data-img-id="image_hidden1" data-img-check="100" data-modal-id="Selectfile" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=imageselect1" class="btn btn-white select_image"><i class="fa fa-plus text"></i> Thẻ thành viên</a>
            </label>
            <div class="col-lg-9">
                <div id="div_image_selected1">
                    @if(isset($data_setting->image_hidden1))
                        @foreach(explode(',',$data_setting->image_hidden1) as $i_img)
                        <div class="thumbnail  pull-left m-l-small item">
                            <a href="{{$i_img}}" target="_blank">
                                <img src="{{$i_img}}"></a><span data-img-id="image_hidden1"> <i class="fa fa-times"></i> </span>
                        </div>
                        @endforeach
                    @endif
                </div>
                @if(isset($data_setting->image_hidden1))
                {{Form::hidden('image_hidden1',$data_setting->image_hidden1, array('id'=>'image_hidden1'))}}
                {{Form::hidden('imageselect1',$data_setting->image_hidden1, array('id'=>'imageselect1'))}}
                @else
                    {{Form::hidden('image_hidden1','', array('id'=>'image_hidden1'))}}
                    {{Form::hidden('imageselect1','', array('id'=>'imageselect1'))}}
                @endif
            </div>
        </div>
		<div class="form-group">
            <label class="col-lg-2 control-label">
                <a href="javascript:;;" data-img-div="div_image_selected08" data-img-title="image_title08" data-img-desc="image_desc08" data-img-id="image_hidden08" data-img-check="10" data-modal-id="Selectfile" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=imageselect08" class="btn btn-white select_image1"><i class="fa fa-plus text"></i> Cá nhân</a>
            </label>
            <div class="col-lg-9">
                <div id="div_image_selected08">
                    @if(isset($data_setting->image_hidden08))
					<?php $i8=0; 
						$d_title = explode(',',$data_setting->image_title08);
						$d_desc = explode(',',$data_setting->image_desc08);
					?>
                        @foreach(explode(',',$data_setting->image_hidden08) as $i_img)
                        <div class="thumbnail  pull-left m-l-small item">
							<input data-default="{{$d_title[$i8]}}" data-type="title" data-title="image_title08" data-id="div_image_selected08" name="title" type="text" placeholder="Tiêu đề..." value="{{$d_title[$i8]}}" class="form-control title_input" style="margin-bottom:5px;"/>
                            <a href="{{$i_img}}" target="_blank">
                                <img src="{{$i_img}}"></a>
							<input data-default="{{$d_desc[$i8]}}" data-type="desc" data-title="image_desc08" data-id="div_image_selected08" name="desc" type="text" placeholder="Nội dung..." value="{{$d_desc[$i8]}}" class="form-control desc_input" style="margin-top:5px;"/>
							<span data-img-id="image_hidden08" data-img-title="image_title08" data-img-desc="image_desc08"> <i class="fa fa-times"></i> </span>
                        </div>
						<?php $i8++; ?>
                        @endforeach
                    @endif
                </div>
                @if(isset($data_setting->image_hidden08))
				{{Form::hidden('image_title08',$data_setting->image_title08, array('id'=>'image_title08'))}}
				{{Form::hidden('image_desc08',$data_setting->image_desc08, array('id'=>'image_desc08'))}}
                {{Form::hidden('image_hidden08',$data_setting->image_hidden08, array('id'=>'image_hidden08'))}}
                {{Form::hidden('imageselect08',$data_setting->image_hidden08, array('id'=>'imageselect08'))}}
                @else
					{{Form::hidden('image_title08','', array('id'=>'image_title08'))}}
					{{Form::hidden('image_desc08','', array('id'=>'image_desc08'))}}
                    {{Form::hidden('image_hidden08','', array('id'=>'image_hidden08'))}}
                    {{Form::hidden('imageselect08','', array('id'=>'imageselect08'))}}
                @endif
            </div>
        </div>
		<div class="form-group">
            <label class="col-lg-2 control-label">
                <a href="javascript:;;" data-img-div="div_image_selected09" data-img-title="image_title09" data-img-desc="image_desc09" data-img-id="image_hidden09" data-img-check="10" data-modal-id="Selectfile" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=imageselect09" class="btn btn-white select_image1"><i class="fa fa-plus text"></i> Hội viên</a>
            </label>
            <div class="col-lg-9">
                <div id="div_image_selected09">
                    @if(isset($data_setting->image_hidden09))
					<?php $i9=0; 
						$d_title = explode(',',$data_setting->image_title09);
						$d_desc = explode(',',$data_setting->image_desc09);
					?>
                        @foreach(explode(',',$data_setting->image_hidden09) as $i_img)
                        <div class="thumbnail  pull-left m-l-small item">
							<input data-default="{{$d_title[$i9]}}" data-type="title" data-title="image_title09" data-id="div_image_selected09" name="title" type="text" placeholder="Tiêu đề..." value="{{$d_title[$i9]}}" class="form-control title_input" style="margin-bottom:5px;"/>
                            <a href="{{$i_img}}" target="_blank">
                                <img src="{{$i_img}}"></a>
							<input data-default="{{$d_desc[$i9]}}" data-type="desc" data-title="image_desc09" data-id="div_image_selected09" name="desc" type="text" placeholder="Nội dung..." value="{{$d_desc[$i9]}}" class="form-control desc_input" style="margin-top:5px;"/>
							<span data-img-id="image_hidden09" data-img-title="image_title09" data-img-desc="image_desc09"> <i class="fa fa-times"></i> </span>
                        </div>
						<?php $i9++; ?>
                        @endforeach
                    @endif
                </div>
                @if(isset($data_setting->image_hidden09))
				{{Form::hidden('image_title09',$data_setting->image_title09, array('id'=>'image_title09'))}}
				{{Form::hidden('image_desc09',$data_setting->image_desc09, array('id'=>'image_desc09'))}}
                {{Form::hidden('image_hidden09',$data_setting->image_hidden09, array('id'=>'image_hidden09'))}}
                {{Form::hidden('imageselect09',$data_setting->image_hidden09, array('id'=>'imageselect09'))}}
                @else
					{{Form::hidden('image_title09','', array('id'=>'image_title09'))}}
					{{Form::hidden('image_desc09','', array('id'=>'image_desc09'))}}
                    {{Form::hidden('image_hidden09','', array('id'=>'image_hidden09'))}}
                    {{Form::hidden('imageselect09','', array('id'=>'imageselect09'))}}
                @endif
            </div>
        </div>
		<div class="form-group">
            <label class="col-lg-2 control-label">Ảnh 404</label>
            <div class="col-lg-6">
                <div class="input-group">
                    {{Form::text('website_image_404',isset($data_setting->website_image_404) ? $data_setting->website_image_404: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_404'))}}
                    <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_404"> Thêm ảnh </button></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Ảnh trang chủ (1920x834)</label>
            <div class="col-lg-6">
                <div class="input-group">
                    {{Form::text('website_image_adv7',isset($data_setting->website_image_adv7) ? $data_setting->website_image_adv7: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv7'))}}
                    <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv7"> Thêm ảnh </button></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Ảnh nền đăng ký tài trợ cá nhân (1920x834)</label>
            <div class="col-lg-6">
                <div class="input-group">
                    {{Form::text('website_image_adv1',isset($data_setting->website_image_adv1) ? $data_setting->website_image_adv1: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv1'))}}
                    <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv1"> Thêm ảnh </button></div>
                </div>
            </div>
        </div>
		<div class="form-group">
            <label class="col-lg-2 control-label">Ảnh nền upcoming stage(1920x834)</label>
            <div class="col-lg-6">
                <div class="input-group">
                    {{Form::text('website_image_adv2',isset($data_setting->website_image_adv2) ? $data_setting->website_image_adv2: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv2'))}}
                    <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv2"> Thêm ảnh </button></div>
                </div>
            </div>
        </div>
		<div class="form-group">
            <label class="col-lg-2 control-label">Ảnh trang orchestra(1140x352)</label>
            <div class="col-lg-6">
                <div class="input-group">
                    {{Form::text('website_image_adv3',isset($data_setting->website_image_adv3) ? $data_setting->website_image_adv3: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv3'))}}
                    <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv3"> Thêm ảnh </button></div>
                </div>
            </div>
        </div>
       
        <div class="form-group">
            <label class="col-lg-2 control-label">Ảnh quảng cáo sidebar(526x414)</label>
            <div class="col-lg-6">
                <div class="input-group">
                    {{Form::text('website_image_adv5',isset($data_setting->website_image_adv5) ? $data_setting->website_image_adv5: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv5'))}}
                    <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv5"> Thêm ảnh </button></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Ảnh quảng cáo trang chủ(526x356)</label>
            <div class="col-lg-6">
                <div class="input-group">
                    {{Form::text('website_image_adv6',isset($data_setting->website_image_adv6) ? $data_setting->website_image_adv6: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv6'))}}
                    <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv6"> Thêm ảnh </button></div>
                </div>
            </div>
        </div>
		<div class="form-group">
            <label class="col-lg-2 control-label">Ảnh trang đăng ký tài trợ</label>
            <div class="col-lg-6">
                <div class="input-group">
                    {{Form::text('website_image_adv9',isset($data_setting->website_image_adv9) ? $data_setting->website_image_adv9: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv9'))}}
                    <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv9"> Thêm ảnh </button></div>
                </div>
            </div>
        </div>
		<div class="form-group">
            <label class="col-lg-2 control-label">Ảnh trang đăng ký tài trợ</label>
            <div class="col-lg-6">
                <div class="input-group">
                    {{Form::text('website_image_adv8',isset($data_setting->website_image_adv8) ? $data_setting->website_image_adv8: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv8'))}}
                    <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv8"> Thêm ảnh </button></div>
                </div>
            </div>
        </div>
		<div class="form-group">
            <label class="col-lg-2 control-label">Ảnh buổi diễn tiếp theo</label>
            <div class="col-lg-6">
                <div class="input-group">
                    {{Form::text('website_image_adv17',isset($data_setting->website_image_adv17) ? $data_setting->website_image_adv17: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv17'))}}
                    <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv17"> Thêm ảnh </button></div>
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#save_setting').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection
