@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.select_file_modal')
@include('admin.template.alert_ajax')
@endsection
@section("breadcrumb")
<li>
    <a href="{{Asset('')}}">Trang chủ</a>
    <i class="fa fa-circle"></i>
</li>
<li>Cấu hình <i class="fa fa-circle"></i></li>
<li>
    Cấu hình
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
<style>
    .ui-autocomplete {
        max-height: 200px;
        overflow-y: auto;
        overflow-x: hidden;
    }
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">

{{Form::open( array('action'=>'\ADMIN\SettingController@postInfo', 'class'=>'form-horizontal','id'=>'save_setting' ))}}
<?php
if ($data) {
    $data_setting = $data;
}
?>

<div class="row">
    <div class="col-md-12"> 
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <ul class="nav nav-tabs">
                    <li class="active">

                        <?php
                        foreach ($l_lang as $item) {
                            if ($item->id == $g_config_all->website_lang) {
                                ?>
                                <a href="#lang_p_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                <?php
                            }
                        }
                        ?>

                    </li>
                    <?php if (count($l_lang) > 1) { ?>
                        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                            <ul class="dropdown-menu text-left">
                                <?php
                                foreach ($l_lang as $item_child) {
                                    if ($item_child->id != $g_config_all->website_lang) {
                                        ?>
                                        <li> <a href="#lang_p_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </li>   
                    <?php } ?>
                </ul>
                <div class="panel-body">
                    <div class="tab-content">
                        <?php
                        foreach ($l_lang as $item_content) {
                            ?>
                            <div class="tab-pane fade in <?php
                            if ($item_content->id == $g_config_all->website_lang) {
                                echo ' active';
                            }
                            ?> " id="lang_p_{{$item_content->id}}">
                                <h3>{{$item_content->name}}</h3> 
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Tiêu đề</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_title_sponsor_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_title_sponsor_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_title_sponsor_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Ngày sinh</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_dob_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_dob_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_dob_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Ghi chú</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_note_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_note_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_note_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Giới tính</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_sex_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_sex_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_sex_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Quốc tịch</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_city_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_city_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_city_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Chức vụ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_job_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_job_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_job_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Fax</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_fax_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_fax_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_fax_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Tiêu đề tài trợ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_title_sponsor1_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_title_sponsor1_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_title_sponsor1_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Tài trợ kim cương</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_diamond_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_diamond_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_diamond_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Tài trợ vàng</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_gold_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_gold_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_gold_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Tài trợ bạc</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_silver_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_silver_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_silver_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Thành viên vàng</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_gold1_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_gold1_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_gold1_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Thành viên bạc</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_silver1_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_silver1_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_silver1_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Địa chỉ nhận thông tin</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_add_receive_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::textarea('lang_add_receive_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control tinymce', 'id'=>'lang_add_receive_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Thông tin liên hệ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_contact_info_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::textarea('lang_contact_info_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control tinymce', 'id'=>'lang_contact_info_'.$item_content->id))}}
                                    </div> 
                                </div>
                            </div>
                            <?php
                        }
                        ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
</div>
<style>
    .ui-state-focus{
        background: #E3E8ED !important;
        border: none  !important;
        margin: 0  !important;
    }
    .ui-menu-item{
        height: auto !important;

    }
    .ui-menu-item img{
        width: 60px  !important;
        height: 60px  !important;
        float: left;
    }
    .ui-menu-item span:last-child{
        font-size: 10px;
        color: #C0c0c0;
        margin-top: 45px;
    }
    .ui-menu-item span{
        margin-left: 5px !important;
        float: left !important;
        position: absolute;
    }
</style>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#save_setting').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection
