@if(isset($data) && count($data)>0)

<table class="table-responsive cus_table">
    <tr>
        <th class="auto_name">Tiêu đề</th>
    </tr>
    @foreach($data as $item)
	<?php $name = str_replace('"', '', $item->news_title);?>
    <tr id="{{$item->news_id}}" onclick="bindingdata('{{$item->news_id}}','{{$name}}','body_list_autokeyword_{{$item->lang_id}}','autokeyword_{{$item->lang_id}}','list_id_autokeyword_{{$item->lang_id}}','div_autokeyword_{{$item->lang_id}}')">        
        <td class="auto_name">{{$name}}
        </td>    
    </tr>
    @endforeach

</table>

@else

<table class="table-responsive cus_table">
    <tr>
        <th class="auto_name">Tiêu đề</th>
    </tr>
    <tr>
        <td>{{Lang::get('general.empty')}}</td>
    </tr>
</table>
@endif



