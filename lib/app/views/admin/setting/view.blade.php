@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Cấu hình <i class="fa fa-circle"></i></li>
<li>
	Cấu hình
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
@include('admin.template.select_file_modal')
@include('admin.template.alert_ajax')
{{Form::open( array('action'=>'\ADMIN\SettingController@postSaveSetting', 'class'=>'form-horizontal','id'=>'save_setting' ))}}
<?php
if ($data->data) {
    $data_setting = json_decode($data->data);
}
?>

<div class="row">
    <div class="col-md-12">  
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tabs-2" data-toggle="tab"><i class="fa fa-thumb-tack fa-lg text-default"></i> Thông tin liên hệ</a>
            </li>
            <li ><a href="#tabs-1" data-toggle="tab"><i class="fa fa-cogs fa-lg text-default"></i> Cấu hình website</a>
            </li>
            <li ><a href="#tabs-7" data-toggle="tab"><i class="fa fa-shopping-cart fa-lg text-default"></i> Thiết lập cửa hàng</a>
            </li>
            <li><a href="#tabs-3" data-toggle="tab"><i class="fa fa-th fa-lg text-default"></i> Cấu hình giao diện</a>
            </li>
            <li><a href="#tabs-4" data-toggle="tab"><i class="fa fa-facebook fa-lg text-default"></i> Mạng xã hội</a>
            </li>
            <li><a href="#tabs-6" data-toggle="tab"><i class="fa fa-envelope-o fa-lg text-default"></i> Mẫu Email</a>
            </li>
            <li><a href="#tabs-5" data-toggle="tab"><i class="fa fa-bar-chart-o fa-lg text-default"></i> Google Analytics</a>
            </li>
        </ul> 
        <div class="tab-content">
            <div class="tab-pane fade" id="tabs-7">
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="website_image_default">Logo</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('website_logo_default',isset($data_setting->website_logo_default) ? $data_setting->website_logo_default: '', array('class'=>'bg-focus form-control', 'id'=>'website_logo_default'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_logo_default"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="website_image_default1">Logo footer</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('website_image_default1',isset($data_setting->website_image_default1) ? $data_setting->website_image_default1: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_default1'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_default1"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="product_store_null">Cho phép bán hàng khi hết</label>
                    <div class="col-lg-6">
                        <div class="checkbox">
                            <label class="checkbox-custom">
                                {{Form::checkbox('product_store_null', 1,isset($data_setting->product_store_null) ? $data_setting->product_store_null: '')}}<i class="fa fa-check-square-o"></i> Bật/tắt </label>
                        </div>
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="currency_default">Tiền mặc định</label> 
                    <div class="col-lg-6">
                        {{Form::select('currency_default', $currencyarray, isset($data_setting->currency_default) ? $data_setting->currency_default: '', [ 'id'=>'currency_default','class' => 'input-sm bg-focus'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="product_store_inventory">Thời gian tối đa có thể sửa đơn hàng</label> 
                    <div class="col-lg-4">
                        {{Form::text('dateline_order', isset($data_setting->dateline_order) ? $data_setting->dateline_order: '', ['id'=>'product_store_inventory','class'=>'bg-focus form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="product_check_code">Tiền tố mã kiểm kho</label> 
                    <div class="col-lg-4">
                        {{Form::text('product_check_code', isset($data_setting->product_check_code) ? $data_setting->product_check_code: '', ['id'=>'product_check_code','class'=>'bg-focus form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="product_import_code">Tiền tố mã nhập kho</label> 
                    <div class="col-lg-4">
                        {{Form::text('product_import_code', isset($data_setting->product_import_code) ? $data_setting->product_import_code: '', ['id'=>'product_import_code','class'=>'bg-focus form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="product_order_code">Đơn hàng</label> 
                    <div class="col-lg-4">
                        {{Form::text('product_order_code', isset($data_setting->product_order_code) ? $data_setting->product_order_code: '', ['id'=>'product_order_code','class'=>'bg-focus form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="product_imcome_code">Tiền tố phiếu thu</label> 
                    <div class="col-lg-4">
                        {{Form::text('product_imcome_code', isset($data_setting->product_imcome_code) ? $data_setting->product_imcome_code: '', ['id'=>'product_imcome_code','class'=>'bg-focus form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="product_expenditure_code">Tiền tố phiếu chi</label> 
                    <div class="col-lg-4">
                        {{Form::text('product_expenditure_code', isset($data_setting->product_expenditure_code) ? $data_setting->product_expenditure_code: '', ['id'=>'product_expenditure_code','class'=>'bg-focus form-control'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="imcome_default_order">Nhóm thu bán hàng mặc định</label> 
                    <div class="col-lg-6">
                        {{Form::select('imcome_default_order', $incomearray, isset($data_setting->imcome_default_order) ? $data_setting->imcome_default_order: '', [ 'id'=>'imcome_default_order','class' => 'input-sm bg-focus'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="invoice_note_order">Ghi chú cho hóa đơn</label> 
                    <div class="col-lg-4">
                        {{Form::text('invoice_note_order', isset($data_setting->invoice_note_order) ? $data_setting->invoice_note_order: '', ['id'=>'invoice_note_order','class'=>'bg-focus form-control'])}}
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tabs-6">
                <div class="row">
    				<div class="form-group">
                        <label class="col-lg-2 control-label" for="mail_host">SMTP</label>
                        <div class="col-lg-6">
                            {{Form::text('mail_host', isset($data_setting->mail_host) ? $data_setting->mail_host: '', ['id'=>'mail_host','class'=>'bg-focus form-control'])}}                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="mail_port">Port</label>
                        <div class="col-lg-6">
                            {{Form::text('mail_port', isset($data_setting->mail_port) ? $data_setting->mail_port: '', ['id'=>'mail_port','class'=>'bg-focus form-control'])}}                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="from_name">From name</label>
                        <div class="col-lg-6">
                            {{Form::text('from_name', isset($data_setting->from_name) ? $data_setting->from_name: '', ['id'=>'from_name','class'=>'bg-focus form-control'])}}                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="from_mail">From mail</label>
                        <div class="col-lg-6">
                            {{Form::text('from_mail', isset($data_setting->from_mail) ? $data_setting->from_mail: '', ['id'=>'from_mail','class'=>'bg-focus form-control'])}}                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="mail_username">Username</label>
                        <div class="col-lg-6">
                            {{Form::text('mail_username', isset($data_setting->mail_username) ? $data_setting->mail_username: '', ['id'=>'mail_username','class'=>'bg-focus form-control'])}}                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="mail_password">Password</label>
                        <div class="col-lg-6">
                            {{Form::text('mail_password', isset($data_setting->mail_password) ? $data_setting->mail_password: '', ['id'=>'mail_password','class'=>'bg-focus form-control'])}}                            
                        </div>
                    </div>
                </div>
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-envelope-o"></i>Mẫu email feedback</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="col-lg-9">
                                    {{Form::textarea('website_template_email', isset($data_setting->website_template_email) ? $data_setting->website_template_email: '', ['class'=>'bg-focus form-control tinymce_html'])}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="tab-pane fade" id="tabs-1">
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="website_title">Tiêu đề</label>
                    <div class="col-lg-6">
                        {{Form::hidden('id', $data->id)}}

                        {{Form::text('website_title', isset($data_setting->website_title) ? $data_setting->website_title: '', ['id'=>'website_title','class'=>'bg-focus form-control'])}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="website_keyword">Từ khóa</label>
                    <div class="col-lg-6">
                        {{Form::textarea('website_keyword', isset($data_setting->website_keyword) ? $data_setting->website_keyword: '', ['class'=>'bg-focus form-control','col'=>'20','rows'=>'2'])}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="website_description">Mô tả</label>
                    <div class="col-lg-6">
                        {{Form::textarea('website_description',isset($data_setting->website_description) ? $data_setting->website_description: '', ['class'=>'bg-focus form-control','col'=>'20','rows'=>'2'])}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="website_lang">Ngôn ngữ mặc định</label>
                    <div class="col-lg-2">
                        <?php
                        $arrmu = array();
                        foreach ($l_lang as $item) {
                            $arrmu = $arrmu + array($item->id => $item->name);
                        }
                        ?>
                        {{Form::select('website_lang', $arrmu,isset($data_setting->website_lang) ? $data_setting->website_lang: '',['class'=>'bg-focus form-control'])}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="website_image_default">Facebook Image</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('website_image_default',isset($data_setting->website_image_default) ? $data_setting->website_image_default: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_default'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_default"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-lg-2 control-label">Ảnh quảng cáo 1 (canh slide 263x113)</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('website_image_adv1',isset($data_setting->website_image_adv1) ? $data_setting->website_image_adv1: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv1'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv1"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-lg-2 control-label">Ảnh quảng cáo 2 (canh slide 263x113)</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('website_image_adv2',isset($data_setting->website_image_adv2) ? $data_setting->website_image_adv2: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv2'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv2"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-lg-2 control-label">Ảnh quảng cáo 3 (canh slide 263x113)</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('website_image_adv3',isset($data_setting->website_image_adv3) ? $data_setting->website_image_adv3: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv3'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv3"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-lg-2 control-label">Ảnh quảng cáo 4(canh slide 263x113)</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('website_image_adv4',isset($data_setting->website_image_adv4) ? $data_setting->website_image_adv4: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv4'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv4"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-lg-2 control-label">Ảnh quảng cáo 5 (bottom 341x211)</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('website_image_adv5',isset($data_setting->website_image_adv5) ? $data_setting->website_image_adv5: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv5'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv5"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-lg-2 control-label">Ảnh quảng cáo 6 (bottom 341x211)</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('website_image_adv6',isset($data_setting->website_image_adv6) ? $data_setting->website_image_adv6: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv6'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv6"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-lg-2 control-label">Ảnh quảng cáo 7 (bottom 419x441)</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('website_image_adv7',isset($data_setting->website_image_adv7) ? $data_setting->website_image_adv7: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv7'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv7"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-lg-2 control-label">Ảnh quảng cáo 8 (bottom 341x442)</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('website_image_adv8',isset($data_setting->website_image_adv8) ? $data_setting->website_image_adv8: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv8'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv8"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-lg-2 control-label">Ảnh quảng cáo 9 (middle 1140x107)</label>
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{Form::text('website_image_adv9',isset($data_setting->website_image_adv9) ? $data_setting->website_image_adv9: '', array('class'=>'bg-focus form-control', 'id'=>'website_image_adv9'))}}
                            <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=website_image_adv9"> Thêm ảnh </button></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade  active in" id="tabs-2">
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="contact_company">Tên công ty</label>
                    <div class="col-lg-6">
                        {{Form::text('contact_company', isset($data_setting->contact_company) ? $data_setting->contact_company: '', ['id'=>'contact_company','class'=>'bg-focus form-control'])}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="contact_address">Địa chỉ</label>
                    <div class="col-lg-6">
                        {{Form::text('contact_address',isset($data_setting->contact_address) ? $data_setting->contact_address: '', ['id'=>'contact_address','class'=>'bg-focus form-control'])}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="contact_phone">Số điện thoại</label>
                    <div class="col-lg-6">
                        {{Form::text('contact_phone', isset($data_setting->contact_phone) ? $data_setting->contact_phone: '', ['id'=>'contact_phone','class'=>'bg-focus form-control'])}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="contact_fax">FAX</label>
                    <div class="col-lg-6">
                        {{Form::text('contact_fax', isset($data_setting->contact_fax) ? $data_setting->contact_fax: '', ['id'=>'contact_fax','class'=>'bg-focus form-control'])}}
                    </div> 
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="contact_email">Email</label>
                    <div class="col-lg-6">
                        {{Form::text('contact_email', isset($data_setting->contact_email) ? $data_setting->contact_email: '', ['id'=>'contact_email','class'=>'bg-focus form-control'])}}
                    </div> 
                </div>
            </div>
            <div class="tab-pane fade" id="tabs-3">
                <div class="row">
                    <div class="form-group col-lg-6">
                        <label class="col-lg-6 control-label" for="website_comment">Comment toàn hệ thống</label>
                        <div class="col-lg-4">
                            <div class="checkbox">
                                <label class="checkbox-custom">
                                    {{Form::checkbox('website_comment', 1, isset($data_setting->website_comment) ? $data_setting->website_comment: '')}}<i class="fa fa-check-square-o"></i> Bật/tắt </label>
                            </div>
                        </div> 
                    </div>
    				<div class="form-group">
                        <label class="col-lg-6 control-label" for="website_comment"></label>
                        
                    </div>
                </div>
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active">

                                <?php
                                foreach ($l_lang as $item) {
                                    if ($item->id == $g_config_all->website_lang) {
                                        ?>
                                        <a href="#lang_p_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                        <?php
                                    }
                                }
                                ?>

                            </li>
                            <?php if (count($l_lang) > 1) { ?>
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                                    <ul class="dropdown-menu text-left">
                                        <?php
                                        foreach ($l_lang as $item_child) {
                                            if ($item_child->id != $g_config_all->website_lang) {
                                                ?>
                                                <li> <a href="#lang_p_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </li>   
                        <?php } ?>
                        </ul>
                        <div class="panel-body">
                            <div class="tab-content">
                                <?php
                                foreach ($l_lang as $item_content) {
                                    ?>
                                    <div class="tab-pane fade in <?php
                                    if ($item_content->id == $g_config_all->website_lang) {
                                        echo ' active';
                                    }
                                    ?> " id="lang_p_{{$item_content->id}}">
                                        <h3>{{$item_content->name}}</h3>
                                        
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="website_menu_{{$item_content->id}}">Menu</label>
                                            <div class="col-lg-4">
                                                <?php
                                                $arrmenu = array();
                                                foreach ($menu_list as $item) {
                                                    $arrmenu = $arrmenu + array($item->id => $item->name);
                                                }
                                                $check = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'website_menu_' . $item_content->id) {
                                                        $check = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::select('website_menu_'.$item_content->id, $arrmenu,$check,['class'=>'bg-focus form-control'])}}

                                            </div> 
                                        </div>
                                         
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="website_menu1_{{$item_content->id}}">Menu chân trang</label>
                                            <div class="col-lg-4">
                                                <?php
                                                $arrmenu = array();
                                                foreach ($menu_list as $item) {
                                                    $arrmenu = $arrmenu + array($item->id => $item->name);
                                                }
                                                $check = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'website_menu1_' . $item_content->id) {
                                                        $check = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::select('website_menu1_'.$item_content->id, $arrmenu,$check,['class'=>'bg-focus form-control'])}}

                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="website_menu2_{{$item_content->id}}">Menu chân trang</label>
                                            <div class="col-lg-4">
                                                <?php
                                                $arrmenu = array();
                                                foreach ($menu_list as $item) {
                                                    $arrmenu = $arrmenu + array($item->id => $item->name);
                                                }
                                                $check = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'website_menu2_' . $item_content->id) {
                                                        $check = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::select('website_menu2_'.$item_content->id, $arrmenu,$check,['class'=>'bg-focus form-control'])}}

                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="website_menu3_{{$item_content->id}}">Menu chân trang</label>
                                            <div class="col-lg-4">
                                                <?php
                                                $arrmenu = array();
                                                foreach ($menu_list as $item) {
                                                    $arrmenu = $arrmenu + array($item->id => $item->name);
                                                }
                                                $check = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'website_menu3_' . $item_content->id) {
                                                        $check = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::select('website_menu3_'.$item_content->id, $arrmenu,$check,['class'=>'bg-focus form-control'])}}

                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="website_menu4_{{$item_content->id}}">Menu chân trang</label>
                                            <div class="col-lg-4">
                                                <?php
                                                $arrmenu = array();
                                                foreach ($menu_list as $item) {
                                                    $arrmenu = $arrmenu + array($item->id => $item->name);
                                                }
                                                $check = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'website_menu4_' . $item_content->id) {
                                                        $check = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::select('website_menu4_'.$item_content->id, $arrmenu,$check,['class'=>'bg-focus form-control'])}}

                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="website_slider_{{$item_content->id}}">Slider</label>
                                            <div class="col-lg-4">
                                                <?php
                                                $arrslider = array();
                                                foreach ($slider_list as $item) {
                                                    $arrslider = $arrslider + array($item->id => $item->name);
                                                }
                                                $check_slider = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'website_slider_' . $item_content->id) {
                                                        $check_slider = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::select('website_slider_'.$item_content->id, $arrslider,$check_slider,['class'=>'bg-focus form-control'])}}

                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="lang_home_{{$item_content->id}}">Trang chủ</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_home_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_home_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_home_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="home_category_select_3_{{$item_content->id}}">Trang chủ nhóm 1</label>
                                            <div class="col-lg-2">
                                                <?php
                                                $arrcat = array();
                                                foreach ($all_category as $item) {
                                                    if ($item->parent == 0) {
                                                        $arrcat = $arrcat + array($item->id => $item->name);
                                                        foreach ($all_category as $item1) {
                                                            if ($item1->parent == $item->id) {
                                                                $arrcat = $arrcat + array($item1->id => ' — ' . $item1->name);
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                                {{Form::select('home_category_select_3_'.$item_content->id, $arrcat,isset($data_setting->{'home_category_select_3_'.$item_content->id}) ? $data_setting->{'home_category_select_3_'.$item_content->id}: '',['class'=>'bg-focus form-control'])}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="home_category_select_4_{{$item_content->id}}">Trang chủ nhóm 2</label>
                                            <div class="col-lg-2">
                                                <?php
                                                $arrcat = array();
                                                foreach ($all_category as $item) {
                                                    if ($item->parent == 0) {
                                                        $arrcat = $arrcat + array($item->id => $item->name);
                                                        foreach ($all_category as $item1) {
                                                            if ($item1->parent == $item->id) {
                                                                $arrcat = $arrcat + array($item1->id => ' — ' . $item1->name);
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                                {{Form::select('home_category_select_4_'.$item_content->id, $arrcat,isset($data_setting->{'home_category_select_4_'.$item_content->id}) ? $data_setting->{'home_category_select_4_'.$item_content->id}: '',['class'=>'bg-focus form-control'])}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="home_category_select_5_{{$item_content->id}}">Trang chủ nhóm 3</label>
                                            <div class="col-lg-2">
                                                <?php
                                                $arrcat = array();
                                                foreach ($all_category as $item) {
                                                    if ($item->parent == 0) {
                                                        $arrcat = $arrcat + array($item->id => $item->name);
                                                        foreach ($all_category as $item1) {
                                                            if ($item1->parent == $item->id) {
                                                                $arrcat = $arrcat + array($item1->id => ' — ' . $item1->name);
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                                {{Form::select('home_category_select_5_'.$item_content->id, $arrcat,isset($data_setting->{'home_category_select_5_'.$item_content->id}) ? $data_setting->{'home_category_select_5_'.$item_content->id}: '',['class'=>'bg-focus form-control'])}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="lang_link_adv1_{{$item_content->id}}">Link ảnh 1 (canh slide)</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_link_adv1_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_link_adv1_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_link_adv1_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="lang_link_adv2_{{$item_content->id}}">Link ảnh 2 (canh slide)</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_link_adv2_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_link_adv2_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_link_adv2_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="lang_link_adv3_{{$item_content->id}}">Link ảnh 3 (canh slide)</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_link_adv3_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_link_adv3_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_link_adv3_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="lang_link_adv4_{{$item_content->id}}">Link ảnh 4 (canh slide)</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_link_adv4_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_link_adv4_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_link_adv4_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="lang_link_adv5_{{$item_content->id}}">Link ảnh bottom 1</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_link_adv5_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_link_adv5_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_link_adv5_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="lang_link_adv6_{{$item_content->id}}">Link ảnh bottom 2</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_link_adv6_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_link_adv6_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_link_adv6_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="lang_link_adv7_{{$item_content->id}}">Link ảnh bottom 3</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_link_adv7_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_link_adv7_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_link_adv7_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="lang_link_adv8_{{$item_content->id}}">Link ảnh bottom 4</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_link_adv8_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_link_adv8_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_link_adv8_'.$item_content->id))}}
                                            </div> 
                                        </div>
										<div class="form-group">
                                            <label class="col-lg-2 control-label" for="lang_link_adv9_{{$item_content->id}}">Link ảnh middle</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_link_adv9_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_link_adv9_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_link_adv9_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="lang_website_title_{{$item_content->id}}">Tiêu đề website</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_website_title_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_website_title_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_website_title_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Mô tả website</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_website_description_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::textarea('lang_website_description_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_website_description_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Keyword</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_website_keyword_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::textarea('lang_website_keyword_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_website_keyword_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label" for="lang_website_address_{{$item_content->id}}">Địa chỉ cửa hàng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_website_address_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::textarea('lang_website_address_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_website_address_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Slogan footer</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_sloganfooter_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_sloganfooter_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_sloganfooter_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Đăng nhập</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_login_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_login_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_login_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Đăng xuất</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_logout_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_logout_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_logout_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Đăng ký</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_register_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_register_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_register_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Tên đăng nhập</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_username_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_username_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_username_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Mật khẩu</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_password_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_password_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_password_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Nhập lại mật khẩu</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_repassword_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_repassword_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_repassword_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Họ tên</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_name_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_name_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_name_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Điện thoại</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_phone_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_phone_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_phone_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Di động</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_mobile_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_mobile_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_mobile_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Địa chỉ</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_address_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_address_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_address_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Giỏ hàng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_cart_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_cart_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_cart_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Sản phẩm</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_product_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_product_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_product_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Giá</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_price_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_price_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_price_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Số lượng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_quantity_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_quantity_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_quantity_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Tổng tiền</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_total_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_total_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_total_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Màu sắc</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_color_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_color_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_color_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Thông tin khách hàng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_cusinfo_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_cusinfo_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_cusinfo_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Yêu cầu thêm</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_note_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_note_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_note_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Gửi đơn hàng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_sendorder_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_sendorder_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_sendorder_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Xem thêm</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_readmore_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_readmore_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_readmore_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Còn hàng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_instock_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_instock_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_instock_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Hết hàng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_outstock_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_outstock_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_outstock_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Theo dõi</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_subcribe_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_subcribe_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_subcribe_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Nhận email</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_subcribeemail_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_subcribeemail_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_subcribeemail_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Tin tức</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_news_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_news_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_news_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Danh mục</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_category_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_category_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_category_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Thương hiệu</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_manuf_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_manuf_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_manuf_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Không có dữ liệu</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_empty_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_empty_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_empty_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Liên quan</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_relate_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_relate_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_relate_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Mới nhất</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_newest_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_newest_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_newest_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Bảo hành</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_guarantee_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_guarantee_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_guarantee_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Tình trạng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_stituation_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_stituation_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_stituation_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Mô tả</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_description_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_description_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_description_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Khuyến mại</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_bonus_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_bonus_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_bonus_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Chi tiết sản phẩm</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_pdetail_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_pdetail_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_pdetail_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Sản phẩm đã xem</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_preview_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_preview_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_preview_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Thêm giỏ hàng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_addcart_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_addcart_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_addcart_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Ghi nhớ</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_remember_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_remember_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_remember_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Đăng nhập thất bại</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_login_fail_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_login_fail_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_login_fail_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Tài khoản chưa kích hoạt</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_active_acc_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_active_acc_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_active_acc_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Tài khoản đã bị khóa</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_lock_acc_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_lock_acc_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_lock_acc_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Email không được để trống</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_email_required_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_email_required_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_email_required_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Họ tên không được để trống</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_name_required_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_name_required_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_name_required_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Mật khẩu không được để trống</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_pass_required_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_pass_required_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_pass_required_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Mật khẩu nhập lại không đúng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_repass_equal_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_repass_equal_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_repass_equal_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Đăng ký thành công</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_register_success_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_register_success_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_register_success_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Email không đúng định dạng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_email_format_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_email_format_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_email_format_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Cần đăng nhập</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_login_required_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_login_required_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_login_required_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Thất bại</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_changeprofile_fail_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_changeprofile_fail_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_changeprofile_fail_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Thành công</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_changeprofile_success_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_changeprofile_success_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_changeprofile_success_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Email không tồn tại</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_email_notexist_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_email_notexist_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_email_notexist_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Vào email lấy mật khẩu</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_get_pass_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_get_pass_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_get_pass_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Thêm giỏ hàng thành công</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_cart_success_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_cart_success_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_cart_success_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Giỏ hàng rỗng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_cartempty_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_cartempty_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_cartempty_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Đặt hàng thành công</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_order_success_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_order_success_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_order_success_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Thông tin khách hàng</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_cusinforequired_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_cusinforequired_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_cusinforequired_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Thông báo</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_notify_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_notify_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_notify_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Lọc</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_filter_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_filter_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_filter_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Thêm yêu thích thành công</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_fav_success_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_fav_success_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_fav_success_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Đã có trong yêu thích</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_fav_exist_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_fav_exist_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_fav_exist_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Thông tin cá nhân</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_profile_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_profile_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_profile_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Yêu thích</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_fav_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_fav_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_fav_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Đã thêm so sánh</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_compare_success_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_compare_success_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_compare_success_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Đã có trong so sánh</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_compare_exist_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_compare_exist_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_compare_exist_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">So sánh tối đa 3 sản phẩm</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_compare_max3_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_compare_max3_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_compare_max3_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">So sánh</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_compare_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_compare_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_compare_'.$item_content->id,'rows'=>'5'))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Tập tin quá lớn</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_fileerror_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_fileerror_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_fileerror_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Thư viện ảnh</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_gallery_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_gallery_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_gallery_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Quên mật khẩu</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_forgot_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_forgot_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_forgot_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Đổi mật khẩu</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_changepass_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_changepass_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_changepass_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Gửi</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_send_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_send_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_send_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Giá liên hệ</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_lienhe_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_lienhe_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_lienhe_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Hàng bán chạy</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_bestsell_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_bestsell_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_bestsell_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Sản phẩm ghép nối</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_suggest_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_suggest_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_suggest_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Thông số kỹ thuật </label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_setting_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_setting_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_setting_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Kích thước </label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_size_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_size_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_size_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Nhận hàng tại d/c đăng nhập</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_orderlogin_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_orderlogin_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_orderlogin_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Nhận hàng tại d/c khác</label>
                                            <div class="col-lg-8">
                                                <?php
                                                $value_lang = '';
                                                foreach ($data_setting as $key => $value) {
                                                    if ($key == 'lang_orderother_' . $item_content->id) {
                                                        $value_lang = $value;
                                                    }
                                                }
                                                ?>
                                                {{Form::text('lang_orderother_'.$item_content->id, $value_lang, array('class'=>'form-control', 'id'=>'lang_orderother_'.$item_content->id))}}
                                            </div> 
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tabs-4">
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-filter"></i>Mạng xã hội</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#social-1" data-toggle="tab"><i class="fa fa-facebook fa-lg text-default"></i></a>
                            </li>
                            <li><a href="#social-2" data-toggle="tab"><i class="fa fa-google-plus fa-lg text-default"></i></a>
                            </li>
                            <li><a href="#social-3" data-toggle="tab"><i class="fa fa-twitter fa-lg text-default"></i></a>
                            </li>
                            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gears fa-lg text-default"></i> <b class="caret"></b></a>
                                <ul class="dropdown-menu text-left">
                                    <li><a href="#social-4" data-toggle="tab"><i class="fa fa-linkedin fa-lg text-default"></i></a>
                                    </li>
                                    <li><a href="#social-5" data-toggle="tab"><i class="fa fa-instagram fa-lg text-default"></i></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                        <div class="panel-body">
                            <div class="tab-content">

                                <div class="tab-pane fade active in" id="social-1">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="facebook_fanpage">Facebook Fanpage</label>
                                        <div class="col-lg-6">
                                            {{Form::text('facebook_fanpage',  isset($data_setting->facebook_fanpage) ? $data_setting->facebook_fanpage: '', ['id'=>'facebook_fanpage','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="facebook_id">Facebook ID</label>
                                        <div class="col-lg-6">
                                            {{Form::text('facebook_id', isset($data_setting->facebook_id) ? $data_setting->facebook_id: '', ['id'=>'facebook_id','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="facebook_secret">Facebook secret</label>
                                        <div class="col-lg-6">
                                            {{Form::text('facebook_secret',  isset($data_setting->facebook_secret) ? $data_setting->facebook_secret: '', ['id'=>'facebook_secret','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="social-2">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="google_fanpage">Google Fanpage</label>
                                        <div class="col-lg-6">
                                            {{Form::text('google_fanpage',  isset($data_setting->google_fanpage) ? $data_setting->google_fanpage: '', ['id'=>'google_fanpage','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="google_id">Google ID</label>
                                        <div class="col-lg-6">
                                            {{Form::text('google_id', isset($data_setting->google_id) ? $data_setting->google_id: '', ['id'=>'google_id','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="google_secret">Google secret</label>
                                        <div class="col-lg-6">
                                            {{Form::text('google_secret',isset($data_setting->google_secret) ? $data_setting->google_secret: '', ['id'=>'google_secret','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="social-3">   
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="twitter_fanpage">Twitter Fanpage</label>
                                        <div class="col-lg-6">
                                            {{Form::text('twitter_fanpage', isset($data_setting->twitter_fanpage) ? $data_setting->twitter_fanpage: '', ['id'=>'twitter_fanpage','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="twitter_id">Twitter ID</label>
                                        <div class="col-lg-6">
                                            {{Form::text('twitter_id', isset($data_setting->twitter_id) ? $data_setting->twitter_id: '', ['id'=>'twitter_id','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="twitter_secret">Twitter secret</label>
                                        <div class="col-lg-6">
                                            {{Form::text('twitter_secret',  isset($data_setting->twitter_secret) ? $data_setting->twitter_secret: '', ['id'=>'twitter_secret','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="social-4">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="linkedin_fanpage">Linkedin Fanpage</label>
                                        <div class="col-lg-6">
                                            {{Form::text('linkedin_fanpage',isset($data_setting->linkedin_fanpage) ? $data_setting->linkedin_fanpage: '', ['id'=>'linkedin_fanpage','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="linkedin_id">Linkedin ID</label>
                                        <div class="col-lg-6">
                                            {{Form::text('linkedin_id', isset($data_setting->linkedin_id) ? $data_setting->linkedin_id: '', ['id'=>'linkedin_id','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="linkedin_secret">Linkedin secret</label>
                                        <div class="col-lg-6">
                                            {{Form::text('linkedin_secret', isset($data_setting->linkedin_secret) ? $data_setting->linkedin_secret: '', ['id'=>'linkedin_secret','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>                    
                                </div>
                                <div class="tab-pane fade" id="social-5">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="instagram_fanpage">Instagram Fanpage</label>
                                        <div class="col-lg-6">
                                            {{Form::text('instagram_fanpage',  isset($data_setting->instagram_fanpage) ? $data_setting->instagram_fanpage: '', ['id'=>'instagram_fanpage','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="instagram_id">Instagram ID</label>
                                        <div class="col-lg-6">
                                            {{Form::text('instagram_id', isset($data_setting->instagram_id) ? $data_setting->instagram_id: '', ['id'=>'instagram_id','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="instagram_secret">Instagram secret</label>
                                        <div class="col-lg-6">
                                            {{Form::text('instagram_secret', isset($data_setting->instagram_secret) ? $data_setting->instagram_secret: '', ['id'=>'instagram_secret','class'=>'bg-focus form-control'])}}
                                        </div> 
                                    </div>     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tabs-5">
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="google_analytics">Google Analytics Code</label>
                    <div class="col-lg-6">
                        {{Form::text('google_analytics', isset($data_setting->google_analytics) ? $data_setting->google_analytics: '', ['id'=>'google_analytics','class'=>'bg-focus form-control'])}}
                    </div> 
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#save_setting').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection
