@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.select_file_modal')
@include('admin.template.alert_ajax')
@endsection
@section("breadcrumb")
<li>
    <a href="{{Asset('')}}">Trang chủ</a>
    <i class="fa fa-circle"></i>
</li>
<li>Cấu hình <i class="fa fa-circle"></i></li>
<li>
    Cấu hình
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
<style>
    .ui-autocomplete {
        max-height: 200px;
        overflow-y: auto;
        overflow-x: hidden;
    }
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">

{{Form::open( array('action'=>'\ADMIN\SettingController@postInfo', 'class'=>'form-horizontal','id'=>'save_setting' ))}}
<?php
if ($data) {
    $data_setting = $data;
}
?>

<div class="row">
    <div class="col-md-12"> 
        <div class="portlet box green-meadow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-filter"></i>Nội dung theo ngôn ngữ</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <ul class="nav nav-tabs">
                    <li class="active">

                        <?php
                        foreach ($l_lang as $item) {
                            if ($item->id == $g_config_all->website_lang) {
                                ?>
                                <a href="#lang_p_{{$item->id}}" data-toggle="tab"><i class="fa fa-comments fa-lg text-default"></i> {{$item->name}}</a>
                                <?php
                            }
                        }
                        ?>

                    </li>
                    <?php if (count($l_lang) > 1) { ?>
                        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg text-default"></i> Ngôn ngữ khác <b class="caret"></b></a>
                            <ul class="dropdown-menu text-left">
                                <?php
                                foreach ($l_lang as $item_child) {
                                    if ($item_child->id != $g_config_all->website_lang) {
                                        ?>
                                        <li> <a href="#lang_p_{{$item_child->id}}" data-toggle="tab">{{$item_child->name}}</a> </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </li>   
                    <?php } ?>
                </ul>
                <div class="panel-body">
                    <div class="tab-content">
                        <?php
                        foreach ($l_lang as $item_content) {
                            ?>
                            <div class="tab-pane fade in <?php
                            if ($item_content->id == $g_config_all->website_lang) {
                                echo ' active';
                            }
                            ?> " id="lang_p_{{$item_content->id}}">
                                <h3>{{$item_content->name}}</h3>                                
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Thành công</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_changeprofile_success_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_changeprofile_success_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_changeprofile_success_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Thất bại</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_changeprofile_fail_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_changeprofile_fail_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_changeprofile_fail_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Họ tên không được để trống</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_name_required_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_name_required_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_name_required_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Email không được để trống</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_email_required_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_email_required_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_email_required_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Email không đúng định dạng</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_email_format_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_email_format_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_email_format_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Email đã tồn tại</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_email_exist_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_email_exist_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_email_exist_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">SĐT không được để trống</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_phone_required_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_phone_required_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_phone_required_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">SĐT phải là số</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_phone_format_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_phone_format_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_phone_format_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Địa chỉ không được để trống</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_address_required_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_address_required_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_address_required_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Nội dung không được để trống</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_content_required_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_content_required_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_content_required_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Tiêu đề không được để trống</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_subject_required_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_subject_required_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_subject_required_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Đang chọn</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_selected_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_selected_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_selected_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Không thể chọn</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_unvail_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_unvail_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_unvail_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Ghế chọn không hợp lệ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_seat_invalid_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_seat_invalid_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_seat_invalid_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Chương trình không hợp lệ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_show_invalid_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_show_invalid_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_show_invalid_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Phiên làm việc quá hạn</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_session_invalid_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_session_invalid_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_session_invalid_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Thông tin không hợp lệ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_info_invalid_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_info_invalid_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_info_invalid_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Giao dịch thành công</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_success_trade_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_success_trade_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_success_trade_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Giao dịch thất bại</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_fail_trade_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_fail_trade_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_fail_trade_'.$item_content->id))}}
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Chữ ký không hợp lệ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_key_trade_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_key_trade_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_key_trade_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Chưa chọn phương thức thanh toán</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_ptype_required_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_ptype_required_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_ptype_required_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Phương thức thanh toán ko hợp lệ</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_ptype_err_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_ptype_err_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_ptype_err_'.$item_content->id))}}
                                    </div> 
                                </div>
								<div class="form-group">
                                    <label class="col-lg-2 control-label">Đặt vé thành công</label>
                                    <div class="col-lg-8">
                                        <?php
                                        $value_lang = '';
                                        foreach ($data_setting as $key => $value) {
                                            if ($key == 'lang_cod_success_' . $item_content->id) {
                                                $value_lang = $value;
                                            }
                                        }
                                        ?>
                                        {{Form::text('lang_cod_success_'.$item_content->id, $value_lang, array('class'=>'bg-focus form-control', 'id'=>'lang_cod_success_'.$item_content->id))}}
                                    </div> 
                                </div>
                            </div>
                            <?php
                        }
                        ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}
</div>
<style>
    .ui-state-focus{
        background: #E3E8ED !important;
        border: none  !important;
        margin: 0  !important;
    }
    .ui-menu-item{
        height: auto !important;

    }
    .ui-menu-item img{
        width: 60px  !important;
        height: 60px  !important;
        float: left;
    }
    .ui-menu-item span:last-child{
        font-size: 10px;
        color: #C0c0c0;
        margin-top: 45px;
    }
    .ui-menu-item span{
        margin-left: 5px !important;
        float: left !important;
        position: absolute;
    }
</style>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a href="javascript:void(0)" onclick="$('#save_setting').submit()" target="_blank" class="active">
                <span>Lưu</span>
                <i class="icon-check"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection
