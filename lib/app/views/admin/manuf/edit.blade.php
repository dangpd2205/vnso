@section("title")
PUBWEB.VN
@endsection
@section("description")
fdgfdgdfg
@endsection
@section("modal_option")
@include('admin.template.alert_ajax')
@include('admin.users.users.file')
@endsection
@section("breadcrumb")
<li>
	<a href="{{Asset('')}}">Trang chủ</a>
	<i class="fa fa-circle"></i>
</li>
<li>Nội dung <i class="fa fa-circle"></i></li>
<li>
	Liên kết website
</li>
@endsection
@extends("admin.template_admin.index")
@section("content")
<div class="row">
    {{Form::open(array('action' => '\ADMIN\ManufController@postEdit','id'=>'add_user','class'=>'form-horizontal'))}}
	<input type="hidden" name="id" value="{{$data->id}}"/>
    <div class="col-md-12">  
        @include('admin.template.error')
        <div class="form-group">
            <label class="col-lg-2 control-label">Ảnh đại diện (400x400)</label>
            <div class="col-lg-6">
                <div class="input-group">
                    {{Form::text('avatar',$data->avatar, array('class'=>'bg-focus form-control', 'id'=>'avatar'))}}
                    <div class="input-group-btn"> <button class="btn btn-white select_image"   data-img-check="0" data-modal-id="Selectfile"  data-toggle="dropdown" data-link="{{Asset('')}}asset/filemanager/dialog.php?type=1&amp;field_id=avatar"> Thêm ảnh </button></div>
                </div>
            </div>
        </div>
		<div class="form-group">
			<label class="col-lg-2 control-label">Tên</label>
			<div class="col-lg-6">
				{{Form::text('name',  $data->name, array('class'=>'bg-focus form-control input-sm', 'id'=>'name'))}}
			</div> 
		</div> 
		<div class="form-group">
			<label class="col-lg-2 control-label">Nội dung</label>
			<div class="col-lg-6">
				{{Form::textarea('content',  $data->content, array('class'=>'bg-focus form-control input-sm tinymce', 'id'=>'content'))}}
			</div> 
		</div> 
		<div class="form-group">
			<label class="col-lg-2 control-label">Link</label>
			<div class="col-lg-6">
				{{Form::text('url',  $data->url, array('class'=>'bg-focus form-control input-sm', 'id'=>'url'))}}
			</div> 
		</div>		<div class="form-group">			<label class="col-lg-2 control-label">Vị trí</label>			<div class="col-lg-6">				{{Form::text('position', $data->position, array('class'=>'bg-focus form-control input-sm', 'id'=>'position'))}}			</div> 		</div>
      
           
    </div>
    {{Form::close()}}
</div>
<nav class="quick-nav">
    <a class="quick-nav-trigger" href="#0">
        <span aria-hidden="true"></span>
    </a>
    <ul>
        <li>
            <a onclick="$('#add_user').submit()" target="_blank" class="active">
                <span>Cập nhật</span>
                <i class="icon-plus"></i>
            </a>
        </li>
    </ul>
    <span aria-hidden="true" class="quick-nav-bg"></span>
</nav>
<div class="quick-nav-overlay"></div>
@endsection