@include('admin.template.header')
<section class="main padder">
    @yield("content")
</section>
@include('admin.template.footer')