</section>
<!-- .modal -->
<div id="modal" class="modal fade">
    <form class="m-b-none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="fa fa-times">
                        </i>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Post your first idea</h4>
                </div>
                <div class="modal-body">
                    <div class="block">
                        <label class="control-label">Title</label>
                        <input type="text" class="form-control" placeholder="Post title">
                    </div>
                    <div class="block">
                        <label class="control-label">Content</label>
                        <textarea class="form-control" placeholder="Content" rows="5">
                        </textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"> Share with all memebers of first </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-sm btn-primary" data-loading-text="Publishing...">Publish</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </form>
</div>
<!-- / .modal -->
<!-- footer -->
<footer id="footer">
    <div class="text-left padder clearfix">
        <p>
            <small>© Copyright by <a href="http://pubweb.vn" target="_blank">Pubweb.vn</a> 2014 . Hotline +84-43-863-1111</small>
        </p>
    </div>
</footer>
<a href="#" class="hide slide-nav-block" data-toggle="class:slide-nav slide-nav-left" data-target="body">
</a>
<!-- / footer -->
<!-- Bootstrap -->

<!-- fuelux -->
<script src="{{Asset('asset/admin')}}/js/fuelux/fuelux.js"></script>
<!-- datepicker -->
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="{{Asset('asset/admin')}}/js/datepicker/bootstrap-datetimepicker.js"></script>
<!-- slider --> 
<script src="{{Asset('asset/admin')}}/js/slider/bootstrap-slider.js"></script>
<!-- file input --> 
<script src="{{Asset('asset/admin')}}/js/file-input/bootstrap.file-input.js"></script> 
<!-- combodate --> 
<script src="{{Asset('asset/admin')}}/js/combodate/moment.min.js"></script> 
<script src="{{Asset('asset/admin')}}/js/combodate/combodate.js"></script> 
<!-- parsley --> 
<script src="{{Asset('asset/admin')}}/js/parsley/parsley.min.js"></script>
<!-- select2 --> 
<script src="{{Asset('asset/admin')}}/js/select2/select2.min.js"></script>
<script src="{{Asset('asset/admin')}}/js/jquery.tagsinput.min.js"></script>
<!-- Tiny MCE -->
<script type="text/javascript" src="{{Asset('asset')}}/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="{{Asset('asset')}}/tinymce/tinymce_config.js"></script>
<script src="{{Asset('asset/admin')}}/js/prettyphoto/jquery.prettyPhoto.js"></script>
<script src="{{Asset('asset/admin')}}/js/grid/jquery.grid-a-licious.min.js"></script>
<script src="{{Asset('asset/admin')}}/js/grid/gallery.js"></script>
<script src="{{Asset('asset/admin')}}/js/jquery.observe_field.js"></script>
<script src="{{Asset('asset/admin')}}/js/autoNumeric.js"></script>
<script src="{{Asset('asset/admin')}}/js/jquery.nestable.js"></script>
<script src="{{Asset('asset/admin')}}/js/jquery-ui.js"></script>
<?php
$controllername = Route::currentRouteAction();
$controllername = substr($controllername, 7);
$pattern = '/(\w+)(@\w+)/i';
$replacement = '${1}';
$controllername = preg_replace($pattern, $replacement, $controllername);
if ($controllername == 'SliderController') {
    ?>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/js/admin.js?ver=5.3.2'></script>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/js/greensock.js?ver=1.11.8'></script>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/js/layerslider.kreaturamedia.jquery.js?ver=5.3.2'></script>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/js/layerslider.transitions.js?ver=5.3.2'></script>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/js/layerslider.transition.gallery.js?ver=5.3.2'></script>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/js/minicolors/jquery.minicolors.js?ver=5.3.2'></script>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/codemirror/lib/codemirror.js?ver=5.3.2'></script>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/codemirror/mode/css/css.js?ver=5.3.2'></script>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/codemirror/mode/javascript/javascript.js?ver=5.3.2'></script>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/codemirror/addon/fold/foldcode.js?ver=5.3.2'></script>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/codemirror/addon/fold/foldgutter.js?ver=5.3.2'></script>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/codemirror/addon/fold/brace-fold.js?ver=5.3.2'></script>
    <script type='text/javascript' src='{{Asset('asset/slider')}}/codemirror/addon/selection/active-line.js?ver=5.3.2'></script>
    <?php
}
?>
<script src="{{Asset('asset/admin')}}/js/jquery-1.7.min.js"></script>                                                                                                  
<script> var j170 = jQuery.noConflict(true);</script>
<script src="{{Asset('asset/admin')}}/js/custom.js"></script>
<script>
 var post_data_url = "{{action('\ADMIN\QuickController@postDataToIndexedDB')}}";</script>
<?php
if ($controllername == 'MenuController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/menu.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'ProductController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/product.js"></script>
	
    <?php
}
?>
<?php
if ($controllername == 'PageController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/product.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'OrderController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/indexeddb.js"></script>
    <script src="{{Asset('asset/admin')}}/js/sync_data.js"></script>
    <script src="{{Asset('asset/admin')}}/js/order_view.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'VendorController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/vendor.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'ThanhToanController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/vendor.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'EventController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/event.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'StoreController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/store.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'LiabilityController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/product.js"></script>
    <script src="{{Asset('asset/admin')}}/js/user.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'PhieuThuController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/product.js"></script>
    <script src="{{Asset('asset/admin')}}/js/user.js"></script>
	
    <script src="{{Asset('asset/admin')}}/js/select2/select2.min.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'PhieuChiController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/product.js"></script>
    <script src="{{Asset('asset/admin')}}/js/user.js"></script>
    <script src="{{Asset('asset/admin')}}/js/select2/select2.min.js"></script>
    <?php
}
?>
 <?php
if ($controllername == 'CouponsController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/coupons.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'XuatHangController') {
    ?>
    <link rel="stylesheet" href="{{Asset('asset/admin')}}/css/jquery-ui-1.10.1.custom.css" type="text/css" />
    <link rel="stylesheet" href="{{Asset('asset/admin')}}/css/jquery.ui.combogrid.css" type="text/css" />
    <script src="{{Asset('asset/admin')}}/js/xuathang.js"></script>
    
    
    <?php
}
?>
<?php
if ($controllername == 'ChuyenKhoController') {
    ?>
    <link rel="stylesheet" href="{{Asset('asset/admin')}}/css/jquery-ui-1.10.1.custom.css" type="text/css" />
    <link rel="stylesheet" href="{{Asset('asset/admin')}}/css/jquery.ui.combogrid.css" type="text/css" />
    <script src="{{Asset('asset/admin')}}/js/chuyenkho.js"></script>
    
    
    <?php
}
?>
<?php
if ($controllername == 'ThongKeController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/xuathang.js"></script>
    
    
    <?php
}
?>
<?php
if ($controllername == 'PinpostController') {
    ?>
    
    <script src="{{Asset('asset/admin')}}/js/news.js"></script>
    <!-- slider --> 
<script src="{{Asset('asset/admin')}}/js/slider/bootstrap-slider.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'NewsController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/product.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'NccOrderController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/xuathang.js"></script>
    <?php
}
?>
<?php
if ($controllername == 'StatisticController') {
    ?>
    <script src="{{Asset('asset/admin')}}/js/charts/flot/jquery.flot.min.js"></script>
    <script src="{{Asset('asset/admin')}}/js/charts/flot/jquery.flot.tooltip.min.js"></script>
    <script src="{{Asset('asset/admin')}}/js/charts/flot/jquery.flot.resize.js"></script>
    <script src="{{Asset('asset/admin')}}/js/charts/flot/jquery.flot.orderBars.js"></script>
    <script src="{{Asset('asset/admin')}}/js/charts/flot/jquery.flot.pie.min.js"></script>
    <script src="{{Asset('asset/admin')}}/js/charts/flot/demo.js"></script>
    <div id="flotTip" style="display: none; position: absolute;"></div>
    <?php
}
?>
</body>
</html>