<?php
if ($errors->has()) {
    ?>
    <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
        <h4><i class="fa fa-bell-o"></i>Cảnh báo!</h4>
        <p>
        <ul>
            <?php
            foreach ($errors->all() as $errors_item) {
                echo $errors_item;
            }
            ?>
        </ul>
    </p>
    </div>
    <?php
}
?>
<?php
if (Session::has('noti_error')) {
    ?>
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">
            <i class="fa fa-times"></i></button> 
        <strong> {{Session::get('noti_error')}} !</strong> 
    </div>
    <?php
}
?>
<?php
if (Session::has('noti_susscess')) {
    ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">
            <i class="fa fa-times"></i></button> 
        <i class="fa fa-check fa-lg"></i><strong> {{Session::get('noti_susscess')}} !</strong> 
    </div>
    <?php
}
?>