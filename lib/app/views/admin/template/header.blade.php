<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="utf-8">
        <title>@yield("title")</title>
        <meta name="description" content='@yield("description")'>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="{{Asset('asset/admin')}}/css/font.css">
        <link rel="stylesheet" href="{{Asset('asset/admin')}}/css/jquery.tagsinput.css" type="text/css" />
        <link rel="stylesheet" href="{{Asset('asset/admin')}}/js/select2/select2.css">
        <link rel="stylesheet" href="{{Asset('asset/admin')}}/js/prettyphoto/prettyPhoto.css" type="text/css" />
        <link rel="stylesheet" href="{{Asset('asset/admin')}}/css/app.v2.css" type="text/css" />
        <link rel="stylesheet" href="{{Asset('asset/admin')}}/css/custom.css" type="text/css" />
		<!-- Bootstrap -->
<script src="{{Asset('asset/admin')}}/js/app.v2.js"></script>
        <!--[if lt IE 9]>
        <script src="{{Asset('asset/admin')}}/js/ie/respond.min.js">
        </script>
        <script src="{{Asset('asset/admin')}}/js/ie/html5.js">
        </script>
        <script src="{{Asset('asset/admin')}}/js/ie/excanvas.js">
        </script>
        <![endif]-->
    </head>
    <body class="navbar-fixed">

        <!-- header -->
        <header id="header" class="navbar">
            <ul class="nav navbar-nav navbar-avatar pull-right">
                <li class="dropdown">
                    <a href="javascript:;;" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="thumb-small avatar inline">
                            <img src="{{Asset('asset/admin')}}/images/avatar.jpg" alt="Mika Sokeil" class="img-circle">
                        </span>
                        <b class="caret hidden-xs-only">
                        </b>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="javascript:;;">Settings</a>
                        </li>
                        <li>
                            <a href="javascript:;;">Profile</a>
                        </li>
                        <li>
                            <a href="javascript:;;">
                                <span class="badge bg-danger pull-right">3</span>Notifications</a>
                        </li>
                        <li class="divider">
                        </li>
                        <li>
                            <a href="docs.html">Help</a>
                        </li>
                        <li>
                            <a href="{{route('admin_logout')}}">Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <a class="navbar-brand" href="">Pubweb.vn</a>
            <button type="button" class="btn btn-link pull-left nav-toggle visible-xs" data-toggle="class:slide-nav slide-nav-left" data-target="body">
                <i class="fa fa-bars fa-lg text-default">
                </i>
            </button>
            <ul class="nav navbar-nav hidden-xs">
                <li>
                    <div class="m-t m-b-small" id="panel-notifications">
                        <a href="javascript:;;" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-comment-o fa-fw fa-lg text-default">
                            </i>
                            <b class="badge badge-notes bg-danger count-n">{{$comment_count}}</b>
                        </a>
                        <section class="dropdown-menu m-l-small m-t-mini">
                            <section class="panel panel-large arrow arrow-top">
                                <header class="panel-heading bg-white">
                                    <span class="h5">
                                        <strong>Bạn có <span class="count-n">{{$comment_count}}</span> thông báo</strong>
                                    </span>
                                </header>
                                <div class="list-group">
                                    <a href="{{action('\ADMIN\CommentController@getView')}}" class="media list-group-item">
                                        <span class="pull-left thumb-small">
                                            <i class="fa fa-comments fa-2x text-default"></i>
                                        </span>
                                        <span class="media-body block m-b-none"> Comment mới<br>
                                        </span>
                                    </a>
                                </div>

                            </section>
                        </section>
                    </div>
                </li>
                <li class="dropdown shift" data-toggle="shift:appendTo" data-target=".nav-primary .nav">
                    <a href="javascript:;;" class="dropdown-toggle" data-toggle="dropdown">

                    </i><i class="fa fa-suitcase"></i> Mặt hàng
                </b>
            </a>
            <ul class="dropdown-menu">
                <li class="dropdown-submenu">
                    <a tabindex="-1" href="javascript:;;">Mặt hàng</a>
                    <ul class="dropdown-menu">
			
					 <li>{{RoleModel::GenLink('\ADMIN\ProductController@getView', 'Tất cả mặt hàng')}}</li>
						 <li>{{RoleModel::GenLink('\ADMIN\ProductController@getAdd', 'Thêm  mặt hàng')}}</li>
							 <li>{{RoleModel::GenLink('\ADMIN\ProductController@getAddGroup', 'Thêm nhóm mặt hàng')}}</li>
								 <li>{{RoleModel::GenLink('\ADMIN\ProductController@getDevideGroup', 'Tách mặt hàng')}}</li>
                    </ul>
                </li>
        
					 <li>{{RoleModel::GenLink('\ADMIN\ProductController@getCategoryView', 'Danh mục')}}</li>
      <li>{{RoleModel::GenLink('\ADMIN\ProductController@getDepartmentView', 'Ngành hàng')}}</li>
	   <li>{{RoleModel::GenLink('\ADMIN\ProductController@getPropertiesView', 'Thuộc tính')}}</li>
	    <li>{{RoleModel::GenLink('\ADMIN\ProductController@getManufactureView', 'Nhà sản xuất')}}</li>
               
            </ul>
        </li>
        <li class="dropdown shift" data-toggle="shift:appendTo" data-target=".nav-primary .nav">
            <a href="javascript:;;" class="dropdown-toggle" data-toggle="dropdown">

            </i><i class="fa fa-dropbox"></i> Kho hàng  <span class="badge bg-warning">35</span>
        </b>
    </a>
    <ul class="dropdown-menu">
		  <li>{{RoleModel::GenLink('\ADMIN\BranchesController@getView', 'Chi nhánh')}}</li>
		    <li>{{RoleModel::GenLink('\ADMIN\VendorController@getView', 'Nhà cung cấp')}}</li>
        <li class="divider hidden-xs"></li>
        <li class="dropdown-submenu">
            <a tabindex="-1" href="javascript:;;">
                Quản lý kho
            </a>
			<ul class="dropdown-menu">
			 <li>{{RoleModel::GenLink('\ADMIN\StoreController@getView', 'Quản lý kho')}}</li>
			  <li>{{RoleModel::GenLink('\ADMIN\StoreController@getCheckView', 'Kiểm kho')}}</li>
			   <li>{{RoleModel::GenLink('\ADMIN\ChuyenKhoController@getView', 'Chuyển kho')}}</li>

                    </ul>
        </li>
		 <li class="dropdown-submenu">
                    <a tabindex="-1" href="javascript:;;">Quản lý nhập kho</a>
                    <ul class="dropdown-menu">
						 <li>{{RoleModel::GenLink('\ADMIN\StoreController@getImportView', 'Quản lý phiếu nhập')}}</li>
						  <li>{{RoleModel::GenLink('\ADMIN\StoreController@getImport', 'Nhập kho')}}</li>
                    </ul>
        </li>
        
       
		 <li class="dropdown-submenu">
                    <a tabindex="-1" href="javascript:;;">In barcode</a>
                    <ul class="dropdown-menu">
						 <li>{{RoleModel::GenLink('\ADMIN\StoreController@getPrintBarcode', 'In barcode to')}}</li>
						  <li>{{RoleModel::GenLink('\ADMIN\StoreController@getPrintBarcodeMin', 'In barcode nhỏ')}}</li>
                    </ul>
        </li>
		  <li>{{RoleModel::GenLink('\ADMIN\StoreController@getSetPrice', 'Thiết lập giá bán')}}</li>
		    <li>{{RoleModel::GenLink('\ADMIN\EventController@getView', 'Khuyến mại')}}</li>
			  <li>{{RoleModel::GenLink('\ADMIN\CouponsController@getView', 'Coupons - Vourcher')}}</li>
    </ul>
</li>
<li class="dropdown shift" data-toggle="shift:appendTo" data-target=".nav-primary .nav">
    <a href="javascript:;;" class="dropdown-toggle" data-toggle="dropdown">

    </i><i class="fa fa-shopping-cart"></i> Đơn hàng  <span class="badge bg-warning">35</span>
</b>
</a>
<ul class="dropdown-menu">
  <li>{{RoleModel::GenLink('\ADMIN\OrderController@getView', 'Quản lý đơn hàng')}}</li>
   <li>{{RoleModel::GenLink('\ADMIN\NccOrderController@getCheckView', 'Đặt hàng NCC')}}</li>
    <li>{{RoleModel::GenLink('\ADMIN\XuatHangController@getCheckView', 'Xuất hàng')}}</li>
	 <li>{{RoleModel::GenLink('\ADMIN\ProductController@getShipView', 'Vận chuyển')}}</li>
    <li class="divider hidden-xs"></li>
    <li>
        <a href="javascript:;;">
            <span class="text">Tạo hóa đơn GTGT</span>
        </a>
    </li>
</ul>
</li>
<li class="dropdown shift" data-toggle="shift:appendTo" data-target=".nav-primary .nav">
    <a href="javascript:;;" class="dropdown-toggle" data-toggle="dropdown">

    </i><i class="fa fa-usd"></i> Tài chính  <span class="badge bg-warning">35</span>
</b>
</a>
<ul class="dropdown-menu">
 <li>{{RoleModel::GenLink('\ADMIN\PhieuThuController@getView', 'Quản lý thu')}}</li>
  <li>{{RoleModel::GenLink('\ADMIN\PhieuThuController@getGroup', 'Quản lý nhóm thu')}}</li>
   <li>{{RoleModel::GenLink('\ADMIN\PhieuChiController@getView', 'Quản lý chi')}}</li>
    <li>{{RoleModel::GenLink('\ADMIN\PhieuChiController@getGroup', 'Quản lý nhóm chi')}}</li>
	    <li>{{RoleModel::GenLink('\ADMIN\ThanhToanController@getView', 'Đề nghị thanh toán')}}</li>
    <li class="divider hidden-xs"></li>
			    <li>{{RoleModel::GenLink('\ADMIN\ProductController@getCurrencyView', 'Đơn vị tiền tệ')}}</li>
						    <li>{{RoleModel::GenLink('\ADMIN\ProductController@getBankView', 'Tài khoản ngân hàng')}}</li>
    <li>
        <a href="javascript:;;">
            <span class="text">Tài khoản ví điện tử</span>
        </a>
    </li>
    <li class="divider hidden-xs"></li>
	 <li>{{RoleModel::GenLink('\ADMIN\LiabilityController@getCustomer', 'Công nợ KH')}}</li>
						    <li>{{RoleModel::GenLink('\ADMIN\LiabilityController@getVendor', 'Công nợ NPP')}}</li>

</ul>
</li>
<li class="dropdown shift" data-toggle="shift:appendTo" data-target=".nav-primary .nav">
    <a href="javascript:;;" class="dropdown-toggle" data-toggle="dropdown">

    </i><i class="fa fa-globe"></i> Nội dung <span class="badge bg-warning">35</span>
</b>
</a>
<ul class="dropdown-menu">
 <li>{{RoleModel::GenLink('\ADMIN\NewsController@getView', 'Quản lý tin tức')}}</li>
  <li>{{RoleModel::GenLink('\ADMIN\NewsController@getCategoryView', 'Danh mục')}}</li>
   <li>{{RoleModel::GenLink('\ADMIN\PinpostController@getView', 'Ghim bài viết')}}</li>
    <li>{{RoleModel::GenLink('\ADMIN\PageController@getView', 'Quản lý trang tĩnh')}}</li>
	 <li>{{RoleModel::GenLink('\ADMIN\CommentController@getView', 'Bình luận')}}</li>
</ul>
</li>
<li class="dropdown shift" data-toggle="shift:appendTo" data-target=".nav-primary .nav">
    <a href="javascript:;;" class="dropdown-toggle" data-toggle="dropdown">

    </i><i class="fa fa-user"></i> Tài khoản <span class="badge bg-warning">35</span>
</b>
</a>
<ul class="dropdown-menu">
 <li>{{RoleModel::GenLink('\ADMIN\UserController@getView', 'Nhân viên')}}</li>
  <li>{{RoleModel::GenLink('\ADMIN\UserController@getGroup', 'Phòng ban')}}</li>
   <li>{{RoleModel::GenLink('\ADMIN\ShiftController@getAdd', 'Quản lý ca bán hàng')}}</li>
    <li>{{RoleModel::GenLink('\ADMIN\LocationController@getView', 'Địa Điểm')}}</li>
	 <li>{{RoleModel::GenLink('\ADMIN\UserController@getCustomer', 'Khách hàng')}}</li>
	  <li>{{RoleModel::GenLink('\ADMIN\PartnerController@getView', 'Nhóm khách hàng')}}</li>

    <li class="divider hidden-xs"></li>
    <li>
        <a href="javascript:;;">
            <span class="text">Hỗ trợ viên</span>
        </a>
    </li>
    <li class="divider hidden-xs"></li>
    <li>
        <a href="javascript:;;">
            <span class="text">Hộp thư</span>
        </a>
    </li>
    <li>
        <a href="javascript:;;">
            <span class="text">Phản hồi</span>
        </a>
    </li>
    <li>
        <a href="javascript:;;">
            <span class="text">Sms Marketing</span>
        </a>
    </li>
</ul>
</li>
<li class="dropdown shift" data-toggle="shift:appendTo" data-target=".nav-primary .nav">
    <a href="javascript:;;" class="dropdown-toggle" data-toggle="dropdown">

    </i><i class="fa fa-bar-chart-o"></i> Thống kê 
</b>
</a>
<ul class="dropdown-menu">
 <li>{{RoleModel::GenLink('\ADMIN\ThongKeController@getCheck', 'Thống kê chung')}}</li>
	  <li>{{RoleModel::GenLink('\ADMIN\ThongKeController@getCheck1', 'Thống kê nhà cung cấp')}}</li>
	   <li>{{RoleModel::GenLink('\ADMIN\ThongKeController@getCheck4', 'Thống kê sản phẩm')}}</li>
	    <li>{{RoleModel::GenLink('\ADMIN\ThongKeController@getCheck5', 'Thống kê theo nhóm chi')}}</li>
		 <li>{{RoleModel::GenLink('\ADMIN\ThongKeController@getCheck6', 'Thống kê theo nhóm thu')}}</li>
		 	    <li>{{RoleModel::GenLink('\ADMIN\ThongKeController@getCheck2', 'Thống kê khách hàng')}}</li>
		 <li>{{RoleModel::GenLink('\ADMIN\ThongKeController@getCheck3', 'Thống kê doanh thu')}}</li>
	
	<li class="dropdown-submenu">
		<a tabindex="-1" href="javascript:;;">Báo cáo</a>
		<ul class="dropdown-menu">
			 <li>{{RoleModel::GenLink('\ADMIN\ThongKeController@getView', 'Báo cáo chung')}}</li>
			<li><a href="">Báo cáo cửa hàng</a></li>
			<li><a href="">Báo cáo online</a></li>
			
		</ul>
	</li>
</ul>
</li>
<li class="dropdown shift" data-toggle="shift:appendTo" data-target=".nav-primary .nav">
    <a href="javascript:;;" class="dropdown-toggle" data-toggle="dropdown">

    </i><i class="fa fa-cogs"></i> Cấu hình  
</b>
</a>
<ul class="dropdown-menu">
 <li>{{RoleModel::GenLink('\ADMIN\SettingController@getView', 'Quản lý cấu hình')}}</li>
  <li>{{RoleModel::GenLink('\ADMIN\MenuController@getView', 'Quản lý menu')}}</li>
   <li>{{RoleModel::GenLink('\ADMIN\SliderController@getSliderView', 'Quản lý slider')}}</li>
</ul>
</li>

<li>
    <div class="m-t-small">
	{{RoleModel::GenLink('\ADMIN\OrderController@getAdd', '<i class="fa fa-home"></i> Bán hàng',[],['class'=>'btn btn-sm btn-primary'])}}
    </div>
</li>
</ul>
</header>
<!-- / header -->
<!-- nav -->
<nav id="nav" class="nav-primary hidden-xs nav-vertical">
    <ul class="nav" data-spy="affix" data-offset-top="50">

    </ul>
</nav>
<!-- / nav -->
@yield("modal_option")
@include('admin.template.alert_ajax')
<section id="content">