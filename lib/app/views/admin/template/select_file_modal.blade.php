<div class="modal fade" id="Selectfile" tabindex="-1" role="dialog" aria-labelledby="SelectfileLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ls">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="SelectfileLabel">Chọn file</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="input_hidden_img_title">
				<input type="hidden" id="input_hidden_img_desc">
				<input type="hidden" id="input_hidden_img_id">
                <input type="hidden" id="input_hidden_img_check">
                <input type="hidden" id="input_hidden_img_div">
                <script>
                    function responsive_filemanager_callback(fieldID) {
                        var id_img = $("#input_hidden_img_id").val();
						var title_img = $("#input_hidden_img_title").val();
						var desc_img = $("#input_hidden_img_desc").val();
                        var url_img = $("#" + fieldID).val();
                        var div_img = $("#input_hidden_img_div").val();
                        var check_img = $("#input_hidden_img_check").val();
               
                        var html = '<div class="thumbnail  pull-left m-l-small item"><a href="' + url_img + '" target="_blank"><img src="' + url_img + '"/></a><span data-img-id="' + id_img + '"> <i class="fa fa-times"></i> </span></div>';
                        if (check_img == 0) {
                            $('#Selectfile').modal('hide');
                        }
                        if (check_img == 1) {
                            $("#" + div_img).html(html);
                            $("#" + id_img).val(url_img);

                        } 
						if (check_img == 10) {
							var html = '<div class="thumbnail  pull-left m-l-small item"><input  data-default="Tiêu đề" data-type="title" data-title="'+title_img+'" data-id="'+div_img+'" name="title" type="text" placeholder="Tiêu đề..." value="Tiêu đề" class="form-control title_input" style="margin-bottom:5px;"/><a href="' + url_img + '" target="_blank"><img src="' + url_img + '"/></a><input type="text" data-default="Mô tả" data-type="desc" data-id="'+div_img+'" data-title="'+desc_img+'" name="desc" value="Mô tả" class="form-control desc_input" placeholder="Mô tả..." style="margin-top:5px;"/><span data-img-id="' + id_img + '" data-img-title="' + title_img + '" data-img-desc="' + desc_img + '"> <i class="fa fa-times"></i> </span></div>';
                            $("#" + div_img).append(html);
							var title="";
							$("#" + div_img+" input[name='title']").each(function(){
								if(title!=''){
									title += ","+$(this).val();
								}else{
									title += $(this).val();
								}			
							});
							var desc="";
							$("#" + div_img+" input[name='desc']").each(function(){
								if(desc!=''){
									desc += ","+$(this).val();
								}else{
									desc += $(this).val();
								}			
							});
                            var images = $("#" + div_img).find("img").map(function () {
                                return this.getAttribute('src');
                            }).get();
                            for (i = 0; i < images.length; i++) {
                                images[i] = images[i].replace('http://' + window.location.hostname, '');
                            }
							$("#" + desc_img).val(desc);
							$("#" + title_img).val(title);
                            $("#" + id_img).val(images);
                            $('#Selectfile').modal('hide');

                        }
						else {
                            $("#" + div_img).append(html);
                            var images = $("#" + div_img).find("img").map(function () {
                                return this.getAttribute('src');
                            }).get();
                            for (i = 0; i < images.length; i++) {
                                images[i] = images[i].replace('http://' + window.location.hostname, '');
                            }
                            $("#" + id_img).val(images);
                            $('#Selectfile').modal('hide');
                        }
                    }
                </script>
                <iframe width="100%" height="400" src="" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; " __idm_frm__="29">
                </iframe>
            </div>
        </div>
    </div>
</div>