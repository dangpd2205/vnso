<!-- Alert Ajax loading -->
<div class="modal fade" id="ajax_alert_loading" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="label_loading" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body text-center">
                <img src="{{Asset('asset/admin/global/img/ajax-loader.gif')}}" alt="Loading ..." title="Loading ..." />
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ajax_alert_confirm" tabindex="-1" role="dialog" aria-labelledby="label_confirm" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Đóng</span></button>
                <h4 class="modal-title" id="label_confirm">Bạn có chắc chắn</h4>
                <input type="hidden" id="id_delete_hidden_confirm" />
                <input type="hidden" id="action_delete_hidden_confirm" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="delete-cancel" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="delete-confirm">Đồng ý</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ajax_alert_success" tabindex="-1" role="dialog" aria-labelledby="label_success" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" style="border-radius: 6px;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Đóng</span></button>
                <h4 class="modal-title" id="label_success">Xử lý thanh công</h4>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ajax_alert_error" tabindex="-1" role="dialog" aria-labelledby="label_error" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="callbackerrormodal($('#tbl_content_ajax_alert_id').val())"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="label_success">Xử lý thất bại</h4>
                <input type="hidden" id="tbl_content_ajax_alert_id" />
            </div>
            <div class="modal-body">
                <div id="tbl_content_ajax_alert">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ajax_alert_confirm1" tabindex="-1" role="dialog" aria-labelledby="label_confirm" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Đóng</span></button>
                <h4 class="modal-title" id="label_confirm">Bạn có chắc chắn duyệt</h4>
                <input type="hidden" id="id_delete_hidden_confirm" />
                <input type="hidden" id="action_delete_hidden_confirm" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="delete-cancel" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="delete-confirm">Đồng ý</button>
            </div>
        </div>
    </div>
</div>