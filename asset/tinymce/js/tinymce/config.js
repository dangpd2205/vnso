var editor_config = {
    language: 'vi_VN',
    selector: ".tinymce",
    theme: "modern",
    skin: 'pubweb',
    plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking code fullscreen",
        "table contextmenu directionality template colorpicker emoticons paste textcolor textpattern responsivefilemanager save"
    ],
    toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
    toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code | emoticons ",
    image_advtab: true,
    external_filemanager_path: "/test/filemanager/",
    filemanager_title: "PUBWEB.VN File Manager",
    external_plugins: {"filemanager": "/test/filemanager/plugin.min.js"}
};
$(document).ready(function () {
    tinymce.init(editor_config);
});