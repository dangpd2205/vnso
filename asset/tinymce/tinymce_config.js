var editor_config = {
//    forced_root_block: "",
//    force_br_newlines: true,
//    force_p_newlines: false,
    language: 'vi_VN',
    selector: ".tinymce",
    theme: "modern",
    skin: 'light',
    convert_urls: false,
    fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
    plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking code fullscreen",
        "table contextmenu directionality template colorpicker emoticons paste textcolor textpattern responsivefilemanager save"
    ],
    toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
    toolbar2: "| link unlink anchor fontselect | image media | forecolor backcolor fontsizeselect | print preview  | emoticons | code ",
    image_advtab: true,
    external_filemanager_path: "/asset/filemanager/",
    filemanager_title: "PUBWEB.VN File Manager",
    setup: function (ed) {
        ed.on('blur', function (e) {
            if ($('#' + ed.id).hasClass('translate_google')) {
                translate_google(ed.id);
            }
        });
    },
    external_plugins: {"filemanager": "/asset/filemanager/plugin.min.js"}
};
var editor_html_config = {
    forced_root_block: "",
    force_br_newlines: true,
    force_p_newlines: false,
    language: 'vi_VN',
    selector: ".tinymce_html",
    theme: "modern",
    skin: 'light',
    height: 300,
    convert_urls: false,
    fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
    plugins: [
        "fullpage advlist autolink link image lists charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking code fullscreen",
        "table contextmenu directionality template colorpicker emoticons paste textcolor textpattern responsivefilemanager save"
    ],
    toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect ",
    toolbar2: "| link unlink anchor fontselect | image media | forecolor backcolor  fontsizeselect | print preview  | emoticons | code | fullpage",
    image_advtab: true,
    external_filemanager_path: "/asset/filemanager/",
    filemanager_title: "PUBWEB.VN File Manager",
    external_plugins: {"filemanager": "/asset/filemanager/plugin.min.js"}
};

$(document).ready(function () {
    tinymce.init(editor_config);
    tinymce.init(editor_html_config);
});