$(function () {
    $('.search_ncc').click(function () {
        var add_name = $(this).data('name');
        var this_tr_click = $(this).parent().parent();
        $('#modal_search').modal();
        creat_autocom2();
    });
	$(document).on('click', '.product_add_span .close', function () {
		var id_del = $(this).data('id_del');
		var id_value = $(this).data('id');
		var id_name = $(this).data('name');
		var this_form_group = $(this).parent().parent().parent();
		$(this).parent().remove();
		var store_value = this_form_group.find("input[name='" + id_value + "']");
		var id_add = '';
		this_form_group.find('.' + id_name).children().each(function () {
			if ($(this).find('button').data('id_del')) {
				if(id_add!=''){
					id_add += ','+ $(this).find('button').data('id_del');
				}else{
					id_add+=$(this).find('button').data('id_del');
				}
			}
		});
		store_value.val(id_add);
	});
});
function creat_autocom2() {
        $("#input_search").autocomplete({
            source: url_search,
            minLength: 3,
            select: function (event, ui) {
                    var html = '<div class="alert alert-success alert-dismissable product_add_span"> <button type="button" class="close" data-name="span_list" data-id="list_id" data-id_del="' + ui.item.id + '"></button><strong> ' + ui.item.label + ' </strong></div>';
                    $('#span_list').append(html);
                    if($('#list_id').val()!=''){
                        $('#list_id').val($('#list_id').val()+','+ui.item.id);                      
                    }else{
                        $('#list_id').val($('#list_id').val()+ui.item.id);
                    }           
                                            
                return false;
            }
        
    });
}
