$(function() {
    $("#print_search1").autocomplete({
        source: url_search,
        minLength: 3,
        select: function (event, ui) {
            
            $("#print_name").val(ui.item.name);
            $("#print_price").val(ui.item.price);
            $("#print_code").val(ui.item.product_store_code);
            $("#print_search1").val('');
            barcode_print();
            return false;
        }
    });
});