$(function() {
    $("select[name='place_id']").change(function(){
        $("#name_seat_lock").val("");
        $("#type_seat_lock").val("");
        $("#span_name").html("");
        $.ajax({
            url: $(this).data('url'),
            data: {id: $(this).val()},
            type: 'POST',
            dataType: 'html',   
            success: function (data) {
                $('.place_img').attr("src",data);                
            }
        });
    });
	/* End */ 
	$(document).on('change keyup', '.desc_input', function () {				if($(this).val()==''){						$(this).val($(this).data("default"));			}				var div_img = $(this).data('id');			var div_title = $(this).data('title');		var title="";				$("#" + div_img+" input[name='desc']").each(function(){				if($(this).val()==""){								var value = "Mô tả";					}else{						var value=$(this).val();				}						if(title!=''){							title += ","+value;						}else{						title += value;					}						});				$("#" + div_title).val(title);				});		$(document).on('change keyup', '.title_input', function () {				if($(this).val()==''){						$(this).val($(this).data("default"));				}				var div_img = $(this).data('id');				var div_title = $(this).data('title');			var title="";				$("#" + div_img+" input[name='title']").each(function(){				if($(this).val()==""){							var value = "Tiêu đề";					}else{								var value=$(this).val();					}						if(title!=''){					title += ","+value;					}else{							title += value;					}						});				$("#" + div_title).val(title);			});	$(document).on('click', '.select_image1', function () {       		var id_modal = $(this).data('modal-id');      		var id_img = $(this).data('img-id');  				var title_img = $(this).data('img-title');  		var desc_img = $(this).data('img-desc');         		$('#input_hidden_img_id').val(id_img);					$('#input_hidden_img_title').val(title_img);			$('#input_hidden_img_desc').val(desc_img);       		$('#input_hidden_img_check').val($(this).data('img-check'));   		$('#input_hidden_img_div').val($(this).data('img-div'));    		$('#' + id_modal + ' iframe').attr('src', jQuery(this).data('link'));     		$('#' + id_modal).modal();    	});

    $('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'});
	
    $(document).on('click', '.select_image', function () {
        var id_modal = $(this).data('modal-id');
        var id_img = $(this).data('img-id');  
        $('#input_hidden_img_id').val(id_img);
        $('#input_hidden_img_check').val($(this).data('img-check'));
        $('#input_hidden_img_div').val($(this).data('img-div'));
        $('#' + id_modal + ' iframe').attr('src', jQuery(this).data('link'));
        $('#' + id_modal).modal();
    });
    $(document).on('click', '.thumbnail.pull-left.m-l-small > span', function (event) {
        var id_img = $(this).data('img-id');						var title_img = $(this).data('img-title');  				var desc_img = $(this).data('img-desc');          var id_div = $(this).parent().parent().attr('id');        $(this).parent().remove();				var title="";				$("#" + id_div+" input[name='title']").each(function(){					if(title!=''){							title += ","+$(this).val();					}else{								title += $(this).val();					}						});				var desc="";			$("#" + id_div+" input[name='desc']").each(function(){				if(desc!=''){								desc += ","+$(this).val();					}else{							desc += $(this).val();					}							});		        var images = jQuery("#"+id_div).find("img").map(function () {            return this.getAttribute('src');        }).get();        for (i = 0; i < images.length; i++) {            images[i] = images[i].replace('http://' + window.location.hostname, '');        }				$("#" + desc_img).val(desc);			$("#" + title_img).val(title);        $("#" + id_img).val(images);
    });
    $(document).on('click', '.open_modal_quick_view', function (event) {
        var link = $(this).data('link');
        var id = $(this).data('product-id');
        var lang = $(this).data('lang-id');
        var data = {id: id};
        if (typeof lang != 'undefined') {
            data = {id: id, lang: lang};
        }
        var modal_id = $(this).data('modal-id');
        $('#ajax_alert_loading').modal({
            backdrop: false
        })
           $.ajax(
                    {
                            url: link,
                            type: "POST",
                            data: data,
                            success: function (data, textStatus, jqXHR)
                            {
                        $('#ajax_alert_loading').modal('hide');
                        var msg = jQuery.parseJSON(data);
                        $('#' + modal_id).find('#title_modal').html(msg.title);
                        $('#' + modal_id).find('.modal-body').html(msg.data);
                        $('#' + modal_id).modal();
                    },
                            error: function (jqXHR, textStatus, errorThrown)
                            {
                                    //if fails      
                            } 
                 });
    });
    /* barcode*/
    
    /* pinpost */
    $('.add_search_news').click(function (event) {
      
    });
    $(document).on('click', '.btl_add_partner_card', function () {
       
    });
    $(document).on('click', '.edit_table_row', function () {
      
    });
    $(document).on('click', '.delete_table_row', function () {
        var curent_del = $(this).closest('tr');
        $('#ajax_alert_confirm').modal('show').one('click', '#delete-confirm', function (e) {
            curent_del.remove();
            $('#ajax_alert_confirm').modal('hide');
        });
    });

    j170('.clicktoggle').live('click', function (event) {
        if ($('#' + $(this).data('togglediv')).is(':hidden')) {
            var div_tog = $(this).data('togglediv');
            $('.togglediv').hide();
            if ($('#' + div_tog).html().trim() == '') {
                $('#ajax_alert_loading').modal({
                    backdrop: false
                });

                $('#' + div_tog).show();
                $('#ajax_alert_loading').modal('hide');

            } else {
                $('#' + div_tog).show();
            }
        } else {
            $('.togglediv').hide();
        }
    });
    j170('.clickorder_detail').live('click', function (event) {
        var current_id = $(this).data('current-id');
        var url = $(this).data('url-post');

        $.ajax({
            url: url,
            data: {id: current_id},
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#order_detail').html(data);
                $('#pop_up_orderdetail').modal();
            }
        });
    });
    j170('.clicktoggle_normal').live('click', function (event) {
        if ($('#' + $(this).data('togglediv')).is(':hidden')) {
            $('.togglediv').hide();
            var current_id = $(this).data('current-id');
            var div_tog = $(this).data('togglediv');
            var url = $(this).data('url-post');
         
                $('#ajax_alert_loading').modal({
                    backdrop: false
                });
                $.ajax({
                    url: url,
                    data: {id: current_id},
                    type: 'POST',
                    dataType: 'html',
                    success: function (data) {
                        $('#' + div_tog).html(data);
                        $('#' + div_tog).show();
                        $('#ajax_alert_loading').modal('hide');
                    }
                });
            
        } else {
            $('.togglediv').hide();
        }
    });
    j170('.edit_dd3').live('click', function (event) {
        var depart = $(this).parents('.dd3-content');
        depart.find("input").each(function () {

            var set_name = $(this).data('edit');
            var set_val = $(this).val();

            var check = document.getElementsByName(set_name)[0].tagName;
            if (check == 'INPUT') {
                if ($("input[name='" + set_name + "']").attr('type') == 'text' || $("input[name='" + set_name + "']").attr('type') == 'hidden') {
                    $("input[name='" + set_name + "']").val('');
                    $("input[name='" + set_name + "']").val(set_val);
                }
                if ($("input[name='" + set_name + "']").attr('type') == 'radio') {
                    $("input[name='" + set_name + "']").each(function () {
                        $(this).removeAttr('checked');
                        $(this).parent().children('i').removeClass('checked');
                        if ($(this).val() == set_val) {
                            $(this).attr('checked', true);
                            $(this).parent().children('i').addClass('checked');
                        }
                    });
                }
                if ($("input[name='" + set_name + "']").attr('type') == 'checkbox') {
                    $("input[name='" + set_name + "']").removeAttr('checked');
                    if ($("input[name='" + set_name + "']").val() == set_val) {
                        $("input[name='" + set_name + "']").attr('checked', 'checked');
                    }
                }
            }
            if (check == 'TEXTAREA') {
                $(document.getElementsByName(set_name)).html(set_val);
            }
            if (check == 'SELECT') {

                $(document.getElementsByName(set_name)).val(set_val);
            }
        });

    });
    j170('.edit_td').live('click', function (event) {
        var depart = $(this).parents('td');
        depart.find("input").each(function () {

            var set_name = $(this).data('edit');
            var set_val = $(this).val();

            var check = document.getElementsByName(set_name)[0].tagName;
     
            if (check == 'INPUT') {
                if ($("input[name='" + set_name + "']").attr('type') == 'text' || $("input[name='" + set_name + "']").attr('type') == 'hidden') {
                    $("input[name='" + set_name + "']").val('');
                    $("input[name='" + set_name + "']").val(set_val);
                }
                if ($("input[name='" + set_name + "']").attr('type') == 'radio') {
                    $("input[name='" + set_name + "']").each(function () {
                        $(this).removeAttr('checked');
                        $(this).parent().children('i').removeClass('checked');
                        if ($(this).val() == set_val) {
                            $(this).attr('checked', true);
                            $(this).parent().children('i').addClass('checked');
                        }
                    });
                }
                if ($("input[name='" + set_name + "']").attr('type') == 'checkbox') {
                    $("input[name='" + set_name + "']").removeAttr('checked');
                    if ($("input[name='" + set_name + "']").val() == set_val) {
                        $("input[name='" + set_name + "']").attr('checked', 'checked');
                    }
                }
            }
            if (check == 'TEXTAREA' || check == 'META') {
                $(document.getElementsByName(set_name)).html(set_val);
            }
            if (check == 'SELECT') {

                $(document.getElementsByName(set_name)).val(set_val);
            }
        });

    });
    $('.nestable').nestable({
        maxDepth: 5
    }).on('change', updateCategory);

    $(".delete_row_table").click(function (e) {
        var this_row = $(this);
        var id = this_row.data('id');
        $('#ajax_alert_confirm').modal('show').one('click', '#delete-confirm', function (e) {
            $('#ajax_alert_confirm').modal('hide');
            $('#ajax_alert_loading').modal({
                backdrop: false
            });
             $.ajax({
                        url: url_delete,
                        type: "POST",
                        data: {id: id},
                        success: function (data, textStatus, jqXHR)
                        {
                    $('#ajax_alert_loading').modal('hide');
                    this_row.closest('li').remove().fadeOut();
                }
            });
        });
    });
});
function btnlockseat(id_modal) {
    $('#' + id_modal).modal('hide');
    $('#ajax_alert_loading').modal({
        backdrop: false
    });
    var arr_name = [];
    var arr_type = [];
    $("#span_name").html("");
    $("input:checkbox[name='seat[]']:checked").each(function (i) {        
        if($(this).parent().hasClass('seat-s')){
            arr_type.push('seat-s');
        }else if($(this).parent().hasClass('seat-a')){
            arr_type.push('seat-a');
        }else if($(this).parent().hasClass('seat-b')){
            arr_type.push('seat-b');
        }
        arr_name.push($(this).val());
        $("#span_name").append('<span class="label label-sm label-success" style="margin:2px">'+$(this).val()+'</span>');
    });
    $("#name_seat_lock").val(arr_name.join(','));
    $("#type_seat_lock").val(arr_type.join(','));
    $('#ajax_alert_loading').modal('hide');
}
function number_format (number, decimals, dec_point, thousands_sep) {
    var n = number, prec = decimals;
    var toFixedFix = function (n,prec) {
        var k = Math.pow(10,prec);
        return (Math.round(n*k)/k).toString();
    };
    n = !isFinite(+n) ? 0 : +n;
    prec = !isFinite(+prec) ? 0 : Math.abs(prec);
    var sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
    var dec = (typeof dec_point === 'undefined') ? '.' : dec_point;
    var s = (prec > 0) ? toFixedFix(n, prec) : toFixedFix(Math.round(n), prec); 
    //fix for IE parseFloat(0.55).toFixed(0) = 0;
    var abs = toFixedFix(Math.abs(n), prec);
    var _, i;
    if (abs >= 1000) {
        _ = abs.split(/\D/);
        i = _[0].length % 3 || 3;
        _[0] = s.slice(0,i + (n < 0)) +
        _[0].slice(i).replace(/(\d{3})/g, sep+'$1');
        s = _.join(dec);
    } else {
        s = s.replace('.', dec);
    }
    var decPos = s.indexOf(dec);
    if (prec >= 1 && decPos !== -1 && (s.length-decPos-1) < prec) {
        s += new Array(prec-(s.length-decPos-1)).join(0)+'0';
    }
    else if (prec >= 1 && decPos === -1) {
        s += dec+new Array(prec).join(0)+'0';
    }
    return s; 
}
function btnpickseat(id_modal) {
    $('#' + id_modal).modal('hide');
    $('#ajax_alert_loading').modal({
        backdrop: false
    });
    var arr_name = [];
    var arr_type = [];
    var arr_price = 0;
    $("#span_name").html("");
    $("input:checkbox[name='seat[]']:checked").each(function (i) {        
        if($(this).parent().hasClass('seat-s')){
            arr_type.push('seat-s');
        }else if($(this).parent().hasClass('seat-a')){
            arr_type.push('seat-a');
        }else if($(this).parent().hasClass('seat-b')){
            arr_type.push('seat-b');
        }
        arr_name.push($(this).val());
        arr_price += parseInt($(this).data('price'));
        $("#span_name").append('<span class="label label-sm label-success" style="margin:2px">'+$(this).val()+'</span>');
        
    });
    $("#span_total").text(number_format(arr_price));
    $("#name_seat").val(arr_name.join(','));
    $("#type_seat").val(arr_type.join(','));
    $('#ajax_alert_loading').modal('hide');
}

function editchange(quan,price,total){
	$("#"+total).text(formatNumber(parseInt($("#"+price).val().split(',').join(''))*parseInt($("#"+quan).val().split(',').join(''))));
}
function removeorder(field) {

    $(field).parent().parent().remove();
}

if (typeof (url_search) == "undefined") {
    url_search = '';
}
function creat_autocom1() {
    var lang = $('#input_search').val();
    $("#news_search").autocomplete({
        source: url_search+"?lang="+$('#input_search').val(),
        minLength: 3,
        select: function (event, ui) {
            var html = '<span class="label bg-primary">'+ui.item.name+'</span>';
                    $('#span_news_'+ui.item.lang).empty().html(html);
                    $('#news_id_'+ui.item.lang).val(ui.item.id);
            return false;

        }
    });
}
function open_modal(id) {
    $('#' + id).modal({
        backdrop: false
    })
}
var updateCategory = function (e)
{
    $('#ajax_alert_loading').modal({
        backdrop: false
    });
    var list = e.length ? e : $(e.target);
    //       alert(window.JSON.stringify(list.nestable('serialize')));
        var postData = window.JSON.stringify(list.nestable('serialize'));
     $.ajax({
                url: url_update_position,
                type: "POST",
                data: {data: postData},
                success: function (data, textStatus, jqXHR)
                {
            $('#ajax_alert_loading').modal('hide');
        }
    });
}
$(document).on('click', '#product_event_list tr td:last-child .fa-trash-o', function () {
    var curent_del = $(this).closest('tr');
    $('#ajax_alert_confirm').modal('show').one('click', '#delete-confirm', function (e) {
        curent_del.remove();
        $('#ajax_alert_confirm').modal('hide');
    });
});
$(document).on('click', '.add_orther_product_event', function () {
    var add_name = $(this).data('name');
    var add_id = $(this).data('id');
    var this_tr_click = $(this).parent().parent();
    $('#add_product_orther').modal();

    $("#search_product_add").autocomplete({
        source: url_search + '?f=1',
        minLength: 3,
        select: function (event, ui) {
            this_tr_click.find('.' + add_name).html(this_tr_click.find('.' + add_name).html() + '<div class="alert alert-success product_add_span"> <button type="button" class="close" data-name="' + add_name + '" data-id="' + add_id + '" data-id_del="' + ui.item.id + '" data-set="product_promotion_id_store"><i class="fa fa-times"></i></button><strong> ' + ui.item.value + ' </strong></div>');
            this_tr_click.find("input[name='" + add_id + "[]']").val(this_tr_click.find("input[name='" + add_id + "[]']").val() + ui.item.id + ',');
            $("#search_product_add").val('');
            return false;
        }
    });
});
function formatNumber(number)
{
    var number = parseFloat(number).toFixed(0) + '';
    var x = number.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
function multi_delete(url, id_return) {
    var array = [];
    $("input:checkbox[name='id[]']:checked").each(function (i) {
        array.push($(this).val());
    });
    if (array.length > 0) {
        $('#ajax_alert_confirm').modal('show').one('click', '#delete-confirm', function (e) {
            $('#ajax_alert_confirm').modal('hide');
            $('#ajax_alert_loading').modal({
                backdrop: false
            })
            var pathname = window.location.href;
            $.ajax({
                url: url,
                 type: "POST",
                dataType: 'html',
                data: {list_id: array, url_r: pathname},
                success: function (data) {
                    $('#' + id_return).html(data);
                    $('#ajax_alert_loading').modal('hide');
                }
            });
        });
    } else {
        alert('please select !');
    }
}
function delete_one(id, action, id_return) {
    $('#ajax_alert_confirm').modal('show').one('click', '#delete-confirm', function (e) {
        $('#ajax_alert_confirm').modal('hide');
        $('#ajax_alert_loading').modal({
            backdrop: false
        })
         $.ajax(
                    {
                            url: action,
                            type: "POST",
                            data: {id: id},
                            success: function (data, textStatus, jqXHR)
                            {
                        $('#' + id_return).html(data);
                        $('#ajax_alert_loading').modal('hide');
                    }
                });
    });
    ;
}
function delete_reload(id, action) {
    $('#ajax_alert_confirm').modal('show').one('click', '#delete-confirm', function (e) {
        $('#ajax_alert_confirm').modal('hide');
        $('#ajax_alert_loading').modal({
            backdrop: false
        });
         $.ajax(
                    {
                            url: action,
                            type: "POST",
                            data: {id: id},
                            success: function (data, textStatus, jqXHR)
                            {
                        location.reload(true);
                    }
                });
    });

}
function btnsubmitadd(id, id_modal) {
    $('#' + id_modal).modal('hide');
    $('#ajax_alert_loading').modal({
        backdrop: false
    })
    $('#' + id).submit(function (e)
    {
            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");
            $.ajax(
                    {
                            url: formURL,
                            type: "POST",
                            data: postData,
                            success: function (data, textStatus, jqXHR)
                            {
                        $('#ajax_alert_loading').modal('hide');
                        msg = jQuery.parseJSON(data);
                        if (msg.check == 'reload') {
                            location.reload(true);
                        } else {
                            if (msg.check == 0) {
                                $('#ajax_alert_error').modal();
                                $('#tbl_content_ajax_alert').html(msg.content);
                                $('#tbl_content_ajax_alert_id').val(id_modal);
                            } else {
                                $('#ajax_alert_success').modal();
                                $('#' + id)[0].reset();
                                $('#tbl_content_ajax').html(msg.content);
                            }
                        }
                                    //data: return data from server
                            },
                            error: function (jqXHR, textStatus, errorThrown)
                            {
                                    //if fails      
                            }
                    });
            e.preventDefault(); //STOP default action
        e.unbind();
    });
    $('#' + id).submit();

}
function thanhtoannot(id, id_modal) {
    
}
function submit_shift_user(id,url,type){
   
}
function qickAddCat(id, id_modal) {
    $('#' + id_modal).modal('hide');
    $('#ajax_alert_loading').modal({
        backdrop: false
    })
    $('#' + id).submit(function (e)
    {
            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");
            $.ajax(
                    {
                            url: formURL,
                            type: "POST",
                            data: postData,
                            success: function (data, textStatus, jqXHR)
                            {
                        $('#ajax_alert_loading').modal('hide');
                        msg = JSON.parse(data);
                        $.each(msg, function (i, field) {
                            $('#cat_list_add_' + field.check).html(field.content);
                        });
                        //$('.checkbox-custom > input[type=checkbox]').each(function () {
                       //     var $this = $(this);
                       //     if ($this.data('checkbox'))
                       //         return;
                       //     $this.checkbox($this.data());
                       // });
                        $('#' + id)[0].reset();
                            },
                            error: function (jqXHR, textStatus, errorThrown)
                            {
                                    //if fails      
                            }
                    });
            e.preventDefault(); //STOP default action
        e.unbind();
    });
    $('#' + id).submit();
}

creat_autocom2('comment');
function creat_autocom2(id_fillter) {
   
}
$('input:radio[name=comment]').change(function () {
    creat_autocom2('comment');
});
$(document).on('click', '.product_add_span .close', function () {
   
});
$(document).on('click', '#product_event_list tr td .add_condition_event', function () {
   
});
$(document).on('click', '#product_event_list tr td .add_promotion_event', function () {
  
});

$(document).on('click', '#add_promotion_event button', function () {
    
});

$(document).on('change', '#fr_add_condition_event', function () {
   
});
function quick_card_form(id, id_modal,id_return){
   
}
function removeNum(string, val){
   var arr = string.split(',');
   for(var i in arr){
      if(arr[i] == val){
         arr.splice(i, 1);
         i--;
      }
  }            
 return arr.join(',');
}
function removeData(field,list_id_value,id) {
	var list_id = $('#'+list_id_value).val();
    var list_id_after_remove = removeNum(list_id,id);
    /* gan lai gia tri chuoi id cho input */
    $('#'+list_id_value).val(list_id_after_remove);
    $(field).parent().parent().remove();
}
function autohidden(id){
    $('#'+id).hide();
}
function fillone(url_link,searchTerm,div_autocomplete){
	if($("#"+searchTerm).val()==''){
        autohidden(div_autocomplete);      

    }else{
		$('#'+div_autocomplete).show();
		var keyword = $("#"+searchTerm).val();
        var request = $.ajax({
            url: url_link +"?keyword=" + keyword+"&special="+searchTerm,
            type: "POST",
            dataType: "html"
        });
        request.done(function(msg) {
            $('#'+div_autocomplete).html(msg);
        });
	}
}
function bindone(id,title,input_display,list_id_value,div_autocomplete){
	$('#'+input_display).val(title);
	$('#'+list_id_value).val(id);
    $('#'+div_autocomplete).hide();
}
function autofill(url_link,searchTerm,div_autocomplete,lang) {
    if($("#"+searchTerm).val()==''){
        autohidden(div_autocomplete);      

    }else{
        $('#'+div_autocomplete).show();
        var keyword = $("#"+searchTerm).val();
        var request = $.ajax({
            url: url_link +"?keyword=" + keyword+"&lang="+lang,
            type: "POST",
            dataType: "html"
        });
        request.done(function(msg) {
            $('#'+div_autocomplete).html(msg);
        });
    }
}
function bindingdata(id,title,id_table,input_display,list_id_value,div_autocomplete){

    var list_id = $('#'+list_id_value).val();
    var check = false;
    /* kiem tra xem da ton tai trong list chua*/

    $('tbody#'+id_table+' tr').each(function(){
        if($(this).attr('id')==id){
            alert('Đã tồn tại trong danh sách');
            $('#'+input_display).val("");
            check=true;
        }
    });
    if(check==false){
        $('#'+input_display).val(title);
        if(id!=''){        
            $('#'+input_display).val($('#'+id).find("td").text());
            if(list_id!=''){
               $('#'+list_id_value).val(list_id+','+id);
            }else{
                $('#'+list_id_value).val(list_id+id);
            }
            /* add du lieu vao table */
            $('#'+id_table).append("<tr id="+id+"><td>"+title+"</td><td><a href='javascript:void(0);' onclick=removeThis(this,'"+list_id_value+"',"+id+");>x</a></td></tr>");
            $('#'+input_display).val('');
        }else{
            $('#'+input_display).val('');
        }
    }
    $('#'+div_autocomplete).hide();
}
function removeThis(field,list_id_value,id) {
    var list_id = $('#'+list_id_value).val();
    var list_id_after_remove = removeNum(list_id,id);
    /* gan lai gia tri chuoi id cho input */
    $('#'+list_id_value).val(list_id_after_remove);
    $(field).parent().parent().remove();

}