var updateOutput = function (e)
{
    $('#ajax_alert_loading').modal({
        backdrop: false
    });
    var list = e.length ? e : $(e.target);
    //       alert(window.JSON.stringify(list.nestable('serialize')));
        var postData = window.JSON.stringify(list.nestable('serialize'));
    var group_id = $('#group_id').val();
        var formURL = $('#action_change').val();
     $.ajax({
         url: formURL,
                type: "POST",
                data: {group_id: group_id, data: postData},
                success: function (data, textStatus, jqXHR)
                {
            $('#ajax_alert_loading').modal('hide');
        }
    });
}
j170('.edit_inline_pin').live('click', function(e) {
    $(".nestable").unbind("change");
    if($(this).is(':checked')){
        var status=1;
    }else{
        var status=0;
    }
    $('#ajax_alert_loading').modal({
        backdrop: false
    });
    $.ajax({
        url: url_pin,
         type: "POST",
        dataType: 'html',
        data: {id:$(this).data("id"),status:status},
        success: function(data) {
            $('#ajax_alert_loading').modal('hide');
        }
    });      
}); 
function open_modal(id) {
    $('#' + id).modal({
        backdrop: false
    })
}
$(function () {
    $('.nestable').nestable({
        maxDepth:5
    }).on('change', updateOutput);
});
function delete_menu(id, action, id_return) {
    $('#ajax_alert_confirm').modal('show').one('click', '#delete-confirm', function (e) {
        $('#ajax_alert_confirm').modal('hide');
        $('#ajax_alert_loading').modal({
            backdrop: false
        })
         $.ajax(
                    {
                            url: action,
                            type: "POST",
                            data: {id: id, group_id: $('#group_id').val()},
                            success: function (data, textStatus, jqXHR)
                            {
                        $('#' + id_return).html(data);
                        $('.nestable').nestable({
                            maxDepth: 5
                        }).on('change', updateOutput);
                        $('#ajax_alert_loading').modal('hide');
                    }
                });
    });
    ;
}
function add_menu(form_add) {
    $('#ajax_alert_loading').modal({
        backdrop: false
    });
    $('#' + form_add).submit(function (e)
    {
            var postData = $(this).serialize();
            var formURL = $(this).attr("action");
         $.ajax({
             url: formURL,
                    type: "POST",
                    data: postData,
                    success: function (data, textStatus, jqXHR)
                    {
                msg = jQuery.parseJSON(data);
                $('#content_menu_edit').html(msg.content);
                $('.nestable').nestable({
                    maxDepth: 3
                }).on('change', updateOutput);
                $('#' + form_add)[0].reset();
                $('#menu_id_edit').val('');
                $('#ajax_alert_loading').modal('hide');
            }
        });
           e.preventDefault(); //STOP default action
        e.unbind();
    });
    $('#' + form_add).submit();
}
function edit_menu(id, name, url, acton, id_return) {
    $('#menu_id_edit').val(id);
    $('#menu_name').val(name);
    $('#menu_name').focus();
    // $('#urlselectoption').val(url);
    $('#menu_url').val(url);
}
function multi_delete_menu(url, id_return) {
    var array = [];
    $("input:checkbox[name='id[]']:checked").each(function (i) {
        array.push($(this).val());
    });
    if (array.length > 0) {
        $('#ajax_alert_confirm').modal('show').one('click', '#delete-confirm', function (e) {
            $('#ajax_alert_confirm').modal('hide');
            $('#ajax_alert_loading').modal({
                backdrop: false
            })
            $.ajax({
                url: url,
                 type: "POST",
                dataType: 'html',
                data: {list_id: array},
                success: function (data) {
                    $('#' + id_return).html(data);
                    $('#ajax_alert_loading').modal('hide');
                }
            });
        });
    } else {
        alert('please select !');
    }
}
$(function () {
    $('#name_group').change(function (e) {
        var name = $(this).val();
        var id = $('#group_id').val();
        var url = $(this).data('link');
        $('#ajax_alert_loading').modal({
            backdrop: false
        })
         $.ajax({
             url: url,
                    type: "POST",
                    data: {id: id, name: name},
                    success: function (data, textStatus, jqXHR)
                    {
                $('#ajax_alert_loading').modal('hide');
            }
        });
    });
});

function btnsubmitadd_menu(id, id_modal) {
    $('#' + id_modal).modal('hide');
    $('#ajax_alert_loading').modal({
        backdrop: false
    })
    $('#' + id).submit(function (e)
    {
            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");
            $.ajax(
                    {
                            url: formURL,
                            type: "POST",
                            data: postData,
                            success: function (data, textStatus, jqXHR)
                            {
                        $('#ajax_alert_loading').modal('hide');
                        msg = jQuery.parseJSON(data);
                        if (msg.check == 0) {
                            $('#ajax_alert_error').modal();
                            $('#tbl_content_ajax_alert').html(msg.content);
                            $('#tbl_content_ajax_alert_id').val(id_modal);
                        } else {
                            $('#ajax_alert_success').modal();
                            $('#' + id)[0].reset();
                            $('#tbl_content_menu_ajax').html(msg.content);
                        }
                                    //data: return data from server
                            },
                            error: function (jqXHR, textStatus, errorThrown)
                            {
                                    //if fails      
                            }
                    });
            e.preventDefault(); //STOP default action
        e.unbind();
    });
    $('#' + id).submit();

}