$(function () {
    $(document).on('change', "input[name='type_coupons']", function () {
        var check = $(this).val();

        if (check == 1) {
            $('#giftcard').addClass('hide');
            $('#coupons').removeClass('hide');
        }
        if (check == 0) {
            $('#coupons').addClass('hide');
            $('#giftcard').removeClass('hide');
        }
    });
    $(document).on('change', "input[name='auto_prefix']", function () {
        var check = $(this).val();
        if ($(this).is(':checked')) {
            $('#prefix').val('CP-');
        }
    });
    var val_seelc = $('#branch_id_select').val();
    $('#branch_id_select').change(function () {
        if ($('.add_orther_product_name').html() !== '') {
            var this_val = $(this).val();
            $('#ajax_alert_confirm_add_sp').modal('show').one('click', '#delete-confirm', function (e) {
                $('.add_orther_product_name').html('');
                $('#product_id_store').val('');
                $('#ajax_alert_confirm_add_sp').modal('hide');
                start();
            }).one('click', '#delete-cancel', function (e) {
                $('#branch_id_select').val(val_seelc);
                start();
            });
        } else {
            start();
        }

    });

});
function publish_one(id, action, id_return) {
    $('#ajax_alert_confirm_publish').modal('show').one('click', '#delete-confirm', function (e) {
        $('#ajax_alert_confirm').modal('hide');
        $('#ajax_alert_loading').modal({
            backdrop: false
        });
         $.ajax(
                    {
                            url: action,
                            type: "POST",
                            data: {id: id},
                            success: function (data, textStatus, jqXHR)
                            {
                        $('#' + id_return).html(data);
                        $('#ajax_alert_loading').modal('hide');
                    }
                });
    });
    ;
}
$(document).on('click', '.view_coupons', function () {
    var location = $(this).parent('td');
    $('#edit_new_location').find('#name').val(location.find("input[name='name']").val());
    $('#edit_new_location').find('#parent').val(location.find("input[name='parent']").val());
    $('#edit_new_location').find('#id').val(location.find("input[name='id']").val());
    $('#edit_new_location').find('#description').val(location.find("input[name='description']").val());
});
var fill = $('input:radio[name=comment]:checked').val();
var c_this;
$('input:radio[name=comment]').change(function () {
    fill = $('input:radio[name=comment]:checked').val();
    search_item(c_this, fill);
});

$(document).on('click', '.add_orther_product_event', function () {
    c_this = $(this);
    search_item($(this), fill);
});
function search_item(c_this, val_fill) {
    var add_name = c_this.data('name');
    var add_type = c_this.data('type');
    var add_id = c_this.data('id');
    var this_tr_click = c_this.parent().parent();
    $('#add_product_orther').modal();
    $("#search_product_add").autocomplete({
        source: url_search + '?f=' + val_fill,
        minLength: 3,
        select: function (event, ui) {
            if (ui.item.type == 0) {
                if (this_tr_click.find("input[name='" + add_type + "']").val() == '1') {
                    this_tr_click.find('.' + add_name).html('');
                    this_tr_click.find("input[name='" + add_id + "']").val('');
                }
                this_tr_click.find("input[name='" + add_type + "']").val(val_fill);
                this_tr_click.find('.' + add_name).html('<div class="alert alert-success product_add_span"> <strong> ' + ui.item.value + ' </strong></div>');
                this_tr_click.find("input[name='" + add_id + "']").val(ui.item.id);
                $("#search_product_add").val('');
            } else {
                if (this_tr_click.find("input[name='" + add_type + "']").val() == '0') {
                    this_tr_click.find('.' + add_name).html('');
                    this_tr_click.find("input[name='" + add_id + "']").val('');
                }
                this_tr_click.find("input[name='" + add_type + "']").val(val_fill);
                this_tr_click.find('.' + add_name).html(this_tr_click.find('.' + add_name).html() + '<div class="alert alert-success product_add_span"> <button type="button" class="close" data-name="' + add_name + '" data-id="' + add_id + '" data-id_del="' + ui.item.id + '" data-set="product_promotion_id_store"><i class="fa fa-times"></i></button><strong> ' + ui.item.value + ' </strong></div>');
                this_tr_click.find("input[name='" + add_id + "']").val(this_tr_click.find("input[name='" + add_id + "']").val() + ui.item.id + ',');
                $("#search_product_add").val('');
            }
            return false;
        }
    });
}