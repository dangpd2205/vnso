function find_event_sale(id_product, p_price) {
    var event_list = [];
    var list_table = [];
    var product_cat_list = [];
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    var trans = db_index.transaction(list_table, 'readonly');
    trans.oncomplete = function (evt) {
        var event_sale_p = 0;
        var dkap = '';
        $.each(event_list, function (i, event_item) {
            var event_sale_p_child = 0;
            if (event_item.price_sales_type == 1) {
                event_sale_p_child = event_item.price_sales;
            }
            if (event_item.price_sales_type == 2) {
                event_sale_p_child = parseFloat(p_price) / 100 * parseFloat(event_item.price_sales);
            }

            if (event_sale_p_child > event_sale_p) {
                event_sale_p = event_sale_p_child;
            }
            if (event_item.localtion != '' || event_item.gender != 0 || event_item.vip_card != '' || parseInt(event_item.total_pay) != 0 || event_item.recommend != 0) {
                dkap = ' - <a href="javascript:;" class = "bt_comfirm_event">Điều kiện áp dụng </a> <input style="margin: 0px !important;position: relative;top: 4px;" type="checkbox" name="product_event_confirm[]" onchange="total_money_order()" value="1">';
            }
        });
        var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
        $(id_tabs).find('#product_order_list').find('tr').each(function (i) {
            var id_p = $(this).find("input[name='product_select_id[]']").val();
            if (id_p == id_product) {

                $(this).find("input[name='product_event_sale[]']").val(event_sale_p);
                $(this).find(".giam_event_des").html('Giảm giá KM : ' + formatNumber(event_sale_p) + 'VNĐ ' + dkap);
            }
        });
        total_money_order();
    }


    var store_e = trans.objectStore('tbl_event');
    var cursorRequeste = store_e.openCursor();
    cursorRequeste.onerror = function (error) {
        console.log(error);
    };
    cursorRequeste.onsuccess = function (evte) {
        var cursor = evte.target.result;
        if (cursor) {

            if (cursor.value.type == 1) {
                $.each($.parseJSON(cursor.value.product_id), function (i, field) {
                    if (field.id == id_product) {
                        event_list.push(cursor.value);
                    }
                });
            }
            if (cursor.value.type == 0) {
                var store_v = trans.objectStore('tbl_product_views');
                var cursorRequestv = store_v.openCursor();
                cursorRequestv.onerror = function (error) {
                    console.log(error);
                };
                cursorRequestv.onsuccess = function (evtv) {
                    var cursorv = evtv.target.result;
                    if (cursorv) {
                        if (cursorv.value.product_id == id_product) {
                            if (cursorv.value.cat_id == $.parseJSON(cursor.value.product_id).id) {
                                event_list.push(cursor.value);
                            }
                        }
                        cursorv.continue();
                    }
                }

            }

            cursor.continue();
        }
    }
}
function order_return(order_code) {
    if (order_code != '') {
        var this_order = [];
        var list_table = [];
        var order_detail_list = [];
        $.each(db_index.objectStoreNames, function (i, field) {
            list_table.push(field);
        });
        var trans = db_index.transaction(list_table, 'readonly');
        trans.oncomplete = function (evt) {
            if (this_order.length == 0) {
                alert('Đơn hàng không tồn tại !');
            } else {
                var html_inser = '<input type="hidden" name="order_user_return[]" value="' + this_order.user_id + '">\n\
<tr>';
                var count_detail = 1;
                $.each(order_detail_list, function (i, field) {
                    if (field.order_id == this_order.id) {
                        html_inser += '<td>\n\
<input type="hidden" name="product_id_return[]" value="' + field.p_id + '">\n\
<input type="hidden" name="product_store_id_return[]" value="' + field.store_code + '">\n\
<input type="hidden" name="product_quantity_return[]" value="' + field.quantity + '">\n\
<input type="hidden" name="product_price_return[]" value="' + field.order_price + '">\n\
' + count_detail + '</td>';
                        html_inser += '<td>' + field.store_code + '</td>';
                        html_inser += '<td>' + field.name + '</td>';
                        html_inser += '<td>' + formatNumber(field.order_price) + '</td>';
                        html_inser += '<td>' + formatNumber(field.quantity) + '</td>';
                        html_inser += '<td><input name="product_order_return[]"  onkeyup="total_money_order();"  onkeydown="total_money_order();" onchange="total_money_order();" onkeypress="total_money_order();"  class="bg-focus form-control input-sm auto_number" type="text" value="0"></td>';
                        html_inser += '<td><input name="product_order_price_return[]"  onkeyup="total_money_order();"  onkeydown="total_money_order();" onchange="total_money_order();" onkeypress="total_money_order();"  class="bg-focus form-control input-sm auto_number" type="text"></td>';
                        count_detail++;
                    }
                });
                html_inser += '</tr>';
                var d = new Date();
                var n = d.getTime();
                var id = n;
                var tabId = 'order_' + id;
                $('.tabs_order').find('li:last-child').before('<li><a data-toggle="tab" href="#order_' + id + '">#Trả hàng ' + $(".tabs_order").children().length + '</a> <span> <i class="fa fa-times"></i> </span></li>');
                $('.tabs_order_content').append('<div class="tab-pane" id="' + tabId + '">' + $('#sample_order').html().replace(/infoorder/gi, "infoorder_" + id) + '</div>');
                var trahang = $('#sample_order_retrieve').html();
                $('#' + tabId).find('.order_div_left').prepend(trahang);
                $('#' + tabId).find('.table_product_group_divide_item').html(html_inser);
                $('#' + tabId).find('#tong_tien_hang').closest('div.panel-body').prepend('<div class="form-group">\n\
                            <label class="col-xs-3 control-label">Tiền trả hàng</label>\n\
                            <div class="col-xs-8">\n\
                                <strong id="tong_tien_tra_hang"> 0</strong>\n\
                                <br><span id="tong_tien_tra_hang_ngoai_te"></span>\n\
                            </div>\n\
                        </div>');
                $('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'});
                $('.tabs_order').find("a[href='#" + tabId + "']").trigger('click');
                add_user_to_input();
                add_data_to_curremcy('tbl_currency', '#currency_id_select');
                add_data_to_select('tbl_bank_acc', '#thanh_toan_type');
                add_data_to_alert('tbl_currency', '.alert_exchange_rate');
                $('.radio-custom > input[type=radio]').each(function () {
                    var $this = $(this);
                    if ($this.data('radio'))
                        return;
                    $this.radio($this.data());
                });
                $('.tabs_order_content div.active .select2-option-sample').select2();
                $('#search_order_product').modal('hide');
            }
        }
        var tbl_order = trans.objectStore('tbl_order');
        var tbl_order_req = tbl_order.openCursor();
        tbl_order_req.onerror = function (error) {
            console.log(error);
        };
        tbl_order_req.onsuccess = function (evt) {
            var cursor = evt.target.result;
            if (cursor) {
                var cur_row = cursor.value;
                if (cur_row.code == order_code) {
                    this_order = cur_row;
                }
                cursor.continue();
            }
        };
        var properties_detail = trans.objectStore('tbl_order_detail');
        var cursorRequest_properties = properties_detail.openCursor();
        cursorRequest_properties.onerror = function (error) {
            console.log(error);
        };
        cursorRequest_properties.onsuccess = function (evt) {
            var cursor = evt.target.result;
            if (cursor) {
                var cur_row = cursor.value;
                order_detail_list.push(cur_row);
                cursor.continue();
            }
        };
    } else {
        alert('Đơn hàng không tồn tại !');
    }
}
$(function () {
    $(document).on('click', '.bt_comfirm_event', function () {
        $('#add_quick_event').trigger('click');
    });
    $('.apply_event_btn').click(function (e) {
        var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
        var this_modal = $(this).closest('#event_apply');
        var list_product = [];
        $(id_tabs).find('section.banhang').find('#product_order_list').children('tr').each(function (i) {
            var id = $(this).find("input[name='product_select_id[]']").val();
            var quantity = $(this).find("input[name='product_quatity[]']").autoNumeric('get');
            var info = $(this).find("input[name='product_full_info[]']").val();
            list_product.push({
                'id': id,
                'info': info,
                'quantity': quantity
            });
        });

        var list_event_apply = [];
        this_modal.find('#product_event_list').find('tr').each(function () {
            if ($(this).find("input[name='applyed']").is(':checked')) {
                var id = $(this).find("input[name='applyed']").val();
                var event = $(this).find("input[name='all_event']").val();
                var quantity = $(this).find("input[name='quantity_apply']").autoNumeric('get');
                list_event_apply.push({
                    'id': id,
                    'quantity': quantity,
                    event: event
                });
            }
        });

    });
    function modal_apply_event() {
        var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
        var this_modal = $(this).closest('#event_apply');
        var list_product = [];
        $(id_tabs).find('section.banhang').find('#product_order_list').children('tr').each(function (i) {
            var id = $(this).find("input[name='product_select_id[]']").val();
            var quantity = $(this).find("input[name='product_quatity[]']").autoNumeric('get');
            var info = $(this).find("input[name='product_full_info[]']").val();
            list_product.push({
                'id': id,
                'info': info,
                'quantity': quantity
            });
        });
        this_modal.find('#product_event_list').find('tr').each(function () {
            if ($(this).find("input[name='applyed']").is(':checked')) {

            }
        });
    }
    $(document).on('change', '#event_apply input[name=\'quantity_apply\']', function (e) {
        modal_apply_event();
    });
    $(document).on('change', '#event_apply input[name=\'applyed\']', function (e) {
        modal_apply_event();
    });
    $(document).on('click', '.btl_end_shift', function (e) {
        run_upload_db();
    });
    function run_upload_db() {
        var post_data = [];
        var list_table = ['tbl_order', 'tbl_order_detail', 'tbl_event', 'tbl_code', 'tbl_users', 'tbl_order_return', 'tbl_order_return_detail'];
        var trans = db_index.transaction(['tbl_order', 'tbl_order_detail', 'tbl_event', 'tbl_code', 'tbl_users', 'tbl_order_return', 'tbl_order_return_detail'], 'readwrite');
        trans.oncomplete = function (evt) {
            if (post_data.length > 0) {

                 $.ajax({
                     url: post_sync_url,
                            type: "POST",
                            data: {data: JSON.stringify(post_data)},
                            success: function (data, textStatus, jqXHR)
                            {
                        db_index.close();
                        var DBDeleteRequest = window.indexedDB.deleteDatabase('pubweb_indexeddb11111');
                        location.reload(true);
                    }
                });

            }
        }
        $.each(list_table, function (key, value) {
            var tbl_table = trans.objectStore(value);
            var tbl_table_req = tbl_table.openCursor();
            tbl_table_req.onerror = function (error) {
                console.log(error);
            };
            tbl_table_req.onsuccess = function (evt) {
                var cursor = evt.target.result;
                if (cursor) {
                    var cur_row = cursor.value;
                    cursor.value.table = value;
                    if (cur_row.sync == 0) {
                        post_data.push(cursor.value);

                    }
                    cursor.continue();
                }
            };
        });
    }
    $('#end_shift_post').click(function (e) {
        if (navigator.onLine) {
            var this_order = [];
            var list_table = [];
            var order_detail_list = [];
            $.each(db_index.objectStoreNames, function (i, field) {
                list_table.push(field);
            });
            var trans = db_index.transaction(list_table, 'readonly');
            trans.oncomplete = function (evt) {
                var count_order = 1;
                var count_product_quantity = 0;
                var count_product_money = 0;
                var count_product_money_cash = 0;
                var count_product_money_atm = 0;
                var html_inser = '';
                $.each(this_order, function (i, field) {
                    var product_count = 0;
                    var order_total_price = 0;
                    $.each(order_detail_list, function (i, field_detail) {
                        if (field.id == field_detail.order_id) {
                            product_count += parseInt(field_detail.quantity);
                            order_total_price += parseInt(field_detail.quantity) * parseInt(field_detail.order_price);
                        }
                    });
                    count_product_quantity += product_count;
                    count_product_money += order_total_price;
                    if (field.pay_type == 0) {
                        count_product_money_cash += order_total_price;
                    } else {
                        count_product_money_atm += order_total_price;
                    }
                    html_inser += '<tr>';
                    html_inser += '<td> #' + count_order + '</td>';
                    html_inser += '<td>' + field.code + '</td>';
                    html_inser += '<td>' + formatNumber(product_count) + '</td>';
                    if (field.pay_type == 0) {
                        html_inser += '<td>Tiền mặt </td>';
                    } else {
                        html_inser += '<td>Thẻ</td>';
                    }
                    html_inser += '<td>' + formatNumber(order_total_price) + '</td>';
                    html_inser += '</tr>';
                    count_order++;
                });
                $('#order_list_end_shift').html(html_inser);
                var html_total_shift = '';
                html_total_shift += '<tr>\n\
                                <td colspan="4"><strong> Tổng đơn hàng</strong></td>\n\
                                <td><strong>' + (parseInt(count_order) - 1) + '</strong></td>\n\
                            </tr>';
                html_total_shift += '<tr>\n\
                                <td colspan="4"><strong> Tổng số sản phẩm</strong></td>\n\
                                <td><strong>' + formatNumber(count_product_quantity) + '</strong></td>\n\
                            </tr>';
                html_total_shift += '<tr>\n\
                                <td colspan="4"><strong> Tổng tiền thu về</strong></td>\n\
                                <td><strong>' + formatNumber(count_product_money) + '</strong></td>\n\
                            </tr>';
                html_total_shift += '<tr>\n\
                                <td colspan="4"><strong> Tiền mặt</strong></td>\n\
                                <td><strong>' + formatNumber(count_product_money_cash) + '</strong></td>\n\
                            </tr>';
                html_total_shift += '<tr>\n\
                                <td colspan="4"><strong>Tiền thẻ</strong></td>\n\
                                <td><strong>' + formatNumber(count_product_money_atm) + '</strong></td>\n\
                            </tr>';
                $('#product_shift_end_total').html(html_total_shift);
                open_modal('end_shift');
            }
            var tbl_order = trans.objectStore('tbl_order');
            var tbl_order_req = tbl_order.openCursor();
            tbl_order_req.onerror = function (error) {
                console.log(error);
            };
            tbl_order_req.onsuccess = function (evt) {
                var cursor = evt.target.result;
                if (cursor) {
                    var cur_row = cursor.value;
                    if (cur_row.sync == 0) {
                        this_order.push(cur_row);
                    }
                    cursor.continue();
                }
            };
            var properties_detail = trans.objectStore('tbl_order_detail');
            var cursorRequest_properties = properties_detail.openCursor();
            cursorRequest_properties.onerror = function (error) {
                console.log(error);
            };
            cursorRequest_properties.onsuccess = function (evt) {
                var cursor = evt.target.result;
                if (cursor) {
                    var cur_row = cursor.value;
                    if (cur_row.sync == 0) {
                        order_detail_list.push(cur_row);
                    }
                    cursor.continue();
                }
            };
        } else {
            alert('Không có kết nối mạng !');
        }
    });
    $('#btl_back_order').click(function (e) {
        open_modal('search_order_product');
        var d = new Date();
        var curent_time = parseInt(d.getTime()) / 1000;
        var order_list = [];
        var order_detail = [];
        var list_table = [];
        var product_cat_list = [];
        $.each(db_index.objectStoreNames, function (i, field) {
            list_table.push(field);
        });
        $.each(db_index.objectStoreNames, function (i, field) {
            list_table.push(field);
        });
        var trans = db_index.transaction(list_table, 'readonly');
        trans.oncomplete = function (evt) {
            console.log(order_list);
        }
        var store_e = trans.objectStore('tbl_order');
        var cursorRequeste = store_e.openCursor();
        cursorRequeste.onerror = function (error) {
            console.log(error);
        };
        cursorRequeste.onsuccess = function (evte) {
            var cursor = evte.target.result;
            if (cursor) {
                if (cursor.value.time > curent_time - 30 * 24 * 60 * 60) {
                    order_list.push(cursor.value);
                }
                cursor.continue();
            }
        }

//        var id = n;
//        var tabId = 'order_' + id;
//        $('.tabs_order').find('li:last-child').before('<li><a data-toggle="tab" href="#order_' + id + '">#Trả hàng ' + $(".tabs_order").children().length + '</a> <span> <i class="fa fa-times"></i> </span></li>');
//        $('.tabs_order_content').append('<div class="tab-pane" id="' + tabId + '">' + $('#sample_order').html().replace(/infoorder/gi, "infoorder_" + id) + '</div>');
//        var trahang = $('#sample_order_retrieve').html();
//        $('#' + tabId).find('.order_div_left').prepend(trahang);
//        $('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'});
    });
// add tabs order
    $('.add-contact').click(function (e) {
        e.preventDefault();
        var d = new Date();
        var n = d.getTime();
        var id = n;
        var tabId = 'order_' + id;
        $(this).closest('li').before('<li><a data-toggle="tab" href="#order_' + id + '">#' + $(".tabs_order").children().length + '</a> <span> <i class="fa fa-times"></i> </span></li>');
        $('.tabs_order_content').append('<div class="tab-pane" id="' + tabId + '">' + $('#sample_order').html().replace(/infoorder/gi, "infoorder_" + id) + '</div>');
        $('.tabs_order li:nth-child(' + id + ') a').click();
        $('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'});
        $('.tabs_order').find("a[href='#" + tabId + "']").trigger('click');
        $('.radio-custom > input[type=radio]').each(function () {
            var $this = $(this);
            if ($this.data('radio'))
                return;
            $this.radio($this.data());
        });
        $('.tabs_order_content div.active .select2-option-sample').select2();
        $("[data-toggle=popover]").popover({trigger: "hover"});
        add_user_to_input();
        add_data_to_curremcy('tbl_currency', '#currency_id_select');
        add_data_to_select('tbl_bank_acc', '#thanh_toan_type');
        add_data_to_alert('tbl_currency', '.alert_exchange_rate');
    });
    $(".tabs_order").on("click", "a", function (e) {
        e.preventDefault();
        if (!$(this).hasClass('add-contact')) {
            $(this).tab('show');
        }
    })//Xóa tabs
            .on("click", "span", function () {
                var this_click = $(this);
                var anchor = $(this).siblings('a');
                $('#ajax_alert_confirm').modal('show').one('click', '#delete-confirm', function (e) {
                    $(anchor.attr('href')).remove();
                    this_click.parent().remove();
                    $(".tabs_order li").children('a').first().click();
                    $('#ajax_alert_confirm').modal('hide');
                });
            });
    //Thay đổi giá sản phẩm
    $(document).on('click', '.change_price_order', function (event) {
        $('#form_change_price')[0].reset();
        $('#change_price #price').autoNumeric('set', ($(this).autoNumeric('get')));
        $('#change_price #price_sale').autoNumeric('set', $(this).data('sale-vale'));
        $('.type-money,.type-pre').removeClass('active');
        if ($(this).data('sale-type') == '$') {
            $('.type-money').addClass('active');
        } else {
            $('.type-pre').addClass('active');
        }
        var $radios = $('input:radio[name=type_sale]');
        $radios.removeAttr('checked');
        $radios.filter('[value="' + $(this).data('sale-type') + '"]').attr('checked', true);
        $('#change_price #id_edit').val($(this).attr('id'));
        $('#change_price').modal();
    });
    //Thay đổi thuế sản phẩm
    $(document).on('click', '.change_tax_order', function (event) {
        $('#form_change_tax')[0].reset();
        $('#form_change_tax #tax_change').autoNumeric('set', ($(this).autoNumeric('get')));
        $('#form_change_tax #id_edit').val($(this).attr('id'));
        $('#change_tax').modal();
    });
    function change_tax() {
        var modal_tax = $('#form_change_tax #tax_change').autoNumeric('get');
        var id_tax = $('#form_change_tax #id_edit').val();
        $('#' + id_tax).autoNumeric('set', modal_tax);
        total_money_order();
    }
    $('#form_change_tax #tax_change').bind('keydown', function (event) {
        change_tax();
    });
    $('#form_change_tax #tax_change').bind('keyup', function (event) {
        change_tax();
    });
    //Thêm note cho sản phẩm
    $(document).on('click', '.add_note_product', function (event) {
        $('#form_add_note_product')[0].reset();
        $('#product_note #id_edit').val($(this).attr('id'));
        $('#product_note #product_note_detail').val($(this).parent().find("input[name='product_note_order[]']").val());
        $('#product_note').modal();
    });
    $('#product_note #product_note_detail').bind('keydown', function (event) {
        add_note_order();
    });
    $('#product_note #product_note_detail').bind('keyup', function (event) {
        add_note_order();
    });
    function add_note_order() {
        $('#' + $('#product_note #id_edit').val()).parent().find(".quicknote").html($('#product_note #product_note_detail').val().substring(0, 20) + '...');
        $('#' + $('#product_note #id_edit').val()).parent().find("input[name='product_note_order[]']").val($('#product_note #product_note_detail').val());
    }
    $(document).on('click', '#change_price .type-money,#change_price .type-pre', function (event) {
        $('input:radio[name=type_sale]').removeAttr('checked');
        var modal_price = $('#change_price #price').autoNumeric('get');
        var modal_price_sale = $('#change_price #price_sale').autoNumeric('get');
        var modal_price_type = $("#change_price input:radio[name=type_sale][checked]").val();
        if ($(this).hasClass('type-money')) {
            $('#change_price #price_sale').autoNumeric('set', parseFloat(parseFloat(modal_price) / 100 * parseFloat(modal_price_sale)));
            $('input:radio[name=type_sale][value="$"]').attr('checked', true);
        }
        if ($(this).hasClass('type-pre')) {
            $('#change_price #price_sale').autoNumeric('set', parseFloat(parseFloat(modal_price_sale) / parseFloat(modal_price) * 100));
            $('input:radio[name=type_sale][value="%"]').attr('checked', true);
        }
        change_money();
    });
    $('#change_price #price,#change_price #price_sale').bind('keydown', function (event) {
        change_money();
    });
    $('#change_price #price,#change_price #price_sale').bind('keyup', function (event) {
        change_money();
    });
    $(document).on('click', 'input.chang_thanh_toan', function (event) {
        if ($(this).val() == 1) {
            $(this).parents('div.col-xs-8').find('#thanh_toan_type').css('display', 'block');
        } else {
            $(this).parents('div.col-xs-8').find('#thanh_toan_type').css('display', 'none');
        }
    });
    function change_money() {
        var modal_price = $('#change_price #price').autoNumeric('get');
        var modal_price_sale = $('#change_price #price_sale').autoNumeric('get');
        var modal_price_type = $("#change_price input:radio[name=type_sale][checked]").val();
        $('#' + $('#change_price #id_edit').val()).data('sale-vale', modal_price_sale);
        var giamgia = 0;
        if (modal_price_type == '%') {
            giamgia = parseFloat(parseFloat(modal_price) / 100 * parseFloat(modal_price_sale));
            $('#' + $('#change_price #id_edit').val()).data('sale-type', '%');
        }
        if (modal_price_type == '$') {
            giamgia = parseFloat(modal_price_sale);
            $('#' + $('#change_price #id_edit').val()).data('sale-type', '$');
        }

        var cur_lable = 'VNĐ';
        $('#' + $('#change_price #id_edit').val()).parent().children("input[name='giam_order_des[]']").val(giamgia);
        $('#' + $('#change_price #id_edit').val()).parent().children('span.giam_order_des').html('Giảm :' + formatNumber(giamgia) + ' ' + cur_lable)
        $('#' + $('#change_price #id_edit').val()).attr('data-sale-type', modal_price_type);
        $('#' + $('#change_price #id_edit').val()).attr('data-sale-vale', modal_price_sale);
        $('#' + $('#change_price #id_edit').val()).autoNumeric('set', modal_price);
        total_money_order();
    }
    $(document).on('change', '.change_qty_order', function (event) {
        var max = $(this).attr('data-max');
        var curent = $(this).autoNumeric('get');
        if (parseInt(curent) > parseInt(max)) {
            $(this).parent().children('a').css('display', 'block');
        } else {
            $(this).parent().children('a').css('display', 'none');
        }
    });
    $(document).on('change', '#khach_hang_mua', function (event) {
//alert($(this).val());
    });
    $(document).keyup(function (event) {
        if (event.which == 120 || event.keyCode == 120) {
            $('#submit_order').trigger('click');
        }
        if (event.which == 119 || event.keyCode == 119) {
            $('#print_order').trigger('click');
        }
        if (event.which == 118 || event.keyCode == 118) {
            $('.add-contact').trigger('click');
        }
        if (event.which == 46 || event.keyCode == 46) {
            var id = $(".tabs_order").children().length;
            if (id > 2) {
                var id_tabs = $('.tabs_order li.active>span').trigger('click');
            }
        }
    });
    var bien_chon_thuoc_tinh = '';
    $(document).on('focus', '.chang_properties', function (event) {
        bien_chon_thuoc_tinh = $(this).val();
    });
    $(document).on('change', '.chang_properties', function (event) {
        var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
        var this_select_box = $(this);
        var this_value = $(this).val();
        var c_p_id = $(this).parents('tr').find("input[name='product_id_order[]']").val();
        var check = 0;
        $(id_tabs).find('section.banhang').find('#product_order_list').children('tr').each(function (i) {
            var p_id = $(this).find("input[name='product_id_order[]']").val();
            var td_value = $(this).find("select[name='properties_id_select[]']").val();
            if (c_p_id == p_id) {
                if (this_value == td_value) {
                    check++;
                }
            }
        });
        if (check > 1) {
            alert('da co roi');
            this_select_box.val(bien_chon_thuoc_tinh);
        }


        var soluong = $(this).find(':selected').data('soluong');
        var daban = $(this).find(':selected').data('daban');
        if (daban == null) {
            daban = 0;
        }
        $(this).parent().parent().find("a[data-toggle='tooltip']").attr('data-original-title', 'Max : ' + soluong + ' - Order : ' + daban + '');
        $(this).parent().parent().find("input[name='product_quatity[]']").attr('data-max', soluong);
        $(this).parent().parent().find("input[name='product_quatity[]']").attr('data-order', daban);
        var curent = $(this).parent().parent().find("input[name='product_quatity[]']").autoNumeric('get');
        if (parseInt(curent) > parseInt(soluong) - parseInt(daban)) {
            $(this).parent().parent().find("a[data-toggle='tooltip']").css('display', 'block');
        } else {
            $(this).parent().parent().find("a[data-toggle='tooltip']").css('display', 'none');
        }
    });
    $(document).on('change', '#currency_id_select', function (event) {
        total_money_order();
    });
    $(document).on('click', '#add_quick_customer', function (event) {
//  add_city();
    });
    $(document).on('change', '#city_select_id', function (event) {
        add_district($(this).val());
    });
    $(document).on('change', '#ma_giam_gia', function (event) {
        find_value_table('tbl_code', 'code', $(this).val());
    });
    $(document).on('change', '#user_partner', function (event) {
        find_user_table('tbl_user_partner_card', 'card_number', $(this).val());
    });
    $(document).on('change', '#khach_hang_mua', function (event) {
        var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
        $(id_tabs).find('#user_partner').val('');
    });
    $(document).on('click', '.delete_import_product', function (event) {
        var curent_del = $(this).parents('tr');
        $('#ajax_alert_confirm').modal('show').one('click', '#delete-confirm', function (e) {
            curent_del.remove();
            total_money_order();
            $('#ajax_alert_confirm').modal('hide');
        });
    });
});
function locdau(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g, "-");
    str = str.replace(/-+-/g, " "); //thay thế 2- thành 1-
    // str = str.replace(/^\-+|\-+$/g, "");//cắt bỏ ký tự - ở đầu và cuối chuỗi
    return str;
}

function order_search_build() {
    var list_table = [];
    var order_list = [];
    var order_detail_list = [];
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    var trans = db_index.transaction(list_table, 'readonly');
    trans.oncomplete = function (evt) {
        console.log(order_list);
    }
    var properties_detail = trans.objectStore('tbl_order');
    var cursorRequest_properties = properties_detail.openCursor();
    cursorRequest_properties.onerror = function (error) {
        console.log(error);
    };
    cursorRequest_properties.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            var cur_row = cursor.value;
            if (cur_row.status == '1') {
                order_list.push(cur_row);
            }
            cursor.continue();
        }
    };
    //Get branch
    var properties_detail = trans.objectStore('tbl_order_detail');
    var cursorRequest_properties = properties_detail.openCursor();
    cursorRequest_properties.onerror = function (error) {
        console.log(error);
    };
    cursorRequest_properties.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            var cur_row = cursor.value;
            order_detail_list.push(cur_row);
            cursor.continue();
        }
    };
}

//user is "finished typing," do something
function doneTyping(key_word) {
    key_word = key_word.trim();
    if (key_word.length > 2) {
        var item = [];
        var fruits = [];
        var list_table = [];
        var properties_list = [];
        var brach_list = [];
        $('.autocomplete_image').css('display', 'block');
        $.each(db_index.objectStoreNames, function (i, field) {
            list_table.push(field);
        });
        var trans = db_index.transaction(list_table, 'readonly');
        trans.oncomplete = function (evt) {
            if (item.length == 1) {
                var pro_item = item[0];
                var p_quan = parseInt(pro_item.quantity) + parseInt(pro_item.quantity_prom) + parseInt(pro_item.quantity_find);
                p_quan += parseInt(pro_item.quantity_retrieve) - parseInt(pro_item.quantity_sold);
                p_quan = p_quan - parseInt(pro_item.quantity_lost) - parseInt(pro_item.quantity_divide) - parseInt(pro_item.quantity_destroy);
                p_quan = p_quan - parseInt(pro_item.quantity_change) - parseInt(pro_item.quantity_give) - parseInt(pro_item.quantity_internal);
                var fruitsiem = {
                    value: pro_item.product_store_code + ' - ' + pro_item.name,
                    label: pro_item.name + ',' + pro_item.code + ',' + pro_item.product_store_code,
                    icon: pro_item.thumbnail,
                    id: pro_item.p_id,
                    name: pro_item.name,
                    store_id: pro_item.id,
                    code: pro_item.product_store_code,
                    price: pro_item.price,
                    price_wholes: pro_item.price_whole,
                    quantity: p_quan,
                    unit: pro_item.unit,
                    branch: pro_item.branches_id,
                    currency: pro_item.currency,
                    expired: pro_item.expired,
                    properties: pro_item.properties_id,
                    tax: pro_item.tax
                };
                pro_item = fruitsiem;
                var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
                var rowCount = $(id_tabs).find('section.banhang').find('#product_order_list').children('tr').length;
                var d = new Date();
                var n = d.getTime();
                var expir_select = '';
                if (pro_item.expired > 0) {
                    c_expired = new Date(pro_item.expired * 1000);
                    expir_select = c_expired.getDay() + '/' + c_expired.getMonth() + '/' + c_expired.getFullYear();
                }
                var html = '<tr>';
                html += '<td class="text-center"><img src="' + pro_item.icon + '"/><input type="hidden" name="product_select_id[]" value="' + pro_item.id + '">  <input type="hidden" name="product_id_store[]" value="' + pro_item.store_id + '"><input type="hidden" name="product_full_info[]" value=\'' + JSON.stringify(pro_item) + '\'></td>';
                html += '<td><input type="hidden" name="product_note_order[]" value=""><input type="hidden" name="product_event_sale[]" value=""><input type="hidden" name="product_name_order[]" value="' + pro_item.name + '">' + pro_item.value + '   <br/><a id="add_note_product_' + (n + 1) + '" class="add_note_product" style="font-size: 13px;position: relative;top: -4px;float: left;" href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i><span class="quicknote" style="font-size: 10px;color: #c0c0c0;"></span></a><span style="font-size: 10px;color: #FF5F5D;top: -6px;float: left;clear: both;position: relative;" class="giam_event_des"></span></td>';
                html += '<td> <input style="text-align: right;" class="form-control input-sm auto_number change_price_order" id="product_price_order_' + (n + 1) + '" readonly="true" name="product_price_order[]" data-price="' + pro_item.price + '" type="text" value="' + pro_item.price + '" data-sale-vale="0" data-sale-type="$"><span style="font-size: 10px;  color: #FF5F5D;position: relative;top: -3px;" class="giam_order_des"></span><input type="hidden" name="giam_order_des[]" value="0"></td>';
                html += '<td> ' + expir_select + '</td>';
                if (pro_item.properties) {
                    html += '<td> ' + pro_item.properties + '</td>';
                } else {
                    html += '<td></td>';
                }
                html += '<td><a style="float: left; margin-top: 5px; color: red;display: none"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Trong kho : ' + pro_item.quantity + '"><i class="fa fa-warning"></i></a><input style="float: left; margin-left: 10px; width: 79%;text-align: right;" class="form-control input-sm auto_number change_qty_order" onkeyup="total_money_order();"  onkeydown="total_money_order();" onchange="total_money_order();" onkeypress="total_money_order();" id="product_quatity_order_' + (rowCount + 1) + '" name="product_quatity[]" data-max="' + pro_item.quantity + '"  type="text" value="1"></td>';
                html += '<td>(' + pro_item.unit + ')</td>';
                html += '<td> <span class="thanh_tien_order" id="thanh_tien_order_' + (n + 1) + '">' + parseFloat(pro_item.price).format(2) + '</span></td>';
                html += '<td class="text-center"><input style="text-align: right;float: left;width: 80%;" type="text" readonly="true" id="product_tax_order_' + (n + 1) + '"  name="product_tax_order[]" class="form-control input-sm auto_number change_tax_order" value="' + pro_item.tax + '"><span style="line-height: 30px;">%</span></i></td>';
                html += '<td class="text-center"><i class="fa fa-trash-o delete_import_product"></i></td>';
                html += '</tr>';
                var check_exit = false;
                $(id_tabs).find('#product_order_list').find('tr').each(function (i) {
                    var product_id_store_check = $(this).find("input[name='product_id_store[]']").val();
                    var product_id_product_check = $(this).find("input[name='product_select_id[]']").val();
                    if (product_id_store_check == pro_item.store_id && product_id_product_check == pro_item.id) {
                        $(this).find("input[name='product_quatity[]']").autoNumeric('set', parseInt($(this).find("input[name='product_quatity[]']").autoNumeric('get')) + 1);
                        check_exit = true;
                    }
                });

                if (check_exit != true) {
                    $(id_tabs).find('section.banhang').find('#product_order_list').append(html);
                }
                $('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'});
                $('a[data-toggle=tooltip]').tooltip();
                $("#search_product_order").val('');
                total_money_order();
                if ($(id_tabs).find('#ma_giam_gia').val()) {
                    find_value_table('tbl_code', 'code', $(id_tabs).find('#ma_giam_gia').val());
                }
                find_event_sale(pro_item.id, pro_item.price);
                $('.autocomplete_image').css('display', 'none');
                $("#search_product_order_index").val('');
                return false;
            } else {
                $.each(item, function (y, field_item) {
                    var p_quan = parseInt(field_item.quantity) + parseInt(field_item.quantity_prom) + parseInt(field_item.quantity_find);
                    p_quan += parseInt(field_item.quantity_retrieve) - parseInt(field_item.quantity_sold);
                    p_quan = p_quan - parseInt(field_item.quantity_lost) - parseInt(field_item.quantity_divide) - parseInt(field_item.quantity_destroy);
                    p_quan = p_quan - parseInt(field_item.quantity_change) - parseInt(field_item.quantity_give) - parseInt(field_item.quantity_internal);
                    var fruitsiem = {
                        value: field_item.product_store_code + ' - ' + field_item.name,
                        label: field_item.name + ',' + field_item.code + ',' + field_item.product_store_code,
                        icon: field_item.thumbnail,
                        id: field_item.p_id,
                        name: field_item.name,
                        store_id: field_item.id,
                        code: field_item.product_store_code,
                        price: field_item.price,
                        price_wholes: field_item.price_whole,
                        quantity: p_quan,
                        unit: field_item.unit,
                        branch: field_item.branches_id,
                        currency: field_item.currency,
                        expired: field_item.expired,
                        properties: field_item.properties_id,
                        tax: field_item.tax
                    };
                    fruits.push(fruitsiem);
                });
                var accentMap = {
                    "à": "a",
                    "á": "a",
                    "ạ": "a",
                    "ả": "a",
                    "ã": "a",
                    "â": "a",
                    "ầ": "a",
                    "ấ": "a",
                    "ậ": "a",
                    "ẩ": "a",
                    "ẫ": "a",
                    "ă": "a",
                    "ằ": "a",
                    "ắ": "a",
                    "ặ": "a",
                    "ẳ": "a",
                    "ẵ": "a",
                    "ö": "o",
                    "è": "e",
                    "é": "e",
                    "ẹ": "e",
                    "ẻ": "e",
                    "ẽ": "e",
                    "ê": "e",
                    "ề": "e",
                    "ế": "e",
                    "ệ": "e",
                    "ể": "e",
                    "ễ": "e",
                    "ì": "i",
                    "í": "i",
                    "ị": "i",
                    "ỉ": "i",
                    "ĩ": "i",
                    "ò": "o",
                    "ó": "o",
                    "ọ": "o",
                    "ỏ": "o",
                    "õ": "o",
                    "ô": "o",
                    "ồ": "o",
                    "ố": "o",
                    "ộ": "o",
                    "ổ": "o",
                    "ỗ": "o",
                    "ơ": "o",
                    "ờ": "o",
                    "ớ": "o",
                    "ợ": "o",
                    "ở": "o",
                    "ỡ": "o",
                    "ù": "u",
                    "ú": "u",
                    "ụ": "u",
                    "ủ": "u",
                    "ũ": "u",
                    "ư": "u",
                    "ừ": "u",
                    "ứ": "u",
                    "ự": "u",
                    "ử": "u",
                    "ữ": "u",
                    "ỳ": "y",
                    "ý": "y",
                    "ỵ": "y",
                    "ỷ": "y",
                    "ỹ": "y",
                };
                var normalize = function (term) {
                    var ret = "";
                    for (var i = 0; i < term.length; i++) {
                        ret += accentMap[ term.charAt(i) ] || term.charAt(i);
                    }
                    return ret;
                };
                $("#search_product_order_index").autocomplete({
                    minLength: 2,
                    source: function (request, response) {
                        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                        response($.grep(fruits, function (value) {
                            value = value.label || value.value || value;
                            return matcher.test(value) || matcher.test(normalize(value));
                        }));
                    },
                    focus: function (event, ui) {
                        $("#search_product_order_index").val('');
                        return false;
                    },
                    select: function (event, ui) {
                        var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
                        var rowCount = $(id_tabs).find('section.banhang').find('#product_order_list').children('tr').length;
                        var d = new Date();
                        var n = d.getTime();
                        var expir_select = '';
                        if (ui.item.expired > 0) {
                            c_expired = new Date(ui.item.expired * 1000);
                            expir_select = c_expired.getDay() + '/' + c_expired.getMonth() + '/' + c_expired.getFullYear();
                        }
                        var html = '<tr>';
                        html += '<td class="text-center"><img src="' + ui.item.icon + '"/><input type="hidden" name="product_select_id[]" value="' + ui.item.id + '">  <input type="hidden" name="product_id_store[]" value="' + ui.item.store_id + '"><input type="hidden" name="product_full_info[]" value=\'' + JSON.stringify(ui.item) + '\'></td>';
                        html += '<td><input type="hidden" name="product_note_order[]" value=""><input type="hidden" name="product_event_sale[]" value=""><input type="hidden" name="product_name_order[]" value="' + ui.item.name + '">' + ui.item.value + '   <br/><a id="add_note_product_' + (n + 1) + '" class="add_note_product" style="font-size: 13px;position: relative;top: -4px;float: left;" href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i><span class="quicknote" style="font-size: 10px;color: #c0c0c0;"></span></a><span style="font-size: 10px;color: #FF5F5D;top: -6px;float: left;clear: both;position: relative;" class="giam_event_des"></span></td>';
                        html += '<td> <input style="text-align: right;" class="form-control input-sm auto_number change_price_order" id="product_price_order_' + (n + 1) + '" readonly="true" name="product_price_order[]" data-price="' + ui.item.price + '" type="text" value="' + ui.item.price + '" data-sale-vale="0" data-sale-type="$"><span style="font-size: 10px;  color: #FF5F5D;position: relative;top: -3px;" class="giam_order_des"></span><input type="hidden" name="giam_order_des[]" value="0"></td>';
                        html += '<td> ' + expir_select + '</td>';
                        if (ui.item.properties) {
                            html += '<td> ' + ui.item.properties + '</td>';
                        } else {
                            html += '<td></td>';
                        }
                        html += '<td><a style="float: left; margin-top: 5px; color: red;display: none"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Trong kho : ' + ui.item.quantity + '"><i class="fa fa-warning"></i></a><input style="float: left; margin-left: 10px; width: 79%;text-align: right;" class="form-control input-sm auto_number change_qty_order" onkeyup="total_money_order();"  onkeydown="total_money_order();" onchange="total_money_order();" onkeypress="total_money_order();" id="product_quatity_order_' + (rowCount + 1) + '" name="product_quatity[]" data-max="' + ui.item.quantity + '"  type="text" value="1"></td>';
                        html += '<td>(' + ui.item.unit + ')</td>';
                        html += '<td> <span class="thanh_tien_order" id="thanh_tien_order_' + (n + 1) + '">' + parseFloat(ui.item.price).format(2) + '</span></td>';
                        html += '<td class="text-center"><input style="text-align: right;float: left;width: 80%;" type="text" readonly="true" id="product_tax_order_' + (n + 1) + '"  name="product_tax_order[]" class="form-control input-sm auto_number change_tax_order" value="' + ui.item.tax + '"><span style="line-height: 30px;">%</span></i></td>';
                        html += '<td class="text-center"><i class="fa fa-trash-o delete_import_product"></i></td>';
                        html += '</tr>';
                        var check_exit = false;
                        $(id_tabs).find('#product_order_list').find('tr').each(function (i) {
                            var product_id_store_check = $(this).find("input[name='product_id_store[]']").val();
                            var product_id_product_check = $(this).find("input[name='product_select_id[]']").val();
                            if (product_id_store_check == ui.item.store_id && product_id_product_check == ui.item.id) {
                                $(this).find("input[name='product_quatity[]']").autoNumeric('set', parseInt($(this).find("input[name='product_quatity[]']").autoNumeric('get')) + 1);
                                check_exit = true;
                            }
                        });

                        if (check_exit != true) {
                            $(id_tabs).find('section.banhang').find('#product_order_list').append(html);
                        }
                        $('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'});
                        $('a[data-toggle=tooltip]').tooltip();
                        $("#search_product_order").val('');
                        total_money_order();
                        if ($(id_tabs).find('#ma_giam_gia').val()) {
                            find_value_table('tbl_code', 'code', $(id_tabs).find('#ma_giam_gia').val());
                        }
                        find_event_sale(ui.item.id, ui.item.price);
                        return false;
                    }
                }
                ).autocomplete("instance")._renderItem = function (ul, item) {
                    var cur_name = '';
                    if (item.icon) {
                        var resimg = item.icon.split(",");
                    } else {
                        var resimg = [''];
                    }
                    var expir = '';
                    if (item.expired > 0) {
                        var c_expired = new Date(item.expired * 1000);
                        expir = " <strong class='lable-green'> Hạn dùng</strong> :<b class='lable-green'>" + c_expired.getDay() + '/' + c_expired.getMonth() + '/' + c_expired.getFullYear() + '</b>';
                    }
                    var prorpe = '';
                    if (item.properties) {
                        var res_properties = item.properties.split(",");
                        var prop = '';
                        $.each(res_properties, function (i_p, field_properties) {
                            var c_properties = properties_list.filter(function (obj2s) {
                                return parseInt(obj2s.p_p_id) === parseInt(field_properties);
                            })[0];
                            if (c_properties) {
                                if (typeof c_properties.name != 'undefined')
                                    prop += c_properties.name + ',';
                            }
                        });
                        item.properties = prop.slice(0, -1);
                        prorpe = " <strong class='lable-green'> Thuộc tính</strong> :<b class='lable-green'>" + item.properties + '</b>';
                    }

                    var brach_item = '';
                    if (item.branch) {
                        var c_brach = brach_list.filter(function (obj2s) {
                            return parseInt(obj2s.id) === parseInt(item.branch);
                        })[0];
                        brach_item = " <strong class='lable-green'> Chi nhánh</strong> :<b class='lable-green'>" + c_brach.name + '</b>';
                    }
                    return $("<li>")
                            .append('<img src="' + item.icon + '"/><span>' + item.value + "</span><span><strong class='lable-green'>Giá</strong> :<b class='lable-green'>" + formatNumber(parseFloat(item.price)) + '</b> ' + " <strong class='lable-green'>Trong kho</strong> :<b class='lable-green'>" + item.quantity + '</b> ' + prorpe + expir + brach_item + "</span></a>")
                            .appendTo(ul);
                };
                $("#search_product_order_index").autocomplete("search", key_word);
                $('.autocomplete_image').css('display', 'none');
            }
        }
//Get Properties
        var properties_detail = trans.objectStore('tbl_product_properties_detail_lang');
        var cursorRequest_properties = properties_detail.openCursor();
        cursorRequest_properties.onerror = function (error) {
            console.log(error);
        };
        cursorRequest_properties.onsuccess = function (evt) {
            var cursor = evt.target.result;
            if (cursor) {
                var cur_row = cursor.value;
                properties_list.push(cur_row);
                cursor.continue();
            }
        };
        //Get branch

        //Get branch
        var properties_detail = trans.objectStore('tbl_product_branches');
        var cursorRequest_properties = properties_detail.openCursor();
        cursorRequest_properties.onerror = function (error) {
            console.log(error);
        };
        cursorRequest_properties.onsuccess = function (evt) {
            var cursor = evt.target.result;
            if (cursor) {
                var cur_row = cursor.value;
                brach_list.push(cur_row);
                cursor.continue();
            }
        };
        var store = trans.objectStore('tbl_product_search');
        var cursorRequest = store.openCursor();
        cursorRequest.onerror = function (error) {
            console.log(error);
        };
        cursorRequest.onsuccess = function (evt) {
            var cursor = evt.target.result;
            if (cursor) {
                var cur_row = cursor.value;
                var check_search = false;
                if (cur_row.product_store_code.indexOf(key_word) !== -1) {
                    check_search = true;
                }
                if (typeof cur_row.code != "undefined" && cur_row.code != null) {
                    if (cur_row.code.indexOf(key_word) !== -1) {
                        check_search = true;
                    }
                }
                if (typeof cur_row.name != "undefined") {
                    if (locdau(cur_row.name).indexOf(locdau(key_word)) !== -1) {
                        check_search = true;
                    }
                }
                if (check_search == true) {
                    if (item.length < 50) {
                        item.push(cur_row);
                        var thumb = '';
                        var store_img = trans.objectStore('tbl_image');
                        var cursorRequestLocal = store_img.index('thumbnail').get(cur_row.thumbnail);
                        cursorRequestLocal.onerror = function (error) {
                            console.log(error);
                        };
                        cursorRequestLocal.onsuccess = function (evt) {
                            var cursor = evt.target.result;
                            if (cursor) {
                                cur_row.thumbnail = cursor.data;
                            }
                        }
                    }
                }
                cursor.continue();
            }
        };
    } else {
        console.log('<3');
    }
}

function find_value_table(table, index, key) {
    var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
    var list_product_event = [];
    $(id_tabs).find('#product_order_list').find('tr').each(function (i) {
        list_product_event.push($(this).find("input[name='product_select_id[]']").val());
        $(this).find("input[name='giam_order_des[]']").val('0');
        $(this).find('span.giam_order_des').html('')
    });
    var code_done = [];
    var coupon_done = [];
    var product_done = [];
    var cate_done = [];
    var cate_view_done = [];
    var list_table = [];
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    var trans = db_index.transaction(list_table, 'readonly');
    trans.oncomplete = function (evt) {
        if (code_done.length != 0) {
            var html_insert = '';
            html_insert += '<input type="hidden" name="coupon_detail_code"  value="' + code_done.code + '">';
            html_insert += '<input type="hidden" name="coupon_detail_type"  value="' + coupon_done.type + '">';
            html_insert += '<input type="hidden" name="coupon_detail_coupons_exp_date"  value="' + coupon_done.coupons_exp_date + '">';
            if (coupon_done.type == 1) {
                html_insert += '<input type="hidden" name="coupon_detail_coupons_discount"  value="' + coupon_done.coupons_discount + '">';
                html_insert += '<input type="hidden" name="coupon_detail_discount_type"  value="' + coupon_done.discount_type + '">';
                html_insert += '<input type="hidden" name="coupon_detail_coupons_limit_discount"  value="' + coupon_done.coupons_limit_discount + '">';
                html_insert += '<input type="hidden" name="coupon_detail_coupons_condition"  value="' + coupon_done.coupons_condition + '">';
                html_insert += '<input type="hidden" name="coupon_detail_product"  value="' + coupon_done.p_id_list + '">';
                html_insert += 'Tên mã : ' + coupon_done.name;
                html_insert += '<br/>Mã giảm giá : ' + code_done.code;
                if (coupon_done.discount_type == 2) {
                    html_insert += '<br/>Chết khấu : ' + formatNumber(coupon_done.coupons_discount) + '%';
                }
                if (coupon_done.discount_type == 1) {
                    html_insert += '<br/>Chết khấu : ' + formatNumber(coupon_done.coupons_discount) + 'VNĐ';
                }
                html_insert += '<br/>Loại mã : Mã giảm giá';
                if (coupon_done.coupons_limit_discount != 0) {
                    html_insert += '<br/>Giới hạn giảm giá : ' + formatNumber(coupon_done.coupons_limit_discount) + 'VNĐ';
                }
                if (coupon_done.coupons_condition != 0) {
                    html_insert += '<br/>Điều kiện áp dụng : ' + formatNumber(coupon_done.coupons_condition) + 'VNĐ';
                }
                if (coupon_done.p_type == 1) {
                    if (coupon_done.p_id_list != '') {
                        var html_pro = '';
                        $.each(product_done, function (i, field_p) {
                            html_pro += '<tr><tr><td>' + field_p.code + '</td><td>' + field_p.name + '</td></tr>'
                        });
                        html_insert += '<br/>Sản phẩm áp dụng : <a href="javascript:;" data-toggle="popover_fix" data-html="true" data-placement="left" data-content="<table class=\'table\'><thead> <tr> <th>Mã SP</th> <th>TÊN SP</th> </tr> </thead><tbody>' + html_pro + '</tbody></table>" title="" data-original-title="Bảng sản phẩm được giảm giá">Chi tiết</a>';
                    }
                }
                if (coupon_done.p_type == 0) {
                    if (coupon_done.p_id_list != '') {
                        html_insert += '<br/>Danh mục áp dụng : ' + cate_done.name;
                    }
                }
                html_insert += '<br/>Hạn sử dụng : ' + short_timeConverter(coupon_done.coupons_exp_date);
                $(id_tabs).find('.coupons_div_view_detail').html(html_insert);
            }
            if (coupon_done.type == 0) {
                html_insert += '<input type="hidden" name="coupon_detail_value"  value="' + coupon_done.value + '">';
                html_insert += 'Tên mã : ' + coupon_done.name;
                html_insert += '<br/>Mã giảm giá : ' + code_done.code;
                html_insert += '<br/>Giảm giá : ' + formatNumber(coupon_done.value) + 'VNĐ';
                html_insert += '<br/>Loại mã : Mã quà tặng';
                html_insert += '<br/>Hạn sử dụng : ' + short_timeConverter(coupon_done.coupons_exp_date);
//                var ex_rate_select = $(id_tabs).find("#currency_id_select").find('option:selected').data('exchane');
//                var quy_doi = coupon_done.value / parseFloat(ex_rate_select);
//                $(id_tabs).find("#giam_gia").autoNumeric('set', quy_doi);
                $(id_tabs).find('.coupons_div_view_detail').html(html_insert);
            }
            var d = new Date();
            var n = d.getTime();
            if (n < coupon_done.coupons_exp_date * 1000) {
                $(id_tabs).find('#product_order_list').find('tr').each(function (i) {
                    var this_tr = $(this);
                    var c_price = this_tr.find("input[name='product_price_order[]']").autoNumeric('get');
                    var t_giam = $(this).find("input[name='giam_order_des[]']");
                    if (coupon_done.discount_type == 2) {
                        var c_price_sale = parseFloat(this_tr.find("input[name='product_price_order[]']").autoNumeric('get')) / 100 * parseFloat(coupon_done.coupons_discount);
                    }
                    if (coupon_done.discount_type == 1) {
                        var c_price_sale = parseFloat(coupon_done.coupons_discount);
                    }
                    if (cate_view_done.indexOf(parseInt($(this).find("input[name='product_select_id[]']").val())) != -1) {
                        t_giam.val(c_price_sale);
                        this_tr.find('span.giam_order_des').html('Giảm :' + formatNumber(c_price_sale) + ' VNĐ')
                    }
                });
                total_money_order();
            }
        } else {
            $(id_tabs).find('#ma_giam_gia').val('');
            $(id_tabs).find('.coupons_div_view_detail').html('Mã đã được sử dụng hoặc hết hạn !');
        }
        $("[data-toggle=popover_fix]").popover({trigger: "click"});
    }
    var store_local = trans.objectStore(table);
    var store_coupon = trans.objectStore('tbl_coupons');
    var product_coupon = trans.objectStore('tbl_product_search');
    var cate_coupon = trans.objectStore('tbl_product_category');
    var cate_view_coupon = trans.objectStore('tbl_product_views');
    var cursorRequestLocal = store_local.index(index).get(key);
    cursorRequestLocal.onerror = function (error) {
        console.log(error);
    };
    cursorRequestLocal.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            if (cursor.status != 1) {
                code_done = cursor;
                var cursorRequestCoupon = store_coupon.index('id').get(cursor.id_destination);
                cursorRequestCoupon.onsuccess = function (evt_cp) {
                    var cursor_coupon = evt_cp.target.result;
                    coupon_done = cursor_coupon;
                    if (coupon_done.p_type == 1) {
                        if (coupon_done.p_id_list != '') {
                            var id_list = coupon_done.p_id_list;
                            $.each(id_list.split(","), function (i, field_p) {
                                if (field_p) {
                                    cate_view_done.push(parseInt(field_p));
                                    var cursorRequestproduct = product_coupon.index('p_id').get(parseInt(field_p));
                                    cursorRequestproduct.onsuccess = function (evt_pro) {
                                        var cursor_product = evt_pro.target.result;
                                        product_done.push(cursor_product);
                                    }
                                }
                            });
                        }
                    }
                    if (coupon_done.p_type == 0) {
                        if (coupon_done.p_id_list != '') {
                            var cursorRequestcate = cate_coupon.index('id').get(parseInt(coupon_done.p_id_list));
                            cursorRequestcate.onsuccess = function (evt_cate) {
                                var cursor_cate = evt_cate.target.result;
                                cate_done = (cursor_cate);
                            }
                            var cursorRequestcateview = cate_view_coupon.openCursor();
                            cursorRequestcateview.onsuccess = function (evt_cate_view) {
                                var cursor = evt_cate_view.target.result;
                                if (cursor) {
                                    if (cursor.value.cat_id == coupon_done.p_id_list) {
                                        cate_view_done.push(cursor.value.product_id);
                                    }
                                    cursor.continue();
                                }
                            }
                        }
                    }
                }

            }
        }
    }

}

function find_user_table(table, index, key) {
    var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
    $(id_tabs).find('#customer_info').html('');
    var card_user_done = [];
    var user_done = [];
    var partner_done = [];
    var list_table = [];
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    var trans = db_index.transaction(list_table, 'readonly');
    trans.oncomplete = function (evt) {
        var html_insert = '';
        html_insert += '<input type="hidden" name="partner_card_discount"  value="' + partner_done.discount + '">';
        if (navigator.onLine) {
            if (user_done.avatar) {
                html_insert += '<div class="customer_image"><img src="' + user_done.avatar + '" /> </div>';
            } else {
                html_insert += '<div class="customer_image"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAABZUlEQVRYR+2VT4uCUBTFj0YLE7JFuHCT6MbcSN//W5RGq5ZBRKgL7Y+aw30wMczQ8PJCRry7fNzDO/7ueVcty7IWb1yaMsicjiLIBAhFUBHkEuDqVQYVQS4Brv4zM9i2LU6nE263mxQgXddhGAY0TZPq/9nUieDlckEcxxgMBlIXNk2DIAhgmqZUP9vg+XxGkiRYLBb4pkmXPyK0XC7hui7G4/HrDW63W+R5jtlshul0KgzUdY39fg/HcYTpXg3udjscDgf4vo/hcAiiezwehWk6m0wm/RqkB0BjJmqbzQbX6/U+RjIchiHW63V/Iy6KAlVViZGWZfknY7ZtI03TfgxGUSSo0at+VJZlCeO9PBLP80TW/ivaf0T3pQZppKvVSmRPtubzOUajkWz7va/ToiY1Ld9n/iSyS/33F3Q2+DSKjgJlsCM4fga5F8vq1YhlST3qUwQVQS4Brl5lUBHkEuDq3z6DX0RYOpA/abG9AAAAAElFTkSuQmCC" /> </div>';
            }
        } else {
            html_insert += '<div class="customer_image"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAABZUlEQVRYR+2VT4uCUBTFj0YLE7JFuHCT6MbcSN//W5RGq5ZBRKgL7Y+aw30wMczQ8PJCRry7fNzDO/7ueVcty7IWb1yaMsicjiLIBAhFUBHkEuDqVQYVQS4Brv4zM9i2LU6nE263mxQgXddhGAY0TZPq/9nUieDlckEcxxgMBlIXNk2DIAhgmqZUP9vg+XxGkiRYLBb4pkmXPyK0XC7hui7G4/HrDW63W+R5jtlshul0KgzUdY39fg/HcYTpXg3udjscDgf4vo/hcAiiezwehWk6m0wm/RqkB0BjJmqbzQbX6/U+RjIchiHW63V/Iy6KAlVViZGWZfknY7ZtI03TfgxGUSSo0at+VJZlCeO9PBLP80TW/ivaf0T3pQZppKvVSmRPtubzOUajkWz7va/ToiY1Ld9n/iSyS/33F3Q2+DSKjgJlsCM4fga5F8vq1YhlST3qUwQVQS4Brl5lUBHkEuDq3z6DX0RYOpA/abG9AAAAAElFTkSuQmCC" /> </div>';
        }
        html_insert += ' <div class="customer_info">';
        html_insert += 'Họ tên : ' + user_done.full_name;
        html_insert += '<br/>Địa chỉ : ' + user_done.address;
        html_insert += '<br/>Sđt : ' + user_done.phone;
        html_insert += '<br/>Email : ' + user_done.email;
        if (partner_done.id) {
            html_insert += '<br/>Giảm giá : ' + formatNumber(partner_done.discount) + '%';
        }
        html_insert += '<br/>Điểm tích tài khoản : ' + formatNumber(user_done.point);
        html_insert += '<br/>Hạng thẻ : ' + partner_done.name;
        html_insert += '<br/>Tích lũy nâng thẻ : ' + formatNumber(card_user_done.use_point);
        html_insert += '  </div>';
        if (user_done.id) {
            $(id_tabs).find('#customer_info').html(html_insert);
        } else {
            $(id_tabs).find('#customer_info').html('Thông tin thẻ không hợp lệ !');
        }
        var check_option = 0;
        $(id_tabs).find('#khach_hang_mua option').each(function () {
            if ($(this).val() == user_done.id) {
                check_option = 1;
            }
        });
        if (check_option == 1) {
            $(id_tabs).find('#khach_hang_mua').val(user_done.id);
            $(id_tabs).find('#khach_hang_mua').select2();
        }
        total_money_order();
    }
    var store_local = trans.objectStore(table);
    var cursorRequestLocal = store_local.index(index).get(key);
    cursorRequestLocal.onerror = function (error) {
        console.log(error);
    };
    cursorRequestLocal.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            if (cursor.status == 1) {
                card_user_done = (cursor);
            }
        }
    }
    var store_partner = trans.objectStore('tbl_users');
    var cursorRequestLocal = store_partner.openCursor();
    cursorRequestLocal.onerror = function (error) {
        console.log(error);
    };
    cursorRequestLocal.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {

            if (cursor.value.id == card_user_done.user_id) {
                user_done = cursor.value;
            }
            cursor.continue();
        }
    }
    var store_partner = trans.objectStore('tbl_partner');
    var cursorRequestLocal = store_partner.openCursor();
    cursorRequestLocal.onerror = function (error) {
        console.log(error);
    };
    cursorRequestLocal.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            if (cursor.value.id == card_user_done.partner_id) {
                partner_done = cursor.value;
            }
            cursor.continue();
        }
    }
}

function add_city() {
    var list_city = [];
    var list_table = [];
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    var trans = db_index.transaction(list_table, 'readonly');
    trans.oncomplete = function (evt) {
        var option_city_select = '';
        $.each(list_city, function (i, city_item) {
            if (i == 0) {
                add_district(city_item.id)
            }
            option_city_select += '<option value="' + city_item.id + '">' + city_item.name + '</option>';
        });
        $('#city_select_id').html(option_city_select);
        $('#select_district_id').html('');
        $('#city_select_id,#select_district_id').select2();
    }
    var store_local = trans.objectStore('tbl_location');
    var cursorRequestLocal = store_local.openCursor();
    cursorRequestLocal.onerror = function (error) {
        console.log(error);
    };
    cursorRequestLocal.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            if (cursor.value.parent == 0) {
                list_city.push(cursor.value);
            }
            cursor.continue();
        }
    }
}
function add_district(city_id) {
    var list_district = [];
    var list_table = [];
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    var trans = db_index.transaction(list_table, 'readonly');
    trans.oncomplete = function (evt) {
        var option_district_select = '';
        $.each(list_district, function (i, city_item) {
            option_district_select += '<option value="' + city_item.id + '">' + city_item.name + '</option>';
        });
        $('#select_district_id').html(option_district_select);
        $('#select_district_id').select2();
    }
    var store_local = trans.objectStore('tbl_location');
    var cursorRequestLocal = store_local.index('parent').openCursor();
    cursorRequestLocal.onerror = function (error) {
        console.log(error);
    };
    cursorRequestLocal.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            if (cursor.value.parent == city_id) {
                list_district.push(cursor.value);
            }
            cursor.continue();
        }

    }
}
function add_user_to_input() {
    var user_list = [];
    var admin_list = [];
    var list_table = [];
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    var trans = db_index.transaction(list_table, 'readonly');
    trans.oncomplete = function (evt) {
        var option_user_select = '   <option data-code=""  data-location=""  data-gender="" data-partner="" value="0">Khách vãng lai</option>';
        $.each(user_list, function (i, user_item) {
            option_user_select += '<option data-code="' + user_item.code + '" data-location="' + user_item.location + '" data-gender="' + user_item.gender + '" data-partner="' + user_item.partner_id + '" value="' + user_item.id + '">' + user_item.full_name + ' - ' + user_item.phone + '</option>';
        });
        var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
        $(id_tabs).find('#khach_hang_mua').html(option_user_select);
        $(id_tabs).find('#khach_hang_mua').select2();

        var option_user_select = '   <option data-code=""  data-location=""  data-gender="" data-partner="" value="0">Lựa chọn</option>';
        $.each(user_list, function (i, user_item) {
            option_user_select += '<option data-code="' + user_item.code + '" data-location="' + user_item.location + '" data-gender="' + user_item.gender + '" data-partner="' + user_item.partner_id + '" value="' + user_item.id + '">' + user_item.full_name + ' - ' + user_item.phone + '</option>';
        });
        $(id_tabs).find('#nguoi_gioi_thieu').html(option_user_select);
        $(id_tabs).find('#nguoi_gioi_thieu').select2();
    }
    var store = trans.objectStore('tbl_users');
    var cursorRequest = store.openCursor();
    cursorRequest.onerror = function (error) {
        console.log(error);
    };
    cursorRequest.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            if (cursor.value.status == 1) {
                user_list.push(cursor.value);
            }
            if (cursor.value.admin == 1) {
                admin_list.push(cursor.value);
            }
            cursor.continue();
        }
    }

}
function add_data_to_alert(table_select, id_select) {
    var data_return = [];
    var list_table = [];
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    var trans = db_index.transaction(list_table, 'readonly');
    trans.oncomplete = function (evt) {
        var option_user_select = "<table class='table'><thead> <tr> <th>Mã NT</th> <th>";
        $.each(data_return, function (i, user_item) {
            if (user_item.id == d_currency) {
                option_user_select += user_item.name + "</th> </tr> </thead><tbody>";
            }
        });
        $.each(data_return, function (i, user_item) {
            if (user_item.id != d_currency) {
                option_user_select += "<tr><td style='white-space:nowrap;'>1 " + user_item.name + "</td><td  style='white-space:nowrap;'>" + formatNumber(parseFloat(user_item.ex_rate)) + '</td></tr>';
            }
        });
        option_user_select += "</tbody></table>";
        $('.tabs_order_content  ' + id_select).each(function () {
            $(this).attr('data-content', option_user_select);
        });
    }
    var store = trans.objectStore(table_select);
    var cursorRequest = store.openCursor();
    cursorRequest.onerror = function (error) {
        console.log(error);
    };
    cursorRequest.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            data_return.push(cursor.value);
            cursor.continue();
        }
    }

}
function add_data_to_curremcy(table_select, id_select) {
    var data_return = [];
    var list_table = [];
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    var trans = db_index.transaction(list_table, 'readonly');
    trans.oncomplete = function (evt) {
        var option_user_select = '';
        $.each(data_return, function (i, user_item) {
            var c_selected = '';
            if (user_item.id == d_currency) {
                c_selected = 'selected';
            }
            option_user_select += '<option value="' + user_item.id + '" data-exchane="' + user_item.ex_rate + '" ' + c_selected + '>' + user_item.name + '</option>';
        });
        $('.tabs_order_content  ' + id_select).each(function () {
            $(this).html(option_user_select);
        });
    }
    var store = trans.objectStore(table_select);
    var cursorRequest = store.openCursor();
    cursorRequest.onerror = function (error) {
        console.log(error);
    };
    cursorRequest.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            data_return.push(cursor.value);
            cursor.continue();
        }
    }

}
function add_data_to_select(table_select, id_select) {
    var data_return = [];
    var list_table = [];
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    var trans = db_index.transaction(list_table, 'readonly');
    trans.oncomplete = function (evt) {
        var option_user_select = '   <option value="0">Lựa chọn</option>';
        $.each(data_return, function (i, user_item) {
            option_user_select += '<option value="' + user_item.id + '">' + user_item.name + '</option>';
        });
        $('.tabs_order_content  ' + id_select).each(function () {
            $(this).html(option_user_select);
        });
    }
    var store = trans.objectStore(table_select);
    var cursorRequest = store.openCursor();
    cursorRequest.onerror = function (error) {
        console.log(error);
    };
    cursorRequest.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            data_return.push(cursor.value);
            cursor.continue();
        }
    }

}
$(document).on('click', '#add_quick_event', function (event) {
    var cr_tabs = $('.tabs_order li.active>a[data-toggle="tab"]');
    var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
    var list_product_event = [];
    $(id_tabs).find('#product_order_list').find('tr').each(function (i) {
        list_product_event.push($(this).find("input[name='product_select_id[]']").val());
    });
    var localtion = $(id_tabs).find('#khach_hang_mua').find(':selected').data('location');
    var branch = current_user.branches_id;
    var recommend = $(id_tabs).find('#nguoi_gioi_thieu').val();
    var gender = $(id_tabs).find('#khach_hang_mua').find(':selected').data('gender');
    var total_pay = total_money_order();
    var part_select = $(id_tabs).find('#nguoi_gioi_thieu').val();
    add_event_apply(localtion, branch, recommend, gender, total_pay, part_select, list_product_event);
});
function add_event_apply(localtion, branch, recommend, gender, total_pay, part_select, list_product_event) {
    var event_list = [];
    var list_table = [];
    var list_partner = [];
    var branch_list = [];
    var localtion_list = [];
    var category_list = [];
    var product_view_list = [];
    var product_list = [];
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    $.each(db_index.objectStoreNames, function (i, field) {
        list_table.push(field);
    });
    var trans = db_index.transaction(list_table, 'readonly');
    trans.oncomplete = function (evt) {
        var html = '';
        var coutn = 1;
        $.each(event_list, function (i, event_item) {
            var check_current_event = false;
            var check_active_event = true;
            var html_event = '';
            html_event += '<input type="hidden" name="event_product_type"  value="' + event_item.type + '"><input type="hidden" name="all_event"  value=\'' + JSON.stringify(event_item) + '"><input type="hidden" name="event_type"  value="' + event_item.product_id + '\'></td>\n\
<td >';
            if (event_item.type == 1) {
                html_event += 'Sản phẩm :';
                var msg = $.parseJSON(event_item.product_id);
                if (msg) {
                    $.each(msg, function (i, field) {
                        var ev_check = 'bg-danger';
                        if (list_product_event) {
                            if (list_product_event.indexOf(field.id.toString()) != -1) {
                                ev_check = 'bg-primary';
                                check_current_event = true;
                            } else {
                                check_active_event = false;
                            }
                        }
                        html_event += '<div class="label ' + ev_check + '">' + field.name + '</div> ';
                    });
                }
            }
            if (event_item.type == 0) {
                html_event += 'Chuyên mục :';
                var msg = $.parseJSON(event_item.product_id);
                if (msg) {
                    var ev_check = 'bg-danger';
                    var curent_cate = [];
                    $.each(product_view_list, function (i, field) {
                        if (list_product_event.indexOf(field.product_id.toString()) !== -1) {
                            curent_cate.push(field.cat_id);
                        }
                    });
                    var event_cate = [];
                    $.each(category_list, function (i, field) {
                        if (msg.id == field.id || msg.id == field.parent) {
                            event_cate.push(field.id);
                        }
                    });
                    console.log(curent_cate);
                    if (compareArrays(curent_cate, event_cate) && curent_cate.length != 0) {
                        check_current_event = true;
                        ev_check = 'bg-primary';
                    } else {
                        check_active_event = false;
                    }
                    html_event += '<div class="label ' + ev_check + '">' + msg.name + '</div> ';
                }
            }

            html_event += '<br/>(' + event_item.name + ')</td><td class="event_dk"><ul>';

            if (event_item.gender == '1') {
                var ev_check = 'bg-danger';
                if (event_item.gender == gender) {
                    ev_check = 'bg-primary';
                } else {
                    check_active_event = false;
                }
                html_event += '<li>Giới tính : <div class="label ' + ev_check + '">Nam</div></li>';
            }
            if (event_item.gender == '2') {
                var ev_check = 'bg-danger';
                if (event_item.gender == gender) {
                    ev_check = 'bg-primary';
                } else {
                    check_active_event = false;
                }
                html_event += '<li>Giới tính : <div class="label ' + ev_check + '">Nữ</div></li>';
            }
            if (event_item.total_pay > 0) {
                var ev_check = 'bg-danger';
                if (event_item.total_pay <= total_pay) {
                    ev_check = 'bg-primary';
                } else {
                    check_active_event = false;
                }
                html_event += '<li>Tổng đơn hàng trên : <div class="label ' + ev_check + '">' + formatNumber(parseFloat(event_item.total_pay)) + '</div></li>';
            }
            if (event_item.recommend == '1') {
                var ev_check = 'bg-danger';
                if (recommend != '0') {
                    ev_check = 'bg-primary';
                } else {
                    check_active_event = false;
                }
                html_event += '<li>Người giới thiệu : <div class="label ' + ev_check + '">Có</div></li>';
            }
            if (event_item.branch != '0') {
                var c_brach = branch_list.filter(function (obj2s) {
                    return parseInt(obj2s.id) === parseInt(event_item.branch);
                })[0];
                var ev_check = 'bg-danger';
                if (event_item.branch == branch) {
                    ev_check = 'bg-primary';
                } else {
                    check_active_event = false;
                }
                html_event += '<li>Chi nhánh :<div class="label ' + ev_check + '">' + c_brach.name + '</div></li>';
            }
            if (event_item.vip_card != '') {
                var ev_partner = '';
                if (list_partner) {
                    var res_partner = event_item.vip_card.split(",");
                    $.each(res_partner, function (i_p, field_partner) {
                        var c_partner = list_partner.filter(function (obj2s) {
                            return parseInt(obj2s.id) === parseInt(field_partner);
                        })[0];
                        if (c_partner) {
                            if (typeof c_partner.name != 'undefined') {
                                var ev_check = 'bg-danger';
                                if (c_partner.id == part_select) {
                                    ev_check = 'bg-primary';
                                } else {
                                    check_active_event = false;
                                }
                                ev_partner += '<div class="label ' + ev_check + '">' + c_partner.name + '</div> ';
                            }
                        }
                    });
                }
                html_event += '<li>Hạng thẻ : ' + ev_partner + '</li>';
            }
            if (event_item.localtion != '') {
                var res_local = event_item.localtion.split(",");
                var ev_local = '';
                $.each(res_local, function (i_p, field_local) {
                    var c_localtion = localtion_list.filter(function (obj2s) {
                        return parseInt(obj2s.id) === parseInt(field_local);
                    })[0];
                    if (c_localtion) {
                        var ev_check = 'bg-danger';
                        if (c_localtion.id == localtion) {
                            ev_check = 'bg-primary';
                        } else {
                            check_active_event = false;
                        }
                        ev_local += '<div class="label ' + ev_check + '">' + c_localtion.name + '</div> ';
                    }
                });
                html_event += '<li>Khu vực : ' + ev_local + '</li>';
            }
            html_event += '</ul></td>\n\
                <td><ul>';
            if (event_item.promotion && parseFloat(event_item.promotion) != 0) {
                var type_money = '';
                if (event_item.money_type == 1) { //1-tiền 2-%
                    type_money = ' ($)';
                }
                if (event_item.money_type == 2) { //1-tiền 2-%
                    type_money = ' (%)';
                }
                html_event += '<li>Giảm giá đơn hàng : <div class="label bg-primary">' + formatNumber(parseFloat(event_item.promotion)) + type_money + '</div> </li>';
            }
            if (event_item.price_sales) {
                var type_money = '';
                if (event_item.price_sales_type == 1) { //1-tiền 2-%
                    type_money = ' ($)';
                }
                if (event_item.price_sales_type == 2) { //1-tiền 2-%
                    type_money = ' (%)';
                }
                html_event += '<li>Giảm giá sản phẩm : <div class="label bg-primary">' + formatNumber(parseFloat(event_item.price_sales)) + type_money + '</div> </li>';
            }
            if (event_item.point) {
                var type_money = '';
                if (event_item.point_type == 1) { //1-tiền 2-%
                    type_money = ' (điểm)';
                }
                if (event_item.point_type == 2) { //1-tiền 2-%
                    type_money = ' (%)';
                }
                html_event += '<li>Tặng điểm : <div class="label bg-primary">' + formatNumber(parseFloat(event_item.point)) + type_money + '</div> </li>';
            }
            if (event_item.voucher) {
                html_event += '<li>Mã giảm giá : <div class="label bg-primary">' + event_item.voucher + '</div> </li>';
            }
            if (event_item.product_promotion) {
                html_event += '<li>Tặng sản phẩm :';
                var res = event_item.product_promotion.split(",");
                $.each(res, function (j, res_item) {
                    $.each(product_list, function (i, p_field) {
                        if (res_item == p_field.p_id) {
                            html_event += '<span class="label bg-primary">' + p_field.name + '</span>';
                        }
                    });
                });
                html_event += '</li>';
            }
            //   html += event_item.promotion + event_item.money_type + event_item.price_sales + event_item.price_sales_type + event_item.product_promotion;
            html_event += '</ul></td>\n\
<td >' + event_item.number + '</td>\n\
<td></td>\n\
                        < /tr>';
            if (check_active_event) {
                html_event = '<tr>\n\
<td><input type="checkbox" name="applyed"  value="' + event_item.event_id + '">\n\
' + html_event;
            } else {
                html_event = '<tr class="not_active">\n\
<td><input type="checkbox" name="applyed"  value="' + event_item.event_id + '" disabled>\n\
' + html_event;
            }
            if (check_current_event == true || list_product_event == '') {
                html += html_event;
            }
            coutn++;
        });
        if (html) {
            $('#product_event_list').html(html);
            $('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'});
        } else {
            $('#product_event_list').html('');
        }
    }
    var store = trans.objectStore('tbl_product_views');
    var cursorRequest = store.openCursor();
    cursorRequest.onerror = function (error) {
        console.log(error);
    };
    cursorRequest.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            product_view_list.push(cursor.value);
            cursor.continue();
        }
    }

    var store = trans.objectStore('tbl_location');
    var cursorRequest = store.openCursor();
    cursorRequest.onerror = function (error) {
        console.log(error);
    };
    cursorRequest.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            localtion_list.push(cursor.value);
            cursor.continue();
        }
    }
    var store = trans.objectStore('tbl_product_category');
    var cursorRequest = store.openCursor();
    cursorRequest.onerror = function (error) {
        console.log(error);
    };
    cursorRequest.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            category_list.push(cursor.value);
            cursor.continue();
        }
    }

    var store = trans.objectStore('tbl_partner');
    var cursorRequest = store.openCursor();
    cursorRequest.onerror = function (error) {
        console.log(error);
    };
    cursorRequest.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            list_partner.push(cursor.value);
            cursor.continue();
        }
    }
    //Get branch
    var branches_detail = trans.objectStore('tbl_product_branches');
    var cursorRequest_branches = branches_detail.openCursor();
    cursorRequest_branches.onerror = function (error) {
        console.log(error);
    };
    cursorRequest_branches.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            var cur_row = cursor.value;
            branch_list.push(cur_row);
            cursor.continue();
        }
    };
    var store = trans.objectStore('tbl_event');
    var cursorRequest = store.openCursor();
    cursorRequest.onerror = function (error) {
        console.log(error);
    };
    cursorRequest.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            var product_list_id = [];
            if (cursor.value.product_promotion) {
                var res = cursor.value.product_promotion.split(",");
                $.each(res, function (j, res_item) {
                    if (res_item != '') {
                        var tbl_product = trans.objectStore('tbl_product_search');
                        var cursorRequestproduct = tbl_product.index('p_id').get(parseInt(res_item));
                        cursorRequestproduct.onsuccess = function (evt_pro) {
                            var cursor_product = evt_pro.target.result;
                            if (product_list.indexOf(cursor_product) == -1) {
                                product_list.push(cursor_product);
                            }
                        }
                    }
                });
            }
            event_list.push(cursor.value);
            cursor.continue();
        }
    }

}
function start_search_product() {
    add_user_to_input();
    add_data_to_curremcy('tbl_currency', '#currency_id_select');
    add_data_to_select('tbl_bank_acc', '#thanh_toan_type');
    add_data_to_alert('tbl_currency', '.alert_exchange_rate');
    add_city();
    //   add_event_apply();
    order_search_build();
    var typingTimer; //timer identifier
    var doneTypingInterval = 500; //time in ms, 5 second for example
    $(document).on('input', '#search_product_order_index', function (event) {
        clearTimeout(typingTimer);
        if ($('#search_product_order_index').val()) {
            typingTimer = setTimeout(function () {
                doneTyping($('#search_product_order_index').val());
            }, doneTypingInterval);
        }
    });
}

Number.prototype.format = function (n, x) {
    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
};
Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (size || 2)) {
        s = "0" + s;
    }
    return s;
};
function objectFindByKey(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}
function total_money_order1() {
    var t_money_count = 0;
    var t_money_count_tax = 0;
    var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
    count = 1;
    var select_currency = $(id_tabs).find('#currency_id_select').val();
    $(id_tabs).find('section.banhang').find('#product_order_list').children('tr').each(function (i) {
        var t_money = $(this).find("input[name='product_price_order[]']");
        var t_giam = $(this).find("input[name='giam_order_des[]']");
        var t_quaty = $(this).find("input[name='product_quatity[]']");
        var t_tax = $(this).find("input[name='product_tax_order[]']");
        var gia = (parseFloat(t_money.autoNumeric('get')) - parseFloat(t_giam.val())) * parseFloat(t_quaty.autoNumeric('get'));
        tax_c = 0;
        if (parseFloat(t_tax.autoNumeric('get')) > 0) {
            var tax_c = parseFloat(gia) * parseFloat(t_tax.autoNumeric('get')) / 100;
        }
        if (isNaN(gia)) {
            t_money_count = t_money_count;
            $(this).find('span.thanh_tien_order').html('0')
        } else {
            $(this).find('span.thanh_tien_order').html(formatNumber(gia));
            t_money_count = parseFloat(gia) + t_money_count;
            t_money_count_tax = parseFloat(tax_c) + t_money_count_tax;
        }
    });
    var giam_gia = $(id_tabs).find("#giam_gia").autoNumeric('get');
    var tien_khach_tra = $(id_tabs).find("#tien_khach_tra").autoNumeric('get');
    var cur_name = 'VNĐ';
    var ngoai_te = 0;
    var ngoai_te_tax = 0;
    var cur_te = $(id_tabs).find("#currency_id_select").find('option:selected').text();
    var quy_doi = 0;
    var ex_rate_select = $(id_tabs).find("#currency_id_select").find('option:selected').data('exchane');
    ngoai_te = t_money_count / parseFloat(ex_rate_select);
    ngoai_te_tax = t_money_count_tax / parseFloat(ex_rate_select);
    quy_doi = giam_gia / parseFloat(ex_rate_select);
    ex_rate_select = parseFloat(ex_rate_select);
    var t_money_buy = ngoai_te + ngoai_te_tax - giam_gia;
    if (ngoai_te < giam_gia) {
        t_money_buy = 0;
    }
    $(id_tabs).find('#tong_tien_hang').html(formatNumber(t_money_count) + ' ' + cur_name);
    $(id_tabs).find('#tong_tien_thue').html(formatNumber(t_money_count_tax) + ' ' + cur_name);
    $(id_tabs).find('#tong_tien_hang_ngoai_te').html('~ ' + formatNumber(ngoai_te) + ' ' + cur_te);
    $(id_tabs).find('#tong_tien_thue_ngoai_te').html('~ ' + formatNumber(ngoai_te_tax) + ' ' + cur_te);
    $(id_tabs).find('#khach_can_tra').html(formatNumber(t_money_buy) + ' ' + cur_te);
    if (t_money_buy <= 0) {
        $(id_tabs).find("#giam_gia").autoNumeric('set', ngoai_te);
    }
    var tra_lai = 0;
    tra_lai = tien_khach_tra - t_money_buy;
    $(id_tabs).find('#tra_khach').html(formatNumber(tra_lai) + ' ' + cur_te);
    return t_money_count;
}
function total_money_return_order() {
    var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
}
function total_money_order() {
    var t_money_count = 0;
    var t_money_count_tax = 0;
    var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
    count = 1;
    var select_currency = $(id_tabs).find('#currency_id_select').val();
    $(id_tabs).find('section.banhang').find('#product_order_list').children('tr').each(function (i) {
        var t_money = $(this).find("input[name='product_price_order[]']");
        var t_giam = $(this).find("input[name='giam_order_des[]']");
        var t_quaty = $(this).find("input[name='product_quatity[]']");
        var t_tax = $(this).find("input[name='product_tax_order[]']");
        var t_event_sale = 0;
        if ($(this).has("input[name='product_event_confirm[]']").length == 0) {
            t_event_sale = $(this).find("input[name='product_event_sale[]']").val();
        } else {
            if ($(this).find("input[name='product_event_confirm[]']").is(':checked')) {
                t_event_sale = $(this).find("input[name='product_event_sale[]']").val();
            }
        }
        var gia = (parseFloat(t_money.autoNumeric('get')) - parseFloat(t_giam.val()) - parseFloat(t_event_sale)) * parseFloat(t_quaty.autoNumeric('get'));
        tax_c = 0;
        if (parseFloat(t_tax.autoNumeric('get')) > 0) {
            var tax_c = parseFloat(gia) * parseFloat(t_tax.autoNumeric('get')) / 100;
        }
        if (isNaN(gia)) {
            t_money_count = t_money_count;
            $(this).find('span.thanh_tien_order').html('0')
        } else {
            $(this).find('span.thanh_tien_order').html(formatNumber(gia));
            t_money_count = parseFloat(gia) + t_money_count;
            t_money_count_tax = parseFloat(tax_c) + t_money_count_tax;
        }
    });
    var t_ship = $(id_tabs).find("#ship_order_price").autoNumeric('get');
    t_money_count = parseFloat(t_money_count) + parseFloat(t_ship);
    var partner_discount = $(id_tabs).find("input[name='partner_card_discount']").val();
    if (partner_discount) {
        t_money_count = t_money_count * (1 - parseFloat(partner_discount) / 100);
    }
    var ex_rate_select = $(id_tabs).find("#currency_id_select").find('option:selected').data('exchane');
    var coupon_discount = $(id_tabs).find("input[name='coupon_detail_value']").val();
    if (coupon_discount) {
        $(id_tabs).find("#giam_gia").autoNumeric('set', parseFloat(coupon_discount) / parseFloat(ex_rate_select));
    }
    var giam_gia = $(id_tabs).find("#giam_gia").autoNumeric('get');
    var tien_khach_tra = $(id_tabs).find("#tien_khach_tra").autoNumeric('get');
    var cur_name = 'VNĐ';
    var ngoai_te = 0;
    var ngoai_te_tax = 0;
    var cur_te = $(id_tabs).find("#currency_id_select").find('option:selected').text();
    var quy_doi = 0;

    ngoai_te = t_money_count / parseFloat(ex_rate_select);
    ngoai_te_tax = t_money_count_tax / parseFloat(ex_rate_select);
    quy_doi = giam_gia / parseFloat(ex_rate_select);
    ex_rate_select = parseFloat(ex_rate_select);
    var t_money_buy = ngoai_te + ngoai_te_tax - giam_gia;
    if (ngoai_te < giam_gia) {
        t_money_buy = 0;
    }
    if (t_money_buy <= 0) {
        $(id_tabs).find("#giam_gia").autoNumeric('set', ngoai_te);
    }
    if ($(id_tabs).find('.table_product_group_divide_item').has()) {
        var total_money_return = 0;
        $(id_tabs).find('.table_product_group_divide_item').children('tr').each(function (i) {
            var order_p_money_return = $(this).find("input[name='product_order_price_return[]']").autoNumeric('get');
            var order_p_quaty = $(this).find("input[name='product_order_return[]']").autoNumeric('get');
            if (order_p_money_return && order_p_quaty != 0) {
                total_money_return += parseFloat(order_p_money_return) * parseInt(order_p_quaty);
            }
        });
        var ngoai_te_total_money_return = total_money_return / parseFloat(ex_rate_select);
        t_money_buy = parseFloat(t_money_buy) - parseFloat(ngoai_te_total_money_return);
        $(id_tabs).find('#tong_tien_tra_hang').html(formatNumber(total_money_return) + ' ' + cur_name);
        $(id_tabs).find('#tong_tien_tra_hang_ngoai_te').html(formatNumber(ngoai_te_total_money_return) + ' ' + cur_te);
    }
    $(id_tabs).find('#tong_tien_hang').html(formatNumber(t_money_count) + ' ' + cur_name);
    $(id_tabs).find('#tong_tien_thue').html(formatNumber(t_money_count_tax) + ' ' + cur_name);
    $(id_tabs).find('#tong_tien_hang_ngoai_te').html('~ ' + formatNumber2(ngoai_te) + ' ' + cur_te);
    $(id_tabs).find('#tong_tien_thue_ngoai_te').html('~ ' + formatNumber2(ngoai_te_tax) + ' ' + cur_te);
    $(id_tabs).find('#khach_can_tra').html(formatNumber2(t_money_buy) + ' ' + cur_te);

    var tra_lai = 0;
    tra_lai = tien_khach_tra - t_money_buy;
    $(id_tabs).find('#tra_khach').html(formatNumber2(tra_lai) + ' ' + cur_te);
    if (t_money_buy <= 0) {
        $('strong#khach_can_tra ').css('color', 'red');
    } else {
        $('strong#khach_can_tra ').css('color', '#218876');
    }
    return t_money_count;
}
function httpGet(theUrl)
{
    var xmlHttp = null;
    xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false);
    xmlHttp.send(null);
    return xmlHttp.responseText;
}
function print_order(url) {
    var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
    count = $(id_tabs).find('section.banhang').find('#product_order_list').children('tr').length;
    if (count > 0) {
        var data = $(id_tabs).find('#form_order').serialize();
         $.ajax({
             url: url,
                    type: "POST",
                    data: data,
                    success: function (data, textStatus, jqXHR)
                    {
                var divContents = $("#dvContainer").html();
                var printWindow = window.open('', '', 'height=400,width=800');
                printWindow.document.write(data);
                printWindow.document.close();
                printWindow.print();
            }
        });
    } else {
        alert('No data !');
    }

}
function print_order_vat(url, code) {
    var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
    count = $(id_tabs).find('section.banhang').find('#product_order_list').children('tr').length;
    if (count > 0) {
        var data = $(id_tabs).find('#form_order').serialize();
         $.ajax({
             url: url,
                    type: "POST",
                    data: data,
                    success: function (data, textStatus, jqXHR)
                    {
                data = data.replace("h4_order_code_print", "#" + code);
                var divContents = $("#dvContainer").html();
                var printWindow = window.open('', '', 'height=400,width=800');
                printWindow.document.write(data);
                printWindow.document.close();
                printWindow.print();
            }
        });
    } else {
        alert('No data !');
    }
}
function compareArrays(arr1, arr1) {
    if (arr1.sort().join(',') === arr1.sort().join(',')) {
        return  true;
    } else {
        return  false;
    }
}
function timeConverter(UNIX_timestamp) {
    var a = new Date(UNIX_timestamp * 1000);
    var year = a.getFullYear();
    var month = a.getMonth() + 1;
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + '/' + month + '/' + year + ' ' + hour + ':' + min + ':' + sec;
    return time;
}
function short_timeConverter(UNIX_timestamp) {
    var a = new Date(UNIX_timestamp * 1000);
    var year = a.getFullYear();
    var month = a.getMonth() + 1;
    var date = a.getDate();
    var time = date + '/' + month + '/' + year;
    return time;
}
function submit_add_user() {
    var check_max = [];
    var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
    var fr_user = $('#form_add_customer');
    var u_name = fr_user.find("input[name='c_full_name']").val();
    var u_email = fr_user.find("input[name='c_email']").val();
    var u_phone = fr_user.find("input[name='c_phone']").val();
    var u_gender = fr_user.find("input:radio[name=gender]:checked").val();
    var u_city = fr_user.find('#city_select_id').val();
    var u_district = fr_user.find("#select_district_id").val();
    var u_address = fr_user.find("#c_address").val();

    var user_order = {
        full_name: u_name,
        email: u_email,
        phone: u_phone,
        gender: u_gender,
        location: u_district,
        address: u_address,
        sync: 0,
        code: 1,
        status: 1,
    };

    var check_exit = false;

    var trans = db_index.transaction(['tbl_users'], 'readwrite');
    trans.oncomplete = function (evt) {
        var d = new Date();
        var timer = d.getTime();
        user_order.code = 'KH' + timer;
        console.log(user_order);
        if (check_exit != true) {
            var trans1 = db_index.transaction(['tbl_users'], 'readwrite');
            var tbl_users = trans1.objectStore('tbl_users');
            var req = tbl_users.add(user_order);
            req.onsuccess = function (evt) {
                fr_user[0].reset();
                $('#add_customer').modal('hide');
                add_user_to_input();
            }
        } else {
            // alert('E-mail đã tồn tại trong hệ thống !');
        }
    }

    var store_u = trans.objectStore('tbl_users');
    var cursorRequeste = store_u.openCursor();
    cursorRequeste.onerror = function (error) {

    };
    cursorRequeste.onsuccess = function (evt) {
        var cursor = evt.target.result;
        if (cursor) {
            var user_data = cursor.value;
            if (u_email == user_data.email) {
                check_exit = true;
            }
            check_max = user_data;
            cursor.continue();
        }
    }

}
function submit_order() {
    var user_list = [];
    var user_id = 0;
    var cr_tabs = $('.tabs_order li.active>a[data-toggle="tab"]');
    var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
    var count = $(id_tabs).find('section.banhang').find('#product_order_list').children('tr').length;
    var c_user = current_user;
    if ($(id_tabs).find('#khach_hang_mua').val() != 0) {
        user_id = $(id_tabs).find('#khach_hang_mua').val();
    }
    if (count > 0) {
        var d = new Date();
        var timer = d.getTime();
        var inser_tbl_order = {};
        inser_tbl_order.id = timer;
        inser_tbl_order.code = 'DH' + timer;
        inser_tbl_order.branches_id = c_user.branches_id;
        inser_tbl_order.currency_id = $(id_tabs).find('#currency_id_select').val();
        inser_tbl_order.ex_rate = $(id_tabs).find('#currency_id_select').find(':selected').data('exchane');
        inser_tbl_order.pay_type = $(id_tabs).find("input:radio[name=thanh_toan]:checked").val();
        if (inser_tbl_order.pay_type == '1') {
            inser_tbl_order.bank_acc_id = $(id_tabs).find("#thanh_toan_type").val();
        }
        inser_tbl_order.discount = $(id_tabs).find('#giam_gia').autoNumeric('get');
        inser_tbl_order.coupons_code = $(id_tabs).find('#ma_giam_gia').val();
        inser_tbl_order.user_id = $(id_tabs).find('#khach_hang_mua').val();
        inser_tbl_order.admin_id = c_user.id;
        inser_tbl_order.note = $(id_tabs).find("textarea[name=ghi_chu]").val();
        inser_tbl_order.order_type = 2;
        inser_tbl_order.ship_name = $(id_tabs).find("input[name=ship_order_name]").val();
        inser_tbl_order.ship_address = $(id_tabs).find("input[name=ship_order_addess]").val();
        inser_tbl_order.ship_phone = $(id_tabs).find("input[name=ship_order_phone]").val();
        inser_tbl_order.ship_note = $(id_tabs).find("textarea[name=ship_order_note]").val();
        inser_tbl_order.ship_price = $(id_tabs).find("input[name=ship_order_price]").autoNumeric('get');
        inser_tbl_order.delivery_status = $(id_tabs).find("input:radio[name=delevery_status]:checked").val();
        inser_tbl_order.time = parseInt(parseInt(timer) / 1000);
        inser_tbl_order.sync = 0;
        inser_tbl_order.status = 1;
        var inser_tbl_order_detail = [];
        var count_detail = 0;
        var total_pay = 0;
        var datap = $(id_tabs).find('#product_order_list').find('tr').each(function (i) {
            var t_event_sale = 0;
            if ($(this).has("input[name='product_event_confirm[]']").length == 0) {
                t_event_sale = $(this).find("input[name='product_event_sale[]']").val();
            } else {
                if ($(this).find("input[name='product_event_confirm[]']").is(':checked')) {
                    t_event_sale = $(this).find("input[name='product_event_sale[]']").val();
                }
            }
            var detal_order = {};
            detal_order.order_id = timer;
            detal_order.name = $(this).find("input[name='product_name_order[]']").val();
            detal_order.store_code = $(this).find("input[name='product_id_store[]']").val();
            detal_order.p_id = $(this).find("input[name='product_select_id[]']").val();
            detal_order.tax = $(this).find("input[name='product_tax_order[]']").val();
            detal_order.p_price = $(this).find("input[name='product_price_order[]']").data('price');
            detal_order.order_price = $(this).find("input[name='product_price_order[]']").autoNumeric('get');
            detal_order.currency_id = inser_tbl_order.currency_id;
            detal_order.ex_rate = inser_tbl_order.ex_rate;
            detal_order.quantity = $(this).find("input[name='product_quatity[]']").autoNumeric('get');
            detal_order.discount = parseFloat($(this).find("input[name='giam_order_des[]']").val()) + parseFloat(t_event_sale);
            detal_order.note = $(this).find("input[name='product_note_order[]']").val();
            detal_order.sync = 0;
            detal_order.status = 1;
            inser_tbl_order_detail.push(detal_order);
            total_pay += parseFloat(detal_order.order_price) * parseFloat(detal_order.quantity) - parseFloat(detal_order.discount);
            count_detail++;
        });
        inser_tbl_order.total_paid = total_pay;
        var list_table = [];
        $.each(db_index.objectStoreNames, function (i, field) {
            list_table.push(field);
        });
        var trans = db_index.transaction(['tbl_order', 'tbl_order_detail', 'tbl_product_search', 'tbl_users', 'tbl_code'], 'readwrite');
        trans.oncomplete = function (evt) {
            var div_print = '  <!doctype html><html>' + $('#print_sampe_order').html() + '</html>';
            div_print = div_print.replace("----HOA_DON_MAHOADON----", inser_tbl_order.code);
            div_print = div_print.replace("----HOA_DON_NGAYBAN----", timeConverter(inser_tbl_order.time));
            div_print = div_print.replace("----HOA_DON_TITLE----", 'HÓA ĐƠN BÁN LẺ');
            var user_sale = '';
            if (inser_tbl_order.user_id == 0) {
                user_sale = 'Khách vãng lai';
            } else {
                user_sale += user_list.full_name + '<br/>';
                user_sale += user_list.email + '<br/>';
                user_sale += user_list.address + '<br/>';
                user_sale += user_list.phone + '<br/>';
                user_sale += 'Điểm : ' + formatNumber(user_list.point) + '<br/>';
            }
            div_print = div_print.replace("----HOA_DON_KHACHMUA----", user_sale);
            var order_detail_sale = '';
            var count_product = 0;
            var count_product_quatity = 0;
            jQuery.each(inser_tbl_order_detail, function (key_child, value_child) {
                var last_item = '';
                if (key_child + 1 == inser_tbl_order_detail.length) {
                    last_item = 'last';
                }
                order_detail_sale += '<tr class="item ' + last_item + '">\n\
<td>' + value_child.name + '</td>\n\
                <td>' + formatNumber(value_child.quantity) + '</td>\n\
                <td>' + formatNumber2(value_child.order_price) + '</td>\n\
                <td>' + formatNumber2(value_child.discount) + '</td>\n\
                              <td>' + formatNumber2(parseFloat(value_child.quantity) * parseFloat(value_child.order_price) - parseFloat(value_child.discount)) + '</td>\n\
</tr>';
                count_product_quatity += parseInt(value_child.quantity);
                count_product++;
            });
            div_print = div_print.replace('<tr><td colspan="5">----HOA_DON_SANPHAM----</td></tr>', order_detail_sale);
            var ship_user = '';
            if (inser_tbl_order.ship_name == '') {
                ship_user == user_sale;
            } else {
                ship_user += inser_tbl_order.ship_name + '<br/>';
                ship_user += inser_tbl_order.ship_address + '<br/>';
                ship_user += inser_tbl_order.ship_phone + '<br/>';
                ship_user += inser_tbl_order.ship_note + '<br/>';
            }

            div_print = div_print.replace("----HOA_DON_NGUOINHAN----", ship_user);
            div_print = div_print.replace("----HOA_DON_TONGCONG----", formatNumber2(total_pay));
            if (inser_tbl_order.ship_price) {
                div_print = div_print.replace("----HOA_DON_VANCHUYEN----", formatNumber2(inser_tbl_order.ship_price));
                div_print = div_print.replace("----HOA_DON_PHAITRA----", formatNumber2(parseFloat(total_pay) + parseFloat(inser_tbl_order.ship_price) - parseFloat(inser_tbl_order.discount)));
                div_print = div_print.replace("----HOA_DON_TICHLUY----", formatNumber((parseFloat(total_pay) + parseFloat(inser_tbl_order.ship_price) - parseFloat(inser_tbl_order.discount)) / 10000));
            } else {
                div_print = div_print.replace("----HOA_DON_VANCHUYEN----", 0);
                div_print = div_print.replace("----HOA_DON_PHAITRA----", formatNumber2(parseFloat(total_pay) - parseFloat(inser_tbl_order.discount)));
                div_print = div_print.replace("----HOA_DON_TICHLUY----", formatNumber((parseFloat(total_pay) - parseFloat(inser_tbl_order.discount)) / 10000));
            }
            div_print = div_print.replace("----HOA_DON_SOLUONG----", formatNumber(count_product_quatity));
            div_print = div_print.replace("----HOA_DON_MATHANG----", formatNumber(count_product));
            div_print = div_print.replace("----HOA_DON_MAGIAMGIA----", inser_tbl_order.coupons_code);
            div_print = div_print.replace("----HOA_DON_GIAMGIA----", formatNumber2(inser_tbl_order.discount));
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write(div_print);
            printWindow.document.close();
            printWindow.print();
        }
        var tbl_order = trans.objectStore('tbl_order');
        var req = tbl_order.add(inser_tbl_order);
        req.onsuccess = function (evt) {
            jQuery.each(inser_tbl_order_detail, function (key_child, value_child) {
                var tbl_order_detail = trans.objectStore('tbl_order_detail');
                var req_dt = tbl_order_detail.add(value_child);
                req_dt.onsuccess = function (evt) {
                }
            });
        }
        var tbl_code = trans.objectStore('tbl_code');
        var cursorRequestv = tbl_code.openCursor();
        cursorRequestv.onerror = function (error) {
            console.log(error);
        };
        cursorRequestv.onsuccess = function (evtv) {
            var cursorv = evtv.target.result;
            if (cursorv) {
                var updateData = cursorv.value;

                if (inser_tbl_order.coupons_code) {
                    if (inser_tbl_order.coupons_code == updateData.code) {
                        updateData.status = 1;
                        updateData.sync = 0;
                        var request = cursorv.update(updateData);
                        request.onsuccess = function () {
                            console.log('Updated code !');
                        };
                    }
                }
                cursorv.continue();
            }
        }
        jQuery.each(inser_tbl_order_detail, function (key_child, value_child) {
            var tbl_product = trans.objectStore('tbl_product_search');
            var req = tbl_product.get(parseInt(value_child.store_code));
            req.onsuccess = function (evt) {
                var record = evt.target.result;
                record.quantity_sold = parseInt(record.quantity_sold) + parseInt(value_child.quantity);
                var store_v = trans.objectStore('tbl_product_search');
                var cursorRequestv = store_v.openCursor();
                cursorRequestv.onerror = function (error) {
                    console.log(error);
                };
                cursorRequestv.onsuccess = function (evtv) {
                    var cursorv = evtv.target.result;
                    if (cursorv) {
                        var updateData = cursorv.value;
                        if (updateData.id == record.id) {
                            updateData = record;
                            updateData.sync = 0;
                            var request = cursorv.update(updateData);
                            request.onsuccess = function () {
                                console.log('Updated !');
                            };
                        }
                        cursorv.continue();
                    }
                }
            }
        });
        if (user_id != 0) {
            var table_user = trans.objectStore('tbl_users');
            var row_t = table_user.get(parseInt(user_id));
            row_t.onsuccess = function (evt) {
                var record = evt.target.result;
                user_list = record;
            }
        }
//        var data_even = '';
//        $('#product_event_list').find('tr').each(function (i) {
//            var this_checkbox_event = $(this).find("input[name='check_apply_event[]']");
//            if (this_checkbox_event.is(':checked')) {
//                data_even = data_even + this_checkbox_event.val() + ',';
//            }
//        });
//        // datap.push({name: 'event', value: data_even});
//        var order_add = [];
//        console.log(datap);
//        jQuery.each(datap, function (i, field) {
//            console.log(field.name + " ");
//            console.log(field.value + " ");
//        });
//        order_add['code'] = 'ddđ';
//        console.log(order_add);
//         $.ajax({
//             url: url,
//                    type: "POST",
//                    data: datap,
//                    success: function (data, textStatus, jqXHR)
//                    {
//                var msg = jQuery.parseJSON(data);
//                if (msg.check == 1) {
//                    print_order_vat(url_print_vat, msg.content);
//
//                    cr_tabs.parent().remove();
//                    $(id_tabs).remove();
//                    if ($('.tabs_order li').length > 1) {
//                        $(".tabs_order li").children('a').first().click();
//                    }
//                    if ($('.tabs_order li').length == 1) {
//                        $('.add-contact').trigger('click');
//                    }
//
//                }
//                if (msg.check == 0) {
//                    alert('Có lỗi trong quá trình xử lý !')
//                }
//            }
//        });
    } else {
        alert('No data !');
    }
}