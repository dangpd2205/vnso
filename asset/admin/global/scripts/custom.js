/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




var CustomJS = function () {
    var e = function () {
//  BIGIN JS
	
    // Phân trang child
        $(document).on('click', 'div.paginate_child .pagination li a', function (event) {
            var elem = $(this);
            var url = $(this).attr('href');
            var div_content = $(this).closest('div.dataTables_content .child_content');
            event.preventDefault();
            event.stopPropagation();
            App.blockUI({
                target: div_content,
                animate: !0,
                overlayColor: "gray"
            });
            $.ajax({
                url: url,
                dataType: 'html',
                success: function (data) {
                    App.unblockUI(div_content);
                    div_content.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error('Server connection error !');
                    App.unblockUI(div_content);
                }

            });
        });
// Phân trang 
        $(document).on('click', 'div.paginate_normal .pagination li a', function (event) {
            var elem = $(this);
            var url = $(this).attr('href');
            var div_content = $(this).closest('div.dataTables_content');
            event.preventDefault();
            event.stopPropagation();
            App.blockUI({
                target: div_content,
                animate: !0,
                overlayColor: "gray"
            });
            $.ajax({
                url: url,
                dataType: 'html',
                success: function (data) {
                    App.unblockUI(div_content);
                    div_content.html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error('Server connection error !');
                    App.unblockUI(div_content);
                }

            });
        });
// Xem nhanh
        $(document).on('click', 'div.dataTables_content a.quick_view_table', function (event) {
            var this_a = $(this);
            var this_tr = this_a.closest('tr');
            console.log(this_a.children('i').hasClass('fa-plus-circle'));
            if (this_a.children('i').hasClass('fa-plus-circle')) {
                this_a.children('i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
                var url_post = $('div.dataTables_content').data('quick-url');
                var data_id = this_a.data('id');
                App.blockUI({
                    target: 'div.dataTables_content',
                    animate: !0,
                    overlayColor: "gray"
                });
                $.ajax({
                    url: url_post,
                    data: {id: data_id},
                    type: 'POST',
                    dataType: 'html',
                    success: function (data) {
                        this_tr.after(data);
                        App.unblockUI('div.dataTables_content');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error('Server connection error !');
                        App.unblockUI('div.dataTables_content');
                    }

                });
            } else {
                this_a.children('i').removeClass('fa-minus-circle').addClass('fa-plus-circle');
                this_tr.next().remove().fadeOut();
            }
        });
// Lọc dữ liệu
        $(document).on('click', '.filter_form a.btn_submit_filter', function (event) {
            var this_form = $(this).closest('form');
            var url_post = this_form.find("input:hidden[name='url_filter']").val();
            var this_content = $(this).closest('.page-content').find('div.dataTables_content').first();
            App.blockUI({
                target: this_content,
                animate: !0,
                overlayColor: "gray"
            });
            $.ajax({
                url: url_post,
                data: this_form.serialize(),
                type: 'POST',
                dataType: 'html',
                success: function (data) {
                    this_content.html(data);
                    App.unblockUI(this_content);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error('Server connection error !');
                    App.unblockUI(this_content);
                }

            });
        });
// Tìm kiếm
        $(document).on('submit', '.search_form', function (event) {
            event.preventDefault();
            var this_form = $(this);
            var url_post = this_form.find("input:hidden[name='url_search']").val();
            var this_content = $(this).closest('.page-content').find('div.dataTables_content').first();
            App.blockUI({
                target: this_content,
                animate: !0,
                overlayColor: "gray"
            });
            $.ajax({
                url: url_post,
                data: this_form.serialize(),
                type: 'POST',
                dataType: 'html',
                success: function (data) {
                    this_content.html(data);
                    App.unblockUI(this_content);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error('Server connection error !');
                    App.unblockUI(this_content);
                }

            });
        });
        // Chỉnh số row 
        $(document).on('change', '.row_setting_form select', function (event) {
            var this_form = $(this).closest('form');
            var url_post = this_form.find("input:hidden[name='url_search']").val();
            var this_content = $(this).closest('.page-content').find('div.dataTables_content').first();
            App.blockUI({
                target: this_content,
                animate: !0,
                overlayColor: "gray"
            });
            $.ajax({
                url: url_post,
                data: this_form.serialize(),
                type: 'POST',
                dataType: 'html',
                success: function (data) {
                    this_content.html(data);
                    App.unblockUI(this_content);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error('Server connection error !');
                    App.unblockUI(this_content);
                }

            });
        });
        // Kiếm tra tồn tại trên input 
        $(document).on('change', '.check_exit_database', function (event) {
            var this_input = $(this);
            var this_value = $(this).val();
            var url_post = this_input.data('url-check');
            this_input.parent().parent().removeClass('has-success');
            this_input.parent().parent().removeClass('has-error');
            this_input.parent().children('i').remove();
            this_input.parent().prepend(' <i class="fa fa-circle-o-notch fa-spin font-blue"></i>');
            $.ajax({
                url: url_post,
                data: {value: this_value},
                type: 'POST',
                dataType: 'html',
                success: function (data) {
                    this_input.parent().children('i').remove();
                    var msg = jQuery.parseJSON(data);
                    if (msg.check) {
                        this_input.parent().parent().addClass('has-success');
                        this_input.parent().prepend('<i class="fa fa-check tooltips" data-original-title="' + msg.message + '" data-container="body"></i>');
                    } else {
                        this_input.parent().parent().addClass('has-error');
                        this_input.parent().prepend('<i class="fa fa-exclamation tooltips" data-original-title="' + msg.message + '" data-container="body"></i>');
                    }
                    $(".tooltips").tooltip();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    this_input.parent().children('i').remove();
                    toastr.error('Server connection error !');
                }
            });
        });
        // Auto complate 
        var data_auto_search = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: $('.search_product_to_table').data('url-check') + '?term=%QUERY',
                wildcard: '%QUERY'
            }
        })
        $('.search_product_to_table').typeahead(null, {
            name: 'select-product',
            source: data_auto_search,
            display: 'name',
            minLength: 3,
            templates: {
                empty: [
                    '<div class="empty-message">',
                    'unable to find any Best Picture winners that match the current query',
                    '</div>'
                ].join('\n'),
                suggestion: Handlebars.compile(['<div class="media">', '<div class="pull-left">', '<div class="media-object">', '<img src="{{thumbnail}}" width="50" height="50"/>', "</div>", "</div>", '<div class="media-body">', '<h4 class="media-heading">{{name}}</h4>', "<p>{{name}}</p>", "</div>", "</div>"].join(""))
            }
        });
        $('.search_product_to_table').on('typeahead:asyncrequest', function (evt, item) {
            console.log('typeahead:asyncrequest');
        });
        $('.search_product_to_table').on('typeahead:asyncreceive', function (evt, item) {
            console.log('typeahead:asyncreceive');
        });
        $('.search_product_to_table').on('typeahead:idle', function (evt, item) {
            console.log('typeahead:idle');
            data_auto_search.clear();
        });
        $('.search_product_to_table').on('typeahead:select', function (evt, item) {
            //console.log(evt)
            //console.log(item)
			$('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'});
            var rowCount = $('#product_import_list').children('tr').length;
            var html = '<tr  class="child">';
            html += '<td><img src="' + item.thumbnail + '"/></td>';
            html += '<td><input type="hidden" name="product_id_store[]" value="'+item.p_id+'"/><input type="hidden" name="import_vat[]" value="'+item.p_tax+'"/><input type="hidden" name="properties_id[]"/><input type="hidden" name="mfg[]"/><input type="hidden" name="lot[]"/><input type="hidden" name="expired[]"/><input type="hidden" name="made_in[]"/><input type="hidden" name="manufacturers_id[]"/><ul>';
            html += '<li><span class="dtr-title">Tên sản phâm :</span> <span class="dtr-data">' + item.name + '</span></li>';
            html += '<li><span class="dtr-title">Mã sản phẩm :</span> <span class="dtr-data">' + item.code + '</span></li>';
            html += '<li><span class="dtr-title">Thuộc tính </span><span class="product_properties_view"></span> <a href="javascript:void(0)" class="product_properties_add"> <i class="fa fa-plus"></i></a></li>';
            html += '</ul></td>';
            html += '<td><ul>\n\
<li><input name="import_prices[]" class="bg-focus form-control input-sm auto_number" type="text" value=""></li>\n\
</ul> </td>';
			html += '<td><ul>\n\
<li><input name="quantity[]" class="bg-focus form-control input-sm auto_number" type="text" value=""></li>\n\
</ul> </td>';
            html += '<td><a href="javascript:void(0);" onclick="removeThis(this);"><i class="fa fa-trash-o"></i></a></td>';
            html += '</tr>';
            $(this).val('');
			
            $('#product_import_list').append(html);
            // Your Code Here
            $('.search_product_to_table').typeahead('close');
            $('.search_product_to_table').typeahead('val', '');
        });
// Xóa tr no ajax

        $(document).on('click', 'td .del_no_ajax', function (event) {
            var this_del = $(this);
            var this_tr = $(this).closest('tr');
            $('#ajax_alert_confirm').modal('show').one('click', '#delete-confirm', function (e) {
                this_del.closest('tr').remove();
                var nnn = 1;
                this_body.find('tr').each(function () {
                    $(this).find('td:first-child').html('#' + nnn);
                    nnn++;
                });
                $('#ajax_alert_confirm').modal('hide');
            });
        });

        
        
//END JS
    }
    return {
        init: function () {
            e()
        }
    }
}();
App.isAngularJsApp() === !1 && jQuery(document).ready(function () {
    CustomJS.init()
});