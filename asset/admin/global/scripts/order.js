/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var OrderJs = function () {
    var e = function () {
        var db_index;
        var total_res = 0;
        var check_res = 0;
        // In the following line, you should include the prefixes of implementations you want to test.
        window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
// DON'T use "var indexedDB = ..." if you're not in a function.
// Moreover, you may need references to some window.IDB* objects:
        window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || {READ_WRITE: "readwrite"}; // This line should only be needed if it is needed to support the object's constants for older browsers
        window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
// (Mozilla has never prefixed these objects, so we don't need window.mozIDB*)
        if (!window.indexedDB) {
            toastr.error('Trình duyệt của bạn quá cũ . Vui lòng cập nhật bản mới nhất!');
        } else {

            var DB_NAME = 'pupos_database_offline';
            var DB_VERSION = 88;
            //   open_modal_cancel_hide('ajax_alert_loading');
            var list_table = {
                tbl_product: 'id,code',
                tbl_store: 'id,product_store_code',
                tbl_order: 'id',
                tbl_order_detail: 'id',
                tbl_event: 'id',
                tbl_code: 'id_destination,code',
                tbl_coupons: 'id',
                tbl_users: 'id,code',
                tbl_partner: 'id',
                tbl_user_partner_card: 'id,card_number',
                tbl_order_return: 'id',
                tbl_order_return_detail: 'id',
            };
            var req = indexedDB.open(DB_NAME, DB_VERSION);
            req.onsuccess = function (evt) {
                db_index = this.result;
                if (typeof (load_db_shift) != "undefined") {
                    if (load_db_shift == true) {
                        run_download_db();
                    }
                }
                //      user_to_input();
            };
            req.onerror = function (evt) {
                console.error("openDb:", evt.target.errorCode);
            };
            req.onupgradeneeded = function (evt) {
                $.each(list_table, function (key, value) {
                    var store = evt.currentTarget.result.createObjectStore(key, {keyPath: 'id', autoIncrement: true});
                    $.each(value.split(','), function (key_chil, value_chil) {
                        store.createIndex(value_chil, value_chil, {unique: false});
                    });
                });
            };
            var run_image = 0;
            var gen_image = setInterval(function () {
                $(document).ajaxStop(function () {
                    if (run_image == 0) {
                        convert_table_image();
                        run_image++;
                        var d = new Date();
                        var n = d.getTime();
                        localStorage.setItem("latest_sync", n);
                        $('.system_info').html('Lần đồng bộ mới nhất : ' + timeConverter(localStorage.latest_sync / 1000));
                    }
                    App.unblockUI('body');
                    clearInterval(gen_image);
                });
            }, 1000);
            function timeConverter(UNIX_timestamp) {
                var a = new Date(UNIX_timestamp * 1000);
                var months = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
                var year = a.getFullYear();
                var month = months[a.getMonth()];
                var date = a.getDate();
                var hour = a.getHours();
                var min = a.getMinutes();
                var sec = a.getSeconds();
                var time = date + '/' + month + '/' + year + ' ' + hour + ':' + min + ':' + sec;
                return time;
            }
            if (localStorage.latest_sync) {
                $('.system_info').html('Lần đồng bộ mới nhất : ' + timeConverter(localStorage.latest_sync / 1000));
            }
            function loop_synx(table_loop, page_loop, total_page_loop) {
                if (page_loop <= total_page_loop) {
                    var data_send = {table: table_loop, page: page_loop};
                     $.ajax(
                                {
                                        url: post_data_url,
                                        type: "POST",
                                data: data_send,
                                complete: function (jqXHR, textStatus) {
                                },
                                        success: function (data_child, textStatus, jqXHR)
                                        {
                                    if (msg_child != '') {
                                        var msg_child = jQuery.parseJSON(data_child);
                                        var tx = db_index.transaction(table_loop, 'readwrite');
                                        tx.oncomplete = function (event) {
                                            check_res++;
                                            if (page_loop <= total_page_loop) {
                                                page_loop++;
                                                loop_synx(table_loop, page_loop, total_page_loop);
                                            }
                                            if (total_res == check_res) {
                                                App.unblockUI('body');
                                            }
                                        };
                                        var store_table = tx.objectStore(table_loop);

                                        jQuery.each(msg_child.data, function (key_child, value_child) {
                                            var req = store_table.add(value_child);
                                            req.onsuccess = function (evt) {
                                                console.log("Insertion in DB successful");
                                            }
                                        });
                                    }
                                }
                            });
                }
            }

            function run_download_db() {
                run_image = 0;
                db_index.close();
                var DBDeleteRequest = window.indexedDB.deleteDatabase(DB_NAME);
                DBDeleteRequest.onsuccess = function (event) {
                    console.log("Database deleted successfully");
                    var req = window.indexedDB.open(DB_NAME, DB_VERSION);
                    req.onsuccess = function (evt) {
                        db_index = this.result;
                        run_synx();
                    };
                    req.onerror = function (evt) {
                        console.error("openDb:", evt.target.errorCode);
                    };
                    req.onupgradeneeded = function (evt) {
                        $.each(list_table, function (key, value) {
                            var store = evt.currentTarget.result.createObjectStore(key, {keyPath: 'id', autoIncrement: true});
                            $.each(value.split(','), function (key_chil, value_chil) {
                                store.createIndex(value_chil, value_chil, {unique: false});
                            });
                        });
                    };
                };
            }
            function run_synx() {
                App.blockUI({
                    target: 'body',
                    animate: !0,
                    overlayColor: "gray"
                });
                $.each(list_table, function (key, value) {
                    if (value != '') {
                        var page = 2;
                        var total_page = 0;
                        var data_send = {table: key, page: 1};
                         $.ajax(
                                    {
                                            url: post_data_url,
                                            type: "POST",
                                    data: data_send,
                                            success: function (data, textStatus, jqXHR)
                                            {

                                        if (data != '') {
                                            var msg = jQuery.parseJSON(data);
                                            total_page = msg.total_page;
                                            if (msg.data.length != 0) {
                                                clearObjectStore(key);
                                                var tx = db_index.transaction(key, 'readwrite');
                                                tx.oncomplete = function (event) {
                                                    check_res++;
                                                    if (page <= total_page) {
                                                        total_res += total_page;
                                                        loop_synx(key, page, total_page);
                                                    } else {
                                                        total_res += 1;
                                                    }
                                                };
                                                var store_table = tx.objectStore(key);
                                                jQuery.each(msg.data, function (key_child, value_child) {
                                                    var req = store_table.add(value_child);
                                                    req.onsuccess = function (evt) {
                                                        console.log("Insertion in DB successful");
                                                    }
                                                });
                                            }
                                        }
                                         }
                                    });
                    }
                });

            }
            function getObjectStore(table_name, mode) {
                var tx = db_index.transaction(table_name, mode);
                return tx.objectStore(table_name);
            }
            function clearObjectStore(table_name) {
                var store = getObjectStore(table_name, 'readwrite');
                var req = store.clear();
                req.onsuccess = function (evt) {
                };
                req.onerror = function (evt) {
                    console.error("clearObjectStore:", evt.target.errorCode);
                    displayActionFailure(this.error);
                };
            }
            function convert_base64_image(url, callback) {
                var image = new Image();
                image.onload = function () {
                    var canvas = document.createElement('canvas');
                    canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
                    canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

                    canvas.getContext('2d').drawImage(this, 0, 0);

                    callback(canvas.toDataURL('image/png'));
                };
                image.src = url;
            }
            function convert_table_image() {
                var list_image = [];
                var trans = db_index.transaction(['tbl_product'], 'readonly');
                trans.oncomplete = function (evt) {
                    convert_table_image_base(list_image);
                }
                var image_tbl = trans.objectStore('tbl_product');
                var req = image_tbl.openCursor();
                req.onsuccess = function (evt) {
                    var cursor = evt.target.result;
                    if (cursor) {
                        var user = cursor.value;
                        list_image.push(user);
                        cursor.continue();
                    }
                }
            }
            function convert_table_image_base(data_url) {
                $.each(data_url, function (key_chil, value_chil) {
                    convert_base64_image(value_chil.thumbnail, function (dataUri) {
                        var trans = db_index.transaction(['tbl_product'], 'readwrite');
                        var image_tbl = trans.objectStore('tbl_product');
                        var req = image_tbl.openCursor();
                        req.onsuccess = function (evt) {
                            var cursor = evt.target.result;
                            if (cursor) {
                                if (cursor.value.id === value_chil.id) {
                                    var updateData = cursor.value;
                                    updateData.thumbnail = dataUri;
                                    var request = cursor.update(updateData);
                                    request.onsuccess = function () {
                                        console.log('ok');
                                    };
                                }
                                cursor.continue();
                            }
                        }
                    });
                });

            }
        }
//ADD ORDER
        $(document).on('click', '#add-order', function () {
            var d = new Date();
            var n = d.getTime();
            var id = n;
            var tabId = 'order_' + id;
            var tab_length = $('#order_button').children().length;
            if (tab_length <= 10) {
                $('#order_button').append('<li>\n\
<a data-toggle="tab" href="#order_' + id + '"> <i class="fa fa-shopping-cart"></i>#' + (tab_length + 1) + '</a> \n\
</li>');
                $('#order_content').append('<div class="tab-pane" id="' + tabId + '"><i class="fa fa-shopping-cart"></i>' + tabId + '</div>');
                $('#order_button li:nth-child(' + (tab_length + 1) + ') a').click();
            } else {
                toastr.error('Bán tạo quá nhiều đơn hàng cho phép !');
            }
        });
        //SYSC DATA       
        $(document).on('click', '#btl_sync_data', function () {
            run_download_db();
        });

        $(".mask_currency").inputmask("₫ 999.999.999.999", {
            numericInput: !0
        });

        $(".mask_number").inputmask("9999", {
            numericInput: !0
        });
        var s = "Lựa chọn";
        $(".select2, .select2-multiple").select2({
            placeholder: s,
            width: null
        });
        Number.prototype.format = function (n, x) {
            var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
            return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
        };
        function formatNumber(number)
        {
            var number = parseFloat(number).toFixed(0) + '';
            var x = number.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }
        function locdau(str) {
            str = str.toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            // str = str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g, "-");
            //  str = str.replace(/-+-/g, " "); //thay thế 2- thành 1-
            // str = str.replace(/^\-+|\-+$/g, "");//cắt bỏ ký tự - ở đầu và cuối chuỗi
            return str;
        }
        function select_product(product_s) {
            var data = jQuery.parseJSON(product_s);
            var this_tab_active;
            var html_insert = '<tr>';
            html_insert += '<td>#2  <input  value="' + data.id + '" name="order_detail_id_hidden[]" type="hidden"> <input  value="' + data.name + '" name="order_detail_name_hidden[]" type="hidden"></td>';
            html_insert += '<td>\n\
           <div class="product_image">\n\
                                                                    <img src="' + data.thumbnail + '">\n\
                                                                </div>\n\
                                                                <div class="product_name">\n\
                                                                    <span>' + data.name + '</span>\n\
                                                                    <!--                                                                    <ul>\n\
                                                                                                                                            <li> Thuộc tính :  ' + data.prop_name + '</li>\n\
                                                                                                                                            <li> Nhà sản xuất :  ' + data.prop_name + '</li>\n\
                                                                                                                                            <li> Hạn sử dụng :  12/12/2010</li>\n\
                                                                                                                                            <li> Bảo hành :  10 tháng</li>\n\
                                                                                                                                            <li> Khuyến mại :  -100.000 đ</li>\n\
                                                                                                                                        </ul>-->\n\
                                                                </div>\n\
                                                            </td>';

            html_insert += '<td><select  class="form-control  input-sm"  name="order_detail_store[]" style="width: 200px;">';
            html_insert += '<option value="0">Chọn kho hàng</option>';
            $.each(data.store, function (i, field) {
                console.log(field);
                html_insert += '<option value="' + field.id + '">' + field.product_store_code + '</option>';
            });
            html_insert += '</select></td>';
            html_insert += '<td>\n\
                     <input  value="" name="order_detail_price_sale[]" type="hidden">                                           <input class="form-control  input-sm mask_currency"  value="' + parseInt(data.price) + '" name="order_detail_price[]" id="" type="text">\n\
                                                                <span class="price_sale_lable">Giảm giá : 100.000 đ (KM)</span>\n\
                                                            </td>';
            html_insert += '<td><input class="form-control  input-sm mask_number" value="1" name="order_detail_quantity[]" type="text"></td>';
            html_insert += '<td>' + data.unit + '</td>';
            html_insert += '<td><input class="form-control  input-sm mask_number"  name="order_detail_vat[]" type="text"></td>';
            html_insert += '<td><input readonly="" value="' + parseInt(data.price) + '" class="form-control  input-sm mask_currency" name="order_detail_total[]"  type="text"></td>';
            html_insert += '<td style="text-align: center;"><a href="javascript:;" class="delete_detail" data-toggle="confirmation_yn" data-placement="left" data-original-title="Bạn có chắc chắn không ?"  aria-describedby="confirmation831254"><i class="fa fa-trash-o"></i></a></td>';
            html_insert += '</tr>';
            var tab_active = {};
            $('#order_content').children('.tab-pane').each(function () {
                if ($(this).hasClass('active')) {
                    tab_active = $(this);
                }
            });
            var check_exit = 0;
            tab_active.find('tbody').find('tr').each(function () {
                var p_id = $(this).find("input[name='order_detail_id_hidden[]']").val();
                if (p_id == data.id) {
                    $(this).find("input[name='order_detail_quantity[]']").val(parseInt($(this).find("input[name='order_detail_quantity[]']").inputmask('unmaskedvalue')) + 1);
                    check_exit = 1;
                }
            });
            if (check_exit == 0) {
                tab_active.find('tbody').append(html_insert);
            }
            $(".mask_currency").inputmask("₫ 999.999.999.999", {
                numericInput: !0
            });
            $(".mask_number").inputmask("9999", {
                numericInput: !0
            });
            $('[data-toggle=confirmation_yn]').confirmation({
                btnOkClass: "btn btn-sm btn-success",
                btnCancelClass: "btn btn-sm btn-danger",
                template: '<div class="popover confirmation"><div class="arrow"></div><div class="popover-content text-center"><div class="btn-group"><a class="btn" data-apply="confirmation"></a><a class="btn" data-dismiss="confirmation"></a></div></div></div>'
            });
            total_money();

        }

        $(document).on('change', "input[name='order_code_sale']", function (event) {
            var this_input = $(this);
            var this_code = $(this).val();
            var check_id = [];
            var check_coupon = [];
            var trans = db_index.transaction(['tbl_code', 'tbl_coupons'], 'readonly');
            trans.oncomplete = function (evt) {
                console.log(check_coupon);
                this_input.closest('.tab-pane').find("input[name='order_price_sale']").val(parseInt(check_coupon.value));
                total_money();
            }
            var tbl_code = trans.objectStore('tbl_code');
            var cursorRequest_tbl_store = tbl_code.index('code').get(this_code);
            cursorRequest_tbl_store.onsuccess = function (evt) {
                var cursor = evt.target.result;
                if (cursor) {
                    check_id = cursor;
                }
            };

            var tbl_coupons = trans.objectStore('tbl_coupons');
            var req_c = tbl_coupons.openCursor();
            req_c.onsuccess = function (evt_c) {
                var cursor = evt_c.target.result;
                if (cursor) {
                    var coupons = cursor.value;
                    if (coupons.id == check_id.id_destination) {
                        check_coupon = coupons;
                    }
                    cursor.continue();
                }
            }
        });

        $(document).on('change', "input[name='order_user_card']", function (event) {
            console.log('sad');
        });
        $(document).on('change', '#order_detail input', function (event) {
            total_money();
        });
        $(document).on('change', "input[name='order_user_money']", function (event) {
            total_money();
        });
        $(document).on('change', "input[name='order_price_sale']", function (event) {
            total_money();
        });
        $(document).on('click', '.auto_item', function (event) {
            var this_value = $(this).find('input.select_p_hidden').val();
            $(this).parent().parent().find('input.form-control').val('');
            $(this).parent().parent().find('.auto_div').html('');
            $(this).parent().parent().find('.auto_div').css('display', 'none');
            select_product(this_value);
        });
        $(document).mouseup(function (e)
        {
            var container = $("#auto_div");
            var container_input = $("#txt_search_input");
            if (!container.is(e.target)
                    && container.has(e.target).length === 0)
            {
                if (!container_input.is(e.target)
                        && container_input.has(e.target).length === 0)
                {
                    container.hide();
                }
            }
        });
        $(document).on('keyup', function (event) {
            if (event.key == 'Escape') {
                var this_parrent = $('#txt_search_input').parent();
                this_parrent.find('.auto_div').hide();
            }
        });
        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 200;  //time in ms, 5 second for example
        var $input = $('#txt_search_input');
        var this_input;
        var this_input_event;
//on keyup, start the countdown
        $input.on('keyup', function (event) {
            this_input = $(this);
            this_input_event = event;
            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
            var this_parrent = this_input.parent();
            if (this_parrent.find('.auto_div').html() != '') {
                if (this_input_event.key == 'Escape') {
                    this_parrent.find('input').val('');
                    this_parrent.find('.auto_div').html('');
                    this_parrent.find('.auto_div').css('display', 'none');
                }
                if (this_input_event.key == 'Enter') {
                    this_parrent.find('.auto_div').find('.auto_item').each(function (e) {
                        if ($(this).hasClass('active')) {
                            $(this).addClass('hover');
                            this_parrent.find('.auto_div').html('');
                            this_parrent.find('input').val('');
                            var this_value = $(this).find('input.select_p_hidden').val();
                            select_product(this_value);
                        }
                        check++;
                    });
                }
                if (this_input_event.key == 'ArrowUp') {
                    if (this_parrent.find('.auto_div').html() != '') {
                        this_parrent.find('.auto_div').show();
                    }
                    var check_active = 0;
                    var check = 1;
                    this_parrent.find('.auto_div').find('.auto_item').each(function (e) {
                        if ($(this).hasClass('active')) {
                            check_active = check;
                            $(this).removeClass('active');
                        }
                        check++;
                    });


                    if (1 == check_active) {
                        check_active = check;
                    }
                    check_active--;
                    var check_f = 1;
                    var hea = 0;
                    var h = 0;
                    this_parrent.find('.auto_div').find('.auto_item').each(function (e) {
                        h += $(this).height();
                        if (check_f == check_active) {
                            $(this).addClass('active');
                            hea = parseInt(h) - parseInt($(this).height());
                            this_parrent.find('.auto_div').scrollTop(hea).animate(1000);
                        }
                        check_f++;
                    });

                }
                if (this_input_event.key == 'ArrowDown') {
                    if (this_parrent.find('.auto_div').html() != '') {
                        this_parrent.find('.auto_div').show();
                    }
                    var check_active = 0;
                    var check = 1;
                    this_parrent.find('.auto_div').find('.auto_item').each(function (e) {
                        if ($(this).hasClass('active')) {
                            check_active = check;
                            $(this).removeClass('active');
                        }
                        check++;
                    });

                    check_active++
                    if (check == check_active) {
                        check_active = 1;
                    }
                    var check_f = 1;
                    var hea = 0;
                    var h = 0;
                    this_parrent.find('.auto_div').find('.auto_item').each(function (e) {
                        h += $(this).height();
                        if (check_f == check_active) {
                            $(this).addClass('active');
                            hea = parseInt(h) - parseInt($(this).height());
                            this_parrent.find('.auto_div').scrollTop(hea).animate(1000);
                        }
                        check_f++;
                    });
                }
            }
        });

//on keydown, clear the countdown 
        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });

//user is "finished typing," do something
        function doneTyping() {
            var w = this_input.parent().width();
            var m_l = parseInt(this_input.parent().css("padding-left").replace('px', '')) + parseInt(this_input.css("margin-left").replace('px', ''));
            var this_parrent = this_input.parent();
            var this_html = this_parrent.html();
            var this_value = this_input.val();
            var key_word = this_value.trim();
            if (this_input_event.key != 'ArrowUp' && this_input_event.key != 'ArrowDown' && this_input_event.key != 'Enter' && this_input_event.key != 'Escape') {
                if (this_value.length > 2) {
                    this_input.parent().find('.fa-spin').css('display', 'block');
                    var list_table = [];
                    var tbl_store_select = [];
                    var tbl_product_select = [];

                    $.each(db_index.objectStoreNames, function (i, field) {
                        list_table.push(field);
                    });
                    var trans = db_index.transaction(list_table, 'readonly');
                    trans.oncomplete = function (evt) {
                        console.log(tbl_product_select);
                        this_parrent.find('.auto_div').css('width', w + 'px');
                        this_parrent.find('.auto_div').css('left', m_l + 'px');
                        var html = '';
                        if (tbl_product_select.length == 0) {
                            this_parrent.find('.auto_div').html('<span>Không có dữ liệu ...</span>');
                            this_parrent.find('.auto_div').css('display', 'block');
                        } else {
                            $.each(tbl_product_select, function (i, field) {
                                var thumb = field.thumbnail;
//                                delete field.thumbnail;
                                html += '<div class="auto_item"><input type="hidden" class="select_p_hidden" value=\'' + JSON.stringify(field) + '\'/>';
                                html += ' <div class="auto_left"> <img src="' + thumb + '" class="auto_image"></div>';
                                html += '<div class="auto_right">\n\
                                    <span class="auto_title"> ' + field.name + '</span>\n\
                                    <span class="auto_des">Mã hàng hóa :  <strong>' + field.code + '</strong></span>\n\
  <span class="auto_des">Giá bán lẻ : <strong> ' + formatNumber(field.price) + '</strong> ₫</span>\n\
  <span class="auto_des">Giá bán buôn : <strong> ' + formatNumber(field.price_whole) + ' </strong> ₫</span>\n\
                                </div>';
                                html += '</div>';
                            });
                            if (html != '') {
                                this_parrent.find('.auto_div').html(html);
                                this_parrent.find('.auto_div').css('display', 'block');
                            }
                        }
                        this_parrent.find('.fa-spin').css('display', 'none');
                    }
                    //search store
                    var tbl_store = trans.objectStore('tbl_store');
                    var cursorRequest_tbl_store = tbl_store.index('product_store_code').get(key_word);
                    cursorRequest_tbl_store.onsuccess = function (evt) {
                        var cursor = evt.target.result;
                        if (cursor) {
                            tbl_store_select = cursor;
                        }
                    };
                    var store = trans.objectStore('tbl_product');
                    var cursorRequest = store.openCursor();
                    cursorRequest.onerror = function (error) {
                        console.log(error);
                    };
                    cursorRequest.onsuccess = function (evt) {
                        var cursor = evt.target.result;
                        if (cursor) {
                            var cur_row = cursor.value;
                            if (tbl_store_select.length != 0) {
                                if (tbl_store_select.p_id == cur_row.id) {
                                    tbl_product_select.push(cur_row);
                                    tbl_product_select[0].store = tbl_store_select;
                                }
                            } else {
                                var check_search = false;
                                if (typeof cur_row.code != "undefined" && cur_row.code != null) {
                                    if (cur_row.code.toLowerCase().indexOf(key_word.toLowerCase()) !== -1) {
                                        check_search = true;
                                    }
                                }
                                if (typeof cur_row.name != "undefined") {
                                    if (locdau(cur_row.name.toLowerCase()).indexOf(locdau(key_word.toLowerCase())) !== -1) {
                                        check_search = true;
                                    }
                                }
                                if (check_search == true) {
                                    tbl_product_select.push(cur_row);
                                }
                            }
                            cursor.continue();
                        }
                    }
                    var store = trans.objectStore('tbl_store');
                    var cursorRequest = store.openCursor();
                    cursorRequest.onerror = function (error) {
                        console.log(error);
                    };
                    cursorRequest.onsuccess = function (evt) {
                        if (tbl_store_select.length == 0) {
                            var cursor = evt.target.result;
                            if (cursor) {
                                var cur_row = cursor.value;
                                if (tbl_product_select.length > 0) {
                                    $.each(tbl_product_select, function (i, field) {
                                        tbl_product_select[i].store = [];
                                        if (field.id == cur_row.p_id) {
                                            tbl_product_select[i].store.push(cur_row);
                                        }
                                    });
                                }
                                cursor.continue();
                            }
                        }
                    }

                } else {
                    this_parrent.find('.auto_div').html('');
                    this_parrent.find('.auto_div').css('display', 'none');
                }
            }
        }
        $(document).on('click', '.delete_detail', function (event) {
            $(this).closest('tr').remove().animate();
        });
        $(document).on('click', '#event-info', function (event) {
            alert('ok');
        });

        function total_money() {


            var tab_active = {};
            $('#order_content').children('.tab-pane').each(function () {
                if ($(this).hasClass('active')) {
                    tab_active = $(this);
                }
            });
            var product_list = [];
            var product_total_price = 0;
            tab_active.find('#order_detail').children('tr').each(function (i) {
                var product_id = $(this).find("input[name='order_detail_id_hidden[]']").val();
                var product_name = $(this).find("input[name='order_detail_name_hidden[]']").val();
                var product_store = $(this).find("select[name='order_detail_store[]']").val();
                var product_row_price = $(this).find("input[name='order_detail_price[]']").inputmask('unmaskedvalue');
                var product_row_price_sale = $(this).find("input[name='order_detail_price_sale[]']").val();
                var product_row_quantity = $(this).find("input[name='order_detail_quantity[]']").inputmask('unmaskedvalue');
                var product_row_vat = $(this).find("input[name='order_detail_vat[]']").inputmask('unmaskedvalue');
                if (!product_row_price_sale) {
                    product_total_price += product_row_price * product_row_quantity;
                } else {
                    product_total_price += (product_row_price - product_row_price_sale) * product_row_quantity;
                }
                if (product_row_vat) {
                    var p_vat = (parseInt(product_total_price) / 100) * parseInt(product_row_vat);
                    product_total_price = parseInt(product_total_price) + parseInt(p_vat);
                }
                product_list.push({
                    product_id: product_id,
                    product_name: product_name,
                    product_store: product_store,
                    product_row_price: product_row_price,
                    product_row_price_sale: product_row_price_sale,
                    product_row_quantity: product_row_quantity,
                    product_row_vat: product_row_vat
                });

                $(this).find("input[name='order_detail_total[]']").val(parseInt(product_total_price));
            });
            var order_price_sale = tab_active.find("input[name='order_price_sale']").inputmask('unmaskedvalue');
            if (!order_price_sale) {
                order_price_sale = 0;
            }
            if ((parseInt(product_total_price) - parseInt(order_price_sale)) > 0) {
                product_total_price = parseInt(product_total_price) - parseInt(order_price_sale);
            } else {
                product_total_price = 0;
            }
            tab_active.find("input[name='order_total']").val(parseInt(product_total_price));
            var user_money = tab_active.find("input[name='order_user_money']").inputmask('unmaskedvalue');
            tab_active.find("input[name='order_user_back_money']").val(user_money - product_total_price);
            if (user_money < product_total_price) {
                tab_active.find("input[name='order_user_back_money']").css('color', 'red');
            } else {
                tab_active.find("input[name='order_user_back_money']").css('color', '#29b4b6');
            }
//
//            var t_money_count = 0;
//            var t_money_count_tax = 0;
//            var id_tabs = $('.tabs_order li.active>a[data-toggle="tab"]').attr('href');
//            count = 1;
//            var select_currency = $(id_tabs).find('#currency_id_select').val();
//            $(id_tabs).find('section.banhang').find('#product_order_list').children('tr').each(function (i) {
//                var t_money = $(this).find("input[name='product_price_order[]']");
//                var t_giam = $(this).find("input[name='giam_order_des[]']");
//                var t_quaty = $(this).find("input[name='product_quatity[]']");
//                var t_tax = $(this).find("input[name='product_tax_order[]']");
//                var t_event_sale = 0;
//                if ($(this).has("input[name='product_event_confirm[]']").length == 0) {
//                    t_event_sale = $(this).find("input[name='product_event_sale[]']").val();
//                } else {
//                    if ($(this).find("input[name='product_event_confirm[]']").is(':checked')) {
//                        t_event_sale = $(this).find("input[name='product_event_sale[]']").val();
//                    }
//                }
//                var gia = (parseFloat(t_money.autoNumeric('get')) - parseFloat(t_giam.val()) - parseFloat(t_event_sale)) * parseFloat(t_quaty.autoNumeric('get'));
//                tax_c = 0;
//                if (parseFloat(t_tax.autoNumeric('get')) > 0) {
//                    var tax_c = parseFloat(gia) * parseFloat(t_tax.autoNumeric('get')) / 100;
//                }
//                if (isNaN(gia)) {
//                    t_money_count = t_money_count;
//                    $(this).find('span.thanh_tien_order').html('0')
//                } else {
//                    $(this).find('span.thanh_tien_order').html(formatNumber(gia));
//                    t_money_count = parseFloat(gia) + t_money_count;
//                    t_money_count_tax = parseFloat(tax_c) + t_money_count_tax;
//                }
//            });
//            var t_ship = $(id_tabs).find("#ship_order_price").autoNumeric('get');
//            t_money_count = parseFloat(t_money_count) + parseFloat(t_ship);
//            var partner_discount = $(id_tabs).find("input[name='partner_card_discount']").val();
//            if (partner_discount) {
//                t_money_count = t_money_count * (1 - parseFloat(partner_discount) / 100);
//            }
//            var ex_rate_select = $(id_tabs).find("#currency_id_select").find('option:selected').data('exchane');
//            var coupon_discount = $(id_tabs).find("input[name='coupon_detail_value']").val();
//            if (coupon_discount) {
//                $(id_tabs).find("#giam_gia").autoNumeric('set', parseFloat(coupon_discount) / parseFloat(ex_rate_select));
//            }
//            var giam_gia = $(id_tabs).find("#giam_gia").autoNumeric('get');
//            var tien_khach_tra = $(id_tabs).find("#tien_khach_tra").autoNumeric('get');
//            var cur_name = 'VNĐ';
//            var ngoai_te = 0;
//            var ngoai_te_tax = 0;
//            var cur_te = $(id_tabs).find("#currency_id_select").find('option:selected').text();
//            var quy_doi = 0;
//
//            ngoai_te = t_money_count / parseFloat(ex_rate_select);
//            ngoai_te_tax = t_money_count_tax / parseFloat(ex_rate_select);
//            quy_doi = giam_gia / parseFloat(ex_rate_select);
//            ex_rate_select = parseFloat(ex_rate_select);
//            var t_money_buy = ngoai_te + ngoai_te_tax - giam_gia;
//            if (ngoai_te < giam_gia) {
//                t_money_buy = 0;
//            }
//            if (t_money_buy <= 0) {
//                $(id_tabs).find("#giam_gia").autoNumeric('set', ngoai_te);
//            }
//            if ($(id_tabs).find('.table_product_group_divide_item').has()) {
//                var total_money_return = 0;
//                $(id_tabs).find('.table_product_group_divide_item').children('tr').each(function (i) {
//                    var order_p_money_return = $(this).find("input[name='product_order_price_return[]']").autoNumeric('get');
//                    var order_p_quaty = $(this).find("input[name='product_order_return[]']").autoNumeric('get');
//                    if (order_p_money_return && order_p_quaty != 0) {
//                        total_money_return += parseFloat(order_p_money_return) * parseInt(order_p_quaty);
//                    }
//                });
//                var ngoai_te_total_money_return = total_money_return / parseFloat(ex_rate_select);
//                t_money_buy = parseFloat(t_money_buy) - parseFloat(ngoai_te_total_money_return);
//                $(id_tabs).find('#tong_tien_tra_hang').html(formatNumber(total_money_return) + ' ' + cur_name);
//                $(id_tabs).find('#tong_tien_tra_hang_ngoai_te').html(formatNumber(ngoai_te_total_money_return) + ' ' + cur_te);
//            }
//            $(id_tabs).find('#tong_tien_hang').html(formatNumber(t_money_count) + ' ' + cur_name);
//            $(id_tabs).find('#tong_tien_thue').html(formatNumber(t_money_count_tax) + ' ' + cur_name);
//            $(id_tabs).find('#tong_tien_hang_ngoai_te').html('~ ' + formatNumber2(ngoai_te) + ' ' + cur_te);
//            $(id_tabs).find('#tong_tien_thue_ngoai_te').html('~ ' + formatNumber2(ngoai_te_tax) + ' ' + cur_te);
//            $(id_tabs).find('#khach_can_tra').html(formatNumber2(t_money_buy) + ' ' + cur_te);
//
//            var tra_lai = 0;
//            tra_lai = tien_khach_tra - t_money_buy;
//            $(id_tabs).find('#tra_khach').html(formatNumber2(tra_lai) + ' ' + cur_te);
//            if (t_money_buy <= 0) {
//                $('strong#khach_can_tra ').css('color', 'red');
//            } else {
//                $('strong#khach_can_tra ').css('color', '#218876');
//            }
//            return t_money_count;
        }

        function user_to_input() {
            var users_list = [];
            var trans = db_index.transaction(['tbl_users'], 'readonly');
            trans.oncomplete = function (evt) {
                var html = '  <option value="0">Khách vãng lai</option>';
                $.each(users_list, function (key, value) {
                    html += ' <option value="' + value.id + '">' + value.full_name + ' - ' + value.phone + '</option>'
                });
                $('#user_select').html(html);
                $('#user_select').select2();
            }
            var tbl_users = trans.objectStore('tbl_users');
            var req = tbl_users.openCursor();
            req.onsuccess = function (evt) {
                var cursor = evt.target.result;
                if (cursor) {
                    var user = cursor.value;
                    users_list.push(user);
                    cursor.continue();
                }
            }

        }
        setTimeout(function () {
            var tab_active = {};
            $('#order_content').children('.tab-pane').each(function () {
                if ($(this).hasClass('active')) {
                    tab_active = $(this);
                }
            });
            var he = $('.page-content').css('min-height');
            he = he.replace('px', '');
            tab_active.find('.table-responsive').css('min-height', parseInt(he) - 15);
        }, 1000);

    }
    return {
        init: function () {
            e()
        }
    }
}();
App.isAngularJsApp() === !1 && jQuery(document).ready(function () {
    OrderJs.init()
});