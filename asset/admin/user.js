$(function () {
	$("input[name='pay_type']").click(function(){
		if($(this).val()==0){
			$("#pay_type_bank").hide();
		}else{
			$("#pay_type_bank").show();
		}
	});
    $('.search_ncc').click(function () {
        var add_name = $(this).data('name');
        var this_tr_click = $(this).parent().parent();
        $('#modal_search').modal();
        creat_autocom1();
    });
    $("select[name='exchange_store_type']").change(function () {
        if($(this).val()==0){
            $('.exchange_store').show();
            $('.xuathang').hide();
        }else{
            $('.exchange_store').hide();
            $('.xuathang').show();
        }
    });
    $("#print_search").autocomplete({
        source: url_search,
        minLength: 3,
        select: function (event, ui) {
            var count = $('tr[id="' + ui.item.product_store_code + '"]').length;
            if (count <= 0) {
                var html = '<tr id="' + ui.item.product_store_code + '">';
                html += '<td><input type="hidden" name="store_id[]" value="' + ui.item.store_id + '"><input type="hidden" name="product_store_code[]" value="' + ui.item.product_store_code + '"><input type="hidden" name="store_code[]" value="' + ui.item.store_code + '">' + ui.item.product_store_code + '</td>';
                html += '<td>' + ui.item.product_code + '-'+ui.item.name+'</td>';
                html += '<td><input type="hidden" name="sodu[]" value="' + (parseInt(ui.item.quantity) - parseInt(ui.item.quantity_sold) -parseInt(ui.item.quantity_lost)-parseInt(ui.item.quantity_divide)-parseInt(ui.item.quantity_destroy)-parseInt(ui.item.quantity_give)-parseInt(ui.item.quantity_internal)-parseInt(ui.item.quantity_change)+parseInt(ui.item.quantity_find)+parseInt(ui.item.quantity_import_internal)) + '">' + formatNumber((parseInt(ui.item.quantity) - parseInt(ui.item.quantity_sold) -parseInt(ui.item.quantity_lost)-parseInt(ui.item.quantity_divide)-parseInt(ui.item.quantity_destroy)-parseInt(ui.item.quantity_give)-parseInt(ui.item.quantity_internal)+parseInt(ui.item.quantity_find)+parseInt(ui.item.quantity_import_internal))) + '</td>';
                html += '<td><input name="number[]" class="bg-focus form-control input-sm auto_number"   type="text" value=""></td>';
                html +='<td>';
                if(ui.item.property.length>0){
                    for(var i=0;i<ui.item.property.length;i++){
                        if(ui.item.property[i].hasOwnProperty('parent') && ui.item.property[i]['parent']==0){
                            if(ui.item.property[i].hasOwnProperty('pp_name')){
                                html+= "<b>"+ui.item.property[i]['pp_name']+":</b>";
                            }
                            for(var j=0;j<ui.item.property.length;j++){
                                if(ui.item.property[j].hasOwnProperty('parent') && ui.item.property[j]['parent']==ui.item.property[i]['id']){
                                    if(ui.item.property[j].hasOwnProperty('pp_name')){
                                        html+= ui.item.property[j]['pp_name']+"&nbsp;";
                                    }
                                }
                            }
                            
                        }                        
                        html+= "<br/>";
                    }
                }
                html+='</td>';
                html += '<td><input name="note_detail[]" class="bg-focus form-control input-sm auto_number"   type="text" value=""></td>';
                html += '<td><a href="javascript:void(0);" onclick="removeThis(this);"><i class="fa fa-trash-o "></i></a></td>';
                html += '</tr>';  
                $('#product_event_list').append(html);
                $('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'}); 
            }
            return false;
        }
    });
	/* kiem kho search */
	$("#check_store_search").autocomplete({
        source: url_search,
        minLength: 3,
        select: function (event, ui) {
            var count = $('tr[id="' + ui.item.product_store_code + '"]').length;
            if (count <= 0) {
                var html = '<tr id="' + ui.item.product_store_code + '">';
                html += '<td><input type="hidden" name="product_id_store[]" value="' + ui.item.store_id + '">' + ui.item.product_store_code + '</td>';
                html += '<td>' + ui.item.p_name+'</td>';
                html += '<td><input type="hidden" name="quantity[]" value="' + (parseInt(ui.item.quantity) - parseInt(ui.item.quantity_sold) -parseInt(ui.item.quantity_lost)-parseInt(ui.item.quantity_divide)-parseInt(ui.item.quantity_destroy)-parseInt(ui.item.quantity_give)-parseInt(ui.item.quantity_internal)-parseInt(ui.item.quantity_change)+parseInt(ui.item.quantity_find)+parseInt(ui.item.quantity_import_internal)) + '">' + formatNumber((parseInt(ui.item.quantity) - parseInt(ui.item.quantity_sold) -parseInt(ui.item.quantity_lost)-parseInt(ui.item.quantity_divide)-parseInt(ui.item.quantity_destroy)-parseInt(ui.item.quantity_give)-parseInt(ui.item.quantity_internal)+parseInt(ui.item.quantity_find)+parseInt(ui.item.quantity_import_internal))) + '</td>';
                html += '<td><input name="number[]" class="bg-focus form-control input-sm auto_number"   type="text" value=""></td>';
                html+='<td><select name="type_check[]" id="type_check" class="form-control input-sm"><option value="">Chọn</option><option value="0">Mất</option><option value="1">Hủy</option><option value="2">Tìm thấy</option></select></td>';
                html += '<td><input name="note_detail[]" class="bg-focus form-control input-sm"   type="text" value=""></td>';
                html += '<td><a href="javascript:void(0);" onclick="removeThis(this);"><i class="fa fa-trash-o "></i></a></td>';
                html += '</tr>';  
                $('#product_event_list').append(html);
                $('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'}); 
            }
            return false;
        }
    });
	$(document).on('change', "#product_event_list input[name='number[]']", function (event) {
		var c_this = $(this);
		c_this.closest('tr').find("select[name='type_check[]']").find('option').each(function () {
			$(this).removeAttr('disabled');
			$(this).css('display', 'block');
		});
		var number_current = c_this.autoNumeric('get');
		var number_store = c_this.closest('tr').find("input[name='quantity[]']").val();
		if (parseInt(number_store) > parseInt(number_current)) {
			c_this.closest('tr').find("select[name='type_check[]']").find('option').each(function () {
				if ($(this).val() == '2') {
					$(this).attr('disabled', 'disabled');
					$(this).css('display', 'none');
				}
			});
		}
		if (parseInt(number_store) < parseInt(number_current)) {
			c_this.closest('tr').find("select[name='type_check[]']").find('option').each(function () {
				if ($(this).val() == '1' || $(this).val() == '0') {
					$(this).attr('disabled', 'disabled');
					$(this).css('display', 'none');
				}
			});
		}
		if (parseInt(number_store) == parseInt(number_current)) {
			c_this.closest('tr').find("select[name='type_check[]']").find('option').each(function () {
				if ($(this).val() != '') {
					$(this).attr('disabled', 'disabled');
					$(this).css('display', 'none');
				}
			});
		}
	});
    $("#mahopdong").autocomplete({
        source: url_search + "?type=" + $('input[name=receiver]:checked').val() + "&branch=" + $("#branches_id").val(),
        minLength: 3,
        select: function (event, ui) {
            var count = $('tr[id="' + ui.item.id + '"]').length;
            if (count <= 0) {
                var html = '<tr id="' + ui.item.id + '">';
                html += '<td><input type="hidden" name="code_contract[]" value="' + ui.item.code + '">' + ui.item.code + '</td>';
                html += '<td><input type="hidden" name="sodu" value="' + ui.item.sodu + '"/><input type="hidden" name="tigia" value="' + ui.item.cur_rate + '"/>' + formatNumber(ui.item.sodu) + ' ' + ui.item.cur_name + '</td>';
                html += '<td><div class="input-group"><input type="text" name="amount[]" value="0" class="form-control auto_number"/><span class="input-group-btn"><span class="btn blue label-cur">' + $('select[name="pay_unit"] option:selected').text() + '</span></span></div></td>';
                html += '<td><a href="javascript:void(0);" onclick="removeThis(this);"><i class="fa fa-trash-o "></i></a></td>';
                html += '</tr>';
                $('#table_receiver').append(html);
                $('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'});
                tinhtien($("input[name='default_rate']").val());
            }
            return false;

        }
    });

    $('input:radio[name=receiver]').click(function () {
        $("#mahopdong").autocomplete({
            source: url_search + "?type=" + $('input[name=receiver]:checked').val() + "&branch=" + $("#branches_id").val(),
            minLength: 3,
            select: function (event, ui) {
                var count = $('tr[id="' + ui.item.id + '"]').length;
                if (count <= 0) {
                    var html = '<tr id="' + ui.item.id + '">';
                    html += '<td><input type="hidden" name="code_contract[]" value="' + ui.item.code + '">' + ui.item.code + '</td>';
                    html += '<td><input type="hidden" name="sodu" value="' + ui.item.sodu + '"/><input type="hidden" name="tigia" value="' + ui.item.cur_rate + '"/>' + formatNumber(ui.item.sodu) + ' ' + ui.item.cur_name + '</td>';
                    html += '<td><div class="input-group"><input type="text" name="amount[]" value="0" class="form-control auto_number"/><span class="input-group-btn"><span class="btn blue label-cur">' + $('select[name="pay_unit"] option:selected').text() + '</span></span></div></td>';
                    html += '<td><a href="javascript:void(0);" onclick="removeThis(this);"><i class="fa fa-trash-o "></i></a></td>';
                    html += '</tr>';
                    $('#table_receiver').append(html);
                    $('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'});
                    tinhtien($("input[name='default_rate']").val());
                }
                return false;

            }
        });
    });

    $("#branches_id").change(function () {
        $("#mahopdong").autocomplete({
            source: url_search + "?type=" + $('input[name=receiver]:checked').val() + "&branch=" + $("#branches_id").val(),
            minLength: 3,
            select: function (event, ui) {
                var count = $('tr[id="' + ui.item.id + '"]').length;
                if (count <= 0) {
                    var html = '<tr id="' + ui.item.id + '">';
                    html += '<td><input type="hidden" name="code_contract[]" value="' + ui.item.code + '">' + ui.item.code + '</td>';
                    html += '<td><input type="hidden" name="sodu" value="' + ui.item.sodu + '"/><input type="hidden" name="tigia" value="' + ui.item.cur_rate + '"/>' + formatNumber(ui.item.sodu) + ' ' + ui.item.cur_name + '</td>';
                    html += '<td><div class="input-group"><input type="text" name="amount[]" value="0" class="form-control auto_number"/><span class="input-group-btn"><span class="btn blue label-cur">' + $('select[name="pay_unit"] option:selected').text() + '</span></span></div></td>';
                    html += '<td><a href="javascript:void(0);" onclick="removeThis(this);"><i class="fa fa-trash-o "></i></a></td>';
                    html += '</tr>';
                    $('#table_receiver').append(html);
                    $('.auto_number').autoNumeric('init', {pSign: 's', dGroup: '2'});
                    tinhtien($("input[name='default_rate']").val());
                }
                return false;

            }
        });
    });

    var befor_change = $('select[data-change^="branches"]').val();
    $('select[data-change^="branches"]').change(function (event) {
        var this_select = $(this);
        $('#ajax_alert_confirm').modal('show').one('click', '#delete-confirm', function (e) {
            $('#' + this_select.data('remove-table')).html('');
            $('#ajax_alert_confirm').modal('hide');
            resetTotal();
        }).one('click', '#delete-cancel', function (e) {
            this_select.val(befor_change);

        });
        ;
    });

    j170("input[name='auto_prefix']").change(function () {
        var input = $(this).data('input');
        var url = $(this).data('url');
        if ($(this).is(':checked')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'html',
                success: function (data) {
                    $('#' + input).val(data);
                }
            });

        } else {
            $('#' + input).val('');
        }
    });

    j170('.clickorder_detail').live('click', function (event) {

        var current_id = $(this).data('current-id');
        var url = $(this).data('url-post');

        $.ajax({
            url: url,
            data: {id: current_id},
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#order_detail').html(data);
                $('#pop_up_orderdetail').modal();
            }
        });
    });
    j170('input:radio[name="receiver"]').change(function () {
        if (j170(this).val() == 1) {
            $("#div_receiver1").hide();
            $(".div_receiver").show();
            j170.ajax({
                url: url_user,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    j170('#select2-option').html(data.html);
                }
            });
            j170("#table_receiver").empty();
        } else if (j170(this).val() == 0) {
            $("#div_receiver1").hide();
            $(".div_receiver").show();
            j170.ajax({
                url: url_vendor,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    j170('#select2-option').html(data.html);
                }
            });
            j170("#table_receiver").empty();
        } else {
            $("#div_receiver1").show();
            $(".div_receiver").hide();
        }
    });
    $("#pay_unit").change(function () {

        $(".label-cur").text($('select[name="pay_unit"] option:selected').text());
        var id = $(this).val();
        $.ajax({
            url: url_cur,
            data: {id: id},
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $("input[name='default_rate']").val(data);
                tinhtien(data);
                trahet();
            }
        });

    });
    $(".label-cur").text($('select[name="pay_unit"] option:selected').text());
    $("#trahet").click(function () {
        trahet();
    });
    tinhtien($("input[name='default_rate']").val());
});
function creat_autocom1() {
        $("#input_search").autocomplete({
            source: url_search,
            minLength: 3,
            select: function (event, ui) {
                    var html = '<div class="alert alert-success product_add_span" style="margin:5px;"> <button type="button" class="close" data-name="span_list" data-id="list_id" data-id_del="' + ui.item.id + '"><i class="fa fa-times"></i></button><strong> ' + ui.item.label + ' </strong></div>';
                    $('#span_list').append(html);
                    if($('#list_id').val()!=''){
                        $('#list_id').val($('#list_id').val()+','+ui.item.id);                      
                    }else{
                        $('#list_id').val($('#list_id').val()+ui.item.id);
                    }           
                                            
                return false;
            }
        
    });
}
function removeThis(field) {

    $(field).parent().parent().remove();
    tinhtien($("input[name='default_rate']").val());
}
;
function resetTotal() {
    $("#total").val(formatNumber(0));
}
function trahet() {
    if ($("#table_receiver tr").length > 0) {
        $("#table_receiver tr").each(function () {
            var tr_rate = parseFloat($(this).find("input[name='tigia']").val());
            var tr_sodu = parseFloat($(this).find("input[name='sodu']").val());
            var input_cur = tr_sodu * tr_rate / $("input[name='default_rate']").val();
            $(this).find("input[name='amount[]']").val(formatNumber(parseFloat(input_cur)));
        });
    }
}
function tinhtien(rate) {
    var tongtien = 0;
    var payunit_rate = parseFloat(rate);
    if ($("#table_receiver tr").length > 0) {
        $("#table_receiver tr").each(function () {
            var cur_rate = parseFloat($(this).find("input[name='tigia']").val());
            var cur_sodu = parseFloat($(this).find("input[name='sodu']").val());
            tongtien += cur_sodu * cur_rate / payunit_rate;
        });
    }
    $("#total").val(formatNumber(parseFloat(tongtien)));
}
