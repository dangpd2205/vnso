function change_lang(url) {    window.location.replace(url);}$(document).ready(function() {
    $("a.video-popup").YouTubePopUp();
	$("a.reply").click(function() {
        $('html, body').animate({
            scrollTop: $("#comment").offset().top
        }, 2000);
        var this_del = $(this);
        var url_link = this_del.data('url');
        var id = this_del.data('id');
        var parent = this_del.data('parent');
        var addCart = $.ajax({
            url: url_link + "?id=" + id + '&parent=' + parent,
            type: "post",
            dataType: "html"
        });
        addCart.done(function(data) {
            var msg = jQuery.parseJSON(data);
            $('#li_hidden').show();

            $('#reply_name').val(msg.name);
            $('#parent').val(parent);
        });
    });
    $(".modal_artist").click(function() {
        var addCart = $.ajax({
            url: $(this).data("url") + "?id=" + $(this).data("id"),
            type: "post",
            dataType: "html"
        });
        addCart.done(function(msg) {
            $('.content_modal').html(msg);
            $('#myModal').modal();
        });
    });
});
function hide_div() {
    $('#li_hidden').hide();
    $('#reply_name').val('');
    $('#parent').val('');
}
function submitForm(id) {
    $('#' + id).submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
            {
                url: formURL,
                type: "POST",
                data: postData,
                success: function(data, textStatus, jqXHR)
                {
                    msg = jQuery.parseJSON(data);
                    if (msg.check == 0) {
                        $('#'+msg.alert).html("<p style='color:red'>" + msg.content+"</p>");
                    } else if (msg.check == 2) {
                        $('#'+msg.alert).removeClass('alert alert-error').addClass('alert alert-success').html("<button type='button' class='close' data-miss='alert'>×</button>" + msg.content);
                    } else if (msg.check == 1) {
                        $('#'+msg.alert).html("<p style='color:blue'>" + msg.content+"</p>");
                        $('#li_hidden').hide();
                        $('#subject').val('');
                        $('#name').val('');
                        $('#email').val('');
                        $('#comment').val('');
                        $('#post_id').val('');
                        $('#parent').val('');
                        $('#reply_name').val('');
                        // reset lai form contact
                        if(typeof msg.form !== 'undefined'){
                            $('#'+msg.form)[0].reset();
                        }
                    }
                    //data: return data from server
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    //if fails     
                }
            });
        e.preventDefault(); //STOP default action
        e.unbind();
    });
    $('#' + id).submit();
}